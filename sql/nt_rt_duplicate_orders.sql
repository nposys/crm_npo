--  15105 - Новоторг

-- --------------------------------------------------------------------------------
-- получаем список зачтенных заказов РТ / НТ
-- --------------------------------------------------------------------------------
select distinct
       ord_rt.id as rt_id,
       ord_rt.f7071 as rt_num,
       ord_rt.f4431  as rt_name_order,
       ord_rt.f4461  as rt_sum_order, -- сумма заказа
       ord_rt.f10270 as rt_sum_paid,  -- оплачено
       ord_rt.f6551 as rt_status,
       ord_rt.f14791 as rt_par_id,
       ord_rt_par.id as rt_par_id,
       ord_rt_par.f7071 as rt_par_num,
       ord_rt_par.f4431 as rt_par_name,
       ord_rt_par.f4461 as rt_par_sum,
       ord_rt_par.f10270 as rt_par_paid,
       ord_rt_par.f6551 as rt_par_status,
       ord_nt.id as nt_id,
       ord_nt.f7071 as nt_num,
       ord_nt.f4431 as nt_name,
       ord_nt.f4461 as nt_sum,
       ord_nt.f10270 as nt_paid,
       ord_nt.f6551 as nt_status
from
    cb_data271 as ord_rt
        left join cb_data271 as ord_rt_par on ord_rt_par.id = ord_rt.f14791
        left join cb_data271 as ord_nt on ord_nt.f4431 = ord_rt_par.f4431 and ord_nt.f10270 = 0.0
where
    true
  and ord_rt.status = 0
  and ord_rt.f4441 = 3813 -- Регионторг
  and ord_rt.f4451 = 5    -- подготовка заявки
  and ord_rt.f6551 IN ('30. Завершено')
  and ord_rt.f14791 > 0 -- есть родительский заказ


-- --------------------------------------------------------------------------------
-- получаем список заказов-дубликатов РТ которые надо бы компенсировать из новых
select
    ord_rt_child.id as rt_child_id,
    ord_rt_child.f7071 as rt_child_num,
    ord_rt_child.f4431  as rt_child_name,
    ord_rt_child.f4461  as rt_child_sum, -- сумма заказа
    ord_rt_child.f10270 as rt_child_paid,  -- оплачено
    ord_rt.id as rt_id,
    ord_rt.f7071 as rt_num,
    ord_rt.f4431  as rt_name,
    ord_rt.f4461  as rt_sum, -- сумма заказа
    ord_rt.f10270 as rt_paid,  -- оплачено
    ord_nt.id as nt_id,
    ord_nt.f7071 as nt_num,
    ord_nt.f4431  as nt_name,
    ord_nt.f4461  as nt_sum, -- сумма заказа
    ord_nt.f10270 as nt_paid  -- оплачено

from
     cb_data271 as ord_rt
        left join cb_data271 as ord_nt on ord_nt.f4431 = ord_rt.f4431 and ord_nt.f10270 = 0.0
        left join cb_data271 as ord_rt_child on ord_rt_child.f14791 = ord_rt.id
where
      true
  and ord_rt.status = 0
  and ord_rt.f4441 = 3813 -- Регионторг
  and ord_rt.f4451 = 5    -- подготовка заявки
  and ord_rt.f6551 IN ('30. Завершено')
  and ord_rt.f4461 > 0 -- сумма заказа
  and ord_rt.f10270 > 0 -- оплачено
  and ord_rt.f4431 IN
      (-- выбор завершенных заказов заявок новоторга
          select ord_nt.f4431
          from cb_data271 as ord_nt
          where true
            and ord_nt.status = 0
            and ord_nt.f4441 = 15105 -- Новоторг
            and ord_nt.f4451 = 5     -- подготовка заявки
            and ord_nt.f6551 NOT IN ('98. Отказ', '97. Не исполнено')
      );

-- заказы заявок РТ с дочерними заказами
select
  ord_rt.f4431
from
    cb_data271 as ord_rt
where
  true
  and ord_rt.status = 0
  and ord_rt.f4441 = 3813 -- Регионторг
  and ord_rt.f4451 = 5 -- подготовка заявки
  and ord_rt.f6551  IN ('30. Завершено')
  and ord_rt.f14791 > 0;



-- выбор завершенных заказов заявок новоторг
select
    ord_nt.*
from
    cb_data271 as ord_nt
where
    true
    and ord_nt.status = 0
    and ord_nt.f4441 = 15105 -- Новоторг
    and ord_nt.f4451 = 5 -- подготовка заявки
    and ord_nt.f6551 NOT IN ('98. Отказ', '97. Не исполнено')
 and ord_nt.f4431 IN
     (
         select ord_rt.f4431 -- название (описание)
         from cb_data271 as ord_rt
         where true
           and ord_rt.status = 0
           and ord_rt.f4441 = 3813 -- Регионторг
           and ord_rt.f4451 = 5    -- подготовка заявки
           and ord_rt.f6551 IN ('30. Завершено')
           and ord_rt.id IN
               (-- Не оплаченные заверщенные заказы РТ без парента
                select ord_rt.id
                from cb_data271 as ord_rt
                where true
                  and ord_rt.status = 0
                  and ord_rt.f4441 = 3813 -- Регионторг
                  and ord_rt.f4451 = 5    -- подготовка заявки
                  and ord_rt.f6551 IN ('30. Завершено')
                  and ord_rt.f4461 > 0 -- сумма заказа
                  and ord_rt.f10270 = 0 -- оплачено
                  and ord_rt.f14791 = 0 -- parent_id
                   )
     )