{if false}
    <!--   {include file='add/view/marketing/marketing.tpl'} -->
{/if}
<style>
    table.table.header {
        position: relative;
        margin-top: 20px;
    }
    table.table.header th.head{
        position: sticky;
        top: -1px;
        border: 1px solid #ddd;
        background:white;
        height: 60px;
        z-index: 1;
    }
    table.table.header th.sub-head{
        position: sticky;
        top: 75px;
        background:white;
    }
    td.nowrap, th.nowrap{
        white-space: nowrap;
    }
    td span.sticky{
        position: sticky;
        top: 80px;
    }
    div.wrapper{
        margin: 20px 0px 10px;
    }
    span.sort{
        cursor: pointer;
        position: absolute;
        right: 2px;
        top: 2px
    }

</style>
<div class="wrapper">
    <!-- фильтры таблицы -->
    <table>
        <tr>
            <td>
                <label>Дата начала выборки</label>
            </td>
            <td>
                <input type="text" name="date_from" id="date_from" value="{$date_from}" size="10" class="datepicker form-control" />
            </td>
        </tr>
        <tr>
            <td>
                <label>Дата конца выборки</label>
            </td>
            <td>
                <input type="text" name="date_to" id="date_to" value="{$date_to}" size="10" class="datepicker form-control" />
            </td>
        </tr>
        {if $is_admin}
            <tr>
                <td>
                    <label>Показать Регионторг/Новоторг</label>
                </td>
                <td>
                    <input type="checkbox" name="show_rt" id="show_rt" {if $show_rt=='on'} checked {/if}>
                </td>
            </tr>
        {/if}
        <tr>
            <td>
                <label>Выбрать компанию</label>
            </td>
            <td>
                <select name="select_company_id" id="select_company_id" class="form-control">
                    <option value="0">Все компании</option>
                    {foreach from=$table_data.select_companies key=company_id item=company_name}
                        <option value="{$company_id}" {if $company_id == $select_company_id} selected {/if}>{$company_name}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td>
                <label>Первый источник</label>
            </td>
            <td>
                <select name="select_channel_id" id="select_channel_id" class="form-control">
                    <option value="0">Все источники</option>
                    {foreach from=$table_data.select_channels key=channel_id item=channels_name}
                        <option value="{$channel_id}" {if $channel_id == $select_channel_id} selected {/if}>{$channels_name}</option>
                    {/foreach}
                </select>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                <button class="btn btn-primary btn-sm active" name="apply_filter" value="1">Обновить</button>
            </td>
        </tr>
    </table>

    <table class="table table-sm table-striped table-bordered header">
        <tr >
            <th width="300" class="head">
                Клиент
                <span class="sort glyphicon
                        {if $sort_col =='company_name'}
                            {if $sort_direction =='asc'}glyphicon-sort-by-alphabet
                            {elseif $sort_direction =='desc'}glyphicon-sort-by-alphabet-alt
                            {else}glyphicon-sort{/if}
                        {else}
                            glyphicon-sort
                        {/if}"
                      onclick="sort(this,'company_name')"></span>
            </th>
            <th class="head">
                UserID
            </th>
            <th class="head">
                Создание карточки<span title="Дата создания карточки компании в CRM" class="glyphicon glyphicon-question-sign"></span>
                <span class="sort glyphicon
                        {if $sort_col =='company_reg_date'}
                            {if $sort_direction =='asc'}glyphicon-sort-by-alphabet
                            {elseif $sort_direction =='desc'}glyphicon-sort-by-alphabet-alt
                            {else}glyphicon-sort{/if}
                        {else}
                            glyphicon-sort
                        {/if}"
                      onclick="sort(this,'company_reg_date')"></span>
            </th>
            <th class="head">
                Первое событие<span title="Дата первого события (по Группе компаний) в CRM" class="glyphicon glyphicon-question-sign"></span>
                <span class="sort glyphicon
                        {if $sort_col =='company_first_event_date'}
                            {if $sort_direction =='asc'}glyphicon-sort-by-alphabet
                            {elseif $sort_direction =='desc'}glyphicon-sort-by-alphabet-alt
                            {else}glyphicon-sort{/if}
                        {else}
                            glyphicon-sort
                        {/if}"
                      onclick="sort(this,'company_first_event_date')"></span>
            </th>
            <th class="head">
                Первый источник трафика
                <span class="sort glyphicon
                        {if $sort_col =='first_channel'}
                            {if $sort_direction =='asc'}glyphicon-sort-by-alphabet
                            {elseif $sort_direction =='desc'}glyphicon-sort-by-alphabet-alt
                            {else}glyphicon-sort{/if}
                        {else}
                            glyphicon-sort
                        {/if}"
                      onclick="sort(this,'first_channel')"></span>
            </th>
            <th class="head">
                Регион
                <span class="sort glyphicon
                        {if $sort_col =='company_region'}
                            {if $sort_direction =='asc'}glyphicon-sort-by-alphabet
                            {elseif $sort_direction =='desc'}glyphicon-sort-by-alphabet-alt
                            {else}glyphicon-sort{/if}
                        {else}
                            glyphicon-sort
                        {/if}"
                      onclick="sort(this,'company_region')"></span>
            </th>
            <th class="head">
                Всего событий<span title="Общее количество события (по Группе компаний) в CRM" class="glyphicon glyphicon-question-sign"></span>
                <span class="sort glyphicon
                        {if $sort_col =='count_visits'}
                            {if $sort_direction =='asc'}glyphicon-sort-by-alphabet
                            {elseif $sort_direction =='desc'}glyphicon-sort-by-alphabet-alt
                            {else}glyphicon-sort{/if}
                        {else}
                            glyphicon-sort
                        {/if}"
                      onclick="sort(this,'count_visits')"></span>
            </th>
            <th class="head">
                Дата первого заказа (по Группе компаний) <span title="Дата первого заказа" class="glyphicon glyphicon-question-sign"></span>
                <span class="sort glyphicon
                        {if $sort_col =='company_first_order_date'}
                            {if $sort_direction =='asc'}glyphicon-sort-by-alphabet
                            {elseif $sort_direction =='desc'}glyphicon-sort-by-alphabet-alt
                            {else}glyphicon-sort{/if}
                        {else}
                            glyphicon-sort
                        {/if}"
                      onclick="sort(this,'company_first_order_date')"></span>
            </th>
            <th colspan="3" class="head">
                НПО Система
            </th>
            <th colspan="3" class="head">
                TM
            </th>
            <th class="head">
                ИТОГО
            </th>
        </tr>
        {$z_index = count($table_data.companies)}
    {foreach from=$table_data.companies key=company_id item=company_data}
        <tr>
            <td rowspan="{count($company_data.orders) + 1}">
                <span class="sticky">
                    <a target="_blank" href="{$config.base_url}/view_line2.php?table=42&line={$company_id}">
                        {$company_data.company_name}
                    </a>
                    <br>{$company_data.company_inn}
                    {if $company_data.company_group != ""}
                        <br><b>Группа компаний: </b>
                        <a target="_blank" href="{$config.base_url}/view_line2.php?table=311&line={$company_data.company_group_id}">
                        {$company_data.company_group}
                        </a>

                    {/if}
                </span>
            </td>
            <td rowspan="{count($company_data.orders) + 1}">
                 <span class="sticky">{$company_data.company_user_id}</span>
            </td>
            <td rowspan="{count($company_data.orders) + 1}" class="nowrap">
                 <span class="sticky">{$company_data.company_reg_date}</span>
            </td>
            <td rowspan="{count($company_data.orders) + 1}" class="nowrap">
                <span class="sticky">{$company_data.company_first_event_date}</span>
            </td>
            <td rowspan="{count($company_data.orders) + 1}">
                <span class="sticky"> {$company_data.first_channel}</span>
            </td>
            <td rowspan="{count($company_data.orders) + 1}">
                <span class="sticky"> {$company_data.company_region}</span>
            </td>
            <td rowspan="{count($company_data.orders) + 1}">
                 <span class="sticky">{$company_data.count_visits}</span>
            </td>
            <td rowspan="{count($company_data.orders) + 1}"  class="nowrap">
                 <span class="sticky">{$company_data.company_first_order_date}</span>
            </td>
            <th class="sub-head" style="z-index: {$z_index};">
                Услуги
            </th>
            <th class="sub-head" style="z-index: {$z_index};">
                Сумма
            </th>
            <th class="sub-head" style="z-index: {$z_index};">
                Дата
            </th>
            <th class="sub-head" style="z-index: {$z_index};">
                Услуги
            </th>
            <th class="sub-head" style="z-index: {$z_index};">
                Сумма
            </th>
            <th class="sub-head" style="z-index: {$z_index};">
                Дата
            </th>
            <th class="sub-head nowrap" >
                {$company_data.total_sum}
            </th>
        </tr>
        {foreach from=$company_data.orders  item=order_data}
            <tr>
                <td>
                    {$order_data.service_title_npo}
                </td>
                <td class="nowrap">
                    {$order_data.order_sum_npo}
                </td>
                <td class="nowrap">
                    {$order_data.order_date_npo}
                </td>
                <td>
                    {$order_data.service_title_tm}
                </td>
                <td class="nowrap">
                    {$order_data.order_sum_tm}
                </td>
                <td class="nowrap">
                    {$order_data.order_date_tm}
                </td>
                <td>

                </td>
            </tr>
        {/foreach}

        {$z_index = $z_index - 1}
    {/foreach}
        <tr>
            <td colspan="13" style="text-align: right;">
                 ИТОГО:
            </td>
            <td class="nowrap">
                {$table_data.total_sum}
            </td>
        </tr>
</table>
    <input type="hidden" id="sort_col" name="sort_col" value="">
    <input type="hidden" id="sort_direction" name="sort_direction" value="">

</div>

<script type="text/javascript">

    $(function() {
        $('.datepicker').datepicker({
            showOn:"button",
            showAlways: true,
            buttonImage: "images/calbtn.png",
            buttonImageOnly: true,
            buttonText: "Calendar",
            dateFormat: 'yy-mm-dd',
            showAnim: (('\v'=='v')?"":"show"),  // в ie не включаем анимацию, тормозит
        })
    });
    
    function sort(element,sort_col) {
        $('#sort_col').val(sort_col);
        if($(element).hasClass('glyphicon-sort-by-alphabet')){
            $('#sort_direction').val('desc');
        }else{
            $('#sort_direction').val('asc');
        }
        $('form#report_form').submit();
    }
</script>