<?php

use classes\Controllers\SalaryController;

require_once 'add/vendor/autoload.php';

/*
 * В таблице типы мотивации добавить
 * Коэффициент за сумму поступлений по типовым услугам
 *
 * В таблице Система мотивации для коэффициентов для менеджеров проставить базу "Личное"
 *
 * Прописать коэффициенты за сумму поступлений по типовым услугам
 *
 * В настройках отчётов по зарплате сменить в поле Отображение значение с
 * {include file='add/reports/sales_salary_data_view.html'} на {include file='add/reports/sales_salary_data_view.tpl'}
 */

$newSalaryController = new SalaryController();
try {
	$newSalaryController->main();
} catch (Exception $e) {
	echo "Что-то пошло не так, попробуйте ещё раз";
}