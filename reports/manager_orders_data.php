<?php
/**
 * Created by PhpStorm.
 * User: Alt
 * Date: 11.10.2017
 * Time: 20:02
 */

// Здесь подготовливаем данные для вывода в отчете
$tableOrders = 271;
$tableClients = 42;
$tablePersonal = 46;
$tablePrice = 91;

// ====================================================
// выбираем всех менеджеров производства из заказов
$managers = array();
$sqlQuery = "SELECT orders.f9010 as managers_prod FROM ".DATA_TABLE.$tableOrders." as orders WHERE orders.status=0 and orders.f6551='10. Исполнение' GROUP BY orders.f9010";
$res = sql_query($sqlQuery);
while($row = sql_fetch_array($res))
{
    $str_managers = $row["managers_prod"];
    $mn = explode('-', $str_managers);
    foreach($mn as $manager_id)
    {
        $m_id = trim($manager_id);
        if($m_id<>"")
        {
            $manager_data = data_select_array($tablePersonal, 'status=0 and f1400='.$m_id);
            $managers[$m_id] = $manager_data['f483'];
        }
    }
}

// ====================================================
// заказы
$orders = array();
foreach($managers as $man_key => $man_val)
{
    $i = 0;
    $sqlQuery = "SELECT 
                orders.f4431 as title
                ,orders.f4461 as sum
                ,clients.f435 as client
                ,price.f1158 as service
                ,orders.f6551 as status_ord
                ,orders.f10240 as status_prod
              FROM 
                ".DATA_TABLE.$tableOrders." as orders 
                  LEFT JOIN ".DATA_TABLE.$tableClients." as clients on clients.id = orders.f4441
                  LEFT JOIN ".DATA_TABLE.$tablePrice." as price on price.id = orders.f4451
              WHERE 
                orders.status=0 
                and orders.f6551 in ('10. Исполнение', '20. Предоплата')
                and orders.f9010 like '%-".$man_key."-%'
              ORDER BY
                price.f1158, orders.f6551, orders.f10240 
              ";
    $res = sql_query($sqlQuery);
    while($row = sql_fetch_array($res))
    {
        $manager = $man_val;
        $client = $row['client'];
        $title = $row['title'];
        $service = $row['service'];
        $sum = $row['sum'];
        $status_ord = $row['status_ord'];
        $status_prod = $row['status_prod'];

        $orders[$manager][$i]['client'] = $client;
        $orders[$manager][$i]['title'] = $title;
        $orders[$manager][$i]['sum'] = $sum;
        $orders[$manager][$i]['service'] = $service;
        $orders[$manager][$i]['status_ord'] = $status_ord;
        $orders[$manager][$i]['status_prod'] = $status_prod;

        $i++;
    }
}

// Переносим переменные в отображение
$smarty->assign("managers", $managers);
$smarty->assign("orders", $orders);