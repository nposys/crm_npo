{*<script type="text/javascript" src="add/js/reports_scripts.js"></script>*}
<link rel="stylesheet" type="text/css" href="add/styles/report_salary_data_new.css">

{if $submitted_manager_id>0}
{literal}
    <script type="text/javascript">
        $(document).ready(function () {
            // Handler for .ready() called.
            $('html, body').animate({
                scrollTop: $('#manager_{/literal}{$submitted_manager_id}{literal}').offset().top
            }, 'fast');
        });
    </script>
{/literal}
{/if}

<!-- ======================================================== -->
<form name="actionCalculateSalary" method="post" enctype="multipart/form-data">

<br>
    <!-- ################# OPTIONS ################# -->
    <div class="top left" id="page_head">
    <table>
        <tr>
            <td colspan="2">
                    <span class="input_element">
                      Период:
                        <select name="dt_period" id="dt_period">
                          {foreach from=$dates key=i item=date }
                              <option value='{$date}' {if $dt_period==$date} selected {/if}>
                                  {$date}
                              </option>
                          {/foreach}
                        </select>
                    </span>
            </td>
        </tr>
        {if $admin_access}
            <tr>
                <td colspan="2">
                    <input type="checkbox"
                           name="show_rt"
                            {if $show_rt== 'on'} checked {/if}>
                    Показывать суммы наших компаний
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="checkbox"
                           name="override_manual_with_auto"
                           id="override_manual_with_auto"
                            {if $override_manual_with_auto== 'on'} checked {/if}>
                    Заменить премии на автовычисления
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="checkbox"
                           name="override_totals_manual_with_auto"
                           id="override_totals_manual_with_auto"
                            {if $override_totals_manual_with_auto== 'on'} checked {/if}>
                    Заменить общие премии на автовычисления
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="checkbox"
                           name="override_manual_with_auto_common"
                           id="override_manual_with_auto_common"
                            {if $override_manual_with_auto_common== 'on'} checked {/if}>
                    Заменить оклады и отпускные
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="checkbox"
                           name="future_payment"
                           id="future_payment"
                            {if $future_payment=='on'} checked {/if}>
                    С учетом будущих платежей
                </td>
            </tr>
            <tr>
                <td colspan="2">
                        <span class="input_element">
                            Менеджер:
                        <select name="manager">
                            <option value=''>Все</option>
                            {foreach from=$managers key=manager_id item=manager_name }
                                <option value='{$manager_id}' {if $submitted_manager_id==$manager_id} selected {/if}>{$manager_name}</option>
                            {/foreach}
                        </select>
                        </span>
                </td>
            </tr>
        {/if}
        <tr>
            <td colspan="2">&nbsp;&nbsp;&nbsp;
                <input type="submit"
                       value="Обновить"
                       class="no_print"
                       onclick="document.getElementById('xsl_ex').value='0'; document.getElementById('report_form').submit(); return false"/>
            </td>
        </tr>
    </table>
</div>

    <!-- ################# PIVOT DATA ################# -->
    {if $show_totals==1 || true}
    <div id="pivot_data">
        <div class="title totals">
            СВОДНЫЕ ДАННЫЕ
        </div>
        <div id="pivot_data_table">
            <table class="mainTable table-bordered">
                <tr>
                    <th class="sticky_th_0">Менеджер</th>
                    <th class="sticky_th_0">Работает</th>
                    <th class="sticky_th_0">Сумма всего</th>
                    <th class="sticky_th_0">План обычные</th>
                    <th class="sticky_th_0">План комплекс</th>
                    <th class="sticky_th_0">Сумма обычные</th>
                    <th class="sticky_th_0">Сумма комплекс</th>
                    <th class="sticky_th_0">Премия всего</th>
                    <th class="sticky_th_0">Премия обычные</th>
                    <th class="sticky_th_0">Премия комплекс</th>
                    <th class="sticky_th_0">Оклад</th>
                    <th class="sticky_th_0">Отпускные</th>
                    <th class="sticky_th_0 nowrap">
                        Зарплата
                        {if $paid_access}
                            <a style="font-weight: bold;"
                               title="Свернуть/Развернуть"
                               onclick="collapsePaidTable();">[+]
                            </a>
                        {/if}
                    </th>
                    {if $paid_access}
                        <th class="paid-add sticky_th_0" style="display: none;">
                            Дата выплаты
                            <div style="position: relative;white-space: nowrap">
                                <input
                                        style="width: 80px;"
                                        type="text"
                                        size="10"
                                        class="main-paid-date form-control datepicker"
                                        onchange="setPaidDate(this.value);">
                            </div>
                        </th>
                        <th class="paid-add sticky_th_0" style="display: none;">
                            Тип
                            <select class="paid-add form-control main-paid-type" onchange="setPaidType(this.value);"
                                    style="width: 69px;">
                                <option></option>
                                <option value="Аванс">Аванс</option>
                                <option value="Зарплата">Зарплата</option>
                                <option value="Отпускные">Отпускные</option>
                                <option value="Прочее">Прочее</option>
                            </select></th>
                        <th class="paid-add sticky_th_0" style="display: none;">
                            Сумма
                            <input style="width: 69px;" class="form-control main-paid-sum" type="text"
                                   oninput="validatePaidSum(this); setPaidSum(this.value);">
                        </th>
                        <th class="paid-add sticky_th_0" style="display: none;">Примечание</th>
                    {/if}
                </tr>

                <tr class='username'>
                    <td class="left_big">ИТОГИ</td>
                    <td class="left_big">&nbsp;</td>
                    <td class="right_big">{$totals.totals.sum.total}</td>
                    <td class='right_small'>{$totals.totals.sum.typical_plan}</td>
                    <td class='right_small'>{$totals.totals.sum.complex_plan}</td>
                    <td class='right_big'>{$totals.totals.sum.typical}</td>
                    <td class='right_big'>{$totals.totals.sum.complex}</td>
                    <td class="right_big">{$totals.totals.benefit.total}</td>
                    <td class='right'>{$totals.totals.benefit.typical}</td>
                    <td class='right'>{$totals.totals.benefit.complex}</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td class="right_big">{$totals.totals.salary.total}</td>
                    {if $paid_access}
                        <td class="paid-add sticky_th_0" style="display: none;"></td>
                        <td class="paid-add sticky_th_0" style="display: none;"></td>
                        <td class="paid-add sticky_th_0" style="display: none;"></td>
                        <td class="paid-add sticky_th_0" style="display: none;"></td>
                    {/if}
                </tr>

                {foreach from=$new_data key=department item=positions }
                    {if $department=="totals"}
                        {continue}
                    {/if}
                    <tr class="lightgrey">
                        <td colspan="13">
                            <h2>{$department}</h2>
                        </td>
                        {if $paid_access}
                            <td class="paid-add sticky_th_0" style="display: none;"></td>
                            <td class="paid-add sticky_th_0" style="display: none;"></td>
                            <td class="paid-add sticky_th_0" style="display: none;"></td>
                            <td class="paid-add sticky_th_0" style="display: none;"></td>
                        {/if}
                    </tr>
                    {foreach from=$positions key=position item=managers}
                        {if $position=="totals"}
                            {continue}
                        {/if}
                        <tr>
                            <td class='left_big' colspan='3'>{$position}</td>
                            <td class='right_small'>{$positions.totals.typical_plan.total}</td>
                            <td class='right_small'>{$positions.totals.complex_plan.total}</td>
                            <td class='right_big'>{$positions.totals.typical.total}
                                {if $position=="Менеджер по работе с клиентами"}
                                    <div>{$positions.totals.typical.plan_percent}%</div>
                                {/if}
                            </td>
                            <td class='right_big'>{$positions.totals.complex.total}</td>
                            <td colspan='6'>&nbsp;</td>
                            {if $paid_access}
                                <td class="paid-add sticky_th_0" style="display: none;"></td>
                                <td class="paid-add sticky_th_0" style="display: none;"></td>
                                <td class="paid-add sticky_th_0" style="display: none;"></td>
                                <td class="paid-add sticky_th_0" style="display: none;"></td>
                            {/if}
                        </tr>
                        {foreach from=$managers key=manager_name item=$manager_data}
                            {if $manager_name=="totals"}
                                {continue}
                            {/if}
                            <tr
                                    {if $manager_data.manager['Работает сейчас']=="Нет"} class='lightgrey' {/if}>
                                <td class='nowrap'>
                                    <a href="#{if $manager_name==""}[Без менеджера]{else}{$manager_name}{/if}">{if
                                        $manager_name==""}[Без менеджера]{else}{$manager_name}{/if}</a>
                                    <input type="hidden" name="paid-manager[]" value="{$manager_data.manager["ID"]}">
                                </td>
                                <td class='nowrap'>{$manager_data.manager['Работает сейчас']}</td>
                                <td class="right_big">{$manager_data.salary_report.sum_result}</td>
                                <td class='right_small'>{$manager_data.salary_report.service_groups['Обычные'].total.plan_sum}</td>
                                <td class='right_small'>{$manager_data.salary_report.service_groups['Комплексные'].total.plan_sum}</td>

                                <td class='right nowrap'>
                                    {if $position=="Менеджер по работе с клиентами" || $position=="Начальник отдела по сопровождению клиентов"}
                                        <a href="#{if $manager_name==""}[Без менеджера]{else}{$manager_name}{/if}_main">{$manager_data.salary_report.service_groups['Обычные'].total.sum}
                                        </a>
                                        <div>{$manager_data.salary_report.service_groups['Обычные'].total.plan_percent}
                                            %
                                        </div>
                                    {else}
                                        <div>
                                            <span class='div_nowrap div_nopadding div_smallfont div_bold'>П: {$manager_data.sum['Обычные'].new}</span>
                                            <span class='div_nowrap div_nopadding div_smallfont'>В: {$manager_data.sum['Обычные'].old}</span>
                                        </div>
                                    {/if}
                                </td>
                                <td class='right nowrap'>
                                    {if $position=="Менеджер по работе с клиентами" || $position=="Начальник отдела по сопровождению клиентов"}
                                        <a href="#{if $manager_name==""}[Без менеджера]{else}{$manager_name}{/if}_cmplx">
                                            {$manager_data.salary_report.service_groups['Комплексные'].total.sum}
                                        </a>
                                    {else}
                                        <span class='div_nowrap div_nopadding div_smallfont div_nomargin div_bold'>П: {$manager_data.sum['Комплексные'].new}</span>
                                        <span class='div_nowrap div_nopadding div_smallfont div_nomargin'>В: {$manager_data.sum['Комплексные'].old}</span>
                                    {/if}
                                </td>

                                <td class="right_big">{$manager_data.salary_report.benefit_result}</td>

                                <td class='right nowrap'>
                                    {if $position=="Менеджер по работе с клиентами" || $position=="Начальник отдела по сопровождению клиентов"}
                                        {$manager_data.salary_report.service_groups['Обычные'].total.benefit_final}
                                    {else}
                                        <span class='div_nowrap div_nopadding div_smallfont div_bold'>П: {$manager_data.benefit['Обычные'].new}</span>
                                        <span class='div_nowrap div_nopadding div_smallfont'>В: {$manager_data.benefit['Обычные'].old}</span>
                                    {/if}
                                </td>
                                <td class='right nowrap'>
                                    {if $position=="Менеджер по работе с клиентами"}
                                        {$manager_data.salary_report.service_groups['Комплексные'].total.benefit_final}
                                    {else}
                                        <span class='div_nowrap div_nopadding div_smallfont div_bold'>П: {$manager_data.benefit['Комплексные'].new}</span>
                                        <span class='div_nowrap div_nopadding div_smallfont'>В: {$manager_data.benefit['Комплексные'].old}</span>
                                    {/if}
                                </td>

                                <td class='right_big'>{$manager_data.salary.pivot.auto.base_salary_total}</td>
                                <td class="right_big">{$manager_data.salary.pivot.holiday.auto.benefit_holiday}</td>
                                <td class='right_big'>{$manager_data.salary.pivot.auto.salary_total}</td>
                                {if $paid_access}
                                    <td class="paid-add" style="display: none;">
                                        <div style="position: relative;">
                                            <input name="paid-date[]" style="width: 80px;" type="text" size="10"
                                                   class="form-control datepicker paid-date">
                                        </div>
                                    </td>
                                    <td class="paid-add" style="display: none;">
                                        <select name="paid-type[]" class="form-control paid-type" style="width: 69px;">
                                            <option></option>
                                            <option value="Аванс">Аванс</option>
                                            <option value="Зарплата">Зарплата</option>
                                            <option value="Отпускные">Отпускные</option>
                                            <option value="Прочее">Прочее</option>
                                        </select>
                                    </td>
                                    <td class="paid-add" style="display: none;">
                                        <input name="paid-sum[]" style="width: 69px;" class="form-control paid-sum" type="text"
                                               oninput="validatePaidSum(this); setResultSum();">
                                    </td>
                                    <td class="paid-add" style="display: none;">
                                        <textarea name="paid-com[]" class="form-control" style="min-width: 90px;width: 90px;min-height: 50px;"></textarea>
                                    </td>
                                {/if}
                            </tr>
                        {/foreach}
                        <tr style="height: 5px;">
                            <td style="height: 5px; font-size: 5px;" colspan='13'>&nbsp;</td>
                            {if $paid_access}
                                <td class="paid-add sticky_th_0" style="display: none;"></td>
                                <td class="paid-add sticky_th_0" style="display: none;"></td>
                                <td class="paid-add sticky_th_0" style="display: none;"></td>
                                <td class="paid-add sticky_th_0" style="display: none;"></td>
                            {/if}
                        </tr>
                    {/foreach}
                {/foreach}
                <tr>
                    {if $paid_access}
                        <td colspan="16"></td>
                        <td class="paid-add" style="display: none;text-align: center;vertical-align: middle;" >
                            <input type="submit" class="btn btn-primary btn-md active " name="paid_save" value="Сохранить">
                        </td>
                    {/if}
                </tr>
            </table>
        </div>
    </div>
    {/if}

    <!-- ################# PERSONAL DATA ################# -->
    <div id="personal_data">
        <div class="title title_1">
            ИНДИВИДУАЛЬНЫЕ РАСЧЕТЫ
        </div>
        <div id="personal_data_tables">
            {foreach from=$new_data key=department item=positions }
                {if $department=="totals"}
                    {continue}
                {/if}
                <div class="title title_2">
                    <a href="#{$department}">[+]</a> {$department}
                </div>
                {foreach from=$positions key=position item=managers}
                    {if $position=="totals"}
                        {continue}
                    {/if}
                    <div class="position_container div_nomargin">
                        <div class="title sticky">
                            {$position}
                        </div>
                        {foreach from=$managers key=manager_name item=manager_data}
                        {if $manager_name=="totals"}{continue}{/if}
                        {$manager_id = $manager_data.manager.ID}
                        <div class="div_nomargin">
                            <div class='username name_sticky title' id="{$manager_name}">
                                {if $manager_name!=''}{$manager_name}{else}[Без менеджера]{/if} <a href="#page_head">Начало</a>
                            </div>
                            {*ОБЩИЙ ОТЧЁТ О ЗАРПЛАТЕ И ОТПУСКНЫХ*}
                            <table class="table table-bordered" style="width:750px;">
                                {foreach from=$manager_data.salary.pivot_main key=key_data item=item_data }
                                    {if $item_data.display == 1}
                                        <tr>
                                            <td style="width=160px">
                                                {$item_data.title}
                                                {if is_array($item_data.value) and ($item_data.value.comment_auto != '' or $item_data.value.comment != '')}
                                                    <a id="a_comment_{$manager_id}_{$key_data}"
                                                       href="#comment_{$manager_id}_{$key_data}"
                                                       onclick="collapseDiv({$manager_id}, '{$key_data}')" data-toggle="collapse"
                                                       style="float: right; position: relative;">
                                                        [+]</a>
                                                    <a id="a_comment_auto_{$manager_id}_{$key_data}"
                                                       href="#comment_auto_{$manager_id}_{$key_data}" data-toggle="collapse"
                                                       style="display: none;"></a>
                                                {/if}
                                            </td>
                                            {if !is_array($item_data.value)}
                                                <td colspan="2" {if $item_data.bold == 1} style="font-weight: bold;"{/if}>
                                                    {$item_data.value}
                                                </td>
                                            {else}
                                                <td{if $item_data.bold == 1} style="font-weight: bold;"{/if}>
                                                    {$item_data.value.auto}
                                                    {if $item_data.value.comment_auto != ''}
                                                        <div id="comment_auto_{$manager_id}_{$key_data}" class="collapse"
                                                             style="width: 100%; font-style: italic; font-weight: normal">
                                                            {$item_data.value.comment_auto}
                                                        </div>
                                                    {/if}
                                                </td>
                                                <td style="{if $item_data.bold == 1} font-weight: bold;{/if} width: 260px;  position: relative;">
                                                    {$item_data.value.manual}
                                                    {if $item_data.value.comment != ''}
                                                        <div id="comment_{$manager_id}_{$key_data}" class="collapse"
                                                             style="width: 100%; font-style: italic; font-weight: normal">
                                                            {$item_data.value.comment}
                                                        </div>
                                                    {/if}
                                                </td>
                                            {/if}
                                        </tr>
                                    {/if}
                                {/foreach}
                            </table>
                            <br>
                            <table class="table table-bordered" style="width: 750px">
                                <tr>
                                    <td class='left_big font20' style='width: 160px'>Оклад</td>
                                    <td style="vertical-align: top;">
                                        {$manager_data.salary.pivot.base.auto.base_salary_result} /&nbsp;<input
                                                type="text"
                                                id="base-sum-{$manager_id}_0_0_{$manager_data.salary.pivot.base.manual.calculation_id}"
                                                name="base-sum-{$manager_id}_0_0_{$manager_data.salary.pivot.base.manual.calculation_id}"
                                                size="7"
                                                value="{$manager_data.salary.pivot.base.manual.base_salary_result}"
                                                {if !$admin_access} disabled readonly{/if}>

                                        <input type="hidden"
                                               id="base-calculation-{$manager_id}_0_0_{$manager_data.salary.pivot.base.manual.calculation_id}"
                                               name="base-calculation-{$manager_id}_0_0_{$manager_data.salary.pivot.base.manual.calculation_id}"
                                               value="{$manager_data.salary.pivot.base.manual.calculation_id}">
                                    </td>
                                    <td colspan="2" style="vertical-align: top;">
                                        <textarea
                                                rows="5" cols="30"
                                                id="base-comment-{$manager_id}_0_0_{$manager_data.salary.pivot.base.manual.calculation_id}"
                                                name="base-comment-{$manager_id}_0_0_{$manager_data.salary.pivot.base.manual.calculation_id}"
                                                {if !$admin_access} disabled="disabled" {/if}
                                        >{$manager_data.salary.pivot.base.manual.base_salary_comment}</textarea>
                                    </td>
                                    {if $admin_access}
                                        <td align="right">
                                            <button class="btn btn-primary btn-md active" name="data_save" value="base-{$manager_id}">
                                                Сохранить
                                            </button>
                                        </td>
                                    {/if}
                                </tr>
                                <tr>
                                    <td class='left_big font20' style="width: 160px">Отпускные</td>
                                    <td style="vertical-align: top;">
                                        {$manager_data.salary.pivot.holiday.auto.benefit_holiday} /
                                        <input type="text"
                                               id="holiday-sum-{$manager_id}_0_0_{$manager_data.salary.pivot.holiday.manual.calculation_id}"
                                               name="holiday-sum-{$manager_id}_0_0_{$manager_data.salary.pivot.holiday.manual.calculation_id}"
                                               size="7"
                                               value="{$manager_data.salary.pivot.holiday.manual.benefit_holiday}"
                                                {if !$admin_access} disabled readonly{/if}>

                                        <input type="hidden"
                                               id="holiday-calculation-{$manager_id}_0_0_{$manager_data.salary.pivot.holiday.manual.calculation_id}"
                                               name="holiday-calculation-{$manager_id}_0_0_{$manager_data.salary.pivot.holiday.manual.calculation_id}"
                                               value="{$manager_data.salary.pivot.holiday.manual.calculation_id}"
                                        >
                                    </td>
                                    <td colspan="2" style="vertical-align: top;">
                                        <textarea
                                                rows="5" cols="30"
                                                id="holiday-comment-{$manager_id}_0_{$manager_data.salary.pivot.holiday.manual.calculation_id}"
                                                name="holiday-comment-{$manager_id}_0_{$manager_data.salary.pivot.holiday.manual.calculation_id}"
                                                {if !$admin_access} disabled="disabled" {/if}
                                        >{$manager_data.salary.pivot.holiday.manual.benefit_holiday_comment}</textarea>
                                    </td>
                                    {if $admin_access}
                                        <td>
                                            <button class="btn btn-primary btn-md active" name="data_save"
                                                    value="holiday-{$manager_id}">
                                                Сохранить
                                            </button>
                                        </td>
                                    {/if}
                                </tr>
                            </table>
                            <br>

                            {*ОТЧЁТ ДЛЯ МЕНЕДЖЕРОВ КЛИЕНТСКОГО ОТДЕЛА*}
                            {if $position=="Менеджер по работе с клиентами"}

                                {if (key_exists('salary_report', $manager_data))}
                                    <table style="border-width: 1px" class="mainTable table-bordered">
                                        <tr>
                                            <th class="sticky_th_2">Заказ</th>
                                            <th class="sticky_th_2">Клиент</th>
                                            <th class="sticky_th_2">Услуга</th>
                                            <th class="sticky_th_2">Описание</th>
                                            <th class="sticky_th_2">Дата заказ</th>
                                            <th class="sticky_th_2">Дата счет</th>
                                            <th class="sticky_th_2">Дата поступл.</th>
                                            <th class="sticky_th_2">Разн.</th>
                                            <th class="sticky_th_2">Сумма</th>
                                            <th class="sticky_th_2">Нараст. итог</th>
                                            <th class="sticky_th_2">Ставка премии</th>
                                            <th class="sticky_th_2">Коэф. срок</th>
                                            <th class="sticky_th_2">Премия</th>
                                            <th class="sticky_th_2" colspan="3">&nbsp;</th>
                                        </tr>
                                        {foreach from=$manager_data.salary_report.service_groups key=ordertype_group item=manager_item }
                                            {if $manager_item.total.sum>0 }
                                                {if $ordertype_group == 'Обычные'}
                                                    {$order_type = 'typical'}
                                                {else}
                                                    {$order_type = 'complex'}
                                                {/if}
                                                <tr class='grey'>
                                                    <td class='left_big' colspan='8'
                                                        id="{if $manager_name!=''}{$manager_name}{else}[Без менеджера]{/if}_{if $ordertype_group=='Комплексные'}cmplx{else}main{/if}">
                                                        Услуги: {$ordertype_group} <a href="#page_head">Начало</a></td>
                                                    <td class='right_big'>{$manager_item.total.sum}</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td class='right_big'>{$manager_item.total.probably_benefit_final}</td>
                                                    <td colspan="3">&nbsp;</td>
                                                </tr>
                                                <tr class='lightgrey'>
                                                    <td class='left_big' colspan='8'>План</td>
                                                    <td class='right_big'>{$manager_item.total.plan_sum}</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td colspan="3">&nbsp;</td>
                                                </tr>
                                                <tr class='lightgrey'>
                                                    <td class='left_big' colspan='8'>Исполнение плана</td>
                                                    <td class='right_big'>{$manager_item.total.plan_percent}%</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td colspan="3">&nbsp;</td>
                                                </tr>
                                                <tr class='lightgrey'>
                                                    <td class='left_big' colspan='8'>Коэффициент премии за план / Премия итоговая</td>
                                                    <td class='right_big'>
                                                        <input type="hidden"
                                                               class="autoupdate_value" value="{$manager_item.auto.total.plan_coef}">
                                                        {$manager_item.auto.total.plan_coef} /<br>
                                                        <input id="secondary_{$order_type}-plan_coef-{$manager_id}_all_all"
                                                               name="secondary_{$order_type}-plan_coef-{$manager_id}_all_all" {* Первый all обозначает что коэф применяется для всех заказов, а второй - для всех поступлений*}
                                                               type="text"
                                                               size="7"
                                                               value="{$manager_item.manual.total.plan_coef}"
                                                                {if !$admin_access} disabled readonly{/if}>

                                                    </td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td class='right_big_green'>{$manager_item.total.benefit_final}</td>
                                                    <td colspan="3">&nbsp;</td>
                                                </tr>
                                                {foreach from=$manager_item.orders key=date_payment item=orders }
                                                    {foreach from=$orders key=order_name item=order_item }
                                                        <tr>
                                                            <td>
                                                                <a href="/cb/view_line2.php?table=271&line={$order_item.order_id}"
                                                                   target='_blank'>{$order_item.order_num}</a>
                                                                /
                                                                <a href="/cb/view_line2.php?table=511&line={{$order_item.income_id}}"
                                                                   target='_blank'>{$order_item.income_num}</a>
                                                            </td>
                                                            <td>{$order_item.client}
                                                                {if $order_item.client!=$order_item.source_name}<small>
                                                                    <i>&nbsp;{$order_item.source_name}
                                                                        ,&nbsp;{$order_item.source_type}</i></small>{/if}
                                                            </td>
                                                            <td>{$order_item.service}</td>
                                                            <td>{$order_item.description}</td>
                                                            <td>
                                                                {if $ordertype_group=="Обычные"}
                                                                    <b>{$order_item.date_order}</b>
                                                                {else}
                                                                    {$order_item.date_order}
                                                                {/if}
                                                            </td>
                                                            <td>
                                                                {if $ordertype_group=="Комплексные"}
                                                                    <b>{$order_item.date_invoice}</b>
                                                                {else}
                                                                    {$order_item.date_invoice}
                                                                {/if}
                                                            </td>
                                                            <td><b>{$order_item.date_payment}</b></td>
                                                            <td class='right'>{$order_item.date_diff}</td>
                                                            <td class='right' style="white-space: nowrap">
                                                                <input type="hidden"
                                                                       id="secondary_{$order_type}-calculation-{$manager_id}_{$order_item.order_id}_{$order_item.income_id}_{$order_item.manual.calculation_id}"
                                                                       name="secondary_{$order_type}-calculation-{$manager_id}_{$order_item.order_id}_{$order_item.income_id}_{$order_item.manual.calculation_id}"
                                                                       class="autoupdate_value" value="{$order_item.auto.result}">
                                                                {$order_item.auto.result} /<br>
                                                                <input id="secondary_{$order_type}-order_profit-{$manager_id}_{$order_item.order_id}_{$order_item.income_id}"
                                                                       name="secondary_{$order_type}-order_profit-{$manager_id}_{$order_item.order_id}_{$order_item.income_id}"
                                                                       type="text"
                                                                       size="7"
                                                                       value="{$order_item.manual.result}"
                                                                        {if !$admin_access} disabled readonly{/if}>
                                                                {if $ordertype_group=="Комплексные"}
                                                                    <span style="font-size: small">
                                                                <br>{$order_item.order_sum_total}<br>
                                                                <b>{$order_item.order_paid_total}</b>
                                                            </span>
                                                                {/if}
                                                                {if $order_item.order_expenses_total>0.0}
                                                                    <br>
                                                                    <br>
                                                                    <span style="font-style: italic; font-size: x-small">
                                                                Приходы: {$order_item.sum}
                                                                <br>
                                                                Расходы: -{$order_item.order_expenses_total}
                                                            </span>
                                                                {/if}
                                                            </td>
                                                            <td class='right_small'>{$order_item.cur_sum}</td>
                                                            <td class='right'>
                                                                <input type="hidden"
                                                                       id="secondary_{$order_type}-calculation-{$manager_id}_{$order_item.order_id}_{$order_item.income_id}_{$order_item.manual.calculation_id}"
                                                                       name="secondary_{$order_type}-calculation-{$manager_id}_{$order_item.order_id}_{$order_item.income_id}_{$order_item.manual.calculation_id}"
                                                                       class="autoupdate_value" value="{$order_item.auto.benefit_rate}">
                                                                {$order_item.auto.benefit_rate}&nbsp;/&nbsp;<input
                                                                        type="text"
                                                                        id="secondary_{$order_type}-rate-{$manager_id}_{$order_item.order_id}_{$order_item.income_id}"
                                                                        name="secondary_{$order_type}-rate-{$manager_id}_{$order_item.order_id}_{$order_item.income_id}"
                                                                        size="7"
                                                                        value="{$order_item.manual.benefit_rate}"
                                                                        {if !$admin_access} disabled readonly{/if}>
                                                            </td>
                                                            <td class='right'>
                                                                <input type="hidden"
                                                                       id="secondary_{$order_type}-calculation-{$manager_id}_{$order_item.order_id}_{$order_item.income_id}_{$order_item.manual.calculation_id}"
                                                                       name="secondary_{$order_type}-calculation-{$manager_id}_{$order_item.order_id}_{$order_item.income_id}_{$order_item.manual.calculation_id}"
                                                                       class="autoupdate_value"
                                                                       value="{$order_item.auto.benefit_rate_date}">
                                                                {$order_item.auto.benefit_rate_date}&nbsp;/&nbsp;<input
                                                                        type="text"
                                                                        id="secondary_{$order_type}-rate_date-{$manager_id}_{$order_item.order_id}_{$order_item.income_id}"
                                                                        name="secondary_{$order_type}-rate_date-{$manager_id}_{$order_item.order_id}_{$order_item.income_id}"
                                                                        size="7"
                                                                        value="{$order_item.manual.benefit_rate_date}"
                                                                        {if !$admin_access} disabled readonly{/if}>
                                                            </td>
                                                            <td class='right'>{$order_item.auto.benefit} /
                                                                <b>{$order_item.manual.benefit}</b>
                                                                <input type="hidden"
                                                                       name="secondary_{$order_type}-manager_benefit-{$manager_id}_{$order_item.order_id}_{$order_item.income_id}"
                                                                       value="{$order_item.manual.benefit}"></td>
                                                            <td style="vertical-align: top;">
                                                            <textarea
                                                                    rows="5" cols="30"
                                                                    id="secondary_{$order_type}-comment-{$manager_id}_{$order_item.order_id}_{$order_item.income_id}"
                                                                    name="secondary_{$order_type}-comment-{$manager_id}_{$order_item.order_id}_{$order_item.income_id}"
                                                                    {if !$admin_access} disabled="disabled" {/if}
                                                            >{$order_item.manual.benefit_comment}</textarea>
                                                            </td>
                                                            {if $admin_access}
                                                                <td style="text-align: center">
                                                                    <a class="btn btn-success btn-xs glyphicon glyphicon-refresh"
                                                                       onclick="autoupdateRow(this);"
                                                                       title="Заменить на автовычисления"></a>
                                                                </td>
                                                            {/if}
                                                        </tr>
                                                    {/foreach}
                                                {/foreach}
                                                {if $admin_access}
                                                    <tr>
                                                        <td colspan="9">&nbsp;</td>
                                                        <td colspan="6" style="text-align: center">
                                                            <button class="btn btn-primary btn-md active" name="data_save"
                                                                    value="secondary_{$order_type}-{$manager_id}">
                                                                Сохранить
                                                            </button>
                                                        </td>
                                                    </tr>
                                                {/if}
                                            {/if}
                                        {/foreach}
                                    </table>
                                {/if}

                                {*ОТЧЕТ ДЛЯ МЕНЕДЖЕРОВ ПРОДАЖ*}
                                {if $position=="Менеджер корпоративных продаж"}
                                    {if $manager_data.salary_report.sum_result > 0}
                                        <table style="border-width: 1px" class="mainTable table-bordered">
                                            <tr>
                                                <th class="sticky_th_2">Заказ</th>
                                                <th class="sticky_th_2">Клиент</th>
                                                <th class="sticky_th_2">Услуга</th>
                                                <th class="sticky_th_2">Описание</th>
                                                <th class="sticky_th_2">Дата поступл.</th>
                                                <th class="sticky_th_2">Дата перв. поступл.</th>
                                                <th class="sticky_th_2">Сумма</th>
                                                <th class="sticky_th_2">Ставка премии</th>
                                                <th class="sticky_th_2">Премия</th>
                                                <th class="sticky_th_2"></th>
                                                <th class="sticky_th_2"></th>
                                            </tr>
                                            {foreach from=$manager_data.salary_report.income_types key=incometype item=manager_item_group }
                                                {if $manager_item_group.total.sum>0}
                                                    <tr class='incometype'>
                                                        <td class='left_big' colspan='6'>Тип поступления: {$incometype}</td>
                                                        <td class='right_big'>{$manager_item_group.total.sum}</td>
                                                        <td>&nbsp;</td>
                                                        <td class='right_big'>{$manager_item_group.total.benefit}</td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    {foreach from=$manager_item_group.service_groups key=ordertype_group item=manager_item }
                                                        {if $manager_item.total.sum>0}
                                                            {if $ordertype_group == 'Обычные'}
                                                                {$order_type = 'typical'}
                                                            {else}
                                                                {$order_type = 'complex'}
                                                            {/if}
                                                            {if $incometype == 'Первичные'}
                                                                {$income_type = 'firstly'}
                                                            {else}
                                                                {$income_type = 'secondary'}
                                                            {/if}
                                                            <tr class='grey'>
                                                                <td class='left_big' colspan='6'>Услуги: {$ordertype_group}</td>
                                                                <td class='right_big'>{$manager_item.total.sum}</td>
                                                                <td>&nbsp;</td>
                                                                <td class='right_big'>{$manager_item.total.benefit}</td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                            <tr class='lightgrey'>
                                                                <td class='left_big' colspan='6'>План</td>
                                                                <td class='right_big'>{$manager_item.total.plan_sum}</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                            <tr class='lightgrey'>
                                                                <td class='left_big' colspan='6'>Исполнение плана</td>
                                                                <td class='right_big'>{$manager_item.total.plan_percent}%</td>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                            <tr class='lightgrey'>
                                                                <td class='left_big' colspan='6'>Коэффициент премии за план / Премия
                                                                    итоговая
                                                                </td>
                                                                <td class='right_big'>{$manager_item.total.auto.plan_coef} /<br>
                                                                    <input id="{$income_type}_{$order_type}-plan_coef-{$manager_id}_all_all"
                                                                           name="{$income_type}_{$order_type}-plan_coef-{$manager_id}_all_all" {* Первый all обозначает что коэф применяется для всех заказов, а второй - для всех поступлений*}
                                                                           type="text"
                                                                           size="7"
                                                                           value="{$manager_item.total.manual.plan_coef}"
                                                                            {if !$admin_access} disabled readonly{/if}></td>
                                                                <td>&nbsp;</td>
                                                                <td class='right_big_green'>{$manager_item.total.benefit_final}</td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                            {foreach from=$manager_item.orders key=date_payment item=orders }
                                                                {foreach from=$orders key=order_name item=order_item}
                                                                    <tr>
                                                                        <td>
                                                                            <a href="/cb/view_line2.php?table=271&line={$order_item.order_id}"
                                                                               target='_blank'>{$order_item.order_num}</a> / <a
                                                                                    href="/cb/view_line2.php?table=511&line={{$order_item.income_id}}"
                                                                                    target='_blank'>{$order_item.income_num}</a>
                                                                        </td>
                                                                        <td>{$order_item.client}{if $order_item.client!=$order_item.source_name}
                                                                                <br>
                                                                                <small><i>&nbsp;{$order_item.source_name}
                                                                                    ,&nbsp;{$order_item.source_type}</i></small>{/if}
                                                                        </td>
                                                                        <td {if $order_item.service=='Подготовка документации'} class='yellow'{/if}>{$order_item.service}</td>
                                                                        <td>{$order_item.description}</td>
                                                                        <td>{$order_item.date_payment}</td>
                                                                        <td class='right_small'>
                                                                            {$order_item.date_firstpayment}
                                                                        </td>
                                                                        <td class='right'>
                                                                            <input type="hidden"
                                                                                   id="{$income_type}_{$order_type}-calculation-{$manager_id}_{$order_item.order_id}_{$order_item.income_id}_{$order_item.manual.calculation_id}"
                                                                                   name="{$income_type}_{$order_type}-calculation-{$manager_id}_{$order_item.order_id}_{$order_item.income_id}_{$order_item.manual.calculation_id}"
                                                                                   class="autoupdate_value"
                                                                                   value="{$order_item.auto.sum}">
                                                                            {$order_item.auto.sum} /
                                                                            <input id="{$income_type}_{$order_type}-order_profit-{$manager_id}_{$order_item.order_id}_{$order_item.income_id}"
                                                                                   name="{$income_type}_{$order_type}-order_profit-{$manager_id}_{$order_item.order_id}_{$order_item.income_id}"
                                                                                   type="text"
                                                                                   size="7"
                                                                                   value="{$order_item.manual.result}"
                                                                                    {if !$admin_access} disabled readonly{/if}>
                                                                        </td>
                                                                        <td class='right'>
                                                                            <input type="hidden"
                                                                                   id="{$income_type}_{$order_type}-calculation-{$manager_id}_{$order_item.order_id}_{$order_item.income_id}_{$order_item.manual.calculation_id}"
                                                                                   name="{$income_type}_{$order_type}-calculation-{$manager_id}_{$order_item.order_id}_{$order_item.income_id}_{$order_item.manual.calculation_id}"
                                                                                   class="autoupdate_value"
                                                                                   value="{$order_item.auto.benefit_rate}">
                                                                            {$order_item.auto.benefit_rate} / <input
                                                                                    type="text"
                                                                                    id="{$income_type}_{$order_type}-rate-{$manager_id}_{$order_item.order_id}_{$order_item.income_id}"
                                                                                    name="{$income_type}_{$order_type}-rate-{$manager_id}_{$order_item.order_id}_{$order_item.income_id}"
                                                                                    size="7"
                                                                                    value="{$order_item.manual.benefit_rate}"
                                                                                    {if !$admin_access} disabled readonly{/if}>
                                                                        </td>
                                                                        <td class='right'>
                                                                            {$order_item.auto.benefit} /
                                                                            <b>{$order_item.manual.benefit}</b>
                                                                            <input type="hidden"
                                                                                   name="{$income_type}_{$order_type}-manager_benefit-{$manager_id}_{$order_item.order_id}_{$order_item.income_id}"
                                                                                   value="{$order_item.manual.benefit}">
                                                                        </td>
                                                                        <td style="vertical-align: top;">
                                                                    <textarea
                                                                            rows="5" cols="30"
                                                                            id="{$income_type}_{$order_type}-comment-{$manager_id}_{$order_item.order_id}_{$order_item.income_id}"
                                                                            name="{$income_type}_{$order_type}-comment-{$manager_id}_{$order_item.order_id}_{$order_item.income_id}"
                                                                            {if !$admin_access} disabled="disabled" {/if}
                                                                    >{$order_item.manual.benefit_comment}</textarea>
                                                                        </td>
                                                                        {if $admin_access}
                                                                            <td style="text-align: center">
                                                                                <a class="btn btn-success btn-xs glyphicon glyphicon-refresh"
                                                                                   onclick="autoupdateRow(this);"
                                                                                   title="Заменить на автовычисления"></a>
                                                                            </td>
                                                                        {/if}
                                                                    </tr>
                                                                {/foreach}
                                                            {/foreach}
                                                            {if $admin_access}
                                                                <tr>
                                                                    <td colspan="9">&nbsp;</td>
                                                                    <td colspan="2" style="text-align: center">
                                                                        <button class="btn btn-primary btn-md active" name="data_save"
                                                                                value="{$income_type}_{$order_type}-{$manager_id}">
                                                                            Сохранить
                                                                        </button>
                                                                    </td>
                                                                </tr>
                                                            {/if}
                                                        {/if}
                                                    {/foreach}
                                                {/if}
                                            {/foreach}
                                        </table>
                                    {/if}
                                {/if}

                            {/if}
                        <!--
                                {*ОТЧЕТ ДЛЯ МЕНЕДЖЕРОВ ПРОИЗВОДСТВА*}

                                {*                                <table style="border-width: 1px" class="mainTable">*}
                                {*                                    <tr>*}
                                {*                                        <th>Заказ</th>*}
                                {*                                        <th>Дата поступл.</th>*}
                                {*                                        <th>Клиент</th>*}
                                {*                                        <th>Описание</th>*}
                                {*                                        <th>Даты поиска</th>*}
                                {*                                        <th>Сумма</th>*}
                                {*                                        <th>Ставка премии</th>*}
                                {*                                        <th>Премия</th>*}
                                {*                                    </tr>*}
                                {*                                    <tr class='username'>*}
                                {*                                        <td class='left_big font20' colspan='6'*}
                                {*                                            id='#{$manager_name}'>{if $manager_name<>''}{$manager_name}{else}[Без*}
                                {*                                                менеджера]{/if} <a href="#page_head">Начало</a></td>*}
                                {*                                        <td class='right_big'>{$manager_item_incometypes.total.sum}</td>*}
                                {*                                        <td>&nbsp;</td>*}
                                {*                                        <td class='right_big'>{$manager_item_incometypes.total.benefit}</td>*}
                                {*                                    </tr>*}
                                {*                                    {foreach from=$manager_item_incometypes.income_types key=incometype item=manager_item_group }*}
                                {*                                        <tr class='incometype'>*}
                                {*                                            <td class='left_big' colspan='5'>{$incometype}</td>*}
                                {*                                            <td class='right_big'>{$manager_item_group.total.sum}</td>*}
                                {*                                            <td>&nbsp;</td>*}
                                {*                                            <td class='right_big'>{$manager_item_group.total.benefit}</td>*}
                                {*                                        </tr>*}
                                {*                                        {foreach from=$manager_item_group.orders key=order_name item=order_item}*}
                                {*                                            <tr>*}
                                {*                                                <td>*}
                                {*                                                    <a href="javascript:go_table('{$order_item.order_id}','заказ')">{$order_item.order_num}</a>*}
                                {*                                                    / <a*}
                                {*                                                            href="javascript:go_table('{$order_item.income_id}','поступление')">{$order_item.income_num}</a>*}
                                {*                                                </td>*}
                                {*                                                <td>{$order_item.date_payment}</td>*}
                                {*                                                <td>{$order_item.client}</td>*}
                                {*                                                <td class='wide'>{$order_item.description}</td>*}
                                {*                                                <td class='wide_wrap'>{$order_item.dates}</td>*}
                                {*                                                <td class='right'>{$order_item.sum}</td>*}
                                {*                                                <td class='right'>{$order_item.benefit_rate}</td>*}
                                {*                                                <td class='right'>{$order_item.benefit}</td>*}
                                {*                                            </tr>*}
                                {*                                        {/foreach}*}
                                {*                                    {/foreach}*}
                                {*                                </table>*}
                                -->

                    </div>
                    {/foreach}
                </div>
            {/foreach}
            {/foreach}
        </div>
    </div>

{if $admin_access}
    <button class="btn btn-primary btn-md active" name="data_save" value="all">Сохранить все начисления</button>
    {if $override_totals_manual_with_auto}
        <button class="btn btn-primary btn-md active" id="save_total" name="data_save" value="">
            Сохранить общие премии
        </button>
    {/if}
{/if}

<input type="hidden" name="form_name" value="actionCalculateSalary"/>
<input type='hidden' name='csrf' value='{$csrf}'>

</form>

<script>
    function collapsePaidTable() {
        var hidden = $('.paid-add:hidden');
        if (hidden.length > 0) {
            $('.paid-add').show();
            $('.table-name').attr('colspan', 17);
//$('.table-responsive table.main-table').css('width','1680px')
        } else {
            $('.paid-add').hide();
            $('.table-name').attr('colspan', 13);
//$('.table-responsive table.main-table').css('width','1200px')
        }
    }

    function setPaidType(value) {
        $('.paid-type').val(value);
    }

    function setPaidDate(value) {
        $('.paid-date').val(value);
    }

    function validatePaidSum(element) {

        var cursor = $(element).getCursorPosition();
        var value = element.value;
        var length = value.length;
        value = validSum(value);
        if (length < value.length) {
            cursor++;
        }
        $(element).val(value);
        $(element).setCursorPosition(cursor);
    }

    function validSum(value) {
        value = value.replace(/,/g, '.');
        value = value.replace(/[^\d\.]/g, '');

        value = value.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
        value = value.replace(/(\..*)(\.)/g, '$1');
        value = value.replace(/(\..*)(\..+)/g, '$1');
        value = value.replace(/(\....)/g, function (p1) {
            return p1.substring(0, 3);
        });
        return value;
    }

    (function ($) {
        // узнать позицию курсора
        $.fn.getCursorPosition = function () {
            var input = this.get(0);
            if (!input) return;
            if ('selectionStart' in input) {
                return input.selectionStart;
            } else if (document.selection) {
                input.focus();
                var sel = document.selection.createRange();
                var selLen = document.selection.createRange().text.length;
                sel.moveStart('character', -input.value.length);
                return sel.text.length - selLen;
            }
        }
        // установить позицию курсора
        $.fn.setCursorPosition = function (pos) {
            if ($(this).get(0).setSelectionRange) {
                $(this).get(0).setSelectionRange(pos, pos);
            } else if ($(this).get(0).createTextRange) {
                var range = $(this).get(0).createTextRange();
                range.collapse(true);
                range.moveEnd('character', pos);
                range.moveStart('character', pos);
                range.select();
            }
        }

    })(jQuery);

    function setPaidSum(value) {

        $('.paid-sum').val(value);
        setResultSum();
    }

    function setResultSum() {
        var result = 0.0;
        var sumElements = $('.paid-sum');
        var elementVal;
        for (var i = 0; sumElements.length > i; i++) {
            elementVal = $(sumElements[i]).val();
            elementVal = elementVal.replace(/[^\d\.]/g, '');
            elementVal = parseFloat(elementVal);
            if (!isNaN(elementVal)) {
                result += elementVal;
            }
        }
        result = validSum(result.toString());
        $('.paid-result-sum').html(result);
    }

    $(function () {
        $('.datepicker').datepicker({
            showOn: "button",
            showAlways: true,
            buttonImage: "images/calbtn.png",
            buttonImageOnly: true,
            buttonText: "Calendar",
            showAnim: (('\v' == 'v') ? "" : "show")
        }).val("{date("d.m.Y")}");
    });

    function autoupdateRow(element) {
        var elements = $(element).closest("tr").find("input[type='text']");
        for (var i = 0; i < elements.length; i++) {
            $(elements[i]).val($(elements[i]).siblings(".autoupdate_value").val());
        }
    }
</script>