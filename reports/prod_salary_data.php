<?php
// Входные параметры

$dt_period = $_REQUEST['dt_period'];
$prod_benefit_rate = 0.10;

// Здесь подготовливаем данные для вывода в отчете

$tableIncomes = 511;
$tableIncomeInvoiceOrders = 650;
$tableOrders = 271;
$tableOrdersWork = 751;
$tableOrdersSearch = 371;
$tableClients = 42;
$tableClientsData = 630;
$tablePersonal = 46;
$tableServiceTypes = 91;
$tableServicePrice = 580;
$tableSalesPlans = 771;
$tableWorkTypes = 761;

//$dt_period = 2;
switch($dt_period)
{
    default:
    case 1:
        $date1 =  date("Y-m-d", strtotime("first day of this month"));
        $date2 =  date("Y-m-d", strtotime("last day of this month"));
        break;
    case 2:
        $date1 =  date("Y-m-d", strtotime("first day of this month -1 month"));
        $date2 =  date("Y-m-d", strtotime("last day of this month -1 month"));
        break;
}

$benefit_rates = array(
    1 => 0.1, // эцп
    2 => 0.1, // настройка рм
    3 => 0.1, // аккредитация
    4 => 0.1, // поиск
    5 => 0.05, // заявки
    6 => 0.1,
    7 => 0.0, // подготовка документации
    8 => 0.1,
    9 => 0.1,
    11 => 0.1,
    12 => 0.1
);


// ------------------------------------------------------------
// получаем данные о сотрудниках производства работавших над заказами
$sqlQuery = "SELECT 
                tpers.f483 as ManagerName
                ,tpers.f1400 as ManagerUser
               FROM 
                 ".DATA_TABLE.$tableOrdersWork." as tow
                 LEFT JOIN ".DATA_TABLE.$tablePersonal." as tpers ON tpers.f1400 = tow.f12661 AND tpers.status = 0 AND tow.f12661 <> '' 
               WHERE 
                 DATE(tow.f12641) BETWEEN '".$date1."' AND '".$date2."'
                 AND tow.status = 0 AND tpers.f484 = 2
                 ".$managerFilter."
               GROUP BY 
                  tpers.f483
               ORDER BY 
                 tpers.f484
                ";
$res = sql_query($sqlQuery);
$ord_managers = array();
while($row = sql_fetch_array($res)) $ord_managers[$row['ManagerUser']] = $row['ManagerName'];

// выбираем все типы услуг
$res = data_select($tableServiceTypes, "status=0");
$ord_types = array();
while($row = sql_fetch_assoc($res)) $ord_types[$row['id']] = $row['f1158'];

$data = array();
foreach($ord_managers as $user=>$manager)
{
    $benefit_total = 0.0;
    $order_count_total = 0;
    $works_count_total = 0;

    foreach($ord_types as $ord_type_id=>$ord_type)
    {
        $sqlQuery = "SELECT 
                tow.id as WorkId
                ,tord.id as OrderId
                ,tord.f7071 as OrderNum
                ,tord.f6591 as OrderDate
                ,tord.f4461 as OrderSum
                ,tst.id as OrderTypeId
                ,tst.f1158 as OrderType
                ,tsp.f8510 as OrderTypePrice
                ,tord.f4431 as OrderDescription
                ,tcmp.f435 as OrderClient
                ,tord.f6551 as StatusOrder
                ,tord.f10240 as StatuProduction       
                    
                ,tow.f12981 as DateStPlan
                ,tow.f12991 as DateFnPlan
                ,tow.f12641 as DateStFact
                ,tow.f12651 as DateFnFact

                ,twt.f12771 as WorkType
                ,tow.f12671 as WorkDescription
               
               FROM 
                 ".DATA_TABLE.$tableOrdersWork." as tow
                 LEFT JOIN ".DATA_TABLE.$tableWorkTypes." as twt ON twt.id = tow.f12861 AND twt.status = 0
                 LEFT JOIN ".DATA_TABLE.$tableOrders." as tord ON tord.id = tow.f12601 AND tord.status = 0
                 LEFT JOIN ".DATA_TABLE.$tableServiceTypes." as tst ON tst.id = tord.f4451 AND tst.status = 0
                 LEFT JOIN ".DATA_TABLE.$tableServicePrice." as tsp ON tsp.id = tord.f13071 AND tsp.status = 0
                  LEFT JOIN ".DATA_TABLE.$tableClients." as tcmp ON tcmp.id = tord.f4441 AND tcmp.status = 0
              WHERE 
                 DATE(tow.f12641) BETWEEN '".$date1."' AND '".$date2."'
                 AND tow.status = 0 
                 AND tow.f12631 = '30. Завершено'
                 AND tow.f12661 = ".$user."
                 AND tord.f4451 = ".$ord_type_id."
              ORDER BY
                tord.id
                ";
        $res = sql_query($sqlQuery);
        $num_rows = sql_num_rows($res);
        //echo "$manager $ord_type num_rows:".$num_rows."<br>";

        $service_benefit = 0.0;
        $orders_count = 0;
        $works_count = 0;

        while($row = sql_fetch_array($res))
        {
            $ord_id = $row['OrderId'];
            $work_id = $row['WorkId'];

            if($data[$manager]['order_types'][$ord_type]['orders'][$ord_id]['data']['num'] == "")
            {
                $data[$manager]['order_types'][$ord_type]['orders'][$ord_id]['data']['num'] = $row['OrderNum'];
                $data[$manager]['order_types'][$ord_type]['orders'][$ord_id]['data']['date'] = date("Y-m-d", strtotime($row['OrderDate']));
                $data[$manager]['order_types'][$ord_type]['orders'][$ord_id]['data']['sum'] = $row['OrderSum'];
                $data[$manager]['order_types'][$ord_type]['orders'][$ord_id]['data']['type_price'] = $row['OrderTypePrice'];
                $data[$manager]['order_types'][$ord_type]['orders'][$ord_id]['data']['client'] = $row['OrderClient'];
                $data[$manager]['order_types'][$ord_type]['orders'][$ord_id]['data']['description'] = $row['OrderDescription'];
                $data[$manager]['order_types'][$ord_type]['orders'][$ord_id]['data']['status'] = $row['StatusOrder'];

                $benefit = $row['OrderSum'] * $benefit_rates[$row['OrderTypeId']];
                $data[$manager]['order_types'][$ord_type]['orders'][$ord_id]['data']['benefit'] = $benefit;

                $service_benefit += $benefit;
                $orders_count++;
            }

            $data[$manager]['order_types'][$ord_type]['orders'][$ord_id]['work'][$work_id]['date_st_plan'] = $row['DateStPlan'];
            $data[$manager]['order_types'][$ord_type]['orders'][$ord_id]['work'][$work_id]['date_fn_plan'] = $row['DateFnPlan'];
            $data[$manager]['order_types'][$ord_type]['orders'][$ord_id]['work'][$work_id]['date_st_fact'] = $row['DateStFact'];
            $data[$manager]['order_types'][$ord_type]['orders'][$ord_id]['work'][$work_id]['date_fn_fact'] = $row['DateFnFact'];
            $data[$manager]['order_types'][$ord_type]['orders'][$ord_id]['work'][$work_id]['work_type'] = $row['WorkType'];
            $data[$manager]['order_types'][$ord_type]['orders'][$ord_id]['work'][$work_id]['work_description'] = $row['WorkDescription'];

            $works_count++;
        }

        $data[$manager]['order_types'][$ord_type]['total']['benefit'] = $service_benefit;
        $data[$manager]['order_types'][$ord_type]['total']['orders_count'] = $orders_count;
        $data[$manager]['order_types'][$ord_type]['total']['works_count'] = $works_count;

        $benefit_total += $service_benefit;
        $order_count_total += $orders_count;
        $works_count_total += $works_count;

    } // end foreach service

    $data[$manager]['total']['benefit'] = $benefit_total;
    $data[$manager]['total']['orders_count'] = $order_count_total;
    $data[$manager]['total']['works_count'] = $works_count_total;


} // end foreach manager
// print_r($data);

// ==================================================================
// Переносим переменные в отображение
$smarty->assign("data", $data);
$smarty->assign("dt_period", $dt_period);