<?php
// Здесь подготовливаем данные для вывода в отчете
$tablePersonal = 46;
$tableClients = 42;
$tableOrders = 271;
$tableOrdersSearch = 371;
$tableOrdersApp = 381;

// редирект на нужную карточку
$line_id = $_REQUEST['_line_id'];
if ($line_id) {
    switch ($_REQUEST['_type']) {
        case "Клиент":
            header("Location: " . $config["site_root"] . "/view_line2.php?table=42&filter=2041&line=" . $line_id . "&back_url=" . $base64_current_url);
            break;
        case "Заказ":
            header("Location: " . $config["site_root"] . "/view_line2.php?table=271&filter=1891&line=" . $line_id . "&back_url=" . $base64_current_url);
            break;
    }
}

$output = "";
$results = array();

$resSerchTrial = data_select($tableOrdersSearch,
    // "status=0 and f8550  in (2,56,63,65) and f6951 BETWEEN '2015-10-01' and '2018-12-31' and f9260='30. Завершено'");
    "status=0 and f8550  in (2,56,63,65) and f6951 > '2015-10-01' and f9260='30. Завершено'");
$numTrial = 0;
$numConverted = 0;
$sumConverted = 0;

// перебираем все пробные поиски
while ($rowSearchTrial = sql_fetch_array($resSerchTrial)) {
    $numTrial++;

    $idClient = $rowSearchTrial['f10190']; // клиент
    $dtSearchTrialBegin = date("Y-m-d", strtotime($rowSearchTrial['f6941'])); // дата начала поиска
    $dtSearchTrialEnd = date("Y-m-d", strtotime($rowSearchTrial['f6951'])); // дата завершения поиска
    $dtSearchTrialMonth = date("Y-m", strtotime($dtSearchTrialBegin)); //месяц начала поиска

    $rowOrderTrial = data_select_array($tableOrders, "status=0 and id=" . $rowSearchTrial['f5411']);  // связанный заказ с записью в таблице производства
    $idOrderTrial = $rowOrderTrial['f7071'];
    $rowClientTrial = data_select_array($tableClients, "status=0 and id=" . $rowSearchTrial['f10190']); // клиент по данному пробоному поиску
    $clientName = $rowClientTrial['f435'];
    $clientInn = $rowClientTrial['f1056'];
    $fromTm = $rowClientTrial['f16390'];
    $fromTmAdvert = $rowClientTrial['f16400'];

    // выбираем менеджера который привлек пробный поиск
    if ($rowSearchTrial['f9080'] > 0) {
        $rowManagerSalesTrial = data_select_array($tablePersonal, "status=0 and f1400=" . $rowSearchTrial['f9080']); //f483
        $manager = $rowManagerSalesTrial["f483"];

    } else {
        $rowManagerSalesTrial = array('f483' => "---");
        $manager = "---";
    }

    $results['months'][$dtSearchTrialMonth]['orders'][$idOrderTrial]['order_id'] = $rowOrderTrial['id'];
    $results['months'][$dtSearchTrialMonth]['orders'][$idOrderTrial]['date_begin'] = $dtSearchTrialBegin;
    $results['months'][$dtSearchTrialMonth]['orders'][$idOrderTrial]['date_end'] = $dtSearchTrialEnd;
    $results['months'][$dtSearchTrialMonth]['orders'][$idOrderTrial]['client_id'] = $rowClientTrial['id'];
    $results['months'][$dtSearchTrialMonth]['orders'][$idOrderTrial]['client'] = $clientName;
    $results['months'][$dtSearchTrialMonth]['orders'][$idOrderTrial]['inn'] = $clientInn;
    $results['months'][$dtSearchTrialMonth]['orders'][$idOrderTrial]['from_tm'] = $fromTm;
    $results['months'][$dtSearchTrialMonth]['orders'][$idOrderTrial]['from_tm_advert'] = $fromTmAdvert;
    $results['months'][$dtSearchTrialMonth]['orders'][$idOrderTrial]['have_future_event'] = $rowClientTrial['f8980'];
    $results['months'][$dtSearchTrialMonth]['orders'][$idOrderTrial]['manager'] = $manager;

    $results['months'][$dtSearchTrialMonth]['totals']['num_trials'] += 1;
    (int)$fromTm>0 ? $results['months'][$dtSearchTrialMonth]['totals']['num_from_tm'] += 1 : '';

    // ищем платные поиски по этому клиенту
    $resSerchPaid = data_select($tableOrdersSearch, "status=0 and f8550 in (1,55,62,64) and f6941>'" . $dtSearchTrialEnd . "' and f10190=" . $idClient);
    while ($rowSearchPaid = sql_fetch_array($resSerchPaid)) {
        $rowOrderPaid = data_select_array($tableOrders, "status=0 and id=" . $rowSearchPaid['f5411']);  // связанный заказ с записью в таблице производства
        $idOrderPaid = $rowOrderPaid['f7071'];
        $sumRealPaid = (float)$rowOrderPaid['f10270'];
        $rowClientPaid = data_select_array($tableClients, "status=0 and id=" . $rowSearchPaid['f10190']);

        $results['months'][$dtSearchTrialMonth]['orders'][$idOrderTrial]['converted'][$idOrderPaid]['date_begin'] = date("Y-m-d", strtotime($rowSearchPaid['f6941']));
        $results['months'][$dtSearchTrialMonth]['orders'][$idOrderTrial]['converted'][$idOrderPaid]['date_end'] = date("Y-m-d", strtotime($rowSearchPaid['f6951']));
        $results['months'][$dtSearchTrialMonth]['orders'][$idOrderTrial]['converted'][$idOrderPaid]['sum_order'] = $rowSearchPaid['f10230'];
        $results['months'][$dtSearchTrialMonth]['orders'][$idOrderTrial]['converted'][$idOrderPaid]['sum_paid'] = $sumRealPaid;
        $results['months'][$dtSearchTrialMonth]['orders'][$idOrderTrial]['converted'][$idOrderPaid]['order_id'] = $rowOrderPaid['id'];

        $results['months'][$dtSearchTrialMonth]['totals']['num_converted'] += 1;
        $results['months'][$dtSearchTrialMonth]['totals']['sum_converted'] += $sumRealPaid;
        (int)$fromTm>0 ? $results['months'][$dtSearchTrialMonth]['totals']['trials_from_tm_converted'][$idOrderTrial] = 1 : '';

        $numConverted++;
        $sumConverted += $rowSearchPaid['f10230'];
    }
}

$results['totals']['num_trials'] = $numTrial;
$results['totals']['num_converted'] = $numConverted;
$results['totals']['sum_converted'] = $sumConverted;


// Переносим переменные в отображение
$smarty->assign("results", $results);
?>