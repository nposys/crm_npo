<?php
// Здесь подготовливаем данные для вывода в отчете
$tableClients = 42;
$tablePersonal = 46;

$types = array('Потенциальный'=>array('num'=>0, 'status_field'=>'f552','manager_field'=>'f438'), 'Клиент'=>array('num'=>0, 'status_field'=>'f4921','manager_field'=>'f8720'));
$manager_fields = array(1=>'f438',6=>'f8720');

$field = sql_select_array(FIELDS_TABLE, "id=552");
$statuses_lead  = explode(PHP_EOL,$field["type_value"]);

$field = sql_select_array(FIELDS_TABLE, "id=4921");
$statuses_client = explode(PHP_EOL,$field["type_value"]);

// ============================================================
// row names
$statuses = array();
foreach($statuses_lead as $status)  $statuses['Потенциальный'][] = $status;
foreach($statuses_client as $status)  $statuses['Клиент'][] = $status;
$types['Потенциальный']['num'] = count($statuses_lead);
$types['Клиент']['num'] = count($statuses_client);

// ============================================================
$managers = array();
$sqlQuery = "SELECT personal.f1400 as id, personal.f483 as name, personal.f484 as type FROM ".DATA_TABLE.$tablePersonal." as personal WHERE personal.f7941='Да' and personal.f484 in (1,6) ORDER BY personal.f484";
$res = sql_query($sqlQuery);
$i=0;
while($row = sql_fetch_array($res)) if(trim($row["id"])<>'')
{
    $managers[$i]['id'] = $row["id"];
    $managers[$i]['type'] = $row["type"];
    $managers[$i]['name'] = $row["name"];
    $i++;
}

// ============================================================
$clients = array();
$totals = array();
foreach($managers as $manager)
{
    foreach($statuses as $type_name=>$type)
    {
        foreach($type as $index=>$status)
        {
            $clients[$manager['name']][$type_name][$status] = 0;

            $status_field = $types[$type_name]['status_field'];
            $manager_field = $manager_fields[$manager['type']]; // $types[$type_name]['manager_field'];
            $manager_id = $manager['id'];

            //$result = data_select_field($tableClients, "COUNT(id) AS count", "status=0 and f772='".trim($type_name)."' and ".$status_field."='".trim($status)."' and ".$manager_field."=".$manager_id);
            $result = data_select_field($tableClients, "COUNT(id) AS count", "status=0 and  ".$status_field."='".trim($status)."' and ".$manager_field."=".$manager_id);
            $row = sql_fetch_assoc($result);
            if($row['count']>0) $clients[$manager['name']][$type_name][$status] = $row['count'];

            $totals[$type_name][$status] += intval($row['count']);
        }
    }
}
$clients['ВСЕГО'] = $totals;

// Переносим переменные в отображение
$smarty->assign("clients", $clients);
$smarty->assign("statuses", $statuses);
$smarty->assign("types", $types);