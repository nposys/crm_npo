<?php
// отчет для отображения событий по менеджерам продаж
require_once 'add/tables.php';
require_once 'add/functions/service_functions.php';

// примерная оценка трудоемкости событий
$workloads = [
    "Звонок" => 10
    , "Письмо" => 5
    , "Встреча" => 120
    , "Создание заказа" => 15
    , "Выставление счета" => 10
    , "Получение оплаты" => 0
    , "Подача заявки" => 15
    , "Торги" => 30
];

$positions = [
    'sales_managers' => [
        1, // Менеджер корпоративных продаж / Отдел продаж / Академ
        16, // Менеджер корпоративных продаж/ Отдел корпоративных продаж / Академ
        47 //Менеджер корпоративных продаж / Отдел по работе с клиентами / Центр
    ],
    'client_managers' => [
        6,  // Менеджер по работе с клиентами / Отдел продаж / Академ
        17  // Менеджер по работе с клиентами / Отдел по работе с клиентами / Центр
    ]
];

// =============================================================================
// обрабатываем фильтры на входе
$dt_period = isset($_REQUEST['dt_period']) ? $_REQUEST['dt_period'] : '0';
if ($dt_period == '1') {
    $date1 = date("01.m.Y");
    $date2 = date("t.m.Y");
} else if ($dt_period == '2') {
    $date1 = date("01.m.Y", strtotime(date("01.m.Y") . " -1 month"));
    $date2 = date("t.m.Y", strtotime(date("01.m.Y") . " -1 month"));
} else {
    if($_REQUEST['date1']) {
        $date1 = date("d.m.Y", strtotime(form_eng_time($_REQUEST['date1'])));
    } else {
        $date1 = date("01.m.Y");
        // $date1 = date("d.m.Y", strtotime("monday this week"));
    }
    if ($_REQUEST['date2']) {
        $date2 = date("d.m.Y", strtotime(form_eng_time($_REQUEST['date2'])));
    } else {
        $date2 = date("d.m.Y", strtotime("friday this week"));
    }
}

$date1_fet = form_eng_time($date1 . ' 00:00:00');
$date2_fet = form_eng_time($date2 . ' 23:59:59');

$dt_done = isset($_REQUEST['dt_done']) ? $_REQUEST['dt_done'] : 'Да';
$dt_effective = isset($_REQUEST['dt_effective']) ? $_REQUEST['dt_effective'] : 'Все';

if ($_REQUEST['_type'] || $_REQUEST['_manager'] || $_REQUEST['_dates']) {
    reset_filters(62);
    if ($_REQUEST['_dates']) {
        set_filter(724, "period", $date1 . " 00:00:00", $date2 . " 23:59:59");
    }
    if ($_REQUEST['_manager']) {
        set_filter(727, "=", (int)($_REQUEST['_manager']));
    }
    if ($_REQUEST['_type']) {
        set_filter(773, "=", form_input($_REQUEST['_type']));
    }
    set_filter(1053, "=", "Да");
    header("Location: " . $config["site_root"] . "/fields.php?table=62");
}

/*
 1 = Администратор
 791 = Менеджер по персоналу
 811 = Начальнки отдела по работе с клиентами
*/
$allowed_user_groups = [1, 791, 811];
$isAdminUser = in_array($user['group_id'],$allowed_user_groups);
if (!$isAdminUser) {
    $manager = $user['id'];
} elseif ($_REQUEST['manager']) {
    $manager = (int)($_REQUEST['manager']);
}

// ==============================================================================================
// формируем селектбокс по выбору менеджеров
if ($isAdminUser) {
    $sel_manager = "<option value=''>Все</option>\r\n";
}

$result = sql_query(
    "SELECT 
          DISTINCT `user`.`id`, `user`.`fio` 
              FROM `" . USERS_TABLE . "` AS `user`, `" . GROUPS_TABLE . "` AS `group` 
              WHERE `user`.`arc`=0 AND `user`.`group_id`!='777'");

while ($row = sql_fetch_assoc($result)) {
    $sel_manager .= "<option value='" . $row['id'] . "'" . (($row['id'] == $manager) ? " selected" : "") . ">" . $row['fio'] . "</option>\r\n";
}

if ($manager) {
    $userCond = " and pers.f1400=" . $manager;
}

if ($date1 && $date2) {
    $dateCond = " and events.f724>='" . $date1_fet . "' and events.f724<='" . $date2_fet . "'";
}

// =============================================================================

// -----------------------------------------
// заполняем все типы клиентов
// -----------------------------------------
$field = sql_select_array(FIELDS_TABLE, "id=772");
$client_types = explode(PHP_EOL, $field["type_value"]);
foreach ($client_types as &$client_type) {
    $client_type = trim($client_type);
}

// -----------------------------------------
// заполняем все типы событий
// -----------------------------------------
$field = sql_select_array(FIELDS_TABLE, "id=773");
$event_types = explode(PHP_EOL, $field["type_value"]);
foreach ($event_types as &$event_type) {
    $event_type = trim($event_type);
}
// -----------------------------------------
// заполняем массивы с сотрудниками
// -----------------------------------------
$managers = [];
$i = 0;
$sqlPersonal = "
  SELECT 
    pers.*, 
    user.id as user_id 
  FROM 
    " . DATA_TABLE . $cb_tables['tablePersonal'] . " as pers 
        JOIN " . USERS_TABLE . " as user ON user.login=pers.f1410 
  WHERE 
    pers.status=0 
    and pers.f7941='Да' -- работает сейчас
    and pers.f24031 <> 'Да' -- фильтруем служебных пользователей
    and pers.f12021 IN (10,11) -- отделы:  10 - сопровождение клиентов, 11 - корпоративные продаж 
    -- and pers.f484 IN () -- должности
    ";

$resPersonal = sql_query($sqlPersonal);
while ($rowPersonal = sql_fetch_assoc($resPersonal)) {
    $managers[$i]['id'] = $rowPersonal['id'];
    $managers[$i]['name'] = $rowPersonal['f6631'];
    $managers[$i]['user'] = $rowPersonal['f1410'];
    $managers[$i]['user_id'] = $rowPersonal['user_id'];
    $managers[$i]['position'] = $rowPersonal['f484'];
    $managers[$i]['work_now'] = $rowPersonal['f7941'];
    $managers[$i]['work_from'] = $rowPersonal['f489'];
    $managers[$i]['work_to'] = $rowPersonal['f14051'];
    $i++;
}

// менеджеры продаж
$managers_sales = array();
foreach ($managers as $next_manager) {
    if (in_array($manager['position'], $positions['sales_managers'])) {
        $managers_sales[] = $next_manager;
    }
}
$managers_sales[] = ["id" => 0, "name" => "", "user" => "", "user_id" => "", "position" => ""];

// менеджеры по работе с клиентами
$managers_clients = [];
foreach ($managers as $next_manager) {
    if (in_array($manager['position'], $positions['client_managers'])) {
        $managers_clients[] = $next_manager;
    }
}
$managers_clients[] = ["id" => 0, "name" => "", "user" => "", "user_id" => "", "position" => ""];

// -----------------------------------------------------
// получаем данные по планам и фактам сборов
// -----------------------------------------------------
$date_month = date("Y-m-d", strtotime($date1_fet . ' first day of this month'));
$resSalesPlans = data_select($cb_tables['tableSalesPlans'], "status=0 and f13281='" . $date_month . "'");
while ($rowSalesPlans = sql_fetch_assoc($resSalesPlans)) {
    $manager_id = $rowSalesPlans['f13321'];
    $manager_user = $rowSalesPlans['f13331'];
    $sales_type = $rowSalesPlans['f15181']; // первичные, вторичные
    $service_type = $rowSalesPlans['f13801']; // обычные, комплексны
    $sum_plan = $rowSalesPlans['f13311'];
    $sum_fact = $rowSalesPlans['f13341'];
    $plan_date = $rowSalesPlans['f13281'];

    $next_manager_name = "";
    foreach ($managers as $next_manager) if ($next_manager['id'] == $manager_id) {
        $next_manager_name = $next_manager['name'];
        break;
    }
    // $line['f13351'] // percent

    $plans[$next_manager_name]['plan'] = $sum_plan;
    $plans[$next_manager_name]['fact'] = $sum_fact;

    //$plans[$next_manager_name]['plan'] = 50000;
    //$plans[$next_manager_name]['fact'] = 40000;
    $plans[$next_manager_name]['percent'] = $plans[$next_manager_name]['fact'] / $plans[$next_manager_name]['plan'];
}

// -----------------------------------------------------
// данные по событиям
// -----------------------------------------------------
$events = [];

initializeEvents($events, $date1, $date2, $managers, $client_types, $event_types);
getEvents($events, $date1, $date2);
getEvents($events, $date1, $date2, true);

// удаляем лишние пустые ряды событий
foreach ($managers as $manager){
    $manager_name = $manager['name'];
    foreach ($client_types as $client_type_tmp){
        foreach ($event_types as $event_type_tmp){
            if($events['managers'][$manager_name]['totals']['clients'][$client_type_tmp]['events'][$event_type_tmp]['total'] ==0 ){
                foreach ($events['managers'][$manager_name]['dates'] as &$date){
                    unset ($date['clients'][$client_type_tmp]['events'][$event_type_tmp]);
                }
            }
        }
    }
}

$a=1;

/*
// считаем итоговый счет по дням
$manager_position = 1;
$service_type = 'Обычные';
$graph_data1 = [];
$max = 0;
$i = 0;
foreach ($managers as $next_manager) {
    $strName = $next_manager['name'];
    $bFoundEvents1 = false;
    if (($manager == 0 || $manager == $next_manager['user_id']) && $next_manager['position'] == 1) {
        $cur_date = date("Y-m-d", strtotime($date1));
        $fin_date = date("Y-m-d", strtotime($date2 . " +0 day"));
        do {
            $score_plan = 0;
            $score_fact = 0;

            foreach ($event_types as $event_type) {
                $score_plan +=
                    $events[$next_manager['name']]['events'][$event_type][$cur_date]['plan'] * $workloads[$event_type];
                $score_fact +=
                    $events[$next_manager['name']]['events'][$event_type][$cur_date]['fact'] * $workloads[$event_type];
            }
            $graph_data1[$i][$cur_date] = 0;
            if ((int)$events[$next_manager['name']]['events']["Звонок"][$cur_date]['fact'] > 0) {
                $graph_data1[$i][$cur_date] += (int)$events[$next_manager['name']]['events']["Звонок"][$cur_date]['fact'];
                $max = $max < $graph_data1[$i][$cur_date] ? $graph_data1[$i][$cur_date] : $max;
                $bFoundEvents1 = true;
            }

            // -----------------------------------------------------------------------
            // вычисляем поступления
            // -----------------------------------------------------------------------
            $sum_income_day = 0;

            if ($manager_position == 1) {
                $manager_field = "f11251";
            }
            if ($manager_position == 6) {
                $manager_field = "f11261";
            }

            if ($manager_field <> "") {
                $pre_filter = "";
                if ($service_type == 'Обычные') $pre_filter = "not";

                $res = data_select_field($cb_tables['tableIncomeInvoiceOrders'],
                    "SUM(f10920) AS sum",
                    "status=0 and " . $manager_field . "=" . $next_manager['user_id'] .
                    " and f11271='" . $cur_date . "' and f11741 " . $pre_filter . " in (7)");
                $row = sql_fetch_assoc($res);
                $sum_income_day = $row['sum'];
            }
            $events[$next_manager['name']]['event']["Поступления"][$cur_date]['fact'] = $sum_income_day;

            $events[$next_manager['name']]['events']["СЧЕТ"][$cur_date]['plan'] = (float)($score_plan) / 60;
            $events[$next_manager['name']]['events']["СЧЕТ"][$cur_date]['fact'] = (float)($score_fact) / 60;

            $cur_date = date("Y-m-d", strtotime($cur_date . " +1 day"));
        } while ($cur_date <= $fin_date);
    }
    if ($bFoundEvents1) {
        $strSeriesName1 .= "'" . $strName . "',";
        $i++;
    }
}

$graph_settings1 = array('type_graph' => 'LineDate'
, 'title' => 'График'
, 'div_id' => 'chart1'
, 'series_names' => substr($strSeriesName1, 0, strlen($strSeriesName1) - 1)
, 'y_max' => $max
, 'x_min' => date("Y-m-d", strtotime($date1))
, 'x_max' => date("Y-m-d", strtotime($date2))
, 'zoom' => true
);
$data_gr1 = draw_graph($graph_data1, $graph_settings1);

*/

// ----------------------------------------------
// Переносим переменные в отображение
$smarty->assign("events", $events);
$smarty->assign("client_types", $client_types);
$smarty->assign("event_types", $event_types);


$smarty->assign("dates", $dates);
$smarty->assign("plans", $plans);

$smarty->assign("graph1", $data_gr1);

$smarty->assign("date1", $date1);
$smarty->assign("date2", $date2);
$smarty->assign("dt_done", $dt_done);
$smarty->assign("dt_effective", $dt_effective);
$smarty->assign("data", $data_m);
$smarty->assign("manager", $manager);
$smarty->assign("sel_manager", $sel_manager);
$smarty->assign("user_group", $user['group_id']);
$smarty->assign("admin_user", $isAdminUser);

/*
 * функция для выборки событий по типу
*/
function getEvents(&$events, $date1, $date2, $doneEvents = false, $managerId = 0)
{
    $dateFrom = date("Y-m-d 00:00:00", strtotime($date1));
    $dateTo = date("Y-m-d 23:59:59", strtotime($date2));

    if($doneEvents){
        $dateField = "f11100"; // last modify
        $doneFilter = "Да";
    }else{
        $dateField = "f724"; // plan date
        $doneFilter = "Нет";
    }

    $whereManager = "";
    if($managerId > 0){
        $whereManager = " and events.f7821 = $managerId";
    }

    // запрос выборки событий
    $sqlQuery = "
        SELECT
            DATE(events.".$dateField.") as event_date,
            pers.f6631 as manager_name,
            clients.f772 as client_type,
            events.f773 as event_type,
            events.f1053 as event_done,
            events.f4761 as event_res,
            sum(CASE WHEN DATE(events.f11100) = DATE(events.f724) THEN 1 ELSE 0 END) as date_match_num,
            sum(CASE WHEN DATE(events.f11100) = DATE(events.f724) THEN 0 ELSE 1 END) as date_not_match_num,            
            count(events.id) as events_num,
            pers.f1400 as user_id
        FROM
            cb_data62 as events
                LEFT JOIN cb_data46 as pers on pers.id = events.f7821
                LEFT JOIN cb_data42 as clients on clients.id = events.f723
        WHERE
          TRUE
          and events.f1053 = '".$doneFilter."' -- event_done, завершенные события
          and events.status = 0
          and events.".$dateField.">='".$dateFrom."' 
          and events.".$dateField."<='".$dateTo."'
          and pers.f12021 IN (10,11) -- отделы:  10 - сопровождение клиентов, 11 - корпоративные продаж
          and pers.f24031 <> 'Да' -- фильтруем служебных пользователей
          ".$whereManager."
        GROUP BY
            DATE(events.".$dateField."), -- last_modify date
            events.f773, -- event_type
            pers.id,
            pers.f1400,
            events.f1053, -- event_done
            events.f4761, -- event_effective
            clients.f772 -- client_type
        ORDER BY 
            DATE(events.".$dateField.") ASC
    ";

    $res_query = sql_query($sqlQuery);
    while ($row = sql_fetch_array($res_query)) {
        $manager_name = $row['manager_name'];
        $user_id = $row['user_id']; // todo можно убрать из запроса и считывать из массива $personal
        $client_type = $row['client_type'];
        $event_date = $row['event_date'];
        $event_type = $row['event_type'];
        $event_done = $row['event_done'];
        $event_res = $row['event_res'];
        $events_num = (int)$row['events_num'];

        $result_group = 'without_result';
        if($event_res == 'Да'){
            $result_group = 'with_result';
        };

        $done_group = 'plan_not_done';
        if($event_done == 'Да'){
            $done_group = 'plan_done';
        }

        // данные по типу и дню
        $events['managers'][$manager_name]
            ['dates'][$event_date]
            ['clients'][$client_type]
            ['events'][$event_type]
            [$done_group][$result_group]
                = $events_num;

        // итоги по менеджеру
        $events['managers'][$manager_name]['totals']
            ['clients'][$client_type]
            ['events'][$event_type]
            [$done_group][$result_group]
                += $events_num;

        $events['managers'][$manager_name]['totals']['clients'][$client_type]['total'] += 1;
        $events['managers'][$manager_name]['totals']['clients'][$client_type]['events'][$event_type]['total'] += 1;

        if(!isset($events['managers'][$manager_name]['totals']['user_id'])) {
            $events['managers'][$manager_name]['totals']['user_id'] = $user_id;
        }

        // если добавили новый тип событий в истории, то считаем его
        if($events['managers'][$manager_name]['totals']['clients'][$client_type]['events'][$event_type]['total'] == 1){
            // $events[$manager_name]['totals']['clients'][$client_type]['event_types'] += 1;
            $events['managers'][$manager_name]['totals']['clients'][$client_type]['distinct_event_types'][$event_type] = 1;
        }

        if($events['managers'][$manager_name]['totals']['clients'][$client_type]['events'][$event_type]['total'] == 1){
            // $events[$manager_name]['totals']['clients'][$client_type]['event_types'] += 1;
            $events['totals']['clients'][$client_type]['distinct_event_types'][$event_type] = 1;
        }

        /*
        $events[$manager_name]['totals'][$client_type][$event_type]['plan_done']['with_result'] = 0;
        $events[$manager_name]['totals'][$client_type][$event_type]['plan_done']['without_result'] = 0;
        $events[$manager_name]['totals'][$client_type][$event_type]['plan_not_done']['with_result'] = 0;
        $events[$manager_name]['totals'][$client_type][$event_type]['plan_not_done']['without_result'] = 0;
        */
    }
}

/*
 * Функция для заполнения массива событий дефолтными датами и цифрами, чтобы таблицы были единообразными
 */
function initializeEvents(&$events, $dateFrom, $dateTo, $managers, $clientTypes, $eventTypes)
{
    // TODO надо инициализировать до максимальной даты плановых событий, либо не учитывать плановые события дальше текущей даты
    // $dates = [];
    $fin_date = date("Y-m-d", strtotime($dateTo));

    foreach ($managers as $manager){
        if(
            ($manager['work_from'] > '0000-00-00 00:00:00' && $manager['work_from'] > $dateTo) ||
            ($manager['work_to'] > '0000-00-00 00:00:00' && $manager['work_to'] < $dateFrom) ){
            continue;
        }

        $cur_date = date("Y-m-d", strtotime($dateFrom));
        while ($cur_date <= $fin_date) {
            // $dates[$cur_date]['date'] = date("d.m.Y", strtotime($cur_date));
            foreach ($clientTypes as $clientType){
                foreach($eventTypes as $eventType){
                    $events['managers'][$manager['name']]
                        ['dates'][$cur_date]
                        ['clients'][$clientType]
                        ['events'][$eventType]
                        ['plan_done']['with_result'] = 0;
                    $events['managers'][$manager['name']]
                        ['dates'][$cur_date]
                        ['clients'][$clientType]
                        ['events'][$eventType]
                        ['plan_done']['without_result'] = 0;
                    $events['managers'][$manager['name']]
                        ['dates'][$cur_date]
                        ['clients'][$clientType]
                        ['events'][$eventType]
                        ['plan_not_done']['with_result'] = 0;
                    $events['managers'][$manager['name']]
                        ['dates'][$cur_date]
                        ['clients'][$clientType]
                        ['events'][$eventType]
                        ['plan_not_done']['without_result'] = 0;

                    $events['managers'][$manager['name']]['totals']['clients'][$clientType]['events'][$eventType]['total'] = 0;
                    $events['managers'][$manager['name']]['totals']['clients'][$clientType]['events'][$eventType]['plan_done']['with_result'] = 0;
                    $events['managers'][$manager['name']]['totals']['clients'][$clientType]['events'][$eventType]['plan_done']['without_result'] = 0;
                    $events['managers'][$manager['name']]['totals']['clients'][$clientType]['events'][$eventType]['plan_not_done']['with_result'] = 0;
                    $events['managers'][$manager['name']]['totals']['clients'][$clientType]['events'][$eventType]['plan_not_done']['without_result'] = 0;
                }
            }
            $cur_date = date("Y-m-d", strtotime($cur_date . " +1 day"));
        }
    }
}
