<?php
// Входные параметры

$dt_period = $_REQUEST['dt_period'];
$prod_benefit_rate = 0.10;

// Здесь подготовливаем данные для вывода в отчете

$tableIncomes = 511;
$tableIncomeInvoiceOrders = 650;
$tableOrders = 271;
$tableOrdersWork = 751;
$tableOrdersSearch = 371;
$tableClients = 42;
$tableClientsData = 630;
$tablePersonal = 46;
$tableServiceTypes = 91;
$tableServicePrice = 580;
$tableSalesPlans = 771;
$tableWorkTypes = 761;

// ----------------------------------------------------
// проверяем права и устанавливаем фильтр по менеджерам
$managerFilter = "";
$bShowSales = false;
$bShowClients = false;
$bShowProduction = false;

if($user['group_id']==1)
{
    // для админов полный просмотр
    $managerFilter = "";
    $bShowSales = true;
    $bShowClients = true;
    $bShowProductionSearch = true;
    $bShowProductionOrders = true;
}


//$dt_period = 2;
switch($dt_period)
{
    default:
    case 1:
        $date1 =  date("Y-m-d", strtotime("first day of this month"));
        $date2 =  date("Y-m-d", strtotime("last day of this month"));
        break;
    case 2:
        $date1 =  date("Y-m-d", strtotime("first day of this month -1 month"));
        $date2 =  date("Y-m-d", strtotime("last day of this month -1 month"));
        break;
}

// -------------------------------------------------------
// Заполняем переменные данными
if($bShowProductionOrders)
{

    $ord_managers = array();
    $i = 0;

    // ------------------------------------------------------------
    // получаем данные о всех сотрудниках работавших над заказами
    $sqlQuery = "SELECT 
                tpers.f483 as ManagerName
               FROM 
                 ".DATA_TABLE.$tableOrdersWork." as tow
                 LEFT JOIN ".DATA_TABLE.$tablePersonal." as tpers ON tpers.f1400 = tow.f12661 AND tpers.status = 0 AND tow.f12661 <> ''
               WHERE 
                 DATE(tow.f12641) BETWEEN '".$date1."' AND '".$date2."'
                 AND tow.status = 0
                 ".$managerFilter."
               GROUP BY 
                  tpers.f483
               ORDER BY 
                 tpers.f484
                ";
    $res = sql_query($sqlQuery);
    while($row = sql_fetch_array($res))
        $ord_managers[$i++] = $row['ManagerName'];

    //print_r($ord_managers);

    // ------------------------------------------------------------
    // получение работы по конкретному менеджеру, сгруппированной по заказам

    /*
    $data_salary = array();

    $sqlQuery = "SELECT
                  tow.id as WorkId
                  ,tord.f7071 as OrderNum
                  ,tord.f6591 as OrderDate
                  ,tord.f4461 as OrderSum
                  ,tst.id as OrderTypeId
                  ,tst.f1158 as OrderType
                  ,tsp.f8510 as OrderTypePrice
                  ,tord.f4431 as OrderDescription
                  ,tcmp.f435 as OrderClient
                  ,tord.f6551 as StatusOrder
                  ,tord.f10240 as StatuProduction

                  ,tow.f12981 as DateStPlan
                  ,tow.f12991 as DateFnPlan
                  ,tow.f12641 as DateStFact
                  ,tow.f12651 as DateFnFact
                  ,tpers.f483 as ManagerName
                  ,tow.f12661 as ManagerUser
                  ,twt.f12771 as WorkType
                  ,tow.f12671 as WorkDescription

                 FROM
                   ".DATA_TABLE.$tableOrdersWork." as tow
                   LEFT JOIN ".DATA_TABLE.$tableWorkTypes." as twt ON twt.id = tow.f12861 AND twt.status = 0
                   LEFT JOIN ".DATA_TABLE.$tableOrders." as tord ON tord.id = tow.f12601 AND tord.status = 0
                   LEFT JOIN ".DATA_TABLE.$tableServiceTypes." as tst ON tst.id = tord.f4451 AND tst.status = 0
                   LEFT JOIN ".DATA_TABLE.$tableServicePrice." as tsp ON tsp.id = tord.f13071 AND tsp.status = 0
                   LEFT JOIN ".DATA_TABLE.$tableClients." as tcmp ON tcmp.id = tord.f4441 AND tcmp.status = 0
                   LEFT JOIN ".DATA_TABLE.$tablePersonal." as tpers ON tpers.f1400 = tow.f12661 AND tpers.status = 0 AND tow.f12661 <> ''
                 WHERE
                   DATE(tow.f12641) BETWEEN '".$date1."' AND '".$date2."'
                   AND tow.status = 0
                   AND tpers.f483 = 'Полякова Н.А.'
                 GROUP BY
                   tord.f7071
                 ORDER BY
                    tord.f6591, tow.f12981
                  ";
    $res = sql_query($sqlQuery);
    while($row = sql_fetch_array($res))
    {
      $work_id = $row['WorkId'];

      $ord_num = $row['OrderNum'];
      $ord_date = date("Y-m-d",strtotime($row['OrderDate']));;
      $ord_sum = $row['OrderSum'];

      $ord_type_id = $row['OrderTypeId'];
      $ord_type = $row['OrderType'];
      $ord_type_price = $row['OrderTypePrice'];
      $ord_description = $row['OrderDescription'];
      $ord_client = $row['OrderClient'];
      $ord_status = $row['StatusOrder'];
      $ord_prod_benefit = $row['OrderSum'] * $prod_benefit_rate;

      $date_st_plan = $row['DateStPlan'];
      $date_fn_plan = $row['DateFnPlan'];
      $duration_plan = round((strtotime($date_fn_plan) - strtotime($date_st_plan)) / 60 ,2);
      $date_st_fact = $row['DateStFact'];
      $date_fn_fact = $row['DateFnFact'];
      $duration_fact = round((strtotime($date_fn_fact) - strtotime($date_st_fact)) / 60 ,2);
      $manager = $row['ManagerName'];
      $work_type = $row['WorkType'];
      $work_descr = $row['WorkDescription'];

      switch($ord_type_id)
      {
        case 4:
          break;
        case 5:
          break;
      }
      //$data_salary[$manager][$ord_num]

      //print_r($row);
      //echo "<br>";
    }

    */
    // ------------------------------------------------------------
    // выборка всей работы по заказам
    $sqlQuery = "SELECT 
                tow.id as WorkId
                ,tord.f7071 as OrderNum
                ,tord.f6591 as OrderDate
                ,tord.f4461 as OrderSum
                ,tst.id as OrderTypeId
                ,tst.f1158 as OrderType
                ,tsp.f8510 as OrderTypePrice
                ,tord.f4431 as OrderDescription
                ,tcmp.f435 as OrderClient
                ,tord.f6551 as StatusOrder                
                ,tord.f10240 as StatuProduction           
                
                ,tow.f12981 as DateStPlan
                ,tow.f12991 as DateFnPlan
                ,tow.f12641 as DateStFact
                ,tow.f12651 as DateFnFact
                ,tpers.f483 as ManagerName
                ,tow.f12661 as ManagerUser
                ,twt.f12771 as WorkType
                ,tow.f12671 as WorkDescription

               FROM 
                 ".DATA_TABLE.$tableOrdersWork." as tow
                 LEFT JOIN ".DATA_TABLE.$tableWorkTypes." as twt ON twt.id = tow.f12861 AND twt.status = 0
                 LEFT JOIN ".DATA_TABLE.$tableOrders." as tord ON tord.id = tow.f12601 AND tord.status = 0
                 LEFT JOIN ".DATA_TABLE.$tableServiceTypes." as tst ON tst.id = tord.f4451 AND tst.status = 0
                 LEFT JOIN ".DATA_TABLE.$tableServicePrice." as tsp ON tsp.id = tord.f13071 AND tsp.status = 0
                 LEFT JOIN ".DATA_TABLE.$tableClients." as tcmp ON tcmp.id = tord.f4441 AND tcmp.status = 0
                 LEFT JOIN ".DATA_TABLE.$tablePersonal." as tpers ON tpers.f1400 = tow.f12661 AND tpers.status = 0 AND tow.f12661 <> ''
               WHERE 
                 DATE(tow.f12641) BETWEEN '".$date1."' AND '".$date2."'
                 AND tow.status = 0
                 ".$managerFilter."
               ORDER BY 
                  tord.f6591, tow.f12981 
                ";

    $res = sql_query($sqlQuery);
    while($row = sql_fetch_array($res))
    {
        $work_id = $row['WorkId'];

        $ord_num = $row['OrderNum'];
        $ord_date = date("Y-m-d",strtotime($row['OrderDate']));;
        $ord_sum = $row['OrderSum'];
        $ord_type_id = $row['OrderTypeId'];
        $ord_type = $row['OrderType'];
        $ord_type_price = $row['OrderTypePrice'];
        $ord_description = $row['OrderDescription'];
        $ord_client = $row['OrderClient'];
        $ord_status = $row['StatusOrder'];
        $ord_prod_benefit = $row['OrderSum'] * $prod_benefit_rate;

        $date_st_plan = $row['DateStPlan'];
        $date_fn_plan = $row['DateFnPlan'];
        $duration_plan = round((strtotime($date_fn_plan) - strtotime($date_st_plan)) / 60 ,2);
        $date_st_fact = $row['DateStFact'];
        $date_fn_fact = $row['DateFnFact'];
        $duration_fact = round((strtotime($date_fn_fact) - strtotime($date_st_fact)) / 60 ,2);
        $manager = $row['ManagerName'];
        $work_type = $row['WorkType'];
        $work_descr = $row['WorkDescription'];

        $data_pr_ord[$ord_num]['work'][$work_id]['manager'] = $manager;
        $data_pr_ord[$ord_num]['work'][$work_id]['work_type'] = $work_type;
        $data_pr_ord[$ord_num]['work'][$work_id]['work_descr'] = $work_descr;
        $data_pr_ord[$ord_num]['work'][$work_id]['date_st_plan'] = $date_st_plan;
        $data_pr_ord[$ord_num]['work'][$work_id]['date_fn_plan'] = $date_fn_plan;
        $data_pr_ord[$ord_num]['work'][$work_id]['duration_plan'] = $duration_plan;
        $data_pr_ord[$ord_num]['work'][$work_id]['date_st_fact'] = $date_st_fact;
        $data_pr_ord[$ord_num]['work'][$work_id]['date_fn_fact'] = $date_fn_fact;
        $data_pr_ord[$ord_num]['work'][$work_id]['duration_fact'] = $duration_fact;

        if($data_pr_ord[$ord_num]['order']['order_date']=="") $data_pr_ord[$ord_num]['order']['order_date'] = $ord_date;
        if($data_pr_ord[$ord_num]['order']['order_benefit']=="") $data_pr_ord[$ord_num]['order']['order_benefit'] = $ord_prod_benefit;
        if($data_pr_ord[$ord_num]['order']['order_status']=="") $data_pr_ord[$ord_num]['order']['order_status'] = $ord_status;
        if($data_pr_ord[$ord_num]['order']['order_client']=="") $data_pr_ord[$ord_num]['order']['order_client'] = $ord_client;
        if($data_pr_ord[$ord_num]['order']['order_sum']=="") $data_pr_ord[$ord_num]['order']['order_sum'] = $ord_sum;
        if($data_pr_ord[$ord_num]['order']['order_type']=="") $data_pr_ord[$ord_num]['order']['order_type'] = $ord_type;
        if($data_pr_ord[$ord_num]['order']['order_type_price']=="") $data_pr_ord[$ord_num]['order']['order_type_price'] = $ord_type_price;
        if($data_pr_ord[$ord_num]['order']['order_description']=="") $data_pr_ord[$ord_num]['order']['order_description'] = $ord_description;

        switch($ord_type_id)
        {
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                break;
            case 5:
                break;
            case 6:
                break;
            case 7:
                break;
            case 8:
                break;
            case 9:
                break;
            case 11:
                break;
            case 12:
                break;
        }


    }

    //print_r($data_pr_ord);

}

// -------------------------------------------------------
// Переносим переменные в отображение
$smarty->assign("data_pr_ord", $data_pr_ord);
$smarty->assign("ord_managers", $ord_managers);
$smarty->assign("managers_count", count($ord_managers));

$smarty->assign("show_sales", $bShowSales);
$smarty->assign("show_clients", $bShowClients);
$smarty->assign("show_prod_srch", $bShowProductionSearch );
$smarty->assign("show_prod_ord", $bShowProductionOrders );

$smarty->assign("dt_period", $dt_period);