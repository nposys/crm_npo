<?php
// ----------------------------------------------
// Здесь подготовливаем данные для вывода в отчете
$tablePersonal = 46;
$tableEvents = 62;
$tableClients = 42;
$tableClientsData = 630;

$tableOrders = 271;
$tableInvoices = 43;
$tableIncomes = 511;
$tableIncomeInvoiceOrders = 650;

$date_sales1 = "2016-01-11 00:00:00";
$date_sales2 = "2016-01-31 23:59:59";

// примерная оценка трудоемкости событий
$workloads = array(
    "Звонок"=>10
,"Письмо"=>5
,"Встреча"=>120
,"Создание заказа"=>15
,"Выставление счета"=>10
,"Получение оплаты"=>0
,"Подача заявки"=>15
,"Торги"=>30

);

// =============================================================================
// обрабатываем фильтры на входе
if ($_REQUEST['date1']) $date1 = date("d.m.Y",strtotime(form_eng_time($_REQUEST['date1'])));
//else $date1 = date("d.m.Y", mktime(0,0,0,date("m"),date("d")-7,date("Y")));
else $date1 = date("d.m.Y", strtotime("first day of this month"));

if ($_REQUEST['date2']) $date2 = date("d.m.Y",strtotime(form_eng_time($_REQUEST['date2'])));
else $date2 = date("d.m.Y");

$date1_fet = form_eng_time($date1.' 00:00:00');
$date2_fet = form_eng_time($date2.' 23:59:59');

if ($_REQUEST['_type'] || $_REQUEST['_manager'] || $_REQUEST['_dates'])
{
    reset_filters(62);
    if ($_REQUEST['_dates']) set_filter(724, "period", $date1." 00:00:00", $date2." 23:59:59");
    if ($_REQUEST['_manager']) set_filter(727, "=", (int)($_REQUEST['_manager']));
    if ($_REQUEST['_type']) set_filter(773, "=", form_input($_REQUEST['_type']));
    set_filter(1053, "=", "Да");
    header("Location: ".$config["site_root"]."/fields.php?table=62");
}

if ($_REQUEST['manager']) $manager = (int)($_REQUEST['manager']); elseif ($user['group_id']!=1) $manager = $user['id'];

// формируем селектбокс по выбору менеджеров
if ($user['group_id']==1)
    $sel_manager = "<option value=''>Все</option>\r\n";
$result = sql_query("SELECT DISTINCT `user`.`id`, `user`.`fio` FROM `".USERS_TABLE."` AS `user`, `".GROUPS_TABLE."` AS `group` WHERE `user`.`arc`=0 AND `user`.`group_id`!='777'");
while ($row = sql_fetch_assoc($result))  $sel_manager.= "<option value='".$row['id']."'".(($row['id']==$manager)?" selected":"").">".$row['fio']."</option>\r\n";

if($manager) $userCond = " and pers.f1400=".$manager;
if($date1 && $date2) $dateCond = " and events.f724>='".$date1_fet."' and events.f724<='".$date2_fet."'";

// =============================================================================
// основная логика

// -----------------------------------------
// заполняем все типы событий
// -----------------------------------------
$field = sql_select_array(FIELDS_TABLE, "id=773");
$event_types  = explode(PHP_EOL,$field["type_value"]);
foreach($event_types as &$event_type) $event_type = trim($event_type);

// -----------------------------------------
// заполняем массив с сотрудниками
// -----------------------------------------
$managers = array();
$i = 0;
$sqlPersonal = "SELECT pers.*, user.id as user_id FROM ".DATA_TABLE.$tablePersonal." as pers JOIN ".USERS_TABLE." as user ON user.login=pers.f1410 WHERE pers.status=0 and pers.f7941='Да' ";
$resPersonal = sql_query($sqlPersonal);
while($rowPersonal = sql_fetch_assoc($resPersonal ))
{
    $managers[$i]['id'] = $rowPersonal['id'];
    $managers[$i]['name'] = $rowPersonal['f6631'];
    $managers[$i]['user'] = $rowPersonal['f1410'];
    $managers[$i]['user_id'] = $rowPersonal['user_id']; // 1 = менеджер продаж, 6 == менеджер по работе с клиентами
    $managers[$i]['position'] = $rowPersonal['f484']; // 1 = менеджер продаж, 6 == менеджер по работе с клиентами
    $i++;
}

$managers_sales = array();
foreach($managers as $next_manager) if($manager['position']==1) $managers_sales[] = $next_manager;
$managers_sales[] = array("id"=>0,"name"=>"","user"=>"","user_id"=>"","position"=>"");

$managers_clients = array();
foreach($managers as $next_manager) if($manager['position']==6) $managers_clients[] = $next_manager;
$managers_clients[] = array("id"=>0,"name"=>"","user"=>"","user_id"=>"","position"=>"");

// --------------------------------------------------------------------------
// выбираем альтернативно всех фактических менеджеров продаж из заказов
// --------------------------------------------------------------------------

$mr = array();
$i = 0;
$condSales = "";
$sqlPersonal = "
              SELECT 
                SUM(orders.f4461) as sum_orders
                ,pers.*
                ,users.id as user_id 
              FROM 
                ".DATA_TABLE.$tableOrders." as orders
                    JOIN ".USERS_TABLE." as users ON users.id=orders.f4411
                    JOIN ".DATA_TABLE.$tablePersonal." as pers on pers.f1410=users.login
                    JOIN ".DATA_TABLE.$tableIncomeInvoiceOrders." as iio on iio.f10890=orders.id
                    JOIN ".DATA_TABLE.$tableIncomes." as inc ON iio.f10910=inc.id
              WHERE 
                orders.status = 0
                and iio.status = 0
                and inc.f7521 BETWEEN '".$date_sales1."' AND '".$date_sales2."'             
                ".$condSales."
              GROUP BY
                orders.f4411
                ";
$resPersonal = sql_query($sqlPersonal);
while($rowPersonal = sql_fetch_assoc($resPersonal ))
{
    $mr[$i]['id'] = $rowPersonal['id'];
    $mr[$i]['name'] = $rowPersonal['f6631'];
    $mr[$i]['user'] = $rowPersonal['f1410'];
    $mr[$i]['user_id'] = $rowPersonal['user_id']; // 1 = менеджер продаж, 6 == менеджер по работе с клиентами
    $mr[$i]['position'] = $rowPersonal['f484']; // 1 = менеджер продаж, 6 == менеджер по работе с клиентами  $i++;
    $mr[$i]['sum'] = $rowPersonal['sum_orders']; // 1 = менеджер продаж, 6 == менеджер по работе с клиентами  $i++;

    $i++;
}

// --------------------------------------------------------
// получаем данные по всем типам событий по каждому менеджеру
// --------------------------------------------------------

$events = array();

// заполняем нулями
foreach ($managers as $next_manager)
{
    if($manager == 0 || $manager==$next_manager['user_id'])
    {
        foreach ($event_types as $event_type)
        {
            $event_type = trim($event_type);
            $cur_date = date("Y-m-d", strtotime($date1));
            $fin_date = date("Y-m-d", strtotime($date2." +0 day"));
            do
            {
                $events[$next_manager['name']][$event_type][$cur_date]['plan'] = 0;
                $events[$next_manager['name']][$event_type][$cur_date]['fact'] = 0;
                $cur_date = date("Y-m-d", strtotime($cur_date." +1 day"));
            }
            while ($cur_date<=$fin_date);
        }
    }
}

// группируем по плановой дате
if($date1 && $date2) $dateCond = " and events.f724>='".$date1_fet."' and events.f724<='".$date2_fet."'";
foreach ($managers as $next_manager)
{
    if($manager == 0 || $manager==$next_manager['user_id'])
    {
        foreach ($event_types as $event_type)
        {
            $event_type = trim($event_type);
            // звонки f1053 = поле "Выполнено", f4761 = поле "Результативно"
            $sqlQuery = "
            SELECT 
                DATE(events.f724) as event_date, pers.f6631, events.f773, count(events.id) as events_num, pers.f1400 as user
            FROM 
                ".DATA_TABLE.$tableEvents." as events 
                  JOIN ".DATA_TABLE.$tablePersonal." as pers on pers.id=events.f7821 
            WHERE 
                events.f1053='Да' and events.f4761='Да' and events.f773='".$event_type."' and events.status=0 and events.f7821=".$next_manager['id'].$userCond." ".$dateCond."
            GROUP BY 
                DATE(events.f724), events.f773, pers.id, pers.f1400
      ";
            $resSQL = sql_query($sqlQuery);
            while($rowSQL = sql_fetch_array($resSQL))
            {
                $events[$next_manager['name']][$event_type][$rowSQL['event_date']]['plan'] = $rowSQL['events_num'];
            }
        }
    }
}

// группируем по фактической дате изменения события
if($date1 && $date2) $dateCond = " and events.f11100>='".$date1_fet."' and events.f11100<='".$date2_fet."'";
foreach ($managers as $next_manager)
{
    if($manager == 0 || $manager==$next_manager['user_id'])
    {
        foreach ($event_types as $event_type)
        {
            $event_type = trim($event_type);
            // звонки f1053 = поле "Выполнено", f4761 = поле "Результативно"
            $sqlQuery = "
            SELECT 
                DATE(events.f11100) as event_date, pers.f6631, events.f773, count(events.id) as events_num, pers.f1400 as user
            FROM 
                ".DATA_TABLE.$tableEvents." as events 
                  JOIN ".DATA_TABLE.$tablePersonal." as pers on pers.id=events.f7821 
            WHERE 
                events.f1053='Да' and events.f4761='Да' and events.f773='".$event_type."' and events.status=0 and events.f7821=".$next_manager['id'].$userCond." ".$dateCond."
            GROUP BY 
                DATE(events.f11100), events.f773, pers.id, pers.f1400
      ";
            $resSQL = sql_query($sqlQuery);
            while($rowSQL = sql_fetch_array($resSQL))
            {
                $events[$next_manager['name']][$event_type][$rowSQL['event_date']]['fact'] = $rowSQL['events_num'];
            }
        }
    }
}


// считаем итоговый счет по дням
foreach ($managers as $next_manager)
{
    if($manager == 0 || $manager==$next_manager['user_id'])
    {

        $cur_date = date("Y-m-d", strtotime($date1));
        $fin_date = date("Y-m-d", strtotime($date2." +0 day"));
        do
        {
            $score_plan = 0;
            $score_fact = 0;

            foreach ($event_types as $event_type)
            {
                $score_plan += $events[$next_manager['name']][$event_type][$cur_date]['plan']*$workloads[$event_type];
                $score_fact += $events[$next_manager['name']][$event_type][$cur_date]['fact']*$workloads[$event_type];
            }

            $events[$next_manager['name']]["СЧЕТ"][$cur_date]['plan'] = (float)($score_plan) / 60;
            $events[$next_manager['name']]["СЧЕТ"][$cur_date]['fact'] = (float)($score_fact) / 60;

            $cur_date = date("Y-m-d", strtotime($cur_date." +1 day"));
        }
        while ($cur_date<=$fin_date);
    }
}

// --------------------------------------------------------
// получаем данные по продажам
$sales = array();

foreach ($managers_sales as $manager_sales)
{
    foreach ($managers_clients as $manager_clients)
    {
        $sales[$manager_sales['name']][$manager_clients['name']] = 0;
    }
}

foreach ($managers_sales as $manager_sales)
{
    foreach ($managers_clients as $manager_clients)
    {
        $condSales = "";
        $condClients = "";

        if($manager_sales['user_id']<>"") $condSales = "and ord.f4411=".$manager_sales['user_id']." ";
        if($manager_clients['user_id']<>"") $condClients = "and ord.f6581=".$manager_clients['user_id']." ";

        $sqlQuery = "
          SELECT 
            SUM(oii.f10920) as sum_sales
          FROM 
            ".DATA_TABLE.$tableIncomeInvoiceOrders." as oii
              LEFT JOIN ".DATA_TABLE.$tableIncomes." as inc ON oii.f10910 = inc.id
              LEFT JOIN ".DATA_TABLE.$tableOrders." as ord ON oii.f10890 = ord.id
          WHERE 
             oii.status=0
             and inc.f7521 BETWEEN '".$date_sales1."' AND '".$date_sales2."'             
          ".$condSales.$condClients;

        $resSQL = sql_query($sqlQuery);
        $rowSQL = sql_fetch_array($resSQL);
        $sales[$manager_sales['name']][$manager_clients['name']] = $rowSQL['sum_sales'];
    }

}


// ----------------------------------------------
// инициализируем переменные
$arr = array();

$graph_data1 = array();
$graph_data2 = array();

$i = 0;
$j = 0;
$strSeriesName1 = "";
$strSeriesName2 = "";

// ----------------------------------------------
// зполняем данные по событиям менеджеров

$table_events = array();

$resPersonal = data_select($tablePersonal, 'status=0 and f7941="Да"');
while($rowPersonal = sql_fetch_assoc($resPersonal ))
{
    if ($rowPersonal['f484']<>1) continue; // 1 = менеджер продаж, 6 == менеджер по работе с клиентами

    $strName = $rowPersonal['f6631'];
    $strLogin = $rowPersonal['f1410'];

    $bFoundEvents1 = false;
    $bFoundEvents2 = false;

    $arr[$rowPersonal['id']]['name'] = $strName;
    $arr[$rowPersonal['id']]['login'] = $strLogin;

    $result = data_select_field($tableEvents, "count(id) AS events_num ", "f7821=",$rowPersonal['id']);
    $row = sql_fetch_assoc($result);
    $arr[$rowPersonal['id']]['events'] = $row['events_num'];

    // подготовка данных для таблиц
    $table_events_colnames = array();
    $te_cols = 0;
    $cur_date = date("Y-m-d", strtotime($date1));
    $fin_date = date("Y-m-d", strtotime($date2." +0 day"));
    do
    {
        $table_events[$strName]["Звонки"][$cur_date] = 0;
        $table_events[$strName]["Встречи"][$cur_date] = 0;
        $table_events[$strName]["СЧЕТ"][$cur_date] = 0;

        $graph_data1[$i][$cur_date] = 0;
        $graph_data2[$i][$cur_date] = 0;

        $table_events_colnames[$te_cols++] = $cur_date;
        $cur_date = date("Y-m-d", strtotime($cur_date." +1 day"));
    }
    while ($cur_date<=$fin_date);


    // звонки
    $sqlQuery1 = "
        SELECT 
            DATE(events.f724) as event_date, pers.f6631, events.f773, count(events.id) as events_num, pers.f1400 as user
        FROM 
            cb_data62 as events JOIN cb_data46 as pers on pers.id=events.f7821 
        WHERE 
            f1053='Да' and f4761='Да' and f773='Звонок' and events.status=0 and pers.id = ".$rowPersonal['id'].$userCond." ".$dateCond."
        GROUP BY 
            DATE(events.f724), events.f773, pers.id, pers.f1400
  ";
    $resSQL1 = sql_query($sqlQuery1);

    //if(sql_num_rows ($resSQL1)>0) for($k=0;$k<31;$k++) $graph_data1[$i]["2016-12-".sprintf("%'02d",$k+1)] = 0;

    while($rowSQL1 = sql_fetch_array($resSQL1 ))
    {
        $graph_data1[$i][$rowSQL1['event_date']] = $rowSQL1['events_num'];
        $bFoundEvents1 = true;

        $table_events[$rowPersonal['f6631']]["Звонки"][$rowSQL1['event_date']] = $rowSQL1['events_num'];
    }

    if ($bFoundEvents1)
    {
        $strSeriesName1 .= "'".$strName."',";
        $i++;
    }

    // встречи
    $sqlQuery2 = "
        SELECT 
            DATE(events.f724) as event_date, pers.f6631, count(events.id) as events_num, pers.f1400 as user
        FROM 
            cb_data62 as events JOIN cb_data46 as pers on pers.id=events.f7821 
        WHERE 
            events.f1053='Да' and events.f4761='Да' and events.f773='Встреча' and events.status=0 and pers.id=".$rowPersonal['id'].$userCond." ".$dateCond."
        GROUP BY 
            DATE(events.f724), pers.id
  ";
    $resSQL2 = sql_query($sqlQuery2);
    //if(sql_num_rows ($resSQL2)>0) for($k=0;$k<31;$k++) $graph_data2[$j]["2016-12-".sprintf("%'02d",$k+1)] = 0;

    while($rowSQL2 = sql_fetch_array($resSQL2 ))
    {
        if (trim($rowSQL2['event_date']) <>"")
        {
            $graph_data2[$j][$rowSQL2['event_date']] = $rowSQL2['events_num'];
            $bFoundEvents2 = true;

            $table_events[$rowPersonal['f6631']]["Встречи"][$rowSQL2['event_date']] = $rowSQL2['events_num'];
        }
    }

    if ($bFoundEvents2)
    {
        $strSeriesName2 .= "'".$strName."',";
        $j++;
    }
}


$graph_settings1 = array('type_graph' => 'LineDate'
,'title' => 'График'
,'div_id' => 'chart1'
,'series_names' => substr($strSeriesName1,0,strlen($strSeriesName1)-1)
,'y_max' => 40
,'x_min' => date("Y-m-d", strtotime($date1))
,'x_max' => date("Y-m-d", strtotime($date2))
,'zoom' => true
);
$data_gr1 = draw_graph($graph_data1, $graph_settings1);

$graph_settings2 = array('type_graph' => 'LineDate'
,'title' => 'График'
,'div_id' => 'chart2'
,'series_names' => substr($strSeriesName2 ,0,strlen($strSeriesName2)-1)
,'y_max' => 10
,'x_min' => date("Y-m-d", strtotime($date1))
,'x_max' => date("Y-m-d", strtotime($date2))
,'zoom' => true
);
$data_gr2 = draw_graph($graph_data2, $graph_settings2);

// подготовка данных для результатов
foreach ($table_events as $events_name_key => &$events_name)
{
    $sum = 0;

    foreach ($events_name["СЧЕТ"] as $key => &$val)
    {
        $val = (int)($events_name["Звонки"][$key]) + 7 * (int)($events_name["Встречи"][$key]);
        $sum += $val;
    }
    if ($sum == 0) unset($table_events[$events_name_key]);
}

// ----------------------------------------------
// данные по приходам, счетам и заказам
$graph_paid = array();
$resPaid = sql_query("SELECT DATE_FORMAT(`f7521`, '%Y-%m') as PaidMonth, SUM(f7611) as PaidSum FROM `".DATA_TABLE."511`WHERE status=0 and `f7521`>'2017-01-01' GROUP BY DATE_FORMAT(`f7521`, '%Y-%m')");
while($rowPaid = sql_fetch_assoc($resPaid))
{
    $graph_paid[0][$rowPaid["PaidMonth"]] = (float)($rowPaid["PaidSum"]);
    $graph_paid[1][$rowPaid["PaidMonth"]] = 0;
}

$curMonth = date("Y-m");
$resInvoice = sql_query("SELECT DATE_FORMAT(`f10810`, '%Y-%m') as ForecastMonth, SUM(f456) as ForecastSum FROM `".DATA_TABLE."43` WHERE status=0 and f10810 is not null and f456>0 and f10830='Нормальный' GROUP BY DATE_FORMAT(`f10810`, '%Y-%m')");
while($rowInvoice = sql_fetch_assoc($resInvoice ))
{
    $strMonth = $rowInvoice["ForecastMonth"];
    if($rowInvoice["ForecastMonth"]=="0000-00" || $rowInvoice["ForecastMonth"]<$curMonth ) $strMonth = $curMonth; // поменять на вычисление текущего месяца!!!!

    $graph_paid[1][$strMonth] += (float)($rowInvoice["ForecastSum"]);
    if($graph_paid[0][$strMonth] == "") $graph_paid[0][$strMonth] = 0;
}

$graph_settings_paid = array('type_graph' => 'Bars'
,'title' => 'Платежи'
,'div_id' => 'chart_paid'
,'varyBarColor' => true
,'stackSeries' => true
);

$data_gr_paid = draw_graph($graph_paid, $graph_settings_paid);

// считаем недовыставленные суммы: сумма заказа минус сумма выставленного за последние полгода
$dateFilter = date("Y-m-d", strtotime("-6 month"));
$resOrders = sql_query("SELECT SUM(f4461-f10780) as GSum FROM `".DATA_TABLE."271` WHERE status=0 and (f4461-f10780)>0 and f6551 in ('10. Исполнение','20. Предоплата','30. Завершено') and f6591>'".$dateFilter."'");
$rowOrders = sql_fetch_assoc($resOrders);
$sumOrders = number_format($rowOrders["GSum"], 2, ',', ' ');

// данные для таблицы по приходам и счетам
$cur_month = date("Y-m");
foreach ($graph_paid[0] as $key => $value)
{
    $graph_paid[2][$key] = 0;
    if($cur_month == $key) $graph_paid[2][$key] = (float)($rowOrders["GSum"]);
    $graph_paid[3][$key] = $graph_paid[0][$key] + $graph_paid[1][$key] + $graph_paid[2][$key];
}

$table_paid = array (
    "Поступления" => $graph_paid[0],
    "Счета" => $graph_paid[1],
    "Не выставлено" => $graph_paid[2],
    "Всего" => $graph_paid[3]
);
// ----------------------------------------------
// Переносим переменные в отображение
$smarty->assign("arr", $arr);
$smarty->assign("graph1", $data_gr1);
$smarty->assign("graph2", $data_gr2);
$smarty->assign("graph_paid", $data_gr_paid);
$smarty->assign("sumOrders", $sumOrders);

$smarty->assign("table_paid", $table_paid);

$smarty->assign("table_events", $table_events);
$smarty->assign("table_events_colnames", $table_events_colnames);
$smarty->assign("te_cols", $te_cols);

$smarty->assign("events", $events);


$smarty->assign("date1", $date1);
$smarty->assign("date2", $date2);
$smarty->assign("data", $data_m);
$smarty->assign("manager", $manager);
$smarty->assign("sel_manager", $sel_manager);
$smarty->assign("user_group", $user['group_id']);