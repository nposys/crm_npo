<?php

require_once 'add/functions/service_functions.php';

$tableWidth = array(15, 30);
$numCols = count($tableWidth);

$totalWidht = 1;
foreach($tableWidth as $nextColWidth) $totalWidht += ($nextColWidth + 1);

$testData =
    array(
       0 => array(
        'Title'=>'Материал номер один по гост такому-то по гост такому-то',
        'Params' =>
        array(
           array(
               'Title' => 'Показатель номер один с длинным названием',
               'Min' => '10',
               'Max' => '20',
               'Fixed' => ''
           ),
           array(
               'Title' => 'Показатель два',
               'Min' => '',
               'Max' => '',
               'Fixed' => 'Наличие'

           ),
           array(
               'Title' => 'Показатель номер три',
               'Min' => '',
               'Max' => 'от сорока и выше',
               'Fixed' => ''

           ),
        )
       ),
    );

$processedData = array();

/// ----------------------------------------------------
$colTitleWidth = $tableWidth[0];
$colParamsWidth = $tableWidth[1];

$numMaterials = count($testData);

$countMaterials = count($testData);
$numProcessedRowTotal = 0;
$nextMaterialNum = 0;
foreach($testData as $nextMaterial)
{
    // вычислем число строк под название материала и общее число строк на ряд
    $numLineForTitle = CalculateNumRows ($nextMaterial['Title'], $colTitleWidth) +2 ;

    $numLinesForParams = 1;

    $countParams = count($nextMaterial['Params']);
    $nextParamNum = 0;

    $numParams = count($nextMaterial['Params'] );

    $processedData[$numProcessedRowTotal][1] = PadWord('',$colParamsWidth,'-');
    $numProcessedRow = $numProcessedRowTotal + 1;
    // вычисляем число строк для ряда таблицы
    foreach($nextMaterial['Params'] as $nextParam)
    {
        $nextParamTitle = $nextParam['Title'];
        $numLinesForParam = CalculateNumRows($nextParamTitle,$colParamsWidth);

        $numLinesForParams += $numLinesForParam;
        $numLinesForParams += 1; // bottom line

        // заполняем данными таблицу для вывода
        $curPos = 0;
        for($n=$numProcessedRow;$n<=$numProcessedRow+$numLinesForParam-1;$n++)
        {
            $nextWord = PadWord(mb_substr($nextParamTitle, $curPos, $colParamsWidth), $colParamsWidth);
            $processedData[$n][1] = $nextWord;
            $curPos += $colParamsWidth;
        }
        if($nextParamNum<$countParams-1)
            $processedData[$n][1] = PadWord('',$colParamsWidth,'-');
        else
        {
            if($numLinesForParams<$numLineForTitle)
            {
                for($n2=$n;$n2<$numProcessedRowTotal+$numLineForTitle-1;$n2++)
                {
                    $processedData[$n2][1] = PadWord('',$colParamsWidth);
                }
                $processedData[$n2][1] = PadWord('',$colParamsWidth,'-');
            }
            else
            {
                $processedData[$n][1] = PadWord('',$colParamsWidth,'-');
            }
        }

        $numProcessedRow = $n+1;

        $nextParamNum++;
    }

    $numLines = max($numLineForTitle,$numLinesForParams);

    // заполняем данными таблицу для вывода
    $curPos = 0;
    $processedData[$numProcessedRowTotal][0] = PadWord('',$colTitleWidth,'-');
    for($n=$numProcessedRowTotal+1;$n<$numProcessedRowTotal+$numLines-1;$n++)
    {
        $nextTitleLine = mb_substr($nextMaterial['Title'], $curPos, $colTitleWidth);
        $processedData[$n][0] = PadWord($nextTitleLine,$colTitleWidth);
        $curPos += $colTitleWidth;
    }
    $processedData[$n][0] = PadWord('',$colTitleWidth,'-');

    $numProcessedRowTotal += $numLines;

    $nextMaterialNum++;
}

/// ----------------------------------------------------
$resultData = '';
for($i=0;$i<count($processedData);$i++)
{
    $nextLine = '|';

    for ($j=0;$j<count($processedData[$i]);$j++)
    {
        $nextLine .= $processedData[$i][$j].'|';
    }

    $resultData .= $nextLine.'<br>';

}
// формирование абстрактной таблицы
foreach($testData as $nextMaterial)
{
    $lineTop = GenerateLine ($totalWidht);
    // вычисляем число строк для ряда таблицы
    foreach($nextMaterial['Params'] as $nextParamTitle => $nextParamValue)
    {

    }

    $lineBottom = GenerateLine ($totalWidht);

    $strTable .= $lineTop."<br>";
    $strTable .= $lineBottom."<br>";
}

// -----------------------------------------------------------------------
function GenerateLine($width)
{
    $result = '';
    for($i=0;$i<$width;$i++)
        $result .= '-';
    return $result;
}

// -----------------------------------------------------------------------
function PadWord($word, $width, $char='&nbsp;')
{
    $curLen = mb_strlen($word);
    $result = $word;
    if($curLen<$width)
    {
        $padChars = $width-$curLen;
        for($i=0;$i<$padChars;$i++)
        {
            $result .= $char;
        }

    }
    return $result;
}

// -----------------------------------------------------------------------
function CalculateNumRows($phrase, $width, $bDoBreakBySpacecs=false)
{
    if (!$bDoBreakBySpacecs)
        return ceil(mb_strlen($phrase) / $width);
}


$smarty->assign("data1", $resultData);

?>