# Коды расширения функционала CRM Клиентская База

## Зависимости
- clientbase 3.0.1
- php 7.0
- php extensions (pdo_pqsql)
- mysql или mariadb

## Первичная установка
- прописать подключение к БД в основном конфигурационном файле crm
  include/config.php

- создать файл с настройками подключений к db.php по примеру db.php.example

- инициализировать composer
