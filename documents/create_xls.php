<?php
/**
 * Created by PhpStorm.
 * User: miheev
 * Date: 30.11.2018
 * Time: 12:03
 */
require_once 'add/vendor/autoload.php';

use PHPExcel;
use PHPExcel_IOFactory;


$departments_info =[];
$row_count =0;
$sqlQuery = "SELECT d.id,p.id, CASE WHEN f11891 LIKE 'Отдел продаж%' THEN 'Отдел продаж' ELSE f11891 END as department, f4331 as position, u.id AS user_id, f4931 as fio
FROM
    cb_data701 AS d
        INNER JOIN
    cb_data46 AS u ON u.f12021 = d.id AND d.f11881 = 1
        INNER JOIN
    cb_data261 AS p ON u.f484 = p.id
WHERE
    f7941 = 'Да'
        AND u.status = 0
ORDER BY d.id <> 5,d.id=4,d.id=1,d.id,p.id,f4931";
$result = sql_query($sqlQuery);
while($row = sql_fetch_assoc($result)) {
    $tabels = data_select_array(781,ALL_ROWS,"f17220 LIKE '%$year%' AND f13421 = ".$row["user_id"]);
    $department_name = $row["department"];
    $user_position =  $row["position"];
    $user_fio = $row["fio"];
    $user_id = $row["user_id"];
    foreach ($tabels as $tabel_data){
        $id = (int)$tabel_data["id"];
        $user_date_start = date("d.m.Y",strtotime($tabel_data["f13461"]));
        $user_date_fact = '';
        if($tabel_data["f13491"]!='план'){
            $user_date_fact = $user_date_start;
        }
        $num_days = (float)$tabel_data["f17210"];
        $departments_info[$department_name]["user"][$user_id]["fio"] = $user_fio;
        $departments_info[$department_name]["user"][$user_id]["num_days"] += $num_days;
        $departments_info[$department_name]["user"][$user_id]["user_position"] = $user_position;
        $departments_info[$department_name]["user"][$user_id]["item"][] =["date_start" => $user_date_start,"date_fact" => $user_date_fact,"num_days"=> $num_days];
        $row_count++;
    }
    if($departments_info[$department_name]["user"][$user_id]["num_days"]<28){
        $departments_info[$department_name]["user"][$user_id]["item"][]=["num_days" => (28 - $departments_info[$department_name]["user"][$user_id]["num_days"])];
        $departments_info[$department_name]["user"][$user_id]["fio"] = $user_fio;
        $departments_info[$department_name]["user"][$user_id]["user_position"] = $user_position;
        $row_count++;
    }
    if(count($tabels)<2){
        $departments_info[$department_name]["user"][$user_id]["item"][]=[];
        $row_count++;
    }
}

$fileName = "add/documents/graf.xlsx";
$reader = PHPExcel_IOFactory::createReaderForFile($fileName);
$excel = $reader->load($fileName);

$cell_to = $excel->getActiveSheet()->getCell("CI12");
$cell_to->setValue(date("d.m.yг."));
$cell_to = $excel->getActiveSheet()->getCell("DB12");
$cell_to->setValue("$year"."г.");
$excel->setActiveSheetIndex(0);
$excel->getActiveSheet()->insertNewRowBefore(20,$row_count);
$all_row = $row_count+20;
for ($row = 20; $row < $all_row; $row++) {
    copyRowFull($excel->getActiveSheet(),19,$row);
}
$start_row = 19;
$last_department = '';
$last_user_name = '';
foreach ($departments_info as $department_key=>$department_info) {
    foreach ($department_info["user"] as $user_id => $user_info) {
        foreach ($user_info["item"] as $id => $info) {

            $cell_to = $excel->getActiveSheet()->getCell("A" . $start_row);
            if ($last_department === $department_key) {
                $cell_to->setValue('');
            } else {
                $cell_to->setValue($department_key);
                $last_department = $department_key;
            }

            $cell_to = $excel->getActiveSheet()->getCell("U" . $start_row);
            if ($last_user_name === $user_info["fio"]) {
                $cell_to->setValue('');
            } else {
                $cell_to->setValue($user_info["user_position"]);
            }

            $cell_to = $excel->getActiveSheet()->getCell("AO" . $start_row);
            if ($last_user_name === $user_info["fio"]) {
                $cell_to->setValue('');
            } else {
                $cell_to->setValue($user_info["fio"]);
                $last_user_name = $user_info["fio"];
            }

            $cell_to = $excel->getActiveSheet()->getCell("CN" . $start_row);
            $cell_to->setValue($info["num_days"]);
            $cell_to = $excel->getActiveSheet()->getCell("DA" . $start_row);
            $cell_to->setValue($info["date_start"]);
            $cell_to = $excel->getActiveSheet()->getCell("DL" . $start_row);
            $cell_to->setValue($info["date_fact"]);
            $start_row++;
        }
    }
}
$writer = PHPExcel_IOFactory::createWriter($excel,"Excel2007");
$writer->save("add/documents/tmp_graf.xlsx");
echo "<script>   
        var link = document.createElement('a');
        link.setAttribute('href','add/documents/tmp_graf.xlsx');
        link.setAttribute('download','График отпусков $year.xlsx');
        link.click();
        </script>";
/**
 * @param $ws PHPExcel_Worksheet
 * @param $row_from
 * @param $row_to
 */
function copyRowFull(&$ws, $row_from, $row_to) {
    $ws->getRowDimension($row_to)->setRowHeight($ws->getRowDimension($row_from)->getRowHeight());
    $lastColumn = $ws->getHighestColumn();
    ++$lastColumn;
    for ($c = 'A'; $c != $lastColumn; ++$c) {
        $cell_from = $ws->getCell($c.$row_from);
        if($cell_from->isMergeRangeValueCell()) {
            $col = PHPExcel_Cell::coordinateFromString(PHPExcel_Cell::splitRange($cell_from->getMergeRange())[0][1])[0];
            $ws->mergeCells($c.$row_to.':'.$col.$row_to);
            //$cell->getMergeRange()
        }
        $cell_to = $ws->getCell($c.$row_to);
        $cell_to->setXfIndex($cell_from->getXfIndex()); // black magic here
        $cell_to->setValue($cell_from->getValue());
    }
}