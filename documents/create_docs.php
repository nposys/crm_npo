<?php
/**
 * Created by PhpStorm.
 * User: miheev
 * Date: 27.11.2018
 * Time: 12:11
 */


require_once 'add/vendor/autoload.php';

use nposys\clientbase\Tables\Contract;
use nposys\clientbase\Tables\Counteragent;
use nposys\clientbase\Tables\Personal;
use nposys\clientbase\Tables\Task;
use PhpOffice\PhpWord\PhpWord;
if($_GET["create_claim_doc"])
{
    create_claim_doc($_GET["contract_id"]);
}else if($_GET["create_refund_guarantee"]){
    create_claim_doc($_GET["contract_id"]);
}

function create_claim_doc($contract_id)
{
    $base_url = "http://" . $_SERVER['HTTP_HOST'] . "/cb";
    $contract = new Contract();
    $contract->getDataById((int)$contract_id);

    $contract->data[0]["Сумма"] = (float)$contract->data[0]["Сумма"];
    $contract->formatNumericData($contract->data[0]);
    $contract_title = $contract->data[0]["Название"];

    $counteragent = new Counteragent();
    $counteragent->getDataById((int)$contract->data[0]["Заказчик"]);

    $contract_num = $contract->data[0]["Номер"];
    $contract_date = $contract->data[0]["Дата"];
    if ($contract_num == "" || $contract_date == "0000-00-00 00:00:00") {
        display_notification("Не заполнены номер и/или дата контракта", 2);
        return;
    }
    $contract_date = date("d.m.Y", strtotime($contract_date));

    $file = "Претензия " . $contract_title . ".docx";
    $local_file = "claim_doc.docx";
    try {
        create_doc(
            ['create_date' => date("d.m.Y"),
                'create_year' => date("y"),
                'organization_title' => mb_strtoupper($counteragent->data[0]["Юридическое название"]),
                'organization_inn' => $counteragent->data[0]["ИНН"],
                'organization_kpp' => $counteragent->data[0]["КПП"],
                'organization_address' => $counteragent->data[0]["Местонахождения"],
                'contract_num' => $contract_num,
                'contract_date' => date("d.m.Y", strtotime($contract_date)),
                'contract_sum' => $contract->data[0]["Сумма"]
            ],
            $local_file
        );
        echo "<script>   
        var link = document.createElement('a');
        link.setAttribute('href','$base_url/add/tmp/$local_file');
        link.setAttribute('download','$file');
        link.click();
        </script>";
    } catch (\PhpOffice\PhpWord\Exception\Exception $e) {
    }
}

function create_refund_guarantee($contract_id)
{
    $base_url = "http://" . $_SERVER['HTTP_HOST'] . "/cb";
    $contract = new Contract();
    $contract->getDataById((int)$contract_id);

    $contract->data[0]["Обеспечение контракта"] = (float)$contract->data[0]["Обеспечение контракта"];
    $contract->formatNumericData($contract->data[0]);
    $contract_title = $contract->data[0]["Название"];
    $sum = $contract->data[0]["Обеспечение контракта"];

    $counteragent = new Counteragent();
    $counteragent->getDataById((int)$contract->data[0]["Заказчик"]);

    $organization_title = mb_strtoupper($counteragent->data[0]["Юридическое название"]);
    $organization_inn = $counteragent->data[0]["ИНН"];
    $organization_kpp = $counteragent->data[0]["КПП"];
    $organization_address = $counteragent->data[0]["Местонахождения"];

    $contract_num = $contract->data[0]["Номер"];
    $contract_date = $contract->data[0]["Дата"];
    if($contract_num=="" || $contract_date=="0000-00-00 00:00:00")
    {
        display_notification("Не заполнены номер и/или дата контракта", 2);
        return;
    }
    $contract_date = date("d.m.Y",strtotime($contract_date));
    $PHPWord = new PhpWord();
    try {
        $create_date = date("d.m.Y");
        $create_year = date("y");
        $file = "Возврат обеспечения ".$contract_title.".docx";
        $local_file = "vozvrat_sredstv.docx";
        $document = $PHPWord->loadTemplate("add/documents/$local_file");
        $document->setValue('create_date', $create_date);
        $document->setValue('create_year', $create_year);
        $document->setValue('organization_title', $organization_title);
        $document->setValue('organization_inn', $organization_inn);
        $document->setValue('organization_kpp', $organization_kpp);
        $document->setValue('organization_address', $organization_address);
        $document->setValue('contract_num', $contract_num);
        $document->setValue('contract_date', $contract_date);
        $document->setValue('sum', $sum);
        $document->saveAs("add/tmp/$local_file");
        echo "<script>   
        var link = document.createElement('a');
        link.setAttribute('href','$base_url/add/tmp/$local_file');
        link.setAttribute('download','$file');
        link.click();
    </script>";

    } catch (\PhpOffice\PhpWord\Exception\Exception $e) {
    }
}

function create_primary_reporting($contract_id)
{
    $base_url = "http://" . $_SERVER['HTTP_HOST'] . "/cb";
    $contract = new Contract();
    $contract->getDataById((int)$contract_id);

    $contract->data[0]["Обеспечение контракта"] = (float)$contract->data[0]["Обеспечение контракта"];
    $contract->formatNumericData($contract->data[0]);
    $contract_title = $contract->data[0]["Название"];
    $contract_type = $contract->data[0]["Вид"];
    $contract_num = $contract->data[0]["Номер"];
    $contract_date = $contract->data[0]["Дата"];

    $counteragent = new Counteragent();
    $counteragent->getDataById((int)$contract->data[0]["Заказчик"]);

    $organization_title = mb_strtoupper($counteragent->data[0]["Юридическое название"]);
    $organization_inn = $counteragent->data[0]["ИНН"];
    $organization_kpp = $counteragent->data[0]["КПП"];
    $organization_address = $counteragent->data[0]["Местонахождения"];

    $settlement_account = new \nposys\clientbase\Base\Table("Расчетные счета");
    $settlement_account->getData([], ["Контрагент" => $counteragent->data[0]["ID"]]);
    $organization_bik = $settlement_account->data[0]["БИК"];
    $organization_bank = $settlement_account->data[0]["Банк"];
    $organization_settlement_account = $settlement_account->data[0]["Расчетный счет"];


    $manager = new Personal();
    $manager->getDataById((int)$contract->data[0]["Менеджер РТ"]);
    $manager_fio = $manager->data[0]["ФИО"];

    if ($contract_num == "" || $contract_date == "0000-00-00 00:00:00") {
        display_notification("Не заполнены номер и/или дата контракта", 2);
        return;
    }
    $contract_date = date("d.m.Y", strtotime($contract_date));
    $PHPWord = new PhpWord();
    try {
        $create_date = date("d.m.Y");
        $create_year = date("y");
        $file = "Заявка на первичку ".$contract_title.".docx";
        $local_file = "primary_reporting.docx";
        $document = $PHPWord->loadTemplate("add/documents/$local_file");
        $document->setValue('create_date', $create_date);
        $document->setValue('create_year', $create_year);
        $document->setValue('contract_num', $contract_num);
        $document->setValue('contract_date', $contract_date);
        $document->setValue('contract_title', $contract_title);
        $document->setValue('contract_type', $contract_type);
        $document->setValue('organization_title', $organization_title);
        $document->setValue('organization_inn', $organization_inn);
        $document->setValue('organization_kpp', $organization_kpp);
        $document->setValue('organization_bank', $organization_bank);
        $document->setValue('organization_bik', $organization_bik);
        $document->setValue('organization_settlement_account', $organization_settlement_account);
        $document->setValue('organization_address', $organization_address);
        $document->setValue('manager_fio', $manager_fio);
        $document->saveAs("add/tmp/$local_file");

        global $user;
        $task = new Task();
        $task->startDate = date("Y-m-d H:i:s");
        $addDays = 1;
        if ((int)date("H") > 12) {
            $addDays++;
        }
        if ((int)date("N") > 4 || $addDays == 2 && (int)date("N") > 3) {
            $addDays = $addDays + 2;
        }
        $task->endDate = date("Y-m-d 17:00:00", strtotime(date("Y-m-d") . " +$addDays days"));
        $task->categoryId = 2;
        $task->workTypeId = 9;
        $task->creator = (int)$user["id"];
        $task->linking = "Контракт";
        $task->employeeId = 20;
        $taskId = $task->CreateTaskForEmployee("Выпустить пакет док-тов (ТН+СФ+Счет)", $contract_id);

        echo "<script>   
        var link = document.createElement('a');
        link.setAttribute('href','$base_url/add/tmp/$local_file');
        link.setAttribute('download','$file');
        link.click();
        ".($taskId>0 ? "window.open('$base_url/view_line2.php?table=150&line=$taskId');":"")."
        </script>";

    } catch (\PhpOffice\PhpWord\Exception\Exception $e) {
    }
}
function create_eksp_form($contract_id)
{
    $base_url = "http://" . $_SERVER['HTTP_HOST'] . "/cb";
    $contract = new Contract();
    $contract->getDataById((int)$contract_id);

    $contract->data[0]["Обеспечение контракта"] = (float)$contract->data[0]["Обеспечение контракта"];
    $contract->formatNumericData($contract->data[0]);
    $contract_title = $contract->data[0]["Название"];
    if ($contract_title == "" ) {
        display_notification("Не заполнено название контракта", 2);
        return;
    }
    $PHPWord = new PhpWord();
    try {
        $file = "Заявка на экспедирование ".$contract_title.".docx";
        $local_file = "eksp_form.docx";
        $document = $PHPWord->loadTemplate("add/documents/$local_file");
        $document->setValue('contract_title', $contract_title);
        $document->saveAs("add/tmp/$local_file");

        global $user;
        $task = new Task();
        $task->startDate = date("Y-m-d H:i:s");
        $addDays = 0;
        if ((int)date("H") > 15) {
            $addDays++;
        }
        if ((int)date("N") > 5 || $addDays == 1 && (int)date("N") > 4) {
            $addDays = $addDays + 2;
        }
        $task->endDate = date("Y-m-d 16:00:00", strtotime(date("Y-m-d") . " +$addDays days"));
        $task->categoryId = 2;
        $task->workTypeId = 10;
        $task->watcherId =  110;
        $task->creator = (int)$user["id"];
        $task->linking = "Контракт";
        $task->employeeId = 30;
        $taskId = $task->CreateTaskForEmployee("Выполнить грузоперевозку. 
Подробности в файле", $contract_id);

        echo "<script>   
        var link = document.createElement('a');
        link.setAttribute('href','$base_url/add/tmp/$local_file');
        link.setAttribute('download','$file');
        link.click();
        ".($taskId>0 ? "window.open('$base_url/view_line2.php?table=150&line=$taskId');":"")."
        </script>";

    } catch (\PhpOffice\PhpWord\Exception\Exception $e) {
    }
}

/**
 * @param $replacement_data
 * @param $local_file
 * @throws \PhpOffice\PhpWord\Exception\Exception
 */
function create_doc($replacement_data, $local_file){

    $PHPWord = new PhpWord();
    $document = $PHPWord->loadTemplate("add/documents/$local_file");
    foreach ($replacement_data as $key=>$item)
    {
        $document->setValue($key, $item);
    }
    $document->saveAs("add/tmp/$local_file");
}