
$(function() {
  $('.datepicker').datepicker({
        showOn:"button",
        showAlways: true,
        buttonImage: "images/calbtn.png",
        buttonImageOnly: true,
        buttonText: "Calendar",
        showAnim: (('\v'=='v')?"":"show"),  // в ie не включаем анимацию, тормозит
  })
});

function go_table(line_id, type)
{
    document.getElementById('_line_id').value = line_id;
    document.getElementById('_type').value = type;
    document.getElementById('report_form').submit();
}


function setCheckboxById(id)
{
	document.getElementById(id).checked = true;
}

function setCheckboxByName(name)
{
	document.getElementByName(name).checked = true;
}

function updateCheckBoxGroup(type, group, uid, hash, status)
{
	id = type + '_'+ group + '_' + uid + '_' + hash;
	var elements = document.getElementsByClassName(id);
	for(var i=0; i<elements.length; i++) {
	    elements[i].checked = status;
	}
}
