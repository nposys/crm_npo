<?php


namespace classes\Controllers;

use classes\Base\Controller;
use classes\Enums\CrmEnums\Departments;
use classes\Models\Salary\SalaryProcessor;
use DateInterval;
use DatePeriod;
use DateTime;

class SalaryController extends Controller
{
	private $show_rt;
	private $override_manual_with_auto;
	private $override_totals_manual_with_auto;
	private $override_manual_with_auto_common;
	private $future_payment;
	private $paid_access = false;
	private $admin_access = false;
	private $department_id = 0;
	private $manager_id = 0;

	public function __construct()
	{
		parent::__construct();
		$this->show_rt = $this->data['show_rt'];
		$this->override_manual_with_auto = $this->data['override_manual_with_auto'];
		$this->override_totals_manual_with_auto = $this->data['override_totals_manual_with_auto'];
		$this->override_manual_with_auto_common = $this->data['override_manual_with_auto_common'];
		$this->future_payment = $this->data['future_payment'];
	}

	public function run()
    {
        try {
            $this->actionCalculateSalary();
        } catch (\Exception $ex) {
            echo "Exception: ".$ex->getMessage();
        }
    }

	private function handleUserData(int $user_id, int $group_id)
	{
		// обработка входных фильтров
		if ($group_id === 1) {
			$this->paid_access = true;
			$this->admin_access = true;
		} elseif ($group_id === 811) {
			// для начальника отдела по работе с клиентами
			$this->department_id = Departments::CUSTOMER_SUPPORT_DEPARTMENT;
		} else if ($user_id > 0) {
			$rowPersTemp = data_select_array(46, "status=0 and f1400=" . $user_id);
			$this->manager_id = (int)$rowPersTemp['id'];
		}
	}

    /**
     * @throws \Exception
     */
    public function actionCalculateSalary()
    {
        global $smarty;
        global $user;
        global $nposys_config;

        $base_url = $nposys_config["BASE_URL_SUFFIX"];

        $user_id = (int)$user['id'];
        $group_id = (int)$user['group_id'];
        $this->handleUserData($user_id, $group_id);

        // =========================================================
        // селектбокс по месяцам
        $dt_period = $this->data['dt_period'];
        if (!$dt_period) {
            $dt_period = date('Y-m-01');
        }

        $from = new DateTime('2018-01-01');
        $to = new DateTime(date('Y-m-d'));
        $to = $to->modify(' + 1 day');

        $period = new DatePeriod($from, new DateInterval('P1M'), $to);
        $arrayOfDates = array_map(
            static function ($item) {
                return $item->format('Y-m-d');
            },
            iterator_to_array($period)
        );
        array_multisort($arrayOfDates, SORT_DESC);

        $sp = new SalaryProcessor(date('Y-m', strtotime($dt_period)),
            $this->manager_id,
            $this->override_manual_with_auto,
            $this->override_manual_with_auto_common,
            '',
            $this->override_totals_manual_with_auto,
            $this->future_payment);

        if (isset($this->data['data_save']) && ($user_id === 1 || $user_id === 470)) {
            $sp->updateCalculations($this->data, $this->data['data_save']);
            $groups_manager = explode(',', $this->data['data_save']);
            if (count($groups_manager) === 1) {
                $params = explode('-', $this->data['data_save']);
                $submitted_manager_id = (int)$params[1];
            }
        }
        else if(isset($this->data['paid_save']) && $this->paid_access){
            $sp->insertPaids($this->data);
        }

        $salary = $sp->calculateSalary($this->manager_id, $this->department_id);

        // Переносим переменные в отображение
        if (isset($submitted_manager_id)) {
            $smarty->assign('submitted_manager_id', $submitted_manager_id);
        }

        $smarty->assign('base_url', $base_url);
        $smarty->assign("salary_data", $salary['salary_data']);
        $smarty->assign("managers", $salary['managers']);

        $smarty->assign('future_payment', $this->future_payment);
        $smarty->assign('show_rt', $this->show_rt);

        $smarty->assign("dt_period", $dt_period);
        $smarty->assign("dates", $arrayOfDates);
        $smarty->assign("paid_access", $this->paid_access);
        $smarty->assign("admin_access", $this->admin_access);
    }

}