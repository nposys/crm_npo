<?php


namespace classes\Controllers;

use classes\Models\ClientProcessor;
use classes\Models\TDBCompany;
use classes\Models\TDBContactPerson;
use PDO;

class TDBCompaniesController
{
    private $pgdb;
    private $startDate;
    private $finishDate;
    private $companies;

    public function __construct(PDO $pgdb, $startDate, $finishDate)
    {
        $this->setStartDate($startDate);
        $this->setFinishDate($finishDate);

        $this->pgdb = $pgdb;
    }


    private function setStartDate(string $date)
    {
        if (($timestamp = strtotime($date)) === false) {
            $this->startDate = date("Y-m-d", strtotime(date("Y-m-d") . " - 1 week"));
        } else {
            $this->startDate = date("Y-m-d", $timestamp);
        }
    }

    private function setFinishDate($date)
    {
        if (($timestamp = strtotime($date)) === false) {
            $this->finishDate = date("Y-m-d");
        } else {
            $this->finishDate = date("Y-m-d", $timestamp);
        }
    }

    private function parseContacts($contactsRows): array
    {
        $contacts = [];
        $contactsRows = explode(';', $contactsRows);
        foreach ($contactsRows as $contactsRow) {
            parse_str($contactsRow, $result);
            $contacts[] = new TdbContactPerson($result);
        }
        return $contacts;
    }

    private function getQuery(): string
    {

        return "SELECT DISTINCT oC.\"orgCompanyId\", 
                                oC.\"FullTitle\",
                                oC.\"ShortTitle\",
                                oC.\"OrganizationForm\",
                                oC.\"Inn\",
                                oC.\"Kpp\",
                                oC.\"Ogrn\",
                                oC.\"Okpo\",
                                oC.\"LegalAddress\",
                                oC.\"PostalAddress\",
                                oC.\"RealAddress\",
                                oC.\"Email\",
                                oC.\"Phone\",
                                oC.\"WebSite\",
                                oC.\"DateRegistered\",
                                sR.\"Title\" as region,
                                (
                                    SELECT
                                        string_agg(concat_ws(', ',
                                                             'FNa='||\"FullName\",
                                                             'Pos='||\"Position\",
                                                             'Eml='||\"Email\",
                                                             'Phn='||\"Phone\",
                                                             'Inn='||\"Inn\"), '; ')
                                    FROM
                                        \"orgContactPerson\" oCP
                                    WHERE
                                            oCP.\"orgCompanyId\" = oC.\"orgCompanyId\"
                                ) as \"Contacts\"
                            FROM
                                \"tLotApp\" as tLA
                                    LEFT JOIN \"orgCompany\" oC on tLA.\"orgSupplierId\" = oC.\"orgCompanyId\" AND \"orgSupplierId\" IS NOT NULL
                                    LEFT JOIN \"sRegion\" sR on oC.\"sRegionId\" = sR.\"sRegionId\"
                            WHERE
                                TRUE
                              AND tLA.\"LastProtocolDate\" BETWEEN '$this->startDate' AND '$this->finishDate'
                              AND oC.\"sRegionId\" IN (54, 70)";
    }

    private function getNewCompaniesForPeriod()
    {
        $string = $this->getQuery();
        $stmt = $this->pgdb->query($string);
        $rows = $stmt->fetchAll();
        foreach ($rows as $row) {
            $this->companies[] = new TDBCompany($row, $this->parseContacts($row['Contacts']));
        }
        write_log("It's ok!");
    }

    public function run()
    {
        $this->getNewCompaniesForPeriod();
        $clientProcessor = new ClientProcessor();
        $clientProcessor->run($this->companies);
    }
}