<?php


namespace classes\Controllers;


use classes\Base\Controller;
use classes\Models\MarketingProcessor;

class MarketingController extends  Controller
{
	public function run()
	{
		$this->actionShowMarketingData();
	}

	public function actionShowMarketingData(){

		global $smarty;
		global $user;

		if(empty($this->data['date_from']) ||$this->data['date_from'] == '')
		{
			$this->data['date_from'] = date("Y-m-d",strtotime(date("Y-m-d")." - 1 month"));
		}

		if(empty($this->data['date_to']) ||$this->data['date_to'] == '')
		{
			$this->data['date_to'] = date("Y-m-d");
		}
		$mp = new MarketingProcessor($this->data);
		$table_data = $mp->getMarketingData();
		$smarty->assign('date_from',  $this->data['date_from']);
		$smarty->assign('select_company_id',  (int)$this->data['select_company_id']);
		$smarty->assign('date_to', $this->data['date_to']);
		$smarty->assign('show_rt',  $this->data['show_rt']);
		$smarty->assign('table_data',  $table_data);
		$smarty->assign('is_admin',  (int)$user['group_id']==1);
		$smarty->assign('sort_col',  $this->data['sort_col']);
		$smarty->assign('sort_direction',  $this->data['sort_direction']);
	}

}