<?php


namespace classes\Tables;


use classes\Base\Table;
use classes\Models\PostgresHelper;

class Tender extends Table
{
	public function __construct($options = [])
	{
		parent::__construct('Тендеры', $options);
	}

	private function getNskTime($time, $timezone)
	{
		$timezone_nsk = 7;
		$utc_diff = $timezone_nsk - (int)$timezone;

		if ($utc_diff !== 0) {
			if ($utc_diff > 0) {
				$time_diff = " + " . $utc_diff;
			} else {
				$time_diff = $utc_diff;
			}

			return date('Y-m-d H:i:s', strtotime($time . $time_diff . " hour"));
		}
		return $time;
	}

	private function getTenderStatus($tender_status)
	{
		switch ($tender_status) {
			case 1:
				return '10. Подача заявок';
			case 2:
				return '20. Рассмотрение заявок';
			case 3:
				return '30. Прошедшая';
			default:
				return '40. Отмененная';
		}
	}

	/*
	 *  require_once('add/vendor/autoload.php');

		use classes\Tables;

		$tender_table = new Tender();
		$tender_table->updateDataByGovRuId((int)$ID);
	 */
	public function updateDataByGovRuId(int $line_id)
	{
		$this->getDataById($line_id);
		$postgres_helper = new PostgresHelper();
		$tender_data = $postgres_helper->getLotDataByGovRuIdAndLotNumber($this->data[0]['GovRuID'], $this->data[0]['Лот']);

		if (count($tender_data) > 0) {
			$tender_info = $tender_data[0];
		} else {
			$this->data[0]['Примечание'] = 'Лот с таким GovRuID и номером лота не был найден';
			$this->save((int)$this->data[0]['ID']);
			return;
		}

		$purchases_types_table = new PurchasesTypes();
		$trading_platform_table = new TradingPlatform();
		$regions_table = new Region();

		$this->data[0]['Площадка'] = $trading_platform_table->getIdByCode($tender_info['tenderSite']);
		$this->data[0]['Вид'] = $purchases_types_table->getIdByTitle($tender_info['tenderType']);
		$this->data[0]['Закон'] = $tender_info['tenderLaw'];

		$this->data[0]['Название'] = $tender_info["lotTitle"];
		$this->data[0]['Статус тендера'] = $this->getTenderStatus($tender_info['tenderStatus']);

		$this->data[0]['Ссылка'] = $tender_info["link"];

		$this->data[0]['Заказчик'] = $tender_info["companyShortTitle"];
		$this->data[0]['Контакты заказчика'] =
			$tender_info["companyLegalAddress"] . " " .
			$tender_info["companyPhone"] . " " .
			$tender_info["companyEmail"] . " " .
			$tender_info["contactPersonFullName"];
		$this->data[0]['Регион'] = $regions_table->getRegionIdByDbId($tender_info['regionId']);

		$this->data[0]['Начальная цена'] = $tender_info["lotCost"];
		$this->data[0]['Обеспечение заявки'] = $tender_info["requestCost"];
		$this->data[0]['Обеспечение контракта'] = $tender_info["contractCost"];

		$this->data[0]['Дата публикации'] = $tender_info["datePublic"];
		$this->data[0]['Дата и время подачи'] = $tender_info["dateOpen"];
		$this->data[0]['Дата рассмотрения'] = $tender_info["dateExam"];
		$this->data[0]['Дата и время торгов'] = $tender_info["dateAuction"];

		$this->data[0]['TimeZone'] = (int)($tender_info["timeZone"]);

		$this->data[0]['Дата публикации (НСК)'] = $this->getNskTime($this->data[0]['Дата публикации'], $tender_info["timeZone"]);
		$this->data[0]['Дата и время подачи (НСК)'] = $this->getNskTime($this->data[0]['Дата и время подачи'], $tender_info["timeZone"]);
		$this->data[0]['Дата рассмотрения (НСК)'] = $this->getNskTime($this->data[0]['Дата рассмотрения'], $tender_info["timeZone"]);
		$this->data[0]['Дата и время торгов (НСК)'] = $this->getNskTime($this->data[0]['Дата и время торгов'], $tender_info["timeZone"]);

		if ($this->data[0]['Лот'] === "") {
			$this->data[0]['Лот'] = $tender_info["lotNumber"];
			if ((int)$tender_info["lotCount"] > 1) {
				$this->data[0]['Примечание'] = 'Количество лотов в закупке - ' . $tender_info["lotCount"] . ', но так как номер лота не был указан, был взят только первый лот';
			}
		}
		$this->save((int)$this->data[0]['ID']);
	}
}