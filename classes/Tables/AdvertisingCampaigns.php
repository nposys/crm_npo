<?php


namespace classes\Tables;


use classes\Base\Table;
use Symfony\Component\Config\Definition\Exception\Exception;

class AdvertisingCampaigns extends Table
{
	private $mysqli;
	private $db;
	private $resource;
	private $resources_types=[1=>"NPOSYS",2=>"TM"];

	public function __construct($resource=null,$options = [])
	{
		parent::__construct('Кампании', $options);
		if($resource){
			$this->SetResource($resource);
		}
	}

	public function SetMysqli($mysqli)
	{
		$this->mysqli = $mysqli;
	}

	/**
	 * @param $resource integer
	 */
	public function SetResource($resource){
		$this->resource = $resource;
	}

	public static function SincWithResource($resource){
		$campaigns = new AdvertisingCampaigns($resource);
		$campaigns->FullSinc();
	}
	private function FullSinc(){
		try {
			$this->CheckConections();
			Channals::SincChannals($this->db);
			ChannelsTypes::SincChannelsTypes($this->db);
			$this->CheckConections();
			$this->Sinc();
			TestVariants::SincTestVariants($this->db);
			AdvertisingCampaignsJournal::SincJournals($this->db);
		}catch (Exception $ex){
			$this->writeLog($ex->getTrace());
		}
	}

	public static function SincCampaigns($resource){
		$campaigns = new AdvertisingCampaigns($resource);
		$campaigns->CheckConections();
		$campaigns->Sinc();
	}

	private function Sinc(){
		$this->writeLog('sinc campaign start');
		$this->getData([],['Ресурс'=>$this->resource]);
		foreach ($this->data as $campaign){
			$this->writeLog($campaign['ID']);
			if($result = $this->mysqli->query("SELECT ID FROM ad_Campaign WHERE CRMID = ".$campaign['ID'])){
				$recource = $result->fetch_array(MYSQLI_ASSOC);
				$ad_ChannelID = (int)$campaign["Канал"]>0 ? "(SELECT ID FROM ad_Channel WHERE CRMID = " . $campaign["Канал"]." LIMIT 1)":'NULL';
				$name = $campaign["Внутренний идентификатор"];
				$externalID = $campaign["Внешний идентификатор"] !='' ? "'".$campaign["Внешний идентификатор"]."'" : 'NULL';
				$ad_CampaignTypeID = (int)$campaign["Тип канала"]>0 ? "(SELECT ID FROM ad_CampaignType WHERE CRMID = " . $campaign["Тип канала"]." LIMIT 1)":'NULL';
				$startDate = $campaign["Дата начала"]!='0000-00-00 00:00:00' ? "'".$campaign["Дата начала"]."'":'NULL';
				$endDate = $campaign["Дата окончания"]!='0000-00-00 00:00:00' ? "'".$campaign["Дата окончания"]."'":'NULL';
				$isActive = $campaign["Актуальная"]=='Да' ? 1 :0;
				if((int)$recource["ID"]>0){
					$query = "UPDATE ad_Campaign SET ";
					$query .="ad_ChannelID = $ad_ChannelID,";
					$query .="Name = '$name',";
					$query .="ExternalID = $externalID,";
					$query .="ad_CampaignTypeID = $ad_CampaignTypeID,";
					$query .="StartDate = $startDate,";
					$query .="EndDate = $endDate,";
					$query .="IsActive = $isActive";
					$query .=" WHERE CRMID = ".$campaign['ID'];
				}else{
					$query = "INSERT INTO ad_Campaign (ad_ChannelID, Name, ExternalID,ad_CampaignTypeID,StartDate,EndDate,IsActive,CRMID) VALUES (";
					$query .="$ad_ChannelID,";
					$query .="'$name',";
					$query .="$externalID,";
					$query .="$ad_CampaignTypeID,";
					$query .="$startDate,";
					$query .="$endDate,";
					$query .="$isActive,";
					$query .=$campaign['ID'];
					$query .=");";
					$this->mysqli->query($query);
				}
				$result->close();
			}
		}
		$this->writeLog('sinc campaign end');
	}

	private function CheckConections(){
		if(!$this->mysqli || !$this->mysqli->ping()){
			$this->CreateConections();
		}
	}
	private function setDB(){
		require 'add/db.php';
		$conection_bd =$this->resources_types[$this->resource];
		if($conection_bd) {
			$this->db = $databases[$conection_bd];
		}
	}
	private function CreateConections()
	{
		if (!$this->db) {
			$this->setDB();
		}
		$this->mysqli = mysqli_init();
		if (!$this->mysqli->real_connect($this->db['server'], $this->db['user'], $this->db['pass'], $this->db['db'])) {
			die('Ошибка подключения (' . mysqli_connect_errno() . ') '
				. mysqli_connect_error());
		}
		$this->mysqli->query("SET NAMES 'utf8'");
	}
}