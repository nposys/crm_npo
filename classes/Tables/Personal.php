<?php
/**
 * Created by PhpStorm.
 * User: Alt
 * Date: 29.10.2019
 * Time: 16:31
 */

namespace classes\Tables;

use classes\Base\Table;

class Personal extends Table
{
    const DEPARTMENT_USLUGI_TYPOVIE = 8;

    public function __construct($options = [])
	{
		parent::__construct('Наши сотрудники', $options);
	}

	/*
	 * вспомогательная функция. вычисление рабочих дней в месяце
	 */
	public function getTotalWorkingDaysByMonth($month) : int
	{
		$date_begin = $month . "-01 00:00:00";

		$last_day = date("Y-m-t 00:00:00", strtotime($month));

		$i = 0;
		$next_day = $date_begin;
		while ($next_day <= $last_day) {
			if ($this->isWorkingDay($next_day)) {
				$i++;
			}
			$next_day = date("Y-m-d 00:00:00", strtotime($next_day . " +1 day"));
		}

		return $i;
	}

	/**
	 * вспомогательная функция - проверка является ли день рабочим
	 * @param $date
	 * @return bool
	 */
	public function isWorkingDay($date) : bool
	{
		// таблица "Праздничные дни"
		$special_day = data_select_array(240, "status=0 and f3470='$date'");

		if ((int)$special_day['id'] > 0) {
			// если это особый день, то обрабатываем по таблице
			if ($special_day['f19581'] == 'Да') {
				return true;
			} else {
				return false;
			}
		} else {
			// иначе смотрим день недели
			$day_of_week = date('w', strtotime($date));
			if ($day_of_week == 0 or $day_of_week == 6) {
				return false;
			} else {
				return true;
			}
		}
	}


    /**
     * @param int $user_id
     * @return int|mixed
     */
	public function getDataByUserId(int $user_id = 0)
	{
		$user_id = (int)$user_id;
		if ($user_id > 0) {
			$personal = new self();
			$personal->getData([], ['Пользователь' => $user_id]);
			return $personal->data[0];
		}
		return 0;
	}

    /**
     * @param int $manager_id
     * @return array|int|mixed
     */
	public function getDataByManagerId(int $manager_id = 0)
    {
		$manager_id = (int)$manager_id;
		if ($manager_id > 0) {
			$personal = new self();
			return $personal->getDataById($manager_id);
		}
		return [];
	}

    /**
     * @param $manager_id
     * @param $month
     * @return float
     */
	public function getNumberOfManagerNotWorkDays($manager_id, $month) : float
	{
		$timetable = new Table('Табель');
		$timetable->getData(
			[],
			[
				'Сотрудник' => $manager_id,
				'Период' => '\'' . $month . '\''
			],
			[],
			['debug' => 1]);

		$work_days_total = 0.0;
		foreach ($timetable->data as $day_record) {
			$work_days = (float)$day_record['Рабочих дней'];

			$coefficient = 1.0;
			if ($day_record['Тип события'] === 'Больничный') {
				$coefficient = 0.5;
			}
			$work_days_total += $work_days * $coefficient;
		}

		return $work_days_total;
	}


    /**
     * возвращает все подразделения, если передали идентификатор менеджера, то только его подразделение
     * @param int $manager_id
     * @return mixed
     */
	public function getDepartments($manager_id = 0)
	{
		$departments = new Table('Отделы');
		if (empty($manager_id)) {
			$departments->getData();
		} else {
			$personal = new self();
			$personal->getDataById((int)$manager_id);
			$departments->getData([], ['ID' => $personal->data[0]['Отдел']]);
		}
		return $departments->data;
	}

    /**
     * Возвращает список всех сотрудников
     * @return array
     */
	public function getAllManagers() : array
	{
		return $this->getData(['ID', 'Фамилия Имя Отчество'], [], ['Фамилия Имя Отчество']);
	}

	/**
	 * Возвращает id сотрдуника по ФИО
	 * @param $fio
	 * @return mixed
	 */
	public function getMangerIdByFio($fio)
	{
		$this->getData(['ID'], ['Фамилия Имя Отчество' => '\'' . $fio . '\'']);
		return $this->data[0]['ID'];
	}

	/**
	 * Возвращает данные сотрудника по ФИО
	 * @param $fio
	 * @param int $replace_related_data
	 * @return mixed
	 */

	public function getManagerDataByFio($fio, $replace_related_data = 1)
	{
		$this->getData([], ['Фамилия Имя Отчество' => '\'' . $fio . '\''], [], ['replace_related_data' => $replace_related_data]);
		return $this->data[0];
	}

	/**
	 * Возвращает ФИО по id сотрудника
	 * @param int $manager_id
	 * @return string
	 */
	public function getNameById(int $manager_id) : string
	{
		$this->getDataById((int)$manager_id);
		return $this->data[0]['Фамилия Имя Отчество'];
	}

    /**
     * возвращает всех сотрудников работавших в указанный месяц
     * Если department_id = 0, возвращает всех сотрудников. Иначе возвращает сотрудников отдела
     * @param string $month
     * @param int $department_id
     * @param bool $filter_rt
     * @return array
     */
	public function getAllManagersByMonth(string $month, int $department_id = 0, bool $filter_rt = true): array
	{
		$month_begin = date("Y-m-01 00:00:00", strtotime($month));
		$month_end = date("Y-m-t 00:00:00", strtotime($month));

		$department_filter = '';
		if ($department_id > 0) {
			$department_filter = ' AND f12021=' . $department_id;
		}

		// f4931 - 'Фамилия Имя Отчество'
		// f489 - 'Дата поступления'
		// f14051 - 'Дата увольнения'
        // f24031 - 'Служебный'

		$total_filter = "status=0 and
		        f24031 <> 'Да' and 
                f489 > '2010-01-01 00:00:00' and 
                f489 IS NOT NULL and
                " . ($filter_rt ? 'f12021<>3 and ' : '') . "
                (
                    (f489<'$month_begin' AND (f14051 IS NULL OR f14051>='$month_end'))
                    OR
                    (f489<'$month_begin' AND (f14051>='$month_begin' AND f14051<='$month_end'))
                    OR
                    ((f489>='$month_begin' and f489<='$month_end') AND (f14051>='$month_begin' AND f14051<='$month_end'))
                    OR
                    ((f489>='$month_begin' AND f489<='$month_end') AND (f14051 IS NULL or f14051>='$month_end'))
                )" .
			$department_filter .
			" ORDER BY f4931 ASC";
		$result_personal = data_select($this->tableId, $total_filter);

		return $this->processSqlResult($result_personal);
	}

	public static function GetManagerForAutoTask($taskTitle)
	{
		$taskPersonal = new Table('Сотрудник - задание (автосозданное)');
		$taskPersonal->getData([], ['Текст задачи' => $taskTitle]);
		if ((int)$taskPersonal->data[0]['ID'] > 0) {
			return (int)$taskPersonal->data[0]['ID'];
		} else {
			return GetIdForManagerByPosition();
		}
	}

	public static function GetIdForManagerByPosition($positionType = 1) : int
	{
		$positionId = 0;
		switch ($positionType) {
			default:
				break;
			case 1:
				$positionId = 21;
				break;
			case 2:
				$positionId = 15;
				break;
			case 3:
				$positionId = 5;
				break;
		}
		$personal = new Personal();
		$personal->getData([], ['Должность' => $positionId]);
		if ((int)$personal->data[0]['ID'] > 0) {
			return (int)$personal->data[0]['ID'];
		}
		return 0;
	}


	public function getWorkingManagersByDepartment(int $departmentId = 0, $fields = []):array
    {
        $filters = ["Работает сейчас" => "'Да'"];

        if($departmentId > 0) {
            $filters["Отдел"] = $departmentId;
        }

        $this->getData($fields, $filters);

        return $this->data;
    }

	// получить список сотрудников с учетом табеля работающих в конкретный день
	public function getAvailableManagersByDate(string $date, int $departmentId = 0) : array
    {
        $schedule = new Table("Табель");

        $filters = [
            [
                'field_name' => 'Дата и время начала',
                'operator' => '<=',
                'value' => "'$date'"
            ],
            [
                'field_name' => 'Дата и время окончания',
                'operator' => '>=',
                'value' => "'$date'"
            ]
        ];

        if($departmentId > 0) {
            $filters["Отдел"] = $departmentId;
        }

        // получаем всех сотрудников по указанному отделу
        $managers = $this->getWorkingManagersByDepartment(
            $departmentId,
            ["ID", "Фамилия Инициалы"]
        );

        // получаем из Табеля список сотрудников, которые не работают в указанную дату
        $schedule->getData([ "Сотрудник" ],$filters);

        $notWorkManagers = [];
        foreach ($schedule->data as $manager){
            $notWorkManagers[] =(int)$manager['Сотрудник'];
        }

        // вычищаем сотрудников
        foreach ($managers as $key => $manager){
            if (in_array($manager['ID'], $notWorkManagers) ) {
                unset($managers[$key]);
            }
        }

        return $managers;
    }

    public function updateResponsibilityMatrix()
    {
        $resp = new Table ("Исполнение");
        $resp->getData();

        // удалить всех неработающих сотрудников
        // добавить всех работающих во все позиции прайса
        // $managers =  $this->getWorkingManagersByDepartment(self::DEPARTMENT_USLUGI_TYPOVIE);
    }

    public function getUserIdByManagerId(int $managerId) : int
    {
        $userId = 0;
        if($managerId > 0){
            $this->getDataById($managerId);
            $userId = (int)$this->data[0]['Пользователь'];
        }
        return $userId;
    }

}