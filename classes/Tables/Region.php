<?php


namespace classes\Tables;


use classes\Base\Table;

class Region extends Table
{
	public function __construct($options = [])
	{
		parent::__construct('Регионы', $options);
	}

	public function getRegionIdByDbId($dbID)
	{
		$regionID = "";
		$this->getData(
			[
				'ID'
			],
			[
				'status' => 0,
				'dbID' => '\'' . $dbID . '\''
			]);
		if ($this->num_rows > 0) {
			$regionID = $this->data[0]['ID'];
		}
		return $regionID;
	}
}