<?php


namespace classes\Tables;


use classes\Base\Table;

class Channals extends Table
{
	private $mysqli;
	private $db;

	public function __construct($options = [])
	{
		parent::__construct('Каналы', $options);
	}

	public function SetDB($db)
	{
		$this->db = $db;
	}


	public static function SincChannals($db)
	{
		$channals = new Channals();
		$channals->SetDB($db);
		$channals->Sinc();
	}

	public function Sinc()
	{
		$this->writeLog('sinc channels start');
		$this->getData([], []);
		$this->CheckConections();
		foreach ($this->data as $channal) {
			if ($result = $this->mysqli->query("SELECT ID FROM ad_Channel WHERE CRMID = " . $channal['ID'])) {
				$recource = $result->fetch_array(MYSQLI_ASSOC);
				if ((int)$recource["ID"] > 0) {
					$result->close();
					continue;
				}
				$result->close();
			}
			if ($result = $this->mysqli->query("SELECT ID FROM ad_Channel WHERE Url ='" . $channal['Ссылка'] . "'")) {
				$recource = $result->fetch_array(MYSQLI_ASSOC);
				if ((int)$recource["ID"] > 0) {
					$query = "UPDATE ad_Channel SET ";
					$query .= "Name = '" . $channal["Название"] . "',";
					$query .= "CRMID =" . $channal["ID"] . "";
					$query .= "WHERE ID  = " . (int)$recource["ID"] . ";";
					$this->mysqli->query($query);
					$result->close();
					continue;
				}
				$result->close();
			}
			$query = "INSERT INTO ad_Channel (Name, Url, CRMID) VALUES (";
			$query .= "'" . $channal["Название"] . "',";
			$query .= "'" . $channal["Ссылка"] . "',";
			$query .= $channal['ID'];
			$query .= ");";
			$this->mysqli->query($query);
		}
		$this->CheckConections();
		if ($result = $this->mysqli->query("SELECT ID,URL FROM ad_Channel WHERE CRMID IS NULL; ")) {
			$recources = $result->fetch_all(MYSQLI_ASSOC);
			$update_ids=[];
			foreach ($recources as $recource) {
				$this->getData([], ['f22761 LIKE \''.$recource['URL'].'%\''],[],['raw_condition' =>1]);
				if ($this->data[0]['ID']>0) {
					$update_ids[$recource['ID']] = $this->data[0]['ID'];
				}else{
					$this->data[0]['Ссылка'] = $recource['URL'];
					$update_ids[$recource['ID']] = $this->save();
				}
			}
			$this->writeLog('new channels count:'.count($update_ids));
			foreach ($update_ids as $id =>$update_id) {
				$query = "UPDATE ad_Channel SET ";
				$query .= "CRMID =" . $update_id . " ";
				$query .= " WHERE ID  = " . (int)$id . ";";
				$this->mysqli->query($query);
			}
			$result->close();
		}
		$this->writeLog('sinc channels end');
	}

	private function CheckConections(){
		if(!$this->mysqli || !$this->mysqli->ping()){
			$this->CreateConections();
		}
	}

	private function CreateConections()
	{
		if (!$this->db) {
			return;
		}
		$this->mysqli = mysqli_init();
		if (!$this->mysqli->real_connect($this->db['server'], $this->db['user'], $this->db['pass'], $this->db['db'])) {
			die('Ошибка подключения (' . mysqli_connect_errno() . ') '
				. mysqli_connect_error());
		}
		$this->mysqli->query("SET NAMES 'utf8'");
	}

}