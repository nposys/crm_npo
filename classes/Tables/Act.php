<?php


namespace classes\Tables;


use classes\Base\Table;

class Act extends Table
{
    public function __construct($options = [])
    {
        parent::__construct('Акт', $options);
    }
}