<?php


namespace classes\Tables;


use classes\Base\Table;

class Billings extends Table
{
	public function __construct($options = [])
	{
		parent::__construct('Начисления зарплаты', $options);
	}

	public function getCountWorkDaysInHolidays($date_start, $date_end, $manager_filter)
	{
		$sqlQuery = "SELECT 
            manager_id,
            SUM(sum)                                                  AS sum,
            start_work_date,
            end_work_date,
            count(CASE WHEN sum <> 0.00 THEN 1 END)                   AS count,
            CASE WHEN start_work_date < '$date_start 00:00:00' THEN 1 ELSE 0 END AS full_year

        FROM (
            SELECT 
                SUM(f24281) AS sum,      -- сумма по месяцам
                f489      AS start_work_date,
                f14051      AS end_work_date,
                tab.f20721  AS manager_id
             FROM " . DATA_TABLE . "1251 AS tab
                INNER JOIN
                 " . DATA_TABLE . "46 AS m
             ON m.id = tab.f20721
             WHERE
                tab.f20711 >= '$date_start 00:00:00'
                AND tab.f20711 < '$date_end 00:00:00'
                $manager_filter
    
             GROUP BY manager_id,
                      MONTH(tab.f20711),
                      YEAR(tab.f20711)
        ) subtable
        GROUP BY manager_id;";

		$result = sql_query($sqlQuery);
		$this->processSqlResult($result);
	}
}