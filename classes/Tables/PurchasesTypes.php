<?php


namespace classes\Tables;


use classes\Base\Table;

class PurchasesTypes extends Table
{
	public function __construct($options = [])
	{
		parent::__construct('Типы закупок', $options);
	}

	public function getIdByTitle($title)
	{
		$ID = "";
		$this->getData(
			[
				'ID'
			],
			[
				'status' => 0,
				'Название' => '\'' . $title . '\''
			]);
		if ($this->num_rows > 0) {
			$ID = $this->data[0]['ID'];
		}
		return $ID;
	}
}