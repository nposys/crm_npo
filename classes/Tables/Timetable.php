<?php


namespace classes\Tables;


use classes\Base\Table;

class Timetable extends Table
{
	public function __construct($options = [])
	{
		parent::__construct('Табель', $options);
	}
}