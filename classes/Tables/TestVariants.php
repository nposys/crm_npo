<?php


namespace classes\Tables;


use classes\Base\Table;

class TestVariants extends Table
{
	private $mysqli;
	private $db;

	public function __construct($options = [])
	{
		parent::__construct('Варианты тестов', $options);
	}

	public function SetDB($db)
	{
		$this->db = $db;
	}

	public static function SincTestVariants($db)
	{
		$test_variants = new TestVariants();
		$test_variants->SetDB($db);
		$test_variants->Sinc();
	}

	public function Sinc()
	{
		$this->writeLog('sinc tests start');
		$this->getData([], []);
		$this->CheckConections();
		foreach ($this->data as $test) {
			if ($result = $this->mysqli->query("SELECT ID FROM ad_Test WHERE CRMID = " . $test['ID'])) {
				$recource = $result->fetch_array(MYSQLI_ASSOC);
				if ((int)$recource["ID"] > 0) {
					$result->close();
					continue;
				}
				$result->close();
			}
			$query = "INSERT INTO ad_Test (Identifier, ad_CampaignID, CRMID) VALUES (";
			$query .= "'" . $test["Идентификатор"] . "',";
			$query .= " (SELECT ID FROM ad_Campaign WHERE CRMID = " . $test["Рекламная кампания"] . " LIMIT 1),";
			$query .= $test['ID'];
			$query .= ");";
			$this->mysqli->query($query);
		}
		$this->writeLog('sinc tests end');
	}

	private function CheckConections(){
		if(!$this->mysqli || !$this->mysqli->ping()){
			$this->CreateConections();
		}
	}

	private function CreateConections()
	{
		if (!$this->db) {
			return;
		}
		$this->mysqli = mysqli_init();
		if (!$this->mysqli->real_connect($this->db['server'], $this->db['user'], $this->db['pass'], $this->db['db'])) {
			die('Ошибка подключения (' . mysqli_connect_errno() . ') '
				. mysqli_connect_error());
		}
		$this->mysqli->query("SET NAMES 'utf8'");
	}
}