<?php


namespace classes\Tables;


use classes\Base\Table;

class IncomeInvoiceOrders extends Table
{
    public function __construct( $options = [])
    {
        parent::__construct("Позиции заказы счета поступления", $options);
    }
}