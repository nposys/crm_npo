<?php


namespace classes\Tables;


use classes\Base\Table;

class Resources extends Table
{
	private $mysqli;
	public function __construct($options = [])
	{
		parent::__construct('Ресурсы', $options);
	}

	public function SetMysqli($mysqli)
	{
		$this->mysqli = $mysqli;
	}
}