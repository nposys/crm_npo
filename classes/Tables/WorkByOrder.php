<?php

namespace classes\Tables;

use classes\Base\Table;

class WorkByOrder extends Table
{
    public function __construct($options = [])
    {
        parent::__construct('Работа по заказам', $options);
    }

    /**
     * Проставить менеджера производства
     * @param int $taskId
     */
    public function setWorkManagerForTask(int $taskId)
    {
        if($taskId>0) {
            $userId = $this->chooseNextWorkManager($taskId);

            $this->getDataById($taskId);
            if($this->num_rows > 0){
                if($userId > 0){
                    $this->updateDataById($taskId,
                        ["Менеджер" => $userId,
                            "Назначить производство" => ""]);
                }
            }
        }
    }

    /**
     * Авто вычисление менеджера на которого назначить задачу по заказам
     * @param int $workId
     * @return int $userId - ID пользователя на которого предлагается назначить задачу
     */
    public function chooseNextWorkManager(int $workId) : int
    {
        $userId = 0;

        if($workId > 0){
            // получаем данные о задаче
            $this->getDataById($workId);
            $priceId = (int)$this->data[0]['Позиция прайс-листа'];

            $exec = new Table("Исполнение");
            $exec->getData(
                ["Менеджер", "Приоритет"],
                ["Позиция прайс-листа" => $priceId],
                [["field_name"=>"Приоритет", "direction"=>"ASC"]]
            );
            $managerAllowedForWork = $exec->data;
            $managerAllowedForWorkId = array_column($exec->data, 'Менеджер');

            // получаем сведения о работающих в указанную дату согласно табелю сотрудниках
            $personal = new Personal();

            $now = date("Y-m-d H:i:s");
            $availableManagers = $personal->getAvailableManagersByDate($now, Personal::DEPARTMENT_USLUGI_TYPOVIE);
            $availableManagersId = array_column($availableManagers, 'ID');

            // пересечем массивы сотрудников допущенных к виду работ и находящихся на рабочем месте
            $filteredManagersId = array_intersect($availableManagersId, $managerAllowedForWorkId);

            // получаем сведения о загрузке тех кто может работать над заказом
            $currentManagersWorkLoad = $this->getManagersWorkLoad($filteredManagersId);

            // подготовим массив для алгоритма выбора
            $managers = [];
            foreach($managerAllowedForWork as $item) {
                $id = (int) $item["Менеджер"];
                $priority = (int) $item["Приоритет"];

                if(!in_array($id, $availableManagersId )){
                    continue;
                }

                $key = array_search($id, array_column($currentManagersWorkLoad, 'PersonId'));
                $count_works = 0;
                $remain_price_duration = 0;
                $work_type_duration = 0;
                $price_duration = 0;
                if($key !== FALSE){
                    $count_works = $currentManagersWorkLoad[$key]["CountWorks"];
                    $remain_price_duration = $currentManagersWorkLoad[$key]["RemainPriceDuration"];
                    $work_type_duration = $currentManagersWorkLoad[$key]["WorkTypeDurationSum"];
                    $price_duration = $currentManagersWorkLoad[$key]["PriceDurationSum"];
                }

                $managers[] = [
                    "id" => $id,
                    "priority" => $priority,
                    "count_works" => $count_works,
                    "remain_price_durations" => $remain_price_duration,
                    "work_type_duration" => $work_type_duration,
                    "price_duration" => $price_duration
                ];
            }

            usort($managers, function ($item1, $item2) {
                return $item1['work_type_duration'] <=> $item2['work_type_duration'];
            });

            // рекомендованный менеджер
            $userId = $personal->getUserIdByManagerId((int)$managers[0]['id']);
        }

        return $userId;
    }

    /**
     * @param string $mode
     * @param int $interval - how many years of history data
     * @return int
     */
    public function updateAverageWorkDurations(string $mode, int $interval = 5) : int
    {
        $method = "";
        $table = "";
        $recordIdField = "";
        $recordNameField = "";

        switch ($mode){
            case "price_type":
                $method = "getSqlForWorkDurationStatisticsByPriceType";
                $table = "Прайс-лист";
                $recordIdField = "PriceId";
                $recordNameField = "PriceName";
                break;
            case "work_type":
                $method = "getSqlForWorkDurationStatisticsByWorkType";
                $table = "Типы работ";
                $recordIdField = "WorkTypeId";
                $recordNameField = "WorkType";
                break;
        }

        if ($method === "") {
            return 0;
        }

        $dateFrom = date("Y-01-01", strtotime(date("Y-m-d")." - $interval year"));
        $query = $this->{$method}($dateFrom);
        $this->writeLog("================================");
        $this->writeLog("Mode:$mode. Update data from: ".$dateFrom);

        // обновляем нормировку в прайс-листе
        $targetTable = new Table($table);
        $currentTime = date("Y-m-d H:i:s");

        $result = sql_query($query);
        while ($row = sql_fetch_assoc($result)) {
            $num = (int)$row['NumRecords'];
            $averageDuration = round((float)$row['AvgDuration']);
            $standardDeviation = (float)$row['StandardDeviation'];
            $serviceId = (int)$row['ServiceId'];
            $serviceName = $row['ServiceName'];
            $recordId = (int)$row[$recordIdField];
            $recordName = $row[$recordNameField];

            // $this->writeLog("OUT: ". $serviceName." / ($recordId)".$recordName);

            // не берем те данные где слишком мало примеров
            if($num >= 5){
                // пересчитываем среднее по конкретной услуге с учетом стандартного отклонения

                // вычисляем уточненные средние с учетом 2 сигма (2 стандартных отклонения от среднего, чтобы убрать выбросы значений)
                $queryInner = $this->{$method}(
                    $dateFrom, $serviceId, $recordId, $averageDuration, $standardDeviation);
                $resultInner = sql_query($queryInner);
                if ($rowInner = sql_fetch_assoc($resultInner)) {
                    $numInner = (int)$rowInner['NumRecords'];
                    $averageInner = round((float)$rowInner['AvgDuration']);

                    $resultUpdate = $targetTable->updateDataById($recordId,
                        [
                            'Базовая трудоемкость' => $averageInner,
                            'Дата обновления трудоемкости' => $currentTime
                        ]);

                    if($resultUpdate === 0){
                        $this->writeLog($serviceName." / ".$recordName. " update failed ");
                    }

                    $this->writeLog($serviceName." / ($recordId) ".$recordName.
                        " : avg_old:". $averageDuration ." avg_new:".$averageInner.
                        " || ".
                        " num_old:$num num_new:$numInner");
                }
            } else {
                $this->writeLog($serviceName." / ".$recordName.
                    " too few data [$num]");
            }
        }

        return 1;
    }

    /**
     * Возвращает SQL запрос для выборки статистики трудоемкости по отделу типовых услуг по позициям прайса
     * @param string $dateFrom
     * @param int $serviceId
     * @param int $priceId
     * @param float $avg
     * @param float $std
     * @param float $num_std
     * @return string
     */
    private function getSqlForWorkDurationStatisticsByPriceType(
        string $dateFrom,
        int $serviceId = 0,
        int $priceId = 0,
        float $avg = 0.0,
        float $std = 0.0,
        float $num_std = 2.0) : string
    {
        $whereServiceId = "";
        if($serviceId > 0){
            $whereServiceId = " AND service.id=$serviceId ";
        }

        $wherePriceId = "";
        if($priceId > 0){
            $wherePriceId = " AND price.id=$priceId ";
        }

        if($avg > 0.0 && $std > 0.0){
            $min = $avg - $num_std * $std;
            $max = $avg + $num_std * $std;
            $whereIntervals = " BETWEEN $min and $max ";
        } else {
            $whereIntervals = " > 0 ";
        }

        return "
            select
                tt.ServiceId,
                tt.ServiceName,
                tt.PriceId,
                tt.PriceName,
                avg(tt.SumFactProdDuration) as AvgDuration,
                stddev(tt.SumFactProdDuration) as StandardDeviation,
                count(tt.OrderId) as NumRecords
            from (
                     select ord.id        as OrderId,
                            service.id    as ServiceId,
                            service.f1158 as ServiceName,
                            price.id      as PriceId,
                            price.f8510   as PriceName,
                            (select sum(ord_work_sub.f19551)
                             from cb_data751 as ord_work_sub
                                      left join cb_data46 as pers on pers.f1400 = ord_work_sub.f12661
                             where 1 = 1
                               and ord_work_sub.f12601 = ord.id
                               and pers.f12021 = 8 -- отдел
                            )             as SumFactProdDuration
                     from cb_data271 as ord
                              left join cb_data91 as service on service.id = ord.f4451
                              left join cb_data580 as price on price.id = ord.f13071
                     where 1 = 1
                       $whereServiceId
                       $wherePriceId
                       and ord.add_time >= '$dateFrom'
                       and ord.status = 0
                       and ord.f6551 IN ('30. Завершено')
                       and (select sum(ord_work_sub.f19551)
                            from cb_data751 as ord_work_sub
                                     left join cb_data46 as pers on pers.f1400 = ord_work_sub.f12661
                            where 1 = 1
                              and ord_work_sub.f12601 = ord.id
                              and pers.f12021 = ".Personal::DEPARTMENT_USLUGI_TYPOVIE."  -- отдел типовых услуг
                           ) $whereIntervals
                 ) as tt
            group by
                tt.ServiceId,
                tt.ServiceName,
                tt.PriceId,
                tt.PriceName
            order by
                tt.ServiceName,
                tt.PriceName
            ";
    }

    /**
     * Возвращает SQL запрос для выборки статистики трудоемкости по отделу типовых услуг по видам работ
     * @param $dateFrom
     * @param int $serviceId
     * @param int $workTypeId
     * @param float $avg
     * @param float $std
     * @param float $num_std
     * @return string
     */
    private function getSqlForWorkDurationStatisticsByWorkType($dateFrom, $serviceId = 0, $workTypeId = 0, $avg = 0.0, $std = 0.0, $num_std = 2.0) : string
    {
        $whereServiceId = "";
        if($serviceId > 0){
            $whereServiceId = " AND service.id=$serviceId ";
        }

        $whereWorkTypeId = "";
        if($workTypeId > 0){
            $whereWorkTypeId = " AND work_type.id=$workTypeId ";
        }

        if($avg > 0.0 && $std > 0.0){
            $min = $avg - $num_std*$std;
            $max = $avg + $num_std*$std;
            $whereIntervals = " BETWEEN $min and $max ";
        } else {
            $whereIntervals = " > 0 ";
        }

        return "
            select
                tt.ServiceId,
                tt.ServiceName,
                tt.WorkTypeId,
                tt.WorkType,
                avg(WorkDurationFact) as AvgDuration,
                stddev(WorkDurationFact) as StandardDeviation,
                count(WorkId) as NumRecords
            from
                (
                    select
                        ord_work.id as WorkId,
                        service.id as ServiceId,
                        service.f1158 as ServiceName,
                        work_type.id as WorkTypeId,
                        work_type.f12771 as WorkType, -- work type name
                        ord_work.f19551 as WorkDurationFact
                    from
                         cb_data751 as ord_work
                            left join cb_data761 as work_type on work_type.id = ord_work.f12861
                            left join cb_data91 as service on service.id = ord_work.f12611
                            left join cb_data580 as price on price.id = ord_work.f14481
                            left join cb_data46 as pers on pers.f1400 = ord_work.f12661
                    where 1=1
                      $whereServiceId
                      $whereWorkTypeId
                      and ord_work.add_time >= '$dateFrom'
                      and ord_work.status = 0
                      and ord_work.f12631 IN ('30. Завершено') -- статус работы по заказу
                      and ord_work.f12611 = work_type.f12761   -- фильтруем ошибочные строки где Услуга в типе работ не совпадет с услугой в Работе
                      and pers.f12021 = ".Personal::DEPARTMENT_USLUGI_TYPOVIE."  -- отдел типовых услуг
                      and ord_work.f19551 $whereIntervals -- трудоемкость факт
                ) as tt
            group by
                tt.ServiceId,
                tt.ServiceName,
                tt.WorkTypeId,
                tt.WorkType
            order by
                tt.ServiceName,
                tt.WorkType
        ";
    }

    public function getManagersWorkLoad(array $managers = []) : array
    {
        $managers_load = [];

        $query = $this->getSqlForCurrentWorkLoad($managers);
        $result = sql_query($query);
        while($row = sql_fetch_assoc($result)){
            $managers_load[] = $row;
        }

        return $managers_load;
    }

    private function getSqlForCurrentWorkLoad(array $managers = []) : string
    {
        $managersString = "";
        if(count($managers) > 0){
            $managersString =
                " and pers.id IN (".implode(",", $managers).") ";
        }

        return "
            select
                tt.PersonId,
                tt.Fio,
                sum(tt.BWD) WorkTypeDurationSum,
                sum(tt.BPD) PriceDurationSum,
                sum(tt.RemainFix) as RemainPriceDuration,
                sum(tt.CountWorks) as CountWorks
            from
                (
                    select
                        pers.id as PersonId,
                        pers.f483 as Fio,
                        ord.id as OrderId,
                        service.id as ServiceId,
                        service.f1158 as ServiceName,
                        price.id as PriceId,
                        price.f8510 as PriceName,
                        price.f24421 as BPD,
                        sum(work_type.f15511) as BWD,
                        ord_work.f12631, -- work status
                        work_type.f12771, -- work type name
                        dur_fact.curDur,
                        CASE
                            WHEN price.f24421 - dur_fact.curDur > 0
                                THEN price.f24421 - dur_fact.curDur
                            ELSE 0
                        END as RemainFix,
                        count(*) As CountWorks
                    from
                        cb_data751 as ord_work
                            left join cb_data761 as work_type on work_type.id = ord_work.f12861
                            left join cb_data271 as ord on ord.id = ord_work.f12601
                            left join cb_data91 as service on service.id = ord.f4451
                            left join cb_data580 as price on price.id = ord.f13071
                            left join cb_data46 as pers on pers.f1400 = ord_work.f12661
                            left join (
                            select
                                ord_sub.id as orderId,
                                sum(ord_work_sub.f19551) as curDur
                            from
                                cb_data751 as ord_work_sub
                                    left join cb_data271 as ord_sub
                                        on ord_sub.id = ord_work_sub.f12601
                            where
                                    ord_sub.f6551 IN (
                                                      '05. Оценка',
                                                      '10. Исполнение',
                                                      '20. Предоплата',
                                                      '25. Ожидание оплаты',
                                                      '27. Ожидание акта')
                            group by
                                ord_sub.id
                        ) as dur_fact on ord.id = dur_fact.orderId
                    where
                            1=1
                      and ord_work.status = 0
                      and ord_work.f12631 IN ('10. Запланировано',
                                              '20. В работе') -- статус
                      and ord.f6551 NOT IN ('30. Завершено',
                                            '97. Не исполнено',
                                            '98. Отказ',
                                            '99. Плохой',
                                            '110. Заморожено')
                      and ord.f4451 NOT IN (4) -- service_id 4=поиск
                      and pers.f12021 = 8 -- отдел
                        $managersString    
                    group by
                        ord.id,
                        ord_work.f12631,
                        work_type.f12771,
                        pers.f483   -- user
                    order by
                        pers.f483, ord.id
                ) as tt
            group by
                tt.PersonId, tt.Fio
        ";
    }

}