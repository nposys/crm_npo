<?php


namespace classes\Tables;


use classes\Base\Table;

class OrdersSearch extends Table
{
    public function __construct($options = [])
    {
        parent::__construct("Поиск", $options);
    }
}