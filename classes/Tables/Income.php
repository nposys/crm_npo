<?php
/**
 * Created by PhpStorm.
 * User: Alt
 * Date: 26.02.2020
 * Time: 16:02
 */

namespace classes\Tables;

use classes\Base\Table;

class Income extends Table
{
    public function __construct($options = [])
    {
        parent::__construct('Поступления', $options);
    }
}