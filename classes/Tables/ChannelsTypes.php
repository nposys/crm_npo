<?php


namespace classes\Tables;


use classes\Base\Table;

class ChannelsTypes extends Table
{
	private $mysqli;
	private $db;

	public function __construct($options = [])
	{
		parent::__construct('Типы каналов', $options);
	}

	public function SetDB($db)
	{
		$this->db = $db;
	}


	public static function SincChannelsTypes($db)
	{
		$channels_types = new ChannelsTypes();
		$channels_types->SetDB($db);
		$channels_types->Sinc();
	}

	public function Sinc()
	{
		$this->writeLog('sinc channels types start');
		$this->getData([], []);
		$this->CheckConections();
		foreach ($this->data as $channels_type) {
			if ($result = $this->mysqli->query("SELECT ID FROM ad_CampaignType WHERE CRMID = " . $channels_type['ID'])) {
				$recource = $result->fetch_array(MYSQLI_ASSOC);
				if ((int)$recource["ID"] > 0) {
					$result->close();
					continue;
				}
				$result->close();
			}
			$query = "INSERT INTO ad_CampaignType (Name, CRMID) VALUES (";
			$query .= "'" . $channels_type["Название"] . "',";
			$query .= $channels_type['ID'];
			$query .= ");";
			$this->mysqli->query($query);
		}
		$this->writeLog('sinc channels types end');
	}

	private function CheckConections(){
		if(!$this->mysqli || !$this->mysqli->ping()){
			$this->CreateConections();
		}
	}
	private function CreateConections()
	{
		if (!$this->db) {
			return;
		}
		$this->mysqli = mysqli_init();
		if (!$this->mysqli->real_connect($this->db['server'], $this->db['user'], $this->db['pass'], $this->db['db'])) {
			die('Ошибка подключения (' . mysqli_connect_errno() . ') '
				. mysqli_connect_error());
		}
		$this->mysqli->query("SET NAMES 'utf8'");
	}

}