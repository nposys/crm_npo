<?php


namespace classes\Tables;


use classes\Base\Table;

class ClientData extends Table
{
    public function __construct($options = [])
    {
        parent::__construct('Данные клиента', $options);
    }
}