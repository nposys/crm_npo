<?php


namespace classes\Tables;


use classes\Base\Table;

class AdvertisingCampaignsJournal extends Table
{

	private $mysqli;
	private $db;
	public function __construct($options = [])
	{
		parent::__construct('Журнал рекламных кампаний', $options);
	}

	public function SetDB($db)
	{
		$this->db = $db;
	}

	public static function SincJournals($resource,$db)
	{
		$journal = new AdvertisingCampaignsJournal();
		$journal->SetDB($db);
		$journal->Sinc();
	}
	public function Sinc($resource)
	{
		$this->writeLog('sinc journal start');
		$this->CheckConections();

		if($resource = 1){
			$query = "SELECT 
					    j.ID,
					    UserID,
					    c.CRMID as campaign_id,
					    ch.CRMID as channel_id,
					    t.CRMID as test_id,
					    j.Referer,
					    j.EntryPage,
                        j.CreateDate as create_date,
                       u.CreateDate as user_create
					FROM
					    ad_Journal AS j
					        LEFT JOIN
					    ad_Campaign AS c ON c.ID = j.ad_CampaignID
					        LEFT JOIN
					    ad_Channel AS ch ON ch.ID = j.ad_ChannelID
					        LEFT JOIN
					    ad_Test AS t ON t.ID = j.ad_TestID
					        LEFT JOIN
					    ad_User AS u ON u.ID = j.UserID
					WHERE
					    j.CRMID IS NULL
					        AND j.ad_CampaignID IS NOT NULL
					      --  AND j.ad_TestID IS NOT NULL
					        AND (j.UserID IS NOT NULL
        						OR j.ad_ChannelID IS NOT NULL)";
		}else if($resource = 2){
			$query = "SELECT 
					    j.ID,
					    UserID,
					    c.CRMID as campaign_id,
					    ch.CRMID as channel_id,
					    t.CRMID as test_id,
					    j.Referer,
					    j.EntryPage,
                        j.CreateDate as create_date,
                        from_unixtime(du.created) as user_create
					FROM
					    ad_Journal AS j
					        LEFT JOIN
					    ad_Campaign AS c ON c.ID = j.ad_CampaignID
					        LEFT JOIN
					    ad_Channel AS ch ON ch.ID = j.ad_ChannelID
					        LEFT JOIN
					    ad_Test AS t ON t.ID = j.ad_TestID
					        LEFT JOIN
					    Users AS u ON u.ID = j.UserID
					        LEFT JOIN
					    n_Logins AS l ON l.Email = u.Login
					        LEFT JOIN
					    drupal.users AS du ON du.uid = l.DrupalID
					WHERE
					    j.CRMID IS NULL
					        AND j.ad_CampaignID IS NOT NULL
					        AND j.ad_TestID IS NOT NULL
					        AND (j.UserID IS NOT NULL
        						OR j.ad_ChannelID IS NOT NULL)";
		}
		if ($result = $this->mysqli->query($query)) {
			$recources = $result->fetch_all(MYSQLI_ASSOC);
			$update_ids=[];
			$company = new Company();
			$journal = new AdvertisingCampaignsJournal();
			foreach ($recources as $recource) {
				if($resource = 1) {
					$journal->getData([],['NPOSYSID'=>$recource['ID']]);
					$company->getData([], ['NPOSYSID' => $recource['UserID']]);
				}else if($resource = 2) {
					$journal->getData([],['TMID'=>$recource['ID']]);
				}
				if((int)$recource['UserID']>0) {
					if($resource = 1) {
						$company->getData([], ['idClientNposys' => $recource['UserID']]);
					}else if($resource = 2) {
						$company->getData([], ['idClientTenmon' => $recource['UserID']]);
					}
				}else{
					$company->data[0]['ID'] = 0;
				}
				if((int)$company->data[0]['ID']>0 || (int)$recource['UserID']==0){
					$journal->data[0]['Рекламная компания'] = (int)$recource['campaign_id'];
					$journal->data[0]['Вариант теста'] = (int)$recource['test_id'];
					$journal->data[0]['Клиент'] = (int)$company->data[0]['ID']>0 ? (int)$company->data[0]['ID'] : null;
					$journal->data[0]['Канал'] = (int)$recource['channel_id']>0 ? (int)$recource['channel_id'] : null;
					$journal->data[0]['Referer'] = $recource['Referer'];
					$journal->data[0]['Страница входа'] = $recource['EntryPage'];
					$journal->data[0]['TMID'] = $recource['ID'];
					$journal->data[0]['Дата события'] = $recource['create_date'];
					$journal->data[0]['Дата регистрации пользователя'] = $recource['user_create'];
					if((int)$journal->data[0]['ID']>0){
						$update_ids[$recource['ID']] = (int)$journal->data[0]['ID'];
						$journal->save();
					}else {
						$update_ids[$recource['ID']] = $journal->save();
					}
				}
			}
			$this->writeLog('new journal rows:'.count($update_ids));
			foreach ($update_ids as $id =>$update_id) {
				if((int)$id>0) {
					$query = "UPDATE ad_Journal SET ";
					$query .= "CRMID =" . $update_id . " ";
					$query .= " WHERE ID  = " . (int)$id . ";";
					$this->mysqli->query($query);
				}
			}
			$result->close();
		}
		$this->writeLog('sinc journal end');
	}

	private function CheckConections(){
		if(!$this->mysqli || !$this->mysqli->ping()){
			$this->CreateConections();
		}
	}

	private function CreateConections()
	{
		if (!$this->db) {
			return;
		}
		$this->mysqli = mysqli_init();
		if (!$this->mysqli->real_connect($this->db['server'], $this->db['user'], $this->db['pass'], $this->db['db'])) {
			die('Ошибка подключения (' . mysqli_connect_errno() . ') '
				. mysqli_connect_error());
		}
		$this->mysqli->query("SET NAMES 'utf8'");
	}
}