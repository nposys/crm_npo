<?php


namespace classes\Tables;


use classes\Base\Table;

class EmployeePositions extends Table
{
	public function __construct($options = [])
	{
		parent::__construct('Должности сотрудника', $options);
	}

	public function getEmployeeMotivation($manager_id)
	{
		$manager_filter = '';
		if ($manager_id > 0) {
			$manager_filter = " AND employee_history.f22321 = $manager_id ";
		}
		// таблица 1371     - история должностей сотрудников
		// таблица 1281     - системы мотивации для должностей сотрудников
		$sqlQuery = "SELECT 
                employee_history.f22321 manager_id,
                f22331 AS employee_history_start,
                f22341 AS employee_history_end,
                motivation.id as motivation_id,
                f21351 AS motivation_start,
                f21361 AS motivation_end,
                f21301 AS motivation_type,
                f21311 AS motivation_base,
                f21321 AS motivation_base_salary,
                f21331 AS motivation_border
            FROM
                " . DATA_TABLE . "1371 AS employee_history
                    INNER JOIN
                        " . DATA_TABLE . "1281 AS motivation 
                            ON motivation.f21291 = employee_history.f24011 OR motivation.f21291 = 0
            WHERE
                (motivation.f21351 <= employee_history.f22341
                    OR employee_history.f22341 = '0000-00-00 00:00:00')
                    AND (motivation.f21361 >= employee_history.f22331
                    OR motivation.f21361 = '0000-00-00 00:00:00')
                    $manager_filter
            ORDER BY employee_history.id DESC , motivation.id DESC";

		$result = sql_query($sqlQuery);
		$this->processSqlResult($result);
	}
}