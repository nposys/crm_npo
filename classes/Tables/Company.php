<?php


namespace classes\Tables;


use classes\Base\Table;

class Company extends Table
{
	public function __construct($options = [])
	{
		parent::__construct('Контрагенты', $options);
	}


    /**
     * Получить id и дату первого заказа из группы компаний
     */
	public function getFirstOrderByClientGroup($clientId)
    {
        $func_res = ["id"=>0, "date"=>""];
        if($clientId >0 ){
            $client = new self();
            $client->getDataById($clientId);

            $goc_id = (int)$client->data[0]['Группа компаний'];
            if($goc_id>0){
                // получаем данные по всем клиентам из группы компаний
                $query = "
                    SELECT 
                        MIN(ord.id) as MinId,
                        MIN(ord.f6591) as MinDate                        
                    FROM 
                      cb_data42 as cmp
                        LEFT JOIN cb_data271 as ord on ord.f4441 = cmp.id and ord.status = 0
                    WHERE 
                      1=1 
                      and cmp.status = 0
                      and cmp.f6301 = $goc_id
                  ";

                  $res = sql_query($query);
                  if($row = sql_fetch_assoc($res)){
                      $func_res['id'] = $row['MinId'];
                      $func_res['date'] = $row['MinDate'];
                  }
            }else{
                // получаем данные по текущему клиенту
                $query = "
                    SELECT 
                        MIN(ord.id) as MinId,
                        MIN(ord.f6591) as MinDate                        
                    FROM 
                      cb_data42 as cmp
                          LEFT JOIN cb_data271 as ord on ord.f4441 = cmp.id and ord.status = 0
                    WHERE 
                      1=1 
                      and cmp.status = 0
                      and cmp.id = $clientId
                  ";

                $res = sql_query($query);
                if($row = sql_fetch_assoc($res)){
                    $func_res['id'] = $row['MinId'];
                    $func_res['date'] = $row['MinDate'];
                }
            }
        }

        return $func_res;
    }

    /**
     * Получить id и дату первого события по работе с Контрагентом
     * @param $clientId
     * @return array
     */
    public function getFirstEventByClientGroup($clientId)
    {
        $func_res = ["id"=>0, "date"=>""];
        if($clientId >0 ){
            $client = new self();
            $client->getDataById($clientId);

            $goc_id = (int)$client->data[0]['Группа компаний'];
            if($goc_id>0){
                // получаем данные по всем клиентам из группы компаний
                $query = "
                    SELECT 
                        MIN(evt.id) as MinId,
                        MIN(evt.f724) as MinDate                        
                    FROM 
                      cb_data42 as cmp
                        LEFT JOIN cb_data62 as evt on evt.f723 = cmp.id and evt.status = 0
                    WHERE 
                      1=1 
                      and cmp.status = 0
                      and cmp.f6301 = $goc_id
                  ";

                $res = sql_query($query);
                if($row = sql_fetch_assoc($res)){
                    $func_res['id'] = $row['MinId'];
                    $func_res['date'] = $row['MinDate'];
                }
            }else{
                // получаем данные по текущему клиенту
                $query = "
                    SELECT 
                        MIN(evt.id) as MinId,
                        MIN(evt.f724) as MinDate                        
                    FROM 
                      cb_data42 as cmp
                        LEFT JOIN cb_data62 as evt on evt.f723 = cmp.id and evt.status = 0
                    WHERE 
                      1=1 
                      and cmp.status = 0
                      and cmp.id = $clientId
                  ";

                $res = sql_query($query);
                if($row = sql_fetch_assoc($res)){
                    $func_res['id'] = $row['MinId'];
                    $func_res['date'] = $row['MinDate'];
                }
            }
        }

        return $func_res;
    }

    public function getNumEventsByClientGroup($clientId)
    {
        $func_res = 0;
        if($clientId >0 ){
            $client = new self();
            $client->getDataById($clientId);

            $goc_id = (int)$client->data[0]['Группа компаний'];
            if($goc_id>0){
                // получаем данные по всем клиентам из группы компаний
                $query = "
                    SELECT 
                        COUNT(evt.id) as CountId
                    FROM 
                      cb_data42 as cmp
                        LEFT JOIN cb_data62 as evt on evt.f723 = cmp.id and evt.status = 0
                    WHERE 
                      1=1 
                      and cmp.status = 0
                      and cmp.f6301 = $goc_id
                  ";

                $res = sql_query($query);
                if($row = sql_fetch_assoc($res)){
                    $func_res = (int)$row['CountId'];
                }
            }else{
                // получаем данные по текущему клиенту
                $query = "
                    SELECT 
                        COUNT(evt.id) as CountId
                    FROM 
                      cb_data42 as cmp
                        LEFT JOIN cb_data62 as evt on evt.f723 = cmp.id and evt.status = 0
                    WHERE 
                      1=1 
                      and cmp.status = 0
                      and cmp.id = $clientId
                  ";

                $res = sql_query($query);
                if($row = sql_fetch_assoc($res)){
                    $func_res = (int)$row['CountId'];
                }
            }
        }
        return $func_res;
    }

}