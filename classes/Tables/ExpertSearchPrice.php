<?php


namespace classes\Tables;


use classes\Base\Table;

class ExpertSearchPrice extends Table
{
	private $mysqli;
	public function __construct($options = [])
	{
		parent::__construct('Экспертный поиск - цены', $options);
	}

	public function UploadCost($id){
		if((int)$id>0){
			$search = new ExpertSearchPrice(['active'=>false]);
			$search->getDataById((int)$id);
			$data_size = $search->data[0]['Размер тематики'];
			$data_price_type = 1;
			if( $search->data[0]['Тип']=='Фильтрация по карточке'){
				$data_price_type = 2;
			}else if( $search->data[0]['Тип']=='Фильтрация по документации'){
				$data_price_type = 3;
			}
			$data_price = $search->data[0]['Цена'];
			$data_date_start = $search->data[0]['Дата начала действия'];
			$data_is_deleted = $search->data[0]['Действующее']=='Да' && $search->data[0]['Статус записи']==0 ? 0:1;
			$query = "";
			if($this->CheckTenmonPrice($id)){
				$query = "UPDATE es_Price SET ";
				$query .="Size = $data_size,";
				$query .="PriceTypeId = $data_price_type,";
				$query .="Price = $data_price,";
				$query .="DateStart = '$data_date_start',";
				$query .="IsDeleted =$data_is_deleted";
				$query .=" WHERE ID = $id";
			}else{
				$query = "INSERT INTO es_Price (ID, Size, PriceTypeId, Price, DateStart, IsDeleted) VALUES (";
				$query .="$id,";
				$query .="$data_size,";
				$query .="$data_price_type,";
				$query .="$data_price,";
				$query .="'$data_date_start',";
				$query .="$data_is_deleted";
				$query .=");";
			}
			$this->mysqli->query($query);
		}
	}
	public function UploadCosts(){
		$this->getData(['ID'],['status'=>0]);
		$this->CheckTenmonConections();
		$this->mysqli->query("UPDATE es_Price SET IsDeleted = 1");
		foreach ($this->data as $data){
			$this->UploadCost($data['ID']);
		}
	}
	private function CreateTenmonConections(){
		require_once 'add/db.php';
		$this->mysqli = mysqli_init();
		if (!$this->mysqli->real_connect($databases['TM']['server'], $databases['TM']['user'], $databases['TM']['pass'], $databases['TM']['db'])) {
			die('Ошибка подключения (' . mysqli_connect_errno() . ') '
				. mysqli_connect_error());
		}
		$this->mysqli->query("SET NAMES 'utf8'");
	}
	private function CheckTenmonConections(){
		if(!$this->mysqli){
			$this->CreateTenmonConections();
		}
	}
	private function CheckTenmonPrice($id){
		$result = false;
		$this->CheckTenmonConections();
		if($result_tm = $this->mysqli->query("SELECT id FROM es_Price WHERE ID = $id")){
			$ptice = $result_tm->fetch_array(MYSQLI_ASSOC);
			if((int)$ptice["id"]>0){
				$result = true;
			};
			$result_tm->close();
		}
		return $result;
	}
}