<?php


namespace classes\Tables;


use classes\Base\Table;

class TradingPlatform extends Table
{
	public function __construct($options = [])
	{
		parent::__construct('Торговые площадки', $options);
	}

	public function getIdByCode($code)
	{
		$ID = "";
		if (trim($code) !== "") {
			$this->getData(
				[
					'ID'
				],
				[
					'status' => 0,
					'Код' => '\'' . $code . '\''
				]);
			$ID = $this->data[0]['ID'];
		}
		return $ID;
	}
}