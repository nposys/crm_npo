<?php


namespace classes\Tables;


use classes\Base\Table;
use classes\Enums\CrmEnums\Positions;

class PositionsOrdersInvoices extends Table
{
	public function __construct($options = [])
	{
		parent::__construct('Позиции заказы счета поступления', $options);
	}

	public function getSalesManagerInvoices($manager_position, $manager_id, $start_date, $finish_date)
	{
		$manager_filter = $this->getManagerFilter($manager_position, $manager_id);
		$query = 'SELECT
                  tiio.id
                 ,tcmp.id AS "ID Клиента"
                 ,tinc.id AS "ID Счета"
                 ,tinc.f7511 AS "Номер счета"
                 ,tord.id AS "ID Заказа"
                 ,tord.f7071 AS "Номер заказа"
                 ,tst.f1158 AS "Вид услуги"
                 ,tord.f4431 AS "Описание"
                 ,tiio.f10920
                 ,tiio.f11271
                 ,tiio.f11291
                 ,tiio.f11281
                 ,tcd.f13811 AS "Дата первого поступления"
                 ,tcmp.f435 AS "Название клиента"
                 ,tsrc.f435 AS "Название клиента плательщика"
                 ,tsrc.f772 AS "Тип клиента плательщика"
                 ,tpers.f483 AS "Фамилия Инициалы"
                 ,tiio.f11251
                 ,tiio.f13961 AS "Тип поступления"
               FROM
                 ' . DATA_TABLE . 650 . ' AS tiio
                 LEFT JOIN ' . DATA_TABLE . 271 . ' AS tord ON tord.id = tiio.f10890 AND tord.status = 0
                 LEFT JOIN ' . DATA_TABLE . 91 . ' AS tst ON tst.id = tord.f4451 AND tst.status = 0
                 LEFT JOIN ' . DATA_TABLE . 42 . ' AS tcmp ON tcmp.id = tord.f4441 AND tcmp.status = 0
                 LEFT JOIN ' . DATA_TABLE . 630 . ' AS tcd ON tcd.f10410 = tord.f4441 AND tcd.status = 0
                 LEFT JOIN ' . DATA_TABLE . 511 . ' AS tinc ON tinc.id = tiio.f10910 AND tinc.status = 0
                 LEFT JOIN ' . DATA_TABLE . 42 . ' AS tsrc ON tsrc.id = tinc.f7531 AND tsrc.status = 0
                 LEFT JOIN ' . DATA_TABLE . 46 . ' AS tpers ON tpers.f1400 = tiio.f11251 AND tpers.status = 0 AND tiio.f11251 <> \'\'
               WHERE
                 DATE(tiio.f11271) BETWEEN \'' . $start_date . '\' AND \'' . $finish_date . '\'
                 AND tiio.status = 0 AND tiio.f11251 <> \'\'
                 ' . $manager_filter . '
               ORDER BY
                  tiio.f11251, tiio.f11271
                ';

		$result = sql_query($query);
		$this->processSqlResult($result);
	}

	public function getClientsManagerInvoices($manager_position, $manager_id, $start_date, $finish_date)
	{
		$manager_filter = $this->getManagerFilter($manager_position, $manager_id);

		if (isset($manager_filter)) {
			$query = 'SELECT
                  tiio.id
                 ,tcmp.id AS "ID Клиента"
                 ,tinc.id AS "ID Счета"
                 ,tinc.f7511 AS "Номер счета"
                 ,tord.id AS "ID Заказа"
                 ,tord.f7071 AS "Номер заказа"
                 ,tord.f4461 AS "Сумма"
                 ,tord.f10270 AS "Оплачено"
                 ,tord.f24101 AS "Учитываемые расходы"
                 ,tord.f24111 AS "Комментарий по расходам"
                 ,tst.f1158 AS "Вид услуги"
                 ,tord.f4431 AS "Описание"
                 ,tiio.f10920
                 ,tiio.f11271
                 ,tiio.f11291
                 ,tiio.f11281
                 ,tord.f13361 AS "Дата исполнения"
                 ,tcmp.f435 AS "Название клиента"
                 ,tsrc.f435 AS "Название клиента плательщика"
                 ,tsrc.f772 AS "Тип клиента плательщика"
                 ,tpers.f483 AS "Фамилия Инициалы"
                 ,tpers.f484 AS "Должность"
                 ,tiio.f11261
                 ,tord.f9470 AS "Менеджер клиента НПО"
                 ,tiio.f18651
               FROM
                 ' . DATA_TABLE . 650 . ' AS tiio
                 LEFT JOIN ' . DATA_TABLE . 271 . ' AS tord ON tord.id = tiio.f10890 AND tord.status = 0
                 LEFT JOIN ' . DATA_TABLE . 91 . ' AS tst ON tst.id = tord.f4451 AND tst.status = 0
                 LEFT JOIN ' . DATA_TABLE . 42 . ' AS tcmp ON tcmp.id = tord.f4441 AND tcmp.status = 0
                 LEFT JOIN ' . DATA_TABLE . 511 . ' AS tinc ON tinc.id = tiio.f10910 AND tinc.status = 0
                 LEFT JOIN ' . DATA_TABLE . 42 . ' AS tsrc ON tsrc.id = tinc.f7531 AND tsrc.status = 0
                 LEFT JOIN ' . DATA_TABLE . 46 . ' AS tpers ON tpers.f1400 = tiio.f11261 AND tpers.status = 0 AND tiio.f11261 <> \'\'
               WHERE
                 DATE(tiio.f11271) BETWEEN \'' . $start_date . '\' AND \'' . $finish_date . '\'
                 AND tiio.status = 0
                 AND tord.f9470 > 0
                 AND tinc.f20771 <> \'Да\'
                 and tcmp.id NOT IN (3813,15105,48212)
                 ' . $manager_filter . ' 
               ORDER BY
                  tiio.f11261 ASC,
                  tiio.f11271 ASC';

			$result = sql_query($query);
			$this->processSqlResult($result);
		}
	}

	private function getManagerFilter($manager_position, $manager_id)
	{
		switch ((int)$manager_position) {
			case 1:
			case Positions::SALES_MANAGER:
				//set manager sales
				$manager_filter = " AND tiio.f11251=" . $manager_id . " ";
				break;
			case 6:
			case Positions::CLIENT_MANAGER:
				//set manager clients
				$manager_filter = " AND tiio.f11261=" . $manager_id . " ";
				break;
			case 2:
				// менеджер по тендерам
				$manager_filter = " AND tow.f12661=" . $manager_id . " ";
				break;
//			case 15:
//              //менеджер-аналитик
//				$managerFilter = " AND tord.f9010='-".$manager_id."-' ";
//				break;
			default:
				return null;
		}
		return $manager_filter;
	}
}