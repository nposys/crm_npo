<?php


namespace classes\Enums\DatabasesEnums\InnerDatabaseEnums\ClientDataTableEnums;


class ClientDataTableFields
{
    const STATUS = 'status';
    const COMPANY_ID = 'f10410';
    const CLIENT_TYPE = 'f12921';
    const CLIENT_STATUS = 'f12941';
    const LEAD_STATUS = 'f14491';
    const DATE_MODIFY = 'f14781';
}