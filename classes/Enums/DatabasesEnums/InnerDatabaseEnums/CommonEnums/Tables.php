<?php


namespace classes\Enums\DatabasesEnums\InnerDatabaseEnums\CommonEnums;


class Tables
{
    const PERSONAL = 46;
    const CLIENTS = 62;
    const EVENTS = 42;
    const CLIENTS_DATA = 630;

    const SIGNATURES = 441;
    const ACCREDITATIONS = 980;

    const LOGINS = 531;
    const SITES = 471;
    const TENDERS = 331;
    const REGIONS = 841;
    const IFNS = 570;
    const UC = 451;

    const ORDERS = 271;
    const ORDERS_ECP = 341;
    const ORDERS_WORK_PLACE = 351;
    const ORDERS_ACCRED = 361;
    const ORDERS_SEARCH = 371;
    const ORDERS_EXPERT_SEARCH = 1351;
    const ORDERS_APP = 381;
    const ORDERS_COMPLAINTS = 391;
    const ORDERS_BG = 411;
    const ORDERS_CMPLX = 401;
    const ORDERS_SRO = 970;

    const ORDERS_MILESTONES = 901;

    const INVOICES = 43;
    const INVOICES_ORDERS = 74;
    const ACTS = 81;
    const INCOMES = 511;
    const INCOMES_INVOICES = 560;
    const INCOME_INVOICE_ORDERS = 650;

    const ORDER_JOBS = 751;
    const JOB_TYPES = 761;
    const SERVICE_TYPES = 91;
    const SALES_PLANS = 771;

    const SERVICE_JOBS = 851;

    const SERVICE_OBJ_CHECKS = 881;
    const SERVICE_OBJ_BUGS = 921;
    const SERVICE_OBJ_GROUPS = 891;

    const MANAGER_EXISTS = 491;

    const JOB_MATRIX = 930;
    const PRICE = 580;

    const TENDER_TYPES = 481;
    const TENDER_SITES = 471;

    const VACANCIES = 691;
    const POSITIONS = 261;
    const DEPARTMENTS = 701;
    const BRANCHES = 661;
}