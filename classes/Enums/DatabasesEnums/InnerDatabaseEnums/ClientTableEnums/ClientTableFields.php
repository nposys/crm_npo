<?php

namespace classes\Enums\DatabasesEnums\InnerDatabaseEnums\ClientTableEnums;

class ClientTableFields
{
    const STATUS = 'status';
    const INN = 'f1056';
    const KPP = 'f1057';
    const LEGAL_TITLE = 'f439';
    const FACT_ADDRESS = 'f440';
    const TITLE = 'f435';
    const POSTAL_ADDRESS = 'f1047';
    const EMAIL = 'f442';
    const PHONE = 'f441';
    const ADDITIONAL_CONTACTS = 'f14821';
    const COMMENT = 'f445';
    const DATE_MODIFY = 'f20481';
    const SOURCE = 'f18761';
    const LEAD_STATUS = 'f552';
    const REGULAR_CLIENT_STATUS = 'f4921';
    const ENTITY = 'f3711';
    const TYPE = 'f772';
    const CATEGORY = 'f11161';
    const CLIENT_MANAGER = 'f8720';
    const SALES_MANAGER = 'f438';
    const REGION = 'f19561';
}