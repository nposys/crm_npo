<?php

namespace classes\Enums\DatabasesEnums\InnerDatabaseEnums\ServiceJobsTableEnums;

class ServiceJobsTableFields
{
    const STATUS = 'status';
    const TYPE = 'f14871';
    const LAST_MAIN_REGION = 'f19571';
    const LAST_SUB_REGION = 'f14881';
}