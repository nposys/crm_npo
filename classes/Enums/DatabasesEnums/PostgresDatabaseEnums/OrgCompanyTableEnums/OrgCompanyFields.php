<?php

namespace classes\Enums\DatabasesEnums\PostgresDatabaseEnums\OrgCompanyTableEnums;

class OrgCompanyFields
{
    const COMPANY_ID = 'orgCompanyId';
    const FULL_TITLE = 'FullTitle';
    const SHORT_TITLE = 'ShortTitle';
    const ORGANIZATION_FORM = 'OrganizationForm';
    const sOkopfID = 'sOkopfID';
    const INN = 'Inn';
    const KPP = 'Kpp';
    const OGRN = 'Ogrn';
    const OKTMO = 'Oktmo';
    const OKPO = 'Okpo';
    const INN_NORMALIZED = 'InnNormalized';
    const LEGAL_ADDRESS = 'LegalAddress';
    const POSTAL_ADDRESS = 'PostalAddress';
    const REAL_ADDRESS = 'RealAddress';
    const EMAIL = 'Email';
    const PHONE = 'Phone';
    const FAX = 'Fax';
    const WEB_SITE = 'WebSite';
    const COMMENT = 'Comment';
    const TIME_ZONE_UTC = 'TimeZoneUtc';
    const DATE_REGISTERED = 'DateRegistered';
    const S_REGION_ID = 'sRegionId';
    const IS_ACTUAL = 'isActual';
    const IS_CUSTOMER = 'isCustomer';
    const IS_SUPPLIER = 'isSupplier';
    const IS_ETP = 'isEtp';
    const EIS_ORGANISATION_CODE = 'EisOrganisationCode';
    const EIS_ORGANISATION_ID = 'EisOrganisationId';
    const S_DISTRICT_ID = 'sDistrictId';
    const IS_FOREIGN = 'isForeign';
    const FTS = 'Fts';
    const HASH_FULL_TITLE = 'HashFullTitle';
    const FTS_FULL = 'FtsFull';
    const KAZ_BIN = 'kazBIN';
    const EXTERNAL_CUSTOMER_ID_MS_SQL = 'ExternalCustomerIdMsSql';
    const EXTERNAL_SUPPLIER_ID_MS_SQL = 'ExternalSupplierIdMsSql';
}