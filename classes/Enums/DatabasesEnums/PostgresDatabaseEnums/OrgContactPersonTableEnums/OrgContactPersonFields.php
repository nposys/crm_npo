<?php

namespace classes\Enums\DatabasesEnums\PostgresDatabaseEnums\OrgContactPersonTableEnums;


class OrgContactPersonFields
{
    const PERSON_ID = 'orgContactPersonId';
    const FULL_NAME = 'FullName';
    const PHONE = 'Phone';
    const FAX = 'Fax';
    const EMAIL = 'Email';
    const POSITION = 'Position';
    const COMPANY_ID = 'orgCompanyId';
    const INN = 'Inn';
    const DATE_MODIFY = 'DateModify';
}