<?php


namespace classes\Enums\DatabasesEnums\PostgresDatabaseEnums\tLotAppTableEnums;


class tLotAppFields
{
    const T_LOT_APP_ID = 'tLotAppId';
    const T_LOT_ID = 'tLotId';
    const APP_DATETIME = 'AppDatetime';
    const LAST_BID_DATETIME = 'LastBidDatetime';
    const APP_NUMBER = 'AppNumber';
    const APP_RATING = 'AppRating';
    const FIRST_PART_REJECTED = 'FirstPartRejected';
    const SECOND_PART_REJECTED = 'SecondPartRejected';
    const REJECT_REASON = 'RejectReason';
    const REJECT_REASON_EXPLANATION = 'RejectReasonExplanation';
    const ORG_SUPPLIER_ID = 'orgSupplierId';
    const S_CURRENCY_ID = 'sCurrencyId';
    const COST = 'Cost';
    const LAST_PROTOCOL_DATE = 'LastProtocolDate';
    const IS_ACTUAL = 'IsActual';
    const LAST_PROTOCOL_TYPE = 'LastProtocolType';
    const CONSIDERED = 'Considered';
    const ORG_CONTACT_PERSON_ID = 'orgContactPersonId';
    const COST_NUMERIC = 'CostNumeric';
}