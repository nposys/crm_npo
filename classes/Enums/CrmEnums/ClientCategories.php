<?php


namespace classes\Enums\CrmEnums;


class ClientCategories
{
    const SUPPLIER = 'Поставщик';
    const CUSTOMER = 'Заказчик';
}