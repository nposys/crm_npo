<?php


namespace classes\Enums\CrmEnums;


class ActivityCategory
{
    const NO_PARTICIPANTS = '00. нет участий';
    const LITTLE_PARTICIPANTS = '10. мало участий';
    const MEDIUM_PARTICIPANTS = '20. средне участий';
    const A_LOT_PARTICIPANTS = '30. много участий';

    public static function getActivityCategory($activityCountPerMonth): string
    {
        if ($activityCountPerMonth === 0) {
            return self::NO_PARTICIPANTS;
        }
        if ($activityCountPerMonth < 1) {
            return self::LITTLE_PARTICIPANTS;
        }
        if ($activityCountPerMonth < 6) {
            return self::MEDIUM_PARTICIPANTS;
        }
        if ($activityCountPerMonth > 6) {
            return self::A_LOT_PARTICIPANTS;
        }
        return self::NO_PARTICIPANTS;
    }
}