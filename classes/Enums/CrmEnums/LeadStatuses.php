<?php


namespace classes\Enums\CrmEnums;


class LeadStatuses
{
    const COLD = 'Холодный';
    const WORK_STARTED = 'Начата работа';
    const INTEREST = 'Интерес';
    const CONDITIONS_DISCUSS = 'Обсуждение условий';
    const INVOICE_ISSUED = 'Выставлен счет';
    const PAYMENT_RECEIVED = 'Получена оплата';
    const REJECTION = 'Отказ';
    const PAUSED = 'Приостановлен';
}