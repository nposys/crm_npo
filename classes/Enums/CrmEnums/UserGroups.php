<?php


namespace classes\Enums\CrmEnums;


class UserGroups
{
    const ADMINISTRATOR = 1;
    const SALES_MANAGERS = 2;
    const CLIENTS = 777;
    const PRODUCTION_MANAGERS = 780;
    const PERSONAL_MANAGERS = 791;
    const ACCOUNTANT = 801;
    const CUSTOMER_RELATIONS_DEPARTMENT_BOSS = 811;
    const PRODUCTION_DEPARTMENT_BOSS = 821;
    const SUPPLY_DEPARTMENT_BOSS = 831;
    const LAWYER = 841;
    const GUESTS = 851;
    const COMPLEX_SERVICES_DEPARTMENT_BOSS = 861;
    const STANDARD_SERVICES_DEPARTMENT = 871;
    const MARKETER = 901;
    const PROGRAMMER = 911;
}