<?php


namespace classes\Enums\CrmEnums;


class Departments
{
	const IT_DEPARTMENT = 4;
	const MANAGEMENT = 5;
	const STANDARD_SERVICES_DEPARTMENT = 8;
	const COMPLEX_SERVICES_DEPARTMENT = 9;
	const CUSTOMER_SUPPORT_DEPARTMENT = 10;
	const CORPORATE_SALES_DEPARTMENT = 11;

}