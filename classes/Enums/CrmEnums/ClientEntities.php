<?php


namespace classes\Enums\CrmEnums;


class ClientEntities
{
    const LEGAL_ENTITY = 'Юр. лицо';
    const PHYSICAL_ENTITY = 'Физ. лицо';
}