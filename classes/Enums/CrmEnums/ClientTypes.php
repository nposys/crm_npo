<?php


namespace classes\Enums\CrmEnums;


class ClientTypes
{
    const POTENTIAL = 'Потенциальный';
    const CLIENT = 'Клиент';
    const PARTNER = 'Партнер';
}