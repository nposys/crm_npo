<?php


namespace classes\Enums\CrmEnums;


class CheckSizeCategory
{
    const NOT_DEFINED = '00. не определено';
    const LOW_CHECK = '10. низкий средний чек';
    const NORMAL_CHECK = '20. нормальный средний чек';
    const HIGH_CHECK = '30. высокий средний чек';

    public static function getCheckSizeCategory(int $avgWinSum):string
    {
        if ($avgWinSum === 0) {
            return self::NOT_DEFINED;
        }
        if ($avgWinSum <= 500000) {
            return self::LOW_CHECK;
        }
        if ($avgWinSum <= 3000000) {
            return self::NORMAL_CHECK;
        }
        if ($avgWinSum >= 3000000) {
            return self::HIGH_CHECK;
        }
        return self::NOT_DEFINED;
    }
}