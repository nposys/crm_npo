<?php


namespace classes\Enums\CrmEnums;


class Positions
{
	const CLIENT_MANAGER = 17;
	const SALES_MANAGER = 16;
	const HEAD_CLIENT_MANAGER = 21;
	const HEAD_SALES_MANAGER = 39;
}