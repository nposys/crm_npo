<?php


namespace classes\Models;


use classes\Enums\DatabasesEnums\PostgresDatabaseEnums\OrgCompanyTableEnums\OrgCompanyFields;

class TDBCompany
{
    private $orgCompanyId;
    private $FullTitle;
    private $ShortTitle;
    private $OrganizationForm;
    private $Inn;
    private $Kpp;
    private $Ogrn;
    private $Okpo;
    private $LegalAddress;
    private $PostalAddress;
    private $RealAddress;
    private $Email;
    private $Phone;
    private $WebSite;
    private $DateRegistered;
    private $region;
    private $contactPersons;

    public function __construct(array $company_options, array $contactPersons)
    {
        $this->contactPersons = $contactPersons;
        foreach ($company_options as $key => $value) {
            switch ($key) {
                case OrgCompanyFields::COMPANY_ID:
                    $this->orgCompanyId = $value;
                    break;
                case OrgCompanyFields::FULL_TITLE:
                    $this->FullTitle = trim($value);
                    break;
                case OrgCompanyFields::SHORT_TITLE:
                    $this->ShortTitle = trim($value);
                    break;
                case OrgCompanyFields::ORGANIZATION_FORM:
                    $this->OrganizationForm = $value;
                    break;
                case OrgCompanyFields::INN:
                    $this->Inn = trim($value);
                    break;
                case OrgCompanyFields::KPP:
                    $this->Kpp = trim($value);
                    break;
                case OrgCompanyFields::OGRN:
                    $this->Ogrn = trim($value);
                    break;
                case OrgCompanyFields::OKPO:
                    $this->Okpo = trim($value);
                    break;
                case OrgCompanyFields::LEGAL_ADDRESS:
                    $this->LegalAddress = trim($value);
                    break;
                case OrgCompanyFields::POSTAL_ADDRESS:
                    $this->PostalAddress = trim($value);
                    break;
                case OrgCompanyFields::REAL_ADDRESS:
                    $this->RealAddress = trim($value);
                    break;
                case OrgCompanyFields::EMAIL:
                    $this->Email = trim($value);
                    break;
                case OrgCompanyFields::PHONE:
                    $this->Phone = trim($value);
                    break;
                case OrgCompanyFields::WEB_SITE:
                    $this->WebSite = trim($value);
                    break;
                case OrgCompanyFields::DATE_REGISTERED:
                    $this->DateRegistered = $value;
                    break;
                case 'region':
                    $this->region = $value;
                    break;
            }
        }
    }

    public function getContactsPersonsPhones(): array
    {
        $phones = [];
        foreach ($this->contactPersons as $contact_person) {
            $result = $contact_person->getPhone();
            foreach (explode(',', $result) as $phone) {
                $phone = trim($phone);
                if (isset($phone) && $phone !== '') {
                    $phones[] = $phone;
                } else {
                    continue;
                }
            }
        }
        return array_unique($phones);
    }

    public function getContactsPersonsEmails(): array
    {
        $emails = [];
        foreach ($this->contactPersons as $contact_person) {
            $result = $contact_person->getEmail();
            foreach (explode(',', $result) as $email) {
                $email = trim($email);
                if (isset($email) && $email !== '') {
                    $emails[] = $email;
                } else {
                    continue;
                }
            }
        }
        return array_unique($emails);
    }

    public function getContactsPersonsFullNames(): array
    {
        $fullNames = [];
        foreach ($this->contactPersons as $contact_person) {
            $result = $contact_person->getFullName();
            foreach (explode(',', $result) as $fullName) {
                $fullName = trim($fullName);
                if (isset($fullName) && $fullName !== '') {
                    $fullNames[] = $fullName;
                } else {
                    continue;
                }
            }
        }
        return array_unique($fullNames);
    }

    /**
     * @return mixed
     */
    public
    function getOrgCompanyId()
    {
        return $this->orgCompanyId;
    }

    /**
     * @return mixed
     */
    public
    function getFullTitle()
    {
        return $this->FullTitle;
    }

    /**
     * @return mixed
     */
    public
    function getShortTitle()
    {
        return $this->ShortTitle;
    }

    /**
     * @return mixed
     */
    public
    function getOrganizationForm()
    {
        return $this->OrganizationForm;
    }

    /**
     * @return mixed
     */
    public
    function getInn()
    {
        return $this->Inn;
    }

    /**
     * @return mixed
     */
    public
    function getKpp()
    {
        return $this->Kpp;
    }

    /**
     * @return mixed
     */
    public
    function getOgrn()
    {
        return $this->Ogrn;
    }

    /**
     * @return mixed
     */
    public
    function getOkpo()
    {
        return $this->Okpo;
    }

    /**
     * @return mixed
     */
    public
    function getLegalAddress()
    {
        return $this->LegalAddress;
    }

    /**
     * @return mixed
     */
    public
    function getPostalAddress()
    {
        return $this->PostalAddress;
    }

    /**
     * @return mixed
     */
    public
    function getRealAddress()
    {
        return $this->RealAddress;
    }

    /**
     * @return mixed
     */
    public
    function getEmail()
    {
        return $this->Email;
    }

    /**
     * @return mixed
     */
    public
    function getPhone()
    {
        return $this->Phone;
    }

    /**
     * @return mixed
     */
    public
    function getWebSite()
    {
        return $this->WebSite;
    }

    /**
     * @return mixed
     */
    public
    function getDateRegistered()
    {
        return $this->DateRegistered;
    }

    /**
     * @return mixed
     */
    public
    function getRegion()
    {
        return $this->region;
    }

}