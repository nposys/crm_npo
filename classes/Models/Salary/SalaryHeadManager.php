<?php


namespace classes\Models\Salary;


class SalaryHeadManager extends Salary
{
	protected $managers;
	protected $totals = [];
	protected $department_results;

	public function prepareTotals()
	{
	}

	/**
	 * @param mixed $department_results
	 */
	public function setDepartmentResults($department_results)
	{
		$this->department_results = $department_results;
	}

	/**
	 * @param array $managers
	 */
	public function setManagers($managers)
	{
		$this->managers = $managers;
	}



	public function getTotals(): array
	{
		return $this->totals;
	}
}