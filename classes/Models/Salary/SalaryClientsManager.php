<?php


namespace classes\Models\Salary;


use classes\Base\Table;
use classes\Enums\CrmEnums\MotivationTypes;
use classes\Tables\Billings;
use classes\Tables\PositionsOrdersInvoices;

class SalaryClientsManager extends Salary
{
	public function calculateSalary()
	{
		parent::calculateSalary();
		$this->getBonus();
	}


	public function getBonus()
	{
		if (!isset($this->motivations)) {
			return;
		}

		$sales_plans = new Table('Планы продаж');
		$sales_plans->getData(
			[
				'План',
				'Тип услуг',
				'Менеджер'
			],
			[
				'status' => 0,
				'Месяц' => '\'' . date('Y-m-01', strtotime($this->month)) . '\'',
				'Сотрудник' => $this->manager['ID']
			]);

		$start_date = date('Y-m-01 00:00:00', strtotime($this->month));
		$finish_date = date('Y-m-t 23:59:59', strtotime($this->month));

		if ($sales_plans->num_rows > 0) {
			$table = new PositionsOrdersInvoices();
			$table->getClientsManagerInvoices($this->manager['Должность'], $sales_plans->data[0]['Менеджер'], $start_date, $finish_date);
			$bonus = $this->getClientManagerBonus($table->data, $sales_plans->data[0]);
			$this->salary['salary']['pivot']['auto']['benefit_win_total'] = $bonus;
			$this->salary['salary']['pivot']['manual']['benefit_win_total'] = $bonus;
			$this->salary['salary_report'] = $this->salary_report;
		}
	}

	private
	function getClientManagerBonus($data, $plan_data)
	{
		$plan = $plan_data['План'];
		$service_type = $plan_data['Тип услуг'];

		$calculations = new Billings();
		$calculations->getData([],
			[
				'Сотрудник' => (int)$this->manager['ID'],
				'Тип' =>
					[
						'field_name' => 'Тип',
						'operator' => 'IN',
						'value' => '(' . MotivationTypes::SECONDARY_TYPICAL_SALES_BONUS . ', ' . MotivationTypes::SECONDARY_COMPLEX_SALES_BONUS . ')'
					],
				'Месяц' => '\'' . date('Y-m-01', strtotime($this->month)) . '\''
			]);
		$manual_plan_coef = 0.0;
		if ($calculations->num_rows > 0) {
			$manual_plan_coef = (float)$calculations->data[0]['Коэффициент 1'];
		}

		$mt = $this->getSumIncomesTypicalCoef();
		$motivation = $this->orderBorderAsc($mt);

		$index = 0;
		$sum = 0;
		$bonus_sum = 0;
		$motivation_length = count($motivation);
		foreach ($data as $income) {
			if ($income['Вид услуги'] === 'Подготовка документации' && $service_type === 'Обычные') {
				continue;
			} #TODO handle

			$order_income_calculation = [];
			if (count($calculations->data) !== 0) {
				$order_income_index = $this->getOrderIncomeIndex($calculations->data, $income['ID Счета'], $income['ID Заказа']);
			}
			if (isset($order_income_index) && $order_income_index !== false) {
				$order_income_calculation = $calculations->data[$order_income_index];
			}

			$profit = (float)$income['Оплачено сумма заказ'] - (float)$income['Учитываемые расходы'];

			if ($sum + $profit > $motivation[$index]['border'] && $index + 1 < $motivation_length) {
				$first_part = $motivation[$index]['border'] - $sum;
				$bonus = $first_part * $motivation[$index]['rate'];

				$second_part = $sum + $profit - $motivation[$index]['border'];
				$bonus += $second_part * $motivation[$index + 1]['rate'];
//				$rate = 'смеш.';
				$rate = round($bonus / $profit, 4);
				$index++;
			} else {
				$bonus = $profit * $motivation[$index]['rate'];
				$rate = $motivation[$index]['rate'];
			}
			$coef_for_timely_fundraising = $this->getCoefForTimelyFundraising($income['Дата заказа'], $income['Дата исполнения'], $income['Дата поступления'], $income['Тип клиента плательщика'] === 'Партнер');
			$bonus *= $coef_for_timely_fundraising;
			$bonus_sum += $bonus;
			$sum += $profit;
			$this->fillSalaryReport($income, $rate, $coef_for_timely_fundraising, $sum, $bonus, $order_income_calculation);
		}
		$plan_percent = $sum / (float)$plan * 100;
		$plan_realization_coef = $this->getPlanRealizationCoef($plan_percent);
		$probably_bonus_sum = $bonus_sum;
		$bonus_sum *= $plan_realization_coef;
		$this->fillResultData((float)$plan, $plan_percent, $sum, $plan_realization_coef, $probably_bonus_sum, $bonus_sum, $manual_plan_coef);
		$this->formatNumericData($bonus_sum);
		return $bonus_sum;
	}

	protected
	function getSumIncomesTypicalCoef()
	{
		return $this->getCoef(date('Y-d-t', strtotime($this->month)), 'Личное', 'sum_incomes_typical_coef');
	}

	private function getCoefForTimelyFundraising($order_date, $done_date, $payment_date, $is_partner)
	{
		$motivation = [
			"Обычные" => $this->orderBorderAsc($this->getTypicalTimePeriodCoefs($payment_date)),
			"Партнер" => $this->orderBorderAsc($this->getPartnerTimePeriodCoefs($payment_date))
		];
		if (!$is_partner) {
			$date_start = date("Y-m-d", strtotime($order_date));
		} else {
			$date_start = date("Y-m-d", strtotime($done_date));
		}
		$payment_date = date("Y-m-d", strtotime($payment_date));
		if (!$is_partner) {
			foreach ($motivation['Обычные'] as $motivation_value) {
				$date_limit = date('Y-m-d', strtotime($date_start . " + " . $motivation_value['border'] . "days"));
				if ($payment_date <= $date_limit) {
					return $motivation_value['rate'];
				}
			}
			return 0;
		} else {
			foreach ($motivation['Партнер'] as $motivation_value) {
				$date_limit = date('Y-m-d', strtotime($date_start . " + " . $motivation_value['border'] . "days"));
				if ($payment_date <= $date_limit) {
					return $motivation_value['rate'];
				}
			}
			return 0;
		}
	}

	private
	function getTypicalTimePeriodCoefs($payment_date)
	{
		return $this->getCoef(date('Y-m-d', strtotime($payment_date)), 'Личное', 'typical_time_period_coef');
	}

	private
	function getPartnerTimePeriodCoefs($payment_date)
	{
		return $this->getCoef(date('Y-m-d', strtotime($payment_date)), 'Личное', 'partner_time_period_coef');
	}

	public
	function fillResultData($plan, $plan_percent, $sum, $plan_realization_coef, $probably_bonus_sum, $bonus_sum, $manual_plan_coef)
	{
		$this->salary_report['service_groups']['Обычные']['total']['plan_sum'] = $this->prepareNumericData($plan);
		$this->salary_report['service_groups']['Обычные']['total']['plan_percent'] = $this->prepareNumericData($plan_percent);
		$this->salary_report['service_groups']['Обычные']['total']['sum'] = $this->prepareNumericData($sum);
		$this->salary_report['service_groups']['Обычные']['total']['benefit_final'] = $this->prepareNumericData($bonus_sum);
		$this->salary_report['service_groups']['Обычные']['total']['probably_benefit_final'] = $this->prepareNumericData($probably_bonus_sum);

		$this->salary_report['service_groups']['Комплексные']['total']['plan_sum'] = '0,00';
		$this->salary_report['service_groups']['Комплексные']['total']['plan_percent'] = $this->prepareNumericData($plan_percent);
		$this->salary_report['service_groups']['Комплексные']['total']['sum'] = '0,00'; #TODO
		$this->salary_report['service_groups']['Комплексные']['total']['benefit_final'] = '0,00';
		$this->salary_report['service_groups']['Комплексные']['total']['probably_benefit_final'] = '0,00';

		$this->salary_report['service_groups']['Обычные']['auto']['total']['plan_coef'] = $this->prepareNumericData($plan_realization_coef);
		$this->salary_report['service_groups']['Комплексные']['auto']['total']['plan_coef'] = $this->prepareNumericData($plan_realization_coef);

		if ($this->override_manual_with_auto) {
			$this->salary_report['service_groups']['Обычные']['manual']['total']['plan_coef'] = $this->prepareNumericData($plan_realization_coef);
			$this->salary_report['service_groups']['Комплексные']['manual']['total']['plan_coef'] = $this->prepareNumericData($plan_realization_coef);
		} else{
			$this->salary_report['service_groups']['Обычные']['manual']['total']['plan_coef'] = $this->prepareNumericData($manual_plan_coef);
			$this->salary_report['service_groups']['Комплексные']['manual']['total']['plan_coef'] = $this->prepareNumericData($manual_plan_coef);
		}

		$this->salary_report["result_salary"] = $this->prepareNumber($this->salary_report["result_salary"]) + $bonus_sum;
		$this->formatNumericData($this->salary_report["result_salary"]);


		$this->salary_report['sum_result'] = $this->prepareNumericData($sum);

		$this->salary_report['benefit_result'] = $this->prepareNumericData($bonus_sum);
		$this->salary_report['probably_benefit_result'] = $this->prepareNumericData($probably_bonus_sum);

	}

	public
	function fillSalaryReport($income, $rate, $coef_for_timely_fundraising, $sum_income, $bonus, $order_income_calculation)
	{
		$this->formatNumericData($bonus);
		$order_type_group = 'Обычные';
		$date_payment = $income['Дата поступления'];
		$iioId = $income['ID'];

		$seconds_per_day = 60 * 60 * 24;
		if ($income['Тип клиента плательщика'] === 'Партнер') {
			$seconds_diff = strtotime($income['Дата поступления']) - strtotime($income['Дата счета']);
		} else {
			$seconds_diff = strtotime($income['Дата поступления']) - strtotime($income['Дата заказа']);
		}
		$days_diff = $seconds_diff / $seconds_per_day;
		$this->formatNumericData($days_diff);

		$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['order_id'] = $income['ID Заказа'];
		$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['order_num'] = $income['Номер заказа'];
		$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['order_sum_total'] = $income['Сумма'];
		$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['order_paid_total'] = $income['Оплачено'];
		$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['order_expenses_total'] = $income['Учитываемые расходы'];
		$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['order_expenses_comment'] = $income['Комментарий по расходам'];
		$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['income_id'] = $income['ID Счета'];
		$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['income_num'] = $income['Номер счета'];
		$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['order_num'] = $income['Номер заказа'];
		$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['client_id'] = $income['ID Клиента'];
		$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['client'] = $income['Название клиента'];
		$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['source_name'] = $income['Название клиента плательщика'];
		$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['source_type'] = $income['Тип клиента плательщика'];
		$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['service'] = $income['Вид услуги'];
		$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['description'] = mb_substr($income['Описание'], 0, 40, 'UTF-8');
		$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['sum'] = $income['Оплачено сумма заказ'];
		$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['date_order'] = date("Y-m-d", strtotime($income['Дата заказа']));
		$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['date_invoice'] = date("Y-m-d", strtotime($income['Дата счета']));
        $this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['date_payment'] = date("Y-m-d", strtotime($income['Дата поступления']));
		$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['date_order_finish'] = date("Y-m-d", strtotime($income['Дата исполнения']));
		$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['manager_id'] = $income['Менеджер клиента НПО'];
		$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['final_payment'] = $this->prepareNumericData($income['Окончательный платеж']);
		$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['date_diff'] = $days_diff;
		$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['cur_sum'] = $this->prepareNumericData($sum_income);

		$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['auto']['result'] =
			$this->prepareNumericData(
				$income['Оплачено сумма заказ'] - $income['Учитываемые расходы']
			);
		$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['auto']['benefit_rate_date'] =
			$this->prepareNumericData($coef_for_timely_fundraising);
		$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['auto']['benefit_rate'] =
			number_format($rate, 4, ',', ' ');
		$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['auto']['benefit'] =
			$this->prepareNumericData($bonus);

		if ($this->override_manual_with_auto) {
			$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['manual']['result'] =
				$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['auto']['result'];

			$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['manual']['benefit_rate_date'] =
				$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['auto']['benefit_rate_date'];

			$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['manual']['benefit_rate'] =
				$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['auto']['benefit_rate'];

			$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['manual']['benefit'] =
				$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['auto']['benefit'];

		} elseif (!empty($order_income_calculation)) {
			$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['manual']['result'] =
				$this->prepareNumericData(
					(float)$order_income_calculation['Основание']
				);
			$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['manual']['benefit_rate_date'] =
				$this->prepareNumericData(
					(float)$order_income_calculation['Коэффициент 2']
				);
			$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['manual']['benefit_rate'] =
				number_format(
					(float)$order_income_calculation['Ставка'], 4, ',', ' '
				);
			$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['manual']['benefit'] =
				$this->prepareNumericData(
					(float)$order_income_calculation['Сумма начисления']
				);
			$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['manual']['calculation_id'] =
				$order_income_calculation['ID'];
			$this->salary_report['service_groups'][$order_type_group]['orders'][$date_payment][$iioId]['manual']['benefit_comment'] =
				$order_income_calculation['Примечание'];
		}

		$this->salary_report['user'] = $income['Менеджер клиента'];
		$this->salary_report['position'] = $income['Должность'];
	}

	private function getOrderIncomeIndex($order_income_calculations, $income_id, $order_id)
	{
		$keys = array_keys(array_column($order_income_calculations, 'Заказ'), $order_id);
		foreach ($keys as $key) {
			if ($order_income_calculations[$key]['Поступление'] === $income_id) {
				return $key;
			}
		}
		return false;
	}
}