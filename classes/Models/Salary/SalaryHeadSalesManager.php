<?php


namespace classes\Models\Salary;


class SalaryHeadSalesManager extends SalaryHeadManager
{
	public function prepareTotals()
	{
		$this->totals['typical_plan']['total'] = '0,00';
		$this->totals['complex_plan']['total'] = '0,00';
		$this->totals['typical']['total'] = '0,00';
		$this->totals['complex']['total'] = '0,00';

		$this->totals["totals"]["benefit"]["total"] = '0,00';
		$this->totals["totals"]["benefit"]["typical"] = '0,00';
		$this->totals["totals"]["benefit"]["complex"] = '0,00';
		$this->totals["totals"]["salary"]["total"] = '0,00';

		$this->totals["group_name"] = "Менеджеры продаж";
		foreach ($this->department_results as $manager => $manager_data) {
			$manager_num = array_search($manager, array_column($this->managers, 'Фамилия Инициалы'), true);
			$this->totals["managers"][$manager]["work_now"] =
				$this->managers[$manager_num]["Работает сейчас"];

			$this->totals["managers"][$manager]["sum"]["total"] =
				$manager_data["sum_result"];

			$this->totals["managers"][$manager]["sum"]["typical_plan"] =
				$manager_data['income_types']["Первичные"]['service_groups']["Обычные"]['total']['plan_sum'];
			$this->totals["managers"][$manager]["sum"]["complex_plan"] =
				0;

			$this->totals["managers"][$manager]["sum"]["typical"]['new'] =
				$manager_data['income_types']["Первичные"]['service_groups']["Обычные"]['total']['sum'];
			$this->totals["managers"][$manager]["sum"]["typical"]['old'] =
				$manager_data['income_types']["Вторичные"]['service_groups']["Обычные"]['total']['sum'];

			$this->totals["managers"][$manager]["sum"]["complex"]['new'] =
				$manager_data['income_types']["Первичные"]['service_groups']["Комплексные"]['total']['sum'];
			$this->totals["managers"][$manager]["sum"]["complex"]['old'] =
				$manager_data['income_types']["Вторичные"]['service_groups']["Комплексные"]['total']['sum'];

			$this->totals['typical_plan']['total'] += $this->totals["managers"][$manager]["sum"]["typical_plan"];
			$this->totals['complex_plan']['total'] += $this->totals["managers"][$manager]["sum"]["complex_plan"];

			$this->totals['typical']['total'] += $manager_data['income_types']["Первичные"]['service_groups']["Обычные"]['total']['sum'];
			$this->totals['complex']['total'] += $manager_data['income_types']["Первичные"]['service_groups']["Комплексные"]['total']['sum'];


			if ($this->managers[$manager_num]["Работает сейчас"] === "Да") {
				$this->totals["managers"][$manager]["benefit"]["total"] = $manager_data["benefit_result"];

				$this->totals["managers"][$manager]["benefit"]["typical"]['new'] =
					$manager_data['income_types']['Первичные']['service_groups']['Обычные']['total']['benefit_final'];
				$this->totals["managers"][$manager]["benefit"]["typical"]['old'] =
					$manager_data['income_types']['Вторичные']['service_groups']['Обычные']['total']['benefit_final'];

				$this->totals["managers"][$manager]["benefit"]["complex"]['new'] =
					$manager_data['income_types']['Первичные']['service_groups']['Комплексные']['total']['benefit_final'];
				$this->totals["managers"][$manager]["benefit"]["complex"]['old'] =
					$manager_data['income_types']['Вторичные']['service_groups']['Комплексные']['total']['benefit_final'];

				$this->totals["managers"][$manager]["base_salary"] =
					$this->managers[$manager_num]["Оклад"];
				$this->totals["managers"][$manager]["result_salary"] =
					$this->managers[$manager_num]["Оклад"] + $manager_data["benefit_result"];

				$this->totals["totals"]["benefit"]["total"] += $manager_data["benefit_result"];
				$this->totals["totals"]["benefit"]["typical"] += $manager_data['income_types']['Первичные']['service_groups']['Обычные']['total']['benefit_final'];
				$this->totals["totals"]["benefit"]["typical"] += $manager_data['income_types']['Вторичные']['service_groups']['Обычные']['total']['benefit_final'];
				$this->totals["totals"]["benefit"]["complex"] += $manager_data['income_types']['Первичные']['service_groups']['Комплексные']['total']['benefit_final'];
				$this->totals["totals"]["benefit"]["complex"] += $manager_data['income_types']['Вторичные']['service_groups']['Комплексные']['total']['benefit_final'];
				$this->totals["totals"]["salary"]["total"] += $this->totals["managers"][$manager]["result_salary"];
			}
		}
	}

	public function getBonus()
	{

	}
}