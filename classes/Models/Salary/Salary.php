<?php


namespace classes\Models\Salary;


use classes\Base\Common;
use classes\Base\Table;
use classes\Enums\CrmEnums\MotivationTypes;
use classes\Tables\Billings;
use classes\Tables\Personal;
use classes\Tables\Timetable;

class Salary extends Common
{
	protected $manager;
	protected $motivations;
	protected $salary;
	protected $month;
	protected $vacation_pay;
	protected $total_work_days;
	protected $override_manual_with_auto_common;
	protected $use_manual_holidays;
	protected $need_report;
	protected $salary_report;
	protected $manager_month_not_work_days;
	protected $benefit_holiday_auto_comment;
	protected $benefit_holiday_auto_note;
	protected $override_manual_with_auto;

    /**
     * Salary constructor.
     * @param array $params
     *  $manager,
     *  $month,
     *  $total_work_days,
     *  $override_manual_with_auto_common,
     *  $use_manual_holidays,
     *  $override_manual_with_auto,
     *  $motivations
     */
	public function __construct($params = [])
	{
		$this->manager = $params['manager'];
		$this->month = $params['month'];
		$this->total_work_days = $params['total_work_days'];
		$this->override_manual_with_auto_common = $params['override_manual_with_auto_common'];
		$this->use_manual_holidays = $params['use_manual_holidays'];
		$this->override_manual_with_auto = $params['override_manual_with_auto'];
        $this->motivations = $params['motivations'];

		$this->salary_report = [];
	}

	public function setTotalSalaryResult()
	{
		$total_salary_auto = 0;
		$total_salary_manual = 0;
		foreach ($this->salary['salary']['pivot']['auto'] as $benefit_key => $benefit_value) {
			if ($benefit_key === 'remain_past_paid' || $benefit_key === 'paid_total') {
				continue;
			}
			$total_salary_auto += $this->prepareNumber($benefit_value);
		}
		foreach ($this->salary['salary']['pivot']['manual'] as $benefit_key => $benefit_value) {
			if ($benefit_key === 'remain_past_paid' || $benefit_key === 'paid_total') {
				continue;
			}
			$total_salary_manual += $this->prepareNumber($benefit_value);
		}
		$this->salary['salary']['pivot']['auto']['salary_total'] = $this->prepareNumericData($total_salary_auto);
		$this->salary['salary']['pivot']['auto']['paid_remain'] = $this->prepareNumericData($total_salary_auto -
			$this->prepareNumber($this->salary['salary']['pivot']['auto']['paid_remain']));

		$this->salary['salary']['pivot']['manual']['salary_total'] = $this->prepareNumericData($total_salary_manual);
		$this->salary['salary']['pivot']['manual']['paid_remain'] = $this->prepareNumericData($total_salary_manual -
			$this->prepareNumber($this->salary['salary']['pivot']['manual']['paid_remain']));
	}

	public function getBonus()
	{
	}

	public function getReport(): array
	{
		return $this->salary_report;
	}

	public
	function setNeedReport(bool $need_report)
	{
		$this->need_report = $need_report;
	}

	/**
	 * @return mixed
	 */
	public
	function getSalary()
	{
		return $this->salary;
	}

	public
	function calculateSalary()
	{
		$this->calculateSalaryCommon();
	}

	protected
	function calculateSalaryCommon()
	{
		$this->calculateBaseSalary();
		$this->calculateHolidaysSalary();
		$this->calculateOtherSalary();

		$this->getPayments();
		$this->createPivotData();
	}

	private
	function getSalaryPerDay($start_work_date, string $day)
	{
		$rate = 0;
		foreach ($this->motivations["motivations"]['base']['Личное']['type'][MotivationTypes::TRIAL_TIME_SALARY] as $motivation) {

			if ($motivation['start'] > $day || ($motivation['end'] > '1971-01-01' && $motivation['end'] < $day)) {
				continue;
			}
			$border = $motivation['val']['border'];
			$trial_period_last_day = date('Y-m-d', strtotime($start_work_date . " + $border days"));
			if ($day < $trial_period_last_day) {
				if (!isset($min_border)) {
					$min_border = $border;
					$rate = $motivation['val']['rate'];
				} elseif ($border < $min_border) {
					$min_border = $border;
					$rate = $motivation['val']['rate'];
				}
			}
		}
		if (isset($min_border)) {
			return $rate;
		}

		foreach ($this->motivations["motivations"]['base']['Личное']['type'][MotivationTypes::BASE_SALARY] as $motivation) {

			if ($motivation['start'] > $day || ($motivation['end'] > '1971-01-01' && $motivation['end'] < $day)) {
				continue;
			}
			return $motivation["val"];
		}
		return null;
	}

	protected
	function calculateBaseSalary()
	{
		if (empty($this->manager)) {
			return;
		}
		$manager_id = (int)$this->manager['ID'];

		if (!isset($this->motivations)) {
			return;
		}

		// ================================================================================================

		$calculations = new Table('Начисления зарплаты');
		$personal = new Personal();

		// ================================================================================================

		$personal->getDataById($manager_id, [], ['replace_related_data' => 0]);

		// ================================================================================================
		// вычислем кол-во отработанных дней
		// ================================================================================================

		$manager_month_not_work_days = $personal->getNumberOfManagerNotWorkDays($this->manager['ID'], $this->month);

		$date_start_work = date("Y-m-d", strtotime($this->manager['Дата поступления']));
		$date_end_work = date("Y-m-d", strtotime($this->manager['Дата увольнения']));
		$month_start_work = date("Y-m", strtotime($date_start_work));
		$month_end_work = date("Y-m", strtotime($date_end_work));
		$total_work_days = $this->total_work_days;
		$base_salary_result_auto = 0;
		$base_salary_auto = 0;

		if ($date_end_work > '2010-01-01' && strtotime($date_end_work) < strtotime($this->month)) {
			$manager_month_not_work_days = $this->total_work_days;
		} else {
			$i = 0;

			$next_day = $this->month . "-01";
			$end_day = date("Y-m-t", strtotime($next_day));
			$timetable = new Table('Табель');
			$timetable->getData(
				[],
				[
					'Сотрудник' => $manager_id,
					'Период' => '\'' . $this->month . '\''
				],
				[],
				['debug' => 1]);
			$last_salary = 0;
			while ($next_day <= $end_day) {
				if ($personal->isWorkingDay($next_day)) {
					$temp_salary = $this->getSalaryPerDay($this->manager['Дата поступления'], $next_day);
					if ($temp_salary) {
						$last_salary = $temp_salary;
					}
					$base_salary_auto += $last_salary;
					if ($this->month === $month_start_work && $next_day < $date_start_work) {
						$i++;
					} else if ($this->month === $month_end_work && $next_day > $date_end_work) {
						$i++;
					} else {
						$work_day = true;
						foreach ($timetable->data as $day_record) {
							$not_work_start = date("Y-m-d", strtotime($day_record['Дата и время начала']));
							$not_work_end = date("Y-m-d", strtotime($day_record['Дата и время окончания']));
							if ($not_work_start <= $next_day && $not_work_end >= $next_day) {
								if ($day_record['Тип события'] === 'Больничный') {
									$base_salary_result_auto += (0.5 * $temp_salary);
								} else if ($not_work_start === $not_work_end) {
									$base_salary_result_auto += ((1 - $day_record['Рабочих дней']) * $temp_salary);
								}
								$work_day = false;
							}
						}
						if ($work_day) {
							$base_salary_result_auto += $temp_salary;
						}
					}
				}
				$next_day = date("Y-m-d", strtotime($next_day . " + 1 day"));
			}
			$manager_month_not_work_days += $i;
		}
		$this->manager_month_not_work_days = $manager_month_not_work_days;
		$this->formatNumericData($base_salary_auto);
		// ================================================================================================
		// вычисляем оклад
		// ================================================================================================
		$base_salary_coef_auto = ($total_work_days - $manager_month_not_work_days) / $total_work_days;
		$calculations->getData([],
			[
				'Сотрудник' => $manager_id,
				'Месяц' => '\'' . date("Y-m-01 00:00:00", strtotime($this->month)) . '\'',
				'База' => '\'Личное\'',
				'Тип' => 1, // Оклад
			]
		);

		if ($this->override_manual_with_auto_common) {
			$base_salary_manual = $base_salary_auto;
			$base_salary_coef_manual = $base_salary_coef_auto;
			$base_salary_result_manual = $base_salary_result_auto;
			$base_salary_comment_manual = '';
		} else {
			// проверям наличие начисления
			$base_salary_manual = 0.0;
			$base_salary_coef_manual = 0.0;
			$base_salary_result_manual = 0.0;
			$base_salary_comment_manual = '';

			if ($calculations->num_rows > 0) {
				$base_salary_manual = (float)$calculations->data[0]['Основание'];
				$base_salary_coef_manual = (float)$calculations->data[0]['Коэффициент 1'];
				$base_salary_result_manual = (float)$calculations->data[0]['Сумма начисления'];
				$base_salary_comment_manual = (string)$calculations->data[0]['Примечание'];
			}
		}

		$this->salary['salary']['pivot']['base']['auto']['base_salary'] = $this->prepareNumericData($base_salary_auto);
		$this->salary['salary']['pivot']['base']['auto']['base_salary_coef'] = $this->prepareNumericData($base_salary_coef_auto);
		$this->salary['salary']['pivot']['base']['auto']['base_salary_result'] = $this->prepareNumericData($base_salary_result_auto);
		$this->salary['salary']['pivot']['base']['auto']['base_salary_comment'] = '';

		$this->salary['salary']['pivot']['base']['manual']['base_salary'] = $this->prepareNumericData($base_salary_manual);
		$this->salary['salary']['pivot']['base']['manual']['base_salary_coef'] = $this->prepareNumericData($base_salary_coef_manual);
		$this->salary['salary']['pivot']['base']['manual']['base_salary_result'] = $this->prepareNumericData($base_salary_result_manual);
		$this->salary['salary']['pivot']['base']['manual']['base_salary_comment'] = $base_salary_comment_manual;
		$this->salary['salary']['pivot']['auto']['base_salary_total'] = $this->prepareNumericData($base_salary_result_auto);
		$this->salary['salary']['pivot']['manual']['base_salary_total'] = $this->prepareNumericData($base_salary_result_manual);
		$this->salary['salary']['pivot']['base']['manual']['calculation_id'] = (string)$calculations->data[0]['ID'];
	}

	protected
	function calculateHolidaysSalary()
	{
		if (empty($this->manager)) {
			return;
		}
		$calculations = new Table('Начисления зарплаты');
		$manager_id = (int)$this->manager['ID'];
		$calculations->getData([],
			[
				'Сотрудник' => $manager_id,
				'Месяц' => '\'' . date("Y-m-01 00:00:00", strtotime($this->month)) . '\'',
				'База' => '\'Личное\'',
				'Тип' => MotivationTypes::HOLIDAY_SALARY, // Отпускные
			]
		);

		// вызываем функцию вычисения отпускных начислений, которая заполняет массив vacation_pay
		$this->getBenefitHolidayAuto($manager_id, $this->month);
		$benefit_holiday_auto = $this->vacation_pay[$manager_id]['benefit'];

		// расчет отпускных: средняя зп * кол-во отпускных дней / 21
		$this->benefit_holiday_auto_comment = $this->vacation_pay[$manager_id]['comment'];

		// Комментарий к отпускным в формате:
		// Комментарий из табеля + Отпускные n р.д. с дата-начала-отпуска
		$this->benefit_holiday_auto_note = $this->benefit_holiday_auto_comment === '' ? '' :
			$this->vacation_pay[$manager_id]['note'] .
			" Отпускные " . $this->vacation_pay[$manager_id]['days'] . " р.д. с " . $this->vacation_pay[$manager_id]['start_vacation'];

		if ($this->override_manual_with_auto_common) {
			$benefit_holiday_manual = $benefit_holiday_auto;
			$benefit_holiday_manual_comment = $this->benefit_holiday_auto_comment . " " . $this->benefit_holiday_auto_note;
		} else {
			$benefit_holiday_manual = (float)$calculations->data[0]['Сумма начисления'];
			$benefit_holiday_manual_comment = (string)$calculations->data[0]['Примечание'];
		}

		if ($this->use_manual_holidays) {
			$benefit_holiday_auto = $benefit_holiday_manual;
		}

		$this->salary['salary']['pivot']['holiday']['auto']['benefit_holiday'] = $this->prepareNumericData($benefit_holiday_auto);
		$this->salary['salary']['pivot']['holiday']['auto']['benefit_holiday_comment'] = '';

		$this->salary['salary']['pivot']['holiday']['manual']['benefit_holiday'] = $this->prepareNumericData($benefit_holiday_manual);
		$this->salary['salary']['pivot']['holiday']['manual']['benefit_holiday_comment'] = $benefit_holiday_manual_comment;
		$this->salary['salary']['pivot']['holiday']['manual']['calculation_id'] = (string)$calculations->data[0]['ID'];

		$this->salary['salary']['pivot']['auto']['benefit_holiday_total'] = $this->prepareNumericData($benefit_holiday_auto);
		$this->salary['salary']['pivot']['manual']['benefit_holiday_total'] = $this->prepareNumericData($benefit_holiday_manual);
	}

    protected
    function calculateOtherSalary()
    {
        if (empty($this->manager)) {
            return;
        }

        $calculations = new Table('Начисления зарплаты');
        $manager_id = (int)$this->manager['ID'];
        $calculations->getData([],
            [
                'Сотрудник' => $manager_id,
                'Месяц' => '\'' . date("Y-m-01 00:00:00", strtotime($this->month)) . '\'',
                'База' => '\'Личное\'',
                'Тип' => MotivationTypes::ANOTHER_BONUS, // Отпускные
            ]
        );

        if ($calculations->num_rows > 0) {
            foreach ($calculations->data as $calculation) {
                $benefit_other_auto = 0.0;

                $benefit_other_manual = (float)$calculation['Сумма начисления'];
                $benefit_other_manual_comment = (string)$calculation['Примечание'];

                $calculation_id = (string)$calculation['ID'];
                $this->salary['salary']['pivot']['other']['items'][$calculation_id]['auto']['benefit_other'] = $this->prepareNumericData($benefit_other_auto);
                $this->salary['salary']['pivot']['other']['items'][$calculation_id]['auto']['benefit_other_comment'] = '';
                $benefit_other_total_auto += (float)$benefit_other_auto;

                $this->salary['salary']['pivot']['other']['items'][$calculation_id]['manual']['benefit_other'] = $this->prepareNumericData($benefit_other_manual);
                $this->salary['salary']['pivot']['other']['items'][$calculation_id]['manual']['calculation_id'] = $calculation_id;
                $this->salary['salary']['pivot']['other']['items'][$calculation_id]['manual']['benefit_other_comment'] = trim($benefit_other_manual_comment);

                $benefit_other_total_manual += (float)$benefit_other_manual;
                if ($benefit_other_manual <> 0) {
                    if ($benefit_other_total_manual_comment == '') {
                        $benefit_other_total_manual_comment = "$benefit_other_manual = \"$benefit_other_manual_comment\"";
                    } else {
                        $benefit_other_total_manual_comment .= "<br>$benefit_other_manual = \"$benefit_other_manual_comment\"";
                    }
                }
            }
        }

        // if (($calculations->num_rows > 1) || ($calculations->num_rows == 1 && $benefit_other_total_manual <> 0) || $calculations->num_rows == 0) {
        if (true) {
            $this->salary['salary']['pivot']['other']['items']['0']['auto']['benefit_other'] = $this->prepareNumericData(0.0);
            $this->salary['salary']['pivot']['other']['items']['0']['auto']['benefit_other_comment'] = '';

            $this->salary['salary']['pivot']['other']['items']['0']['manual']['benefit_other'] = $this->prepareNumericData(0.0);
            $this->salary['salary']['pivot']['other']['items']['0']['manual']['benefit_other_comment'] = '';
            $this->salary['salary']['pivot']['other']['items']['0']['manual']['calculation_id'] = 0;
        }

    }

	protected
	function getBenefitHolidayAuto($manager, $month)
	{
		$manager_id = (int)$manager;
		// Если отпускных дней в месяце нет, дальше не считаем
		if ($this->getBenefitDays($manager_id, $month) === 0) {
			$this->vacation_pay[$manager_id]['benefit'] = 0.0;
			$this->vacation_pay[$manager_id]['comment'] = '';
			$this->vacation_pay[$manager_id]['note'] = '';
			return;
		}

		// считаем количество рабочих дней в отпуске
		$benefit_days = $this->vacation_pay[$manager_id]['days'];

		$manager_filter = " AND m.id = $manager_id ";

		$date_start = date('Y-m-d', strtotime("$month-01" . " -1 year"));
		$date_end = "$month-01";

		$billings_table = new Billings();
		$billings_table->getCountWorkDaysInHolidays($date_start, $date_end, $manager_filter);

		foreach ($billings_table->data as $row) {

			// если count 0 - то row['sum'] = 0.0
			if ((int)$row['count'] === 0) {
				$this->vacation_pay[$manager_id]['benefit'] = round($row['sum'], 2);
				$this->vacation_pay[$manager_id]['comment'] = '';
				$this->vacation_pay[$manager_id]['note'] = '';

				// full year == 1 если дата начала работы меньше даты началя для расчета отпускных (текущий месяц минус год)
			} else if ((int)$row['full_year'] === 1) {

				$this->vacation_pay[$manager_id]['benefit'] = round(((float)$row['sum'] / $row['count']) * ($benefit_days / 21), 2);
				$this->vacation_pay[$manager_id]['comment'] =
					number_format(round(((float)$row['sum'] / $row['count']), 2), 2, ',', ' ') .
					" * $benefit_days / 21";

				// человек работает меньше года, высчитываем точное количество месяцев и размер отпускных
			} else {

				$start_work_month = date("Y-m", strtotime($row['start_work_date']));
				$not_working_days = 0;

				$personal = new Personal();
				$all_days = $personal->getTotalWorkingDaysByMonth($start_work_month);    // считаем количество рабочих дней в месяце, в котором сотрудник начал работать

				// считаем количество отработанных дней в первом отработанном месяце
				if ($row['end_work_date'] > '2010-01-01' && strtotime($row['end_work_date']) < strtotime($start_work_month)) {
					$not_working_days = $all_days;
				} else {
					$i = 0;

					$date_start_work = date("Y-m-d", strtotime($row['start_work_date']));
					$date_end_work = date("Y-m-d", strtotime($row['end_work_date']));
					$month_start_work = date("Y-m", strtotime($date_start_work));
					$month_end_work = date("Y-m", strtotime($date_end_work));

					$next_day = $start_work_month . "-01";
					$end_day = date("Y-m-t", strtotime($next_day));

					while ($next_day <= $end_day) {
						if ($personal->isWorkingDay($next_day)) {
							if ($start_work_month === $month_start_work && $next_day < $date_start_work) {
								$i++;
							} else if ($start_work_month === $month_end_work && $next_day > $date_end_work) {
								$i++;
							}
						}
						$next_day = date("Y-m-d", strtotime($next_day . " +1 day"));
					}
					$not_working_days += $i;
				}

				// считаем отпускные по формуле
				// суммарная зп за отработанный период / (кол-во полных месяцев + [отработанные дни / все дни в месяце]) * кол-во отпускных дней / 21
				$this->vacation_pay[$manager_id]['benefit'] = round($row['sum'], 2) /
					($row['count'] - 1 + round((float)(($all_days - $not_working_days) / $all_days), 2)) *
					($benefit_days / 21);
				$this->vacation_pay[$manager_id]['comment'] =
					round($row['sum'], 2) . " / " .
					($row['count'] - 1 + round((float)(($all_days - $not_working_days) / $all_days), 2)) . " * ($benefit_days / 21)";
			}
		}

		if (!isset($this->vacation_pay[$manager_id])) {
			$this->vacation_pay[$manager_id]['benefit'] = 0.0;
			$this->vacation_pay[$manager_id]['comment'] = '';
			$this->vacation_pay[$manager_id]['note'] = '';
		}
	}

	/**
	 * вычисляем количество дней в отпуске, начинающемся в заданном месяце
	 * @param $manager_id
	 * @param $month
	 * @return int
	 */
	protected
	function getBenefitDays($manager_id, $month): int
	{
		// считаем границы дат для запроса - берем месяц до нашего месяца и месяц после
		// чтобы учитывать отпуска, начинаюиеся и заканчивающиеся в разных месяцах
		$dateStart = date('Y-m-d', strtotime("$month-01" . " -1 month"));
		$dateEnd = date('Y-m-d', strtotime("$month-01" . " +2 month"));
		$timetable = new Timetable();
		$timetable->getData(
			[
				"Сотрудник",
				"Дата и время начала",
				"Дата и время окончания",
				"Рабочих дней",
				"Примечание"
			],
			[
				"Сотрудник" => $manager_id,
				"Дата и время начала" => ["field_name" => "Дата и время начала", "operator" => ">=", "value" => "'$dateStart 00:00:00'"],
				"Дата и время конца" => ["field_name" => "Дата и время начала", "operator" => "<", "value" => "'$dateEnd 00:00:00'"],
				"Тип события" => "'Отпуск оплачиваемый'"
			],
			[
				"Дата и время начала"
			]);

		$days_in_month = 0;
		$comment = '';
		$vacation_start_date = null;

		$month_start = date("Y-m-d", strtotime("$month-01"));
		$month_end = date("Y-m-d", strtotime("$month-01" . " +1 month"));

		// счетчик, если отпуск начался в предыдущем месяце и залез на запрашиваемый
		$is_benefit = 1;
		// счетчик, если отпуск начался в нужном месяце, и может иметь хвост в следующем
		$is_benefit_after = 0;

		foreach ($timetable->data as $row) {
			$row_start_date = date("Y-m-d", strtotime($row['Дата и время начала']));
			$row_end_date = date("Y-m-d", strtotime($row['Дата и время окончания']));

			// если запись в запрашиваемом месяце
			if ($row_start_date >= $month_start && $row_start_date < $month_end) {

				// и при этом отпуск начинается с 1 числа месяца и счетчик равен 0
				// -> это часть отпуска, начавшегося в прошлом месяце - не считаем, дальше все пропускаем
				if ($is_benefit === 0 && date('d', $row_start_date) === '01') {
					$is_benefit = 1;
					continue;
				}

				// прибавляем рабочие дни к счетчику отпускных дней
				$days_in_month += (int)$row['Рабочих дней'];
				$comment = $row['Примечание'];

				// запоминаем дату начала отпуска
				if (!isset($vacation_start_date)) {
					$vacation_start_date = $row_start_date;
				}

				// если конец месяца, ставим счетчик, чтобы проверить, нет ли в следующем месяце хвоста отпуска
				if (date('m', strtotime($row_end_date . " +1 day")) !== date('m', strtotime($row_end_date))) {
					$is_benefit_after = 1;
				}
			} else {
				// если это конец месяца до запрашиваемого, смотрим, нет ли хвоста отпуска в запрашиваемом месяце, дни из которого не нужно учитывать
				if (date('m', strtotime($row_end_date . " +1 day")) !== date('m', strtotime($row_end_date))) {
					$is_benefit = 0;
					continue;
				}

				// если это начало месяца и счетчик равен 1 -> это хвост отпуска, который надо учитывать
				if ($is_benefit_after === 1 && date('d', $row_start_date) === '01') {
					$is_benefit_after = 0;

					$days_in_month += (int)$row['Рабочих дней'];
					$comment .= " " . $row['Примечание'];
				}
			}
		}

		// если отпуск есть, сохраняем данные и возвращаем 1
		if ($days_in_month !== 0) {

			$this->vacation_pay[$manager_id]['days'] = $days_in_month;
			$this->vacation_pay[$manager_id]['note'] = $comment;
			$this->vacation_pay[$manager_id]['start_vacation'] = $vacation_start_date;

			return 1;
		}

		// если отпускных дней не нашлось
		return 0;
	}

	protected
	function orderBorderDesc(array $motivation): array
	{
		$borders = array_column($motivation, 'border');
		array_multisort($borders, SORT_DESC, $motivation);
		return $motivation;
	}

	protected
	function orderBorderAsc(array $motivation): array
	{
		$borders = array_column($motivation, 'border');
		array_multisort($borders, SORT_ASC, $motivation);
		return $motivation;
	}

    /**
     * @param string $day
     * @param string $base
     * @param string $type
     * @return array|mixed|null
     */
	protected
	function getCoef(string $day, string $base, string $type)
	{
		$type_nums = [
			"base_salary_auto_per_day" => 1,
			"trial_salary_auto_per_day" => 2,
			"another_bonus" => 4,
			"firstly_typical_sales_bonus" => 5,
			"secondary_typical_sales_bonus" => 6,
			"firstly_complex_sales_bonus" => 7,
			"secondary_complex_sales_bonus" => 8,
			"plan_realization_coef" => 9,
			"typical_time_period_coef" => 10,
			"complex_time_period_coef" => 11,
			"agency_time_period_coef" => 12,
			"partner_time_period_coef" => 12,
			"sum_incomes_typical_coef" => 14,

		];
		$return_array = [];

		$array_types = [
			"sum_incomes_typical_coef",
			"secondary_typical_sales_bonus",
			"secondary_complex_sales_bonus",
			"plan_realization_coef",
			"typical_time_period_coef",
			"complex_time_period_coef",
			"agency_time_period_coef",
			"partner_time_period_coef"];
		$need_array = in_array($type, $array_types, true);
		$motivations =
			$this->motivations["motivations"]['base'][$base]['type'][$type_nums[$type]];

		foreach ($motivations as $motivation) {

			if ($motivation['start'] > $day || ($motivation['end'] > '1971-01-01' && $motivation['end'] < $day)) {
				continue;
			}

			foreach ($motivation['positions'] as $position) {
				if ($position['start'] > $day || ($position['end'] > '1971-01-01' && $position['end'] < $day)) {
					continue;
				}

				if ($need_array) {
					$return_array[] = $motivation["val"];
				} else {
					return $motivation["val"];
				}
			}
		}
		if ($need_array) {
			return $return_array;
		}
		return null;
	}

    /**
     * @param float $percent
     * @return mixed
     */
	protected
    function getPlanRealizationCoef(float $percent)
	{
		$motivation_plans = $this->orderBorderDesc($this->getCoef(date('Y-m-t', strtotime($this->month)), 'Личное', 'plan_realization_coef'));

		foreach ($motivation_plans as $motivation_plan) {
			if ($percent >= $motivation_plan['border']) {
				return $motivation_plan['rate'];
			}
		}
		return $motivation_plans[count($motivation_plans) - 1]['rate'];
	}

    /**
     *
     */
	private function getCurrentMonthPayments()
	{
		if (empty($this->manager)) {
			return;
		}

		$payment = new Table ('Выплаты зарплаты');
		$filter = [];
		$filter['Сотрудник'] = $this->manager['ID'];
		$filter[] = 'AND';
		$filter['Месяц'] = '\'' . date("Y-m-01 00:00:00", strtotime($this->month)) . '\'';

		if ((int)date("d") < 10 && ($this->month >= date("Y-m", strtotime(date("Y-m-01") . " - 1 month")))) {
			$filter[] = 'AND';
			$filter[] = '(';
			$filter['Вид'] = '\'Нал\'';
			$filter[] = 'OR';
			$filter[] = ' f20621 != \'Зарплата\'';
			$filter[] = ')';
		}
		// только администраторы видят управленческие начисления
		global $user;
		if ((int)$user['group_id'] !== 1) {
//			$filter[] = 'AND';
//			$filter['Управленческий'] = '\'Нет\''; TODO: Узнать необходимы ли управленческие
		}
		$payment->getData(
			[
				[
					'field' => 'Сумма',
					'function' => 'SUM',
					'alias' => 'PaymentsSum'
				]
			], $filter
			, []
			, ['raw_condition' => 1]
		);

		$paid_total = (float)$payment->data[0]['PaymentsSum'];
		$this->salary['salary']['pivot']['auto']['paid_total'] = $this->prepareNumericData($paid_total);
		$this->salary['salary']['pivot']['manual']['paid_total'] = $this->prepareNumericData($paid_total);
	}

	private function createPivotData()
	{
		$this->salary['salary']['pivot_main']['month'] = [
			'title' => 'Месяц расчета',
			'value' => $this->month,
			'display' => 1
		];
		$this->salary['salary']['pivot_main']['position'] = [
			'title' => 'Должность',
			'value' => $this->manager['Должность'],
			'display' => 1
		];
		$this->salary['salary']['pivot_main']['department'] = [
			'title' => 'Отдел',
			'value' => $this->manager['Отдел'],
			'display' => 1
		];
		$this->salary['salary']['pivot_main']['work_days'] = [
			'title' => 'Отработано дней',
			'value' => ($this->total_work_days - $this->manager_month_not_work_days) . " из " .
				$this->total_work_days,
			'display' => 1
		];
		$this->salary['salary']['pivot_main']['benefit_headers'] = [
			'title' => '-',
			'value' => [
				'auto' => 'Автовычисление',
				'manual' => 'Начислено фактически',
			],
			'display' => 1,
			'bold' => 1
		];

		$base_salary_result_auto = $this->salary['salary']['pivot']['base']['auto']['base_salary_result'];
		$this->formatNumericData($base_salary_result_auto);
		$this->salary['salary']['pivot_main']['base_salary_calculation'] = [
			'title' => 'Оклад расчет',
			'value' => [
				'auto' => $base_salary_result_auto .
					" = ( " . $this->salary['salary']['pivot']['base']['auto']['base_salary'] . " * " .
					($this->total_work_days - $this->manager_month_not_work_days) .
					" / " . $this->total_work_days . " ) ",
				'manual' => $this->salary['salary']['pivot']['base']['manual']['base_salary']
			],
			'display' => 1
		];

//		if ((int)$benefit_win_sum_auto != 0) {
//			$data['pivot_main']['benefit_win_result'] = [
//				'title' => 'Премия за победы',
//				'value' => [
//					'auto' => $benefit_win_sum_auto,
//					'manual' => $benefit_win_sum_manual,
//				],
//				'display' => 1
//			];
//		}
//		if ((int)$benefit_win_sum_auto != 0) {
//			$data['pivot_main']['benefit_win_result'] = [
//				'title' => 'Премия за победы',
//				'value' => [
//					'auto' => $benefit_win_sum_auto,
//					'manual' => $benefit_win_sum_manual,
//				],
//				'display' => 1
//			];
//		}
//		$data['pivot_main']['benefit_win_correct'] = [
//			'title' => 'Корректировка за победы',
//			'value' => [
//				'auto' => $benefit_correction_win_sum_auto,
//				'manual' => $benefit_correction_win_sum_manual,
//			],
//			'display' => ($benefit_correction_win_sum_auto + $benefit_correction_win_sum_manual != 0.0) ? 1 : 0
//		];
//
//		if ((int)$benefit_done_sum_auto != 0) {
//			$data['pivot_main']['benefit_done_result'] = [
//				'title' => 'Премия за исполнения',
//				'value' => [
//					'auto' => $benefit_done_sum_auto,
//					'manual' => $benefit_done_sum_manual,
//				],
//				'display' => 1
//			];
//		}
//		$data['pivot_main']['benefit_done_correct'] = [
//			'title' => 'Корректировка за исполнения',
//			'value' => [
//				'auto' => $benefit_correction_done_sum_auto,
//				'manual' => $benefit_correction_done_sum_manual,
//			],
//			'display' => ($benefit_correction_done_sum_auto + $benefit_correction_done_sum_manual != 0.0) ? 1 : 0
//		];
//
//		$data['pivot_main']['benefit_other_result'] = [
//			'title' => 'Начисления прочие',
//			'value' => [
//				'auto' => $benefit_other_total_auto,
//				'manual' => $benefit_other_total_manual,
//				'comment' => $benefit_other_total_manual_comment,
//			],
//			'display' => 1
//		];
		$benefit_holiday_auto = $this->salary['salary']['pivot']['holiday']['auto']['benefit_holiday'];
		$this->formatNumericData($benefit_holiday_auto);
		$this->salary['salary']['pivot_main']['benefit_holiday_result'] = [
			'title' => 'Отпускные',
			'value' => [
				'auto' => $this->benefit_holiday_auto_comment !== '' ? $benefit_holiday_auto . " = ( " . $this->benefit_holiday_auto_comment . " )" : $benefit_holiday_auto,
				'comment_auto' => $this->benefit_holiday_auto_note,
				'manual' => $this->salary['salary']['pivot']['manual']['benefit_holiday_total'],
			],
			'display' => 1
		];

//		$data['pivot_main']['salary_result'] = [
//			'title' => 'Зарплата итог',
//			'value' => [
//				'auto' => $manager_salary_total_auto,
//				'manual' => $manager_salary_total_manual,
//			],
//			'display' => 1,
//			'bold' => 1
//		];

		$this->salary['salary']['pivot_main']['paid_result'] = [
			'title' => 'Выплачено',
			'value' => [
				'auto' => $this->salary['salary']['pivot']['auto']['paid_total'],
				'manual' => $this->salary['salary']['pivot']['auto']['paid_total'],
			],
			'display' => 1,
			'bold' => 1
		];


		$this->salary['salary']['pivot_main']['paid_remain_result'] = [
			'title' => 'Остаток',
			'value' => [
				'auto' => $this->prepareNumericData(
					$this->prepareNumber($this->salary['salary']['pivot']['auto']['paid_remain']) +
					$this->prepareNumber($this->salary['salary']['pivot']['auto']['remain_past_paid'])
				),
				'manual' => $this->prepareNumericData(
					$this->prepareNumber($this->salary['salary']['pivot']['manual']['paid_remain']) +
					$this->prepareNumber($this->salary['salary']['pivot']['auto']['remain_past_paid'])
				),
				'comment' => $this->salary['salary']['pivot']['manual']['paid_remain_comment']
			],
			'display' => 1,
			'bold' => 1
		];
	}

	private function getPayments()
	{
		$this->getCurrentMonthPayments();
		$this->getRemainPastPaid();
	}

	private function getRemainPastPaid()
	{
		if (empty($this->manager)) {
			return;
		}

		$result = [];

		$cur_month = "$this->month-01";
		$filter = [];
		$filter["Сотрудник"] = $this->manager['ID'];
		$filter['status'] = 0;
		$filter[] = " AND f20711 <= '$cur_month 23:59:59'";

		$calculations = new Table("Начисления зарплаты");
		$calculations->getData([], $filter, [], ['open_related' => ['Тип'], 'raw_condition' => 1]);
		$cur_month_paid_remain = 0;
		foreach ($calculations->data as $calculation) {
			$calculation_month = date("Y-m-d", strtotime($calculation["Месяц"]));
			$result['items'][$calculation_month]['calculations'][$calculation["ID"]]['type'] = $calculation["Тип"]["Название"];
			$result['items'][$calculation_month]['calculations'][$calculation["ID"]]['calculation_sum'] = $calculation["Сумма начисления"];
			$result['items'][$calculation_month]['calculations'][$calculation["ID"]]['description'] = $calculation["Примечание"];
			$result['items'][$calculation_month]['total']['calculation_sum'] += $calculation["Сумма начисления"];
			$result['items'][$calculation_month]['total']['paid_remain'] += $calculation["Сумма начисления"];
			$result['total']['calculation_sum'] += $calculation["Сумма начисления"];
			if ($calculation_month === $cur_month) {
				$cur_month_paid_remain += $calculation["Сумма начисления"];
			}
		}

		$filter = [];
		$filter["Сотрудник"] = $this->manager['ID'];
		$filter['status'] = 0;
		$filter[] = " AND f20631 <= '$cur_month 23:59:59'";
		$payments = new Table("Выплаты зарплаты");
		$payments->getData([], $filter, [], ['raw_condition' => 1]);

		foreach ($payments->data as $payment) {
			$payment_month = date("Y-m-d", strtotime($payment["Месяц"]));
			$result['items'][$payment_month]['payments'][$payment["ID"]]['type'] = $payment["Тип"];
			$result['items'][$payment_month]['payments'][$payment["ID"]]['payment_sum'] = $payment["Сумма"];
			$result['items'][$payment_month]['payments'][$payment["ID"]]['description'] = $payment["Примечание"];
			$result['items'][$payment_month]['total']['payment_sum'] += $payment["Сумма"];
			$result['items'][$payment_month]['total']['paid_remain'] -= $payment["Сумма"];
			$result['total']['payment_sum'] += $payment["Сумма"];
			if ($payment_month == $cur_month) {
				$cur_month_paid_remain -= $payment["Сумма"];
			}
		}
		$total_sum = 0.0;
		foreach ($result['items'] as $month => $data) {
			$total_sum += $result['items'][$month]['total']['paid_remain'];
			$result['items'][$month]['total']['paid_remain_total'] = $total_sum;
		}

		$result['total']['paid_remain'] = $result['total']['calculation_sum'] - $result['total']['payment_sum'];
		$result['total']['paid_remain_without_cur_month'] = $result['total']['paid_remain'] - $cur_month_paid_remain;

		$this->salary['salary']['pivot']['auto']['remain_past_paid'] = $result['total']['paid_remain_without_cur_month'];
		$this->salary['salary']['pivot']['manual']['remain_past_paid'] = $result['total']['paid_remain_without_cur_month'];

		$this->salary['salary']['pivot']['auto']['paid_remain_total'] =
			$this->salary['salary']['pivot']['auto']['paid_remain'] +
			$this->salary['salary']['pivot']['auto']['remain_past_paid'];

		$this->salary['salary']['pivot']['manual']['paid_remain_total'] =
			$this->salary['salary']['pivot']['manual']['paid_remain'] +
			$this->salary['salary']['pivot']['manual']['remain_past_paid'];

		if ($this->salary['salary']['pivot']['manual']['remain_past_paid'] !== 0) {
			$t[0] = $this->salary['salary']['pivot']['manual']['remain_past_paid'];
			$this->formatNumericData($t);
			$this->salary['salary']['pivot']['manual']['paid_remain_comment'] = "с учетом остатка за прошлые месяцы (" . $t[0] . ")";
		}
	}

	protected function prepareNumber($num): float
	{
		$num = str_replace(array(' ', ','), array('', '.'), $num);
		return (float)$num;
	}

	protected function prepareNumericData($data): string
	{
		if (is_float($data)) {
			$data = number_format($data, 2, ',', ' ');
		} else if (is_int($data)) {
			$data = number_format($data, 0, ',', ' ');
		}
		if (is_null($data)) {
			echo '';
		}
		return $data;
	}
}