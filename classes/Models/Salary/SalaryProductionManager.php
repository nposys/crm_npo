<?php


namespace classes\Models\Salary;


class SalaryProductionManager
{

	/*
// ================================================================================================
if($bShowProductionSearch)
{
  // вычисляем зарплату по менеджерам-аналитикам
  $data_pr_srch = array();

  $manager_name = "Иванова Е.В.";

  // -----------------------------------------
  // вычисляем по платным поискам
  $sum_paid = 0;
  $benefit_rate = 0.1;
  $sqlQuery = "SELECT
                  tiio.id as Id
                 ,tord.f7071 as OrderId
                 ,tord.f4431 as OrderDescription
                 ,tiio.f10920 as PaymentSum
                 ,DATE(tiio.f11271) as PaymentDate
                 ,DATE(tiio.f11291) as InvoiceDate
                 ,DATE(tiio.f11281) as OrderDate
                 ,DATE(tsch.f6941) as DateStart
                 ,DATE(tsch.f6951) as DateFinish
                 ,tcmp.f435 as ClientName
               FROM
                 ".DATA_TABLE.$tableIncomeInvoiceOrders." as tiio
                 LEFT JOIN ".DATA_TABLE.$tableOrders." as tord ON tord.id = tiio.f10890 AND tord.status = 0
                 LEFT JOIN ".DATA_TABLE.$tableOrdersSearch." as tsch ON tsch.f5411 = tord.id AND tsch.status = 0
                 LEFT JOIN ".DATA_TABLE.$tableClients." as tcmp ON tcmp.id = tord.f4441 AND tcmp.status = 0
               WHERE
                 DATE(tiio.f11271) BETWEEN '".$date1."' AND '".$date2."'
                 AND tiio.status = 0
                 AND tiio.f11741 = 4
                 ".$managerFilter."
               ORDER BY
                  tiio.f11271
            ";
  $res = sql_query($sqlQuery);
  while($row = sql_fetch_array($res))
  {
    $inc_id = $row['Id'];

    $data_pr_srch[$manager_name]["income_types"]["Платный поиск"]['orders'][$inc_id]['order_id'] = $row['OrderId'];
    $data_pr_srch[$manager_name]["income_types"]["Платный поиск"]['orders'][$inc_id]['client'] = $row['ClientName'];
    $data_pr_srch[$manager_name]["income_types"]["Платный поиск"]['orders'][$inc_id]['description'] = $row['OrderDescription'];
    $data_pr_srch[$manager_name]["income_types"]["Платный поиск"]['orders'][$inc_id]['dates'] = $row['DateStart']." - ".$row['DateFinish'];
    $data_pr_srch[$manager_name]["income_types"]["Платный поиск"]['orders'][$inc_id]['date_payment'] = $row['PaymentDate'];
    $data_pr_srch[$manager_name]["income_types"]["Платный поиск"]['orders'][$inc_id]['sum'] = $row['PaymentSum'];
    $data_pr_srch[$manager_name]["income_types"]["Платный поиск"]['orders'][$inc_id]['benefit_rate'] = $benefit_rate;
    $data_pr_srch[$manager_name]["income_types"]["Платный поиск"]['orders'][$inc_id]['benefit'] = $row['PaymentSum'] * $benefit_rate;

    $sum_paid += $row['PaymentSum'];
  }

  $data_pr_srch[$manager_name]["income_types"]["Платный поиск"]['total']['sum'] = $sum_paid;
  $data_pr_srch[$manager_name]["income_types"]["Платный поиск"]['total']['benefit'] = $sum_paid * $benefit_rate;

  // -----------------------------------------
  // вычисляем по бесплатным поискам
  $benefit_sum = 0.0;
  $sqlQuery = "SELECT
                  tord.f7071 as OrderId
                 ,tord.f4431 as OrderDescription
                 ,DATE(tord.f6591) as OrderDate
                 ,DATE(tsch.f6941) as DateStart
                 ,DATE(tsch.f6951) as DateFinish
                 ,tcmp.f435 as ClientName
               FROM
                 ".DATA_TABLE.$tableOrders." as tord
                 LEFT JOIN ".DATA_TABLE.$tableOrdersSearch." as tsch ON tsch.f5411 = tord.id AND tsch.status = 0
                 LEFT JOIN ".DATA_TABLE.$tableClients." as tcmp ON tcmp.id = tord.f4441 AND tcmp.status = 0
               WHERE
                 DATE(tsch.f6951) BETWEEN '".$date1."' AND '".$date2."'
                 AND tord.status = 0
                 AND tord.f4451 = 4
                 AND tord.f13071 = 2
                  ".$managerFilter."
              ORDER BY
                  tord.f6591
            ";
  $res = sql_query($sqlQuery);
  while($row = sql_fetch_array($res))
  {
    $order_id = $row['OrderId'];

    $data_pr_srch[$manager_name]["income_types"]["Пробный поиск"]['orders'][$order_id]['order_id'] = $row['OrderId'];
    $data_pr_srch[$manager_name]["income_types"]["Пробный поиск"]['orders'][$order_id]['client'] = $row['ClientName'];
    $data_pr_srch[$manager_name]["income_types"]["Пробный поиск"]['orders'][$order_id]['description'] = $row['OrderDescription'];
    $data_pr_srch[$manager_name]["income_types"]["Пробный поиск"]['orders'][$order_id]['date_payment'] = $row['PaymentDate'];
    $data_pr_srch[$manager_name]["income_types"]["Пробный поиск"]['orders'][$order_id]['dates'] = $row['DateStart']." - ".$row['DateFinish'];
    $data_pr_srch[$manager_name]["income_types"]["Пробный поиск"]['orders'][$order_id]['sum'] = $row['PaymentSum'];

    $ts1 = strtotime($row['DateFinish']);
    $ts2 = strtotime($row['DateStart']);
    $seconds_diff = $ts1 - $ts2;
    $days_diff = $seconds_diff/86400;

    //ставка 200 руб. за 14 дней
    $benefit = 200.0/14.0*$days_diff;
    $data_pr_srch[$manager_name]["income_types"]["Пробный поиск"]['orders'][$order_id]['benefit'] = $benefit;

    $benefit_sum += $benefit;
  }

  $data_pr_srch[$manager_name]["income_types"]["Пробный поиск"]['total']['sum'] = 0;
  $data_pr_srch[$manager_name]["income_types"]["Пробный поиск"]['total']['benefit'] = $benefit_sum;

  // -------------------------------------
  // итоги
  $data_pr_srch[$manager_name]['total']['sum'] = $sum_paid;
  $data_pr_srch[$manager_name]['total']['benefit'] = $data_pr_srch[$manager_name]["income_types"]["Пробный поиск"]['total']['benefit'] +  $data_pr_srch[$manager_name]["income_types"]["Платный поиск"]['total']['benefit'];
  //print_r($data_pr_srch);
}

*/
}