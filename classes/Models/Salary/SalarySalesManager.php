<?php


namespace classes\Models\Salary;


use classes\Base\Table;
use classes\Tables\PositionsOrdersInvoices;

class SalarySalesManager extends Salary
{
	public function calculateSalary()
	{
		parent::calculateSalary();
		$this->getBonus();
	}

	public function getBonus()
	{
		if (!isset($this->motivations)) {
			return;
		}

		$manager_position = $this->manager['Должность'];
		$manager_id = $this->manager['ID'];

		$sales_plans = new Table('Планы продаж');
		$sales_plans->getData(
			[
				'План',
				'Тип услуг',
				'Менеджер'
			],
			[
				'status' => 0,
				'Месяц' => '\'' . date('Y-m-01', strtotime($this->month)) . '\'',
				'Сотрудник' => $manager_id
			]);

		$start_date = date('Y-m-01 00:00:00', strtotime($this->month));
		$finish_date = date('Y-m-t 23:59:59', strtotime($this->month));

		if ($sales_plans->num_rows > 0) {
			$table = new PositionsOrdersInvoices();
			$table->getSalesManagerInvoices($manager_position, $sales_plans->data[0]['Менеджер'], $start_date, $finish_date);
			$this->prepareReport($table->data);
			$bonus = $this->getSalesManagerBonus($sales_plans->data[0]);
			$this->salary['salary']['pivot']['auto']['benefit_win_total'] = $this->prepareNumericData($bonus);
			$this->salary['salary']['pivot']['manual']['benefit_win_total'] = $this->prepareNumericData($bonus);
			$this->salary['salary_report'] = $this->salary_report;
		}
	}

	private
	function getSalesManagerBonus($plan_data)
	{
		$plan = (float)$plan_data['План'];
		$income_types = ['Первичные', 'Вторичные'];
		$order_types = ['Обычные', 'Комплексные'];

		$benefit_result = 0;
		$sum_result = 0;
		$plan_execution = 0.0;

		$manager_data = $this->salary_report;

		foreach ($income_types as $income_type) {
			$sum_income_by_type = 0;
			$sum_bonus_by_type = 0;
			foreach ($order_types as $order_type) {
				$sum_income = 0;
				$bonus_sum = 0;
				foreach ($manager_data['income_types'][$income_type]['service_groups'][$order_type]['orders'] as $date_payment => $orders_array) {
					foreach ($orders_array as $iioId => $order) {
						$order_income =$this->prepareNumber($order['auto']['sum']);
						$sum_income += $order_income;
						$coef = $this->getIncomeCoef($order_type, $income_type, $order['date_payment']);

						$bonus = $coef * $order_income;
						$bonus_sum += $bonus;
						$this->fillBonusInfo($income_type, $order_type, $date_payment, $iioId, $coef, $bonus);
					}
				}
				$this->fillOrdersTotalInfo($income_type, $order_type, $sum_income, $bonus_sum);

				$sum_income_by_type += $sum_income;
				$sum_bonus_by_type += $bonus_sum;

				if ($income_type === "Первичные") {
					$plan_sum = $plan;

				} else {
					$plan_sum = 0.0;
				}
				if ($income_type === "Первичные" && $order_type === "Обычные") {
					$plan_execution = round((float)($sum_income) / $plan * 100.00);
				}
				$plan_realization_coef = $this->getPlanRealizationCoef($plan_execution);
				$benefit_final = $bonus_sum * $plan_realization_coef;
				$this->fillPlanRealizationInfo($income_type, $order_type, $plan_sum, $plan_execution, $plan_realization_coef, $benefit_final);

				if ($income_type === "Первичные") {
					$benefit_result += $benefit_final;
					$sum_result += $sum_income;
				}
			}
			$this->fillTotalInfoByOrderType($income_type, $sum_income_by_type, $sum_bonus_by_type);
		}
		$this->fillTotalResult($benefit_result, $sum_result);

		return $benefit_result;
	}

	private function getIncomeCoef($order_type, $income_type, $date_payment): float
	{
		if ($income_type === 'Первичные') {
			if ($order_type === 'Обычные') {
				$coef = $this->getCoef(date('Y-m-d', strtotime($date_payment)), 'Личное', 'firstly_typical_sales_bonus');
				return (float)$coef['rate'];
			}

			$coef = $this->getCoef(date('Y-m-d', strtotime($date_payment)), 'Личное', 'firstly_complex_sales_bonus');
			return (float)$coef['rate'];
		}
		if ($order_type === 'Обычные') {
			$coef = $this->getCoef(date('Y-m-d', strtotime($date_payment)), 'Личное', 'secondary_typical_sales_bonus');
			return (float)$coef['rate'];
		}
		$coef = $this->getCoef(date('Y-m-d', strtotime($date_payment)), 'Личное', 'secondary_complex_sales_bonus');
		return (float)$coef['rate'];
	}

	private function fillOrdersTotalInfo($income_type, $order_type, float $sum_income, float $bonus_sum)
	{
		$this->salary_report['income_types'][$income_type]['service_groups'][$order_type]['total']['sum'] =
			$this->prepareNumericData($sum_income);
		$this->salary_report['income_types'][$income_type]['service_groups'][$order_type]['total']['benefit'] =
			$this->prepareNumericData($bonus_sum);
	}

	private function fillTotalInfoByOrderType($income_type, float $sum_income_by_type, float $sum_bonus_by_type)
	{
		$this->salary_report['income_types'][$income_type]['total']['sum'] = $this->prepareNumericData($sum_income_by_type);
		$this->salary_report['income_types'][$income_type]['total']['benefit'] = $this->prepareNumericData($sum_bonus_by_type);
	}

	private function fillTotalResult(float $benefit_result, float $sum_result)
	{
		$this->salary_report['benefit_result'] = $this->prepareNumericData($benefit_result);
		$this->salary_report['sum_result'] = $this->prepareNumericData($sum_result);
	}

	private function getOrderType($service_type): string
	{
		if ($service_type !== 'Подготовка документации') {
			return "Обычные";
		}

		return "Комплексные";
	}

	private function getIncomeType($income_type, $payment_date, $first_payment_date): string
	{
		if (!empty(trim($income_type))) {
			return trim($income_type);
		}

		$ts1 = strtotime($payment_date);
		$ts2 = strtotime($first_payment_date);
		$seconds_diff = $ts1 - $ts2;
		$days_diff = $seconds_diff / 86400;

		if ($first_payment_date !== "0000-00-00" && $days_diff > 35) { #TODO вытаскивать border из системы мотивации
			return "Вторичные";
		}
		return "Первичные";
	}

	private function prepareReport($data)
	{
		foreach ($data as $income) {
			$date_payment = $income['Дата поступления'];
			$iioId = $income['ID'];

			$income_type = $this->getIncomeType($income['Тип поступления'], $income['Дата поступления'], $income['Дата первого поступления']);
			$order_type = $this->getOrderType($income['Вид услуги']);

			$this->salary_report['income_types'][$income_type]['service_groups'][$order_type]['orders'][$date_payment][$iioId]['income_id'] = $income['ID'];
			$this->salary_report['income_types'][$income_type]['service_groups'][$order_type]['orders'][$date_payment][$iioId]['income_num'] = $income['Номер счета'];
			$this->salary_report['income_types'][$income_type]['service_groups'][$order_type]['orders'][$date_payment][$iioId]['order_id'] = $income['ID Заказа'];
			$this->salary_report['income_types'][$income_type]['service_groups'][$order_type]['orders'][$date_payment][$iioId]['order_num'] = $income['Номер заказа'];
			$this->salary_report['income_types'][$income_type]['service_groups'][$order_type]['orders'][$date_payment][$iioId]['client_id'] = $income['ID Клиента'];
			$this->salary_report['income_types'][$income_type]['service_groups'][$order_type]['orders'][$date_payment][$iioId]['client'] = $income['Название клиента'];
			$this->salary_report['income_types'][$income_type]['service_groups'][$order_type]['orders'][$date_payment][$iioId]['source_name'] = $income['Название клиента плательщика'];
			$this->salary_report['income_types'][$income_type]['service_groups'][$order_type]['orders'][$date_payment][$iioId]['source_type'] = $income['Тип клиента плательщика'];
			$this->salary_report['income_types'][$income_type]['service_groups'][$order_type]['orders'][$date_payment][$iioId]['service'] = $income['Вид услуги'];
			$this->salary_report['income_types'][$income_type]['service_groups'][$order_type]['orders'][$date_payment][$iioId]['description'] = $income['Описание'];
			$this->salary_report['income_types'][$income_type]['service_groups'][$order_type]['orders'][$date_payment][$iioId]['date_payment'] = $income['Дата поступления'];
			$this->salary_report['income_types'][$income_type]['service_groups'][$order_type]['orders'][$date_payment][$iioId]['date_firstpayment'] = $income['Дата первого поступления'];
			$this->salary_report['income_types'][$income_type]['service_groups'][$order_type]['orders'][$date_payment][$iioId]['incometype'] = $income_type;

			$this->salary_report['income_types'][$income_type]['service_groups'][$order_type]['orders'][$date_payment][$iioId]['auto']['sum'] =
				$this->prepareNumericData((float)$income['Оплачено сумма заказ']);
			if ($this->override_manual_with_auto) {
				$this->salary_report['income_types'][$income_type]['service_groups'][$order_type]['orders'][$date_payment][$iioId]['manual']['sum'] =
					$this->salary_report['income_types'][$income_type]['service_groups'][$order_type]['orders'][$date_payment][$iioId]['auto']['sum'];
			}

			$this->salary_report['user'] = $income['Менеджер клиента'];
			$this->salary_report['position'] = $income['Должность'];
		}
	}

	public function fillBonusInfo($income_type, $order_type, $date_payment, $iioId, $coef, $bonus)
	{
		$this->salary_report['income_types'][$income_type]['service_groups'][$order_type]['orders'][$date_payment][$iioId]['auto']['benefit_rate'] =
			$this->prepareNumericData($coef);
		$this->salary_report['income_types'][$income_type]['service_groups'][$order_type]['orders'][$date_payment][$iioId]['auto']['benefit'] =
			$this->prepareNumericData($bonus);

		if ($this->override_manual_with_auto) {
			$this->salary_report['income_types'][$income_type]['service_groups'][$order_type]['orders'][$date_payment][$iioId]['manual']['benefit_rate'] =
				$this->salary_report['income_types'][$income_type]['service_groups'][$order_type]['orders'][$date_payment][$iioId]['auto']['benefit_rate'];

			$this->salary_report['income_types'][$income_type]['service_groups'][$order_type]['orders'][$date_payment][$iioId]['manual']['benefit'] =
				$this->salary_report['income_types'][$income_type]['service_groups'][$order_type]['orders'][$date_payment][$iioId]['auto']['benefit'];
		}
	}

	public function fillPlanRealizationInfo($income_type, $order_type, $plan_sum, $plan_execution, $plan_realization_coef, $benefit_final)
	{
		$this->salary_report['income_types'][$income_type]['service_groups'][$order_type]['total']['plan_sum'] =
			$this->prepareNumericData($plan_sum);
		$this->salary_report['income_types'][$income_type]['service_groups'][$order_type]['total']['plan_percent'] =
			$plan_execution;
		$this->salary_report['income_types'][$income_type]['service_groups'][$order_type]['total']['benefit_final'] =
			$this->prepareNumericData($benefit_final);
		$this->salary_report['income_types'][$income_type]['service_groups'][$order_type]['total']['auto']['plan_coef'] =
			$plan_realization_coef;

		if ($this->override_manual_with_auto) {
			$this->salary_report['income_types'][$income_type]['service_groups'][$order_type]['total']['manual']['plan_coef'] =
				$plan_realization_coef;
		}
	}
}