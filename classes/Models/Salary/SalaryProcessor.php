<?php
/**
 * Created by PhpStorm.
 * User: Alt
 * Date: 29.10.2019
 * Time: 15:47
 */

namespace classes\Models\Salary;

use classes\Base\Common;
use classes\Base\Table;
use classes\Enums\CrmEnums\MotivationTypes;
use classes\Enums\CrmEnums\Positions;
use classes\Tables\EmployeePositions;
use classes\Tables\Personal;

class SalaryProcessor extends Common
{
	public $manager_id;
	public $manager_data;
	public $month;
	public $total_work_days;
	public $use_manual_holidays;
	public $override_manual_with_auto;
	public $override_manual_with_auto_common;
	public $override_totals_manual_with_auto;
	public $show_dismissed;
	public $motivations;
	public $future_payment;
	private $departments;
	private $positions;

	public function __construct($month,
	                            $manager_id = 0,
	                            $override_manual_with_auto = '',
	                            $override_manual_with_auto_common = '',
	                            $show_dismissed = '',
	                            $override_totals_manual_with_auto = '',
	                            $future_payment = "")
	{
		$this->month = $month;
		$this->manager_id = $manager_id;

		$this->show_dismissed = ($show_dismissed === 'on');
		$this->future_payment = ($future_payment === 'on');

		// пока данных не накоплено по зарплатам берем в автоначисления отпускные из ручных начислений
		$this->use_manual_holidays = false;
		$this->override_manual_with_auto = ($override_manual_with_auto === 'on');
		$this->override_totals_manual_with_auto = ($override_totals_manual_with_auto === 'on');
		$this->override_manual_with_auto_common = ($override_manual_with_auto_common === 'on');

		$personal = new Personal();
		$this->total_work_days = $personal->getTotalWorkingDaysByMonth($this->month);

		$this->manager_data = '';
		if ($manager_id > 0) {
			$this->manager_data = $personal->getDataById($manager_id);
		}
	}

	/**
	 * вычисляет зарплату конкретного сотрудника
	 * @param int $manager_id
	 * @param int $department_id
	 * @return array
	 */

	public function calculateSalary($manager_id = 0, $department_id = 0): array
	{
		$salary_data = [];
		$personal = new Personal();

		$all_managers = [];

		// определяем систему мотивации
		$this->buildMotivationData($manager_id);

		$positions_table = new Table('Должности');
		$this->positions = $positions_table->getData(
			[
				'ID',
				'Название'
			],
			[]
		);

		$this->departments = $personal->getDepartments($manager_id);
		$managersByMonth = $this->getManagersByDepartments($manager_id, $department_id);

		foreach ($managersByMonth as $department => $positions) {
			$head_manager = [];
			foreach ($positions as $position => $managers) {
				foreach ($managers as $manager_name => $manager) {
                    $all_managers[(int)$manager['ID']]['fio'] = $manager['Фамилия Инициалы'];
                    $all_managers[(int)$manager['ID']]['user_id'] = (int)$manager['Пользователь'];

                    if (!$this->show_dismissed && $manager['Работает сейчас'] === 'Нет' &&
						($manager['Дата увольнения'] < $this->month . "-01 00:00:00")) {
						continue;
					}

					// если передали идентификатор менеджера, то пропускаем всех кроме него
					if (($manager_id > 0) && $manager_id !== (int)$manager['ID']) {
						continue;
					}

					$salary = $this->getSalaryCounter($manager, (int)$manager['Должность']);
					if ($salary instanceof SalaryHeadManager) {
						$head_manager = $manager;
						$head_manager_position = $position;
						continue;
					}
					$salary->calculateSalary();
					$salary->setTotalSalaryResult();

					$salary_data[$department][$position][$manager_name] = $salary->getSalary();
					$salary_data[$department][$position][$manager_name]['manager'] = $manager;
				}
			}

			$salary = $this->getSalaryHeadCounter($head_manager, (int)$head_manager['Должность']);
			$salary->setDepartmentResults($salary_data[$department]);
			$salary->prepareTotals();
			if (!empty($head_manager) && !empty($head_manager_position)) {
				$salary->calculateSalary();
				$salary->setTotalSalaryResult();
				$salary_data[$department][$head_manager_position][$head_manager['Фамилия Инициалы']] = $salary->getSalary();
				$salary_data[$department][$head_manager_position][$head_manager['Фамилия Инициалы']]['manager'] = $head_manager;
			}
			$salary_data[$department]['totals'] = $salary->getTotals();
		}

		return [
			'salary_data' => $salary_data,
            'managers' => $all_managers
		];
	}

	/*
	 * заполняем данные о системе мотивации
	 */
	private function buildMotivationData($manager_id = 0)
	{
		$employee_positions_table = new EmployeePositions();
		$employee_positions_table->getEmployeeMotivation($manager_id);

		foreach ($employee_positions_table->data as $row) {

			$position['start'] = date('Y-m-d', strtotime($row['employee_history_start']));
			$position['end'] = date('Y-m-d', strtotime($row['employee_history_end']));

			$this->motivations[(int)$row["manager_id"]]["motivations"]['base'][$row['motivation_base']]
			['type'][$row['motivation_type']][$row['motivation_id']]['positions'][] =
				$position;

			$this->motivations[(int)$row["manager_id"]]["motivations"]['base'][$row['motivation_base']]
			['type'][$row['motivation_type']][$row['motivation_id']]['start'] =
				date('Y-m-d', strtotime($row['motivation_start']));

			$this->motivations[(int)$row["manager_id"]]["motivations"]['base'][$row['motivation_base']]
			['type'][$row['motivation_type']][$row['motivation_id']]['end'] =
				date('Y-m-d', strtotime($row['motivation_end']));

			// заполняем данные о ставках окладов и премий
			if ((int)$row['motivation_type'] === 1) {
				$this->motivations[(int)$row["manager_id"]]["motivations"]['base'][$row['motivation_base']]
				['type'][$row['motivation_type']][$row['motivation_id']]['val'] =
					(float)$row['motivation_base_salary'] / $this->total_work_days;

			} else if ((int)$row['motivation_type'] === 2) {
				$this->motivations[(int)$row["manager_id"]]["motivations"]['base'][$row['motivation_base']]
				['type'][$row['motivation_type']][$row['motivation_id']]['val'] =
					['border' => (float)$row['motivation_border'],
						'rate' => (float)$row['motivation_base_salary'] / $this->total_work_days];

			} // заполняем данные о коэффициентах
			else if (($row['motivation_type'] >= 5 && $row['motivation_type'] <= 12) || (int)$row['motivation_type'] === 14) {
				$this->motivations[(int)$row["manager_id"]]["motivations"]['base'][$row['motivation_base']]
				['type'][$row['motivation_type']][$row['motivation_id']]['val'] =
					[
						'border' => (float)$row['motivation_border'],
						'rate' => (float)$row['motivation_base_salary']
					];
			}
		}
	}

	public function updateCalculations($data, $group_manager = '')
	{
		// разбираем от какого менеджера сабмит, чтобы не обновлять всех
		$groups_manager = explode(',', $group_manager);
		if (count($groups_manager) > 1) {
			foreach ($groups_manager as $manager_group) {
				$this->updateCalculations($data, $manager_group);
			}
			return;
		}

		$group = '';
		$submitted_manager_id = 0;
		if ($group_manager !== '' && count($groups_manager)) {
			$params = explode('-', $group_manager);
			$group = $params[0];
			$submitted_manager_id = (int)$params[1];
		}

		// распарсиваем POST данные
		$parsed_data = [];
		foreach ($data as $key => $value) {
			$params = explode('-', $key);

			$manager_id = 0;
			$contract_id = 0;
			$calculation_id = 0;
			$income_id = 0;

			if (count($params) === 3) {
				list($benefit_group, $benefit_var,) = $params;

				$ids = explode('_', $params[2]);

				if (count($ids) >= 1) {
					list($manager_id, $contract_id, $income_id, $calculation_id) = $ids;
					$manager_id = (int)$manager_id;
                    //==============================================================================================================================
					if ($contract_id !== 'all') {
						$contract_id = (int)$contract_id;
					}//Эти 2 if - небольшой костыль для некоторых коэфов, которые нужно применить для всех заказов и поступлений (Например, для коэфа за выполнение плана)
					if ($income_id !== 'all') {
						$income_id = (int)$income_id;
					}
                    //==============================================================================================================================
					$calculation_id = (int)$calculation_id;
				}

				if ($manager_id > 0) {
					if ($benefit_var === 'calculation' && $calculation_id > 0) {
						$parsed_data[$manager_id][$benefit_group][$contract_id][$income_id][$benefit_var] = $calculation_id;
					} elseif ($benefit_var !== 'calculation') {
						$parsed_data[$manager_id][$benefit_group][$contract_id][$income_id][$benefit_var] = $value;
					}
				}
			}
		}

		// формируем апдейты и инсерты начислений
		$calculations = new Table('Начисления зарплаты');

		$simple_groups = ['other', 'holiday', 'firstly_typical', 'secondary_typical', 'firstly_complex', 'secondary_complex'];
		$department_groups = ['win_department', 'done_department'];

		foreach ($parsed_data as $manager_id => $calculation_data) {

			// пропускаем обработку данных по менеджерам не связанным с кнопкой сабмита
			$manager_id = (int)$manager_id;
			if ($submitted_manager_id > 0 && $submitted_manager_id !== $manager_id) {
				continue;
			}

			foreach ($calculation_data as $benefit_group => $benefit_data) {
				// пропускаем обработку разделов не связанных с кнопкой сабмита
				if ($group !== '' && $group !== 'all' && trim($group) !== trim($benefit_group)) {
					continue;
				}
				if ($benefit_group === 'firstly_typical' || $benefit_group === 'secondary_typical' ||
					$benefit_group === 'firstly_complex' || $benefit_group === 'secondary_complex') {
					$coef_plan = $benefit_data['all']['all']['plan_coef'];
				}
				foreach ($benefit_data as $order_id => $contract_data) {
					if ($order_id === 'all') {
						continue;
					}
					foreach ($contract_data as $income_id => $income_data) {
						if (isset($coef_plan)) {
							$income_data['coef_plan'] = $coef_plan;
						}
						$calculations->data = [];
						$calculation_id = (int)$income_data['calculation'];
						if ($calculation_id > 0) {
							$calculations->data[0]['ID'] = $calculation_id;
							if ($benefit_group === 'base' || in_array($benefit_group, $simple_groups, true)) {
								if ($benefit_group === "other" && $income_data['check'] == '' && $calculation_id > 0) {
									// delete record
									$calculations->deleteSoft();
									continue;
								}
								// update record
								$calculations->data[0]['Дата начисления'] = date('Y-m-d H:i:s');
								$calculations->data[0]['Основание'] = $this->prepareNumber($income_data['order_profit']);
								$calculations->data[0]['Ставка'] = $this->prepareNumber($income_data['rate']);

								$this->prepareCalculations($calculations, $income_data, $benefit_group);

								$calculations->save();
							} else {
								$calculations->deleteSoft();
							}
						} else {
							// insert calculation record
							if ($benefit_group === 'other' && (float)$income_data['sum'] === 0.0) {
								continue;
							}

							$calculations->data[0]['Месяц'] = date("Y-m-01 00:00:00", strtotime($this->month . "-01"));
							if ($income_data['additional_manager_check'] === 'on' && (int)$income_data['additional_manager'] > 0) {
								$calculations->data[0]['Сотрудник'] = (int)$income_data['additional_manager'];
							} else {
								$calculations->data[0]['Сотрудник'] = $manager_id;
							}
							$calculations->data[0]['Тип'] = $this->getBenefitTypeId($benefit_group);
							$calculations->data[0]['Дата начисления'] = date('Y-m-d H:i:s');
							$calculations->data[0]['Заказ'] = $order_id;
							$calculations->data[0]['Поступление'] = $income_id;

							if (in_array($benefit_group, $department_groups, true)) {
								$calculations->data[0]['База'] = 'Отдел';
							} else {
								$calculations->data[0]['База'] = 'Личное';
							}

							$calculations->data[0]['Основание'] = $this->prepareNumber($income_data['order_profit']);
							$calculations->data[0]['Ставка'] = $this->prepareNumber($income_data['rate']);

							$this->prepareCalculations($calculations, $income_data, $benefit_group);

							if ((string)$this->month !== "") {
								$calculations->save();
							}
						}

//						if ($calculation_id > 0 &&
//							($benefit_group === 'base' || in_array($benefit_group, $simple_groups, true))) {
//
//							$calculations->data[0]['ID'] = $calculation_id;
//
//							if ($benefit_group === "other") {
//								// delete record
//								$calculations->deleteSoft();
//								continue;
//							}
//							// update record
//							$calculations->data[0]['Дата начисления'] = date('Y-m-d H:i:s');
//							$calculations->data[0]['Основание'] = $this->prepareNumber($income_data['contract_profit']);
//							$calculations->data[0]['Ставка'] = $this->prepareNumber($income_data['rate']);
//
//							$this->prepareCalculations($calculations, $income_data, $benefit_group);
//
//							$calculations->save();
//						} else {
//							if ($calculation_id > 0) {
//								// delete record
//								$calculations->data[0]['ID'] = $calculation_id;
//								$calculations->deleteSoft();
//							} else {
//								// insert calculation record
//								if ($benefit_group === 'other' && (float)$income_data['sum'] === 0.0) {
//									continue;
//								}
//
//								$calculations->data[0]['Месяц'] = date("Y-m-01 00:00:00", strtotime($this->month . "-01"));
//								if ($income_data['additional_manager_check'] === 'on' && (int)$income_data['additional_manager'] > 0) {
//									$calculations->data[0]['Сотрудник'] = (int)$income_data['additional_manager'];
//								} else {
//									$calculations->data[0]['Сотрудник'] = $manager_id;
//								}
//								$calculations->data[0]['Тип'] = $this->getBenefitTypeId($benefit_group);
//								$calculations->data[0]['Дата начисления'] = date('Y-m-d H:i:s');
//								$calculations->data[0]['Заказ'] = $order_id;
//								$calculations->data[0]['Поступление'] = $income_id;
//
//								if (in_array($benefit_group, $department_groups, true)) {
//									$calculations->data[0]['База'] = 'Отдел';
//								} else {
//									$calculations->data[0]['База'] = 'Личное';
//								}
//
//								$calculations->data[0]['Основание'] = $this->prepareNumber($income_data['order_profit']);
//								$calculations->data[0]['Ставка'] = $this->prepareNumber($income_data['rate']);
//
//								$this->prepareCalculations($calculations, $income_data, $benefit_group);
//
//								if ((string)$this->month !== "") {
//									$calculations->save();
//								}
//							}
//						}
					}
				}
			}
		}
	}

	private function getBenefitTypeId($type): int
	{
		switch ($type) {
			case 'base': // оклад
				return MotivationTypes::BASE_SALARY;
			case 'holiday':
				return MotivationTypes::HOLIDAY_SALARY; // отпускные
			case 'firstly_typical':
				return MotivationTypes::FIRSTLY_TYPICAL_SALES_BONUS;
			case 'secondary_typical':
				return MotivationTypes::SECONDARY_TYPICAL_SALES_BONUS;
			case 'firstly_complex':
				return MotivationTypes::FIRSTLY_COMPLEX_SALES_BONUS;
			case 'secondary_complex':
				return MotivationTypes::SECONDARY_COMPLEX_SALES_BONUS;
			default:
				return MotivationTypes::ANOTHER_BONUS; // премия прочая
		}
	}

	private function getSalaryHeadCounter($manager, int $position_id): SalaryHeadManager
	{
        $params = [
            'manager' => $manager,
            'month' => $this->month,
            'total_work_days' => $this->total_work_days,
            'override_manual_with_auto_common' => $this->override_manual_with_auto_common,
            'use_manual_holidays' => $this->use_manual_holidays,
            'override_manual_with_auto' => $this->override_manual_with_auto,
            'motivations' => $this->motivations[(int)$manager['ID']]
        ];

		if ($position_id === Positions::HEAD_CLIENT_MANAGER) {
			$salary = new SalaryHeadClientsManager($params);
		} else {
			$salary = new SalaryHeadSalesManager($params);
		}

		return $salary;
	}

	private function getSalaryCounter($manager, int $position_id): Salary
	{
	    $params = [
            'manager' => $manager,
            'month' => $this->month,
            'total_work_days' => $this->total_work_days,
            'override_manual_with_auto_common' => $this->override_manual_with_auto_common,
            'use_manual_holidays' => $this->use_manual_holidays,
            'override_manual_with_auto' => $this->override_manual_with_auto,
            'motivations' => $this->motivations[(int)$manager['ID']]
        ];

		switch ($position_id) {
			case Positions::SALES_MANAGER:
				$salary = new SalarySalesManager($params);
				break;
			case Positions::CLIENT_MANAGER:
				$salary = new SalaryClientsManager($params);
				break;
			case Positions::HEAD_CLIENT_MANAGER:
				$salary = new SalaryHeadClientsManager($params);
				break;
			case Positions::HEAD_SALES_MANAGER:
				$salary = new SalaryHeadSalesManager($params);
				break;
			default:
				$salary = new Salary($params);
				break;
		}
		return $salary;
	}

	private function getManagersByDepartments($manager_id = 0, $department_id = 0): array
	{
		$managers = [];
		$personal = new Personal();
		if (empty($manager_id)) {
			$managersByMonth = $personal->getAllManagersByMonth($this->month, $department_id, true);
		} else {
			$personal->getDataById($manager_id);
			$managersByMonth = $personal->data;
		}
		foreach ($managersByMonth as $manager) {
			$department_index = array_search($manager['Отдел'], array_column($this->departments, 'ID'), true);
			if ($department_index === false) {
				continue;
			}

			$position_index = array_search($manager['Должность'], array_column($this->positions, 'ID'), true);
			if ($position_index === false) {
				continue;
			}

			$department = $this->departments[$department_index]['Название'];
			$position = $this->positions[$position_index]['Название'];
			$managers[$department][$position][$manager['Фамилия Инициалы']] = $manager;
		}
		return $managers;
	}

	private function prepareNumber($num): float
	{
		$num = str_replace(array(' ', ','), array('', '.'), $num);
		return (float)$num;
	}

	private function prepareCalculations(Table $calculations, $income_data, $benefit_group)
	{
		$calculations->data[0]['Примечание'] = $income_data['comment'];

		if ($benefit_group === 'other' || $benefit_group === 'holiday' || $benefit_group === 'base') {
			$calculations->data[0]['Основание'] = $this->prepareNumber($income_data['sum']);
			$calculations->data[0]['Ставка'] = 1.0;
			$calculations->data[0]['Коэффициент 1'] = 1.0;
			$calculations->data[0]['Коэффициент 2'] = 1.0;
			$calculations->data[0]['Сумма начисления'] = $this->prepareNumber($income_data['sum']);
		}
		if ($benefit_group === 'firstly_typical' || $benefit_group === 'firstly_complex' ||
			$benefit_group === 'secondary_typical' || $benefit_group === 'secondary_complex') {

			$calculations->data[0]['Коэффициент 1'] = $this->prepareNumber($income_data['coef_plan']);
			if (isset($income_data['rate_date'])) {
				$calculations->data[0]['Коэффициент 2'] = $this->prepareNumber($income_data['rate_date']);
			} else {
				$calculations->data[0]['Коэффициент 2'] = 1.0;
			}
			$calculations->data[0]['Сумма начисления'] =
				$calculations->data[0]['Основание'] *
				$calculations->data[0]['Ставка'] *
				$calculations->data[0]['Коэффициент 1'] *
				$calculations->data[0]['Коэффициент 2'];
		}

		$calculations->data[0]['Коэффициент 3'] = 1.0;

		if ($benefit_group === 'win_correction' || $benefit_group === 'done_correction') {
			$calculations->data[0]['Корректировка'] = 'Да';
		}
	}

	public function insertPaids(array $data)
	{
		global $smarty;
		$allAdded = '';
		$managers = $data['paid-manager'];
		$dates = $data['paid-date'];
		$types = $data['paid-type'];
		$sums = $data['paid-sum'];
		$comments = $data['paid-com'];
		$managerRow = new Personal();
		for ($i = 0, $iMax = count($managers); $i < $iMax; $i++) {
			$manager = $managers[$i];
			$date = $dates[$i];
			$type = $types[$i];
			$sum = $sums[$i];
			$sum = str_replace(' ', '', $sum);
			$comment = $comments[$i];
			$managerRow->getData(['ФИО'], ['ID' => $manager]);
			if (!empty($manager) && !empty($date) && !empty($type) && !empty($sum)) {
				data_insert(1241, EVENTS_ENABLE, array(
					'f20601' => $manager,
					'f20621' => $type,
					'f20641' => date('Y-m-d', strtotime($date)),
					'f20631' => date('Y-m-d', strtotime($this->month . '-01')),
					'f20651' => $sum,
					'f20661' => $comment
				));
				$allAdded .= $managerRow->data[0]['ФИО'] . ' Дата:' . $date . ' Тип:' . $type . ' Сумма:' . $sum . ($comment != '' ? ' Примечание:' . $comment : '') . '<br>';
			}
		}

		// TODO разобраться зачем нужен редирект, можно ли без него
		header("Location:report.php?id=351");
		$smarty->assign('dt_period', date($this->month . '-01'));
		if ($allAdded != '') {
			$success = 'Добавлены данные: <br>' . $allAdded;
			$smarty->assign('success', $success);
		} else {
			$myError = 'Данные по выплатам не внесены';
			$smarty->assign('myError', $myError);
		}
		exit;
	}
}