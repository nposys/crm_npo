<?php


namespace classes\Models\Salary;


class SalaryHeadClientsManager extends SalaryHeadManager
{
	public function calculateSalary()
	{
		parent::calculateSalary();
		$this->getBonus();
	}

	public function prepareTotals()
	{
		$this->totals["totals"]["sum"]["total"] = '0,00';
		$this->totals['typical_plan']['total'] = '0,00';
		$this->totals['complex_plan']['total'] = '0,00';
		$this->totals['typical']['total'] = '0,00';
		$this->totals['complex']['total'] = '0,00';

		$this->totals["totals"]["benefit"]["total"] = '0,00';
		$this->totals["totals"]["benefit"]["typical"] = '0,00';
		$this->totals["totals"]["benefit"]["complex"] = '0,00';
		$this->totals["totals"]["salary"]["total"] = '0,00';

		foreach ($this->department_results as $position) {
			foreach ($position as $manager => &$manager_data) {
				$this->totals["managers"][$manager]["work_now"] =
					$manager_data['manager']["Работает сейчас"];

				$this->totals["managers"][$manager]["sum"]["total"] =
					$manager_data['salary_report']["sum_result"];

				$this->totals["managers"][$manager]["sum"]["typical_plan"] =
					$manager_data['salary_report']['service_groups']["Обычные"]['total']['plan_sum'];
				$this->totals["managers"][$manager]["sum"]["complex_plan"] =
					'0,00';

				if ($this->prepareNumber($manager_data['salary_report']['service_groups']["Обычные"]['total']['plan_sum']) === 0.0) {
					$manager_data['salary_report']["plan_percent"] = '0,00';
				} else {
					$this->totals["managers"][$manager]['sum']["typical"]["plan_percent"] =
						round($this->prepareNumber($manager_data['salary_report']['service_groups']["Обычные"]['total']['sum']) /
							$this->prepareNumber($manager_data['salary_report']['service_groups']["Обычные"]['total']['plan_sum'] * 100.00));
				}


				$this->totals["managers"][$manager]["sum"]["typical"]['old'] =
					$manager_data['salary_report']['service_groups']["Обычные"]['total']['sum'];
				$this->totals["managers"][$manager]["sum"]["complex"]['old'] =
					$manager_data['salary_report']['service_groups']["Комплексные"]['total']['sum'];

				$this->totals["totals"]["sum"]["total"] += $this->prepareNumber($manager_data['salary_report']["sum_result"]);
				$this->totals['typical_plan']['total'] += $this->prepareNumber($this->totals["managers"][$manager]["sum"]["typical_plan"]);
				$this->totals['complex_plan']['total'] += $this->prepareNumber($this->totals["managers"][$manager]["sum"]["complex_plan"]);

				$this->totals['typical']['total'] += $this->prepareNumber($manager_data['salary_report']['service_groups']["Обычные"]['total']['sum']);
				$this->totals['complex']['total'] += $this->prepareNumber($manager_data['salary_report']['service_groups']["Комплексные"]['total']['sum']);

				if ($manager_data['manager']["Работает сейчас"] === "Да") {
					$this->totals["managers"][$manager]["benefit"]["total"] =
						$manager_data['salary_report']["benefit_result"];
					$this->totals["managers"][$manager]["benefit"]["typical"]['old'] =
						$manager_data['salary_report']['service_groups']["Обычные"]['total']['benefit_final'];
					$this->totals["managers"][$manager]["benefit"]["complex"]['old'] =
						$manager_data['salary_report']['service_groups']["Комплексные"]['total']['benefit_final'];

					if (!$manager_data['salary_report']['hide_totals']) {
						$this->totals["managers"][$manager]["base_salary"] =
							$manager_data['salary']['pivot']['auto']['base_salary_total'];
						$this->totals["managers"][$manager]["result_salary"] =
							$manager_data['salary']['pivot']['auto']['base_salary_total'] + $manager_data["salary_report"]["benefit_result"];
					}

					// не суммируем из раздела
					$this->totals["totals"]["benefit"]["total"] =
						$this->prepareNumber($this->totals["totals"]["benefit"]["total"]) +
						$this->prepareNumber($manager_data['salary_report']["benefit_result"]);
					$this->totals["totals"]["benefit"]["typical"] =
						$this->prepareNumber($this->totals["totals"]["benefit"]["typical"]) +
						$this->prepareNumber($manager_data['salary_report']['service_groups']["Обычные"]['total']['benefit_final']);
					$this->totals["totals"]["benefit"]["complex"] =
						$this->prepareNumber($this->totals["totals"]["benefit"]["complex"]) +
						$this->prepareNumber($manager_data['salary_report']['service_groups']["Комплексные"]['total']['benefit_final']);
					$this->totals["totals"]["salary"]["total"] =
						$this->prepareNumber($this->totals["totals"]["salary"]["total"]) +
						$this->prepareNumber($this->totals['groups']["manager_clients"]["managers"][$manager]["result_salary"]);
				}
			}
		}
		unset($manager_data);

		if ($this->prepareNumber($this->totals['typical_plan']['total']) === 0.0) {
			$this->totals['typical']['plan_percent'] = 0;
		} else {
			$this->totals['typical']['plan_percent'] =
				round($this->prepareNumber($this->totals['typical']['total']) /
					$this->prepareNumber($this->totals['typical_plan']['total']) * 100.00);
		}
		$this->formatNumericData($this->totals);
	}

	public function getBonus()
	{
		$manager = $this->manager['Фамилия Инициалы'];

		$this->salary_report["service_groups"]["Обычные"]["total"]["plan_sum"] =
			$this->totals['typical_plan']['total'];

		$this->salary_report["service_groups"]["Комплексные"]["total"]["plan_sum"] = '0,00';

		$sum_typical_head = $this->prepareNumber($this->totals['typical']['total']);

		$sum_complex_head = $this->prepareNumber($this->totals['complex']['total']);

		$this->salary_report["service_groups"]["Обычные"]["total"]['sum'] =
			$this->prepareNumericData($sum_typical_head);
		$this->salary_report["service_groups"]["Комплексные"]["total"]['sum'] =
			$this->prepareNumericData($sum_complex_head);

		if ($this->totals['typical_plan']['total'] > 0.0) {
			$this->salary_report['service_groups']["Обычные"]["total"]["plan_percent"] =
				round($sum_typical_head /
					$this->prepareNumber($this->totals['typical_plan']['total']) *
					100.00
				);
		}

		if ($this->manager["Работает сейчас"] === "Да") {

			$this->salary_report["sum_result"] =
				$this->prepareNumericData($sum_typical_head + $sum_complex_head);

			// ищем правильную ставку
			$plan_percent = $sum_typical_head / $this->totals['typical_plan']['total'] * 100;
			$typical_plan_rate = $this->getPlanRealizationCoef($plan_percent);
			$benefit_typical_head = $this->getBaseBonus('Обычные', $sum_typical_head, $typical_plan_rate);
			$benefit_complex_head = $this->getBaseBonus('Комплексные', $sum_complex_head, 1.0);

			$this->salary_report["service_groups"]["Обычные"]["total"]["benefit_final"] =
				$this->prepareNumericData($benefit_typical_head);
			$this->salary_report["service_groups"]["Комплексные"]["total"]["benefit_final"] =
				$this->prepareNumericData($benefit_complex_head);

			$this->salary_report["benefit_result"] =
				$this->prepareNumericData($benefit_typical_head + $benefit_complex_head);

			$this->salary_report["result_salary"] = $this->prepareNumericData(
				$this->prepareNumber($this->salary_report["result_salary"]) + $benefit_typical_head + $benefit_complex_head
			);

			$this->totals["totals"]["benefit"]["total"] = $this->prepareNumericData(
				$this->prepareNumber($this->totals["totals"]["benefit"]["total"]) + $benefit_typical_head + $benefit_complex_head
			);

			$this->totals["totals"]["benefit"]["typical"] = $this->prepareNumericData(
				$this->prepareNumber($this->totals["totals"]["benefit"]["typical"]) + $benefit_typical_head
			);
			$this->totals["totals"]["benefit"]["complex"] = $this->prepareNumericData(
				$this->prepareNumber($this->totals["totals"]["benefit"]["complex"]) + $benefit_complex_head
			);
			$this->totals["totals"]["salary"]["total"] = $this->prepareNumericData(
				$this->prepareNumber($this->totals["totals"]["salary"]["total"]) + $this->totals["managers"][$manager]["result_salary"]
			);

			$this->salary['salary']['pivot']['auto']['benefit_department_wins'] = $this->salary_report["benefit_result"];
			$this->salary['salary_report'] = $this->salary_report;
		}
	}

	private function getBaseBonus($type, $sum, $plan_rate): float
	{
		if ($type === 'Обычные') {
			$motivation = $this->orderBorderAsc($this->getCoef(date('Y-d-t', strtotime($this->month)), 'Отдел', 'secondary_typical_sales_bonus'));
		} else {
			$motivation = $this->orderBorderAsc($this->getCoef(date('Y-d-t', strtotime($this->month)), 'Отдел', 'secondary_complex_sales_bonus'));
		}
		$benefit_sum = 0;
		$previous_border = 0;
		foreach ($motivation as $motivation_value) {
			if ($sum > $motivation_value['border']) {
				$benefit_sum += ($motivation_value['border'] - $previous_border) * $motivation_value['rate'];
				$previous_border = $motivation_value['border'];
			} else {
				$benefit_sum += ($sum - $previous_border) * $motivation_value['rate'];
				break;
			}
		}
		return $benefit_sum * $plan_rate;
	}
}