<?php


namespace classes\Models;


use classes\Enums\DatabasesEnums\PostgresDatabaseEnums\OrgContactPersonTableEnums\OrgContactPersonFields;

class TDBContactPerson
{
    private $FullName;
    private $Position;
    private $Email;
    private $Phone;
    private $Inn;

    public function __construct(array $personOptions)
    {
        foreach ($personOptions as $key => $value) {
            switch ($key) {
                case OrgContactPersonFields::FULL_NAME:
                    $this->FullName = $value;
                    break;
                case OrgContactPersonFields::POSITION:
                    $this->Position = $value;
                    break;
                case OrgContactPersonFields::EMAIL:
                    $this->Email = $value;
                    break;
                case OrgContactPersonFields::PHONE:
                    $this->Phone = $value;
                    break;
                case OrgContactPersonFields::INN:
                    $this->Inn = $value;
                    break;
            }
        }
    }

    /**
     * @return mixed
     */
    public function getFullName()
    {
        return $this->FullName;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->Position;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->Email;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->Phone;
    }

    /**
     * @return mixed
     */
    public function getInn()
    {
        return $this->Inn;
    }
}