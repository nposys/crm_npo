<?php


namespace classes\Models;


use classes\Base\PGDB;
use PDO;

class PostgresHelper
{
	private $pgdb;

	public function __construct()
	{
		$this->pgdb = new PGDB([
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
			PDO::ATTR_EMULATE_PREPARES => false,
		]);
	}

	public function getLotDataByGovRuIdAndLotNumber($gov_ru_id, $lot_number): array
	{
		$query = $this->getQueryLotDataByGovRuIdAndLotNumber($gov_ru_id, $lot_number);
		$stmt = $this->pgdb->query($query);
		return $stmt->fetchAll();
	}

	private function getQueryLotDataByGovRuIdAndLotNumber($gov_ru_id, $lot_number): string
	{
		if ($lot_number !== "") {
			$lot_number = (int)$lot_number;
		} else {
			$lot_number = 1;
		}

        $gov_ru_id = trim($gov_ru_id);
        return 'SELECT sTL."Title"                                     		as "tenderLaw",
				       tL."DatePublic"                                 		as "datePublic",
				       tL."DateOpen"                                   		as "dateOpen",
				       tL."DateAuction"                                		as "dateAuction",
				       tL."DateExam"                                   		as "dateExam",
				       tL."Link"                                       		as "link",
				       tL."Title"                                      		as "lotTitle",
				       sS."ShortTitle"                                 		as "tenderSite",
				       tL."sTenderStatusId"                                 as "tenderStatus",
				       oC."ShortTitle"                                 		as "companyShortTitle",
				       oC."LegalAddress"                               		as "companyLegalAddress",
				       oC."Phone"                                      		as "companyPhone",
				       oC."Email"                                      		as "companyEmail",
				       (SELECT "FullName"
				        FROM "orgContactPerson" as oCP
				        WHERE oCP."orgCompanyId" = oC."orgCompanyId"
				        LIMIT 1)                                       		as "contactPersonFullName",
				       (SELECT tT."CountLots"
				        FROM "tTender" as tT
				        WHERE tT."GovRuId" = \'' . $gov_ru_id . '\')     as "lotCount",
				       sTTS."Title"                                         as "tenderType",
				       sTTS."sTenderTypeShortId"                            as "tenderTypeId",
				       tL."sRegionId"                                       as "regionId",
				       tL."LotNumber"                                       as "lotNumber",
				       tL."Cost"                                            as "lotCost",
				       tL."CostAppGuarantee"                                as "requestCost",
				       tL."CostContractGuarantee"                           as "contractCost",
				       tL."TimeZoneUtc"                                     as "timeZone"
				FROM "tLot" as tL
				         LEFT JOIN "orgCompany" as oC ON tl."orgOrganizationResponsibleId" = oC."orgCompanyId"
				         LEFT JOIN "sTenderLaw" as sTL ON tL."sTenderLawId" = sTL."sTenderLawId"
				         LEFT JOIN "sSite" as sS ON "sSiteId" = tL."sAuctionSiteId"
				         LEFT JOIN "sTenderTypeShort" as sTTS ON tL."sTenderTypeShortId" = sTTS."sTenderTypeShortId"
				WHERE tL."GovRuId" = \'' . $gov_ru_id . '\'
				  AND tL."LotNumber" = ' . $lot_number;
	}
}