<?php


namespace classes\Models;


use classes\Base\PGDB;
use classes\Base\Table;
use classes\Enums\CrmEnums\ActivityCategory;
use classes\Enums\CrmEnums\CheckSizeCategory;
use classes\Enums\CrmEnums\ClientCategories;
use classes\Enums\CrmEnums\ClientEntities;
use classes\Enums\CrmEnums\ClientTypes;
use classes\Enums\CrmEnums\LeadStatuses;
use classes\Enums\CrmEnums\UserGroups;
use classes\Tables\Act;
use classes\Tables\ClientData;
use classes\Tables\Company;
use classes\Tables\Income;
use classes\Tables\IncomeInvoiceOrders;
use classes\Tables\Order;
use classes\Tables\OrdersSearch;
use PDO;


class ClientProcessor
{
    private function updateCompany(TDBCompany $company, Table $companiesTable)
    {
        $clientId = (int)$companiesTable->data[0]['ID'];
        if (trim($companiesTable->data[0]['КПП']) === "") {
            $companiesTable->data[0]['КПП'] = $company->getKpp();
        }
        if (trim($companiesTable->data[0]['Юридическое название']) === "") {
            $companiesTable->data[0]['Юридическое название'] = $company->getFullTitle();
        }
        if (trim($companiesTable->data[0]['Фактический адрес']) === "") {
            $companiesTable->data[0]['Фактический адрес'] = $company->getRealAddress();
        }
        if (trim($companiesTable->data[0]['Краткое юридическое название']) === "") {
            $companiesTable->data[0]['Краткое юридическое название'] = $company->getShortTitle();
        }
        if (trim($companiesTable->data[0]['Почтовый адрес']) === "") {
            $companiesTable->data[0]['Почтовый адрес'] = $company->getPostalAddress();
        }
        if (trim($companiesTable->data[0]['E-mail']) === "") {
            $companiesTable->data[0]['E-mail'] = $company->getEmail();
        }
        if (trim($companiesTable->data[0]['Телефон']) === "") {
            $companiesTable->data[0]['Телефон'] = $company->getPhone();
        }
        if (trim($companiesTable->data[0]['Дополнительные контакты']) === "") {
            $companiesTable->data[0]['Дополнительные контакты'] = $this->createAdditionalContacts($company);
        }
        if (!$companiesTable->data[0]['Регион']) {
            $companiesTable->data[0]['Регион'] = $company->getRegion();
        }
        $companiesTable->save((int)$companiesTable->data[0]['ID'], false);

        $clientDataTable = new ClientData();
        // проверяем наличие записи в подтаблице с доп.данными
        $clientDataTable->getData([],
            [
                'Компания' => $clientId,
                'status' => 0
            ]);

        if (count($clientDataTable->data) === 0) {
            // добавляем строчку в таблицу статистики по клиенту
            $this->createClientStatistics($clientDataTable, $clientId);
        }

        // обновляем данные об участиях в тендерах
        $bAlwaysUpdate = true;
        $dt_not_update = date('Y-m-d', strtotime(date('Y-m-d H:i:s') . " -1 hours"));
        if ($bAlwaysUpdate || $clientDataTable->data[0]['Дата обновления данных'] <= $dt_not_update) {
            $this->updateParticipationInfo($clientId, $company->getOrgCompanyId());
        }
    }

    private function updateParticipationInfo($clientId, $pgCompanyId)
    {
        $dtNow = date('Y-m-d H:i:s');
        $dtStart = date('Y-01-01 H:i:s', strtotime($dtNow . " - 1 year"));

        $secondsInMonth = 60 * 60 * 24 * 30; //60sec * 60min * 24h * 30d

        $pgdb = new PGDB([PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false]);
        $stmt = $pgdb->query($this->getParticipationInfo($pgCompanyId, $dtStart, $dtNow));
        $rows = $stmt->fetchAll();

        $participationCount = $rows[0]['participation_count'];
        $participationSum = $rows[0]['participation_sum'];

        $winCount = $rows[0]['win_count'];
        $winSum = $rows[0]['win_sum'];

        $startWinSum = $rows[0]['win_start_sum'];
        $startParticipationSum = $rows[0]['participation_start_sum'];

        $maxDate = $rows[0]['last_participation_date'];
        $minDate = $rows[0]['first_participation_date'];

        $secondsDiff = strtotime($dtNow) - strtotime($minDate);
        $monthsDiff = $secondsDiff / $secondsInMonth;
        if ($monthsDiff > 0 && $monthsDiff < 1) {
            $monthsDiff = 1;
        }

        $avgParticipationSum = round($participationSum / $participationCount, 2);

        $avgParticipationPerMonth = '';
        $avgWinPerMonth = '';
        if ($monthsDiff > 0) {
            $avgParticipationPerMonth = round($participationCount / $monthsDiff, 2);
            $avgWinPerMonth = round($winCount / $monthsDiff, 2);
        }

        $clientDataTable = new ClientData();
        $clientDataTable->getData(
            [],
            [
                'status' => 0,
                'Компания' => $clientId
            ]);
        $needToCreateClientData = ($clientDataTable->num_rows === 0);
        $dateUpdate = date('Y-m-d H:i:s');
        if($needToCreateClientData){
            $clientDataTable->data[0]['Компания'] = $clientId;
            $clientDataTable->data[0]['status'] = 0;
        }
        $clientDataTable->data[0]['Дата начала статистики'] = $dtStart;
        $clientDataTable->data[0]['Дата обновления данных'] = $dateUpdate;

        $clientDataTable->data[0]['Начальная сумма участий'] = $startParticipationSum;
        $clientDataTable->data[0]['Сумма участий'] = $participationSum;
        $clientDataTable->data[0]['Кол-во участий'] = $participationCount;

        $clientDataTable->data[0]['Начальная сумма побед'] = $startWinSum;
        $clientDataTable->data[0]['Сумма побед'] = $winSum;
        $clientDataTable->data[0]['Кол-во побед'] = $winCount;

        $clientDataTable->data[0]['Процент побед'] = round($winCount / $participationCount * 100, 2);

        if ($winCount > 0) {
            $clientDataTable->data[0]['Среднее снижение'] = round(($startWinSum - $winSum) / $startWinSum * 100, 2);
            $clientDataTable->data[0]['Средняя сумма побед'] = round($winSum / $winCount, 2);
        } else {
            $clientDataTable->data[0]['Среднее снижение'] = 0;
            $clientDataTable->data[0]['Средняя сумма побед'] = 0;
        }

        $clientDataTable->data[0]['Средняя сумма участий'] = $avgParticipationSum;

        $clientDataTable->data[0]['Дата первого участия'] = $minDate;
        $clientDataTable->data[0]['Дата последнего участия'] = $maxDate;
        $clientDataTable->data[0]['Кол-во участий в месяц'] = $avgParticipationPerMonth;
        $clientDataTable->data[0]['Кол-во побед в месяц'] = $avgWinPerMonth;

        if ($rows[0]['count_44fz'] > 0) {
            $clientDataTable->data[0]['Участник44'] = 'Да';
        } else {
            $clientDataTable->data[0]['Участник44'] = 'Нет';
        }
        if ($rows[0]['count_223fz'] > 0) {
            $clientDataTable->data[0]['Участник223'] = 'Да';
        } else {
            $clientDataTable->data[0]['Участник223'] = 'Нет';
        }

        $clientDataTable->data[0]['Категория по активности'] = ActivityCategory::getActivityCategory($avgWinPerMonth);
        $clientDataTable->data[0]['Категория по размеру'] = CheckSizeCategory::getCheckSizeCategory($clientDataTable->data[0]['Средняя сумма побед']);

        $this->updateWorkWithNPO($clientId, $clientDataTable);

        $companyTable = new Company();
        $companyTable->getData([],
            [
                "ID" => $clientId,
                "status" => 0
            ]);
        $companyTable->data[0]['Категория по активности'] = $clientDataTable->data[0]['Категория по активности'];
        $companyTable->data[0]['Категория по размеру'] = $clientDataTable->data[0]['Категория по размеру'];
        $companyTable->data[0]['Дата обновления статистики'] = $dateUpdate;

        if (!$needToCreateClientData) {
            $clientDataTable->save((int)$clientDataTable->data[0]['ID']);
        } else {
            $clientDataTable->save(0);
        }
        $companyTable->save((int)$companyTable->data[0]['ID']);
    }

    private function updateWorkWithNPO($clientId, $clientDataTable, $dt_start = "2015-04-01 00:00:00")
    {
        if ($clientId === "") {
            return;
        } // если id клиента не передан, то выходим

        if ($clientId > 0) {
            $orderTable = new Order();
            $orderTable->getData(
                [
                    "sum" => ["field" => "Клиент", "function" => "SUM", "alias" => "sum"],
                    "first_order_date" => ["field" => "Дата", "function" => "MIN", "alias" => "first_order_date"],
                    "last_order_date" => ["field" => "Дата", "function" => "MAX", "alias" => "last_order_date"]
                ],
                [
                    "Клиент" => $clientId,
                    "status" => 0
                ]);

            if ($orderTable->data[0]['sum'] > 0) // Выполнено заказов
            {
                // считаем сумму поступлений в привязке к заказам
                $incomeInvoiceOrdersTable = new IncomeInvoiceOrders();
                $incomeInvoiceOrdersTable->getData(
                    [
                        "sum" => ["field" => "Оплачено сумма заказ", "function" => "SUM", "alias" => "sum"],
                        "min" => ["field" => "Дата поступления", "function" => "MIN", "alias" => "min"]
                    ],
                    [
                        "status" => 0,
                        "Клиент" => $clientId
                    ]);

                $actTable = new Act();
                $actTable->getData(
                    [
                        "sum" => ["field" => "Сумма", "function" => "SUM", "alias" => "sum"]
                    ],
                    [
                        "Клиент" => $clientId,
                        "status" => 0
                    ]);

                $incomeTable = new Income();
                $incomeTable->getData(
                    [
                        "sum" => ["field" => "Сумма", "function" => "SUM", "alias" => "sum"]
                    ],
                    [
                        "Клиент" => $clientId,
                        "Вид" => "'Безналичные'",
                        "status" => 0
                    ]);

                $ordersSearchTable = new OrdersSearch();
                $ordersSearchTable->getData(
                    [
                        "max" => ["field" => "Дата окончания поиска", "function" => "MAX", "alias" => "max"]
                    ],
                    [
                        "Клиент" => $clientId,
                        "status" => 0
                    ]);
                $clientDataTable->data[0]["Выполнено заказов"] = $orderTable->data[0]['sum'];
                $clientDataTable->data[0]["Дата первого заказа"] = $orderTable->data[0]['first_order_date'];
                $clientDataTable->data[0]["Дата последнего заказа"] = $orderTable->data[0]['last_order_date'];

                $clientDataTable->data[0]['Привязанно поступлений'] = $incomeInvoiceOrdersTable->data[0]['sum'];
                $clientDataTable->data[0]['Дата первого платежа'] = $incomeInvoiceOrdersTable->data[0]['min'];

                $clientDataTable->data[0]['Сальдо (Заказы/Деньги)'] = $clientDataTable->data[0]['Выполнено заказов'] - $clientDataTable->data[0]['Привязанно поступлений'];

                $clientDataTable->data[0]['Закрыто актов'] = $actTable->data[0]['sum'];

                $clientDataTable->data[0]['Получено безнал'] = $incomeTable->data[0]['sum'];

                $clientDataTable->data[0]['Сальдо (Деньги/Акты)'] = $clientDataTable->data[0]['Закрыто актов'] - $clientDataTable->data[0]['Получено безнал'];

                $clientDataTable->data[0]['Дата завершения последнего поиска'] = $ordersSearchTable->data[0]['max'];

                // вычисляем дату последего заказа заявки
                $orderTable = new Order();
                $orderTable->getData(
                    [
                        "max" => ["field" => "Дата", "function" => "MAX", "alias" => "max"]
                    ],
                    [
                        "Клиент" => $clientId,
                        "status" => 0,
                        "Услуга" => 5
                    ]);
                $clientDataTable->data[0]["Дата последнего заказа заявки"] = $orderTable->data[0]["max"];

                // вычисляем дату последего заказа ТЗ
                $orderTable = new Order();
                $orderTable->getData(
                    [
                        "max" => ["field" => "Дата", "function" => "MAX", "alias" => "max"]
                    ],
                    [
                        "Клиент" => $clientId,
                        "status" => 0,
                        "Услуга" => 7
                    ]);
                $clientDataTable->data[0]["Дата последнего заказа заявки"] = $orderTable->data[0]["max"];

                // вычисляем кол-во заказанных заявок и ТЗ (с даты сравнения статистики)
                $orderTable = new Order();
                $orderTable->getData(
                    [
                        "count" => ["field" => "id", "function" => "COUNT", "alias" => "count"]
                    ],
                    [
                        "Клиент" => $clientId,
                        "status" => 0,
                        "Услуги" =>
                            [
                                "field_name" => "Услуга",
                                "operator" => "IN",
                                "value" => "(5, 7)"
                            ],
                        "Дата" => ["field_name" => "Дата", "operator" => ">=", "value" => $dt_start],
                        "Статус заказа" => "30. Завершено"
                    ]);
                $clientDataTable->data[0]["Заказано заявок"] = $orderTable->data[0]["count"];

                // вычисляем сумму заказанных заявок (с даты сравнения статистики)
                $orderTable = new Order();
                $orderTable->getData(
                    [
                        "sum" => ["field" => "Сумма", "function" => "SUM", "alias" => "sum"]
                    ],
                    [
                        "Клиент" => $clientId,
                        "status" => 0,
                        "Услуга" => 5,
                        "Дата" => ["field_name" => "Дата", "operator" => ">=", "value" => "'$dt_start'"],
                        "Статус заказа" => "'30. Завершено'"
                    ]);
                $clientDataTable->data[0]["Заказано заявок (руб)"] = $orderTable->data[0]["sum"];

                // вычисляем сумму заказанных поисков (с даты сравнения статистики)
                $orderTable = new Order();
                $orderTable->getData(
                    [
                        "sum" => ["field" => "Сумма", "function" => "SUM", "alias" => "sum"]
                    ],
                    [
                        "Клиент" => $clientId,
                        "status" => 0,
                        "Услуга" => 4,
                        "Дата" => ["field_name" => "Дата", "operator" => ">=", "value" => "'$dt_start'"]
                    ]);
                $clientDataTable->data[0]["Заказано поиска (руб)"] = $orderTable->data[0]["sum"];

                // вычисляем сумму заказанных комплексных заказов
                $orderTable = new Order();
                $orderTable->getData(
                    [
                        "sum" => ["field" => "Сумма", "function" => "SUM", "alias" => "sum"]
                    ],
                    [
                        "Клиент" => $clientId,
                        "status" => 0,
                        "Услуга" => 7,
                        "Дата" => ["field_name" => "Дата", "operator" => ">=", "value" => "'$dt_start'"],
                        "Статус заказа" => "'30. Завершено'"
                    ]);
                $clientDataTable->data[0]["Заказано комплекс (руб)"] = $orderTable->data[0]["sum"];

                // вычисляем сумму заказанных прочих заказов
                $orderTable = new Order();
                $orderTable->getData(
                    [
                        "sum" => ["field" => "Сумма", "function" => "SUM", "alias" => "sum"]
                    ],
                    [
                        "Клиент" => $clientId,
                        "status" => 0,
                        "Услуги" =>
                            [
                                "field" => "Услуга",
                                "operator" => "NOT IN",
                                "value" => "(4, 5, 7)"
                            ],
                        "Дата" => ["field_name" => "Дата", "operator" => ">=", "value" => "'$dt_start'"],
                        "Статус заказа" => "'30. Завершено'"
                    ]);
                $clientDataTable->data[0]["Заказано прочее (руб)"] = $orderTable->data[0]["sum"];
            }
        }
        $clientDataTable->data[0]['Участвует без нас'] = $this->isClientWorkWithoutNPO($clientDataTable);
    }


    private function createCompany(TDBCompany $company, Table $companiesTable)
    {
        global $user;
        $companiesTable->data[0]['status'] = 0;
        $companiesTable->data[0]['ИНН'] = $company->getInn();
        $companiesTable->data[0]['КПП'] = $company->getKpp();
        $companiesTable->data[0]['Юридическое название'] = $company->getFullTitle();
        $companiesTable->data[0]['Юридический адрес'] = $company->getLegalAddress();
        $companiesTable->data[0]['Название'] = $company->getShortTitle();
        $companiesTable->data[0]['Краткое юридическое название'] = $company->getShortTitle(); // название
        $companiesTable->data[0]['Почтовый адрес'] = $company->getPostalAddress(); // Почтовый адрес.адрес
        $companiesTable->data[0]['E-mail'] = $company->getEmail(); // емайл
        $companiesTable->data[0]['Телефон'] = $company->getPhone(); // тел
        $companiesTable->data[0]['Дополнительные контакты'] = $this->createAdditionalContacts($company); // дополнительные контакты
        $companiesTable->data[0]['Дата обновления статистики'] = date('Y-m-d H:i:s'); // Дата обновления статистики
        $companiesTable->data[0]['Источник'] = 6; // истоичник = Загружен из протоколов

        $companiesTable->data[0]['Статус лида'] = LeadStatuses::COLD; // статус лида  Холодный
        $companiesTable->data[0]['Статус клиента'] = ''; // статус постоянного клиента
        $companiesTable->data[0]['Вид'] = ClientEntities::LEGAL_ENTITY; // вид: Юр.лицо Физ.лицо
        $companiesTable->data[0]['Тип'] = ClientTypes::POTENTIAL; // тип: Потенциальный Клиент Партнер
        $companiesTable->data[0]['Категория'] = ClientCategories::SUPPLIER; // категория: Поставщик Заказчик

        $userId = (int)$user['id'];
        $user_group = (int)$user['group_id'];
        if (in_array(
            $user_group,
            [UserGroups::CUSTOMER_RELATIONS_DEPARTMENT_BOSS, UserGroups::SALES_MANAGERS],
            true)) {

            if (in_array($userId, [471, 640, 751], true)) {
                $companiesTable->data[0]['Менеджер клиента'] = $userId; // Менеджер клиента
            } else if (in_array($userId, [921, 911], true)) {
                $companiesTable->data[0]['Менеджер продаж'] = $userId; // Менеджер продаж
            }
        }
        $clientId = $companiesTable->save(0, false);

        if ($clientId > 0) {
            // добавляем строчку в таблицу статистики по клиенту
            $workWithClient = new Table("Работа с клиентами");
            $this->createClientStatistics($workWithClient, $clientId);
        }
        $this->updateParticipationInfo($clientId, $company->getOrgCompanyId());
    }

    private function createAdditionalContacts(TDBCompany $company): string
    {
        $additionalContacts = "";
        if (count($additional_phones = $company->getContactsPersonsPhones()) > 0) {
            $additionalContacts .= "Phones: " . implode("\r\n\t", $additional_phones) . "\r\n\r\n";
        }
        if (count($additional_emails = $company->getContactsPersonsEmails()) > 0) {
            $additionalContacts .= "Emails: " . implode("\r\n\t", $additional_emails) . "\r\n\r\n";
        }
        if (count($additional_full_names = $company->getContactsPersonsFullNames()) > 0) {
            $additionalContacts .= "Контактные лица: " . implode("\r\n\t", $additional_full_names);
        }
        if (empty($additionalContacts)) {
            return "Контактная информация не найдена";
        }
        return $additionalContacts;
    }

    public function run(array $companies)
    {
        foreach ($companies as $company) {
            $companyTable = new Company();
            $companyTable->getData([],
                [
                    "ИНН" => $company->getInn(),
                    "status" => 0
                ]);
            if (count($companyTable->data) > 0) {
                $this->updateCompany($company, $companyTable);
            } else {
                $this->createCompany($company, $companyTable);
            }
        }
    }

    private function getParticipationInfo($clientId, $dtStart, $dtNow): string
    {
        return "SELECT COUNT(*)                                              AS participation_count,
                       COUNT(*) FILTER ( WHERE \"AppRating\" = 1 )           AS win_count,
                       MAX(tLA.\"AppDatetime\")                              AS last_participation_date,
                       MIN(tLA.\"AppDatetime\")                              AS first_participation_date,
                       SUM(tLA.\"Cost\")                                     AS participation_sum,
                       SUM(tL.\"Cost\")                                      AS participation_start_sum,
                       SUM(tL.\"Cost\") FILTER ( WHERE \"AppRating\" = 1 )   AS win_start_sum,
                       SUM(tLA.\"Cost\") FILTER ( WHERE \"AppRating\" = 1 )  AS win_sum,
                       COUNT(CASE WHEN tL.\"sTenderLawId\" = 1 THEN 1 END)   AS count_44fz,
                       COUNT(CASE WHEN tL.\"sTenderLawId\" = 2 THEN 1 END)   AS count_223fz,
                       COUNT(CASE WHEN tL.\"sTenderLawId\" > 2 THEN 1 END)   AS count_other
                FROM \"tLotApp\" as tLA
                         LEFT JOIN \"tLot\" AS tL ON tL.\"tLotId\" = tLA.\"tLotId\"
                WHERE \"AppDatetime\" BETWEEN '$dtStart' AND '$dtNow'
                  AND \"orgSupplierId\" = $clientId
                  AND \"IsActual\" = TRUE";
    }

    private function createClientStatistics(Table $clientData, $clientId)
    {
        $clientData->data = [];
        $clientData->data[0]['status'] = 0;
        $clientData->data[0]['Компания'] = $clientId;
        $clientData->data[0]['Тип'] = ClientTypes::POTENTIAL;
        $clientData->data[0]['Статус клиента'] = '';
        $clientData->data[0]['Статус лида'] = LeadStatuses::COLD;
        $clientData->save(0, false);
    }

    private function isWithUs($lastContact, $daysDelay, $lastPartitionDate, &$bWithUs)
    {
        if ($lastContact !== "0000-00-00 00:00:00") {
            $dtCalc = $lastContact;
            $dtLimit = $daysDelay;
            $secondsDiff = strtotime($lastPartitionDate) - strtotime($dtCalc);
            $daysDiff = floor($secondsDiff / 86400.0);
            if ($daysDiff < $dtLimit) {
                $bWithUs = true;
            }
        }
    }

    private function isClientWorkWithoutNPO(ClientData $clientDataTable): string
    {
        // константы для мониторинга участий без нас
        $daysDelayApps = 30;
        $daysDelayComplex = 60;

        // получить данные из статистики
        $dtLastComplex = $clientDataTable->data[0]['Дата последнего заказа ТЗ'];
        $dtLastApp = $clientDataTable->data[0]['Дата последнего заказа заявки'];

        if ($clientDataTable->data[0]['Тип'] === "Клиент" && ($dtLastComplex !== "0000-00-00 00:00:00" || $dtLastApp !== "0000-00-00 00:00:00")) {
            $bWithUs = false;

            $this->isWithUs($dtLastComplex, $daysDelayComplex, $clientDataTable->data[0]['Дата последнего участия'], $bWithUs);
            $this->isWithUs($dtLastApp, $daysDelayApps, $clientDataTable->data[0]['Дата последнего участия'], $bWithUs);

            if ($bWithUs) {
                return 'Нет';
            }
        }
        return 'Да';
    }
}