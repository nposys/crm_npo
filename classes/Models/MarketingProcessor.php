<?php


namespace classes\Models;


use classes\Base\Common;
use classes\Base\Table;
use classes\Tables\Company;
use classes\Tables\Order;

class MarketingProcessor extends Common
{

	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	public function getMarketingData()
	{
	    // готовим списко регионов
		$region_list = [];
		$regions = new Table('Регионы');
		$regions->getData([], []);
		foreach ($regions->data as $region) {
			$region_list[(int)$region['ID']] = $region['Полное название'];
		}

		// готовим список каналов
		$channel_list = [];
		$channels = new Table('Источники клиентов');
		$channels->getData([], []);
		foreach ($channels->data as $channel) {
			$channel_list[(int)$channel['ID']] = $channel['Название'];
		}

		// ------------------------------------------------------------------------
        // получаем список Поступлений за конкретный период
        // ------------------------------------------------------------------------
        $filter_pos = [
            "date_from" => [
                'field_name' => 'Дата поступления',
                'operator' => '>=',
                'value' => $this->data['date_from'] . " 00:00:00",
                'add_quotes' => 1,
            ],

            "date_to" => [
                'field_name' => 'Дата поступления',
                'operator' => '<=',
                'value' => $this->data['date_to'] . " 23:59:29",
                'add_quotes' => 1,
            ],
            "sum" => [
                'field_name' => 'Сумма',
                'operator' => '>',
                'value' => 0,
                'add_quotes' => 1,
            ]
        ];
        if (!isset($this->data['show_rt'])) {
            $filter_pos['not_rt'] = [
                'field_name' => 'Клиент',
                'operator' => '!=',
                'value' => 3813,
                'add_quotes' => 1,
            ];
            $filter_pos['not_nt'] = [
                'field_name' => 'Клиент',
                'operator' => '!=',
                'value' => 15105,
                'add_quotes' => 1,
            ];
        }

		$pos_oii = new Table("Позиции заказы счета поступления");
        $pos_oii->getData(
            [
                'ID',
                [ 'field' => 'Оплачено сумма заказ', 'alias' => 'Сумма' ],
                [ 'field' => 'Дата поступления', 'alias' => 'Дата' ],
                'Клиент',
                'Услуга',
                'Поступление',
                'Счет',
                'Заказ',
            ],
            $filter_pos,
            [],
            [
                'open_related' => [
                    'Клиент',
                    'Услуга',
                    'Заказ'
                ]
            ]
        );

        // ------------------------------------------------------------------------
        // получаем список заказов за конкретный период
        // ------------------------------------------------------------------------
        $filter = [
			"date_from" => [
				'field_name' => 'Дата',
				'operator' => '>=',
				'value' => $this->data['date_from'] . " 00:00:00",
				'add_quotes' => 1,
			],

			"date_to" => [
				'field_name' => 'Дата',
				'operator' => '<=',
				'value' => $this->data['date_to'] . " 23:59:29",
				'add_quotes' => 1,
			],
			"sum" => [
				'field_name' => 'Сумма',
				'operator' => '>',
				'value' => 0,
				'add_quotes' => 1,
			]
		];

		if (!isset($this->data['show_rt'])) {
			$filter['not_rt'] = [
				'field_name' => 'Клиент',
				'operator' => '!=',
				'value' => 3813,
				'add_quotes' => 1,
			];
			$filter['not_nt'] = [
				'field_name' => 'Клиент',
				'operator' => '!=',
				'value' => 15105,
				'add_quotes' => 1,
			];
		}

		$orders = new Order();
		$orders->getData(
			[
				'ID',
				'Сумма',
				'Дата',
				'Клиент',
				'Услуга'
			],
			$filter,
			[],
			[
				'open_related' => [
					'Клиент',
					'Услуга',
					'Источник'
				]
			]
		);
		$result_data = [];
		$first_order = new Order();
		$company = new Company();

		$goc = new Table("Группы контрагентов");
		//foreach ($orders->data as $data) {
        foreach ($pos_oii->data as $data) {
			$company_id = (int)$data['Клиент']['ID'];
			$company_name = $data['Клиент']['Название'];
            $company_inn = $data['Клиент']['ИНН'];

            $company_group_id = (int)$data['Клиент']['Группа компаний'];
            $company_group = "";
            if($company_group_id >0 ){
                $goc->getDataById($company_group_id);
                $company_group = $goc->data[0]['Название группы'];
            }

            $first_channel = $channel_list[(int)$data['Клиент']['Источник']];
			$result_data['select_companies'][$company_id]= $company_name;
			$result_data['select_channels'][(int)$data['Клиент']['Источник']]= $first_channel;

			if ((int)$this->data['select_company_id'] == 0 || (int)$this->data['select_company_id'] == $company_id) {

			    /*
				$first_order->getData(
					[
						'min_date' => [
							'field' => 'Дата',
							'function' => 'MIN',
							'alias' => 'min_date'
						]
					],
					[
						'Клиент' => $data['Клиент']['ID'],
						'date' => [
							'field_name' => 'Дата',
							'operator' => '>',
							'value' => "0000-00-00 00:00:00",
							'add_quotes' => 1
						]
					]);
			    */

                $first_order = $company->getFirstOrderByClientGroup($company_id);
                $first_event = $company->getFirstEventByClientGroup($company_id);
                $count_events = $company->getNumEventsByClientGroup($company_id);

				$company_user_id = $data['Клиент']['МетрикаUserID'];
				$company_region = $region_list[(int)$data['Клиент']['Регион']];
				$company_reg_date = date("d-m-Y", strtotime($data['Клиент']['Время добавления']));

				$order_id = (int)$data['ID'];
				$order_sum = (float)$data['Сумма'];
				$order_date = date("d-m-Y", strtotime($data['Дата']));
				$service_title = $data['Услуга']['Наименование'];
				$service_channel = (int)$data['Услуга']['ID'] == 4 ? 'tm' : 'npo';
				$channel_count_row = (int)$result_data['companies'][$company_id]['order_' . $service_channel];

				$company_first_order_date = date("d-m-Y", strtotime($first_order['date']));
                $company_first_event_date = date("d-m-Y", strtotime($first_event['date']));

				$result_data['total_sum'] += $order_sum;
				$result_data['companies'][$company_id]['order_' . $service_channel]++;
				$result_data['companies'][$company_id]['total_sum'] += $order_sum;

				$result_data['companies'][$company_id]['company_name'] = $company_name;
                $result_data['companies'][$company_id]['company_inn'] = $company_inn;
                $result_data['companies'][$company_id]['company_group'] = $company_group;
                $result_data['companies'][$company_id]['company_group_id'] = $company_group_id;
				$result_data['companies'][$company_id]['sort_company_name'] = $company_name;

				$result_data['companies'][$company_id]['company_user_id'] = $company_user_id;
				$result_data['companies'][$company_id]['company_region'] = $company_region;
				$result_data['companies'][$company_id]['sort_company_region'] = $company_region;

				$result_data['companies'][$company_id]['company_reg_date'] = $company_reg_date;
				$result_data['companies'][$company_id]['sort_company_reg_date'] = strtotime($data['Клиент']['Время добавления']);

                $result_data['companies'][$company_id]['company_first_event_date'] = $company_first_event_date;
                $result_data['companies'][$company_id]['sort_company_first_event_date'] = strtotime($first_event['date']);

                $result_data['companies'][$company_id]['company_first_order_date'] = $company_first_order_date;
				$result_data['companies'][$company_id]['sort_company_first_order_date'] = strtotime($first_order['date']);

				$result_data['companies'][$company_id]['first_channel'] = $first_channel;
				$result_data['companies'][$company_id]['sort_first_channel'] = $first_channel;

				$result_data['companies'][$company_id]['count_visits'] = $count_events;
				$result_data['companies'][$company_id]['sort_count_visits'] = $count_events;

				$result_data['companies'][$company_id]['orders'][$channel_count_row]['service_title_' . $service_channel] = $service_title;
				$result_data['companies'][$company_id]['orders'][$channel_count_row]['order_sum_' . $service_channel] = $order_sum;
				$result_data['companies'][$company_id]['orders'][$channel_count_row]['order_date_' . $service_channel] = $order_date;
				$result_data['companies'][$company_id]['orders'][$channel_count_row]['order_id_' . $order_id] = $order_id;
			}
		}
		asort($result_data['select_companies']);
		if(!empty($this->data['sort_col']) && !empty($this->data['sort_direction'])) {
			function build_sorter($key,$direction)
			{
				return function ($a, $b)use($key,$direction)
				{
					if ($a['sort_'.$key] == $b['sort_'.$key]) {
						return 0;
					}
					if($direction=='asc') {
						return ($a['sort_' . $key] < $b['sort_' . $key]) ? -1 : 1;
					}else{
						return ($a['sort_' . $key] < $b['sort_' . $key]) ? 1 : -1;
					}
				};
			}
			usort($result_data['companies'], build_sorter($this->data['sort_col'],$this->data['sort_direction']));
		}
		$this->formatNumericData($result_data);
		return $result_data;
	}
}