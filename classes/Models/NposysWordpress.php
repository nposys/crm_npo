<?php

/* cron

require_once 'add/db.php';
require_once 'add/vendor/autoload.php';

use classes\Models\NposysWordpress;

$nposys = new NposysWordpress('NPOSYS', $databases, 2);
$nposys->syncUsersRequests();

$nposys = new NposysWordpress('NSK.NPOSYS', $databases, 2);
$nposys->syncUsersRequests();

 */

namespace classes\Models;

use classes\Base\Common;
use classes\Base\Table;
use classes\Tables\Company;
require_once 'add/functions/service_functions.php';

class NposysWordpress extends Common
{
    private $db_param;      // настройки подключения к БД
	private $mysqli;        // подключение к БД сайта nposys.ru
    private $interval;      // сколько дней в прошлое обрабатывать

    private $site_name = 'NPOSYS'; // какой сайт обрабатываем
    private $site_id = 1;

    /* Списко форм плагина Caldera Forms в CMS WordPress сайта nposys.ru */
	private $forms = [
	    // сайт nposys.ru
	    "NPOSYS" => [
            "CF5f72b6f81df8b" => "Вопрос калькулятор Экспертного поиска",
            "CF5d824072cc61c" => "Вопрос калькулятор стоимости Заявки",
            "CF5d73bee1871c8" => "Задать вопрос со страницы Вопрос-ответ",
            "CF5f7193df39a5f" => "Заказать Банковскую гарантию/Проверку документов Банер",
            "CF5f719416b65d6" => "Заказать Банковскую гарантию/Проверку документов Банер мобильная",
            "CF5f7193904c785" => "Заказать Банковскую гарантию/Проверку документов Футер",
            "CF5f71a3bc174ee" => "Заказать Жалобу Банер",
            "CF5f71a3fa48e3a" => "Заказать Жалобу Банер мобильная",
            "CF5f71a47e0f2e8" => "Заказать Жалобу Футер",
            "CF5f71ab53e79f1" => "Заказать Тендерную аналитику Банер",
            "CF5f71aedc0b128" => "Заказать Тендерную аналитику Банер мобильная",
            "CF5f71af05c011b" => "Заказать Тендерную аналитику Футер",
            "CF5f71992bd3ba2" => "Заказать ЭЦП Банер",
            "CF5f7199612f23b" => "Заказать ЭЦП Банер мобильная",
            "CF5f7198f53d3e7" => "Заказать ЭЦП Футер",
            "CF5f71a0d37aa8c" => "Заказать Юридическую консультацию Банер",
            "CF5f71a101a475d" => "Заказать Юридическую консультацию Банер мобильная",
            "CF5f71a1384c73a" => "Заказать Юридическую консультацию Футер",
            "CF5d8aeeaee158b" => "Заказать звонок Верхнее меню",
            "CF5f64547d4666f" => "Заказать индивидуальный расчет стоимости тарифа Участнику торгов/Тендерное сопровождение Банер",
            "CF5f64544510c93" => "Заказать индивидуальный расчет стоимости тарифа Участнику торгов/Тендерное сопровождение Банер мобильная",
            "CF5f487a6c30df7" => "Заказать индивидуальный расчет стоимости тарифа Участнику торгов/Тендерное сопровождение Выбор тарифа",
            "CF5f4899bd4f977" => "Заказать индивидуальный расчет стоимости тарифа Участнику торгов/Тендерное сопровождение Выбор тарифа мобильная",
            "CF5f6df9380063f" => "Заказать индивидуальный расчет стоимости тарифа Участнику торгов/Тендерное сопровождение Футер",
            "CF5f7184143893f" => "Заказать подготовку Заявки Банер",
            "CF5f71848f325e9" => "Заказать подготовку Заявки Банер мобильная",
            "CF5f71883df16f5" => "Заказать подготовку Заявки Футер",
            "CF5d8af52781701" => "Заказать тариф ТендерМонитор",
            "CF5f6e0d3367e94" => "Получить бесплатную консультацию Быстрый старт Банер",
            "CF5f6e0d6b74a1a" => "Получить бесплатную консультацию Быстрый старт Банер мобильная",
            "CF5f6e0dac689ca" => "Получить бесплатную консультацию Быстрый старт Футер",
            "CF5ecf2c8c41711" => "Получить бесплатную консультацию Главная страница Банер",
            "CF5f3a4a69de6f2" => "Получить бесплатную консультацию Главная страница Банер мобильная",
            "CF5f6daaf3f423a" => "Получить бесплатную консультацию Главная страница Футер",
            "CF5f6e045046e2e" => "Получить бесплатную консультацию Заказчику Банер",
            "CF5f6e04a01875c" => "Получить бесплатную консультацию Заказчику Банер мобильная",
            "CF5f6e04dba7e23" => "Получить бесплатную консультацию Заказчику Футер",
            "CF5f6e17754aff3" => "Получить бесплатную консультацию Экспертный поиск Банер",
            "CF5f6e17b18a198" => "Получить бесплатную консультацию Экспертный поиск Банер мобильная",
            "CF5f6e17e3909b5" => "Получить бесплатную консультацию Экспертный поиск Футер",

            "CF5d8addc7ee19d"=>"Скачать книгу", // не собирает данные
            "CF5d777ceb4c900"=>"Сумма",
            "CF5d89aa1ab2cde"=>"Телефон перезвоните калькулятор",
            "CF5d74a8dab1757"=>"Зарегистрироваться",
            "CF5d735c201187a"=>"Горизонтальная Заказать", // Собственнику, Тендер-менеджеру
            "CF5d73220106fe9"=>"Горизонтальная выиграть",

            //"CF5d824072cc61c"=>"Заказать звонок",
            //"CF5d8aeeaee158b"=>"Заказать звонок",
            //"CF5d8af52781701"=>"Заказать тариф",
            //"CF5d73bee1871c8"=>"Задать вопрос",

        ],

        // сайт nsk.nposys.ru
        "NSK.NPOSYS" => [
            "CF5f8add3b07a08" => "Выбрать тариф Комплексное сопровождение Popup",
            "CF5f364d7fe3b21" => "Заказать БЕСПЛАТНУЮ консультацию по тендерному сопровождению",
            "CF5f0fd6ebd2288" => "Заказать БЕСПЛАТНУЮ консультацию по тендерному сопровождению мобильная",
            "CF5f4469bdbb684" => "Заказать Банковская гарантия бесплатная консультация",
            "CF5f5374f499b88" => "Заказать Банковская гарантия бесплатная консультация мобильная",
            "CF5f7febd0a6ce9" => "Заказать Бесплатная консультация Быстрый старт",
            "CF5f7febfde80f9" => "Заказать Бесплатная консультация Быстрый старт мобильная",
            "CF5f7ff91b010e3" => "Заказать Отраслевую аналитику",
            "CF5f7ff93ba9c95" => "Заказать Отраслевую аналитику мобильная",
            "CF5f8001180325c" => "Заказать Тендерную аналитику",
            "CF5f800131d8b79" => "Заказать Тендерную аналитику мобильная",
            "CF5f15c425e03ce" => "Заказать Экспертный поиск 7 дней бесплатно",
            "CF5f15c4565bf64" => "Заказать Экспертный поиск 7 дней бесплатно мобильная",
            "CF5f86a5d6df9d0" => "Заказать бесплатную консультацию Комплексное сопровождение",
            "CF5f86a5f743b71" => "Заказать бесплатную консультацию Комплексное сопровождение мобильная",
            "CF5f43a5b674dd6" => "Заказать выпуск Электронной цифровой подписи",
            "CF5f526494eb631" => "Заказать выпуск Электронной цифровой подписи мобильная",
            "CF5f97de787d29f" => "Заказать звонок",
            "CF5f487a6c30df7" => "Заказать индивидуальный расчет стоимости тарифа Участнику торгов/Тендерное сопровождение Выбор тарифа",
            "CF5f43ac4d499a0" => "Заказать подготовку Заявки",
            "CF5f52723ba100d" => "Заказать подготовку Заявки мобильная",
            "CF5f539b6301cd1" => "Заказать подготовку жалобы",
            "CF5f539b79ac2f6" => "Заказать подготовку жалобы мобильная",
            "CF5f8adce1d8c30" => "Рассчитать тариф Комплексное сопровождение Popup",
            "CF5f8addcba506e" => "Регистрация ТМ Popup",
            "CF5f9e754636070" => "Скачать/Заказать КП"
        ]
    ];

	public function __construct($site_name, $databases, $interval = 2)
    {
        $this->site_name = $site_name;
        $this->db_param = $databases[$site_name];
        $this->interval = $interval;

        switch ($this->site_name) {
            default:
            case "NPOSYS":
                $this->site_id = 1;
                break;
            case "NSK.NPOSYS":
                $this->site_id = 2;
                break;
        }

        $this->checkConnection();
    }

    public function __destruct ()
    {
        $this->closeConnection();
    }

    private function getFormName ($form_id)
    {
        return $this->forms[$this->site_name][$form_id];
    }

    /*
     * Метод для копирования новых запросов пользователей из CMS в CRM
     */
	public function syncUsersRequests()
    {
        var_dump($this->site_name);
		$this->checkConnection();

		$query = "SELECT 
					    f.form_id,
					    fe.datestamp + interval 7 hour as order_date,
					    fe.id AS order_id,
					    fev.slug AS field,
					    fev.value,
					    u.ID as user_id
					FROM
					    wp_cf_forms AS f
					        INNER JOIN wp_cf_form_entries AS fe 
					          ON f.form_id = fe.form_id
					        INNER JOIN wp_cf_form_entry_values AS fev 
					          ON fe.id = fev.entry_id
					    	LEFT JOIN ad_User AS u 
					    	  ON u.Email IN 
					    	    (SELECT 
					    	        value 
					    	      FROM 
					    	        wp_cf_form_entry_values 
					    	      WHERE 
					    	        entry_id= fe.id 
					    	        AND slug = 'e_mail')
					WHERE
					    f.type = 'primary'
					        AND fe.status = 'active'
					        AND fev.slug IN 
					          ('e_mail' , 
					          'phone', 
					          'name',
					          'comment', 
					          'page', 
					          'tarif_name', 
					          'tarif_cost', 
					          'month_count')
					        AND fe.datestamp >= NOW()-interval $this->interval DAY
					ORDER BY 
					  fe.datestamp, 
					  fe.id";

		// формируем список заказов за указанный период
		$orders=[];
		if ($result = $this->mysqli->query($query)) {
			$requests = $result->fetch_all(MYSQLI_ASSOC);

			foreach ($requests as $request){
				$order_id = (int)$request['order_id'];
				$form_id = $request['form_id'];
				$order_date = $request['order_date'];
				$field = $request['field'];
				$user_id = $request['user_id'];
				$value = trim($request['value']);
				$form_name = $this->getFormName($form_id);

                $orders[$order_id]['site_id'] = $this->site_id;
                $orders[$order_id]['order_id'] = $order_id;
				$orders[$order_id]['form_id'] = $form_id;
                $orders[$order_id]['form_name'] = $form_name;
				$orders[$order_id]['order_date'] = $order_date;
				$orders[$order_id]['user_id'] = $user_id;

				if(!empty($value)) {
					$orders[$order_id]['fields'][$field] = $value;
				}
			}
			$result->close();
		}

		write_log("Total ".count($orders)." new orders from site");

		// создаем в CRM задачи по работе с обращениями клиентов (если нет клиента, то его тоже создаст
        $new_work_count = 0;
		$company_work =  new Table("Работа с клиентами");
		foreach ($orders as $order_id => $order){
			if($order_id>0){
                $company_work->getData(['ID'], ['idNposys'=>    $order_id, 'sourceNposys'=>$this->site_id]);
				if((int)$company_work->data[0]['ID'] == 0){
					$id_work = $this->createOrder($order);

					if($id_work>0){
                        $new_work_count++;
                    }
				} else {
				    write_log("work with id $order_id already exists");
                }
			}
		}

		$this->closeConnection();
	}

	/*
	 * метод для создания заказа на базе данных CalderaForms
	 */
	private function createOrder($request)
    {
        $site_id = (int)$request['site_id'];
        $order_id = (int)$request['order_id'];
		$form_id = $request['form_id'];
		$order_date = $request['order_date'];
		$fields = $request['fields'];

		$company = $this->getCompany($request);
		if((int)$company['ID'] == 0){
            return;
		}

		$client_task = new Table("Работа с клиентами");
        $client_task->data[0]['sourceNposys'] = $site_id;
        $client_task->data[0]['idNposys'] = $order_id;
		$client_task->data[0]['Компания'] = (int)$company['ID'];
		$client_task->data[0]['Тип'] = 'Запрос с формы';
		$client_task->data[0]['Дата'] = $order_date;
		$client_task->data[0]['Менеджер'] =
            (int)$company['Менеджер клиента']>0 ? (int)$company['Менеджер клиента'] : (int)$company['Менеджер продаж'];

        $form_name = $this->getFormName($form_id);
		if($form_name != "") {
			$client_task->data[0]['Описание'] = 'Форма:' . $this->site_name .":". $form_name .';';
		}else{
			$client_task->data[0]['Описание'] = 'Форма:' . $this->site_name .":". $form_id .';';
		}

		if(isset($fields['page']) && $fields['page']!=''){
			$client_task->data[0]['Описание'] .= ' Страница:' . $fields['page'].';';
		}

		if(isset($fields['comment']) && $fields['comment']!=''){
			$client_task->data[0]['Описание'] .= ' Комментарий:' . $fields['comment'].';';
		}

		// сохраняем новую запись о работе с клиентом
		$newOrderId = (int)$client_task->save();
        write_log ("created new work id:$newOrderId for client id:".$company['ID']);

		return $newOrderId;
	}

	/*
	 * метод для поиска компании или создания новой карточки, если компания не найдена
	 */
	private function getCompany($request)
	{
		$fields = $request['fields'];
		$user_id = $request['user_id'];

		$company = new Company();
		if((int)$user_id>0){
			$company->getData([], ['idClientNposys' => $user_id]);
		}

		if ((int)$company->data[0]['ID'] == 0 && isset($fields['e_mail'])) {
			if(strpos($fields['e_mail'],'@nposys.ru')!==false){
				return 0;
			}
			$company->getData([], ['E-mail' => '\''.$fields['e_mail'].'\'']);
		}

		if ((int)$company->data[0]['ID'] == 0 && isset($fields['phone'])) {
			$company->getData([], ['Телефон' => '\''.$fields['phone'].'\'']);
		}

		if (isset($fields['e_mail'])&& (!isset($company->data[0]['E-mail']) || empty($company->data[0]['E-mail']))) {
			$company->data[0]['E-mail'] = $fields['e_mail'];
			$company->data[0]['E-mail для рассылок'] = $fields['e_mail'];
		}

		if (isset($fields['phone']) && (!isset($company->data[0]['Телефон']) || empty($company->data[0]['Телефон']))) {
			$company->data[0]['Телефон'] = $fields['phone'];
		}

		if((int)$user_id > 0){
			$company->data[0]['idClientNposys'] = $user_id;
		}

		if ((int)$company->data[0]['ID'] == 0) {
			$company->data[0]['Вид'] = 'Физ.лицо';
			$company->data[0]['Тип'] = 'Потенциальный';
			$company->data[0]['Источник'] = 8;
			$company->data[0]['Маркетинговый ресурс'] = 1;
			$company->data[0]['Статус лида'] = 'Интерес';
			$company->data[0]['Статус клиента'] = 'В работе';

            $default_managers = data_select_array(1271, "status=0 and f21231='sync_nposys_clients'");
            $default_manager_sales = data_select_array(46, "status=0 and id=".$default_managers['f21211']);
            $default_manager_clients = data_select_array(46, "status=0 and id=".$default_managers['f21221']);
            // $default_manager_production = data_select_array(46, "status=0 and id=".$default_managers['f21241']);

            $company->data[0]['Менеджер продаж'] = $default_manager_sales['f1400'];     // пользователь
			$company->data[0]['Менеджер продаж НПО'] = $default_managers['f21211'];     // Менеджер продаж НПО
			$company->data[0]['Менеджер клиента'] = $default_manager_clients['f1400'];  // пользователь
			$company->data[0]['Менеджер клиента НПО'] = $default_managers['f21221'];    // Менеджер клиента НПО

			if (isset($fields['name'])) {
				$company->data[0]['ФИО'] = $fields['name'];
				$company->data[0]['Название'] = $fields['name'];
			}

			// сохраняем нового клиента
			$id = $company->save();
			$company->data[0]['ID'] = $id;

		} else {

            // обновляем данные найденного клиента
			$company->save();
		}

		return $company->data[0];
	}

	private function checkConnection(){
		if(!$this->mysqli || !$this->mysqli->ping()){
			$this->createConnection();
		}
	}

	private function createConnection()
	{
		$this->mysqli = mysqli_init();
		if (!$this->mysqli->real_connect(
            $this->db_param['server'],
            $this->db_param['user'],
            $this->db_param['pass'],
            $this->db_param['db'])) {
			die('Ошибка подключения (' . mysqli_connect_errno() . ') '. mysqli_connect_error());
		}
		$this->mysqli->query("SET NAMES 'utf8'");
	}

    private function closeConnection()
    {
        if ($this->mysqli) {
            $this->mysqli->close();
        }
    }
}
