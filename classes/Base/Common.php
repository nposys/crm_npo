<?php


namespace classes\Base;


class Common
{
	public $log_message;

	/**
	 * @param $obj
	 * @param string $mode
	 */
	public function writeLog($obj, $mode = 'echo')
	{
		$message = "[" . date("Y-m-d H:i:s", time()) . "] ";
		if (is_array($obj) || is_object($obj)) {
			$message .= print_r($obj, true);
		} else {
			$message .= $obj;
		}

		switch ($mode) {
			case 'store':
				$this->log_message .= $message;
				break;
			case 'file':
				break;
			default:
			case 'echo':
				$message .= '<br>' . "\r\n";
				echo $message;
				break;
		}

	}

	/**
	 * @param $data - массив для обработки
	 * @param int $level - для ограничения глубины рекурсии
	 */
	public function formatNumericData(&$data, $level = 0)
	{
		if (is_array($data)) {
			foreach ($data as $key => &$value) {
				if (!is_array($value) || $level < 10) {
					$this->formatNumericData($value, $level + 1);
				}
			}
		} else {
			if (is_float($data)) {
				$data = number_format($data, 2, ',', ' ');
			} else if (is_int($data)) {
				$data = number_format($data, 0, ',', ' ');
			}
		}
	}
}