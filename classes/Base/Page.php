<?php


namespace classes\Base;


use classes\Base\Template;

abstract class Page
{
	public $request = [];
	protected $template;

	public function __construct($pageRoot = '')
	{
			$this->template = new Template($pageRoot);

	}

	public function __toString()
	{
		return $this->template->getPage();
	}

	public function fillPage($tmp, $vars = [])
	{
		if (!$tmp || !$vars) {
			return null;
		}
		foreach ($vars as $key => $value) {
			/** @var Template $tmp */
			$tmp->setParam($key, $value, 'null');
		}
		return $tmp;
	}
	protected function collect($parts, $root = true) {
		if ($root) $root = $_SERVER['DOCUMENT_ROOT'];
		foreach ($parts as $part) {
			$t = $root . $part . '.php';
			$this->template[(string)$part] = $this->handle(new Template(
				file_get_contents($root . $part . '.php')
			));
		}
	}

}