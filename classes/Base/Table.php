<?php


namespace classes\Base;


class Table extends Common
{
	public $tableName;      // название таблицы
	public $tableId;        // ID таблицы
	public $fields;         // ассоциативный массив Название поля и ID поля
	public $fields_id;         // ассоциативный массив Название поля и ID поля
	public $data;           // данные из таблицы по фильтру
	public $num_rows;       // количество рядов данных
	public $only_active_records;    // возвращать только Активные записи
	public $sql_query;

	public static $field_types = [
		1=>'Число' ,
		2=>'Дата/время',
		3=>'Текст',
		4=>'Список',
		5=>'Связь',
		6=>'Файл',
		7=>'Пользователь',
		8=>'Изображение',
		9=>'Группа',
		10=>'Число',        // ID
		11=>'Пользователь', // Кто добавил
		12=>'Дата/время',   // Время добавления
		13=>'Список',       // Статус записи    = активные / архив / удаленные
		14=>'Группа',
	];

	public static $service_fields = [
		'id' => 'ID',
		'user_id' => 'Кто добавил',
		'add_time' => 'Время добавления',
		'status' => 'Статус записи'
	];

	/**
	 * Table constructor.
	 * @param $tableName string
	 * @param $options array
	 * @throws table not exists, empty table name
	 */
	public function __construct($tableName, $options = [])
	{
		if (trim($tableName)!=='') {

			$this->tableName = $tableName;

			// по умолчанию показываем только активные записи
			$this->only_active_records = true;
			if (isset($options['active']) && $options['active']===false) {
				$this->only_active_records = false;
			}

			// выборка из перечня таблиц
			$table_result = sql_select_field(TABLES_TABLE, 'id', 'name_table=\'' . $tableName . '\'');
			if ($table_row = sql_fetch_assoc($table_result)) {
				$this->tableId = (int)$table_row['id'];
				$field_result = sql_select_field(FIELDS_TABLE, 'id, name_field, type_field, type_value', 'table_id=' . $this->tableId);
				while ($field_row = sql_fetch_assoc($field_result)) {
					$id = (int)$field_row['id'];
					$name = $field_row['name_field'];
					$type = (int)$field_row['type_field'];

					$this->fields[$name]['id'] = $id;
					$this->fields[$name]['type'] = $type;
					$this->fields[$name]['table_field'] = $this->getTableField($id, $type);

					$this->fields_id[$this->getTableField($id, $type)]['name'] = $name;
					$this->fields_id[$this->getTableField($id, $type)]['type'] = $type;

					if ($type === self::getTypeIdByTypeName('Связь')) {
						// для поля типа Связь, заполняем данные о связанной таблице и связанном поле
						$type_value = explode('|', $field_row['type_value']);
						$this->fields[$name]['table_related']['id'] = (int)$type_value[0];
						$this->fields[$name]['table_related']['name'] = self::getTableNameById((int)$type_value[0]);
						$this->fields[$name]['table_field_related']['id'] = (int)$type_value[1];
						$this->fields[$name]['table_field_related']['fid'] = $this->getTableField((int)$type_value[1], $type);
						$this->fields[$name]['table_field_related']['name'] = self::getFieldNameById((int)$type_value[1]);

						$this->fields_id['f'.$id]['table_related']['id'] = (int)$type_value[0];;
						$this->fields_id['f'.$id]['table_related']['name'] = self::getTableNameById((int)$type_value[0]);
						$this->fields_id['f'.$id]['table_field_related']['id'] = (int)$type_value[1];
						$this->fields_id['f'.$id]['table_field_related']['fid'] = $this->getTableField((int)$type_value[1], $type);
						$this->fields_id['f'.$id]['table_field_related']['name'] = self::getFieldNameById((int)$type_value[1]);
					}
				}
			} else {
				throw new \Exception('Table name:['. $tableName.'] not found');
			}
		} else {
			throw new \Exception('Empty table name provided');
		}
	}

	public static function getUserIdByName($userName)
	{
		$user = sql_select_array(USERS_TABLE, 'fio=\''.$userName.'\'');
		if (is_array($user)) {
			return $user['id'];
		} else {
			return 0;
		}
	}

	public static function getUserNameById($userId)
	{
		if ((int)$userId>0) {
			$user = sql_select_array(USERS_TABLE, 'id='.$userId);
			if (is_array($user)) {
				return $user['fio'];
			} else {
				return 0;
			}
		} else {
			return '';
		}
	}
	public static function getTypeNameByTypeId($typeId)
	{
		if (isset(self::$field_types[$typeId])) {
			return self::$field_types[$typeId];
		} else {
			return '';
		}
	}

	public static function getTypeIdByTypeName($typeName)
	{
		return array_search ($typeName, self::$field_types, TRUE);
	}
	/**
	 * Получить название таблицы по id
	 * @param $tableId - Id таблицы
	 */
	public static function getTableNameById($tableId)
	{
		if ((int)$tableId > 0) {
			$table_result = sql_select_field(TABLES_TABLE, 'name_table', 'id=' . $tableId . '');
			if ($table_row = sql_fetch_assoc($table_result)) {
				return $table_row['name_table'];
			}
		}
		return '';
	}

	/**
	 * Получить id таблицы по названию
	 */
	public static function getTableIdByName($tableName)
	{
		if (trim($tableName) !== '') {
			$table_result = sql_select_field(TABLES_TABLE, 'id', 'name_table=\'' . $tableName . '\'');
			if ($table_row = sql_fetch_assoc($table_result)) {
				return (int)$table_row['id'];
			}
		}
		return 0;
	}

	/**
	 *  Получить название поля по Id
	 */
	public static function getFieldNameById($fieldId)
	{
		if (strpos($fieldId, 'f') === 0) {
			$fieldId = (int)substr($fieldId, '1');
		}

		if ((int)$fieldId > 0) {
			$table_result = sql_select_field(FIELDS_TABLE, 'name_field', 'id=' . $fieldId . '');
			if ($table_row = sql_fetch_assoc($table_result)) {
				return $table_row['name_field'];
			}
		}
		return 0;
	}

	/**
	 * Получить id поля по названию - не точно, т.к. названия могут совпадать
	 */
	public static function getFieldIdByName($fieldName, $tableId = 0)
	{
		if (trim($fieldName) !== '') {
			if ((int)$tableId > 0) {
				$table_result = sql_select_field(FIELDS_TABLE, 'id', 'name_field=\'' . $fieldName . '\'' . ' and table_id=' . (int)$tableId);
			} else {
				$table_result = sql_select_field(FIELDS_TABLE, 'id', 'name_field=\'' . $fieldName . '\'');
			}

			if ($table_row = sql_fetch_assoc($table_result)) {
				return (int)$table_row['id'];
			}
		}
		return 0;
	}

	/**
	 * Заменят название внутренних полей БД на человеко-читаемые названия
	 * @param $row - ряд из БД
	 */
	public static function replaceFieldsNames($row)
	{
		global $service_fields;

		$new_row = [];
		$table_filed_name = array_keys($row);
		foreach ($table_filed_name as $field_name) {
			if (isset($service_fields[$field_name])) {
				$field_human_readable_name = $service_fields[$field_name];
			} else {
				$field_human_readable_name = self::getFieldNameById($field_name);
				if ($field_human_readable_name === 0) $field_human_readable_name = $field_name;
			}
			$new_row[$field_human_readable_name] = $row[$field_name];
		}

		return $new_row;
	}



	/**
	 * @param $id - id поля из таблицы FIELDS
	 * @param $type - тип поля из таблицы FIELDS
	 * @return string - возвращаем текстовое значение поля для запроса в БД
	 */
	private function getTableField($id, $type)
	{
		switch ($type) {
			case 10:
				return 'id';
			case 11:
				return 'user_id';
			case 12:
				return 'add_time';
			case 13:
				return 'status';
			default:
				return 'f' . $id;
		}
	}

	/**
	 * @param $fields_array - поля для выборки - массив имен на русском языке
	 *      ['Поле1', 'Поле2', 'Поле3']
	 *          либо
	 *      ['Поле1' => ['field'=>'Поле1', 'function'=>'SUM', 'alias'=>'MySum'], 'Поле2', 'Поле3' => ['field'=>'Поле3', 'function'=>'COUNT']]
	 * @param $condition_array
	 *      - массив условий для формирования строки WHERE вида
	 *          'Имя условия' => [
	 *              'field_name' => 'Контрагент'
	 *              'operator' => '=',
	 *              'value'=>'20',
	 *              'add_quotes' => 1,
	 *              'related_field' => 'Название'      - искать в значениии связанного поля, если не указано то ищем по ID
	 *          ]
	 *              либо
	 *          'Имя поля' => 'Значение поля'
	 *
	 *  допускается передавать служебные операторы на ряду с полями.
	 *  в этом случае система объеденить условия через пробел
	 *  для этого необходимо также передать опцию raw_condition = 1
	 * пример:
	 *         $filter[] = '(';
	 *          $filter['Менеджер'] = $user_id;
	 *          $filter[] = 'OR';
	 *          $filter['Дополнительный менеджер'] = $user_id;
	 *          $filter[] = ')';
	 *          $filter[] = 'AND';
	 *          $filter['Статусы'] = [
	 *          'field_name' => 'Статус',
	 *          'operator' => '<',
	 *          'value'=>'30',
	 *          'add_quotes' => 1,
	 *          ];
	 *
	 * @param order_array
	 *      массив с полями для сортировки
	 *      [
	 *          [
	 *              'field_name' => 'Поле1',
	 *              'direction' => 'ASC'
	 *          ]
	 *      ]
	 *
	 * @param $options - настройки выборки
	 *      replace_related_data - 0 или 1 заменять ли ID строк связанной таблицы на значение связанного поля
	 *          !!!! сильно замедляет запрос если заменям связанные поля и таких полей много
	 *      open_related - передаем массив полей родительской таблицы, которые стоит раскрыть
	 *             в итоге будет подстановка вместо ID значеня - массива со значениями из связанного поля
	 *      link_operator   - and / or
	 *      rows - 1,2,... сколько рядов возвращать, при неуказании параметра вернет все
	 *      debug - 1  сохраянет сформированный SQL запрос
	 *      raw_condition - 1 для трактовки запрос как прямого
	 *
	 * @return $data[]
	 */
	public function getData($fields_array = [], $condition_array = [], $order_array = [], $options = [])
	{
		$this->data = [];

		if ($fields_array === []) {
			foreach ($this->fields as $field_name => $field_data) {
				$fields_array[] = $field_name;
			}
		}

		// подготовка строки с полями для выборки
		$db_fields_array = [];
		$db_group_array = [];
		$have_function_fields = false;
		foreach ($fields_array as $field_key => $field) {
			if (is_array($field)) {
				if (isset($this->fields[$field['field']])) {
					$next_field = $field['function'].'('.$this->fields[$field['field']]['table_field'].')';
					if (isset($field['alias'])) {
						$next_field .= ' AS '.$field['alias'];
					} else {
						$fields_array[$field_key]['alias'] =  $next_field;
					}
					$db_fields_array[] = $next_field;
				}
				$have_function_fields = true;
			} else {
				if (isset($this->fields[$field])) {
					$db_fields_array[] = $this->fields[$field]['table_field'];
					$db_group_array[] = $this->fields[$field]['table_field'];
				}
			}
		}
		$field_string = implode(",", $db_fields_array);
		$group_string = '';
		if ($have_function_fields ) {
			$group_string = implode(",", $db_group_array);
			if ($group_string !== '') {
				$group_string = ' GROUP BY '.$group_string;
			}
		}

		// подготовка строки WHERE
		$have_service_operands = false;
		$db_condition_array = [];
		foreach ($condition_array as $cond_name => $condition) {
			if(is_int($cond_name) && isset($options['raw_condition']) && $options['raw_condition'] == 1){
				// добавляем напрямую служебные операторы
				$db_condition_array[] = $condition;
				$have_service_operands = true;
			} else {
				if (is_array($condition)) {
					$field_name = $condition['field_name'];
					if (isset($this->fields[$field_name])) {

						// если тип поля Связь, а также указано свяазанное поле, то работаем со вложенной таблице
						if ((int)$this->fields[$field_name]['type'] === self::getTypeIdByTypeName('Связь') &&
							isset($condition['related_field']))
						{
							// получаем все ID записей из связанной таблицы
							$related = new self($this->fields[$field_name]['table_related']['name']);
							$related_data = $related->getData(
								['ID', $condition['related_field']],
								[
									'main_condition' => [
										'field_name' => $condition['related_field'],
										'operator' => $condition['operator'],
										'value' => $condition['value']
									]
								]
							);

							// если в связанной таблице ничего не нашли, то созадем фиктивное условие
							if (count($related->data) === 0) {
								$table_field_name = '';
								$operator = ' 1=2 ';
								$value = '';
								$add_quotes = 0;
							} else {
								$related_id_array = [];
								foreach ($related_data as $next_related_data) {
									$related_id_array[] = $next_related_data['ID'];
									$condition_array[$field_name]['data'][(int)$next_related_data['ID']] = $next_related_data[$condition['related_field']];
								}
								$related_id_string = '(' . implode(',', $related_id_array) . ')';

								$table_field_name = $this->fields[$field_name]['table_field'];
								$operator = 'IN';
								$value = $related_id_string;
								$add_quotes = 0;
							}
						} else {
							$table_field_name = $this->fields[$field_name]['table_field'];
							$value = $condition['value'];
							$add_quotes = $condition['add_quotes'];
							isset($condition['operator']) ? $operator = $condition['operator'] : $operator = '=';
						}

						if ($add_quotes === 1) {
							$db_condition_array[] = $table_field_name . ' ' . $operator . ' \'' . $value . '\'';
						} else {
							$db_condition_array[] = $table_field_name . ' ' . $operator . ' ' . $value;
						}
					}
				} else {
					$field_name = $cond_name;
					if (isset($this->fields[$field_name])) {
						$table_field_name = $this->fields[$field_name]['table_field'];
						$operator = '=';
						$value = $condition;

						$db_condition_array[] = $table_field_name . ' ' . $operator . ' ' . $value;
					}
				}
			}


		}

		if($have_service_operands){
			$condition_string = '('.implode(' ', $db_condition_array).')';
		} else {
			if (isset($options['link_operator']) && $options['link_operator']==='or') {
				$condition_string = '('.implode(' or ', $db_condition_array).')';
			} else {
				$condition_string = implode(' and ', $db_condition_array);
			}
		}

		if ($condition_string === '') {
			$condition_string = ' 1=1 ';
		}

		if ($this->only_active_records === true) {
			$condition_string .= ' and status = 0 ';
		}

		// подготовка строки ORDER BY
		$db_order_array = [];

		foreach ($order_array as $order_name => $order) {
			if(is_array($order)){
				$field_name = $order['field_name'];
				$direction = $order['direction'];
			} else {
				$field_name = $order;
				$direction = "ASC";
			}

			if (isset($this->fields[$field_name])) {
				$table_field_name = $this->fields[$field_name]['table_field'];
			}

			if (array_search($field_name, array_column($fields_array, 'alias'), true) !== false) {
				$table_field_name = $field_name;
			}

			$db_order_array[] = $table_field_name.' '.$direction;
		}

		$order_string = implode(",", $db_order_array);
		if ($order_string !== '') {
			$order_string = ' ORDER BY '.$order_string;
		}

		// получаем данные из БД по запросу, превращаем их в удобоваримый вид
		if ($field_string !== '' && $condition_string !== '') {
			// $result = data_select_field($this->tableId, $field_string, $condition_string, $order_string);
			$sql_query = "SELECT ".$field_string." FROM ".DATA_TABLE.$this->tableId." WHERE ".$condition_string.$group_string.$order_string;

			if (isset($options['debug']) && $options['debug'] === 1) {
				$this->sql_query = $sql_query;
			}
			$result = sql_query($sql_query);

			$this->processSqlResult($result, $fields_array, $condition_array, $options);
		}

		return $this->data;
	}

	/**
	 *
	 * Функция для подготовки запроса для БД - переводит человеко-читабельные имена полей и таблиц в служебные
	 *
	 * SELECT AVG([[Контракты]]<<НМЦК>>), [[Контракты]]<<Месяц победы>>
	 * FROM [[Контракты]] as cont
	 * LEFT JOIN [[Счета]] as inv ON [[Контракты]]<<ID>> = [[Счета]]<<Контракт>>
	 * WHERE [[Контракты]]<<Менеджер РТ>> = 'Рожков Никита'
	 *
	 * @param $query
	 */
	public function prepareSqlQuery($query)
	{

	}

	/**
	 * Функция для обработки SQL результата, заменяет служебные имена полей Клиентской базы на читабельные
	 * Также заполняет массив data,  переменную num_rows
	 *
	 * @param $result
	 * @param array $fields_array
	 * @param array $condition_array
	 * @param array $options
	 * @return array
	 */

	public function processSqlResult($result, $fields_array = [], $condition_array = [], $options = [])
	{
		$this->data = [];

		/*
		if ($fields_array === []) {
			// добавляем все поля из коллекции, хотя это не верно!
			foreach ($this->fields as $field_name => $field_data) {
				$fields_array[] = $field_name;
			}
		}
		*/

		$i = 0;

		while ($row = sql_fetch_assoc($result)) {
			// заполняем массив столбцов
			if($fields_array === []){
				foreach($row as $field_name => $field_value){
					$fields_array[] = $field_name;
				}
			}

			$next_row = [];

			/*
			// заполняем значение из аггрегтных полей
			foreach ($fields_array as $field) {
				if (is_array($field)) {
					if (isset($field['alias'])) {
						$row_field_name = $field['alias'];
						$row_field_value = $row[$row_field_name];
						$next_row[$row_field_name] = $row_field_value;
					}
				}
			}
			*/

			// заполняем значения по полям
			// foreach ($fields_array as $field_name) {
			foreach ($fields_array as $field_name) {
				if (!is_array($field_name) && isset($this->fields[$field_name])) {
					// полная подставновка данных о связанном поле
					if (isset($options['open_related']) && is_array($options['open_related']) && in_array($field_name, $options['open_related'])) {
						if ($this->fields[$field_name]['type'] === self::getTypeIdByTypeName('Связь')) {
							$related = new self($this->fields[$field_name]['table_related']['name']);
							$related_data = $related->getData(
								[],
								[
									'main_condition' => [
										'field_name' => 'ID',
										'operator' => '=',
										'value' => $row[$this->fields[$field_name]['table_field']],
									]
								]
							);
							$next_row[$field_name] = $related_data[0];
						} else {
							$next_row[$field_name] = $row[$this->fields[$field_name]['table_field']];
						}
					} else {
						// заменяем обратно на человеко-читаемые имена связанных полей
						if (isset($options['replace_related_data']) && $options['replace_related_data'] === 1) {
							if ($this->fields[$field_name]['type'] === self::getTypeIdByTypeName('Связь')) {

								if (isset($condition_array[$field_name]['data'][$row[$this->fields[$field_name]['table_field']]])) {
									$next_row[$field_name] = $condition_array[$field_name]['data'][$row[$this->fields[$field_name]['table_field']]];
								} else {
									$related = new self($this->fields[$field_name]['table_related']['name']);
									$related_data = $related->getData(
										[$this->fields[$field_name]['table_field_related']['name']],
										[
											'main_condition' => [
												'field_name' => 'ID',
												'operator' => '=',
												'value' => $row[$this->fields[$field_name]['table_field']],
											]
										]
									);

									$next_row[$field_name] = $related_data[0][$this->fields[$field_name]['table_field_related']['name']];
								}
							} elseif (self::getTypeNameByTypeId($this->fields[$field_name]['type'] ) === 'Пользователь') {
								$next_row[$field_name] = self::getUserNameById($row[$this->fields[$field_name]['table_field']]);
							} else {
								$next_row[$field_name] = $row[$this->fields[$field_name]['table_field']];
							}
						} else {
							$next_row[$field_name] = $row[$this->fields[$field_name]['table_field']];
						}
					}
				} else {
					if (is_array($field_name)) {
						if (isset($field_name['alias'])) {
							$row_field_name = $field_name['alias'];
							$row_field_value = $row[$row_field_name];
							$next_row[$row_field_name] = $row_field_value;
						}
					} else {
						if(isset($this->fields_id[$field_name])){
							$next_row[$this->fields_id[$field_name]['name']] = $row[$field_name];
						}else{
							$next_row[$field_name] = $row[$field_name];
						}
					}

				}
			}

			if(isset($options['by_id']) && $options['by_id']==1 && isset($next_row['ID'])){
				$this->data[$next_row['ID']] = $next_row;
			} else {
				$this->data[] = $next_row;
			}

			if (isset($options['rows'])) {
				if ($options['rows'] === $i) {
					break;
				}
			}

			$i++;
		}

		$this->num_rows = $i;
		return $this->data;
	}

	/**
	 * @param $id
	 * @param array $fields_array
	 */
	public function getDataById($id, $fields = [], $options = [])
	{
		if (is_int($id) && $id > 0) {
			if(is_array($fields)){
				$this->getData($fields, ['ID'=>$id], [], $options);
				if(isset($this->data[0])){
					return $this->data[0];
				}
			} elseif ( is_string($fields) && trim($fields)!=='') {
				$this->getData([$fields], ['ID'=>$id], [], $options);
				if(isset($this->data[0][$fields])){
					return $this->data[0][$fields];
				}
			}
		}
		return -1;
	}

	/**
	 * @param array $condition_array
	 * @return mixed
	 */
	public function getIdByCondition($condition_array = [])
	{
		$this->getData(['ID'], $condition_array);
		if ( $this->num_rows > 0 ){
			return $this->data[0]['ID'];
		}
	}

	/**
	 * Обновление конкретной строки данных по ID
	 * @param $id integer
	 * @param $values array
	 */
	public function updateDataById ($id, $values, $do_save = true)
	{
		$id = (int)$id;
		$result = 0;

		if ($this->num_rows > 0){
			// если уже есть данные из БД, то ищем соответствующий ряд
			foreach ($this->data as $index=>&$item) {
				if ((int)$item['ID'] === $id) {
					foreach ($values as $key=>$value) {
						if ($key === 'ID') {
							continue;
						}
						$item[$key] = $value;
						$result = 1;
					}
					break;
				}
			}
		}

		if ((int)$this->num_rows === 0 || $result === 0 ) {
			// если еще нет данных из БД, то заполняем ряд
			$item = [];
			$have_id_field = false;
			foreach ($values as $key=>$value) {
				if ($key === 'ID') {
					$have_id_field = true;
				}
				$item[$key] = $value;
				$result = 1;
			}
			if (!$have_id_field) {
				$item['ID'] = $id;
			}

			$this->data[] = $item;
		}

		if ($result === 1 && $do_save) {
			return $this->save($id);
		}

		return $result;
	}

	/*
	 * Добавляет или обновляет записи в БД
	 */
	public function save($id_to_update = 0, $events_enable = TRUE, $options = [])
	{
		$result = -1;

		if ($id_to_update>0) {
			foreach ($this->data as $index => $next_data) {
				if ((int)$next_data['ID'] === $id_to_update) {
					$next_data_row = [];
					$next_condition = '';
					foreach ($next_data as $field_name => $field_value) {

						if ($field_name === 'ID' && (int)$field_value > 0) {
							$next_condition = ' id = ' . $field_value;
						}
						// проверка что поле существа
						if (array_key_exists($field_name, $this->fields) && $field_name!=='ID') {
							$value = $field_value;

							// получаем id пользователя
							if (self::getTypeNameByTypeId($this->fields[$field_name]['type']) === 'Пользователь') {
								if (isset($options['user_by_name']) && $options['user_by_name']===1) {
									$value = self::getUserIdByName($field_value);
								}
							}
							$next_data_row[$this->fields[$field_name]['table_field']] = $value;
						}
					}
					if ($next_condition !== '') {
						if( $events_enable ) {
							$result = data_update($this->tableId, EVENTS_ENABLE, $next_data_row, $next_condition);
						} else {
							$result = data_update($this->tableId, $next_data_row, $next_condition);
						}
					}
					break;
				}
			}
		} else {
			// insert or update

			if (isset($this->data)) {

				foreach ($this->data as $index => $next_data) {
					$next_data_row = [];
					$next_condition = '';
					foreach ($next_data as $field_name => $field_value) {

						if ($field_name === 'ID' && (int)$field_value > 0) {
							$next_condition = ' id = ' . $field_value;
						}
						// проверка что поле существа
						if (array_key_exists($field_name, $this->fields)) {

							$value = $field_value;

							// получаем id пользователя
							if (self::getTypeNameByTypeId($this->fields[$field_name]['type']) === 'Пользователь') {
								if (isset($options['user_by_name']) && $options['user_by_name']===1) {
									$value = self::getUserIdByName($field_value);
								}
							}
							$next_data_row[$this->fields[$field_name]['table_field']] = $value;
						}
					}

					if ($next_condition !== '') {
						if( $events_enable ) {
							$result = data_update($this->tableId, EVENTS_ENABLE, $next_data_row, $next_condition);
						} else {
							$result = data_update($this->tableId, $next_data_row, $next_condition);
						}
					} else {

						if( $events_enable ) {
							$result = data_insert($this->tableId, EVENTS_ENABLE, $next_data_row);
						} else {
							$result = data_insert($this->tableId, $next_data_row);
						}
					}
				}
			}
		}
		return $result;
	}

	/**
	 * Поиск данных в таблице
	 *
	 */
	public function searchInData($search, $return_id = false)
	{
		$result = -1;
		if($this->num_rows > 0){
			foreach ($this->data as $index=>$row) {
				$found_match = false;
				foreach ($search as $key=>$value) {
					if(!isset($row[$key]) || $row[$key] !== $value) {
						$found_match = false;
						break;
					} else {
						$found_match = true;
					}
				}
				if ($found_match) {
					$return_id ? $result = $row['ID'] : $result = $index;
					break;
				}
			}
		}
		return $result;
	}

	/**
	 *
	 */
	public function delete($events_enable = TRUE)
	{
		foreach($this->data as $key=>$record){
			$record_id = (int)$record['ID'];
			if($record_id > 0){
				if($events_enable) {
					data_delete($this->tableId, EVENTS_ENABLE, "id=$record_id");
				}else{
					data_delete($this->tableId,  "id=$record_id");
				}
			}
		}
	}

	public function deleteSoft()
	{
		foreach($this->data as $key=>$record){
			$record_id = (int)$record['ID'];
			if($record_id > 0){
				data_update($this->tableId, ['status'=>2], "id=$record_id");
			}
		}
	}

}