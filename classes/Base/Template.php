<?php


namespace classes\Base;


class Template
{
	public $param;
	public $template;

	public function __construct($tmp = '') {
		$this->template = file_get_contents($tmp);
	}
	public function __clone()
	{
		$this->param = null;
	}

	public function set($tmp) {
		$this->template = file_get_contents($tmp);
		return $this;
	}

	/**
	 * @param      $key
	 * @param      $param
	 * @param bool $hide_is_null
	 *
	 * @return Template
	 */
	public function setParam($key,$param, $hide_is_null = false) {
		$this->param->$key = $param;
		if ((is_null($param) || $param == '') && $hide_is_null)
		{
			$new_key = $key .'_style';
			$this->param->$new_key= 'style = "display:none"';
		}

		return $this;
	}

	/**
	 * @param $needle
	 * @param $replacement
	 *
	 * @return Template
	 */
	public function replace($needle, $replacement) {
		$this->template = str_replace($needle, $replacement, $this->template);

		return $this;
	}

	/**
	 * @return string
	 */
	public function getPage() {
		foreach ($this->param as $element => $value) {
			$this->template = str_replace('{'.strtoupper($element).'}', $value, $this->template);
		}

		self::Clear();

		return $this->template;
	}

	public function Clear() {
		$this->template = preg_replace('/{+[A-Z0-9_]+}{1}/iD', '', $this->template);
	}
}