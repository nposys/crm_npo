<?php


namespace classes\Base;


class MSDB extends \PDO
{
    public function __construct($options = [])
    {
        include __DIR__ . '/../../db.php';
        $dsn = "sqlsrv:Database=".$databases['MSSQL']['db'].";server=".$databases['MSSQL']['server'].";ConnectionPooling=0";
        parent::__construct($dsn, $databases['MSSQL']['user'], $databases['MSSQL']['pass'], $options);
    }
}