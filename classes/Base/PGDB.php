<?php


namespace classes\Base;


class PGDB extends \PDO
{
	public function __construct($options = [])
	{
		include __DIR__ . '/../../db.php';
		$dsn = "pgsql:dbname=".$databases['PG']['db'].";host=".$databases['PG']['server'];
		parent::__construct($dsn, $databases['PG']['user'], $databases['PG']['pass'], $options);
	}
}