<?php


namespace classes\Base;



class Controller extends Common
{
	public $action_name;
	public $data;

	/**
	 * Controller constructor.
	 */
	public function __construct($default_action = '')
	{
		$this->action_name = $default_action;
		$this->data = [];

		foreach (array_merge($_GET,$_POST) as $var_name => $var_value) {
			switch ($var_name) {
				case 'csrf':
					break;
				case 'form_name':
					$this->action_name = $var_value;
					break;
				default:
					$this->data[$var_name] = $var_value;
					break;
			}
		}
	}

	/**
	 * @return string
	 */
	public function run()
	{
		$method = $this->action_name;
		if ($method !== '' && method_exists($this, $method) &&  is_callable(array($this, $method))) {
			return $this->$method();
		} else {
			return 'Controller action not found';
		}
	}

}