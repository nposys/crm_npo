<?php
/**
 * Created by PhpStorm.
 * User: Alt
 * Date: 03.09.2018
 * Time: 12:22
 */

namespace classes\Helpers;


class Dates
{
    public static function isWorkingDay($day)
    {
        $week_day = (int)date('w', strtotime($day));
        $special_day = data_select_array (470, "status=0 and f7340='".$day." 00:00:00'");

        if($special_day['f7360'] !== 'Нет' && (($week_day>0 && $week_day<6) || ($special_day['f7360']=='Да'))){
            return true;
        }
        return false;
    }
}