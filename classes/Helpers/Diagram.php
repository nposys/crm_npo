<?php
/**
 * Created by PhpStorm.
 * User: Alt
 * Date: 23.06.2019
 * Time: 11:54
 */

namespace classes\Helpers;


class Diagram
{

    // Функция построения графиков типа Bars, Pie, Line
    // параметры:
    // type_graph : Bars, Pie, Line1, LineDate
    // div_id : id div тега, в котором будет размещен график
    // title : текст, заголовок графика
    // title_fontSize : число. Размер шрифта заголовка в pt, например 16
    // title_fontFamily : текст. Название шрифта заголовка , например Arial
    // x_label, y_label : текст. Надпись по соответствующей оси для Line1
    // x_fontSize, y_fontSize : число. Размер шрифта подписи в pt, по соответствующей оси для Line1
    // x_fontFamily, y_fontFamily : текст. Название шрифта подписи по соответствующей оси для Line1
    // series_names : текст. Название серий данных
    // min и max : текст или число. Для задания границ графика
    // zoom : true/false
    static function draw_graph($data, $settings)
    {
        $div_id = $settings['div_id'];
        $user_colors = $settings['user_colors'];
        $return_str = '';
        $title = '';
        if ($settings['title']) {
            $title = "title: {
        text:'" . $settings['title'] . "'";
            if ($settings['title_fontSize']) {
                $title .= ",\n        fontSize: '" . $settings['title_fontSize'] . "pt'";
            }
            if ($settings['title_fontFamily']) {
                $title .= ",\n        fontFamily: '" . $settings['title_fontFamily'] . "'";
            }
            $title .= "\n      },";
        }
        $rand_g = rand(0, 99999);
        $return_str .= "<script>
  function draw_graph$rand_g(){\n";
        $return_str .= "    $.jqplot.config.enablePlugins = true;\n";
        $return_str .= "     $.jqplot.sprintf.thousandsSeparator = ' ';\n";
        $return_str .= "     $.jqplot.sprintf.decimalMark = ',';\n";
        $quote = '"';

        switch ($settings['type_graph']) {

            case 'Bars': // ----------------------------  Bars
                // ищем самую длинную серию данных
                $num_series = count($data); // кол-во серий данных
                $max_len_ser = 0;
                for ($i = 0; $i < $num_series; $i++) {
                    if (count($data[$i]) > $max_len_ser) {
                        $max_len_ser = count($data[$i]);
                        $num_max_ser = $i;
                    }
                }
                // формируем метки
                $tiks = array();
                $tiks_str = "    var ticks = [";
                foreach ($data[$num_max_ser] as $t => $s) {
                    $tiks[] = $t;
                    $tiks_str .= "'" . $t . "', ";
                }
                $return_str .= substr($tiks_str, 0, -2) . "];\n";

                // Формируем серии с проверкой пропущенных значений
                $ticks_y_check = 1;
                $series_list = '';
                for ($i = 0; $i < $num_series; $i++) {
                    $series_list .= 's' . ($i + 1) . ', ';
                    $series = "";
                    for ($j = 0; $j < $max_len_ser; $j++) {
                        $element = each($data[$i]);
                        if ($element) {
                            if ($tiks[$j] == $element['key']) {
                                if ($element['value'] != 0.1) {
                                    $ticks_y_check = 0;
                                }
                                $series .= $element['value'] . ', ';
                            } else {
                                $series .= '0, ';
                                prev($data[$i]);
                            }
                        } else {
                            $series .= '0, ';
                        }
                    }
                    $return_str .= "    var s" . ($i + 1) . " = [" . substr($series, 0, -2) . "];\n";
                }
                if ($ticks_y_check) {
                    $ticks_y = "ticks: [0,1,2,3,4]";
                } else {
                    $ticks_y = "";
                }
                $series_list = substr($series_list, 0, -2);
                // строим график
                $return_str .= "    plot_bars = $.jqplot('" . $div_id . "', [" . $series_list . "], {
      " . $title . "
      
      seriesDefaults: {
        shadow:false,
        renderer:$.jqplot.BarRenderer,
            rendererOptions: {
                fillToZero: true,
                useNegativeColors: false,
                barMargin : 22,
            },
        pointLabels: { show: true }
      },
      grid:{
        shadow:false,
        borderWidth: 0,
        background: '#fff',
      },
      axesDefaults:{
        tickOptions:{
            showMark:false,
            fontSize:'12px',
            fontFamily:'Arial',
          },
      },
      axes: {
        xaxis: {
          renderer: $.jqplot.CategoryAxisRenderer,
          ticks: ticks,
        },
        yaxis: {
          showMark:false,
          tickOptions:{
            formatString: ".$quote."%'.0f".$quote.",
            min : 0,
            rendererOptions: { forceTickAt0: true, forceTickAt100: true }
          },
          $ticks_y
        }
      }
    });\n";
                /*
                 *
                 *         yaxis: {
              showMark:false,
              tickOptions:{
                formatString: ".$quote."%'.0f".$quote.",
                min : 0
              },
                 */

                break; // ---------------------------- end Bars

            case 'Pie': // ----------------------------  Pie
                echo "<style>.jqplot-data-label {color:white}</style>";

                $colors = array(
                    "'#3366cc'",
                    "'#dc3912'",
                    "'#ff9900'",
                    "'#109618'",
                    "'#990099'",
                    "'#4bb2c5'",
                    "'#c5b47f'",
                    "'#71c49b'",
                    "'#579575'",
                    "'#839557'",
                    "'#958c12'",
                    "'#953579'",
                    "'#4b5de4'",
                    "'#d8b83f'",
                    "'#ff5800'",
                    "'#0085cc'"
                );
                $colors = implode(",", $colors);

                $pie_sum = array_sum($data);
                foreach ($data as $k => $v) {
                    if (round(100 * $v / $pie_sum) >= 1) {
                        if ($user_colors[$k]) {
                            $n_colors["$v"] = "'" . $user_colors[$k] . "'";
                        } else {
                            $n_colors["$v"] = "'#" . substr(md5($k), 0, 6) . "'";
                        }
                    }
                }
                krsort($n_colors);
                if (count($n_colors) > 0) {
                    $push_colors = "'#" . substr(md5('Остальное'), 0, 6) . "'";
                    $colors = implode(",", $n_colors);
                }
                arsort($data);

                $return_str .= "    if ($('#" . $div_id . "').height() < $('#" . $div_id . "').width() && $('#" . $div_id . "').height() > 0)
                          {
                              var h_rows = $('#" . $div_id . "').height()/18/2;
                          }
                          else
                          {
                              var h_rows = $('#" . $div_id . "').width()/19/3;
                          }
                          var g_padding = $('#" . $div_id . "').width()/5;
                          var b_padding = -50;
                          var r_padding = g_padding/2;
                          var l_padding = g_padding/2;
                          var t_padding = 20;
                      \n";

                $series = '';
                $common_percent = 0;
                $common_count = 0;
                $common_var = 0;
                foreach ($data as $t => $s) {
                    $pie_percent = round(100 * $s / $pie_sum);
                    if ($pie_percent < 1) {
                        $common_count++;
                        $common_percent += (100 * $s / $pie_sum);
                        $common_var += $s;
                    } else {
                        $pie_labels[] = "'" . $t . " (" . $pie_percent . " %)'";
                        $series .= "['" . $t . " ($pie_percent %)', " . $s . "], ";
                    }
                }
                if ($common_count > 0) {
                    $push_labels = "'Остальное (" . round($common_percent) . " %)'";
                    $push = "['Остальное (" . round($common_percent) . " %)', " . round($common_var) . "]";
                }

                $return_str .= "    var data = [" . substr($series, 0, -2) . "];\n";
                $return_str .= "    data.splice((Number(h_rows)-1));\n";
                $return_str .= "    data.push($push);\n";

                $return_str .= "    var pie_labels = [" . implode(",", $pie_labels) . "];\n";
                $return_str .= "    pie_labels.splice((Number(h_rows)-1));\n";
                $return_str .= "    pie_labels.push($push_labels);\n";

                $return_str .= "    var colors = [" . $colors . "];\n";
                $return_str .= "    colors.splice((Number(h_rows)-1));\n";
                $return_str .= "    colors.push($push_colors);\n";

                $return_str .= "    var plot_pie = jQuery.jqplot ('" . $div_id . "', [data], {
      " . $title . "
      grid: {
        shadow: false,
        borderWidth: 0,
        background: '#fff',
      },
      gridPadding: {left: l_padding, right: r_padding, bottom: b_padding, top: t_padding},
      seriesDefaults: {
        shadow:false,
        renderer: jQuery.jqplot.PieRenderer,
        rendererOptions: {
          showDataLabels: true,
          sliceMargin: 3,
          padding: 0,
        },
        seriesColors: colors
      },
      legend: {
        renderer: $.jqplot.EnhancedLegendRenderer,
        rendererOptions: {
          numberRows: h_rows,
          numberColumns: 1
        },
        show:true,
        location:'e',
        labels: pie_labels
      }
    });\n";

                break; // ---------------------------- end Pie

            case 'Line1': // ----------------------------  Line1
                // Формируем серии
                $num_series = count($data); // кол-во серий данных
                $series_list = '';
                for ($i = 0; $i < $num_series; $i++) {
                    $series_list .= 's' . ($i + 1) . ', ';
                    $series = "";
                    foreach ($data[$i] as $k => $v) {
                        $series .= "[" . $k . "," . $v . "], ";
                    }
                    $return_str .= "    var s" . ($i + 1) . " = [" . substr($series, 0, -2) . "];\n";
                }
                $series_list = substr($series_list, 0, -2);

                $return_str .= "    var plot_line = $.jqplot('" . $div_id . "', [" . $series_list . "], {
      " . $title . "
      legend: {show:true, location: 'se'},
      axes:{
        xaxis:{
          tickRenderer:$.jqplot.CanvasAxisTickRenderer,
          label:'" . $settings['x_label'] . "',
          labelOptions:{
            fontFamily:'" . $settings['x_fontFamily'] . "',
            fontSize: '" . $settings['x_fontSize'] . "pt'
          },
          labelRenderer: $.jqplot.CanvasAxisLabelRenderer
        },
        yaxis:{
          tickRenderer:$.jqplot.CanvasAxisTickRenderer,
          labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
          labelOptions:{
            fontFamily:'" . $settings['y_fontFamily'] . "',
            fontSize: '" . $settings['y_fontSize'] . "pt'
          },
          label:'" . $settings['y_label'] . "'
        }
      }";
                if ($settings['zoom']) { // zoom
                    $return_str .= ",
      cursor:{
        show: true,
        zoom: true
      }\n";
                }
                $return_str .= "    });\n";

                break; // ---------------------------- end Line1

            case 'LineDate': // ----------------------------  LineDate
                if (!isset($settings['markerOptions'])) {
                    $settings['markerOptions'] = array();
                }
                if (!isset($settings['markerOptions']['shadow'])) {
                    $settings['markerOptions']['shadow'] = 'false';
                }
                $markerOptionsLine = "";
                foreach ($settings['markerOptions'] as $k => $v) {
                    $markerOptionsLine .= $k . ":" . $v . ",";
                }
                $markerOptionsLine = substr($markerOptionsLine, 0, -1);
                if (!isset($settings['pointLabels'])) {
                    $settings['pointLabels'] = array();
                }
                if (!isset($settings['pointLabels']['show'])) {
                    $settings['pointLabels']['show'] = 'false';
                }
                $pointLabelsLine = "";
                foreach ($settings['pointLabels'] as $k => $v) {
                    $pointLabelsLine .= $k . ":" . $v . ",";
                }
                $pointLabelsLine = substr($pointLabelsLine, 0, -1);
                //Формирует отметки по y
                if (!isset($settings['correction_values_y'])) {
                    $settings['correction_values_y'] = true;
                }
                if ($settings['correction_values_y']) {
                    $settings['y_max'] = ceil($settings['y_max'] / pow(10,
                                (strlen(abs((int)$settings['y_max'])) - 1))) * pow(10,
                            (strlen(abs((int)$settings['y_max'])) - 1));
                    $settings['y_min'] = floor($settings['y_min'] / pow(10,
                                (strlen(abs((int)$settings['y_min'])) - 1))) * pow(10,
                            (strlen(abs((int)$settings['y_min'])) - 1));
                }
                /* Закоментированнны по заданию № 49 703
                if ($settings['y_min'] < 0 && $settings['y_max'] > 0) {
                    $ticks = array();
                    $ymax = $settings['y_max'];
                    $ymin = $settings['y_min'];

                    $prep = $ymax / 3;
                    $prepy = -$ymin / 3;
                    if ($prep > $prepy) {
                        $ticks[0] = 0 - $prep;
                        if ($ticks[0] > $ymin) {
                            $ticks[0] = $ymin;
                            $prep = -$ymin;
                        }
                        $ticks[1] = 0;
                        $ticks[2] = 0 + $prep;
                        for ($i = 3; $ymax > $s; $i++) {
                            $ticks[$i] = $ticks[$i - 1] + $prep;
                            $s = $ticks[$i];
                        }
                    } else {
                        $s = -1;
                        $prep = -$ymin / 3;
                        $ticks[0] = $ymin;

                        for ($i = 1; $s < 0; $i++) {
                            $ticks[$i] = $ticks[$i - 1] + $prep;
                            $s = $ticks[$i];
                        }
                        if (isset($ticks[$i + 1])) {
                            $ticks[$i + 1] = 0;
                        }

                        $ticks[$i + 2] = $ticks[$i + 1] + $prep;
                    }
                    $ticks = implode(',', $ticks);
                }
                */

                // Смещение
                /* Закоментированнны по заданию № 49 703
                if (!$prep) {
                    $prep = $settings['y_max'] / 5 * 1;
                }
                if ($data[0]) {
                    foreach ($data[0] as $k => $v) {
                        $data[0][$k] = $v + $prep * 0.04;
                    }
                }
                if ($data[1]) {
                    foreach ($data[1] as $k => $v) {
                        $data[1][$k] = $v + $prep * 0.01;
                    }
                }
                if ($data[2]) {
                    foreach ($data[2] as $k => $v) {
                        $data[2][$k] = $v + $prep * 0.07;
                    }
                }
                if ($data[3]) {
                    foreach ($data[3] as $k => $v) {
                        $data[3][$k] = $v + $prep * 0.1;
                    }
                }
                if ($data[4]) {
                    foreach ($data[4] as $k => $v) {
                        $data[4][$k] = $v - $prep * 0.2;
                    }
                }
                */

                // Формируем серии
                $sotkl = array();
                $num_series = count($data); // кол-во серий данных
                $series_list = '';
                $nullGraf = 0;
                $nullColor = "'#3366cc'";
                $nullLegend = '';
                for ($i = 0; $i < $num_series; $i++) {
                    $series_list .= 's' . ($i + 1) . ', ';
                    $series = "";
                    foreach ($data[$i] as $k => $v) {
                        $series .= "['" . $k . "'," . $v . "], ";
                        $sotkl[$i][$k] = $v;
                        if ($v != 0) {
                            $nullGraf = 1;
                        }
                    }
                    $return_str .= "    var s" . ($i + 1) . " = [" . substr($series, 0, -2) . "];\n";
                }
                $series_list = substr($series_list, 0, -2);

                if (!$nullGraf) {
                    $nullColor = "(('\v'=='v')?'white':'transparent')";
                    $nullLegend = 'showLabels:false,showSwatches:false,';
                }
                $return_str .= "     var plot_line_date = $.jqplot('" . $div_id . "', [" . $series_list . "], {
      " . $title . "
      legend: {
        " . $nullLegend . "
        renderer: $.jqplot.EnhancedLegendRenderer,";
                if ($settings['series_names']) {
                    $return_str .= "\n        labels: [ " . $settings['series_names'] . " ],";
                }
                $return_str .= "\n        show:true,
        location: 's',
        placement: 'outsideGrid',
        fontFamily:'Arial',
        fontSize:'20px',
        textColor:'#666',
        rendererOptions: {
          numberRows: 1,
          numberColumns: 10,
          seriesToggle: false,
          disableIEFading: true
        }
      },
      seriesDefaults: {
        shadow: false,
        shadowAngle: 0,
        shadowOffset:0,
        shadowDepth:0,
        shadowAlpha:0,
        lineWidth: 3.5,
        pointLabels: { $pointLabelsLine },
        markerOptions:{ $markerOptionsLine }";
                $return_str .= "},
      seriesColors: [" . $nullColor . ", '#dc3912', '#ff9900', '#109618', '#990099',
                      '#4bb2c5', '#c5b47f', '#EAA228', '#579575', '#839557', '#958c12',
                        '#953579', '#4b5de4', '#d8b83f', '#ff5800', '#0085cc'],
      grid: {
        shadow: false,
        backgroundColor: (('\\v'=='v')?'white':'transparent'),
        drawBorder:false
      },
      axes: {
        xaxis: {";
                if ($settings['x_max']) {
                    $return_str .= "\n          max: '" . $settings['x_max'] . "',";
                }
                if ($settings['x_min']) {
                    $return_str .= "\n          min: '" . $settings['x_min'] . "',";
                }
                if ($settings['view_time']) {
                    $time_format = "<br />%H:%M";
                }
                if ($settings['view_sec']) {
                    $sec_format = ":%S";
                }
                $return_str .= "\n          renderer: $.jqplot.DateAxisRenderer,
          tickOptions: {
            formatString: '%d.%m.%y" . $time_format . $sec_format . "',
            showMark:false,
            fontSize:'12px',
            fontFamily:'Arial'
          }";
                if ($settings['ticks_x']) {
                    $return_str .= ",ticks:[" . $settings['ticks_x'] . "]";
                }
                if ($settings['ticks_x_interval']) {
                    $return_str .= ",tickInterval:'" . $settings['ticks_x_interval'] . "'";
                }
                $return_str .= "},
        yaxis: {";
                if ($settings['y_max']) {
                    $return_str .= "\n          max: " . $settings['y_max'] . ",";
                }
                if ($settings['y_min']) {
                    $return_str .= "\n          min: " . $settings['y_min'];
                } else {
                    $return_str .= "\n          min:0";
                }
                $return_str .= ",
            tickOptions:{
              formatString: '%.0f',
              fontSize:'12px',
              fontFamily:'Arial',
              showMark:false
            },
            tickRenderer: $.jqplot.CanvasAxisTickRenderer,
            labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
            labelOptions:{
              formatString: '%.0f',
              fontSize:'12px',
              fontFamily:'Arial',
              showMark:false
        }}}";
                if ($settings['zoom']) { // zoom
                    $return_str .= ",
      cursor:{
        show: true,
        zoom: true
      }\n";
                }
                $return_str .= "    });\n";

                break; // ---------------------------- end LineDate

        } // end switch

        $return_str .= "  }; setTimeout(draw_graph$rand_g, 1000);
</script>\n";
        return $return_str;
    }
}