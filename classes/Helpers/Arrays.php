<?php
/**
 * Created by PhpStorm.
 * User: Alt
 * Date: 22.06.2019
 * Time: 10:19
 */

namespace classes\Helpers;


class Arrays
{
    static function unique_multidim_array($array, $key) {
        $key_array = [];

        foreach($array as $val) {
            if (!in_array($val[$key], $key_array)) {
                $key_array[] = $val[$key];
            }
        }
        return $key_array;
    }

    /*
     * $month = 1...12
     */
    static function getRusMonth($month, $big_letter = false)
    {
        if($month<1 or $month>12 or !is_int($month)) {
            return '';
        }

        $names = [
            'small_letter' =>
                ['январь',
                'февраль',
                'март',
                'апрель',
                'май',
                'июнь',
                'июль',
                'август',
                'сентябрь',
                'октябрь',
                'ноябрь',
                'декабрь'],
            'big_letter' =>
                ['Январь',
                    'Февраль',
                    'Март',
                    'Апрель',
                    'Май',
                    'Июнь',
                    'Июль',
                    'Август',
                    'Сентябрь',
                    'Октябрь',
                    'Ноябрь',
                    'Декабрь'],
        ];

        if($big_letter){
            return $names['big_letter'][$month-1];
        }
        return $names['small_letter'][$month-1];
    }
}