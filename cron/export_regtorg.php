<?php
require_once 'add/db.php';
require_once 'add/tables.php';
require_once 'add/functions/service_functions.php';

// =============================================================================================
// 2. Переводим в MS SQL статус заказа в "17. Заявка на доставке" все заказы, которые перевели
// в статус производства "50. На подаче" в CRM.

$tableOrders = 271;
$tableOrdersApp = 381;
$tableTenders = 331;

$debug = false;

$serverName = '192.168.0.10';
$dbName = 'tenderdb';
$connectionOptions = array(
    "Database" => $dbName,
    "Uid" => "tenders",
    "PWD" => "gdlNtylthc 98",
    'ReturnDatesAsStrings'=>true,
 );

$numUpdated = 0;
ini_set('mssql.charset','UTF8');
$sqlLink = sqlsrv_connect($serverName, $connectionOptions);

// $sqlLink = sqlsrv_connect ($serverName , 'tenders', 'gdlNtylthc 98');
if ($sqlLink)
{
    // $db = sqlsrv_select_db("tenderdb", $sqlLink);
    if (true)
    {
        // выбираем в CRM заказы из таблицы Заявка с клиентом Регионторг и статусом "на подаче"
        $i = 1;
        $resOrderApp = data_select($tableOrdersApp, "status=0 and f7731=3813 and f7701 IN ('10. Исполнение') and f6561='50. На подаче'" );
        while ($rowOrderApp = sql_fetch_assoc($resOrderApp))
        {
            // выбираем тендеры в статусе на подготовке заявки
            $query = "SELECT [Status] FROM [ClientsLots] WHERE [ID] = ".$rowOrderApp['f10300'];
            $resultQuery = sqlsrv_query($sqlLink, $query);

            if($resultQuery)
            {
                $numRows = sqlsrv_num_rows($resultQuery);
                while($row = sqlsrv_fetch_array($resultQuery))
                {
                    if(intval($row["Status"])==9)
                    {
                        // ID  StatusName 19  17. Заявка на доставке
                        $queryUpdate = "UPDATE [ClientsLots] SET [Status] = 19 WHERE [ID] = ".$rowOrderApp['f10300'];
                        $resultUpdateQuery = sqlsrv_query($sqlLink, $queryUpdate);
                        $numUpdated++;
                    }

                } // end while ms sql fetch array

            } // end if result query

            sqlsrv_free_stmt( $resultQuery);
            $i++;
        } // end while следующая заявка в CRM

    } // end if select db

    //close the connection
    sqlsrv_close($sqlLink);

} // end if SQL LINK

write_log("num processed:$i");
write_log("num updated:$numUpdated");
?>