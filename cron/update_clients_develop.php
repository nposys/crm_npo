<?php 

require_once 'add/db.php';
require_once 'add/tables.php';
require_once 'add/functions/common_clients_funcitons.php';

// обновление статистики участий клиентов из MS SQL
$debug = екгу;
if($debug) echo "start<br>\r\n";

$last_region = 0;
$rowServiceJob = data_select_array($cb_tables['tableServiceJobs'],"status=0 and f14871='update_clients_develop'");
if($rowServiceJob['id']<>"") $last_region = $rowServiceJob['f14881'];
if($last_region==99) $last_region = 0;
$sub_region = $last_region+1;
    
$dt_start = '2015-04-01';
$sub_region_str = sprintf("%'.02d", $sub_region);
$region = "54025%";
if($debug) echo "region:".$region."\r\n";

ini_set('max_execution_time', 900);
ini_set('mssql.charset','UTF8');
ini_set('mssql.connect_timeout','120');
ini_set('mssql.timeout','600');

$sqlConnectId = mssql_connect ($databases["MSSQL"]["server"] , $databases["MSSQL"]["user"] , $databases["MSSQL"]["pass"] );

if ($sqlConnectId)
{
    if($debug) echo "sql connected<br>\r\n";    
    $db = mssql_select_db($databases["MSSQL"]["db"], $sqlConnectId);
    if ($db)
    {
        if($debug) echo "db selected<br>\r\n";    
        // выбираем всех поставщиков за заданный период по заданному региону
        $query = "
        SELECT prot.SupplierINN as INN,prot.SupplierKPP as KPP
        FROM prot_Results as prot LEFT JOIN tenders as t on t.GovRuID = prot.GovRuId LEFT JOIN Suppliers as sup on sup.ID = prot.SupplierID
        WHERE t.CreateDate > CAST('".$dt_start."' as datetime2) and prot.SupplierINN like '".$region."' and (prot.SupplierKPP = '' or prot.SupplierKPP like '".$region."')
        GROUP BY  prot.SupplierINN, prot.SupplierKPP
          UNION
        SELECT prot.INN as INN,prot.KPP as KPP
        FROM ProtocolsEF as prot LEFT JOIN tenders as t on t.GovRuID = prot.GovRuId LEFT JOIN Suppliers as sup on sup.ID = prot.SuppliersID
        WHERE t.CreateDate > CAST('".$dt_start."' as datetime2) and prot.INN like '".$region."' and (prot.KPP = '' or prot.KPP like '".$region."')
        GROUP BY  prot.INN, prot.KPP
          UNION
        SELECT prot.INN as INN,prot.KPP as KPP
        FROM ProtocolsZK as prot LEFT JOIN tenders as t on t.GovRuID = prot.GovRuId LEFT JOIN Suppliers as sup on sup.ID = prot.SuppliersID
        WHERE t.CreateDate > CAST('".$dt_start."' as datetime2) and prot.INN like '".$region."' and (prot.KPP = '' or prot.KPP like '".$region."')
        GROUP BY prot.INN, prot.KPP
            UNION
        SELECT prot.INN as INN,prot.KPP as KPP
        FROM ProtocolsOK as prot LEFT JOIN tenders as t on t.GovRuID = prot.GovRuId LEFT JOIN Suppliers as sup on sup.ID = prot.SuppliersID
        WHERE t.CreateDate > CAST('".$dt_start."' as datetime2) and prot.INN like '".$region."' and (prot.KPP = '' or prot.KPP like '".$region."')
        GROUP BY prot.INN, prot.KPP
            UNION
        SELECT prot.INN as INN,prot.KPP as KPP
        FROM ProtocolsPO as prot LEFT JOIN tenders as t on t.GovRuID = prot.GovRuId LEFT JOIN Suppliers as sup on sup.ID = prot.SuppliersID
        WHERE t.CreateDate > CAST('".$dt_start."' as datetime2) and prot.INN like '".$region."' and (prot.KPP = '' or prot.KPP like '".$region."')
        GROUP BY prot.INN, prot.KPP
        ORDER BY INN
            ";
        //execute the SQL query and return records
        $resultQuery = mssql_query($query, $sqlConnectId);
        if($debug) echo "$query<br>";
        
        $num_rows = mssql_num_rows($resultQuery);
        if($num_rows==0) if($debug) echo "mssql_error:".mysql_error($query)."<br>\r\n";
        if($debug) echo "num_rows:".$num_rows."<br>\r\n";
        $i = 0;
        $j = 0;

        if($resultQuery)
        {
            if($debug) echo "query done<br>\r\n";    
            while($rowSQL = mssql_fetch_array($resultQuery, MSSQL_BOTH))
            {
                $inn = trim($rowSQL['INN']);
                $kpp = trim($rowSQL['KPP']);                
                if($inn<>"") CreateClient($inn, $kpp, $sqlConnectId, $cb_tables, true, '2015-04-01 00:00:00', true);
                if($debug) echo "done with INN $inn<br>\r\n";
            } // end while mssql_fetch
            
            $cur_date = date("Y-m-d H:i:s");
            if($rowServiceJob['id']<>"") 
                data_update($cb_tables['tableServiceJobs'],array("f14881"=>$sub_region,"f14891"=>$cur_date),"id=".$rowServiceJob['id']);
            else
                data_insert($cb_tables['tableServiceJobs'],array("f14871"=>"update_clients_develop", "f14881"=>$sub_region,"f14891"=>$cur_date));
        } // end if resultQuery
    } // end if select_db
    mssql_close($sqlConnectId);
    if($debug) echo "end at $cur_date\r\n";
} // end if sqlLink  
?>