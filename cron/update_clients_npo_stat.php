<?php
/**
 * Created by PhpStorm.
 * User: Alt
 * Date: 16.11.2018
 * Time: 13:27
 */
require_once 'add/db.php';
require_once 'add/tables.php';
require_once 'add/functions/common_clients_functions.php';
require_once 'add/functions/service_functions.php';

// connect to ms sql
$serverName = $databases["MSSQL"]["server"];
$connectionOptions = array(
    "Database" => $databases["MSSQL"]["db"],
    "Uid" => $databases["MSSQL"]["user"],
    "PWD" => $databases["MSSQL"]["pass"],
    //'ReturnDatesAsStrings'=>true,
);
$sqlConnectId = sqlsrv_connect($serverName, $connectionOptions);

$i = 0;
$j = 0;
// получаем список клеинтов
$res = data_select($cb_tables['tableClients'],"status=0 and f772='Клиент'");
while($row = sql_fetch_assoc($res)){
    $client_id = (int)$row['id'];
    if($client_id>0){
        // обновляем статистику работы с НПО
        UpdateWorkWithNPO($client_id, $cb_tables);

        $client_data_row = data_select_array($cb_tables['tableClientsData'], "status=0 and f10410=".$client_id );
        if((int)$client_data_row['id']==0){
            UpdateParticipations($client_id, $sqlConnectId, $cb_tables);
            $j++;
        }
        $i++;

    }
}

write_log( "$j clients without data");
write_log( "updated $i clients");