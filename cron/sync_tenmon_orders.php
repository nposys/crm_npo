<?php

require_once 'add/db.php';
require_once 'add/tables.php';
require_once 'add/functions/service_functions.php';
require_once 'add/functions/tenmon_functions.php';
require_once 'add/functions/common_order_functions.php';

// ---------------------------------------------------------------------
// подключаемся к базе тенмон
// ---------------------------------------------------------------------
$bTMok = false;
$mysqli = mysqli_init();
if (!$mysqli->real_connect(
        $databases['TM']['server'],
        $databases['TM']['user'],
        $databases['TM']['pass'],
        $databases['TM']['database'])) {
    die('Ошибка подключения (' . mysqli_connect_errno() . ') '. mysqli_connect_error());
} else {
    $bTMok = true;
}
$mysqli->query("SET NAMES 'utf8'");

$count_clients_missed = 0;
$count_clients_ok = 0;
$price_positions = '1,2,55,56,62,63,64,65';

// перебираем все действующие заказы на поиск в CRM
$resOrderSearch = data_select($cb_tables['tableOrdersSearch'], "status=0 and f9260='10. Исполнение' and f8550 in ($price_positions)");
while ($rowOrderSearch = sql_fetch_assoc($resOrderSearch)) {
    if ($rowOrderSearch['f5411'] > 0) {
        // TODO: переделать на вызов функции ActivateTenmon

        $rowOrder = data_select_array($cb_tables['tableOrders'], "status=0 and id=" . $rowOrderSearch['f5411']);
        $client_id = $rowOrder['f4441'];
        $order_id = $rowOrder['f7071'];
        $sum_order = $rowOrder['f4461'];
        $sum_paid = $rowOrder['f10270'];

        if ($client_id > 0) {
            $rowClient = data_select_array($cb_tables['tableClients'], "status=0 and id=$client_id");
            $company_group_id = $rowClient['f6301'];
            $client_name = $rowClient['f435'];
            $client_inn = $rowClient['f1056'];

            // готовим данные для активации аккаунта
            $dt_from = date("Y-m-d", strtotime($rowOrderSearch['f6941'])) . " 00:00:00"; // date start
            $dt_to = date("Y-m-d", strtotime($rowOrderSearch['f6951'])) . " 23:59:59"; // date finish
            $dt_now = date("Y-m-d H:i:s");

            $tariff_name = $rowOrderSearch['f16870'];
            $tm_tariff_id = GetTenmonTariffByName($tariff_name); // tenmon tariff

            // готовим набор услуг для включения в тенмон
            $products = array();
            $products['search'] = ConvertYesNoToBool($rowOrderSearch['f16880']); // ручной поиск;
            $products['autoSearch'] = ConvertYesNoToBool($rowOrderSearch['f16890']); // автоматический шаблонный поиск
            $products['expertSearch'] = ConvertYesNoToBool($rowOrderSearch['f16900']); // экспертный поиск
            $products['userCount'] = (int)$rowOrderSearch['f16840']; // кол-во пользователей
            $products['integration'] = ConvertYesNoToBool($rowOrderSearch['f16850']); // интеграция с ЭТП
            $products['notification'] = ConvertYesNoToBool($rowOrderSearch['f16860']); // уведомления
            $products['analytics'] = ConvertYesNoToBool($rowOrderSearch['f18691']); // аналитика
            $products['holding'] = ConvertYesNoToBool($rowOrderSearch['f17190']); // группы компаний
            $products['exportLotInfo'] = ConvertYesNoToBool($rowOrderSearch['f19621']); // Выгрузка закупок в Excel
            $products['exportAnalyticInfo'] = ConvertYesNoToBool($rowOrderSearch['f19631']); // Выгрузка результатов аналитики в Excel
            $products['csvLimit'] = (int)$rowOrderSearch['f19641']; // Лимит строк для выгрузки результатов аналитики в Excel
            $products['analyticIndustryEnable'] = ConvertYesNoToBool($rowOrderSearch['f19651']); // Аналитика по произвольным запросам
            $products['forecastEnable'] = ConvertYesNoToBool($rowOrderSearch['f19661']); // Экспертный прогноз снижения на торгах
            $products['lotStatusNotificationEnable'] = ConvertYesNoToBool($rowOrderSearch['f19671']); // Автоматические уведомления о сменах статуса выбранных тендеров
            $products['riskAnalyseEnable'] = ConvertYesNoToBool($rowOrderSearch['f19681']); // Экспертный анализ рисков по заказчикам

            $bGroupOfComapnies = ConvertYesNoToBool($rowOrderSearch['f17190']); // объединение компаний в группы

            // получаем tenmon_uid из группы компаний (иногда кабинет в ТМ создан не на то юрлицо, что заказало поиск)
            // $tenmon_uid = GetTenmonUIDFromGroupOfComapnies($client_id, $cb_tables);
            $tenmon_uid = $rowClient['f16230'];
            if ($tenmon_uid > 0) {
                // включаем поиск только для тех у кого в заказе подошла дата начала
                if ($dt_now >= $dt_from) {
                    $result = SetTenmonServices($tenmon_uid, $tm_tariff_id, $products, $dt_to);
                    $result_dump = print_r($result, true);
                    write_log("Для $client_name inn:$client_inn order_id:$order_id tm:$tenmon_uid  tariff:$tariff_name  res:$result_dump");

                    $result = SetTenmonTariff($tenmon_uid, $dt_from, $dt_to, $sum_order);
                    $count_clients_ok++;
                }
            } else {
                write_log("___ Отуствует tenmon_uid для $client_name $client_inn ");
                $count_clients_missed++;
            }

            // включаем тариф для всей группы компаний (если у них нет своего заказа)
            $userIds = []; // массив для объединения компаний в группу
            $tenmon_uids = GetTenmonUIDsFromGroupOfComapnies($client_id, $cb_tables);
            foreach ($tenmon_uids as $next_company) {
                $next_client_id = $next_company['client_id'];
                $next_client_name = $next_company['client_name'];
                $next_client_inn = $next_company['client_inn'];
                $tenmon_uid = $next_company['tenmon_uid'];

                $userIds[] = (int)$tenmon_uid; // массив для связи групп компаний (см. ниже метод JoinGroupOfCompanies)
                $row_next_company_search = data_select_array(
                                        $cb_tables['tableOrdersSearch'],
                                        "status=0 
                                        and f9260='10. Исполнение'
                                        and f8550 in ($price_positions) 
                                        and f10190=$next_client_id"
                                );

                if ($row_next_company_search['id'] > 0) {
                    write_log("============= компания :$next_client_id $next_client_name $next_client_inn имеет свой заказ на поиск, пропускаем.");
                } else {
                    // иначе берем параметры от того заказа который сейчас обрабатываем
                    // включаем поиск только для тех у кого в заказе подошла дата начала
                    if ($dt_now >= $dt_from) {
                        $result = SetTenmonServices($tenmon_uid, $tm_tariff_id, $products, $dt_to);
                        $result_dump = print_r($result, true);
                        write_log("============= обновление в группе компаний $next_client_name $next_client_inn next_client:$next_client_id tm_uid:$tenmon_uid tariff:$tariff_name res:$result_dump");

                        $result = SetTenmonTariff($tenmon_uid, $dt_from, $dt_to, $sum_order);
                    }
                }
            }

            // объединяем компаниии в группу компаний
            if ($bGroupOfComapnies && count($userIds) > 1) {
                JoinGroupOfCompanies($userIds);
                $var_uids = print_r($userIds, true);
                //write_log ("++++++++++++++++++ join into group of companies uids: $var_uids");
            }
        } else {
            write_log("Отуствует ссылка на клиента в заказе id:" . $rowOrder['id']);
        }
    } else {
        write_log("Отуствует ссылка на основной заказ в поисковом заказе id:" . $rowOrderSearch['id']);
    }

} // end while current search orders

// =====================================================================================

write_log("no clients in CRM: $count_clients_missed");
write_log("set tariff for: $count_clients_ok");

if ($bTMok) {
    $mysqli->close();
}