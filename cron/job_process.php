<?php

require_once 'add/functions/service_functions.php';

// обработка просроченных тестовых заданий
$stringsToFind = array(
        "Тестовое задание - Менеджер по поставкам 2017-01 - SENDED"
        ,"Тестовое задание - Менеджер по тендерам 2017-01 - SENDED"
        ,"Тестовое задание - Тестировщик 2017-02 - SENDED"
        ,"Тестовое задание - Юрист 2017-01 - SENDED"

        ,"Тест - РТ - Менеджер по поставкам 2018-02 (бытовая техника) - SENDED"
        ,"Тест - РТ - Менеджер по поставкам 2018-02 (строительные материалы) - SENDED"
        ,"Тест - РТ - Помощник менеджера по поставкам 2018-02 (хозбыт) - SENDED"
        ,"Тест - РТ - Юрист 2017-01 - SENDED"
        ,"Тест - РТ - Юрист 2018-01 - SENDED"
        ,"Тест - НПО - Менеджер по тендерам 2018-02 - SENDED"
        ,"Тест - НПО - Менеджер по тендерам 2018-02 (материалы) - SENDED"
        ,"Тест - НПО - Менеджер по тендерам 2018-02 (документы) - SENDED"
);

$numExpired = 0;
$numSent = 0;
$numDone = 0;

foreach($stringsToFind as $stringToFind)  {
    $resCandidatesWithTest = data_select (721, "status=0 and f12111='30. Направлено тестовое задание' and f17640 LIKE '%$stringToFind%'");

    while($rowCandidate = sql_fetch_assoc($resCandidatesWithTest)){
        $candidateId = $rowCandidate['id'];
        $candidateName = $rowCandidate['f12071'];
        $candidateMail = $rowCandidate['f12271'];
        $testStatus = $rowCandidate['f17800'];
        $logMail = $rowCandidate['f17640'];
        $firstEmailId = $rowCandidate['f17490'];

        // TODO: получить дату отправки письма регулярным выражением - чтобы проверять только письма после этой даты
        // write_log("next candidate:$candidateName $candidateMail $testStatus");

        // проставляем пустые статусы
        if($candidateId>0 && $testStatus=="") {
            data_update(721, EVENTS_ENABLE, array("f17800"=>"Направлено"), "id=$candidateId");
            $testStatus = "Направлено";
            $numSent++;
        }

        // ищем решение в почте
        if($candidateMail<>"" && $testStatus=="Направлено") {
            if($firstEmailId>0) $checkNotFirstEmail = " and id<>$firstEmailId ";
            // id<>f17490 - не первичное письмо с резюме
            $rowMail = data_select_array(950, "status=0 and f16600='$candidateMail' $checkNotFirstEmail and (f16620 LIKE '%Re%' or f16620 LIKE '%тест%' or f16620 LIKE '%задание%' or f17430 LIKE '%тест%' or f17430 LIKE '%задание%')");
            $mailId = (int)$rowMail['id'];

            // write_log ("search mail for $candidateMail found id:$mailId");

            if($mailId>0) {
                data_update(721, EVENTS_ENABLE, array("f17800"=>"Решено"), "id=$candidateId");
                $testStatus = "Решено";

                // помечаем письмо как обработанное
                data_update(950, EVENTS_ENABLE, array("f17780"=>$candidateId, "f17440"=>"Да"), "id=$mailId");
                $numDone++;
            } else {
                // вычисляем дату когда должно быть решено
                $pos = strpos($logMail, $stringToFind);
                $len = strlen($stringToFind);
                if($pos>0) {
                    $substr = substr($logMail, $pos+$len+2, 19);
                }


                if($substr<>"") {
                    $outdated = 0;
                    $newdate = AddWorkingDays($substr, 3);
                    $currentDate = date("Y-m-d");
                    $finalDate = date("Y-m-d", strtotime($newdate));
                    if($currentDate>$finalDate) {
                        $outdated = 1;
                    }

                    // write_log ("senddate:$substr finaldate:$finalDate curdate:$currentDate");

                    if($candidateId>0 && $testStatus=="Направлено" && $outdated) {
                        write_log ("name:$candidateName candidateMail:$candidateMail substr:$substr newdate:$newdate outdated:$outdated mailId:[$mailId] testStatus:$testStatus");
                        // проставляем просроченные решения
                        data_update(721, EVENTS_ENABLE, array("f17800"=>"Просрочено"), "id=$candidateId");
                        $numExpired++;
                    }

                }
            } // if($mailId>0)
        } // if($candidateMail<>"" && $testStatus=="Направлено")

    } // while($rowCandidate = sql_fetch_assoc($resCandidatesWithTest))
} // foreach($stringsToFind as $stringToFind)

write_log("Num sended:$numSent");
write_log("Num done:$numDone");
write_log("Num expired:$numExpired");