<?php
/**
 * Created by PhpStorm.
 * User: Alt
 * Date: 29.11.2017
 * Time: 11:10
 */

require_once 'add/db.php';
require_once 'add/tables.php';
require_once 'add/functions/service_functions.php';

autoChangeClientStatus('Потенциальный');
autoChangeClientStatus('Клиент');

function autoChangeClientStatus($type)
{
    global $cb_tables;

     // выбираем всех клиентов
    $res_clients = data_select($cb_tables['tableClients'], "status=0 and f772='$type'");
    while($row_client = sql_fetch_assoc($res_clients)){

        $client_id = $row_client['id'];
        $client_name = $row_client['f435'];
        $client_inn = $row_client['f1056'];
        $have_plan_event = $row_client['f8980'];
        $num_events = (int)$row_client['f6961'];
        $group_id = (int)$row_client['f6301'];
        $status_client = $row_client['f4921'];
        $status_lead = $row_client['f552'];

        if($type === 'Потенциальный' && $status_lead==='Холодный' && $num_events>0) {
            data_update( $cb_tables['tableClients'], ['f552'=>'Начата работа'], "id=$client_id");
        }

        if ($status_client !== 'Недобросовестный'){

            if($group_id>0){
                $clients_filter = " AND f4441 in (SELECT id FROM ".DATA_TABLE.$cb_tables['tableClients']." WHERE  f6301=$group_id) ";
            } else {
                $clients_filter = " AND f4441 = $client_id ";
            }

            $sum_filter = '';
            if ($type === 'Потенциальный') {
                $sum_filter = ' AND f4461 > 0.0 ';
            }

            $sql_query = "SELECT MAX(f6591) as LastDate, id, f6551 as StatusOrder FROM ".DATA_TABLE.$cb_tables['tableOrders']." 
                WHERE 
                    status=0 
                    $clients_filter
                    $sum_filter
                GROUP BY f6551";
            $res_field_current = sql_query($sql_query);

            $orders_dates = [];
            while($row_field_current = sql_fetch_assoc($res_field_current)){

                $order_id = $row_field_current['id'];
                $status_order = $row_field_current['StatusOrder'];
                $last_date = $row_field_current['LastDate'];

                $orders_dates[$status_order] = $last_date;

                if($last_date!=='0000-00-00 00:00:00'){
                    write_log("__$status_order: $order_id last_date:$last_date");
                } else {
                    write_log("__$status_order: ____ NO ORDERS");
                }
            }

            $max_date = max($orders_dates);
            write_log("__max_date: $max_date > ".date('Y-m-d H:i:s', strtotime(" - 3 month")));

            if (isset($orders_dates['10. Исполнение'])
                || isset($orders_dates['20. Предоплата'])
                || isset($orders_dates['25. Ожидание оплаты'])
                || $max_date>date('Y-m-d H:i:s', strtotime(" - 3 month"))
            ) {
                if($type === 'Потенциальный' && isset($orders_dates['10. Исполнение'])) {
                    data_update( $cb_tables['tableClients'],
                        ['f772'=>'Клиент', 'f4921'=>'В работе'],
                        "id=$client_id");
                }

                // возвращать ли из отказа тех кого недавно перевели вручную?
                if ($type === 'Клиент' && $status_client !== 'В работе' && $have_plan_event==='Да') {
                    data_update( $cb_tables['tableClients'], ['f4921'=>'В работе'], "id=$client_id");
                }

                $end_status = " ++++++++ ТЕКУЩИЙ КЛИЕНТ";
            } else {

                if ($type === 'Клиент') {
                    if ($max_date>date('Y-m-d H:i:s', strtotime(" - 12 month"))){
                        if ($status_client !== 'Приостановлен' && $status_client!=='Недобросовестный') {
                            data_update( $cb_tables['tableClients'], ['f4921'=>'Приостановлен'], "id=$client_id");
                        }
                        $end_status = " ????? ПРИОСТАНОВЛЕН КЛИЕНТ";
                    } else {
                        if ($status_client !== 'Отказ' && $status_client!=='Недобросовестный') {
                            data_update($cb_tables['tableClients'], ['f4921' => 'Отказ'], "id=$client_id");
                        }
                        $end_status = " -------- НЕ РАБОЧИЙ КЛИЕНТ";
                    }
                } else {
                    $end_status = " -------- НЕ РАБОЧИЙ КЛИЕНТ";
                }
            }

            write_log("[$client_id] ($type) $client_name ИНН:$client_inn [$status_client] plan_events:$have_plan_event $end_status");
        }

    }
}

// ============================================================
/*
$connection_string = "host=".$databases['PG']['server']
    ." port=5432"
    ." dbname=".$databases['PG']['db']
    ." user=".$databases['PG']['user']
    ." password=".$databases['PG']['pass'];

$pg = pg_connect($connection_string);
if($pg) {

    $pg_select = '
      SELECT 
        * 
      FROM 
        "tLot"
      WHERE  
        "DateCreate" > \'2017-01-01\' 
      LIMIT 10
    ';

    $res = pg_query ($pg, $pg_select);
    while ($row = pg_fetch_assoc($res)) {
        write_log($row['GovRuId']);
    }

    // close connection
    pg_close($pg);
} else {
    write_log('no pg connection');
}
*/
