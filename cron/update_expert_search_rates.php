<?php

require_once 'add/db.php';

global $connectionOptions;
global $serverName;
$serverName = $databases["MSSQL"]["server"];
$connectionOptions = array(
	"Database" => $databases["MSSQL"]["db"],
	"Uid" => $databases["MSSQL"]["user"],
	"PWD" => $databases["MSSQL"]["pass"],
	'ReturnDatesAsStrings' => true,
	"CharacterSet" => "UTF-8"
);

global $mysqli;
$mysqli = mysqli_init();
if (!$mysqli->real_connect($databases['TM']['server'], $databases['TM']['user'], $databases['TM']['pass'], $databases['TM']['db'])) {
	die('Ошибка подключения (' . mysqli_connect_errno() . ') '
		. mysqli_connect_error());
}
$mysqli->query("SET NAMES 'utf8'");

function updateCategoryRate($start_date,$end_date,$regions)
{
	global $connectionOptions;
	global $serverName;
	global $mysqli;

	$query_tm = "SELECT * FROM es_Category";
	$categories=[];
	if($result_tm = $mysqli->query($query_tm)){
		while ($category = $result_tm->fetch_array(MYSQLI_ASSOC)){
			$categories[$category['mssql_srhCategoryId']]=$category['ID'];
		}
		$result_tm->close();
	}
	$query = "EXECUTE GetCountLotInCategory '$start_date','$end_date','$regions'";
	$sqlConnectId = sqlsrv_connect($serverName, $connectionOptions);
	$resultQuery = sqlsrv_query($sqlConnectId, $query);

	$data = [];
	$total=0.0;
	$total_without_children=0.0;
	while ($result = sqlsrv_fetch_array($resultQuery)) {
		$CountTotal = (float)$result['CountTotal'];
		$total += $CountTotal;
		$data[(int)$result['lvl1ID']]['all'] += $CountTotal;
		if ((int)$result['lvl2ID'] > 0) {
			$data[(int)$result['lvl2ID']]['all'] += $CountTotal;
			if ((int)$result['lvl3ID'] > 0) {
				$data[(int)$result['lvl3ID']]['all'] += $CountTotal;
				if ((int)$result['lvl4ID'] > 0) {
					$data[(int)$result['lvl4ID']]['all'] += $CountTotal;
					if ((int)$result['lvl5ID'] > 0) {
						$data[(int)$result['lvl5ID']]['all'] += $CountTotal;
						$data[(int)$result['lvl5ID']]['only_this'] += $CountTotal;
						$total_without_children += $CountTotal;
					} else {
						$data[(int)$result['lvl4ID']]['only_this'] += $CountTotal;
						$total_without_children += $CountTotal;
					}
				} else {
					$data[(int)$result['lvl3ID']]['only_this'] += $CountTotal;
					$total_without_children += $CountTotal;
				}
			} else {
				$data[(int)$result['lvl2ID']]['only_this'] += $CountTotal;
				$total_without_children += $CountTotal;
			}
		} else {
			$data[(int)$result['lvl1ID']]['only_this'] += $CountTotal;
			$total_without_children += $CountTotal;
		}
	};
	foreach ($data as $key=>$value){
		$key_where = isset($categories["$key"])&&$key>0 ? $categories["$key"] :0;
		$query_tm = "SELECT * FROM es_CategoryRank WHERE es_CategoryId=$key_where";
		$categoryRate=[];
		if($result_tm = $mysqli->query($query_tm)){
			$categoryRate = $result_tm->fetch_array(MYSQLI_ASSOC);
			$result_tm->close();
		}
		$rank = ($value['all']/$total)*100;
		$rankWithOuChildren = ($value['only_this']/$total_without_children)*100;
		if((int)$categoryRate['ID']>0){
			$query_tm = "UPDATE `es_CategoryRank`
						SET
						`Rank` = $rank,
						`RankWithOuChildren` = $rankWithOuChildren
						WHERE ID = ".$categoryRate['ID'];
			$mysqli->query($query_tm);
		}else{
			$query_tm = "INSERT INTO es_CategoryRank (es_CategoryId,Rank,RankWithOuChildren) VALUES($key_where,$rank,$rankWithOuChildren);";
			$mysqli->query($query_tm);
		}
	}
}

function updateRegionRate($start_date)
{
	global $mysqli;

	$query_tm = "SELECT * FROM Regions WHERE ID <=100";
	$regions=[];
	if($result_tm = $mysqli->query($query_tm)){
		while ($region = $result_tm->fetch_array(MYSQLI_ASSOC)){
			$regions[$region['ID']]=$region['ID'];
		}
		$result_tm->close();
	}

	global $connectionOptions;
	global $serverName;
	$sqlConnectId = sqlsrv_connect($serverName, $connectionOptions);

	$query = "EXECUTE GetAVGCountLotInRegion '$start_date'";
	$resultQuery = sqlsrv_query($sqlConnectId, $query);

	$data = [];
	$total =0.0;
	while ($result = sqlsrv_fetch_array($resultQuery)) {
		$CountTotal= (float)$result['AVG_CountTotal'];
		$Region= (int)$result['Region'];
		$total+=$CountTotal;
		$data[$Region]+=$CountTotal;
	};
	foreach ($data as $key=>$value){
		$key_where = isset($regions["$key"]) ? $regions["$key"] :0;
		if($key_where==0){
			continue;
		}
		$query_tm = "SELECT * FROM es_RegionRank WHERE RegionId=$key_where";
		$regionRate=[];
		if($result_tm = $mysqli->query($query_tm)){
			$regionRate = $result_tm->fetch_array(MYSQLI_ASSOC);
			$result_tm->close();
		}

		$rank=($value/$total)*100;
		$count = $value;
		if((int)$regionRate['ID']>0){
			$query_tm = "UPDATE `es_RegionRank`
						SET
						`Rank` = $rank,
						`Count` = $count
						WHERE ID = ".$regionRate['ID'];
			$mysqli->query($query_tm);
		}else{
			$query_tm = "INSERT INTO es_RegionRank (RegionId,Rank) VALUES($key_where,$rank);";
			$mysqli->query($query_tm);
		}
	}
}

function updateSumRate($start_date)
{
	global $connectionOptions;
	global $serverName;
	global $mysqli;
	$sqlConnectId = sqlsrv_connect($serverName, $connectionOptions);

	$sums = [
		0=>'0',
		1=>'300000',
		2=>'600000',
		3=>'1500000',
		4=>'10000000',
		5=>'100000000',
		6=>'>100000000'
		];

	$query = "
			select
			Sum( CASE WHEN l.LotUID IS NULL THEN 0 ELSE 1 END )as CountTotal
			,CASE WHEN  l.Cost=0 
				THEN 0 
				ELSE 
					CASE WHEN  l.Cost<=300000 
						THEN 1
						ELSE 
							CASE WHEN  l.Cost<=600000 
								THEN 2
								ELSE 
									CASE WHEN  l.Cost<=1500000
										THEN 3 
										ELSE 
											CASE WHEN  l.Cost<=10000000
												THEN 4
												ELSE 
													CASE WHEN  l.Cost<=100000000
														THEN 5
														ELSE 6
													END
											END
									END 
							END 
					END 
			END as MaxCost
		from 
			tenders as t
			inner join Lots as l ON t.UID = l.TenderUID	
			WHERE t.PublicDate>='$start_date' AND t.PersonID=1
		group by	 
			CASE WHEN  l.Cost=0 
				THEN 0 
				ELSE 
					CASE WHEN  l.Cost<=300000 
						THEN 1
						ELSE 
							CASE WHEN  l.Cost<=600000 
								THEN 2
								ELSE 
									CASE WHEN  l.Cost<=1500000
										THEN 3 
										ELSE 
											CASE WHEN  l.Cost<=10000000
												THEN 4
												ELSE 
													CASE WHEN  l.Cost<=100000000
														THEN 5
														ELSE 6
													END
											END
									END 
							END 
					END 
			END
		";
	$resultQuery = sqlsrv_query($sqlConnectId, $query);

	$data = [];
	$total=0.0;
	while ($result = sqlsrv_fetch_array($resultQuery)) {
		$CountTotal= (float)$result['CountTotal'];
		$total+=$CountTotal;
		$maxSum = (int)$result['MaxCost'];
		$sumTest = $sums[$maxSum];
		$data[$sumTest]+=$CountTotal;
	};
	foreach ($data as $key=>$value){
		$query_tm = "SELECT * FROM es_CostRank WHERE CostTo='$key'";
		$sumRate=[];
		if($result_tm = $mysqli->query($query_tm)){
			$sumRate = $result_tm->fetch_array(MYSQLI_ASSOC);
			$result_tm->close();
		}
		$rank=($value/$total)*100;
		if((int)$sumRate['ID']>0){
			$query_tm = "UPDATE `es_CostRank`
						SET
						`Rank` = $rank
						WHERE ID = ".$sumRate['ID'];
			$mysqli->query($query_tm);
		}else{
			$query_tm = "INSERT INTO es_CostRank (RegionId,Rank) VALUES('".$key."',$rank);";
			$mysqli->query($query_tm);
		}
	}
}