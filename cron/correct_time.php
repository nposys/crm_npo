<?php

/*
$res = '123';
system('/usr/bin/php70 /home/HHParser/parser.php', $res);
//$res = '123';
$last_line = system('ls', $retval);
print_r($retval);

die();
 */

require_once 'add/db.php';
require_once 'add/tables.php';
require_once 'add/functions/service_functions.php';
require_once 'add/functions/load_tender.php';

$cb_tables['tableEvents'];
$cb_tables['tableOrderJobs'];

$doEventsUpdate = false;
$doJobsUpdate = false;
$doTendersUpdate = false;

// События по клиентам +
// Работы по заказам +
// Тендеры ?
// Вехи
// Заказы дата - дедлайны, даты добавления, дата исполнения
// Счета
// Акты
// Поступления
// Все подтаблицы

//---------------------------------------------------------
if($doTendersUpdate)
{
    $i = 0;
    $serverName = '192.168.0.10';
    $dbName = 'tenderdb';
    ini_set('mssql.charset','UTF8');
    if($sqlLink = mssql_connect ($serverName , 'tenders', 'gdlNtylthc 98'))
    {
        $resTenders = data_select($cb_tables['tableTenders'],"status=0");
        while($rowTender = sql_fetch_assoc($resTenders))
        {
            $i++;
            LoadTender($rowTender['id'], $sqlLink);        
        }
        
        mssql_close($sqlLink);
    }
    write_log("Обновлено $i строк тендеров.");    
}

//---------------------------------------------------------
if($doEventsUpdate)
{
    $i = 0;
    $resEvents = data_select($cb_tables['tableEvents'],"status=0 and add_time<='2016-08-20 00:00:00' and f11100<='2016-08-22 00:00:00'");
    while($rowEvents = sql_fetch_assoc($resEvents))
    {
        $i++;
        
        $add_time = $rowEvents['add_time'];
        $date = $rowEvents['f724'];
        $last_change = $rowEvents['f11100'];
        
        $add_time2 = date("Y-m-d H:i:s", strtotime($add_time." - 4 hour"));
        $date2 = date("Y-m-d H:i:s", strtotime($date." - 4 hour"));
        $last_change2 = date("Y-m-d H:i:s", strtotime($last_change." - 4 hour"));
        
        data_update($cb_tables['tableEvents'],array("add_time"=>$add_time2,"f724"=>$date2,"f11100"=>$last_change2), "id=".$rowEvents['id']);
    
        //f724 - дата
        //f11100 - last change
    }
    write_log("Обновлено $i строк по работам с клиентами.");
    
}



//---------------------------------------------------------
if($doJobsUpdate)
{
    $i = 0;
    $resJobs = data_select($cb_tables['tableOrderJobs'],"status=0 and add_time<='2016-08-20 00:00:00' and f16330='0000-00-00 00:00:00'");
    while($rowJobs = sql_fetch_assoc($resJobs))
    {
        $i++;

        /*
        f12981 - date plan begon
        f12991 - date plan end
        f12641 - date fact begin
        f12651 - date fact end
        */
        
        $add_time = $rowJobs['add_time'];
        $date_plan_begin = $rowJobs['f12981'];
        $date_plan_end = $rowJobs['f12991'];
        $date_fact_begin = $rowJobs['f12641'];
        $date_fact_end = $rowJobs['f12651'];
        
        //$last_change = $rowEvents['f16330'];

        $add_time2 = date("Y-m-d H:i:s", strtotime($add_time." - 4 hour"));
        $date_plan_begin2 = date("Y-m-d H:i:s", strtotime($date_plan_begin." - 4 hour"));
        $date_plan_end2 = date("Y-m-d H:i:s", strtotime($date_plan_end." - 4 hour"));
        $date_fact_begin2 = date("Y-m-d H:i:s", strtotime($date_fact_begin." - 4 hour"));
        $date_fact_end2 = date("Y-m-d H:i:s", strtotime($date_fact_end." - 4 hour"));
        
        data_update($cb_tables['tableOrderJobs'],array("add_time"=>$add_time2,"f12981"=>$date_plan_begin2,"f12991"=>$date_plan_end2,"f12641"=>$date_fact_begin2,"f12651"=>$date_fact_end2), "id=".$rowJobs['id']);

        //f724 - дата
        //f11100 - last change
    }
    write_log("Обновлено $i строк по задачам по заказам.");
    
}


?>