<?php
require_once 'add/tables.php';
require_once 'add/functions/common_other_functions.php';
require_once 'add/functions/service_functions.php';

UpdateAccreditationStatuses();
UpdateSignaturesStatuses();

/*
 * Автор: Шнитко А.Г.
 * Дата создания: 2016-11-15
 * Назначение:
 *  1) автоматическое обновление данных о статусах аккредитаций
 *  2) автоматическое создания события по работе с клиентами для менеджеров
 *      когда прищел срок переаккредитации
 * Входные параметры: нет
 * Выходные параметры: нет
 */

function UpdateAccreditationStatuses()
{
    global $cb_tables;

    $manager_head_id = 640; // 41 - кричевская 640 - шаталова (переделать на автовыбор начальника отдела по таблице сотрудников)
    $accr_work_statuses = ['Возможно продление','Ограниченная','Не действующая'];
    $clients_bad_statuses = ["Отказ", "Недобросовестный"];

    $clients_events = [];
    $count_updates = 0;
    $count_events = 0;

    // ----------------------------------------------------------------
    $res_accreditation = data_select($cb_tables['tableAccreditations'], "status=0");
        // and f17000 not in ('Не действующая')
    while($row_accreditation = sql_fetch_assoc($res_accreditation)){
        $id_accr = $row_accreditation['id'];
        $accr_status_cur = $row_accreditation['f17000'];
        $date_finish = date("Y-m-d", strtotime($row_accreditation['f16980']));
        $date_limited = date("Y-m-d", strtotime($row_accreditation['f16990']));
        $site_id = $row_accreditation['f16970'];
        $record_id = $row_accreditation['id'];
        $company_id = $row_accreditation['f16960'];
        $manager_id = $row_accreditation['f17010'];

        // определяем менеджеров в арихиве - назначаем такие события на начальника отдела
        if($manager_id==0) $manager_id = $manager_head_id;
        $manager_user = sql_select_array(USERS_TABLE,"id=$manager_id");
        $manager_user_name = $manager_user["fio"];
        $manager_user_archive = $manager_user["arc"];
        if($manager_user_archive==1){
            $manager_id = $manager_head_id;
            $manager_user = sql_select_array(USERS_TABLE,"id=$manager_id");
            $manager_user_name = $manager_user["fio"];
        }

        // получаем данные о лиенте
        $company_name = "";

        $company_manager_id = 0;
        $company_manager_username = "";
        if($company_id>0){
            $row_company = data_select_array($cb_tables['tableClients'],"id=$company_id");
            $client_status = $row_company['f4921'];
            $company_name = $row_company['f435'];

            $company_manager_id = $row_company['f8720']; // Менеджер клиента из компании
            $manager_user = sql_select_array(USERS_TABLE,"id=$company_manager_id");
            $company_manager_username = $manager_user["fio"];

            // пропускаем клиентов в плохих статусах
            if(in_array($client_status , $clients_bad_statuses)){
                continue;
            }
        }

        // получаем данные о названии площадки
        $site_name = "";
        if($site_id>0){
            $row_site = data_select_array($cb_tables['tableSites'],"id=$site_id");
            $site_name = $row_site['f6511'];
        }

        // получаем реальный статус аккредитации
        $accr_status_new = GetAccreditationStatus($date_finish);

        if($accr_status_new<>"" && $accr_status_new<>$accr_status_cur){
            // обновляем статус аккредитации
            data_update($cb_tables['tableAccreditations'],['f17000'=>$accr_status_new],"id=$id_accr");
            $count_updates++;
        }

        // создаем плановое событие на менеджера по работе с клиентами - уточнить по необходимости заказа
        if($company_id>0 && in_array($accr_status_new,$accr_work_statuses)){
            $clients_events[$company_name]['sites'][$site_id]['record_id'] = $record_id;
            $clients_events[$company_name]['sites'][$site_id]['site_id'] = $site_id;
            $clients_events[$company_name]['sites'][$site_id]['site_name'] = $site_name;
            $clients_events[$company_name]['sites'][$site_id]['date_finish'] = $date_finish;
            $clients_events[$company_name]['sites'][$site_id]['status'] = $accr_status_new;
            $clients_events[$company_name]['manager_id'] = $company_manager_id; // $manager_id;
            $clients_events[$company_name]['manager_name'] = $company_manager_username; //$manager_user_name;
            $clients_events[$company_name]['company_id'] = $company_id;
        } // end if company_id && statuses
    } // end while

    // ----------------------------------------------------------------
    // добавляем события по тем клиентам, кому актуально продление аккредитации
    foreach($clients_events as $client_name => $client_data){
        $manager_id = $client_data["manager_id"];
        $taskText = "Уточнить у клиента необходимость переаккредитаций (автосоздано)";
        $manager_id = GetActiveManagerForEvent($manager_id, $taskText);
        $company_id = $client_data["company_id"];
        $manager_user_name = $client_data["manager_name"];

        $statuses = "";
        foreach($client_data['sites'] as $site_id => $site_data){
            $record_id = $site_data['record_id'];

            // проверяем не было ли уже создано события на уточнение по данной переаккредитации
            if( !CheckEventExistForCondition(
                    $company_id,
                    "Уточнить у клиента необходимость переаккредитаций (автосоздано)",
                    "record_id",
                    $record_id)){
                $statuses .= $site_data['site_name'].":".$site_data['status']." [record_id:$record_id] ";
            }
        }
        $statuses = trim($statuses);

        // если есть хоть одна площадка с новым статусом, то добавляем событие
        if($statuses<>"")
        {
            $event_title = "Уточнить у клиента необходимость переаккредитаций (автосоздано): $statuses";
            $event_time = GetNearestWorkingTime("",1);

            $event = [];
            $event['status'] = 0;
            $event['f724'] = $event_time; // надо как-то создавать на ближайший рабочий день
            $event['f723'] = $company_id;
            $event['f727'] = $manager_id;
            $event['f773'] = 'Звонок';
            $event['f725'] = $event_title;
            $event['f1053'] = 'Нет'; // выполнено

            $event_dump = print_r($event, true);
            write_log("before create event $event_dump");

            $new_event_id = data_insert ($cb_tables['tableEvents'], EVENTS_ENABLE, $event);
            if($new_event_id>0) $count_events++;
        }
    }

    write_log ("accreditation: total statues updated:$count_updates");
    write_log ("accreditation: total events created:$count_events");
}

/*
 * Автор: Шнитко А.Г.
 * Дата создания: 2016-11-15
 * Назначение:
 *  1) автоматическое обновление данных о статусах ЭЦП
 *  2) автоматическое создания события по работе с клиентами для менеджеров
 *      когда прищел срок перевыпуска ЭЦП
 * Входные параметры: нет
 * Выходные параметры: нет
 */

 function UpdateSignaturesStatuses()
{
    global $cb_tables;

    //$managerHeadId = GetIdForManagerByPosition(3); // получае id начальника отдела = исполнительного директора
    $accr_work_statuses = ['Возможно продление','Ограниченная','Не действующая'];
    $clientsBadStatuses = ["Отказ", "Недобросовестный"];

    $clientsEvents = [];
    $countUpdates = 0;
    $countEvents = 0;

    // ----------------------------------------------------------------
    $resSignature = data_select($cb_tables['tableSignatures'],"status=0"); // and f17000 not in ('Не действующая')
    while($rowSignature = sql_fetch_assoc($resSignature)){
        $idSignature = $rowSignature['id'];
        $signatureStatusCurrent = $rowSignature['f6101'];
        $dateFinish = date("Y-m-d", strtotime($rowSignature['f6061']));
        $dateNow = date("Y-m-d");
        $ucId = $rowSignature['f6051'];
        $companyId = (int)$rowSignature['f6071'];
        $managerId = (int)$rowSignature['f6131']; // менеджер из записи ЭЦП

        // получаем данные о клиенте
        $companyName = "";
        if($companyId>0){
            $rowCompany = data_select_array($cb_tables['tableClients'],"id=$companyId");
            $clientStatus = $rowCompany['f4921'];
            $companyName = $rowCompany['f435'];

            // меняем менеджера на "Менеджер Клиента" из карточки Контрагента
            $managerId = (int)$rowCompany['f8720'];
            if($managerId == 0){
	            $managerId = (int)$rowCompany['f438'];
            }

            // пропускаем клиентов в плохих статусах
            if(in_array($clientStatus, $clientsBadStatuses)){
                continue;
            }
        }

	    // определяем менеджеров в арихиве - назначаем такие события на начальника отдела
	    $taskText = "Уточнить у клиента необходимость продления ЭЦП (автосоздано)";
	    $managerId = GetActiveManagerForEvent($managerId, $taskText);
	    $managerUserRow = sql_select_array(USERS_TABLE,"id=$managerId");
	    $managerUserName = $managerUserRow["fio"];

        // получаем данные об УЦ
        $ucName = "";
        if($ucId>0){
            $ucCompany = data_select_array($cb_tables['tableUc'],"id=$ucId");
            $ucName = $ucCompany['f6041'];
        }

        // получаем реальный статус аккредитации
        $signatureStatusNew = GetSignatureStatus($dateFinish);

        if($signatureStatusNew<>"" && $signatureStatusNew<>$signatureStatusCurrent){
            // обновляем статус аккредитации
            data_update($cb_tables['tableSignatures'],['f6101'=>$signatureStatusNew],"id=$idSignature");
            $countUpdates++;
        }

        // создание события на продление ЭЦП
        $dateProlongation = date("Y-m-d",strtotime($dateFinish." -45 days"));
        if($dateNow<=$dateFinish && $dateNow>$dateProlongation){
            if(!CheckEventExistForCondition(
                $companyId,
                "Уточнить у клиента необходимость продления ЭЦП (автосоздано)",
                "idSignature",
                $idSignature))
            {
                $signatureData = $ucName." до:  ".$dateFinish;

                $eventTitle = "Уточнить у клиента необходимость продления ЭЦП (автосоздано): $signatureData [idSignature:$idSignature]";
                $eventTime = GetNearestWorkingTime("",1);

                $event = [];
                $event['status'] = 0;
                $event['f724'] = $eventTime;
                $event['f723'] = $companyId;
                $event['f727'] = $managerId;
                $event['f773'] = 'Звонок';
                $event['f725'] = $eventTitle;
                $event['f1053'] = 'Нет'; // выполнено

                write_log("signature: before create event $companyName $managerUserName $eventTitle");

                $newEventId = data_insert ($cb_tables['tableEvents'], EVENTS_ENABLE, $event);
                if($newEventId>0){
                    $countEvents++;
                }
            }
        }
    }

    write_log ("signature: total statuses updated: $countUpdates");
    write_log ("signature: total events created: $countEvents");
}