<?php 

// Обновляет дедлайны

$tableOrders = 271;
$tableOrdersSearch = 371;
$tableOrdersApp = 381;
$tableTenders = 331;

$res = data_select($tableOrders, "status=0 and f6551 in ('10. Исполнение', '20. Предоплата')");
while ($row = sql_fetch_assoc($res))
{
    $orderId = $row['id'];
    $orderStatus = $row['f4451'];
    $productionStatus = $row['f10240'];

    $order_event = "";
    $deadline = "";
    $arrUpdate = array();

    // в зависимости от типа услуг
    switch($orderStatus)
    {
        case 4: // поиск
            // f6951 дата окончания поиска
            $rowSearch =  data_select_array($tableOrdersSearch, "status=0 and f5411=".$orderId);
            if($rowSearch['id']>0)
            {
                $datestart = $rowSearch['f6941'];
                $deadline = $rowSearch['f6951'];
            }

            $arrUpdate['f12681'] = $datestart;
            $arrUpdate['f12701'] = $deadline;
             
            break;
        case 5: // подготовка заявки
            // f5091 дата и время подачи
            // f5101 дата и время торгов
            $rowApp =  data_select_array($tableOrdersApp, "status=0 and f5471=".$orderId);
            $tenderId = $rowApp['f5481'];
             
            if($tenderId>0)
            {
                $rowTender = data_select_array($tableTenders, "status=0 and id=".$tenderId);
                switch($productionStatus)
                {
                    case '10. Оценка стоимости':
                    case '20. Заполнение':
                    case '30. Проверка':
                    case '40. Утверждено':
                        $order_event = "Подача заявки";
                        $deadline = $rowTender['f13971']; // Дата и время подачи НСК
                        break;

                    case '50. На подаче':
                    case '60. Ожидание торгов':
                        $order_event = "Торги";
                        $deadline = $rowTender['f13981']; // Дата и время торгов НСК
                        $arrUpdate['f12681'] = $deadline; // поле сдачи работа
                        break;
                         
                    case '70. Победа':
                        break;
                }
            }
             
            $arrUpdate['f12701'] = $deadline; // поле дедлайн
            $arrUpdate['f14091'] = $order_event; // поле вид события

            break;
        default:
            break;
    }

    if($deadline<>"" && count($arrUpdate)>0) data_update($tableOrders, $arrUpdate, "id=".$orderId);

}

?>