<?php

require_once 'add/tables.php';
require_once 'add/functions/service_functions.php';
require_once 'add/functions/common_other_functions.php';

$res_invoices = data_select($cb_tables['tableInvoices'],"status=0 and f10830='Нормальный' and f456>0");
while($row_invoice = sql_fetch_assoc($res_invoices))
{
    $idInvoice = $row_invoice['id'];
    $numInvoice = $row_invoice['f437'];


    $res_orders_invoice = data_select($cb_tables['tableInvoicesOrders'],"status=0 and f807=$idInvoice");
    $numRows = sql_num_rows ($res_orders_invoice);
    $bAllOrdersCanceled = true;
    $orders = "";
    while($row_orders_invoice = sql_fetch_assoc($res_orders_invoice))
    {
        $idOrder = $row_orders_invoice['f7061'];
        $row_order = data_select_array($cb_tables['tableOrders'],"status=0 and id=$idOrder");
        $numOrder = $row_order['f7071'];
        $statusOrder = $row_order['f6551'];

        $orders .= "[$numOrder $statusOrder] ";

        if($statusOrder<>'98. Отказ')
        {
            $bAllOrdersCanceled = false;
            break;
        }
    }

    if($bAllOrdersCanceled)
    {
        write_log("плохой счет: $numInvoice $orders");
        data_update($cb_tables['tableInvoices'], array("status"=>2),"id=$idInvoice");
    }
}

?>