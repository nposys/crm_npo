<?php

require_once 'add/db.php';
require_once 'add/tables.php';
require_once 'add/functions/service_functions.php';
require_once 'add/functions/common_order_functions.php';

// Копирование заказов РТ в список заказов
// write_log("Start import RT");

$debug = false;
$doImport = true;
$doSyncStatus = true;

$strPathPrefix = "\\\\192.168.0.9\\Common\\Data\\Клиенты\\Регионторг, ООО";

$tableOrders = 271;
$tableOrdersApp = 381;
$tableTenders = 331;
$tableOrderJobs = 751;

$serverName = $databases["MSSQL"]["server"];
$connectionOptions = array(
    "Database" => $databases["MSSQL"]["db"],
    "Uid" => $databases["MSSQL"]["user"],
    "PWD" => $databases["MSSQL"]["pass"],
    'ReturnDatesAsStrings' => true,
);

// выбираем настройки из справочника скриптов
$default_managers = data_select_array(1271, "status=0 and f21231='import_regtorg'");
$default_manager_production = data_select_array(46, "status=0 and id=".$default_managers['f21241']);

$arrManagers = []; // 341 - Круппа; 421 - Рожкова; 630 - Лаврентьева 741 - Головченко 821 - Шмигирилова
$arrManagers[] = $default_manager_production['f1400'];

$indManager = 0;

$arrTenders = [];
$arrActive = [];

$appProdAdd = 0;
$appJobAdd = 0;

$sqlConnectId = sqlsrv_connect($serverName, $connectionOptions);
// =============================================================================================
// 1. получаем список текущих заявок на подготовке в MS SQL

if ($sqlConnectId) {
    if ($doImport) {

        // выбираем тендеры в статусе на подготовке заявки
        $query = "
        SELECT
              ord.[Title] AS OrderTitle
              ,CONVERT(nvarchar(30), ord.[OrderDate], 120) AS OrderDate
              ,CONVERT(nvarchar(30), ord.PlanDate, 120) AS PlanDate
              ,ord.PatchFolder
              ,MIN(vnp.[ID]) as [ID]
              ,vnp.[ShortName]
              ,vnp.[ClientID]
              ,vnp.[GroupID]
              ,vnp.[GroupName]
              ,vnp.[LotUID]
              ,vnp.[IsGov]
              ,vnp.[GovRuID]
              ,vnp.[LotNumber]
              ,vnp.[Title] AS TenderTitle
              ,vnp.[Status]
              ,vnp.[StatusName]
              ,MIN(vnp.[SendDate]) as [SendDate]
              ,MIN(vnp.[IDForClient]) as [IDForClient]
              ,vnp.[ManagerID]
              ,vnp.[ManagerName]
          FROM ViewNPOTenders AS vnp
            LEFT JOIN n_OrderTenders AS ot ON ot.GovRuID = vnp.GovRuID AND ot.LotNumber = vnp.LotNumber
            LEFT JOIN n_Orders AS ord ON ord.UID = ot.UIDOrder
          WHERE
            vnp.[Status] IN (9)
            AND vnp.[IDForClient] IS NOT NULL
		GROUP BY 
			 ord.[Title]
              , ord.[OrderDate]
              , ord.PlanDate 
              ,ord.PatchFolder
			  ,vnp.[ShortName]
              ,vnp.[ClientID]
              ,vnp.[GroupID]
              ,vnp.[GroupName]
              ,vnp.[LotUID]
              ,vnp.[IsGov]
              ,vnp.[GovRuID]
              ,vnp.[LotNumber]
              ,vnp.[Title]
              ,vnp.[Status]
              ,vnp.[StatusName]
              ,vnp.[ManagerID]
              ,vnp.[ManagerName]
        ";

        //execute the SQL query and return records
        $resultQuery = sqlsrv_query($sqlConnectId, $query);

        $arrOrders=[];
        if ($resultQuery) {
            $numRows = sqlsrv_num_rows($resultQuery);
            $i = 1;

            // выбираем последнего менеджера производства по услуге подготова заявки
            /*
             $sqlQueryTmp = "SELECT max(id) as id FROM ".DATA_TABLE.$tableOrders." WHERE f4441=3813 and f4451=5";
             $resTmp = sql_query($sqlQueryTmp);
             $rowTmp = sql_fetch_array($resTmp);
             $maxId= $rowTmp['id'];
             if ($debug) echo " MAXID:".$maxId;
             if ($maxId <> "")
             {
             $sqlQueryTmp = "SELECT f9010 FROM ".DATA_TABLE.$tableOrders." WHERE f4441=3813 and f4451=5 and id=".$maxId;
             $resTmp = sql_query($sqlQueryTmp);
             $rowTmp = sql_fetch_array($resTmp);
             $managerProd = substr($rowTmp['f9010'],0,5);
             if($managerProd == "-170-") $indManager = 0;
             else if($managerProd == "-160-") $indManager = 1;
             }
             */
            $indManager = 0;

            // перебираем заказы из подготовки
            while ($row = sqlsrv_fetch_array($resultQuery)) {
                write_log('Next tender ' . $row['GovRuID'] . " " . $row['LotNumber']);

                switch((int)$row['IsGov']){
                    case 1:
                        $indManager = 0; // госзакупки
                        break;
                    case 0:
                        $indManager = 0; // коммерческие
                        break;
                    default:
                        $indManager = 0; // по умолчанию
                        break;
                }

                $orderId = "";
                $arrTenders[$i] = $row['GovRuID'] . " " . $row['LotNumber'];

                // 2. проверяем каждую заявку не создан ли уже для нее заказ в КБ
                // f10250 idSQL (CRM NPO) = ClientsLotID (MS SQL) = f7690 (CRM RT)
                $rowOrderApp = data_select_array($tableOrders, "status=0 and f4441=3813 and f10250 LIKE '" . trim($row['ID'] . "%'"));

                // f9010 - менеджеры производство 160 Полякова, 170 Васильева
                if ($rowOrderApp['id'] == "") {

                    if ((int)($row['IsGov']) == 1) {
                        $orderSum = 600; // 44-ФЗ по 400 рублей (с 01.05.2018 по 600)
                        $pricePosId = 51; // "Заявка [РТ] обычная"
                    } else {
                        $orderSum = 3000; // 223-ФЗ и прочие по 2000 рублей (с 01.05.2018 по 3000)
                        $pricePosId = 57; // "Заявка [РТ] коммерческая"
                    }

                    // 2.1. Если нет такого заказа, то создаем его в КБ
                    $arrInsert = array(
                        'status' => '0',
                        'f4451' => 5,                     // тип заказа = "Заявка"
                        'f13071' => $pricePosId,                     // позиция прайса = "Заявка [РТ]"
                        'f6551' => '10. Исполнение',      // статус заказа
                        'f4471' => 'Да',                  // согласовано с клиентом
                        'f4431' => $row['OrderTitle'],         // название тендера
                        'f6621' => $strPathPrefix . $row['PatchFolder'],
                        'f7301' => $row['ManagerName'],   // примечание
                        'f6591' => $row['OrderDate'],     // дата заказа
                        //'f6601'=>$row['PlanDate'],
                        'f9010' => "-" . $arrManagers[$indManager] . "-",
                        'f10250' => trim($row['ID']),  // MS SQL ID
                        'f4461' => $orderSum,            // стоимость подготовки заявки регионторг
                        'f4441' => 3813                   // клиент = регионторг
                    );

                    $orderId = data_insert($tableOrders, EVENTS_ENABLE, $arrInsert);
                    //$indManager = !$indManager; // меняем менеджера производство на следующего

                    // вставляем дополнительную информацию
                    $arrInsert = array(
                        "status" => 0,
                        "f10620" => $orderId,
                        "f10670" => 1,
                        "f10720" => $row['ID'],// MS SQL ID
                        "f10730" => $row['PlanDate']
                    );
                    data_insert(640, $arrInsert);

                    $arrOrders[$i]['orderID'] = $orderId;
                    $arrOrders[$i]['GovRuID'] = trim($row['GovRuID']);
                    $arrOrders[$i]['LotNumber'] = trim($row['LotNumber']);

                    // создаем задачу на менеджера
                    $dateStart = date("Y-m-d H:i:s");
                    $dateFinish = date("Y-m-d H:i:s", strtotime($dateStart . " + 10 min"));
                    $arrJob = array(
                        "status" => 0,
                        "f12601" => $orderId, // заказ
                        "f12621" => 3813,                   // клиент = регионторг
                        "f12611" => 5,                     // тип заказа = "Заявка"
                        "f12951" => $row['OrderTitle'],         // название тендера
                        "f12661" => $arrManagers[$indManager],    // менеджер
                        "f13591" => 10,                 // трудоемкость
                        "f12981" => $dateStart,  // дата начала план
                        "f12991" => $dateFinish,  // дата начала план
                        "f14741" => "Обычная",    // важность
                        "f12861" => 4, // тип работы - оформление заявки
                        "f12671" => "Начать работу по заказу", // описание
                        "f12631" => "10. Запланировано" //статус
                    );
                    $jobId = data_insert($tableOrderJobs, EVENTS_ENABLE, $arrJob);

                    $i++;

                } else {
                    // если заказ уже есть - обновляем данные
                    write_log('already have order ' . $rowOrderApp['id']);
                    $rowOrderAppProd = data_select_array($tableOrdersApp, "status=0 and f5471=" . $rowOrderApp['id']);
                    if ($rowOrderAppProd['id'] == "") {
                        $orderAppId = data_insert($tableOrdersApp,
                            EVENTS_ENABLE,
                            array(
                                'status' => '0',
                                'f7441' => trim($row['GovRuID']),
                                'f8680' => trim($row['LotNumber']),
                                'f5491' => 'Нет', // срочная
                                'f8110' => 'Нет', // подбор материалов
                                'f8120' => 'Да',  // подача
                                'f8130' => 'Да',  // торги
                                'f6561' => '20. Заполнение',   // статус производства
                                'f5471' => $rowOrderApp['id']      // связь с "Заказы"
                            )
                        );
                        $appProdAdd++;
                    }

                    $rowOrderAppJob = data_select_array($tableOrderJobs, "status=0 and f12601=" . $rowOrderApp['id']);
                    if ($rowOrderAppJob['id'] == "") {
                        // создаем задачу на менеджера
                        $dateStart = date("Y-m-d H:i:s");
                        $dateFinish = date("Y-m-d H:i:s", strtotime($dateStart . " + 10 min"));
                        $arrJob = array(
                            "status" => 0,
                            "f12601" => $rowOrderApp['id'], // заказ
                            "f12621" => 3813,                   // клиент = регионторг
                            "f12611" => 5,                     // тип заказа = "Заявка"
                            "f12951" => $row['OrderTitle'],         // название тендера
                            "f12661" => $arrManagers[$indManager],    // менеджер
                            "f13591" => 10,                 // трудоемкость
                            "f12981" => $dateStart,  // дата начала план
                            "f12991" => $dateFinish,  // дата начала план
                            "f14741" => "Обычная",    // важность
                            "f12861" => 4, // тип работы - оформление заявки
                            "f12671" => "Начать работу по заказу", // описание
                            "f12631" => "10. Запланировано" //статус
                        );
                        $jobId = data_insert($tableOrderJobs, $arrJob);
                        $appJobAdd++;
                    }

                }
            } // end while - перебор тенедроа
        } // end if normal ms sql query result

        // если добавляли новые заказы
        for ($j = 1; $j < $i; $j++) {
            // 2.2. Заполняем позиции заказа ссылкой на тендер
            //  если это делать сразу, то там при создании записи тендера есть коннект в MS SQL который сбрасывает нашу выборку здесь
            $data = array(
                'status' => '0',
                'f7441' => $arrOrders[$j]['GovRuID'],
                'f8680' => $arrOrders[$j]['LotNumber'],
                'f5491' => 'Нет', // срочная
                'f8110' => 'Нет', // подбор материалов
                'f8120' => 'Да',  // подача
                'f8130' => 'Да',  // торги
                'f6561' => '20. Заполнение',   // статус производства
                'f5471' => $arrOrders[$j]['orderID']      // связь с "Заказы"
            );
            $OrderApp = data_select_array($tableOrdersApp, "status=0 and f5471=" . $arrOrders[$j]['orderID']);
            if($OrderApp['id']>0){
                data_update($tableOrdersApp,EVENTS_ENABLE,array('status' => '2'),'id = ',$OrderApp['id']);
            }else {
                $orderAppId = data_insert($tableOrdersApp,
                    EVENTS_ENABLE,
                    $data
                );
            }
        } // end for
    } // end if normal ms sql connect

    // =============================================================================================
    // 3. проверяем заявки на подготовке в КБ по Регионторгу (по полю idSQL). статус производства в КБ == исполнение
    // если РТ отказался от заявки то меняем статус заказа в КБ и отправляем менеджеру извещение на почту или СМС

    if ($doSyncStatus) {
        $resKB = data_select($tableOrders, "f4441=3813 and f10250<>'' and f4451=5 and status=0 and ((f6551 in ('10. Исполнение','20. Предоплата')) or (f6551='30. Завершено' and f10240='50. На подаче'))"); //
        while ($rowKBOrder = sql_fetch_assoc($resKB)) {
            $resKBApp = data_select_array($tableOrdersApp, "f5471=", $rowKBOrder['id']);
            if ($rowKBOrder['f10250'] <> "") //$resKBApp['f7441']<>"" && $resKBApp['f8680']<>""
            {
                // делаем запрос в MS SQL про этот тендер
                $query = "
                    SELECT vnp.[Status],vnp.[StatusName]
                    FROM ViewNPOTenders AS vnp
                    WHERE vnp.[ID]=" . $rowKBOrder['f10250'] . " AND vnp.[IDForClient] IS NOT NULL
                  ";

                // WHERE vnp.[GovRuID] LIKE '".$resKBApp['f7441']."%' and vnp.[LotNumber]=".$resKBApp['f8680']." and vnp.[IDForClient] is not NULL

                $resultQuery = sqlsrv_query($sqlConnectId, $query);
                if ($resultQuery) {
                    $row = sqlsrv_fetch_array($resultQuery);
                    if ($row) {
                        if ($debug) echo " тут:(" . $row['StatusName'] . ")";
                        switch ($row['StatusName']) {
                            case "10. Отказ от участия":
                                data_update($tableOrders, EVENTS_ENABLE, array('status' => '0', 'f6551' => "98. Отказ"), "id=" . $rowKBOrder['id']);
                                break;
                            case "20. Победа":
                                data_update($tableOrders, EVENTS_ENABLE, array('status' => '0', 'f6551' => "30. Завершено"), "id=" . $rowKBOrder['id']);
                                data_update($tableOrdersApp, EVENTS_ENABLE, array('status' => '0', 'f6561' => "70. Победа"), "f5471=" . $rowKBOrder['id']);
                                break;
                            case "60. Заявка не доставлена":
                            case "30. Поражение":
                                data_update($tableOrders, EVENTS_ENABLE, array('status' => '0', 'f6551' => "30. Завершено"), "id=" . $rowKBOrder['id']);
                                data_update($tableOrdersApp, EVENTS_ENABLE, array('status' => '0', 'f6561' => "80. Поражение"), "f5471=" . $rowKBOrder['id']);
                                break;
                            case "70. Заявка отклонена":
                                data_update($tableOrders, EVENTS_ENABLE, array('status' => '0', 'f6551' => "97. Не исполнено"), "id=" . $rowKBOrder['id']);
                                data_update($tableOrdersApp, EVENTS_ENABLE, array('status' => '0', 'f6561' => "90. Отклонено"), "f5471=" . $rowKBOrder['id']);
                                break;
                            default:
                                break;
                        } // end switch status name
                    }
                } // end if ms sql result query
            } // end if app exists
        } // end while CRM orders

    } // end doSyncStatus

    //close the connection
    sqlsrv_close($sqlConnectId);

} // end if SQL LINK

write_log("[RES] Imported " . ($i - 1) . " record(s)");
write_log("[RES] Prod add $appProdAdd record(s)");
write_log("[RES] Job add $appJobAdd record(s)");