<?php
/*
 * Скприт получает список зарегистрированных клинетов из ТендерМонитор и добавляет их в CRM
 * Скрипт создает заказы на пробный поиск в CRM для вновь зарегистированных клиентов
 *
 */

require_once 'add/db.php';
require_once 'add/tables.php';
require_once 'add/functions/service_functions.php';
require_once 'add/functions/tenmon_functions.php';
require_once 'add/functions/common_other_functions.php';

$exclude_email = [
    "@dropmail.me",
    "@tenmon.ru",
    "@10mail.org",
    "@yomail.info",
    "@emltmp.com",
    "chekushkin@gorodok.net",
    "evgeny.chekushkin1@gmail.com",
    "shnitko@ngs.ru"
];

// ---------------------------------------------------------------------
// подключаемся к базе тенмон
// ---------------------------------------------------------------------
$bTMok = false;
$mysqli = mysqli_init();
if (!$mysqli->real_connect(
        $databases['TM']['server'],
        $databases['TM']['user'],
        $databases['TM']['pass'],
        $databases['TM']['database'])) {
    die('Ошибка подключения (' . mysqli_connect_errno() . ') '
        . mysqli_connect_error());
} else {
    $bTMok = true;
}
$mysqli->query("SET NAMES 'utf8'");

$count_tariff_updated = 0;
$count_clients_created = 0;
$count_clients_updated = 0;
$count_orders_created = 0;
$count_current_clients = 0;
$count_uncurrent_clients = 0;

$count_accreditation = 0;
$count_cl = 0;
$query_tm =
    '
SELECT
  u.ID
  ,u.ShortName
  ,u.FullName
  ,u.INN
  ,u.KPP
  ,u.OGRN
  ,u.LegalAddress
  ,u.Phone
  ,u.SearchEnable
  ,u.Tariff
  ,u.ManagerID as AccountManagerId
  ,u.ManagerSaleID as SaleManagerId
  ,u.login
  ,u.is_Advert
  ,u.isCurrentClient
  ,from_unixtime(du.access) as DateLastAccess
  ,from_unixtime(du.login) as DateLastLogin
  ,from_unixtime(du.created) as DateCreated
  ,(select count(*) from tender_monitor.AppGuarantees as ag where ag.UserID = u.ID) as CountSites
  , CASE WHEN u.INN = \'0\' THEN "Физ.лицо" ELSE "Юр.лицо" END as ClientType
FROM tender_monitor.Users as u
  LEFT JOIN drupal.users as du ON du.mail = u.login
  WHERE u.ShortName IS NOT NULL AND u.ShortName <> \'\'
  AND u.INN IS NOT NULL
  AND  from_unixtime(du.created) >= NOW() - interval 2 MONTH
ORDER BY
  du.created DESC
';

// выбираем настройки из справочника скриптов
$default_managers = data_select_array(1271, "status=0 and f21231='sync_tenmon_clients'");
$default_manager_sales = data_select_array(46, "status=0 and id=".$default_managers['f21211']);
$default_manager_clients = data_select_array(46, "status=0 and id=".$default_managers['f21221']);
$default_manager_production = data_select_array(46, "status=0 and id=".$default_managers['f21241']);

$data['manager_sales'] = $default_manager_sales['id'];
$data['manager_sales_user'] = $default_manager_sales['f1400'];
$data['manager_sales_tm_id'] = $default_manager_sales['f18631'];

$data['manager_client'] = $default_manager_clients['id'];
$data['manager_client_user'] = $default_manager_clients['f1400'];
$data['manager_client_tm_id'] = $default_manager_clients['f18631'];

$data['manager_production'] = $default_manager_production['id'];
$data['manager_production_user'] = $default_manager_production['f1400'];

// перебираем выборку из ТМ
if ($result_tm = $mysqli->query($query_tm)) {
    write_log("Select вернул " . $result_tm->num_rows . " строк.");

    $i = 0;
    while ($row_tm = $result_tm->fetch_array(MYSQLI_ASSOC)) {
        $clientId = 0;
        $tm_client_id = (int)$row_tm['ID'];
        $short_name = trim($row_tm['ShortName']);
        $full_name = trim($row_tm['FullName']);
        $inn = trim($row_tm['INN']);
        $kpp = trim($row_tm['KPP']);
        $ogrn = trim($row_tm['OGRN']);
        $address = trim($row_tm['LegalAddress']);
        $phone = cleanupPhone($row_tm['Phone']);
        $email = trim($row_tm['login']);
        $search_enable = (int)$row_tm['SearchEnable'];
        $tariff = (int)$row_tm['Tariff'];
        $is_advert = (int)$row_tm['is_Advert'];
        $is_current_client = (int)$row_tm['isCurrentClient'];
        $tm_manager_sales_id = (int)$row_tm['SaleManagerId'];
        $tm_manager_client_id = (int)$row_tm['AccountManagerId'];
        $count_sites = (int)$row_tm['CountSites'];
        $count_groups = (int)$row_tm['CountGroups'];
        $client_type = $row_tm['ClientType'];

        //if ($tm_client_id !== 1107) continue;

        $date_lastaccess = $row_tm['DateLastAccess'];
        $date_lastlogin = $row_tm['DateLastLogin'];
        $date_created = $row_tm['DateCreated'];

        if ($short_name === '' && $full_name !== '') {
            $short_name = $full_name;
        }
        if ((int)$inn === 0) {
            $inn = '';
        }

        // переменная для передачи в процедуру создания заказа
        $data['date_start'] = date('Y-m-d H:i:s', strtotime($date_created));
        $data['date_end'] = date('Y-m-d H:i:s', strtotime($data['date_start'] . " + 7 day"));
        $data['date_current'] = date('Y-m-d H:i:s');;
        $data['tariff'] = $tariff;

        $bUpdateAccreditationInfo = false;
        $bCreateTrialOrder = false;

        // проверяем что клиент зарегистрирован не под мусорным емайлом
        $in_exclude_list = false;
        foreach ($exclude_email as $next_pattern) {
            if ($next_pattern === $email || strpos($email, $next_pattern)!==FALSE) {
                $in_exclude_list = true;
                // write_log ("%%%%%%%%%% skip email:$email");
                break;
            }
        }
        // фильтруем мусорные регистрации
        if ($in_exclude_list === true) {
            continue;
        }

        $rowClient = data_select_array($cb_tables['tableClients'],
            "status=0 and f16230=$tm_client_id");
        if((int)$rowClient['id']==0) {
            // проверяем наличие записи о данном клиенте в CRM
            if ((int)$inn > 0) {
                // если указан ИНН
                $rowClient = data_select_array($cb_tables['tableClients'],
                    "status=0 and f1056='$inn'");
            } else {
                // если ИНН пустой, то ищем по имени клиента и email в записях с пустым ИНН
                $rowClient = data_select_array($cb_tables['tableClients'],
                    "status=0 and f1056='' and f435='$short_name' and f442='$email'");
            }
        }else{
            write_log(" +exists $tm_client_id ");
        }

        $clientId = $rowClient['id'];
        $client = [];
        if ($clientId > 0) {
            //write_log(" + found existing client $clientId $inn $short_name in CRM");

            // если клиент уже существует в CRM
            if ((int)$rowClient['f16230'] > 0) { // проверяем что заполнено поле tenmonId в карточке клиента в CRM
                $client['f16380'] = $search_enable;
                $client['f16400'] = $is_advert;
                data_update($cb_tables['tableClients'], $client, "id=$clientId");
            } else {
                $client['f16230'] = $tm_client_id;
                $client['f16380'] = $search_enable;
                $client['f16400'] = $is_advert;
                data_update($cb_tables['tableClients'], $client, "id=$clientId");
                $count_clients_updated++;
            }

            // если недавняя регистрация и нет заказа на пробный поиск, то автоматически создаем заказ
            if ($data['date_current'] < $data['date_end']) {
                $rowOrderSearch = data_select_array($cb_tables['tableOrders'],
                    "status=0 and f4441=$clientId and f4451=4 and f6551 in ('10. Исполнение')"); // f4451=4 Услуга=Поиск
                if (!$rowOrderSearch['id']) {
                    $bCreateTrialOrder = true;
                    write_log(" ++++ for existing client $clientId $inn $short_name no search order ");
                } else {
                    write_log(" ++++ for existing client $clientId $inn $short_name found search order id:".
                        $rowOrderSearch['id']);
                }
            }

            // проставляем менеджеров продаж и клиента в Тенмон
            $managerSalesId = $rowClient['f7811'];
            $managerSalesTmId = 0;
            if ($managerSalesId>0) {
                $rowManager = data_select_array($cb_tables['tablePersonal'], 'id='.$managerSalesId);
                $managerSalesTmId = (int)$rowManager['f18631'];
            }
            $managerClientId = $rowClient['f9000'];
            $managerClientTmId = 0;
            if ($managerClientId>0) {
                $rowManager = data_select_array($cb_tables['tablePersonal'], 'id='.$managerClientId);
                $managerClientTmId = (int)$rowManager['f18631'];
            }

            if ($tm_manager_sales_id!==$managerSalesTmId || $tm_manager_client_id!==$managerClientTmId ) {
                SetTenmonManagers($tm_client_id, $managerSalesTmId, $managerClientTmId);
            }

            // ставим признак синхронизации данных об аккредитациях
            $bUpdateAccreditationInfo = true;

            // проверяем существует ли действующий заказ на платный поиск у клиента
            // f4461>0 сумма заказа больше 0

            // fix to support group of companies - mark all clients from group as enabled
            $tenmon_uids = GetTenmonUIDsFromGroupOfComapnies($clientId, $cb_tables, true);
            $clients_group = '';
            foreach ($tenmon_uids as $next_tenmon_client) {
                $clients_group .= $next_tenmon_client['client_id'] . ',';
            }

            if (trim($clients_group) !== '') {
                $clients_group = '(' . substr($clients_group, 0, -1) . ')';
            } else {
                $clients_group = '(' . $clientId . ')';
            }

            $rowOrderSearch = data_select_array($cb_tables['tableOrders'],
                "status=0 and f4441 in $clients_group and f4451=4 and f13071 in (1,55,62,64) and f6551='10. Исполнение'");
            if ($rowOrderSearch) {

                $row_search_prod = data_select_array($cb_tables['tableOrdersSearch'],
                    "status=0 and f5411=".$rowOrderSearch['id']);
                if ($row_search_prod) {
                    $end_date_found = $row_search_prod['f6951'];
                }

                // setIsCurrentClient
                if ($is_current_client === 0) {
                    $res = SetIsCurrentClient($tm_client_id, true);
                }

                $count_current_clients++;
            } else {
                // получаем последний завершенный поиск
                $end_date = '';

                $resMaxEndDateSearch = data_select_field($cb_tables['tableOrdersSearch'],
                    'max(f6951) as MaxEndDate',
                    "status=0 and f10190 in $clients_group and f9260 in ('10. Исполнение', '30. Завершено')");
                if($rowMaxEndDateSearch = sql_fetch_assoc($resMaxEndDateSearch)) {
                    $end_date = $rowMaxEndDateSearch['MaxEndDate'];
                }

                if($is_current_client !== 0) {
                    $res = SetIsCurrentClient($tm_client_id, false, $end_date);
                    write_log(" ---- for existing client (or group)  ".
                        "tm_id:$tm_client_id crm_id:$clientId inn:$inn $short_name ".
                        "not found paid search order");
                }
                $count_uncurrent_clients++;
            }

        } else {
            // если клиент еще не существует в CRM
            write_log("- Not found client in CRM: tm_id: $tm_client_id  ИНН:($inn) КПП:($kpp) [$short_name / $full_name] [$email]");

            $client['status'] = 0;

            if (($kpp === '' && strlen($inn) === 12) || $client_type==='Физ.лицо') {
                $client['f3711'] = "Физ.лицо";
            } else {
                $client['f3711'] = "Юр.лицо";
            }

            $client['f772'] = "Потенциальный"; // Клиент
            $client['f11161'] = "Поставщик";

            $client['f552'] = 'Начата работа'; // статус лида
            // $client['f4921'] = 'В работе'; // статус постоянного клиента

            $client['f1056'] = $inn;
            $client['f1057'] = $kpp;
            $client['f3340'] = $ogrn;
            $client['f442'] = $email;
            $client['f441'] = $phone;

            $client['f435'] = $short_name;
            $client['f439'] = $full_name;
            $client['f440'] = $address; // Юрадрес
            $client['f1047'] = $address; // фактический адрес
            $client['f18761'] = 4; // откуда узнал - источник Тенмон

            if((int)$data['manager_sales']>0){
                $client['f438'] = $data['manager_sales_user'];
                $client['f7811'] = $data['manager_sales'];
            }

            if((int)($data['manager_client'])>0){
                $client['f8720'] = $data['manager_client_user'];
                $client['f9000'] = $data['manager_client'];
            }

            $client['f16230'] = $tm_client_id;
            $client['f16380'] = $search_enable;
            $client['f16400'] = $is_advert;
            $client['f16390'] = 1;

            if((int)$data['manager_sales_tm_id']>0 || (int)$data['manager_client_tm_id']>0){
                SetTenmonManagers($tm_client_id, $data['manager_sales_tm_id'], $data['manager_client_tm_id']);
            }

            // создаем новую запись клиента в CRM
            $clientId = (int)data_insert($cb_tables['tableClients'],EVENTS_ENABLE, $client);
            //write_log("--- Result create client in CRM:$clientId");

            if ($clientId > 0) {
                $count_clients_created++;
                $bCreateTrialOrder = true;
                $bUpdateAccreditationInfo = true;
                $rowClient = data_select_array($cb_tables['tableClients'], "id=$clientId");
            } // end if client is added

        } // end check if client exist

        // ----------------------------------------------------------------------------------
        // добавляем заказ на тестовый поиск
        // ----------------------------------------------------------------------------------
        if ($clientId>0 && $bCreateTrialOrder) {

            $data['client_id'] = $clientId;

            if ($search_enable && $data['date_current'] < $data['date_end']) {
                $result = CreateSearchOrder($data, $cb_tables, ['new_reg'=>true]);
                CreateManagerEvent($data, $cb_tables);

                write_log(" ++++ CreateSearchOrder for id:$clientId inn:$inn name:$short_name ".
                    " date_start:".$data['date_start'].
                    " date_end:".$data['date_end'].
                    " tariff:".$data['tariff'].
                    " RESULT:" . $result);
                if ($result > 0) {
                    $count_orders_created++;
                } else if ($result == -2) {
                    //write_log(" ++++ already exist order for another company in Group of companies!");
                } else {
                    //write_log(" ++++ order not created for unknown reason");
                }
            } // end if search_enable and is_advert true - creation of trial search orders
        }

        // ----------------------------------------------------------------------------------
        // обновляем сведения об аккредитациях в CRM на базе сведений из Тенмон
        // ----------------------------------------------------------------------------------
        if ($bUpdateAccreditationInfo && $clientId > 0 && $count_sites > 0) {
            $result = GetCompanyInfo($tm_client_id);
            $count_accreditation++;

            foreach ($result['data'] as $site_data) {
                $site_id = (int)$site_data['AuctionSiteID'];
                $date_finish = date("Y-m-d", strtotime($site_data['AccreditationDate']));
                $date_limited = date("Y-m-d", strtotime($date_finish . " -3 month"));
                $date_now = date("Y-m-d");
                $is_actual = GetAccreditationStatus($date_finish);

                if ($date_finish == "1970-01-01" || strtotime($site_data['AccreditationDate']) <= 0) continue;

                if ($site_id > 0) {
                    // проверяем есть ли уже такая запись
                    $row_next_accreditation = data_select_array($cb_tables['tableAccreditations'],
                        "status=0 and f16960=$clientId and f16970=$site_id");

                    $accr = [];
                    $accr['status'] = 0;
                    $accr['f16980'] = $date_finish;
                    $accr['f16990'] = $date_limited;
                    $accr['f16970'] = $site_id;
                    $accr['f16960'] = $clientId;
                    $accr['f17000'] = $is_actual;

                    if ($row_next_accreditation['id'] > 0) {
                        $cur_finish = date("Y-m-d", strtotime($row_next_accreditation['f16980']));
                        $cur_limited = date("Y-m-d", strtotime($row_next_accreditation['f16990']));

                        if ($cur_finish < $date_finish) {
                            //write_log("___changed accreditation date - need update or create new record for tm_uid:$tm_client_id  INN:$inn ShortName:$short_name: $site_id $date_finish==$cur_finish $date_limited==$cur_limited");
                        }
                    } else {
                        // добавляем
                        $new_accr_id = data_insert($cb_tables['tableAccreditations'], $accr);
                        //write_log("accreditation info create new_id:$new_accr_id tm_uid:$tm_client_id INN:$inn ShortName:$short_name: site_id:$site_id $date_finish $date_limited");
                    }
                }
            } // end foreach site_data
        } // end if bAccreditationUpdate
        $i++;
        //if ($i > 25) break;
    } // end while

    /* очищаем результирующий набор */
    $result_tm->close();

} // end if

write_log("[$$] paid clients in CRM: $count_current_clients");
write_log("[??] trial clients in CRM: $count_uncurrent_clients");
write_log("clients created in CRM: $count_clients_created");
write_log("clients updated in CRM: $count_clients_updated");
write_log("orders created in CRM: $count_orders_created");
write_log("accreditation data: $count_accreditation");
write_log("total records procesed: $i");

// ---------------------------------------------------------------------
/* закрываем соединение */
// ---------------------------------------------------------------------
if ($bTMok) {
    $mysqli->close();
}
