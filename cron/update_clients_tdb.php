<?php


use classes\Base\PGDB;
use classes\Controllers\TDBCompaniesController;

require_once 'add/functions/service_functions.php';

$debug = true;

ini_set('max_execution_time', 3600); // 60 minutes

set_time_limit(3600);

if (!is_int($work_duration_minutes)) {
    $work_duration_minutes = 1;
}

write_log("work_duration: $work_duration_minutes");

$cur_time = date("Y-m-d H:i:s");
$max_time = date("Y-m-d H:i:s", strtotime($cur_time . " + $work_duration_minutes minutes"));
write_log("## Дата начала статистики участий: $dt_start");

//connect to pgsql
$options = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES => false,
];

$pgdb = new PGDB($options);
$tdbCompanies = new TDBCompaniesController($pgdb,
    date('y-m-d', strtotime(date('y-m-d') . " - 1 week")),
    date('y-m-d'));
$tdbCompanies->run();




