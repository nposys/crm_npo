<?php

require_once 'add/db.php';
require_once 'add/tables.php';
require_once 'add/functions/service_functions.php';
require_once 'add/functions/common_order_functions.php';
require_once 'add/functions/common_clients_functions.php';
require_once 'add/functions/tenmon_functions.php';

// подключаемся к базе тенмон
$mysqli = mysqli_init();
if (!$mysqli->real_connect($databases['TM']['server'], $databases['TM']['user'], $databases['TM']['pass'], $databases['TM']['db'])) {
    die('Ошибка подключения (' . mysqli_connect_errno() . ') '
        . mysqli_connect_error());
}
$mysqli->query("SET NAMES 'utf8'");

// синхронизуем заказы заявок, жалоб из Тенмона
$query_tm =
    "
SELECT
    ord.ID AS OrderId
     ,ord.CreateDate AS OrderCreateDate
     ,ord.ClientName AS ContactPerson
     ,ord.ClientPhone AS ContactPhone
     ,ord.ClientEmail AS ContactEmail
     ,ord.Processed
     ,ord.ProcessedDate
     ,u.ID AS tenmonId
     ,u.ShortName AS ClientName
     ,u.INN
     ,u.KPP
     ,ot.ID AS OrderTypeId
     ,ot.Title AS OrderType
     ,l.Cost
     ,l.ContractCost
     ,l.TenderLawID
     ,l.TenderTypeID
     ,l.Title AS LotTitle
     ,l.LotUID
     ,l.GovRuID
     ,l.LotNumber
     ,ord.LotUID as ExGovRuId
     ,ord.LotNumber as ExLotNumber
     ,noec.Id as ExCompanyId
     ,noec.CompanyName as ExCompanyName
     ,noec.Inn as ExCompanyInn
     ,ord.OrderData
     ,ord.TariffId
     ,st.Title
     ,ord.TariffDurationId
     ,std.Duration
FROM npoOrders AS ord
         LEFT JOIN npoOrdersExCompany as noec ON noec.Id = ord.ExCompanyId
         LEFT JOIN Users AS u ON u.id=ord.UserID
         LEFT JOIN npoOrderTypes AS ot ON ot.id	= ord.OrderTypeID
         LEFT JOIN Apps AS a ON a.ID = ord.AppID
         LEFT JOIN Lots AS l ON l.LotUID = a.LotUID
         LEFT JOIN sp_Tariff st on ord.TariffId = st.ID
         LEFT JOIN sp_TariffDuration std on ord.TariffDurationId = std.ID
WHERE 
    ord.Processed IS NULL
    and u.ID IS NOT NULL
ORDER BY
    ord.CreateDate DESC;
";

$numMatchApp = 0;
$numDontMatchApp = 0;

if ($result_tm = $mysqli->query($query_tm)) {
    write_log("Select вернул " . $result_tm->num_rows . " строк.");

    while ($row_tm = $result_tm->fetch_array(MYSQLI_ASSOC)) {

	    $order_type_id = (int)$row_tm['OrderTypeId'];

	    // заказы на поиск
	    if($order_type_id == 7){
	        $tm_search_tariff_id = (int)$row_tm['TariffId'];
	        $tm_search_tariff_durations = (int)$row_tm['Duration'];
	        // пока созадется всегда экспертный, хотя это не верно
		    if(CreateExpertSearchOrder($row_tm)){
			    continue;
		    }
	    }

        $tm_order_id = (int)($row_tm['OrderId']);
        $tm_order_date = $row_tm['OrderCreateDate'];
        $tm_order_contactperson = $row_tm['ContactPerson'];
        $tm_order_contactphone = $row_tm['ContactPhone'];
        $tm_order_contactemail = $row_tm['ContactEmail'];

        $tm_user_id = (int)$row_tm['tenmonId'];
        $ex_user_id = (int)$row_tm['ExCompanyId'];
        if($tm_user_id > 0) {
            $inn = trim($row_tm['INN']);
            $kpp = trim($row_tm['KPP']);
            $govruid = $row_tm['GovRuID'];
            $lotnumber = $row_tm['LotNumber'];
            $lotuid = $row_tm['LotUID'];
            $lot_cost = (float)($row_tm['Cost']);
            $contract_cost = (float)($row_tm['ContractCost']);
            //$contract_cost_actual = (float)($row_tm['ContractCostActual']);
            $lot_title = $row_tm['LotTitle'];
        } else {
            if ($ex_user_id > 0) {
                $inn = trim($row_tm['ExCompanyInn']);
                $kpp = '';
                $ex_company_name = trim($row_tm['ExCompanyName']);
                $govruid = $row_tm['ExGovRuId'];
                $lotnumber = $row_tm['ExLotNumber'];
            }
        }

        $order_type = $row_tm['OrderType'];
        $tender_law_id = $row_tm['TenderLawID'];
        $tender_type_id = $row_tm['TenderTypeID'];

        $status_order = '05. Оценка';

        write_log("New unprocessed order from TM type_id:$order_type_id ($order_type) user_id:$tm_user_id ");

        switch ($order_type_id) {
            // ---------------------------------
            case 1: //Подготовка заявки
                // ---------------------------------
                $serviceId = 5;
                $order_description = ($lot_title !== '' ? $lot_title : $govruid ) ;
                switch ($tender_law_id) {
                    case 1: // 44-ФЗ
                        switch ($tender_type_id) {
                            case 1: // ЭА
                                $priceId = 4;
                                break;
                            case 2: // ЗК
                                $priceId = 3;
                                break;
                            case 3: // ОК
                                $priceId = 6;
                                break;
                            case 4: // ЗП
                            case 99: // Прочие
                                $priceId = 10;
                                break;
                        }

                        break;
                    case 2: // 223-ФЗ
                    case 3: // Прочие
                        $priceId = 7;
                        break;
                }
                break;
            // ---------------------------------
            case 2: //Банковская гарантия
                // ---------------------------------
                $serviceId = 8;
                $priceId = 27;  // Оформление БГ
                $order_description = ($lot_title !== '' ? $lot_title : $govruid );
                break;

            // ---------------------------------
            case 3: //Продление ЭЦП
                // ---------------------------------
                $serviceId = 1;
                $priceId = 12;  // Оформление ЭЦП
                $order_description = "Оформление ЭЦП";

                break;

            // ---------------------------------
            case 4: //Подготовка жалобы
                // ---------------------------------
                $serviceId = 6;
                $order_description = ($lot_title !== '' ? $lot_title : $govruid );
                switch ($tender_law_id) {
                    case 1: // 44-ФЗ
                        $priceId = 18;  // Жалоба 44-ФЗ
                        break;
                    case 2: // 223-ФЗ
                    case 3: // Прочие
                        $priceId = 19;  // Жалоба прочая
                        break;
                }
                break;
            case 5: // Настройка поискового шаблона
                $serviceId = 4; // поиск
                $priceId = 74;  // Настройка щаблона
                $order_description = 'Настройка поискового шаблона (из Тенмон)';

                break;

            case 6: // Займ на обеспечение заявки
                $serviceId = 15; // поиск
                $priceId = 70;  // Настройка щаблона
                $order_description = 'Тендерный займ (из Тенмон)';

                break;
        }
        // order fields
        // f16220  - idOrderTm
        // f4441 - id client
        // f7071 - num
        // f6591 - date
        // f4451 - id service
        // f13071 - id price_list
        // f4431 - desciption
        // f6551 - order status
        // f12701 - deadline
        // f7301 - comments

        if (!($rowOrder = data_select_array($cb_tables['tableOrders'], 'status=0 and f16220=' . $row_tm['OrderId']))) {
            write_log("No crm order for " . $row_tm['OrderId']." Try to create.");
            // если заказа нет

            // f1056 - inn
            // f1057 - kpp
            // f16230 - idClientTenmon

            $rowClient = [];
            if($tm_user_id > 0) {
                $rowClient = data_select_array($cb_tables['tableClients'], "status=0 and f16230='$tm_user_id'");
            } elseif ($ex_user_id > 0) {
                $rowClient = data_select_array($cb_tables['tableClients'], "status=0 and (f1056='$inn' or (f1056='' and f435='$ex_company_name')");

                if(!$rowClient['id'] && $ex_company_name !=='') {
                    // try to create client
                    $client = [];
                    $client['status'] = 0;
                    $client['f3711'] = ($inn!=='' ? 'Юр.лицо' : 'Физ.лицо'); // вид
                    $client['f772'] = 'Потенциальный'; // тип
                    $client['f435'] = $ex_company_name; // название
                    $client['f11161'] = 'Поставщик'; // категория

                    $client['f552'] = 'Интерес'; // статус лида
                    $client['f4921'] = 'В работе'; // статус клиента

                    $client['f438'] = 640; // Менеджер продаж Шаталова
                    $client['f7811'] = 106; // Менеджер продаж НПО Шаталова
                    $client['f8720'] = 640; // Менеджер клиента Шаталова
                    $client['f9000'] = 106; // Менеджер клиента НПО Шаталова

                    $client['f1056'] = $inn; // инн
                    $client['f439'] = $ex_company_name; // юридическое название

                    $new_client_id = data_insert($cb_tables['tableClients'], $client);
                    if($new_client_id) {
                        $rowClient = data_select_array($cb_tables['tableClients'], "id=$new_client_id");
                    }
                }
            }

            if ($rowClient['id']) {
                write_log("  ... found client id:" . $rowClient['id'] . " name:" . $rowClient['f435']);

                $order_data = array(
                    'status' => '0',

                    'f4451' => $serviceId,                 // тип заказа = "Заявка"
                    'f13071' => $priceId,         // позиция прайса
                    'f6551' => $status_order,      // статус заказа
                    'f4471' => 'Нет',             // согласовано с клиентом

                    'f4431' => $order_description,         // название тендера
                    //'f6621'=>" ",                // папка

                    'f6591' => $tm_order_date,     // дата заказа
                    'f16220' => $tm_order_id,  // MS SQL ID

                    'f4411' => $rowClient['f438'],  // MS SQL ID
                    'f9460' => $rowClient['f7811'],  // MS SQL ID
                    'f6581' => $rowClient['f8720'],  // MS SQL ID
                    'f9470' => $rowClient['f9000'],  // MS SQL ID

                    //'f4461'=>$orderSum,            // стоимость подготовки
                    'f4441' => $rowClient['id']         // клиент
                );

                if ($newOrderId = data_insert($cb_tables['tableOrders'], EVENTS_ENABLE, $order_data)) {
                    $rowNewOrder = data_select_array($cb_tables['tableOrders'], "id=$newOrderId");
                    write_log("  ... created order id:".$rowNewOrder['f7071']);

                    //-----------------------------------------------
                    // добавляем запись в подтаблицу
                    //-----------------------------------------------
                    $service_data = array();
                    $service_data['status'] = 0;
                    $status_production = '10. Оценка стоимости';

                    switch ($order_type_id) {
                        case 1: //Подготовка заявки
                            $service_data['f5471'] = $newOrderId;
                            $service_data['f7441'] = $govruid;
                            $service_data['f8680'] = $lotnumber;
                            $service_data['f6561'] = $status_production;

                            $service_table = $cb_tables['tableOrdersApp'];
                            break;

                        case 2: //Банковская гарантия
                            $service_data['f5751'] = $newOrderId;
                            $service_data['f8150'] = $govruid;
                            $service_data['f8700'] = $lotnumber;

                            $service_data['f10210'] = $contract_cost; // guarantee base
                            $service_table = $cb_tables['tableOrdersBg'];

                            $service_data['f8700'] = $status_production;
                            break;

                        case 3: //Продление ЭЦП
                            $service_data['f5191'] = $newOrderId;
                            $service_data['f6901'] = $status_production;
                            $service_table = $cb_tables['tableOrdersECP'];

                            break;

                        case 4: //Подготовка жалобы
                            $service_data['f5541'] = $newOrderId;
                            $service_data['f8140'] = $govruid;
                            $service_data['f8710'] = $lotnumber;
                            $service_data['f6881'] = $status_production;
                            $service_table = $cb_tables['tableOrdersComplaints'];

                            break;

                        case 5: // Настройка поискового шаблона
                            $service_table = $cb_tables['tableOrdersSearch'];
                            $service_data['f5411'] = $newOrderId;
                            $service_data['f6931'] = $status_production;
                            $service_data['f18741'] = 'Да';

                            break;
                    }

                    if ($service_table > 0) {
                        $newServiceId = data_insert($service_table, EVENTS_ENABLE, $service_data);
                    }

                    //-----------------------------------------------
                    // формируем путь к папке
                    //-----------------------------------------------
                    $data_order = array();
                    $data_order['order_id'] = $newOrderId;
                    $data_order['client_id'] = $rowNewOrder['f4441'];
                    $data_order['order_path'] = $rowNewOrder['f6621'];
                    $data_order['order_date'] = $rowNewOrder['f6591'];
                    $data_order['order_num'] = $rowNewOrder['f7071'];
                    $data_order['order_description'] = $rowNewOrder['f4431'];
                    $data_order['service_num'] = $rowNewOrder['f4451'];
                    $order_path = CreateOrderPath($data_order, $cb_tables, true);

                    //-----------------------------------------------
                    // формируем вехи
                    //-----------------------------------------------
                    CreateOrderMilestones($rowNewOrder, $cb_tables);

                    /*
                     * формуируем задачу по заказу
                     */
                    // CreateOrderTask
                }
            } else {
                write_log("no client tenmon_id:[$tm_user_id] inn:[$inn] kpp:[$kpp] ex_user_id:$ex_user_id ex_name:$ex_company_name");
            }
        } else {
            write_log("Already have order for tm_id:" . $row_tm['OrderId'] . " crm_num:" . $rowOrder['f7071']);
        }

    } // end while

    /* очищаем результирующий набор */
    $result_tm->close();

} // end if

/* закрываем соединение */
$mysqli->close();
