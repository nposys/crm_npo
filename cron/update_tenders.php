<?php

require_once 'add/db.php';
require_once 'add/tables.php';
require_once 'add/functions/service_functions.php';

require_once 'add/functions/common_tender_functions.php';
require_once 'add/functions/common_order_functions.php';

$curdate = date("Y-m-d H:i:s");
$numTenders = 0;
$numTendersChanged = 0;

write_log("start tenders update");
// --------------------------------------------------------------
// setup SQL connection
// --------------------------------------------------------------
ini_set('max_execution_time', 900);
ini_set('mssql.charset','UTF-8');
ini_set('mssql.connect_timeout','120');
ini_set('mssql.timeout','600');

// connect to ms sql
$serverName = $databases["MSSQL"]["server"];
$connectionOptions = array(
    "Database" => $databases["MSSQL"]["db"],
    "Uid" => $databases["MSSQL"]["user"],
    "PWD" => $databases["MSSQL"]["pass"],
    'ReturnDatesAsStrings'=>true
);

//$sqlConnectId = sqlsrv_connect ($databases["MSSQL"]["server"] , $databases["MSSQL"]["user"] , $databases["MSSQL"]["pass"] );
$sqlConnectId = sqlsrv_connect($serverName, $connectionOptions);

if ($sqlConnectId)
{
    // $db = sqlsrv_select_db($databases["MSSQL"]["db"], $sqlConnectId);
    if (true)
    {
      // выбирает все текущие тендеры c govruid (заявка, жалоба, БГ, документация)

        $statuses_in_progress = "('05. Оценка','10. Исполнение', '20. Предоплата') ";

      $strSqlGovRuID = "
          select distinct
            OrdApp.id as SubID,
            381 as SubTable,
            OrdApp.f5471 as OrderID,
            OrdApp.f7441 as GovRuID,
            OrdApp.f8680 as LotNumber
          from
            cb_data381 as OrdApp
          where
            OrdApp.f7701 in $statuses_in_progress
            and OrdApp.status = 0

                union

          select distinct
            OrdCompl.id as SubID,
            391 as SubTable,
            OrdCompl.f5541 as OrderID,
            OrdCompl.f8140 as GovRuID,
            OrdCompl.f8710 as LotNumber
          from
            cb_data391 as OrdCompl
          where
            OrdCompl.f9270 in $statuses_in_progress
            and OrdCompl.status = 0

                union

          select distinct
            OrdBg.id as SubID,
            411 as SubTable,
            OrdBg.f5751 as OrderID,
            OrdBg.f8150 as GovRuID,
            OrdBg.f8700 as LotNumber
          from
            cb_data411 as OrdBg
          where
            OrdBg.f9290 in $statuses_in_progress
            and OrdBg.status = 0

                union

          select distinct
            OrdDoc.id as SubID,
            401 as SubTable,
            OrdDoc.f5621 as OrderID,
            OrdDoc.f13561 as GovRuID,
            OrdDoc.f13571 as LotNumber
          from
            cb_data401 as OrdDoc
          where
            OrdDoc.f9280 in $statuses_in_progress
            and OrdDoc.status = 0
      ";

      $resGovRuID = sql_query($strSqlGovRuID);
      // перебираем все тендеры в активных заказах
      while($rowGovRuID = sql_fetch_array($resGovRuID))
      {
          $OrderID = intval($rowGovRuID['OrderID']);
          $GovRuID = trim($rowGovRuID['GovRuID']);
          $LotNumber = trim($rowGovRuID['LotNumber']);

          if($GovRuID<>"" && $LotNumber<>"")
          {
            $row_tender = data_select_array($cb_tables['tableTenders'],"f5041='$GovRuID' and f6391=$LotNumber");

            // если тендер еще не загружен, пробуем обновить данные
            if($row_tender['f5051']=="")
            {
                $data = array();
                $data['govruid'] = $GovRuID;
                $data['lot_number'] = $LotNumber;

                if(is_numeric($GovRuID)){
                    write_log("try to add tender $GovRuID ($LotNumber)");
                    UpdateTenderData($data, $cb_tables, $databases);
                }else {
                    write_log("will not add tender $GovRuID ($LotNumber)");
                }

            }
          }

        if($OrderID=='' || $OrderID==0 || $GovRuID==0) continue;

        $rowOrder = data_select_array($cb_tables['tableOrders'],"id=$OrderID");
        $OrderReBind = $rowOrder['f13651'];

       // находим этот тендер в CRM
        if($GovRuID<>"" && $LotNumber<>"")
        {
            if($rowTender = data_select_array($cb_tables['tableTenders'], "f5041='".$GovRuID."' and f6391=".$LotNumber." and status=0"))
            {
                $TenderID = $rowTender['id'];
                $strChanges = $rowTender['f8450'];
                $reBind = $rowTender['f14041'];

                $sqlsrv_query = "
                SELECT
                 t.PublicDate, t.OpenDate, t.StartDate, t.ExamDate, ts.StatusName, t.TimeZoneUTC
                FROM
                    tendersUnique as ut
                      left join tenders as t on t.UID=ut.UID
                       left join TenderStatuses as ts on ts.id = t.Status
                WHERE
                   ut.GovRuID = '$GovRuID'
                ";

                $sqlsrv_res = sqlsrv_query($sqlConnectId, $sqlsrv_query);
                while($sql_row = sqlsrv_fetch_array($sqlsrv_res))
                {

                    $bChanged = false;
                    $arr_update = array();

                    $datePublic = substr($sql_row['PublicDate'],0,19);
                    $dateOpen = substr($sql_row['OpenDate'],0,19);
                    $dateExam = substr($sql_row['ExamDate'],0,19);
                    $dateStart = substr($sql_row['StartDate'],0,19);
                    $status_name = $sql_row['StatusName'];

                    // корректируем  даты и время
                    $time_diff = 0;
                    if($sql_row["TimeZoneUTC"]<>"")
                    {
                        $timezone = intval($sql_row["TimeZoneUTC"]);
                        $utcDiff = 7 - intval($sql_row['TimeZoneUTC']);
                        if($utcDiff<>0) if($utcDiff>0) $time_diff = " + ".$utcDiff; else $time_diff = $utcDiff;
                    }
                    $strChanges .= "time_diff:$time_diff\r\n ";

                    //если были изменения, то ставим дату изменений и обновляем данные
                    if ($rowTender['f5081'] <> $datePublic && $datePublic<>"1900-01-01 00:00:00")
                    {
                        // дата публикации
                        $strChanges .= "Изменено ".$curdate." поле PublicDate на ".$datePublic."\r\n";
                        $bChanged = true;
                        $arr_update['f5081']=$datePublic;
                        $arr_update['f14011']=date('Y-m-d H:i:s', strtotime($datePublic.$time_diff." hour")); // НСК
                    }

                    if ($rowTender['f5091'] <> $dateOpen && $dateOpen<>"1900-01-01 00:00:00")
                    {
                        // дата подачи
                        $strChanges .= "Изменено ".$curdate." поле OpenDate на ".$dateOpen."\r\n";
                        $bChanged = true;
                        $arr_update['f5091']=$dateOpen;
                        $arr_update['f13971']=date('Y-m-d H:i:s', strtotime($dateOpen.$time_diff." hour")); // НСК
                    }

                    if ($rowTender['f6371'] <> $dateExam && $dateExam<>"1900-01-01 00:00:00")
                    {
                        // дата рассмотрения
                        $strChanges .= "Изменено ".$curdate." поле ExamDate на ".$dateExam."\r\n";
                        $bChanged = true;
                        $arr_update['f6371']=$dateExam;
                        $arr_update['f14021']=date('Y-m-d H:i:s', strtotime($dateExam.$time_diff." hour")); // НСК
                    }

                    if ($rowTender['f5101'] <> $dateStart && $dateStart<>"1900-01-01 00:00:00")
                    {
                        // дата торгов
                        $strChanges .= "Изменено ".$curdate." поле StartDate на ".$dateStart."\r\n";
                        $bChanged = true;
                        $arr_update['f5101']=$dateStart;
                        $arr_update['f13981']=date('Y-m-d H:i:s', strtotime($dateStart.$time_diff." hour")); // НСК
                    }

                    if ($rowTender['f6381'] <> $status_name )
                    {
                        // статус тендера
                        $strChanges .= "Изменено ".$curdate." поле StatusName на ".$status_name."\r\n";
                        $bChanged = true;
                        $arr_update['f6381']=$status_name;
                    }

                    if ($bChanged)
                    {
                        write_log("tender record govruid:$GovRuID to be changed.");
                        $numTendersChanged++;

                        $arr_update['status']='0';
                        $arr_update['f8450']=$strChanges;
                        $arr_update['f14041']=$reBind+1;

                        // обновляем запись о тендере
                        data_update($cb_tables['tableTenders'], $arr_update, "id=".$TenderID);

                        // обновляем все заказы в которых используется этот тендер (обновит Вехи)
                        data_update($cb_tables['tableOrders'], EVENTS_ENABLE, array('f13651'=>$OrderReBind+1),'id='.$OrderID);

                        // обновляем вехи в заказах
                        CreateOrderMilestones($rowOrder, $cb_tables);
                    }
                    else
                    {
                        // write_log("tender record govruid:$GovRuID no need to be changed.");
                    }

                } // end while mssql fetch

            } // end if row tender

        }  // end  if($GovRuID<>"" && $LotNumber<>"")

        $numTenders++;

      } // end while GovRuID в производстве
    }
    //close the connection
  sqlsrv_close($sqlLink);
}

write_log("end tenders update. processed [$numTenders] records. changed [$numTendersChanged] records.");
?>