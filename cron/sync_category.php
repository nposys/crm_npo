<?php


require_once 'add/db.php';
require_once 'add/functions/service_functions.php';

global $connectionOptions;
global $serverName;
$serverName = $databases["MSSQL"]["server"];
$connectionOptions = array(
	"Database" => $databases["MSSQL"]["db"],
	"Uid" => $databases["MSSQL"]["user"],
	"PWD" => $databases["MSSQL"]["pass"],
	'ReturnDatesAsStrings' => true,
	"CharacterSet" => "UTF-8"
);
global $mysqli;
$mysqli = mysqli_init();
if (!$mysqli->real_connect($databases['TM']['server'], $databases['TM']['user'], $databases['TM']['pass'], $databases['TM']['db'])) {
	die('Ошибка подключения (' . mysqli_connect_errno() . ') '
		. mysqli_connect_error());
}
$mysqli->query("SET NAMES 'utf8'");

function syncCategoriesByLevel($level = 0, $parent_code = '', $parent_id_db = -1, $parent_id_tm = 'NULL')
{
	global $connectionOptions;
	global $serverName;
	global $mysqli;

	$query = "SELECT * FROM srhCategories WHERE Parent=$parent_id_db AND (IsDeleted= 0 OR IsDeleted IS NULL) ORDER BY Sort ASC";
	$sqlConnectId = sqlsrv_connect($serverName, $connectionOptions);
	$resultQuery = sqlsrv_query($sqlConnectId, $query);
	$num = 1;
	while ($result = sqlsrv_fetch_array($resultQuery)) {
		$id_mssql = (int)$result['ID'];
		$title = trim($result['Title']);
		$comment = trim($result['Comment']);
		$is_deleted = (int)$result['IsDeleted'];
		$tree_level = (int)$result['TreeLevel'];

		if ($level === 0) {
			$code = '00';
		} else {
			$code = sprintf("%02d", $num);
			$parent_code !== '' && $parent_code !== '00' ? $code = $parent_code . '.' . $code : '';
		}
		$query_tm = "SELECT * FROM es_Category WHERE mssql_srhCategoryId=$id_mssql";
		$category_id = [];
		if($result_tm = $mysqli->query($query_tm)){
			$category_id = $result_tm->fetch_array(MYSQLI_ASSOC);
			$result_tm->close();
		}

		$query = "SELECT count(*) as CountSubCategories FROM srhCategories WHERE Parent=$id_mssql";
		$sqlConnectId2 = sqlsrv_connect($serverName, $connectionOptions);
		$resultQuery2 = sqlsrv_query($sqlConnectId2, $query);
		$result_count = sqlsrv_fetch_array($resultQuery2);
		$sub_count = (int)$result_count['CountSubCategories'];
		if ((int)$category_id['ID'] > 0) {
			$new_category_id = (int)$category_id['ID'];
			$query_tm = "UPDATE es_Category SET Code='$code',Title = '$title', Description = '$comment', 
			Level =$level ,Parent =$parent_id_tm, mssql_IsDeleted=$is_deleted WHERE ID = $new_category_id";
			$mysqli->query($query_tm);
		}else {$query_tm = "INSERT INTO `tender_monitor`.`es_Category`
							(`Code`,
							`Title`,
							`Description`,
							`Level`,
							`Parent`,
							`mssql_srhCategoryId`,
							`mssql_IsDeleted`)
							VALUES
							('$code',
							'$title',
							'$comment',
							$level,
							$parent_id_tm,
							$id_mssql,
							$is_deleted);
							";
			$mysqli->query($query_tm);
			$new_category_id =$mysqli->insert_id;
		}

		if ($level < 10 && $sub_count > 0) {
			syncCategoriesByLevel($tree_level + 1, $code, $id_mssql, $new_category_id);
		}

		$num++;
	};
}