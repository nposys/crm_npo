<?php

/*
 * временный модуль для загрузки данных клиентов
 */
require_once 'add/db.php';
require_once 'add/tables.php';
require_once 'add/functions/service_functions.php';
require_once 'add/functions/tenmon_functions.php';

$res = sql_query('select * from tm_users');
$num_clients = 0;
while ($row = sql_fetch_assoc($res)) {
    $tenmon_user_id = (int)$row['tenmon_user_id'];
    if($tenmon_user_id >0){
        $client = data_select_array($cb_tables['tableClients'], "f16230 = $tenmon_user_id");
        $client_id = (int)$client['id'];
        $client_name = $client['f435'];
        $client_inn = $client['f1056'];
        if ($client_id > 0) {
            $num_clients ++;

            $has_oder = false;

            $order = data_select_array($cb_tables['tableOrders'], "status=0 and f4441=$client_id
                and f4451=4 and f13071=56 and f6551='10. Исполнение' and f10240='20. Пробный поиск'");
            if((int)$order['id']>0) {
                write_log("tm_id:$tenmon_user_id $client_name already have active trial search");
                $has_oder = true;
            }

            /*
            $orders = data_select($cb_tables['tableOrders'], "status=0 and f4441=$client_id");
            while ($order = sql_fetch_assoc($orders)){
                $order_id  = (int)$order['id'];
                $service_id = (int)$order['f4451'];
                $price_id = (int)$order['f13071'];
                $status_order = $order['f6551'];
                $status_production = $order['f10240'];
                $order_num = $order['f7071'];
                $order_description = $order['f4431'];

                if ($service_id === 4 && $price_id === 56 && $status_order === '10. Исполнение' && $status_production === '20. Пробный поиск') {
                    $has_oder = true;
                }

                if ($status_order !== '30. Завершено' && $status_order !=='98. Отказ') {
                    //write_log("tm_id:$tenmon_user_id crm_id:$client_id crm_name:$client_name inn:$client_inn");
                    //write_log(" ... order_id:$order_id service:$service_id price:$price_id $order_description [$status_order/$status_production]");
                }
            }
            */

            if (!$has_oder) {
                // переменная для передачи в процедуру создания заказа
                $data['date_start'] = date('Y-m-d H:i:s');
                $data['date_end'] = date('Y-m-d H:i:s', strtotime('24.10.2017 23:59:59'));
                $data['date_current'] = date('Y-m-d H:i:s');
                $data['client_id'] = $client_id;
                $data['tariff'] = 2;
                $options['description'] = 'Пробный поиск для опроса Тенмон';

                $data['tm_options']['SearchEnable'] = 1;
                $data['tm_options']['AutoSearchEnable'] = 1;
                $data['tm_options']['ExpertSearchEnable'] = 0;
                $data['tm_options']['AnalyticEnable'] = 1;
                $data['tm_options']['SMSEnable'] = 1;
                $data['tm_options']['IntegrationEnable'] = 1;
                $data['tm_options']['HoldingEnable'] = 0;
                $data['tm_options']['InvitesCount'] = 3;

                $options['paid_search'] = false;
                $result = CreateSearchOrder($data, $cb_tables, $options);

                write_log("result_create_order:$result tm_id:$tenmon_user_id crm_id:$client_id crm_name:$client_name inn:$client_inn");
            }
        }

    }
}
write_log("обработано $num_clients клиентов");

// До 24.10.2017 23:59:59

/*
require_once 'add/db.php';
require_once 'add/tables.php';
require_once 'add/functions/common_clients_functions.php';
require_once 'add/functions/service_functions.php';

ini_set('max_execution_time', 120);
ini_set('mssql.charset','UTF8');
ini_set('mssql.connect_timeout','120');
ini_set('mssql.timeout','600');

$countClientsUpdated = 0;
$countClientsUpdateFailed = 0;

$sqlConnectId = mssql_connect ($databases["MSSQL"]["server"] , $databases["MSSQL"]["user"] , $databases["MSSQL"]["pass"] );
if ($sqlConnectId)
{
    $db = mssql_select_db($databases["MSSQL"]["db"], $sqlConnectId);
    if ($db)
    {
        $resClients = data_select($cb_tables['tableClients'],"status=0 and f435=''");
        while ($rowClient = sql_fetch_assoc($resClients))
        {
            $inn = $rowClient['f1056'];
            $kpp = $rowClient['f1057'];

            $resultCreate = CreateClient($inn, $kpp, $sqlConnectId, $cb_tables, true);

            if($resultCreate==2) $countClientsUpdated++;
            else $countClientsUpdateFailed++;
        }
    }
}

write_log("clients updated:$countClientsUpdated");
write_log("clients update failed:$countClientsUpdateFailed");
*/
