<?php
require_once 'add/vendor/autoload.php';

require_once 'add/db.php';
require_once 'add/tables.php';
require_once 'add/functions/common_clients_functions.php';
require_once 'add/functions/service_functions.php';

use classes\Tables\WorkByOrder;

// ================================================================================
$wbo = new WorkByOrder();

//$wboID = 143113;
//$wbo->setWorkManagerForTask($wboID);

$wbo->updateAverageWorkDurations("work_type", 3);
$wbo->updateAverageWorkDurations("price_type", 5);

die();
// ================================================================================

// connect to ms sql
$serverName = $databases["MSSQL"]["server"];
$connectionOptions = array(
    "Database" => $databases["MSSQL"]["db"],
    "Uid" => $databases["MSSQL"]["user"],
    "PWD" => $databases["MSSQL"]["pass"],
    'ReturnDatesAsStrings' => true,
);
$sqlConnectId = sqlsrv_connect($serverName, $connectionOptions);


// обновляем данные по клиентам у которых устарела статистика
$i = 0;
$res_query = data_select($cb_tables['tableClients'], "status=0 and f20481<'2018-11-17 00:00' and f772='Клиент'");
while($row = sql_fetch_assoc($res_query)){
    $client_id = (int)$row['id'];
    $inn = trim($row['f1056']);

    if($client_id>0 && $inn<> ''){
        UpdateParticipations($client_id, $sqlConnectId, $cb_tables, $dt_start='2017-01-01 00:00:00', false);
    }
    $i++;
}
write_log('clients with old stat $i');

/*
// синхронизуем категории и даты обновлений статистики
$i=0;
$res_query = sql_query(
    "SELECT cl.id, cld.f14501 as catActivity, cld.f14511 as catSize, cld.f14781 as dateSync
          FROM cb_data42 as cl
          LEFT JOIN cb_data630 as cld on cld.f10410 = cl.id
            WHERE
              cld.f14501 <> cl.f4881 -- категория по активности
              or cld.f14511 <> cl.f4891 -- категория по размеру
  ");
while($row = sql_fetch_assoc($res_query)) {
    $client_id = (int)$row['id'];
    $cat_activity = $row['catActivity'];
    $cat_size = $row['catSize'];
    $date_sync = $row['dateSync'];

    $arr_update = [];
    $arr_update['f4881'] = $cat_activity;
    $arr_update['f4891'] = $cat_size;
    $arr_update['f20481'] = $date_sync;

    data_update($cb_tables['tableClients'], $arr_update,"id=$client_id");
    $i++;
}
write_log("not sync companies:$i");
*/

// обновляем статистику тему у кого не проставлена дата обновления данных
$i=0;
$res_query = sql_query(
    "SELECT cl.id 
        FROM cb_data42 as cl 
        LEFT JOIN cb_data630 as cld on cld.f10410 = cl.id 
        WHERE cld.f14781 < '2018-11-16 00:00:00'");
while($row = sql_fetch_assoc($res_query)) {
    $client_id = (int)$row['id'];
    UpdateParticipations($client_id, $sqlConnectId, $cb_tables, $dt_start='2017-01-01 00:00:00', false);
    $i++;
}
// 9739
write_log("clients with old stat:$i");

// добавляем статистику по клиентам у которых ее нет
$i = 0;
$res_query = sql_query(
    "SELECT cl.id 
        FROM cb_data42 as cl 
        LEFT JOIN cb_data630 as cld on cld.f10410 = cl.id 
        WHERE cld.id is null");
while($row = sql_fetch_assoc($res_query)){
    $client_id = (int)$row['id'];
    UpdateParticipations($client_id, $sqlConnectId, $cb_tables, $dt_start='2017-01-01 00:00:00', false);
    $i++;
}
// 2722
write_log("clients without stat:$i");


// обновляем регионы клиентов
$i = 0;
$res_query = data_select ($cb_tables['tableClientsData'], "status=0 and f20461=0");
while($row = sql_fetch_assoc($res_query)){
    $client_data_id = (int)$row['id'];
    $client_id = (int)$row['f10410'];

    if($client_id>0){
        $row_client = data_select_array($cb_tables['tableClients'], "status=0 and id=$client_id");

        $client_inn = $row_client['f1056'];
        $client_region = (int)substr($client_inn, 0, 2);

        if($client_region>0){
            $row_region = data_select_array($cb_tables['tableRegions'], "status=0 and f14381=$client_region");
            $region_name = $row_region['f14401'];
            $region_id = $row_region['id'];

            $arr_update = [];
            $arr_update['f20461'] = $region_id;
            data_update($cb_tables['tableClientsData'], $arr_update,"id=$client_data_id");

            $arr_update = [];
            $arr_update['f19561'] = $region_id;
            data_update($cb_tables['tableClients'], $arr_update,"id=$client_id");

            $i++;
        }

    }
}
write_log("clients without region:".$i);

sqlsrv_close($sqlConnectId);