<?php

require_once 'add/db.php';
require_once 'add/tables.php';
require_once 'add/functions/common_clients_functions.php';
require_once 'add/functions/service_functions.php';

// скрипт обновление статистики участий клиентов из MS SQL
$debug = false;

ini_set('max_execution_time', 3600); // 60 minutes
ini_set('mssql.charset','UTF8');
ini_set('mssql.connect_timeout','120');
ini_set('mssql.timeout','600');

set_time_limit (3600);
if(!is_int($work_duration_minutes)){
    $work_duration_minutes = 1;
}

write_log("work_duration:$work_duration_minutes");

$cur_time = date("Y-m-d H:i:s");
$max_time = date("Y-m-d H:i:s",strtotime($cur_time." +$work_duration_minutes minutes"));
$dt_start = date("Y-01-01 00:00:00", strtotime($cur_time."- 1 year")); // участия за этот и предыдущий
write_log("## Дата начала статистики участий: $dt_start");


// connect to ms sql
$serverName = $databases["MSSQL"]["server"];
$connectionOptions = array(
    "Database" => $databases["MSSQL"]["db"],
    "Uid" => $databases["MSSQL"]["user"],
    "PWD" => $databases["MSSQL"]["pass"],
    //'ReturnDatesAsStrings'=>true,
);
$sqlConnectId = sqlsrv_connect($serverName, $connectionOptions);

$regions = [54]; // $regions = [54, 78, 16];
$count_regions = count($regions);

if ($sqlConnectId) {
        // обновляем клиентов в течение часа
        while($cur_time<$max_time) {
            if($debug) {
                write_log("## $cur_time < $max_time");
            }

            $last_main_region = 54;
            $last_sub_region = 0;

            // считывем результаты последнего запуска скрипта
            $rowServiceJob = data_select_array($cb_tables['tableServiceJobs'],"status=0 and f14871='update_clients'");
            if($rowServiceJob['id']!=="") {
                $last_main_region = (int)$rowServiceJob['f19571'];
                $last_sub_region = (int)$rowServiceJob['f14881'];
            }

            if($last_sub_region==999) {
                // если закончили с текущим регионом, то выбираем следующий основной регион для загрузки
                for ($i = 0; $i < $count_regions; $i++) {
                    if ($regions[$i] === $last_main_region) {
                        $last_main_region = $regions[($i + 1) % $count_regions];
                        break;
                    }
                }
                $last_sub_region = 0;
            }

            $sub_region = $last_sub_region + 1;
            $main_region = $last_main_region;

            $main_region_str = sprintf("%'.02d", $main_region);
            $sub_region_str = sprintf("%'.03d", $sub_region);

            $region = $main_region_str.$sub_region_str."%";
            write_log("## Анализируем регион: $region");

            $count_updates = 0;
            $count_creates = 0;

            // выбираем всех поставщиков за заданный период по заданному региону
            $query = "
                SELECT prot.SupplierINN as INN,prot.SupplierKPP as KPP
                FROM prot_Results as prot LEFT JOIN tenders as t on t.GovRuID = prot.GovRuId 
                  LEFT JOIN Suppliers as sup on sup.ID = prot.SupplierID
                WHERE t.CreateDate > CAST('".$dt_start."' as datetime2) and prot.SupplierINN like '".$region."' 
                GROUP BY  prot.SupplierINN, prot.SupplierKPP
                  UNION
                SELECT prot.INN as INN,prot.KPP as KPP
                FROM ProtocolsEF as prot LEFT JOIN tenders as t on t.GovRuID = prot.GovRuId 
                    LEFT JOIN Suppliers as sup on sup.ID = prot.SuppliersID
                WHERE t.CreateDate > CAST('".$dt_start."' as datetime2) and prot.INN like '".$region."' 
                GROUP BY  prot.INN, prot.KPP
                  UNION
                SELECT prot.INN as INN,prot.KPP as KPP
                FROM ProtocolsZK as prot LEFT JOIN tenders as t on t.GovRuID = prot.GovRuId 
                    LEFT JOIN Suppliers as sup on sup.ID = prot.SuppliersID
                WHERE t.CreateDate > CAST('".$dt_start."' as datetime2) and prot.INN like '".$region."' 
                GROUP BY prot.INN, prot.KPP
                    UNION
                SELECT prot.INN as INN,prot.KPP as KPP
                FROM ProtocolsOK as prot LEFT JOIN tenders as t on t.GovRuID = prot.GovRuId 
                    LEFT JOIN Suppliers as sup on sup.ID = prot.SuppliersID
                WHERE t.CreateDate > CAST('".$dt_start."' as datetime2) and prot.INN like '".$region."' 
                GROUP BY prot.INN, prot.KPP
                    UNION
                SELECT prot.INN as INN,prot.KPP as KPP
                FROM ProtocolsPO as prot LEFT JOIN tenders as t on t.GovRuID = prot.GovRuId 
                    LEFT JOIN Suppliers as sup on sup.ID = prot.SuppliersID
                WHERE t.CreateDate > CAST('".$dt_start."' as datetime2) and prot.INN like '".$region."' 
                GROUP BY prot.INN, prot.KPP
                ORDER BY INN
            ";

            //execute the SQL query and return records
            $resultQuery = sqlsrv_query($sqlConnectId, $query, [], [ "Scrollable" => 'static' ]);
            $num_rows = sqlsrv_num_rows($resultQuery);

            if($debug){
                write_log("sql: $query");
            }
            write_log("num_rows:$num_rows");

            $companies = [];
            if($resultQuery) {
                while ($rowSQL = sqlsrv_fetch_array($resultQuery)) {
                    $inn = trim($rowSQL['INN']);
                    $kpp = trim($rowSQL['KPP']);
                    if($kpp!=-'') {
                        $companies[$inn][$kpp] = 1;
                    } else {
                        $companies[$inn][0] = '';
                    }
                }
            }

            foreach($companies as $inn => $company_data){
                if($inn != "") {

                    $kpps = '';
                    $kpp = '';
                    foreach ($company_data as $next_kpp => $dummy){
                        if($kpp == '' && $next_kpp>0 && $next_kpp!==''){
                            $kpp = $next_kpp;
                        }
                        $kpps .= $next_kpp." ";
                    }

                    // создаем или обновляем клиента
                    $resultCreate = CreateClient($inn, $kpp, $sqlConnectId, $cb_tables, true, $dt_start, $debug);
                    switch($resultCreate) {
                        case 1:
                            $debug ? write_log("Created client inn:$inn kpp:$kpp [result=$resultCreate]") : '';
                            $count_creates++;
                            break;
                        case 2:
                            $debug ? write_log("Updated client inn:$inn kpp:$kpp [result=$resultCreate]") : '';
                            $count_updates++;
                            break;
                        default:
                            break;
                    }
                }
                $i++;
            }

            $cur_date = date("Y-m-d H:i:s");
            if($rowServiceJob['id']<>"") {
                data_update($cb_tables['tableServiceJobs'], array("f19571" => $main_region, "f14881" => $sub_region, "f14891" => $cur_date), "id=" . $rowServiceJob['id']);
            } else {
                data_insert($cb_tables['tableServiceJobs'], array("f14871" => "update_clients", "f19571" => $main_region,  "f14881" => $sub_region, "f14891" => $cur_date));
            }

            $cur_time = date("Y-m-d H:i:s");
            write_log("processed region:$region created_clients:$count_creates updated_clients:$count_updates");
        } // end while (cur_time<max_time)
} // end if sqlLink
sqlsrv_close($sqlConnectId);
