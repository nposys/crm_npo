<?php
/**
 * Created by PhpStorm.
 * User: Alt
 * Date: 10.02.2020
 * Time: 22:32
 */

require_once 'add/vendor/autoload.php';

require_once 'add/db.php';
require_once 'add/tables.php';
require_once 'add/functions/service_functions.php';
require_once 'add/functions/common_order_functions.php';

use classes\Tables\WorkByOrder;

// выбираем настройки из справочника скриптов
$default_managers = data_select_array(1271, "status=0 and f21231='import_regtorg'");
$default_manager_production = data_select_array(46, "status=0 and id=" . $default_managers['f21241']);
$default_manager_production_user = $default_manager_production['f1400'];

$mode = "NT"; // RT / NT
write_log("mode: $mode");

// сопоставление клиентов CRM RT/NT => CRM NPO
$internalClientsMark = [
    'РЕГ' => 3813,      // Регионторг
    'СГР' => 48212,     // ССГрупп
    'НРГ' => 15105,     // Новоторг
    'ПРС' => 49571,      // ПраймСкин
    'ЮНИ' => 50411,      // ЮнионИнвест
    'ИПК' => 0,      // ИП Кричевская ОИ
];

/* подключаемся к базе CRM RT */
$mode == "RT" ? $config_db = "CRM_RT" : $config_db = "CRM_NT";

$mysqli = mysqli_init();

if (!$mysqli->real_connect(
    $databases[$config_db]['server'],
    $databases[$config_db]['user'],
    $databases[$config_db]['pass'],
    $databases[$config_db]['db'])) {
    die('Ошибка подключения (' . mysqli_connect_errno() . ') '
        . mysqli_connect_error());
}
$mysqli->query("SET NAMES 'utf8'");

// выборка тендеров в CRM RT для экспорта в CRM НПО для создания заказов на заявки
$field_msCLientLotId = "f7690";
if($mode=="RT"){
    // сопоставление клиентов CRM RT => CRM NPO
    $internalClients = [
        0 => 3813,  // по-умолчания Регионторг
        1 => 3813,  // Регионторг
        2 => 48212, // ССГрупп
        3 => 0      // ИП Игнатов
    ];

    $field_internal_company_id = "f10970";
    $field_order_date = "f10130";
    $field_pgEClientLot = "f10730";
    $field_company_mark = "f11301";
    $string_id_crm_rt = "";
}else{
    // сопоставление клиентов CRM RT => CRM NPO
    $internalClients = [
        0 => 15105,  // по-умолчания Новоторг
        1 => 15105,  // Новоторг
        2 => 0, // ИП Кричевская
        3 => 0,      // нет
        4 => 15105,      // ЮнионИнвест
        5 => 49571      // Прайм Скин
    ];

    $field_internal_company_id = "f10780";
    $field_order_date = "f11130";
    $field_pgEClientLot = "f11180";
    $field_company_mark = "f10890";
    $string_id_crm_rt = ", t.f11820 as id_crm_rt";
}

$query = "
  SELECT 
    t.id,
    t.$field_internal_company_id as internal_company_id,
    t.f1270 as order_name,
    t.f290 as govruid,
    t.f8700 as lotnumber,
    t.f5910 as law,
    p.f460 as manager,
    t.$field_order_date as order_date,
    t.f5920 as order_folder,
    t.$field_msCLientLotId as ms_CientsLotID,
    t.$field_pgEClientLot as pg_eClientLotId,
    t.$field_company_mark as company_mark
     $string_id_crm_rt
  FROM 
    cb_data40 as t
      left join cb_data60 as p on t.f1240 = p.id
  WHERE
    t.status = 0
    and t.f950 IN ('20. Подготовка')
 ";

$count = 0;
$count_new = 0;
$count_update = 0;

$arrOrders = [];
$wbo = new WorkByOrder();

if ($result_query = $mysqli->query($query)) {
    while ($row = $result_query->fetch_array(MYSQLI_ASSOC)) {

        $internalCompanyId = (int)$row['internal_company_id'];
        $govruid = trim($row['govruid']);
        $lotumber = (int)($row['lotnumber']);
        $lotumber == 0 ? $lotumber = 1 : "";

        if($mode == "RT"){
            $idTenderCrmRt = (int)$row['id'];
            $idTenderCrmNt = 0;
        }else{
            $idTenderCrmRt = (int)$row['id_crm_rt'];
            $idTenderCrmNt = (int)$row['id'];
        }

        $ms_CientsLotID = (int)$row['ms_CientsLotID'];
        $pg_eClientLotId = (int)$row['pg_eClientLotId'];
        $company_mark = $row['company_mark'];

        // определяем клиента
        $clientId = (int)$internalClientsMark[$company_mark];
        if($clientId == 0){
            $clientId = $internalClients[$internalCompanyId];
        }

        // 2. проверяем каждую заявку не создан ли уже для нее заказ в КБ
        $rowOrder = [];

        if($idTenderCrmNt>0) {
            // 2.1 проверка по CRM NT Тенедры ID
            $rowOrder = data_select_array($cb_tables['tableOrders'],
                " status = 0 and " .
                " f4441 = ".$clientId." and " .
                " f24851 = ".$idTenderCrmNt
            );
        }

        if((int)$rowOrder['id'] == 0 && $idTenderCrmRt>0) {
            // 2.1.1 проверка по CRM RT Тенедры ID
            $rowOrder = data_select_array($cb_tables['tableOrders'],
                " status = 0 and " .
                " f4441 = ".$clientId." and " .
                " f23341 = ".$idTenderCrmRt
            );
        }

        if ((int)$rowOrder['id'] == 0 && $pg_eClientLotId > 0) {
            // 2.2 проверка по Postgresql ID
            $rowOrder = data_select_array($cb_tables['tableOrders'],
                " status = 0 and " .
                " f4441 = ".$clientId." and " .
                " f23971 = ".$pg_eClientLotId
            );
        }

       if ((int)$rowOrder['id'] == 0 && $ms_CientsLotID > 0){
            // 2.3 проверка по MS SQL id
           $rowOrder = data_select_array($cb_tables['tableOrders'],
               " status = 0 and " .
               " f4441 = ".$clientId." and " .
               " f23981 = ".$ms_CientsLotID
           );
        }

        if ($rowOrder['id'] == "") {
            // Если нет такого заказа, то создаем его в КБ

            if ($row['law'] == '44-ФЗ') {
                $orderSum = 600; // 44-ФЗ по 400 рублей (с 01.05.2018 по 600)
                $pricePosId = 51; // "Заявка [РТ] обычная"
            } else {
                $orderSum = 3000; // 223-ФЗ и прочие по 2000 рублей (с 01.05.2018 по 3000)
                $pricePosId = 57; // "Заявка [РТ] коммерческая"
            }

            $cur_date = date('Y-m-d');
            if ($cur_date >= '2021-01-01') {
                // цена заказа
                if ($row['law'] == '44-ФЗ') {
                    $orderSum = 720; // 44-ФЗ по 400 рублей, с 01.05.2018 по 600, с 01.01.2021 па 700
                } else {
                    $orderSum = 3600; // 223-ФЗ и прочие по 2000 рублей, с 01.05.2018 по 3000, с 01.01.2021 по 3500
                }
            }

            $userId = 1;        // admin
            $serviceId = 5;     // тип заказа = "Заявка"

            // ------------------------------------------------------------------------
            // добавляем новую запись заказа
            $arrOrder = [
                'status' => '0',
                'f4451' => $serviceId,                  // тип заказа = "Заявка"
                'f13071' => $pricePosId,                // позиция прайса = "Заявка [РТ]"
                'f6551' => '10. Исполнение',            // статус заказа
                'f4471' => 'Да',                        // согласовано с клиентом
                'f4431' => $row['order_name'],          // название тендера
                'f6621' => $row['PatchFolder'],
                'f7301' => $row['manager'],             // примечание
                'f6591' => $row['order_date'],         // дата заказа
                'f9010' => "-" . $default_manager_production_user . "-",
                'f23981' => $ms_CientsLotID, // idSQL = ClientsLotID из MS SQL
                'f23971' => $pg_eClientLotId, // eClientLotId = eClientLod из Postgresql
                'f23341' => $idTenderCrmRt,                 // id тендера из CRM RT
                'f24851' => $idTenderCrmNt,                // id тендера из CRM NT
                'f23621' => $row['order_folder'],       // внешняя папка
                'f4461' => $orderSum,                   // стоимость подготовки заявки регионторг
                'f4441' => $clientId                    // клиент = регионторг
            ];
            $orderId = (int)data_insert($cb_tables['tableOrders'], EVENTS_ENABLE, $arrOrder);

            if ($orderId > 0) {
                // если заказ создан, то создаем папку заказа
                $row_new_order = data_select_array($cb_tables['tableOrders'], "id=$orderId");
                $row_client = data_select_array($cb_tables['tableClients'], "id=$clientId");

                $orderNum = $row_new_order['f7071'];

                // заполняем массив для создания заказа
                $data = [];
                $data['user_id'] = $userId;
                $data['order_id'] = $orderId;
                $data['order_path'] = "";
                $data['order_date'] = $row['order_date'];
                $data['order_num'] = $orderNum;
                $data['order_description'] = $row['order_name'];
                $data['service_num'] = $serviceId;
                $data['client_id'] = $clientId;
                $data['client_path_folder'] = $row_client['f10080'];

                CreateOrderPath($data, $cb_tables, true);
            }

            // ------------------------------------------------------------------------
            // добавляем дополнительную информацию в таблицу "Заказы дополнительное"
            $arrOrderAdd = [
                "status" => 0,
                "f10620" => $orderId,
                "f10670" => 1,
                "f10720" => $row['clientslotsid'],// MS SQL ID
                // "f10730" => $row['PlanDate']
            ];
            data_insert(640, $arrOrderAdd);

            $arrOrderApp = [
                'status' => '0',
                'f7441' => "$govruid",
                'f8680' => $lotumber,
                'f5491' => 'Нет', // срочная
                'f8110' => 'Нет', // подбор материалов
                'f8120' => 'Да',  // подача
                'f8130' => 'Да',  // торги
                'f6561' => '20. Заполнение'   // статус производства
            ];

            $orderAppId = (int)data_select_field($cb_tables['tableOrdersApp'], "id", " status=0 and f5471=" . $orderId);
            if ($orderAppId > 0) {
                // обновляем строку
                data_update($cb_tables['tableOrdersApp'], EVENTS_ENABLE, $arrOrderApp, " id=" . $orderAppId);
            } else {
                // ------------------------------------------------------------------------
                // добавляем данные в таблицу Производство / Заявка
                $arrOrderApp['f5471'] = $orderId;     // связь с "Заказы"
                $orderAppId = data_insert($cb_tables['tableOrdersApp'], EVENTS_ENABLE, $arrOrderApp);
            }

            // ------------------------------------------------------------------------
            // создаем задачу на менеджера производства
            $dateStart = date("Y-m-d H:i:s");
            $dateFinish = date("Y-m-d H:i:s", strtotime($dateStart . " + 10 min"));
            $arrJob = [
                "status" => 0,
                "f12601" => $orderId, // заказ
                "f12621" => $clientId,                   // клиент = регионторг
                "f12611" => 5,                     // тип заказа = "Заявка"
                "f12951" => $row['order_name'],         // название тендера
                "f12661" => $default_manager_production_user,    // менеджер
                "f13591" => 10,                 // трудоемкость
                "f12981" => $dateStart,  // дата начала план
                "f12991" => $dateFinish,  // дата начала план
                "f14741" => "Обычная",    // важность
                "f12861" => 4, // тип работы - оформление заявки
                "f12671" => "Начать работу по заказу", // описание
                "f12631" => "10. Запланировано" //статус
            ];
            $jobId = data_insert($cb_tables['tableOrderJobs'], EVENTS_ENABLE, $arrJob);

            // меняем менеджера на авто-менеджера в зависимости от доступности и текущей загрузке
            if($jobId>0){
                $dynamic_production_user_id = $wbo->chooseNextWorkManager($jobId);
                if($dynamic_production_user_id>0){
                    write_log(" for task:$jobId dynamic_user:$dynamic_production_user_id");
                    data_update(
                        $cb_tables['tableOrderJobs'],
                        ["f12661"=>$dynamic_production_user_id],
                        "id=$jobId");
                }
            }


            $count_new++;
        } else {
            // если заказ уже существует, обновляем данные о связке с заказом CRM RT
            if ($rowOrder['f23341'] == 0) {
                data_update(
                    $cb_tables['tableOrders'],
                    EVENTS_ENABLE,
                    ['f23341' => $row['id']],
                    'id = ' . $rowOrder['id']
                );
                $count_update++;
            }

            $rowApp = data_select_array($cb_tables['tableOrdersApp'], "status=0 and f5471=" . $rowOrder['id']);
            if ($rowApp['id'] > 0 && $rowApp['f7441'] == "") {
                data_update(
                    $cb_tables['tableOrdersApp'],
                    EVENTS_ENABLE,
                    [
                        'f7441' => "$govruid",
                        'f8680' => $lotumber,
                        'f5491' => 'Нет', // срочная
                        'f8110' => 'Нет', // подбор материалов
                        'f8120' => 'Да',  // подача
                        'f8130' => 'Да',  // торги
                        'f6561' => '20. Заполнение'   // статус производства
                    ],
                    'id = ' . $rowApp['id']
                );
                write_log("updated govruid for $govruid");
            }
        }

        $count++;
    }
}

write_log("[ЗАЯВКИ] Всего обработано $count записей. Добавлено $count_new заказов. Обновлено $count_update заказов.");

// =======================================================================================================
// обновление статусов заказов по заявкам в CRM НПО по результатам перевода статусов в CRM RT
// также завершает заказы

// теперь готовим массив для всех клиентов
$internalClients = [
    1 => 3813,  // Регионторг
    2 => 48212, // ССГрупп
    3 => 15105, // Новоторг
    4 => 49571, // Прайм Скин
    5 => 50411, // ЮнионИнвест
];

// получаем в CRM НПО все заказы незавершенные заказы, созданные на Группу РТ
$internalClientsString = implode(",", $internalClients);
$res_crm_npo = data_select($cb_tables['tableOrders'],
    " f4441 IN (".$internalClientsString.") and " .         // все внутренние клиенты
    " (f23981>0 or f23971>0 or f23341>0) and " .          // ms_ClientsLotID или pg_eClientLotId или idTenderCrmRt
    " f4451 = 5 and " .          // подготовка заявки
    " status = 0 and " .
    " ((f6551 in ('10. Исполнение','20. Предоплата')) or (f6551='30. Завершено' and f10240='50. На подаче'))");

// f6551 - статус заказа
// f10240 - статус производства

while ($row_order = sql_fetch_assoc($res_crm_npo)) {
    $row_order_app = data_select_array($cb_tables['tableOrdersApp'], "f5471=", $row_order['id']);

    $id_crm_nt = (int)$row_order['f24851'];
    $id_crm_rt = (int)$row_order['f23341'];
    $id_ms_sql = (int)$row_order['f23981'];
    $id_pg_sql = (int)$row_order['f23971'];

    if($mode == "RT"){
        $main_condition = " ($id_crm_rt>0 and t.id = $id_crm_rt) or ";
        $aux_condition = " ";
        $sub_condition = " t.id = $id_crm_rt ";
    }else{
        $main_condition = " ($id_crm_nt>0 and t.id = $id_crm_nt) or ";
        $aux_condition = " ($id_crm_rt>0 and t.f11820 = $id_crm_rt) or ";
        $sub_condition = " t.id = $id_crm_nt ";
    }

    // получаем статус из CRM RT по
    $query = "
          SELECT 
            t.f950 as status_name
          FROM 
            cb_data40 as t
          WHERE
            t.status = 0
            and (
                $main_condition
                $aux_condition
                    
                ($id_ms_sql>0 and $sub_condition) or 
                ($id_ms_sql>0 and t.$field_msCLientLotId = $id_ms_sql) or
                 
                ($id_pg_sql>0 and $sub_condition) or 
                ($id_pg_sql>0 and t.$field_pgEClientLot = $id_pg_sql)                 
                )
    ";

    if ($result_query = $mysqli->query($query)) {
        if ($row = $result_query->fetch_array(MYSQLI_ASSOC)) {

            $count_update++;
            switch ($row['status_name']) {
                case "30. Победа":
                    updateCrmOrders ($cb_tables, $row_order['id'], "30. Завершено", "70. Победа");
                    break;
                case "35. Поражение":
                    updateCrmOrders ($cb_tables, $row_order['id'], "30. Завершено", "80. Поражение");
                    break;
                case "40. Итоги не подведены":
                    updateCrmOrders ($cb_tables, $row_order['id'], "30. Завершено", "95. Итоги не подведены");
                    break;
                case "70. Заявка отклонена":
                    updateCrmOrders ($cb_tables, $row_order['id'], "30. Завершено", "90. Отклонено");
                    break;
                case "90. Отказ":
                    updateCrmOrders ($cb_tables, $row_order['id'], "98. Отказ");
                    break;
                default:
                    $count_update--;
                    break;
            } // end switch status name
        }
    }
}
write_log("[СТАТУСЫ] Всего обновлено статусов $count_update.");

/* закрываем соединение */
$mysqli->close();


// =============================================================================================
// Вспомогательные функции
// =============================================================================================

/*
 Функция для обновления статусов в CRM НПО
*/
function updateCrmOrders ($cb_tables, $orderId, $statsOrder = "", $statusProduction = "")
{
    if ((int)$orderId>0) {
        if($statsOrder !== "") {
            data_update($cb_tables['tableOrders'], EVENTS_ENABLE,
                ['status' => '0', 'f6551' => $statsOrder], "id=" . $orderId);
        }
        if($statusProduction != "") {
            data_update($cb_tables['tableOrdersApp'], EVENTS_ENABLE,
                ['status' => '0', 'f6561' => $statusProduction], "f5471=" . $orderId);
            data_update($cb_tables['tableOrders'], EVENTS_ENABLE,
                ['status' => '0', 'f10240' => $statusProduction], "id=" . $orderId);
        }
    }
}
