<?php

require_once 'add/tables.php';
require_once 'add/functions/service_functions.php';
require_once 'add/functions/common_other_functions.php';

// автосоздание нового заказа поиска для Отдела поставок
if(date("Y-m-d") >= '2022-01-31'){
    $idSearchCompany = 50411; // ЮнионИнвест, ООО
    $nameSearchOrder = "ЮНИПС";
}else{
    $idSearchCompany = 3813; // Регионторг, ООО
    $nameSearchOrder = "Регионторг";
}

$eventsCreated = 0;
$ordersDisabled = 0;

// перевод в завершено заказов по поиску, а также создание события менеджеру на продление поиска
$res_orders_search = data_select($cb_tables['tableOrdersSearch'],
    "status=0 and f9260='10. Исполнение'");
while($row_orders_search = sql_fetch_assoc($res_orders_search)) {
    $id_order_search = $row_orders_search['id'];
    $id_order = $row_orders_search['f5411'];
    $id_manager_client = (int)$row_orders_search['f9090']>0 ? $row_orders_search['f9090'] : $row_orders_search['f9080'];
    $id_company = $row_orders_search['f10190'];
    $company_result = data_select($cb_tables['tableClients'],"status=0 and id = ".$id_company);
    $company = sql_fetch_assoc($company_result);

    if(($company['f772']=='Потенциальный')){
        $id_manager_client = (int)$company['f438'] >0 ? $company['f438'] : $company['f8720'];
    }else if(($company['f772']=='Клиент')) {
	    $id_manager_client = (int)$company['f8720'] >0 ? $company['f8720'] : $company['f438'];
    }

    // сверяем по дате без учета времени
    $date_to = date("Y-m-d", strtotime($row_orders_search['f6951']));
    $date_now = date("Y-m-d");
    $date_prolongation = date("Y-m-d", strtotime($date_to." -10 days "));
    $date_prolongation_month = date("Y-m-d", strtotime($date_to." -33 days "));

    if($id_order>0){
        $row_order = data_select_array($cb_tables['tableOrders'],"id=$id_order");

        if($date_to<$date_now){
            // отключаем поиск
            data_update($cb_tables['tableOrdersSearch'],array("f9260"=>"30. Завершено"),"id=$id_order_search");
            data_update($cb_tables['tableOrders'],array("f6551"=>"30. Завершено"),"id=$id_order");
            $ordersDisabled++;
        }

        // создание события менеджеру на продление поиска
        if($date_now>=$date_prolongation_month) {
            $taskText = "Уточнить у клиента необходимость продления поиска и/или доступа в Тенмон (автосоздано)";
            $managerId = GetActiveManagerForEvent($id_manager_client, $taskText);
            if ($managerId > 0) {
                // получаем данные о клиенте
                $companyName = "";
                if ($date_now < $date_prolongation) {
                    $taskText = "Уточнить у клиента необходимость продления поиска и/или доступа в Тенмон (автосоздано/за месяц)";
                }
                // проверяем нет ли уже события на это заказ
                if ($id_company <> $idSearchCompany && !CheckEventExistForCondition
                    ($id_company, $taskText, "idSearch", $id_order_search)) {
                    if ($id_company > 0) {
                        $rowCompany = data_select_array($cb_tables['tableClients'], "id=$id_company");
                        $clientStatus = $rowCompany['f4921'];
                        $companyName = $rowCompany['f435'];
                    }
                    // создаем событие менеджеру о продлении поиска
                    $event_title = "$taskText: $companyName до $date_to [idSearch:$id_order_search]";
                    $event_time = GetNearestWorkingTime("", 3);

                    $event = [];
                    $event['status'] = 0;
                    $event['f724'] = $event_time;
                    $event['f723'] = $id_company;
                    $event['f727'] = $managerId;
                    $event['f773'] = 'Звонок';
                    $event['f725'] = $event_title;
                    $event['f1053'] = 'Нет'; // выполнено

                    $new_event_id = data_insert($cb_tables['tableEvents'], EVENTS_ENABLE, $event);
                    if($new_event_id > 0){
                        $eventsCreated++;
                    }
                }
            }
        }

    }
} // end while order_search
write_log ("search orders disabled: $ordersDisabled");
write_log ("search prolongation events created: $eventsCreated");

// ######################################################################################
// создание нового заказа на поиск на Отдел Поставок на текущий месяц
$row_search_rt = data_select_array($cb_tables['tableOrdersSearch'],
    "status=0 and f9260='10. Исполнение' and f10190=$idSearchCompany AND id!=1624"); //1624 - id бесконечного заказа
if($row_search_rt['id']==""){
    // получаем последнюю запись заказа на поиск Отдела Поставок
    $row_orders_search = data_select_array($cb_tables['tableOrdersSearch'],
        "status=0 and f10190=$idSearchCompany ORDER BY add_time DESC");

    $id_order_search = $row_orders_search['id'];
    $id_order = $row_orders_search['f5411'];
    $id_manager_client = $row_orders_search['f9090'];
    $id_company = $row_orders_search['f10190'];
    $date_from = $row_orders_search['f6941'];
    $date_from = date( "Y-m-d", strtotime( "$date_from +1 month" ) );
    $date_now = date("Y-m-d H:i:s");
    $monthNow = date("m-Y");

    if($id_order>0){
        // получаем сведения о заказе
        $row_order = data_select_array($cb_tables['tableOrders'],"id=$id_order");

        $dateStart = date("Y-m-d  00:00:00",strtotime("first day of $date_from"));
        $dateEnd = date("Y-m-d",strtotime("last day of $date_from"))." 23:59:59";
        $monthName = date("Y-m",strtotime($dateStart));

        $orderSum = ($dateStart <= '2020-12-31' ? 5000 : 12000);
        $orderTitle = "Поиск $nameSearchOrder $monthName";

        $order = array();
        $order['status'] = '0';
        $order['f4451'] = $row_order['f4451'];        // тип заказа  =  "Поиск"
        $order['f13071'] = $row_order['f13071'];      // позиция прайса = "Доступ Тенмон (платный)"
        $order['f6551'] = '10. Исполнение';      // статус заказа
        $order['f10240'] = $row_order['f10240'];  // статус производства
        $order['f4471'] = 'Да';                  // согласовано с клиентом
        $order['f4431'] = $orderTitle;         // название тендера
        $order['f6621'] = ""; // PathFolder
        $order['f16360'] = "Нет";// приостановлено
        $order['f12871'] = "Обычный";// приоритет

        $order['f4411'] = $row_order['f4411']; // manager sales
        $order['f6581'] = $row_order['f6581']; // manager client
        $order['f6581'] = $row_order['f6581']; // manager production

        $order['f7301'] = "";   // примечание
        $order['f6591'] = $date_now;     // дата заказа
        $order['f12701'] = $dateEnd; // deadline

        $order['f4461'] = $orderSum;            // стоимость
        $order['f4441'] = $idSearchCompany;     // клиент

        $newOrderId = (int)data_insert($cb_tables['tableOrders'],EVENTS_ENABLE, $order);

        if($newOrderId>0){
            $row_order_search_old = data_select_array($cb_tables['tableOrdersSearch'],"f5411=$newOrderId");
            $order_search = array();
            $order_search['f5411'] = $newOrderId; // order
            $order_search['f10190'] = $idSearchCompany; // client
            $order_search['f10230'] = $orderSum; // сумма

            $order_search['f6941'] = $dateStart; // дата начала
            $order_search['f6951'] = $dateEnd; // дата окончания
            $order_search['f5421'] = $orderTitle; // примечание

            $order_search['f9260'] = "10. Исполнение"; // статус заказа
            $order_search['f6931'] = $row_orders_search["f6931"]; // статус производства

            $order_search['f16870'] = $row_orders_search["f16870"]; // tenmon tariff конструктор
            $order_search['f16880'] = $row_orders_search["f16880"]; // ручной поиск
            $order_search['f16890'] = $row_orders_search["f16890"]; // автоматический шаблонный поиск
            $order_search['f16900'] = $row_orders_search["f16900"]; // экспертный поиск
            $order_search['f16840'] = $row_orders_search["f16840"]; // кол-во пользователей
            $order_search['f16850'] = $row_orders_search["f16850"]; // интеграция с ЭТП
            $order_search['f16860'] = $row_orders_search["f16860"]; // уведомления

            $order_search['f17190'] = $row_orders_search["f17190"]; // Группы компаний
            $order_search['f19621'] = $row_orders_search["f19621"]; // Выгрузка закупок в Excel
            $order_search['f19631'] = $row_orders_search["f19631"]; // Выгрузка результатов аналитики в Excel
            $order_search['f19641'] = $row_orders_search["f19641"]; // Лимит строк для выгрузки результатов аналитики в Excel
            $order_search['f19651'] = $row_orders_search["f19651"]; // Аналитика по произвольным запросам
            $order_search['f19661'] = $row_orders_search["f19661"]; // Экспертный прогноз снижения на торгах
            $order_search['f19671'] = $row_orders_search["f19671"]; // Автоматические уведомления о сменах статуса выбранных тендеров
            $order_search['f19681'] = $row_orders_search["f19681"]; // Экспертный анализ рисков по заказчикам
            $order_search['f18691'] = $row_orders_search["f18691"]; // Экспертный анализ рисков по заказчикам

            if(isset($row_order_search_old['id'])){
                data_update($cb_tables['tableOrdersSearch'],EVENTS_ENABLE, $order_search,'id = '.$row_order_search_old['id']);
            }else{
                $rowNewOrderSearch = data_insert($cb_tables['tableOrdersSearch'], EVENTS_ENABLE, $order_search);
            }
        }
    }
}
