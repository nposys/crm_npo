<?php
/**
 * Created by PhpStorm.
 * User: Alt
 * Date: 20.09.2017
 * Time: 15:27
 */

require_once 'add/db.php';
require_once 'add/tables.php';
require_once 'add/functions/service_functions.php';
require_once 'add/functions/common_order_functions.php';
require_once 'add/functions/common_clients_functions.php';
require_once 'add/functions/tenmon_functions.php';

// служебные клиенты которых пропускать
$filter_tenmon_id = [1, 93, 989];

// получаем максимальную (последнюю) дату счета импортированного из тенмона
$res_data = data_select_field(43, "MAX(f436) as max_time", "f18701<>''"); // and f18701<>''
//$res_data = data_select_field(43, "MAX(f436) as max_time", "status=0  and f18701<>''"); // and f18701<>''
$max_data = sql_fetch_assoc($res_data);
// если еще не было счетов из тенмона
if ($max_data['max_time'] === '') {
    $max_invoice_date = '2017-08-01 00:00:00';
} else {
    $max_invoice_date = $max_data['max_time'];
}

$res_tm = GetTenmonInvoices ($max_invoice_date." -2");// -2 из-за проблемы с часовыми поясами!

$tm_invoices = json_decode($res_tm['data']);

$num_invoices_processed = 0;
$num_invoices_added = 0;
foreach ($tm_invoices as $tm_invoice) {
    $tm_client_id = (int)$tm_invoice->UserId;

    if (in_array($tm_client_id, $filter_tenmon_id)) {
        continue;
    }

    $tm_invoice_id = (int)$tm_invoice->PaymentInvoiceId;
    $tm_invoice_sum = (float)$tm_invoice->Summary;
    $tm_date_create = $tm_invoice->DateCreate;
    $tm_is_deleted = (int)$tm_invoice->isDeleted;
    $tm_is_paid = (int)$tm_invoice->isPaid;
    $tm_invoice_number = $tm_invoice->PaymentInvoiceNumber;
    $tm_tariff = (int)$tm_invoice->TariffId;

    $tm_product = json_decode($tm_invoice->Product, true);
    $tm_date_end = date('Y-m-d 23:59:59', strtotime($tm_product['EndDate']));

    $tm_is_deleted ? $prefix1 = '---' : $prefix1 = '';
    $tm_is_paid ? $prefix2 = '+++' : $prefix2 = '';

    if ($tm_client_id>0) {
        $client = data_select_array($cb_tables['tableClients'], "status=0 and f16230=$tm_client_id");
        write_log($prefix1.$prefix2
            .'client_id:'.$tm_client_id
            ." name:".$client['f435']
            ." num:$tm_invoice_number"
            ." invoice_id:$tm_invoice_id"
            ." date:$tm_date_create"
            ." sum:$tm_invoice_sum"
            ." is_deleted:$tm_is_deleted"
            ." is_paid:$tm_is_paid"
        );

        // если найден клиент в CRM
        if ((int)$client['id']>0) {

            // проверяем наличие счета
            $crm_invoice = data_select_array ($cb_tables['tableInvoices'], "status=0 and f18701=$tm_invoice_id");

            $price_position = GetPriceIdByTenmonTariff($tm_tariff, false);
            write_log ("tm_tariff:$tm_tariff price_position:$price_position");

            // если нет счета, то создаем его
            if ((int)$crm_invoice['id'] === 0) {

                // создаем заказ сперва
                $order['client_id'] = $client['id'];
                $order['date_start'] = date('Y-m-d H:i:s', strtotime($tm_date_create));
                $order['date_end'] = date('Y-m-d H:i:s', strtotime($tm_date_end));
                $order['date_current'] = date('Y-m-d H:i:s');
                $order['tariff'] = $tm_tariff;
                $order['sum'] = $tm_invoice_sum;
                $order['tm_options'] = $tm_product;
                $new_order_id = CreateSearchOrder($order, $cb_tables, ['paid_search'=>true]);
                write_log("[+order] new order: $new_order_id");

                // если создался заказ, создаем счет
                if ((int)$new_order_id>0){
                    // далее создаем счет
                    $invoice = [];
                    $invoice['status'] = 0;
                    //$invoice['f437'] = $client['id']; // номер = автонумерация
                    $invoice['f436'] = $tm_date_create; // дата
                    $invoice['f18711'] = $tm_invoice_number; // номер счета ТМ
                    $invoice['f839'] = $client['id'];   // клиент
                    $invoice['f18701'] = $tm_invoice_id;   // клиент

                    if ((int)$client['f8720']>0) {
                        $invoice['f448'] = (int)$client['f8720']; // пользователь - менеджер
                    };

                    $invoice['f10810'] = date('Y-m-d H:i:s', strtotime($tm_date_create." + 7 days")); // прогноз оплаты

                    $invoice['f2071'] = $tm_invoice_sum; // сумма счета
                    $invoice['f654'] = 1; // наша компания = ООО НПО "Система"
                    $invoice['f1083'] = 'счет из Тенмон'; // сумма счета
                    $invoice['f9720'] = 'Нет'; // ручная обработка

                    $new_invoice_id = data_insert($cb_tables['tableInvoices'], EVENTS_ENABLE, $invoice);
                    write_log ("[+invoice] created new invoice id:$new_invoice_id ");

                    // если создался счет, то создаем позицию счета
                    if ((int)$new_invoice_id>0) {
                        $invoice_position = [];
                        $invoice_position ['f807'] = $new_invoice_id; // счет
                        $invoice_position ['f7061'] = $new_order_id; // заказ
                        $invoice_position ['f810'] = $tm_invoice_sum; // выставляемая сумма
                        $new_invoice_order = data_insert ($cb_tables['tableInvoicesOrders'], EVENTS_ENABLE, $invoice_position);
                        write_log ("[+invoice_order] created new invoice order id:$new_invoice_order ");
                        $num_invoices_added++;
                    }
                }

            } else {
                write_log ("[exists] invoice_id:$tm_invoice_id already exists in CRM");
            }
        } else {
            // создаем клиента и после этого создаем счет и прочее ?
        }
    }
    $num_invoices_processed++;
}

// проставляем признак оплаты в Тенмон
// 1. получаем счета из CRM которые из тенмона
// 2. проверяем оплаты по CRM
// 3. передаем сведения об оплате
// 4. помечаем счет как обработанный
$invoices = [];
$num_payment_updates = 0;
$res_invoices = data_select($cb_tables['tableInvoices'], "status=0 and f18711<>'' and f9710 <>'Да'");
while ($row_invoice = sql_fetch_assoc($res_invoices)) {
    $invoice = new \StdClass();

    $invoice_id = $row_invoice['id'];
    $invoice_sum = (float)$row_invoice['f2071'];
    $invoice_tm_id = $row_invoice['f18701'];
    $invoice_tm_num = $row_invoice['f18711'];

    $sum_total_paid = 0.0;
    $res_incomes_invoices = data_select($cb_tables['tableIncomesInvoices'], "status=0 and f8270=$invoice_id");
    while ($row_income_invoice = sql_fetch_assoc($res_incomes_invoices)) {
        $sum_paid_accepted = (float)$row_income_invoice['f8300'];
        $sum_total_paid += $sum_paid_accepted;
    }

    if ($sum_total_paid === $invoice_sum) {
        $invoice->paymentInvoiceId = (int)$invoice_tm_id;
        $invoice->setPaid = true;
        $invoices[] = $invoice;
        data_update($cb_tables['tableInvoices'], ['f9710'=>'Да'],"id=$invoice_id");
        $num_payment_updates++;
    }

    write_log("invoice_tm_id:$invoice_tm_id invoice_id:$invoice_id inovice_sum:$invoice_sum invoice_paid:$sum_total_paid");
}

if (count($invoices)>0) {
    $res = ChangePaymentInvoice ($invoices);
    write_log ("ChangePaymentInvoice result:".print_r($res,true));
}

write_log("  дата последнего счета из ТМ:".$max_invoice_date);
write_log("  обработано счетов из ТМ:".$num_invoices_processed);
write_log("  добавлено счетов в CRM:".$num_invoices_added);
write_log("  проставлено оплат в TM:".$num_payment_updates);