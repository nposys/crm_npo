<?php
/**
 * Created by PhpStorm.
 * User: miheev
 * Date: 31.10.2018
 * Time: 10:04
 */

$total=0;

$relatedTableQuery = "SELECT 
                            link_table_id,
                            link_field_id
                        FROM
                            cb_subtables
                        WHERE
                            table_id = 42";
$relatedTables=[];
$resultRelatedTable = sql_query($relatedTableQuery);
while ($relatedTable = sql_fetch_assoc($resultRelatedTable)) {
    $relatedTables[]=$relatedTable;
}

$duplicateSearchQuery = "SELECT 
                            *
                        FROM
                            cb_data42 AS c
                        WHERE
                            status = 0
                                AND c.f772 = 'Потенциальный'
                                AND c.f438 = 0
                                AND c.f8720 = 0
                                AND c.f6961 = 0
                                AND c.f1056 != ''
                                AND EXISTS( SELECT 
                                    id
                                FROM
                                    cb_data42 AS c2
                                WHERE
                                    c.id != c2.id AND c.f1056 = c2.f1056
                                        AND status = 0
                                        AND c.f772 = 'Потенциальный'
                                        AND c2.f438 = 0
                                        AND c2.f8720 = 0
                                        AND c2.f6961 = 0)
                        GROUP BY c.f1056";


$result = sql_query($duplicateSearchQuery);
while ($inn = sql_fetch_assoc($result)) {

    $firstContragent = [];
    $contactInfo = '';
    $kpp = '';
    $duplicateInnQuery = "SELECT 
                                *
                            FROM
                                cb_data42 AS c
                            WHERE
                                c.f772 = 'Потенциальный'
                                    AND status = 0
                                    AND c.f438 = 0
                                    AND c.f8720 = 0
                                    AND c.f6961 = 0
                                    AND c.f1056 = '" . $inn["f1056"] . "'
                            ORDER BY c.id";
    $resultDuplicate = sql_query($duplicateInnQuery);
    while ($contragent = sql_fetch_assoc($resultDuplicate)) {
        if (empty($firstContragent)) {
            $firstContragent = $contragent;
            $contactInfo = $firstContragent["f14821"];
        } else {
            foreach ($firstContragent as $key => $value) {
                if ($key == "f14821" &&
                    !empty($contragent[$key]) &&
                    stripos($contactInfo, $contragent[$key]) === false &&
                    stripos($contragent[$key], "Контактная информация не найдена") === false) {
                    if (empty($contactInfo)) {
                        $contactInfo = $contragent[$key];
                    } else {
                        $contactInfo .= "; " . $contragent[$key];
                    }
                } else if ($key == "f1057" &&
                    !empty($contragent[$key]) &&
                    $value != $contragent[$key]) {
                    if (empty($kpp)) {
                        $kpp = $contragent[$key];
                    } else {
                        $kpp .= ", " . $contragent[$key];
                    }
                } else if (empty($value) || $value == '0000-00-00 00:00:00') {
                    $firstContragent[$key] = $contragent[$key];
                }
            }
            foreach ($relatedTables as $relatedTable) {
                data_update(
                    (int)$relatedTable["link_table_id"],
                    EVENTS_ENABLE,
                    array("f" . $relatedTable["link_field_id"] => (int)$firstContragent["id"]),
                    "f" . $relatedTable["link_field_id"] . "=" . $contragent["id"]
                );
            }
            data_update(42, EVENTS_ENABLE, array("status" => 2), "id=" . $contragent["id"]);
        }
    }
    if (!empty($kpp)) {
        $contactInfo = "Дополнительные КПП:$kpp; $contactInfo";
    }
    $firstContragent["f14821"] = $contactInfo;
    $firstContragentId =  $firstContragent["id"];
    unset($firstContragent["id"]);
    data_update(42, EVENTS_ENABLE, $firstContragent, "id=" . $firstContragentId);
    $total++;
}
echo "обработано дубликатов:$total";