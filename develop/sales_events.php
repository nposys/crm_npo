<?php 
// отчет для отображения событий по менеджерам продаж
require_once 'add/tables.php';
require_once 'add/functions/service_functions.php';

// примерная оценка трудоемкости событий
$workloads = array(
    "Звонок"=>10
    ,"Письмо"=>5
    ,"Встреча"=>120
    ,"Создание заказа"=>15
    ,"Выставление счета"=>10
    ,"Получение оплаты"=>0
    ,"Подача заявки"=>15
    ,"Торги"=>30

);
// =============================================================================
// обрабатываем фильтры на входе
if ($_REQUEST['date1']) $date1 = date("d.m.Y",strtotime(form_eng_time($_REQUEST['date1'])));
else $date1 = date("d.m.Y", strtotime("monday this week"));

if ($_REQUEST['date2']) $date2 = date("d.m.Y",strtotime(form_eng_time($_REQUEST['date2'])));
else $date2 = date("d.m.Y", strtotime("friday this week"));

$date1_fet = form_eng_time($date1.' 00:00:00');
$date2_fet = form_eng_time($date2.' 23:59:59');

if ($_REQUEST['_type'] || $_REQUEST['_manager'] || $_REQUEST['_dates'])
{
    reset_filters(62);
    if ($_REQUEST['_dates']) set_filter(724, "period", $date1." 00:00:00", $date2." 23:59:59");
    if ($_REQUEST['_manager']) set_filter(727, "=", intval($_REQUEST['_manager']));
    if ($_REQUEST['_type']) set_filter(773, "=", form_input($_REQUEST['_type']));
    set_filter(1053, "=", "Да");
    header("Location: ".$config["site_root"]."/fields.php?table=62");
}

if ($_REQUEST['manager']) $manager = intval($_REQUEST['manager']); elseif ($user['group_id']!=1) $manager = $user['id'];

// формируем селектбокс по выбору менеджеров
if ($user['group_id']==1 || $user['group_id']==791)
    $sel_manager = "<option value=''>Все</option>\r\n";
    $result = sql_query("SELECT DISTINCT `user`.`id`, `user`.`fio` FROM `".USERS_TABLE."` AS `user`, `".GROUPS_TABLE."` AS `group` WHERE `user`.`arc`=0 AND `user`.`group_id`!='777'");
    while ($row = sql_fetch_assoc($result))  $sel_manager.= "<option value='".$row['id']."'".(($row['id']==$manager)?" selected":"").">".$row['fio']."</option>\r\n";

    if($manager) $userCond = " and pers.f1400=".$manager;
    if($date1 && $date2) $dateCond = " and events.f724>='".$date1_fet."' and events.f724<='".$date2_fet."'";

// =============================================================================
    
// -----------------------------------------
// заполняем все типы событий
// -----------------------------------------
$field = sql_select_array(FIELDS_TABLE, "id=773");
$event_types  = explode(PHP_EOL,$field["type_value"]);
foreach($event_types as &$event_type) $event_type = trim($event_type);

// -----------------------------------------
// заполняем массив с сотрудниками
// -----------------------------------------
$managers = array();
$i = 0;
$sqlPersonal = "SELECT pers.*, user.id as user_id FROM ".DATA_TABLE.$cb_tables['tablePersonal']." as pers JOIN ".USERS_TABLE." as user ON user.login=pers.f1410 WHERE pers.status=0 and pers.f7941='Да' ";
$resPersonal = sql_query($sqlPersonal);
while($rowPersonal = sql_fetch_assoc($resPersonal ))
{
    $managers[$i]['id'] = $rowPersonal['id'];
    $managers[$i]['name'] = $rowPersonal['f6631'];
    $managers[$i]['user'] = $rowPersonal['f1410'];
    $managers[$i]['user_id'] = $rowPersonal['user_id']; // 1 = менеджер продаж, 6 == менеджер по работе с клиентами
    $managers[$i]['position'] = $rowPersonal['f484']; // 1 = менеджер продаж, 6 == менеджер по работе с клиентами
    $i++;
}

$managers_sales = array();
foreach($managers as $next_manager) if($manager['position']==1) $managers_sales[] = $next_manager;
$managers_sales[] = array("id"=>0,"name"=>"","user"=>"","user_id"=>"","position"=>"");

$managers_clients = array();
foreach($managers as $next_manager) if($manager['position']==6) $managers_clients[] = $next_manager;
$managers_clients[] = array("id"=>0,"name"=>"","user"=>"","user_id"=>"","position"=>"");

// -----------------------------------------------------
// получаем данные по планам и фактам сборов
// -----------------------------------------------------
$date_month = date("Y-m-d", strtotime($date1_fet.' first day of this month'));
$resSalesPlans = data_select($cb_tables['tableSalesPlans'],"status=0 and f13281='".$date_month."'");
while($rowSalesPlans = sql_fetch_assoc($resSalesPlans))
{
    $manager_id = $rowSalesPlans['f13321'];
    $manager_user = $rowSalesPlans['f13331'];
    $sales_type = $rowSalesPlans['f15181']; // первичные, вторичные
    $service_type = $rowSalesPlans['f13801']; // обычные, комплексны
    $sum_plan = $rowSalesPlans['f13311'];
    $sum_fact = $rowSalesPlans['f13341'];
    $plan_date = $rowSalesPlans['f13281'];
    
    
    $next_manager_name = "";
    foreach($managers as $next_manager) if($next_manager['id']==$manager_id){ $next_manager_name = $next_manager['name']; break; }
    // $line['f13351'] // percent
    
    $plans[$next_manager_name]['plan'] = $sum_plan;
    $plans[$next_manager_name]['fact'] = $sum_fact;

    //$plans[$next_manager_name]['plan'] = 50000;
    //$plans[$next_manager_name]['fact'] = 40000;
    $plans[$next_manager_name]['percent'] = $plans[$next_manager_name]['fact'] / $plans[$next_manager_name]['plan'];
    
}

//print_r($plans);
// -----------------------------------------------------
// данные по собятиям
// -----------------------------------------------------
$events = array();

$dates = array();
$cur_date = date("Y-m-d", strtotime($date1));
$fin_date = date("Y-m-d", strtotime($date2));
while ($cur_date<=$fin_date)
{
    $dates[$cur_date] = 1;
    $cur_date = date("Y-m-d", strtotime($cur_date." +1 day"));
}

// группируем по плановой дате
if($date1 && $date2) $dateCond = " and events.f724>='".$date1_fet."' and events.f724<='".$date2_fet."'";
foreach ($managers as $next_manager)
{
    if(($manager == 0 || $manager==$next_manager['user_id']) && $next_manager['position']==1)
    {
        foreach ($event_types as $event_type)
        {
            $event_type = trim($event_type);
            // звонки f1053 = поле "Выполнено", f4761 = поле "Результативно"
            $sqlQuery = "
            SELECT
                DATE(events.f724) as event_date, pers.f6631, events.f773, count(events.id) as events_num, pers.f1400 as user
            FROM
                ".DATA_TABLE.$cb_tables['tableEvents']." as events
                  JOIN ".DATA_TABLE.$cb_tables['tablePersonal']." as pers on pers.id=events.f7821
            WHERE
                events.f1053='Да' and events.f4761='Да' and events.f773='".$event_type."' and events.status=0 and events.f7821=".$next_manager['id'].$userCond." ".$dateCond." 
               
            GROUP BY
                DATE(events.f724), events.f773, pers.id, pers.f1400
      ";
            $resSQL = sql_query($sqlQuery);
            while($rowSQL = sql_fetch_array($resSQL))
            {
                $events[$next_manager['name']][$event_type][$rowSQL['event_date']]['plan'] = $rowSQL['events_num'];
            }
        }
    }
}

// группируем по фактической дате изменения события
if($date1 && $date2) $dateCond = " and events.f11100>='".$date1_fet."' and events.f11100<='".$date2_fet."'";
foreach ($managers as $next_manager)
{
    if(($manager == 0 || $manager==$next_manager['user_id']) && $next_manager['position']==1)
    {
        foreach ($event_types as $event_type)
        {
            $event_type = trim($event_type);
            // звонки f1053 = поле "Выполнено", f4761 = поле "Результативно"
            $sqlQuery = "
            SELECT
                DATE(events.f11100) as event_date, pers.f6631, events.f773, count(events.id) as events_num, pers.f1400 as user
            FROM
                ".DATA_TABLE.$cb_tables['tableEvents']." as events
                  JOIN ".DATA_TABLE.$cb_tables['tablePersonal']." as pers on pers.id=events.f7821
            WHERE
                events.f1053='Да' and events.f4761='Да' and events.f773='".$event_type."' and events.status=0 and events.f7821=".$next_manager['id'].$userCond." ".$dateCond."
                
            GROUP BY
                DATE(events.f11100), events.f773, pers.id, pers.f1400
            ";
            $resSQL = sql_query($sqlQuery);
            while($rowSQL = sql_fetch_array($resSQL))
            {
                $events[$next_manager['name']][$event_type][$rowSQL['event_date']]['fact'] = $rowSQL['events_num'];
            }
        }
        
    }
}

// считаем итоговый счет по дням
$manager_position = 1;
$service_type = 'Обычные';

foreach ($managers as $next_manager)
{
    if(($manager == 0 || $manager==$next_manager['user_id']) && $next_manager['position']==1)
    {
        $cur_date = date("Y-m-d", strtotime($date1));
        $fin_date = date("Y-m-d", strtotime($date2." +0 day"));
        do
        {
            $score_plan = 0;
            $score_fact = 0;

            foreach ($event_types as $event_type)
            {
                $score_plan += $events[$next_manager['name']][$event_type][$cur_date]['plan']*$workloads[$event_type];
                $score_fact += $events[$next_manager['name']][$event_type][$cur_date]['fact']*$workloads[$event_type];
            }

            // -----------------------------------------------------------------------
            // вычисляем поступления
            // -----------------------------------------------------------------------
            $sum_income_day = 0;
            
            if($manager_position==1) $manager_field = "f11251";
            if($manager_position==6) $manager_field = "f11261";
            
            if($manager_field<>"")
            {
                $pre_filter = "";
                if($service_type=='Обычные') $pre_filter = "not";
            
                $res = data_select_field($cb_tables['tableIncomeInvoiceOrders'], "SUM(f10920) AS sum", "status=0 and ".$manager_field."=".$next_manager['user_id']." and f11271='".$cur_date."' and f11741 ".$pre_filter." in (7)");
                $row = sql_fetch_assoc($res);
                $sum_income_day = $row['sum'];
            }            
            $events[$next_manager['name']]["Поступления"][$cur_date]['fact'] = $sum_income_day;
            
            $events[$next_manager['name']]["СЧЕТ"][$cur_date]['plan'] = floatval($score_plan) / 60;
            $events[$next_manager['name']]["СЧЕТ"][$cur_date]['fact'] = floatval($score_fact) / 60;

            $cur_date = date("Y-m-d", strtotime($cur_date." +1 day"));
        }
        while ($cur_date<=$fin_date);
    }
}


// ----------------------------------------------
// Переносим переменные в отображение
$smarty->assign("dates", $dates);
$smarty->assign("events", $events);
$smarty->assign("plans", $plans);

$smarty->assign("date1", $date1);
$smarty->assign("date2", $date2);
$smarty->assign("data", $data_m);
$smarty->assign("manager", $manager);
$smarty->assign("sel_manager", $sel_manager);
$smarty->assign("user_group", $user['group_id']);
?>