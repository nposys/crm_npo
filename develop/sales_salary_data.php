<?php 

require_once 'add/tables.php';
require_once 'add/functions/service_functions.php';


// Здесь подготовливаем данные для вывода в отчете
$tableIncomes = 511;
$tableIncomeInvoiceOrders = 650;
$tableOrders = 271;
$tableOrdersWork = 751;
$tableOrdersSearch = 371;
$tableClients = 42;
$tableClientsData = 630;
$tablePersonal = 46;
$tableServiceTypes = 91;
$tableSalesPlans = 771;

// ----------------------------------------------------
// редирект на страницы
$line_id = $_REQUEST['_line_id'];
if($line_id)
{
    switch($_REQUEST['_type'])
    {
        case "заказ":
            header("Location: ".$config["site_root"]."/view_line2.php?table=".$tableOrders."&filter=1891&line=".$line_id."&back_url=".$base64_current_url);
            break;
        case "поступление":
            header("Location: ".$config["site_root"]."/view_line2.php?table=".$tableIncomes."&line=".$line_id."&back_url=".$base64_current_url);
            break;
    }
}

// ----------------------------------------------------
// проверяем права и устанавливаем фильтр по менеджерам
$managerFilter = "";
$managerFilterCommon = "";
$bShowTotals = false;
$bShowSales = false;
$bShowClients = false;
$bShowProductionSearch = false;
$bShowProductionOrders = false;

// =========================================================
// обработка входных фильтров
if($user['group_id']==1) 
{
  // для админов полный просмотр
  $managerFilter = "";
  $bShowTotals = true;  
  $bShowSales = true;
  $bShowClients = true;
  $bShowProductionSearch = true;
  $bShowProductionOrders = true;
}
else
{
  if($user['id']>0)
  {
    $rowPersTemp = data_select_array($tablePersonal, "status=0 and f1400=".$user['id']);
    
    switch($rowPersTemp['f484'])
    {
      case 1:
       //set manager sales
        $managerFilter = " AND tiio.f11251=".$user['id']." "; 
        $managerFilterCommon = " AND user.id=".$user['id']." "; 
        $bShowSales = true;
        break;
      case 6:
        //set manager clients
        $managerFilter = " AND tiio.f11261=".$user['id']." "; 
        $managerFilterCommon = " AND user.id=".$user['id']." "; 
        $bShowClients = true;
        break;
      case 2:
        // менеджер по тендерам
        //$bShowProductionOrders = true;
        $managerFilter = " AND tow.f12661=".$user['id']." "; 
        $managerFilterCommon = " AND user.id=".$user['id']." "; 
        break;
      case 15:
        // менеджер-аналитик
        $bShowProductionSearch = true;
        $managerFilterCommon = " AND user.id=".$user['id']." "; 
        //$managerFilter = " AND tord.f9010='-".$user['id']."-' "; 
        break;
    }
  }    
} 

// =========================================================
// селектбокс по месяцам
$dt_period = $_REQUEST['dt_period'];

$from = new DateTime(date('Y').'-01-01');
$to   = new DateTime(date('Y-m-d'));

$period = new DatePeriod($from, new DateInterval('P1M'), $to);
$arrayOfDates = array_map(
    function($item){return $item->format('Y-m-d');},
    iterator_to_array($period)
    );

if($dt_period)
{
    $date1 = $dt_period;
    $date2 = new DateTime($dt_period);
    $date2->modify('last day of this month');
    $date2 = $date2->format('Y-m-d');
} else {
    $date1 = new DateTime('now');
    $date1->modify('first day of this month');
    $date1 = $date1->format('Y-m-d');
    $date2 = new DateTime('now');
    $date2->modify('last day of this month');
    $date2 = $date2->format('Y-m-d');
    $dt_period = $date1;
}

$period = array(
    'date1' => $date1,
    'date2' => $date2
);

// =========================================================
$ordertypes_groups = array("Обычные", "Комплексные");
$incometypes = array("Первичные", "Вторичные");

// =============================================================================
// данные системы мотивации
$motivation = array(
        "Обычные"=> array(
               0 => array("border"=>50000,"rate"=>0.10),
               1 => array("border"=>100000,"rate"=>0.12),
               2 => array("border"=>200000,"rate"=>0.13),
               3 => array("border"=>400000,"rate"=>0.14),
               4 => array("border"=>800000,"rate"=>0.15),
               5 => array("border"=>10000000,"rate"=>0.16)
           ),
        "Комплексные"=> array(
               0 => array("border"=>100000,"rate"=>0.05),
               1 => array("border"=>200000,"rate"=>0.07),
               2 => array("border"=>400000,"rate"=>0.08),
               3 => array("border"=>800000,"rate"=>0.09),
               4 => array("border"=>1600000,"rate"=>0.10),
               5 => array("border"=>10000000,"rate"=>0.11)
           )
     );

$motivation_datediff = array(
        "Обычные"=> array(
               0 => array("border"=>7,"rate"=>1.05),
               1 => array("border"=>30,"rate"=>1.00),
               2 => array("border"=>60, "rate"=>0.95),
               3 => array("border"=>180,"rate"=>0.85),
               4 => array("border"=>365,"rate"=>0.75),
               5 => array("border"=>10000,"rate"=>0.5)
             ),
        "Комплексные"=> array(        
               0 => array("border"=>30,"rate"=>1.00),
               1 => array("border"=>90,"rate"=>0.95),
               2 => array("border"=>180, "rate"=>0.90),
               3 => array("border"=>365,"rate"=>0.85),
               4 => array("border"=>10000,"rate"=>0.70)
             ),
        "Партнер"=> array(
               0 => array("border"=>1,"rate"=>1.00),
               1 => array("border"=>3,"rate"=>0.95),
               2 => array("border"=>6,"rate"=>0.85),
               3 => array("border"=>1000, "rate"=>0.70)
             )
    );

$motivation_plans = array(
        0 => array("border"=>1.00, "rate"=>1.00),
        1 => array("border"=>0.75, "rate"=>0.75),
        2 => array("border"=>0.50, "rate"=>0.50),
        3 => array("border"=>0.00, "rate"=>0.00)        
  );
  
$motivaion_sales = array("Обычные"=>0.12,"Комплексные"=>0.06);

$totals = array();

// -----------------------------------------
// заполняем массив с сотрудниками
// -----------------------------------------
$managers = array();
$managers_sales = array();
$managers_clients = array();

$i = 0;
$sqlPersonal = "SELECT
                    tpers.*, user.id as user_id 
                FROM ".DATA_TABLE.$cb_tables['tablePersonal']." as tpers 
                        JOIN ".USERS_TABLE." as user ON user.login=tpers.f1410 
                        
                WHERE 
                    tpers.status=0 $managerFilterCommon
                    and tpers.f1400 in (
                        SELECT tiio.f11261 from ".DATA_TABLE.$tableIncomeInvoiceOrders." as tiio WHERE DATE(tiio.f11271) BETWEEN '".$date1."' AND '".$date2."'
                            UNION
                        SELECT tiio.f11251 from ".DATA_TABLE.$tableIncomeInvoiceOrders." as tiio WHERE DATE(tiio.f11271) BETWEEN '".$date1."' AND '".$date2."'
                            )
                    
                "; //and pers.f7941='Да' 

$resPersonal = sql_query($sqlPersonal);
while($rowPersonal = sql_fetch_assoc($resPersonal))
{
    //echo "next_user:".$rowPersonal['user_id']."<br>";
    $managers[$i]['id'] = $rowPersonal['id'];
    $managers[$i]['name'] = $rowPersonal['f6631'];
    $managers[$i]['work_now'] = $rowPersonal['f7941'];
    $managers[$i]['base_salary'] = $rowPersonal['f487'];
    $managers[$i]['date_start'] = $rowPersonal['f489'];
    $managers[$i]['date_trial'] = $rowPersonal['f14461'];
    $managers[$i]['date_quit'] = $rowPersonal['f14051'];
    $managers[$i]['user'] = $rowPersonal['f1410'];
    $managers[$i]['user_id'] = $rowPersonal['user_id']; 
    $managers[$i]['position'] = $rowPersonal['f484']; // 1 = менеджер продаж, 6 == менеджер по работе с клиентами
    $managers[$i]['fullname'] = trim($rowPersonal['f483']);
    
    switch($managers[$i]['position'])
    {
        case 1:
            $managers_sales[] = $managers[$i];
            break;
        case 6: 
            $managers_clients[] = $managers[$i];
            $data_cl[$managers[$i]['fullname']]['service_groups']["Обычные"]['common'] = 1;
            $data_cl[$managers[$i]['fullname']]['service_groups']["Комплексные"]['common'] =1;
            $data_cl[$managers[$i]['fullname']]['user'] = $managers[$i]['user_id'];
            $data_cl[$managers[$i]['fullname']]['position'] = $managers[$i]['position'];
        break;
    }
    
    $i++;
}

$managers_sales[] = array("id"=>0,"name"=>"","user"=>"","user_id"=>"","position"=>"");
$managers_clients[] = array("id"=>0,"name"=>"","user"=>"","user_id"=>"","position"=>"");

// -------------------------------------------------------
// подготовка данных по менеджерам клиентов
if($bShowClients)
{

  // выборка из таблицы заказы счета поступления по менеджерам клиентов
  // f11261 - manager clients
  $sqlQuery = "SELECT 
                  tiio.id as Id
                 ,tcmp.id as ClientId
                 ,tinc.id as IncomeId
                 ,tinc.f7511 as IncomeNum
                 ,tord.id as OrderId
                 ,tord.f7071 as OrderNum
                 ,tst.f1158 as ServiceType
                 ,tord.f4431 as OrderDescription
                 ,tiio.f10920 as PaymentSum
                 ,DATE(tiio.f11271) as PaymentDate
                 ,DATE(tiio.f11291) as InvoiceDate
                 ,DATE(tiio.f11281) as OrderDate
                 ,DATE(tord.f13361) as OrderFinishDate
                 ,tcmp.f435 as ClientName
                 ,tsrc.f435 as SourceName
                 ,tsrc.f772 as SourceType
                 ,tpers.f483 as ManagerName
                 ,tpers.f484 as ManagerPosition
                 ,tiio.f11261 as ManagerUser
               FROM 
                 ".DATA_TABLE.$tableIncomeInvoiceOrders." as tiio
                 LEFT JOIN ".DATA_TABLE.$tableOrders." as tord ON tord.id = tiio.f10890 AND tord.status = 0
                 LEFT JOIN ".DATA_TABLE.$tableServiceTypes." as tst ON tst.id = tord.f4451 AND tst.status = 0
                 LEFT JOIN ".DATA_TABLE.$tableClients." as tcmp ON tcmp.id = tord.f4441 AND tcmp.status = 0
                 LEFT JOIN ".DATA_TABLE.$tableIncomes." as tinc ON tinc.id = tiio.f10910 AND tinc.status = 0
                 LEFT JOIN ".DATA_TABLE.$tableClients." as tsrc ON tsrc.id = tinc.f7531 AND tsrc.status = 0
                 LEFT JOIN ".DATA_TABLE.$tablePersonal." as tpers ON tpers.f1400 = tiio.f11261 AND tpers.status = 0 AND tiio.f11261 <> '' 
               WHERE 
                 DATE(tiio.f11271) BETWEEN '".$date1."' AND '".$date2."'
                 AND tiio.status = 0
                 ".$managerFilter."
               ORDER BY 
                  tiio.f11261, tiio.f11271
                "; // AND tpers.f7941='Да'
  
  $data = array();
  $sum_common = 0;
  $sum_complex = 0;
  
  $res = sql_query($sqlQuery);
  while($row = sql_fetch_array($res))
  {
    if($row['ServiceType']<>"Подготовка документации")
    {
       $ordertype_group = "Обычные";
       $sum_common += $row['PaymentSum'];
    }
    else
    {
       $ordertype_group = "Комплексные";
       $sum_complex += $row['PaymentSum'];
    }
        
    $manager = $row['ManagerName'];
    $orderId = $row['Id'];
    $datePayment = $row['PaymentDate'];
    
    $data_cl[$manager]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['order_id'] = $row['OrderId'];
    $data_cl[$manager]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['order_num'] = $row['OrderNum'];
    $data_cl[$manager]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['income_id'] = $row['IncomeId'];
    $data_cl[$manager]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['income_num'] = $row['IncomeNum'];
    $data_cl[$manager]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['order_id'] = $row['OrderId'];
    $data_cl[$manager]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['order_num'] = $row['OrderNum'];
    $data_cl[$manager]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['client_id'] = $row['ClientId'];
    $data_cl[$manager]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['client'] = $row['ClientName'];
    $data_cl[$manager]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['source_name'] = $row['SourceName'];
    $data_cl[$manager]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['source_type'] = $row['SourceType'];
    $data_cl[$manager]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['service'] = $row['ServiceType'];
    $data_cl[$manager]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['description'] = mb_substr($row['OrderDescription'],0,40,'UTF-8');
    $data_cl[$manager]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['sum'] = $row['PaymentSum'];
    $data_cl[$manager]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['date_payment'] = $row['PaymentDate'];
    $data_cl[$manager]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['date_order'] = $row['OrderDate'];
    $data_cl[$manager]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['date_invoice'] = $row['InvoiceDate'];
    $data_cl[$manager]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['date_order_finish'] = $row['OrderFinishDate'];
      
    $ts1 = strtotime($row['PaymentDate']);
    if($row['ServiceType']<>"Подготовка документации") $ts2 = strtotime($row['OrderDate']);
    else $ts2 = strtotime($row['InvoiceDate']);
    
    $seconds_diff = $ts1 - $ts2;
    $days_diff = $seconds_diff/86400;
    $data_cl[$manager]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['date_diff'] = $days_diff;
    
    $data_cl[$manager]['user'] = $row['ManagerUser'];
    $data_cl[$manager]['position'] = $row['ManagerPosition'];    
  }
  
  // -----------------------------------------------
  // вычисляем суммы приходов, а также премии
  foreach($data_cl as $manager=>$manager_data)
  {
    $benefit_result = 0;
    $sum_result = 0;
    
    foreach($ordertypes_groups as $ordertype_group)
    {
      $i = 0;
      $sum_income = 0;
      $sum_income_firsttime = 0;
      $sum_benefit = 0;
      
     foreach($manager_data['service_groups'][$ordertype_group]['orders'] as $datePayment=>$orders_array)
      foreach($orders_array as $orderId=>$order)
      {
        $sum_income += $order['sum'];
        $data_cl[$manager]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['cur_sum'] = $sum_income; // справочное значение
        
        // -----------------------------------------------------------
        // выбираем коэффициент за просрочку платежа
        $benefit_rate_date = 1.00;
        if($data_cl[$manager]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['source_type']<>'Партнер')
        {
          foreach($motivation_datediff[$ordertype_group] as $motiv_date)
            if($motiv_date["border"]>=$data_cl[$manager]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['date_diff'])
            {
              $benefit_rate_date = $motiv_date["rate"];
              break;
            }
        }
        else
        {
          $date_finish = $data_cl[$manager]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['date_order_finish'];
          $date_payment = $data_cl[$manager]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['date_payment'];
          
          foreach($motivation_datediff["Партнер"] as $motiv_date)
          {
            $date_expected = date("Y-m-d", strtotime("last day of +".$motiv_date["border"]." month", strtotime($date_finish)));
            if($date_payment < $date_expected)
            {
              $benefit_rate_date = $motiv_date["rate"];
              break;
            }
          }
        }
        
        $data_cl[$manager]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['benefit_rate_date'] = $benefit_rate_date;
        
        // -----------------------------------------------------------
        // смотрим, если еще в текущем интервале премии, то вычисляем по ставке данного интервала
        if($motivation[$ordertype_group][$i]["border"]>$sum_income)
        {
          $data_cl[$manager]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['benefit_rate'] = $motivation[$ordertype_group][$i]["rate"];
          $data_cl[$manager]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['benefit'] = $benefit_rate_date * $motivation[$ordertype_group][$i]["rate"] * floatval($order['sum']);
        }
        else
        {
          $overflow = $sum_income - $motivation[$ordertype_group][$i]["border"];
          if($overflow < $order['sum']) $tail = $order['sum'] - $overflow;
          
          // хвостик считаем по текущему интервалу, превышение по новому интервалу
          $data_cl[$manager]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['benefit_rate'] = "смеш.";
          $data_cl[$manager]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['benefit'] = $benefit_rate_date * ($motivation[$ordertype_group][$i]["rate"] * floatval($tail) + $motivation[$ordertype_group][$i+1]["rate"] * floatval($overflow));
          
          $i++;
        }

        $sum_benefit += $data_cl[$manager]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['benefit'] ;
        
      }

      // -----------------------------------------------------------
      $data_cl[$manager]['service_groups'][$ordertype_group]['total']['sum'] = $sum_income;
      $data_cl[$manager]['service_groups'][$ordertype_group]['total']['benefit'] = $sum_benefit;
      
      // -----------------------------------------------------------
      // получаем данные по планам продаж
      $rowPlan = data_select_array($tableSalesPlans, "status=0 and f13331=".$data_cl[$manager]['user']." and f13281='".$date1."' and f13801='".$ordertype_group."'");
      $data_cl[$manager]['service_groups'][$ordertype_group]['total']['plan_sum'] = $rowPlan['f13311'];
      
      // -----------------------------------------------------------
      // вычисляем процент исполнения плана
      if(floatval($rowPlan['f13311'])>0) $data_cl[$manager]['service_groups'][$ordertype_group]['total']['plan_percent'] =  floatval($sum_income) / floatval($rowPlan['f13311']) * 100.00;
      else $data_cl[$manager]['service_groups'][$ordertype_group]['total']['plan_percent'] = 100.00;
      
      // -----------------------------------------------------------
      // выбираем соответствующий коэффициент за план
      if($data_cl[$manager]['position'] == 6)
      {
          foreach($motivation_plans as $motiv_plan) 
            if(floatval($motiv_plan['border'])*100.00<=$data_cl[$manager]['service_groups'][$ordertype_group]['total']['plan_percent']) 
            {
              $data_cl[$manager]['service_groups'][$ordertype_group]['total']['plan_coef'] = $motiv_plan['rate'];
              $data_cl[$manager]['service_groups'][$ordertype_group]['total']['benefit_final'] = $motiv_plan['rate'] * $sum_benefit;
              break;
            } 
      }
      else
      {
          $data_cl[$manager]['service_groups'][$ordertype_group]['total']['plan_coef'] = 0.0;
          $data_cl[$manager]['service_groups'][$ordertype_group]['total']['benefit_final'] = 0.0;          
      }
      // -----------------------------------------------------------
      // вычисляем итоговые суммы премий
      $benefit_result += $data_cl[$manager]['service_groups'][$ordertype_group]['total']['benefit_final'];
      $sum_result += $data_cl[$manager]['service_groups'][$ordertype_group]['total']['sum'];
        
    } // end foreach ordertype_groups    
    
    $data_cl[$manager]['benefit_result'] = $benefit_result;
    $data_cl[$manager]['sum_result'] = $sum_result;
  } // end foreach manager
  
} // end if show clients managers


// -------------------------------------------------------
// выборка из таблицы заказы счета поступления по менеджерам продаж
if($bShowSales)
{
  // f11251 - manager sales
  $sqlQuery = "SELECT 
                  tiio.id as Id
                 ,tcmp.id as ClientId
                 ,tinc.id as IncomeId
                 ,tinc.f7511 as IncomeNum
                 ,tord.id as OrderId
                 ,tord.f7071 as OrderNum
                 ,tst.f1158 as ServiceType
                 ,tord.f4431 as OrderDescription
                 ,tiio.f10920 as PaymentSum
                 ,DATE(tiio.f11271) as PaymentDate
                 ,DATE(tiio.f11291) as InvoiceDate
                 ,DATE(tiio.f11281) as OrderDate
                 ,DATE(tcd.f13811) as FirstPaymentDate
                 ,tcmp.f435 as ClientName
                 ,tsrc.f435 as SourceName
                 ,tsrc.f772 as SourceType
                 ,tpers.f483 as ManagerName               
                 ,tiio.f11251 as ManagerUser
                 ,tiio.f13961 as IncomeType
               FROM 
                 ".DATA_TABLE.$tableIncomeInvoiceOrders." as tiio
                 LEFT JOIN ".DATA_TABLE.$tableOrders." as tord ON tord.id = tiio.f10890 AND tord.status = 0
                 LEFT JOIN ".DATA_TABLE.$tableServiceTypes." as tst ON tst.id = tord.f4451 AND tst.status = 0
                 LEFT JOIN ".DATA_TABLE.$tableClients." as tcmp ON tcmp.id = tord.f4441 AND tcmp.status = 0
                 LEFT JOIN ".DATA_TABLE.$tableClientsData." as tcd ON tcd.f10410 = tord.f4441 AND tcd.status = 0
                 LEFT JOIN ".DATA_TABLE.$tableIncomes." as tinc ON tinc.id = tiio.f10910 AND tinc.status = 0
                 LEFT JOIN ".DATA_TABLE.$tableClients." as tsrc ON tsrc.id = tinc.f7531 AND tsrc.status = 0
                 LEFT JOIN ".DATA_TABLE.$tablePersonal." as tpers ON tpers.f1400 = tiio.f11251 AND tpers.status = 0 AND tiio.f11251 <> '' 
               WHERE 
                 DATE(tiio.f11271) BETWEEN '".$date1."' AND '".$date2."'
                 AND tiio.status = 0 AND tiio.f11251 <> '' AND tpers.f484=1
                 ".$managerFilter."
               ORDER BY 
                  tiio.f11251, tiio.f11271
                "; // AND tpers.f7941='Да'
  
  $data = array();
  $sum = 0;
  
  $res = sql_query($sqlQuery);
  while($row = sql_fetch_array($res))
  {
  
    if($row['ServiceType']<>"Подготовка документации")
    {
       $ordertype_group = "Обычные";
       $sum_common += $row['PaymentSum'];
    }
    else
    {
       $ordertype_group = "Комплексные";
       $sum_complex += $row['PaymentSum'];
    }

    // вычисляем вторичные или первичные продажи

    if(trim($row['IncomeType'])<>"")
    {
      $incometype = trim($row['IncomeType']);
    }
    else
    {
      $ts1 = strtotime($row['PaymentDate']);
      $ts2 = strtotime($row['FirstPaymentDate']);    
      $seconds_diff = $ts1 - $ts2;
      $days_diff = $seconds_diff/86400;
      if($row['FirstPaymentDate']<>"0000-00-00" && $days_diff>30) $incometype = "Вторичные";
      else $incometype = "Первичные";
    }
  
    $manager = $row['ManagerName'];
    $orderId = $row['Id'];
    $datePayment = $row['PaymentDate'];
    
    $data_sl[$manager]['income_types'][$incometype]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['income_id'] = $row['IncomeId'];
    $data_sl[$manager]['income_types'][$incometype]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['income_num'] = $row['IncomeNum'];
    $data_sl[$manager]['income_types'][$incometype]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['order_id'] = $row['OrderId'];
    $data_sl[$manager]['income_types'][$incometype]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['order_num'] = $row['OrderNum'];
    $data_sl[$manager]['income_types'][$incometype]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['client_id'] = $row['ClientId'];    
    
    $data_sl[$manager]['income_types'][$incometype]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['client'] = $row['ClientName'];
    $data_sl[$manager]['income_types'][$incometype]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['source_name'] = $row['SourceName'];
    $data_sl[$manager]['income_types'][$incometype]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['source_type'] = $row['SourceType'];
    $data_sl[$manager]['income_types'][$incometype]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['service'] = $row['ServiceType'];
    $data_sl[$manager]['income_types'][$incometype]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['description'] = mb_substr($row['OrderDescription'],0,40,'UTF-8');
    $data_sl[$manager]['income_types'][$incometype]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['sum'] = $row['PaymentSum'];
    $data_sl[$manager]['income_types'][$incometype]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['date_payment'] = $row['PaymentDate'];
    $data_sl[$manager]['income_types'][$incometype]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['date_firstpayment'] = $row['FirstPaymentDate'];
    $data_sl[$manager]['income_types'][$incometype]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['incometype'] = $incometype;

    $data_sl[$manager]['user'] = $row['ManagerUser'];
    $sum += $row['PaymentSum'];
  }  
  
  // вычисляем суммы приходов, а также премии
  foreach($data_sl as $manager=>$manager_data)
  {
    $benefit_result = 0;
    $sum_result = 0;

    foreach($incometypes as $incometype)
    {
      $sum_income_by_type = 0;
      $sum_benefit_by_type = 0;
    
      foreach($ordertypes_groups as $ordertype_group)
      {
        $i = 0;
        $sum_income = 0;
        $sum_benefit = 0;
        
       foreach($manager_data['income_types'][$incometype]['service_groups'][$ordertype_group]['orders'] as $datePayment=>$orders_array)
        foreach($orders_array as $orderId=>$order)
        {
          $sum_income += $order['sum'];
          
          $data_sl[$manager]['income_types'][$incometype]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['benefit_rate'] = $motivaion_sales[$ordertype_group];
          $data_sl[$manager]['income_types'][$incometype]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['benefit'] = $motivaion_sales[$ordertype_group] * floatval($order['sum']);

          $sum_benefit += $data_sl[$manager]['income_types'][$incometype]['service_groups'][$ordertype_group]['orders'][$datePayment][$orderId]['benefit']; 
        }
        
        $data_sl[$manager]['income_types'][$incometype]['service_groups'][$ordertype_group]['total']['sum'] = $sum_income;
        $data_sl[$manager]['income_types'][$incometype]['service_groups'][$ordertype_group]['total']['benefit'] = $sum_benefit;
        
        $sum_income_by_type += $sum_income;
        $sum_benefit_by_type += $sum_benefit;
        
        if(trim($data_sl[$manager]['user'])<>"")
        {
          // получаем данные по планам продаж
          $rowPlan = data_select_array($tableSalesPlans, "status=0 and f13331=".$data_sl[$manager]['user']." and f13281='".$date1."' and f13801='".$ordertype_group."'");
          $data_sl[$manager]['income_types'][$incometype]['service_groups'][$ordertype_group]['total']['plan_sum'] = $rowPlan['f13311'];
          
          // вычисляем процент исполнения плана
          if(floatval($rowPlan['f13311'])>0) $data_sl[$manager]['income_types'][$incometype]['service_groups'][$ordertype_group]['total']['plan_percent'] =  floatval($sum_income) / floatval($rowPlan['f13311']) * 100.00;
          else $data_sl[$manager]['income_types'][$incometype]['service_groups'][$ordertype_group]['total']['plan_percent'] = 100.00;
          
          // выбираем соответствующий коэффициент за план
          foreach($motivation_plans as $motiv_plan) 
            if(floatval($motiv_plan['border'])*100.00<=$data_sl[$manager]['income_types'][$incometype]['service_groups'][$ordertype_group]['total']['plan_percent']) 
            {
              $data_sl[$manager]['income_types'][$incometype]['service_groups'][$ordertype_group]['total']['plan_coef'] = $motiv_plan['rate'];
              $data_sl[$manager]['income_types'][$incometype]['service_groups'][$ordertype_group]['total']['benefit_final'] = $motiv_plan['rate'] * $sum_benefit;
              break;
            }
        }
        
        $benefit_result += $data_sl[$manager]['income_types'][$incometype]['service_groups'][$ordertype_group]['total']['benefit_final'];
        $sum_result += $data_sl[$manager]['income_types'][$incometype]['service_groups'][$ordertype_group]['total']['sum'];
            
      }  // end foreach ordertype

      $data_sl[$manager]['income_types'][$incometype]['total']['sum'] = $sum_income_by_type;
      $data_sl[$manager]['income_types'][$incometype]['total']['benefit'] = $sum_benefit_by_type;
    
    } // end foreach incometypes
    
    $data_sl[$manager]['benefit_result'] = $benefit_result;
    $data_sl[$manager]['sum_result'] = $sum_result;

    // пересчитываем премии по коэффициенту исполнения плана первичных продаж
    $plan_coef_typical = $data_sl[$manager]['income_types']['Первичные']['service_groups']['Обычные']['total']['plan_coef'];
    $plan_coef_complex = $data_sl[$manager]['income_types']['Первичные']['service_groups']['Комплексные']['total']['plan_coef'];
    
    $data_sl[$manager]['income_types']['Первичные']['service_groups']['Обычные']['total']['plan_coef'] = $plan_coef_typical;
    $data_sl[$manager]['income_types']['Первичные']['service_groups']['Комплексные']['total']['plan_coef'] = $plan_coef_complex;
    $data_sl[$manager]['income_types']['Вторичные']['service_groups']['Обычные']['total']['plan_coef'] = $plan_coef_typical;
    $data_sl[$manager]['income_types']['Вторичные']['service_groups']['Комплексные']['total']['plan_coef'] = $plan_coef_complex;

    $data_sl[$manager]['income_types']['Первичные']['service_groups']['Обычные']['total']['benefit_final'] = $plan_coef_typical * $data_sl[$manager]['income_types']['Первичные']['service_groups']['Обычные']['total']['benefit'];
    $data_sl[$manager]['income_types']['Первичные']['service_groups']['Комплексные']['total']['benefit_final'] = $plan_coef_complex * $data_sl[$manager]['income_types']['Первичные']['service_groups']['Комплексные']['total']['benefit'];
    $data_sl[$manager]['income_types']['Вторичные']['service_groups']['Обычные']['total']['benefit_final'] = $plan_coef_typical * $data_sl[$manager]['income_types']['Вторичные']['service_groups']['Обычные']['total']['benefit'];
    $data_sl[$manager]['income_types']['Вторичные']['service_groups']['Комплексные']['total']['benefit_final'] = $plan_coef_complex * $data_sl[$manager]['income_types']['Вторичные']['service_groups']['Комплексные']['total']['benefit'];
        
  } // end foreach manager

  
} // end if show sales manager

// ================================================================================================
if($bShowProductionSearch)
{
  // вычисляем зарплату по менеджерам-аналитикам
  $data_pr_srch = array();
  
  $manager_name = "Иванова Е.В.";

  // -----------------------------------------
  // вычисляем по платным поискам
  $sum_paid = 0;
  $benefit_rate = 0.1;
  $sqlQuery = "SELECT 
                  tiio.id as Id
                 ,tord.f7071 as OrderId
                 ,tord.f4431 as OrderDescription
                 ,tiio.f10920 as PaymentSum
                 ,DATE(tiio.f11271) as PaymentDate
                 ,DATE(tiio.f11291) as InvoiceDate
                 ,DATE(tiio.f11281) as OrderDate
                 ,DATE(tsch.f6941) as DateStart
                 ,DATE(tsch.f6951) as DateFinish
                 ,tcmp.f435 as ClientName
               FROM 
                 ".DATA_TABLE.$tableIncomeInvoiceOrders." as tiio
                 LEFT JOIN ".DATA_TABLE.$tableOrders." as tord ON tord.id = tiio.f10890 AND tord.status = 0
                 LEFT JOIN ".DATA_TABLE.$tableOrdersSearch." as tsch ON tsch.f5411 = tord.id AND tsch.status = 0
                 LEFT JOIN ".DATA_TABLE.$tableClients." as tcmp ON tcmp.id = tord.f4441 AND tcmp.status = 0
               WHERE 
                 DATE(tiio.f11271) BETWEEN '".$date1."' AND '".$date2."'
                 AND tiio.status = 0 
                 AND tiio.f11741 = 4
                 ".$managerFilter."
               ORDER BY 
                  tiio.f11271
            ";  
  $res = sql_query($sqlQuery);
  while($row = sql_fetch_array($res))
  { 
    $inc_id = $row['Id'];
    
    $data_pr_srch[$manager_name]["income_types"]["Платный поиск"]['orders'][$inc_id]['order_id'] = $row['OrderId'];
    $data_pr_srch[$manager_name]["income_types"]["Платный поиск"]['orders'][$inc_id]['client'] = $row['ClientName'];
    $data_pr_srch[$manager_name]["income_types"]["Платный поиск"]['orders'][$inc_id]['description'] = $row['OrderDescription'];
    $data_pr_srch[$manager_name]["income_types"]["Платный поиск"]['orders'][$inc_id]['dates'] = $row['DateStart']." - ".$row['DateFinish'];
    $data_pr_srch[$manager_name]["income_types"]["Платный поиск"]['orders'][$inc_id]['date_payment'] = $row['PaymentDate'];
    $data_pr_srch[$manager_name]["income_types"]["Платный поиск"]['orders'][$inc_id]['sum'] = $row['PaymentSum'];
    $data_pr_srch[$manager_name]["income_types"]["Платный поиск"]['orders'][$inc_id]['benefit_rate'] = $benefit_rate;
    $data_pr_srch[$manager_name]["income_types"]["Платный поиск"]['orders'][$inc_id]['benefit'] = $row['PaymentSum'] * $benefit_rate;
    
    $sum_paid += $row['PaymentSum'];
  }
  
  $data_pr_srch[$manager_name]["income_types"]["Платный поиск"]['total']['sum'] = $sum_paid;
  $data_pr_srch[$manager_name]["income_types"]["Платный поиск"]['total']['benefit'] = $sum_paid * $benefit_rate;

  // -----------------------------------------
  // вычисляем по бесплатным поискам
  $benefit_sum = 0.0;
  $sqlQuery = "SELECT 
                  tord.f7071 as OrderId
                 ,tord.f4431 as OrderDescription
                 ,DATE(tord.f6591) as OrderDate
                 ,DATE(tsch.f6941) as DateStart
                 ,DATE(tsch.f6951) as DateFinish
                 ,tcmp.f435 as ClientName
               FROM 
                 ".DATA_TABLE.$tableOrders." as tord 
                 LEFT JOIN ".DATA_TABLE.$tableOrdersSearch." as tsch ON tsch.f5411 = tord.id AND tsch.status = 0
                 LEFT JOIN ".DATA_TABLE.$tableClients." as tcmp ON tcmp.id = tord.f4441 AND tcmp.status = 0
               WHERE 
                 DATE(tsch.f6951) BETWEEN '".$date1."' AND '".$date2."'
                 AND tord.status = 0 
                 AND tord.f4451 = 4
                 AND tord.f13071 = 2
                  ".$managerFilter."
              ORDER BY 
                  tord.f6591
            ";  
  $res = sql_query($sqlQuery);
  while($row = sql_fetch_array($res))
  { 
    $order_id = $row['OrderId'];
    
    $data_pr_srch[$manager_name]["income_types"]["Пробный поиск"]['orders'][$order_id]['order_id'] = $row['OrderId'];
    $data_pr_srch[$manager_name]["income_types"]["Пробный поиск"]['orders'][$order_id]['client'] = $row['ClientName'];
    $data_pr_srch[$manager_name]["income_types"]["Пробный поиск"]['orders'][$order_id]['description'] = $row['OrderDescription'];
    $data_pr_srch[$manager_name]["income_types"]["Пробный поиск"]['orders'][$order_id]['date_payment'] = $row['PaymentDate'];
    $data_pr_srch[$manager_name]["income_types"]["Пробный поиск"]['orders'][$order_id]['dates'] = $row['DateStart']." - ".$row['DateFinish'];
    $data_pr_srch[$manager_name]["income_types"]["Пробный поиск"]['orders'][$order_id]['sum'] = $row['PaymentSum'];

    $ts1 = strtotime($row['DateFinish']);
    $ts2 = strtotime($row['DateStart']);    
    $seconds_diff = $ts1 - $ts2;
    $days_diff = $seconds_diff/86400;
    
    //ставка 200 руб. за 14 дней
    $benefit = 200.0/14.0*$days_diff;
    $data_pr_srch[$manager_name]["income_types"]["Пробный поиск"]['orders'][$order_id]['benefit'] = $benefit;
    
    $benefit_sum += $benefit;
  }
  
  $data_pr_srch[$manager_name]["income_types"]["Пробный поиск"]['total']['sum'] = 0;
  $data_pr_srch[$manager_name]["income_types"]["Пробный поиск"]['total']['benefit'] = $benefit_sum;
  
  // -------------------------------------
  // итоги
  $data_pr_srch[$manager_name]['total']['sum'] = $sum_paid;
  $data_pr_srch[$manager_name]['total']['benefit'] = $data_pr_srch[$manager_name]["income_types"]["Пробный поиск"]['total']['benefit'] +  $data_pr_srch[$manager_name]["income_types"]["Платный поиск"]['total']['benefit'];
  //print_r($data_pr_srch);
}

$sum_total = 0;
$sum_typical = 0;
$sum_complex = 0;
$sum_typical_plan = 0;

$benefit_total = 0;
$benefit_typical = 0;
$benefit_complex = 0;

// =========================================================================================
// готовим таблицы с итогами
// =========================================================================================

// менджеры по работе с клиентами
$totals['groups']["manager_clients"]["group_name"] = "Менеджеры по работе с клиентами";
foreach($data_cl as $manager=>$manager_data)
{
    $manager_num = array_search($manager, array_column($managers, 'fullname'));
    if($managers[$manager_num]["work_now"]<>"")
    {
        $totals['groups']["manager_clients"]["managers"][$manager]["work_now"] = $managers[$manager_num]["work_now"];
        $totals['groups']["manager_clients"]["managers"][$manager]["base_salary"] = $managers[$manager_num]["base_salary"];
    }
    
    $totals['groups']["manager_clients"]["managers"][$manager]["sum"]["total"] = $data_cl[$manager]["sum_result"];
    
    $totals['groups']["manager_clients"]["managers"][$manager]["sum"]["typical_plan"] =  $data_cl[$manager]['service_groups']["Обычные"]['total']['plan_sum'];
    $totals['groups']["manager_clients"]["managers"][$manager]["sum"]["complex_plan"] =  0;
    
    $totals['groups']["manager_clients"]["managers"][$manager]["sum"]["typical"]['old'] =  $data_cl[$manager]['service_groups']["Обычные"]['total']['sum'];
    $totals['groups']["manager_clients"]["managers"][$manager]["sum"]["complex"]['old'] = $data_cl[$manager]['service_groups']["Комплексные"]['total']['sum'];
    
    // итоги
    $sum_total += $data_cl[$manager]["sum_result"];
    $sum_typical_plan += $totals['groups']["manager_clients"]["managers"][$manager]["sum"]["typical_plan"];
    $sum_complex_plan += $totals['groups']["manager_clients"]["managers"][$manager]["sum"]["complex_plan"];
    
    $sum_typical += $data_cl[$manager]['service_groups']["Обычные"]['total']['sum'];
    $sum_complex += $data_cl[$manager]['service_groups']["Комплексные"]['total']['sum'];

    $totals['groups']["manager_clients"]["managers"][$manager]["benefit"]["total"] = $data_cl[$manager]["benefit_result"];
    $totals['groups']["manager_clients"]["managers"][$manager]["benefit"]["typical"]['old'] = $data_cl[$manager]['service_groups']["Обычные"]['total']['benefit_final'];  
    $totals['groups']["manager_clients"]["managers"][$manager]["benefit"]["complex"]['old'] =  $data_cl[$manager]['service_groups']["Комплексные"]['total']['benefit_final'];  
    
    $benefit_total += $data_cl[$manager]["benefit_result"];
    $benefit_typical += $data_cl[$manager]['service_groups']["Обычные"]['total']['benefit_final']; 
    $benefit_complex += $data_cl[$manager]['service_groups']["Комплексные"]['total']['benefit_final'];     
}

// менджеры продаж
$totals['groups']["manager_sales"]["group_name"] = "Менеджеры продаж";
foreach($data_sl as $manager=>$manager_data)
{
    $manager_num = array_search($manager, array_column($managers, 'fullname'));
    if($managers[$manager_num]["work_now"]<>"")
    {
        $totals['groups']["manager_sales"]["managers"][$manager]["work_now"] = $managers[$manager_num]["work_now"];
        $totals['groups']["manager_sales"]["managers"][$manager]["base_salary"] = $managers[$manager_num]["base_salary"];
    }
    
    $totals['groups']["manager_sales"]["managers"][$manager]["sum"]["total"] = $data_sl[$manager]["sum_result"];
    
    $totals['groups']["manager_sales"]["managers"][$manager]["sum"]["typical_plan"] = $data_sl[$manager]['income_types']["Первичные"]['service_groups']["Обычные"]['total']['plan_sum'];
    $totals['groups']["manager_sales"]["managers"][$manager]["sum"]["complex_plan"] = 0;
    
    $totals['groups']["manager_sales"]["managers"][$manager]["sum"]["typical"]['new'] = $data_sl[$manager]['income_types']["Первичные"]['service_groups']["Обычные"]['total']['sum'];
    $totals['groups']["manager_sales"]["managers"][$manager]["sum"]["typical"]['old'] = $data_sl[$manager]['income_types']["Вторичные"]['service_groups']["Обычные"]['total']['sum'];
    
    $totals['groups']["manager_sales"]["managers"][$manager]["sum"]["complex"]['new'] = $data_sl[$manager]['income_types']["Первичные"]['service_groups']["Комплексные"]['total']['sum'];
    $totals['groups']["manager_sales"]["managers"][$manager]["sum"]["complex"]['old'] = $data_sl[$manager]['income_types']["Вторичные"]['service_groups']["Комплексные"]['total']['sum'];

    $totals['groups']["manager_sales"]["managers"][$manager]["benefit"]["total"] = $data_sl[$manager]["benefit_result"];
    
    $totals['groups']["manager_sales"]["managers"][$manager]["benefit"]["typical"]['new'] = $data_sl[$manager]['income_types']['Первичные']['service_groups']['Обычные']['total']['benefit_final'];
    $totals['groups']["manager_sales"]["managers"][$manager]["benefit"]["typical"]['old'] = $data_sl[$manager]['income_types']['Вторичные']['service_groups']['Обычные']['total']['benefit_final'];
    
    $totals['groups']["manager_sales"]["managers"][$manager]["benefit"]["complex"]['new'] =  $data_sl[$manager]['income_types']['Первичные']['service_groups']['Комплексные']['total']['benefit_final'];    
    $totals['groups']["manager_sales"]["managers"][$manager]["benefit"]["complex"]['old'] =  $data_sl[$manager]['income_types']['Вторичные']['service_groups']['Комплексные']['total']['benefit_final'];    
}


// общие итоги
$totals["totals"]["sum"]["total"] = $sum_total;
$totals["totals"]["sum"]["typical_plan"] = $sum_typical_plan;
$totals["totals"]["sum"]["typical"] = $sum_typical;
$totals["totals"]["sum"]["complex"] = $sum_complex;

$totals["totals"]["benefit"]["total"] = $benefit_total;
$totals["totals"]["benefit"]["typical"] = $benefit_typical;
$totals["totals"]["benefit"]["complex"] = $benefit_complex;

//
// -------------------------------------------------------
// Переносим переменные в отображение
$smarty->assign("totals", $totals);
$smarty->assign("data_manager_clients", $data_cl);
$smarty->assign("data_manager_sales", $data_sl);
$smarty->assign("data_manager_prod_srch", $data_pr_srch);
$smarty->assign("data_manager_prod_ord", $data_pr_ord);
$smarty->assign("ord_managers", $ord_managers);

$smarty->assign("show_totals", $bShowTotals);
$smarty->assign("show_sales", $bShowSales);
$smarty->assign("show_clients", $bShowClients);
$smarty->assign("show_prod_srch", $bShowProductionSearch );
$smarty->assign("show_prod_ord", $bShowProductionOrders );

$smarty->assign("dt_period", $dt_period);
$smarty->assign("dates", $arrayOfDates);


?>