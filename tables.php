<?php
global $cb_tables;

$cb_tables = array();
$cb_tables['tablePersonal'] = 46;
$cb_tables['tableEvents'] = 62;
$cb_tables['tableClients'] = 42;
$cb_tables['tableClientsData'] = 630;

$cb_tables['tableSignatures'] = 441;
$cb_tables['tableAccreditations'] = 980;

$cb_tables['tableLogins'] = 531;
$cb_tables['tableSites'] = 471;
$cb_tables['tableTenders'] = 331;
$cb_tables['tableRegions'] = 841;
$cb_tables['tableIFNS'] = 570;
$cb_tables['tableUc'] = 451;

$cb_tables['tableOrders'] = 271;
$cb_tables['tableOrdersECP'] = 341;
$cb_tables['tableOrdersWorkPlace'] = 351;
$cb_tables['tableOrdersAccred'] = 361;
$cb_tables['tableOrdersSearch'] = 371;
$cb_tables['tableOrdersExpertSearch'] = 1351;
$cb_tables['tableOrdersApp'] = 381;
$cb_tables['tableOrdersComplaints'] = 391;
$cb_tables['tableOrdersBg'] = 411;
$cb_tables['tableOrdersCmplx'] = 401;
$cb_tables['tableOrdersSRO'] = 970;

$cb_tables['tableOrdersMilestones'] = 901;

$cb_tables['tableInvoices'] = 43;
$cb_tables['tableInvoicesOrders'] = 74;
$cb_tables['tableActs'] = 81;
$cb_tables['tableIncomes'] = 511;
$cb_tables['tableIncomesInvoices'] = 560;
$cb_tables['tableIncomeInvoiceOrders'] = 650;

$cb_tables['tableOrderJobs'] = 751;
$cb_tables['tableJobTypes'] = 761;
$cb_tables['tableServiceTypes'] = 91;
$cb_tables['tableSalesPlans'] = 771;

$cb_tables['tableServiceJobs'] = 851;

$cb_tables['tableServiceObjChecks'] = 881;
$cb_tables['tableServiceObjBugs'] = 921;
$cb_tables['tableServiceObjGroups'] = 891;

$cb_tables['tableManagerExists'] = 491;

$cb_tables['tableJobMatrix'] = 930;
$cb_tables['tablePrice'] = 580;

$cb_tables['tableTenderTypes'] = 481;
$cb_tables['tableTenderSites'] = 471;

$cb_tables['tableVacancies'] = 691;
$cb_tables['tablePositions'] = 261;
$cb_tables['tableDepartments'] = 701;
$cb_tables['tableBranches'] = 661;
?>