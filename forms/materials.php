<?php


require_once 'add/db.php';
require_once 'add/tables.php';
require_once 'add/functions/service_functions.php';

// write_log("num:".num2str("10.3", "none", "none", 0));
// write_log("is_num:".is_numeric("3450.10"));

// write_log(num2str_fraction("101.101", "none", "none", 0));
// write_log(num2str_fraction("111.1111", "none", "none", 0));

$materials = [];
$parameters = [];
$parametersForMaterials = [];

$i = 0;
$resMaterial = data_select(1050, "status=0");
while ($rowMaterial = sql_fetch_assoc($resMaterial))
{
    $parameterType = "";

    $materialName = $rowMaterial['f17860'];
    $parameterName =  $rowMaterial['f17870'];

    $valueMin = trim($rowMaterial['f17890']);
    $valueMinIncluded = trim($rowMaterial['f17880']);
    $valueMinIsNumeric = is_numeric($valueMin);
    $valueMax = trim($rowMaterial['f17910']);
    $valueMaxIncluded = trim($rowMaterial['f17900']);
    $valueMaxIsNumeric = is_numeric($valueMax); // str_replace(",",".",
    $valueIsInterval = trim($rowMaterial['f17920']);
    $valueFixed = trim($rowMaterial['f17930']);
    $valueFixedIsNumeric = is_numeric($valueFixed);
    $valueMeasure = trim($rowMaterial['f17950']);
    $valueNegative = trim($rowMaterial['f17940']);


    if(trim($materialName)<>"") $materials[$materialName] = [];
    if(trim($parameterName)<>"") $parameters[$parameterName] = [];

    if(trim($materialName)<>"" && trim($parameterName)<>"")
    {
        if($parameterType=="" && $valueFixed<>"") $parameterType = "Фиксировано";
        if($parameterType=="" && $valueIsInterval<>"") $parameterType = "Интервал";
        if($parameterType=="" && ($valueMax<>"" || $valueMin<>"")) $parameterType = "Границы";

        $parametersForMaterials[$i]['id_rnd'] = rand();
        $parametersForMaterials[$i]['material_name'] = $materialName;
        $parametersForMaterials[$i]['parameter_name'] = $parameterName;
        $parametersForMaterials[$i]['type'] = $parameterType;
        $parametersForMaterials[$i]['values']['min'] = $valueMin;
        $parametersForMaterials[$i]['values']['min_str'] = ($valueMinIsNumeric ? str_replace("минус", "минус ", num2str_fraction($valueMin, "none", "none", 0)) : $valueMin);
        $parametersForMaterials[$i]['values']['min_included'] = $valueMinIncluded;
        $parametersForMaterials[$i]['values']['min_is_numeric'] = $valueMinIsNumeric;
        $parametersForMaterials[$i]['values']['max'] = $valueMax;
        $parametersForMaterials[$i]['values']['max_str'] = ($valueMaxIsNumeric ? str_replace("минус", "минус ", num2str_fraction($valueMax, "none", "none", 0)) : $valueMax);
        $parametersForMaterials[$i]['values']['max_included'] = $valueMaxIncluded;
        $parametersForMaterials[$i]['values']['max_is_numeric'] = $valueMaxIsNumeric;
        $parametersForMaterials[$i]['values']['is_interval'] = $valueIsInterval;
        $parametersForMaterials[$i]['values']['fixed'] = $valueFixed;
        $parametersForMaterials[$i]['values']['fixed_str'] = ($valueFixedIsNumeric ? str_replace("минус", "минус ", num2str_fraction($valueFixed, "none", "none", 0)) : $valueFixed);
        $parametersForMaterials[$i]['values']['fixed_is_numeric'] = $valueFixedIsNumeric;
        $parametersForMaterials[$i]['values']['negative'] = $valueNegative;
        $parametersForMaterials[$i]['values']['measure'] = $valueMeasure;

        $i++;
    }
} // while ($rowMaterial = sql_fetch_assoc($resMaterial))

$codePart1 = 0; // 10
$codePart2 = 1;
$codePartIncrement = 1;
foreach($materials as $materialNameTemp => $materialObject)
{
    //$codeInt = intval($codePart1.$codePart2);
    $codeInt = intval(decbin($codePart2));
    $codeStr = num2str($codeInt, "none", "none", 0);

    $materials[$materialNameTemp]['id_num'] = $codeInt;
    $materials[$materialNameTemp]['id_str'] = $codeStr;
    $materials[$materialNameTemp]['id_rnd'] = rand();

    $codePart2 += $codePartIncrement;

}

$codePart1 = 0; // 100
$codePart2 = 1;
$codePartIncrement = 1; //1
foreach($parameters as $parameterNameTemp => $parameterObject)
{
    //$codeInt = intval($codePart1.$codePart2);
    $codeInt = intval(decbin($codePart2));
    $codeStr = num2str($codeInt, "none", "none", 0);

    $parameters[$parameterNameTemp]['id_num'] = $codeInt;
    $parameters[$parameterNameTemp]['id_str'] = $codeStr;
    $parameters[$parameterNameTemp]['id_rnd'] = rand();

    $codePart2 += $codePartIncrement;
}


// пересортировываем по номеру тз
usort($parametersForMaterials, function($a, $b) {
    return $b['id_rnd'] < $a['id_rnd'];
});


$smarty->assign("materials", $materials);
$smarty->assign("parameters", $parameters);
$smarty->assign("parameters_for_materials", $parametersForMaterials);

?>