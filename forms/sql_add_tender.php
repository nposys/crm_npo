<?php

require_once 'add/db.php';
require_once 'add/tables.php';
require_once 'add/functions/service_functions.php';

$managers = array();
$statuses = array();
$groups = array();
$tender = array();
$lots = array();

// --------------------------------------------------------------------------
// входные переменные
$post_vars = getRealPOST();

$govruid = trim($_REQUEST['govruid']);
$tenderuid = trim($_REQUEST['tenderuid']);
$lotuid = trim($_REQUEST['lotuid']);
$status_id = intval($_REQUEST['status_id']);
$group_id = intval($_REQUEST['group_id']);
if(!$status_id) $status_id=2;
$manager_id = intval($_REQUEST['manager_id']);

foreach ($post_vars as $param_name=>$param_val)
{
    if(substr($param_name, 0, 10)=='manager_id' && intval($param_val)>0)
    {
        $lotuid = trim(substr($param_name, 10));
        $lots[$lotuid]['manager_id'] = intval($param_val);
    }

    if(substr($param_name, 0, 9)=='status_id' && intval($param_val)>0)
    {
        $lotuid = trim(substr($param_name, 9));
        $lots[$lotuid]['status_id'] = intval($param_val);
    }

    if(substr($param_name, 0, 8)=='group_id' && intval($param_val)>0)
    {
        $lotuid = trim(substr($param_name, 8));
        $lots[$lotuid]['group_id'] = intval($param_val);
    }
}

// --------------------------------------------------------------------------
// setup SQL connection
ini_set('max_execution_time', 900);
ini_set('mssql.charset','UTF-8');
ini_set('mssql.connect_timeout','120');
ini_set('mssql.timeout','600');

// connect to ms sql
$serverName = $databases["MSSQL"]["server"];
$connectionOptions = array(
    "Database" => $databases["MSSQL"]["db"],
    "Uid" => $databases["MSSQL"]["user"],
    "PWD" => $databases["MSSQL"]["pass"],
    'ReturnDatesAsStrings'=>true
);
$sqlConnectId = sqlsrv_connect($serverName, $connectionOptions);

//$sqlConnectId = mssql_connect ($databases["MSSQL"]["server"] , $databases["MSSQL"]["user"] , $databases["MSSQL"]["pass"] );
// if($sqlConnectId) $db = mssql_select_db($databases["MSSQL"]["db"], $sqlConnectId);

// --------------------------------------------------------------------------
// список всех статусов клиента
$sql = "SELECT [ID],[StatusName] FROM [tenderdb].[dbo].[StatusesOfClient] ORDER BY [StatusName] ASC";
$res = sqlsrv_query($sqlConnectId, $sql);
$i = 0;
while($row = sqlsrv_fetch_array($res))
{
    $statuses[$i]['id'] = $row['ID'];
    $statuses[$i]['name'] = $row['StatusName'];
    $i++;
}

// --------------------------------------------------------------------------
// список всех групп клиента Регионторг = НПО Система
$sql = "SELECT [ID],[GroupName]  FROM GroupsForClient WHERE ClientID=1583";
$res = sqlsrv_query($sqlConnectId, $sql);
$i = 0;
while($row = sqlsrv_fetch_array($res))
{
    $groups[$i]['id'] = $row['ID'];
    $groups[$i]['name'] = $row['GroupName'];
    $i++;
}

// --------------------------------------------------------------------------
// список всех работающих менеджеров
$sql = "
SELECT [ID]
      ,[LastName]
      ,[FirstName]
      ,[MiddleName]
      ,[Email]
      ,[PhoneWork]
      ,[PhoneCell]
      ,[Department]
      ,[Position]
      ,[UserName]
      ,[Password]
      ,[Active]
  FROM [tenderdb].[dbo].[npoPersonal]
  WHERE [Active] = 1 AND [Position]=6
  ORDER BY [LastName] ASC
    ";
$res = sqlsrv_query($sqlConnectId, $sql);
while($row = sqlsrv_fetch_array($res))
{
    $managers[$row['ID']] = $row['LastName']." ".$row['FirstName'];
}

// --------------------------------------------------------------------------
// обновляем данные о тендере
foreach($lots as $lot_uid=>$lot_data)
{
    if($lot_uid<>"")
    {
        // проверяем наличие уже в таблице Регионторга. выводим сообщение при необходимости
        $sqlCheck = "SELECT ID FROM ClientsLots as cl WHERE GroupID IN (SELECT ID FROM GroupsForClient WHERE ClientID=1583) and LotUID='$lot_uid'";
        $resCheck = sqlsrv_query($sqlConnectId, $sqlCheck);
        $rowCheck = sqlsrv_fetch_array($resCheck);

        $status_id = $lot_data['status_id'];
        $manager_id = $lot_data['manager_id'];
        $group_id = $lot_data['group_id'];

        if(intval($rowCheck['ID'])>0)
        {
            // update
            if($status_id<>"" &&$manager_id<>"" && $group_id<>"")
            {
                $sqlUpdate = "
                UPDATE
                    ClientsLots
                SET
                    Status=$status_id, ManagerID=$manager_id, GroupID=$group_id
                WHERE
                    ID = ".intval($rowCheck['ID'])."
                ";
                 $resUpdate = sqlsrv_query($sqlConnectId, $sqlUpdate);
            }
        }
        else
        {
            $sqlInsert = "
            INSERT INTO
                 ClientsLots
                     (GroupID, LotUID, Status, ManagerID, AnalyticsDate)
            VALUES
                     ($group_id,'$lot_uid',$status_id,$manager_id,'')
            ";
            $resInsert = sqlsrv_query($sqlConnectId, $sqlInsert);
        }
    }
}

// --------------------------------------------------------------------------
// находим LotUID по номеру тендера
if($govruid<>"")
{
    $sqlTender = "SELECT [UID] FROM [tenderdb].[dbo].[UniqueTenders] WHERE [GovRuID]='$govruid'";
    $resTender = sqlsrv_query($sqlConnectId, $sqlTender);
    if($rowTender = sqlsrv_fetch_array($resTender))
    {
        $uid = $rowTender['UID'];
        $sqlTenderData = "
            SELECT t.[UID]
                  ,t.[Title] as TenderTitle
                  ,t.[Cost] as TenderCost
                  ,t.[PublicDate]
                  ,t.[Customer]
                  ,cl.[ManagerID]
                  ,soc.[ID] As StatusId
                  ,soc.[StatusName]
                  ,l.[LotUID]
                  ,l.[LotNumber]
                  ,l.[Title] as LotTitle
                  ,l.[Cost] as LotCost
                  ,goc.[ID] as GroupID
                  ,goc.[GroupName] as GroupName
                  ,c.Title as CityTitle
                  ,r.Title as RegionTitle
                  ,o.Title as OkrugTitle
              FROM [tenders] as t
                    LEFT JOIN lots as l on l.TenderUID = t.UID
                    LEFT JOIN ClientsLots as cl ON cl.LotUID = l.LotUID AND cl.GroupID IN (SELECT ID FROM GroupsForClient WHERE ClientID=1583)
                    LEFT JOIN StatusesOfClient as soc ON soc.ID = cl.Status
                    LEFT JOIN GroupsForClient as goc ON goc.ID = cl.GroupID
              		LEFT JOIN Cities as c on c.ID = t.City
		            LEFT JOIN Regions as r on r.ID = c.Region
		            LEFT JOIN Okrugs as o on o.ID = r.Okrug
		      WHERE [UID] = '$uid'
            ";

            //echo $sqlTenderData;
            $resTenderData = sqlsrv_query($sqlConnectId, $sqlTenderData);
            while($rowTenderData = sqlsrv_fetch_array($resTenderData))
            {
                //print_r($rowTenderData);
                if(!$tender['common']['tender_uid']) $tender['common']['tender_uid'] = $uid;
                if(!$tender['common']['tender_title']) $tender['common']['tender_title'] = $rowTenderData['TenderTitle'];
                if(!$tender['common']['tender_cost']) $tender['common']['tender_cost'] = $rowTenderData['TenderCost'];
                if(!$tender['common']['city_title']) $tender['common']['city_title'] = $rowTenderData['CityTitle'];
                if(!$tender['common']['region_title']) $tender['common']['region_title'] = $rowTenderData['RegionTitle'];
                if(!$tender['common']['okrug_title']) $tender['common']['okrug_title'] = $rowTenderData['OkrugTitle'];

                $lotuid = $rowTenderData['LotUID'];
                $lot_number = $rowTenderData['LotNumber'];

                $tender['lots'][$lotuid]['tender_uid'] = $uid;
                $tender['lots'][$lotuid]['lot_number'] = $lot_number;
                $tender['lots'][$lotuid]['lot_uid'] = $lotuid;
                $tender['lots'][$lotuid]['lot_title'] = $rowTenderData['LotTitle'];
                $tender['lots'][$lotuid]['lot_cost'] = $rowTenderData['LotCost'];
                $tender['lots'][$lotuid]['date_public'] = $rowTenderData['PublicDate'];

                if($rowTenderData['ManagerID']<>"") $tender['lots'][$lotuid]['manager_id'] = $rowTenderData['ManagerID'];
                if($rowTenderData['ManagerID']<>"") $tender['lots'][$lotuid]['manager'] = $managers[intval($rowTenderData['ManagerID'])];
                if($rowTenderData['StatusName']<>"") $tender['lots'][$lotuid]['status'] = $rowTenderData['StatusName'];
                if($rowTenderData['StatusId']<>"") $tender['lots'][$lotuid]['status_id'] = $rowTenderData['StatusId'];
                if($rowTenderData['GroupID']<>"") $tender['lots'][$lotuid]['group_id'] = $rowTenderData['GroupID'];
                if($rowTenderData['GroupName']<>"") $tender['lots'][$lotuid]['group_name'] = $rowTenderData['GroupName'];
            }
    }
}

// --------------------------------------------------------------------------

// закрываем соединение
sqlsrv_close($sqlConnectId);

// --------------------------------------------------------------------------
// передача переменных во view
$smarty->assign("govruid", $govruid);
$smarty->assign("tenderuid", $tenderuid);
$smarty->assign("lotuid", $lotuid);

$smarty->assign("status_id", $status_id);
$smarty->assign("manager_id", $manager_id);

$smarty->assign("managers", $managers);
$smarty->assign("statuses", $statuses);
$smarty->assign("groups", $groups);

$smarty->assign("tender", $tender);

$smarty->assign("links", $links);

?>