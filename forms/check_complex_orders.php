<?php

//$pspell = pspell_new('ru','','','utf-8',PSPELL_BAD_SPELLERS);
$pspell_config = pspell_config_create('ru', '', '', 'utf-8');
pspell_config_personal($pspell_config, "/var/pspell/custom.pws");
$pspell = pspell_new_config($pspell_config);

/*
pspell_add_to_personal($pspell, "лещадной");
pspell_add_to_personal($pspell, "дробимости");
pspell_save_wordlist($pspell);
*/

// =========================================

// --------------------------------------------------------------------
if (!function_exists("array_column")) {
    function array_column($array, $column_name)
    {
        return array_map(function ($element) use ($column_name) {
            return $element[$column_name];
        }, $array);
    }
}

// сохраняем то что выбрано в провернных объектах

// --------------------------------------------------------------------
// отчет для удобной проверки ТЗ
require_once 'add/db.php';
require_once 'add/tables.php';
require_once 'add/functions/service_functions.php';
require_once 'add/forms/check_complex_orders_functions.php';

$debug = false;

// --------------------------------------------------------------
// setup SQL connection
// --------------------------------------------------------------
ini_set('max_execution_time', 900);
ini_set('mssql.charset', 'UTF-8');
ini_set('mssql.connect_timeout', '120');
ini_set('mssql.timeout', '600');
// connect to ms sql
$serverName = $databases["MSSQL"]["server"];
$connectionOptions = array(
    "Database" => "materialdb",
    "Uid" => $databases["MSSQL"]["user"],
    "PWD" => $databases["MSSQL"]["pass"],
    'ReturnDatesAsStrings'=>true
);
$sqlConnectId = sqlsrv_connect($serverName, $connectionOptions);

$checks = array(
    "req" => array
    (
        "title" => array(
            "name" => "Заголовок",
            "checks" => array(
                "matchest" => "Соотв. смете",
                "nosign" => "Без тов.знак",
                "noparams" => "Без параметр.",
                "values" => "Показатели",
            ),
        ),

        "description" =>
            array(
                "product" =>
                    array(
                        "name" => "Товар",
                        "checks" => array(
                            "title" => "Назв.",
                            "value" => "Знач.",
                            "measure" => "Ед. изм.",
                            "links" => "Связи",
                            "analogs" => "Аналоги",
                        )
                    ),

                "gost" => array(
                    "name" => "ГОСТ",
                    "checks" => array(
                        "title" => "Назв.",
                        "value" => "Знач.",
                        "measure" => "Ед. изм.",
                        "links" => "Связи",
                        "analogs" => "Аналоги",
                    )
                )

            ),
    ),

    "app" => array
    (
        "title" => array(
            "name" => "Заголовок",
            "checks" => array(
                "matchreq" => "Соотв. ТЗ",
                "havesign" => "Тов.знак",
                "havecountry" => "Страна ОКСМ",
                "values" => "Показатели",
            ),
        ),

        "description" =>
            array(
                "product" =>
                    array(
                        "name" => "ТЗ",
                        "checks" => array(
                            "title" => "Назв.",
                            "value" => "Знач.",
                            "measure" => "Ед. изм.",
                            "links" => "Связи"
                        )
                    ),

                "gost" => array(
                    "name" => "ГОСТ",
                    "checks" => array(
                        "title" => "Назв.",
                        "value" => "Знач.",
                        "measure" => "Ед. изм.",
                        "links" => "Связи",
                    )
                )
            ),
    )
);


$post_vars = getRealPOST();
if ($debug) echo "show project real: " . $post_vars['_show_project'] . "<br>";

$page_rnd = rand();
$page_rnd_submitted = $post_vars['page_rnd'];

$first_time = $post_vars["first_time"];

// фильтры ТЗ
$req_show = $post_vars["req_show"];
if ($first_time == "") $req_show = 1;
$req_status["00"] = $post_vars["req_status_00"];
$req_status["01"] = $post_vars["req_status_01"];
$req_status["02"] = $post_vars["req_status_02"];
$req_status["10"] = $post_vars["req_status_10"];
$req_check_status = $post_vars["req_check_status"];
$req_modifyuser = $post_vars["req_modifyuser"];

// фильтры заявки
$app_show = $post_vars["app_show"];
if ($first_time == "") $app_show = 1;
$app_status["00"] = $post_vars["app_status_00"];
$app_status["01"] = $post_vars["app_status_01"];
$app_status["02"] = $post_vars["app_status_02"];
$app_status["10"] = $post_vars["app_status_10"];
$app_check_status = $post_vars["app_check_status"];
$app_modifyuser = $post_vars["app_modifyuser"];

// формат отображения
$list_format = $post_vars["list_format"];
if ($list_format == "") $list_format = "Список новый";
$position_format = $post_vars["position_format"];
if ($position_format == "") $position_format = "Как в базе";

$show_compare = $post_vars["show_compare"];

$min_position = $post_vars["min_position"];
if ($min_position == "") $min_position = 0;
$max_position = $post_vars["max_position"];
if ($max_position == "") $max_position = 50;

// группировки и сортировки
$grouping = $post_vars["grouping"];
$sorting = $post_vars["sorting"];
if ($sorting == "") $sorting = "№ Смета";

// фильтры по зачени.
$est_num = $post_vars["est_num"];
$req_num = $post_vars["req_num"];
$est_title = $post_vars["est_title"];
$req_title = $post_vars["req_title"];
$app_title = $post_vars["app_title"];
$selected_group = $post_vars["selected_group"];
if ($selected_group == "") $sorting = "[ВСЕ ГРУППЫ]";

// =============================================================================
$projects = array();
$materials = array();
$show_project = "";
$num_project = -1;
$count_changed = 0;

// редиректы и смена режима
$line_id = $post_vars['_line_id'];
if ($line_id) {
    switch ($post_vars['_type']) {
        case "проект":
            if ($debug) echo "request type = проект<br>";
            $show_project = $line_id;
            break;
        case "заказ":
            if ($debug) echo "request type = заказ<br>";
            header("Location: " . $config["site_root"] . "/view_line2.php?table=271&filter=1891&line=" . $line_id . "&back_url=" . $base64_current_url);
            break;
    }
}

if ($show_project == "") $show_project = trim($post_vars["_show_project"]);

$projects2show = array();

$obj_groups = array();
$max_check_status = array();
$req_obj_updates = array();

// ------------------------------------------------------
// массивы для обработки данных об обновлении групп
// ------------------------------------------------------
$grpobj_checks = array();
$grpobj_texts = array();

// ------------------------------------------------------
// массивы для обработки данных о статусах проверки
// ------------------------------------------------------
$obj_allows = array();
$obj_comments = array();
$obj_checks = array();
$obj_statuses = array();

if ($post_vars['xsl'] == 1) {
    foreach ($post_vars as $param_name => $param_val) {
        $param_items = explode("_", $param_name);

        switch ($param_items[0]) {
            // -----------------------------------------
            // обновление названий групп
            // -----------------------------------------
            case "updategroupcheck":
                $grpobj_checks[$param_val] = $param_val;
                break;
            case "grpobjtext":
                $grpobj_texts[$param_items[1]] = $param_val;
                break;

            // -----------------------------------------
            // обновление названий групп
            // -----------------------------------------
            case "reqcheck":
            case "appcheck":
                $object_type = substr($param_items[0], 0, 3);

                switch ($param_items[1]) {
                    // комментарии по проверкам
                    case "objcomment":
                        $specific_uid = $param_items[2];
                        $obj_uid = $param_items[3];
                        $title_comment = trim($param_val);
                        $obj_comments[$object_type][$obj_uid][$specific_uid]['title']['text'] = $title_comment;
                        break;

                    case "linecomment":
                        $obj_uid = $param_items[2];
                        $specific_uid = $param_items[3];
                        $str_hash = $param_items[4];
                        $line_comment = $param_val;
                        $obj_comments[$object_type][$obj_uid][$specific_uid]['line'][$str_hash]['text'] = $line_comment;
                        break;

                    // чекбоксы по проверкам
                    case "title":
                        $check_param = $param_items[2];
                        $specific_uid = $param_items[3];
                        $obj_uid = $param_val;
                        $obj_checks[$object_type][$obj_uid][$specific_uid]['title'][$check_param] = 1;
                        break;

                    case "description":
                        $check_param_group = $param_items[2];
                        $check_param = $param_items[3];
                        $specific_uid = $param_items[4];
                        $str_hash = $param_items[5];
                        $obj_uid = $param_val;
                        $obj_checks[$object_type][$obj_uid][$specific_uid]['description'][$str_hash][$check_param_group][$check_param] = 1;
                        break;
                }
                break;

            // -----------------------------------------
            // список чекбоксов, разрещающих обновление данных по позиции
            // -----------------------------------------
            case "reqcheckallow":
            case "appcheckallow":
                $object_type = substr($param_items[0], 0, 3);
                $obj_allows[$object_type][$param_val] = $param_items[1]; // $req_uid => $obj_uid
                break;

            // -----------------------------------------
            // обновление общего статуса проверки
            // -----------------------------------------
            case "reqcheckstatus":
            case "appcheckstatus":
                $object_type = substr($param_items[0], 0, 3);
                $obj_statuses[$object_type][$param_items[1]][$param_items[2]] = $param_val; // $req_uid => $obj_uid
                break;
        }

        // старая обработка сохранений
        $param_pref = substr($param_name, 0, 7);
        switch ($param_pref) {
            case "project":
                $projects2show[$param_val] = $param_val; //echo $param_name." = $param_val<br>";
                break;

            // статусы
            case "chk_sta":
                $max_check_status[substr($param_name, 7)] = $param_val;
                break;

            case "grp_obj":
                $obj_groups[substr($param_name, 7)] = $param_val;
                break;

            case 'req_upd':
                $req_obj_updates[substr($param_name, 7)] = $param_val;
        }
    }
}

// готовим список проектов для отображения
$projects_uids = array();
if ($show_project <> "") $projects_uids[] = $show_project;
else $projects_uids = $projects2show;

$obj_types = array(
    "req" => array("type" => "ТЗ"),
    "app" => array("type" => "Заявка")
);

// =====================================================================
// обрабатываем данные о статуса и комментариях ТЗ и заявки
// =====================================================================
foreach ($obj_types as $obj_type => $obj_type_params) {
    // -------------------------------------------------------------------------------------------
    // обновляем данные о статусах проверки объектов
    // -------------------------------------------------------------------------------------------
    foreach ($obj_statuses[$obj_type] as $obj_uid => $obj_status) {
        if (in_array($obj_uid, $obj_allows[$obj_type], true)) {
            foreach ($obj_status as $specific_uid => $status) {
                $rowObjCheck = data_select_array($cb_tables['tableServiceObjChecks'], "status=0 and f16011='" . $obj_uid . "' and f15241='" . $specific_uid . "' and f15811='" . $obj_type_params['type'] . "' and f16021='Позиция в целом'");
                $exData = getExtendedPositionData($obj_uid, $show_project, $specific_uid, $obj_type, $sqlConnectId);

                if ($rowObjCheck['id'] == 0) {
                    $arr_status['status'] = 0;
                    $arr_status['f16011'] = $obj_uid;
                    $arr_status['f15241'] = $specific_uid;
                    $arr_status['f15341'] = $status;
                    $arr_status['f15811'] = $obj_type_params['type'];
                    $arr_status['f16021'] = "Позиция в целом";
                    $arr_status['f15261'] = $user['id'];

                    $arr_status['f15331'] = $show_project;
                    $arr_status['f15321'] = $exData['est_uid'];
                    $arr_status['f15291'] = $exData['version'];
                    $arr_status['f15301'] = $exData['modify_date'];
                    $arr_status['f15311'] = $exData['user'];

                    if ($status <> "") data_insert($cb_tables['tableServiceObjChecks'], EVENTS_ENABLE, $arr_status);
                } else {
                    $arr_status['f15341'] = $status; // check status
                    $arr_status['f15261'] = $user['id'];

                    $arr_status['f15331'] = $show_project;
                    $arr_status['f15321'] = $exData['est_uid'];
                    $arr_status['f15291'] = $exData['version'];
                    $arr_status['f15301'] = $exData['modify_date'];
                    $arr_status['f15311'] = $exData['user'];

                    if ($status <> "") data_update($cb_tables['tableServiceObjChecks'], EVENTS_ENABLE, $arr_status, "id=" . $rowObjCheck['id']);
                }
            }
        } // end if obj_allows
    } // end foreach obj_statuses

    // -------------------------------------------------------------------------------------------
    // обновляем данные о замечаниях по объектам
    // -------------------------------------------------------------------------------------------
    foreach ($obj_comments[$obj_type] as $obj_uid => $obj_comment) {
        if (in_array($obj_uid, $obj_allows[$obj_type], true)) {
            foreach ($obj_comment as $specific_uid => $obj_value) {
                // сохраняем примечение к объекту
                $exData = getExtendedPositionData($obj_uid, $show_project, $specific_uid, $obj_type, $sqlConnectId);
                $position_manager = "";
                if ($exData["user"] <> "")
                    if ($rowManager = data_select_array($cb_tables['tablePersonal'], "status=0 and f6631='" . $exData["user"] . "'"))
                        $position_manager = $rowManager['f1400'];

                $rowBug = data_select_array($cb_tables['tableServiceObjBugs'], "status=0 and f16151='$page_rnd_submitted'");
                // ------------------------------------------------------
                // комментарии
                if ($obj_value['title']['text'] <> "" && $rowBug['id'] == "") {
                    $arr_group = array();
                    $arr_group['status'] = 0;
                    $arr_group['f15891'] = $obj_uid;
                    $arr_group['f15901'] = $specific_uid; // specific uid
                    $arr_group['f15951'] = $obj_type_params['type'];
                    $arr_group['f15991'] = "Позиция в целом";
                    $arr_group['f15911'] = $obj_value['title']['text'];
                    $arr_group['f15931'] = $user['id']; // кто создал
                    $arr_group['f15941'] = $position_manager;

                    $arr_group['f16041'] = $exData["project_title"]; // проект
                    $arr_group['f16091'] = $exData["project_customer"]; // проект
                    $arr_group['f16051'] = $exData["num_smeta"]; // № смета
                    $arr_group['f16061'] = $exData["num_random"]; // № ТЗ
                    $arr_group['f16071'] = $exData["est_title"]; // название смета

                    $arr_group['f16121'] = $exData["version"]; // название смета
                    $arr_group['f16131'] = $exData["modify_date"]; // название смета

                    $arr_group['f16151'] = $page_rnd_submitted;

                    // всегда создаем новыую багу, удалять и править по ссылке
                    data_insert($cb_tables['tableServiceObjBugs'], EVENTS_ENABLE, $arr_group);
                }

                foreach ($obj_value['line'] as $line_hash => $line_value) {
                    if (trim($line_value['text']) <> "" && trim($line_hash) <> "" && $rowBug['id'] == "") {
                        $arr_group = array();
                        $arr_group['status'] = 0;
                        $arr_group['f15891'] = $obj_uid;
                        $arr_group['f15901'] = $specific_uid; // specific uid
                        $arr_group['f15981'] = $line_hash; // specific uid
                        $arr_group['f15951'] = $obj_type_params['type'];
                        $arr_group['f15991'] = "Строка в описании";
                        $arr_group['f15911'] = $line_value['text'];
                        $arr_group['f15931'] = $user['id']; // кто создал
                        $arr_group['f15941'] = $position_manager;

                        $arr_group['f16041'] = $exData["project_title"]; // проект
                        $arr_group['f16091'] = $exData["project_customer"]; // проект
                        $arr_group['f16051'] = $exData["num_smeta"]; // № смета
                        $arr_group['f16061'] = $exData["num_random"]; // № ТЗ
                        $arr_group['f16071'] = $exData["est_title"]; // название смета

                        $arr_group['f16121'] = $exData["version"]; // название смета
                        $arr_group['f16131'] = $exData["modify_date"]; // название смета

                        $arr_group['f16151'] = $page_rnd_submitted;

                        // всегда создаем новыую багу, удалять и править по ссылке
                        data_insert($cb_tables['tableServiceObjBugs'], EVENTS_ENABLE, $arr_group);
                    }
                }
            } // foreach req_uid
        } // end if allow
    } // end foreach $obj_comments

    // -------------------------------------------------------------------------------------------
    // обновляем данные о проверках объектов
    // -------------------------------------------------------------------------------------------
    foreach ($obj_checks[$obj_type] as $obj_uid => $obj_titlecheck) {
        if (in_array($obj_uid, $obj_allows[$obj_type], true)) {
            foreach ($obj_titlecheck as $specific_uid => $obj_value) {
                // ----------------------------------------------------------------------
                // смотрим все чекбоксы по заголовку
                $rowObjCheck = data_select_array($cb_tables['tableServiceObjChecks'], "status=0 and f16011='" . $obj_uid . "' and f15241='" . $specific_uid . "' and f15811='" . $obj_type_params['type'] . "' and f16021='Позиция в целом'");

                $check_status = "";
                foreach ($obj_value['title'] as $check_key => $check_flag)
                    $check_status .= $check_key . ";";

                $arr_check = array();
                if ($rowObjCheck['id'] == 0) {
                    $arr_check['status'] = 0;
                    $arr_check['f16011'] = $obj_uid;
                    $arr_check['f15241'] = $specific_uid;
                    $arr_check['f15821'] = $check_status; // check status
                    $arr_check['f15811'] = $obj_type_params['type'];
                    $arr_check['f16021'] = "Позиция в целом";
                    $arr_check['f15261'] = $user['id'];

                    if ($check_status <> "") data_insert($cb_tables['tableServiceObjChecks'], EVENTS_ENABLE, $arr_check);
                } else {
                    $arr_check['f15821'] = $check_status; // check status
                    $arr_check['f15261'] = $user['id'];
                    if ($check_status <> "") data_update($cb_tables['tableServiceObjChecks'], EVENTS_ENABLE, $arr_check, "id=" . $rowObjCheck['id']);
                    //else data_delete($cb_tables['tableServiceObjChecks'], EVENTS_ENABLE, "id=".$rowObjReqBug['id']);
                }

                // ----------------------------------------------------------------------
                // смотрим все чекбоксы по содержимому внутри позиций
                // ----------------------------------------------------------------------
                foreach ($obj_value['description'] as $str_hash => $check_groups) {
                    $rowObjCheck = data_select_array($cb_tables['tableServiceObjChecks'], "status=0 and f16011='" . $obj_uid . "' and f15241='" . $specific_uid . "' and f16031='" . $str_hash . "' and f15811='" . $obj_type_params['type'] . "' and f16021='Строка в описании'");
                    $arr_check = array();

                    $check_status = "";
                    foreach ($check_groups as $group_name => $check_params) {
                        $check_status .= $group_name . ":";

                        foreach ($check_params as $check_param => $check_flag)
                            $check_status .= $check_param . ";";

                        $check_status .= "|";
                    }

                    $arr_check = array();
                    if ($rowObjCheck['id'] == 0) {
                        $arr_check['status'] = 0;
                        $arr_check['f16011'] = $obj_uid;
                        $arr_check['f15241'] = $specific_uid;
                        $arr_check['f16031'] = $str_hash;
                        $arr_check['f15821'] = $check_status; // check status
                        $arr_check['f15811'] = $obj_type_params['type'];
                        $arr_check['f16021'] = "Строка в описании";
                        $arr_check['f15261'] = $user['id'];

                        if ($check_status <> "") data_insert($cb_tables['tableServiceObjChecks'], EVENTS_ENABLE, $arr_check);
                    } else {
                        $arr_check['f15821'] = $check_status; // check status
                        $arr_check['f15261'] = $user['id'];
                        if ($check_status <> "") data_update($cb_tables['tableServiceObjChecks'], EVENTS_ENABLE, $arr_check, "id=" . $rowObjCheck['id']);
                        //else data_delete($cb_tables['tableServiceObjChecks'], EVENTS_ENABLE, "id=".$rowObjReqBug['id']);
                    }
                }
            } // end foreach $reqobj_titlecheck
            //echo
        }
    }
} // end foreach obj_types


// -------------------------------------------------------------------------------------------
// обновляем данные о группах объектов
// -------------------------------------------------------------------------------------------
foreach ($grpobj_texts as $obj_uid => $group_name) {
    if (in_array($obj_uid, $grpobj_checks, true)) {
        // сохраняем для каждого проекта
        foreach ($projects2show as $proj_uid) {
            $rowObjGroup = data_select_array($cb_tables['tableServiceObjGroups'], "status=0 and f16001='$obj_uid' and f15421='$proj_uid'");

            $arr_group = array();
            $arr_group['status'] = 0;
            $arr_group['f16001'] = $obj_uid; // obj_uid_group
            $arr_group['f15421'] = $proj_uid;
            $arr_group['f15391'] = $group_name;

            //echo "tut $group_name $obj_uid<br>";

            if ($rowObjGroup['id'] == 0) {
                if (trim($group_name) <> "") data_insert($cb_tables['tableServiceObjGroups'], $arr_group);
            } else {
                if (trim($group_name) <> "") {
                    data_update($cb_tables['tableServiceObjGroups'], $arr_group, "id=" . $rowObjGroup['id']);
                } else {
                    data_delete($cb_tables['tableServiceObjGroups'], "id=" . $rowObjGroup['id']);
                }
            }
        }
    }
}

// -------------------------------------------------------------------------------------------
// получаем из базы данных объекты для сравнения
// -------------------------------------------------------------------------------------------
$groups = array();
$sqlQuery = "SELECT f15391 AS grp_name FROM " . DATA_TABLE . $cb_tables['tableServiceObjGroups'] . " WHERE status=0 GROUP BY f15391";
$res = sql_query($sqlQuery);
while ($row = sql_fetch_array($res)) $groups[] = $row['grp_name'];

if ($sqlConnectId) {
    // ******************************************************
    // получаем список проектов из БД материалов
    // ******************************************************
    getProjects($projects, $cb_tables, $sqlConnectId);

    // ******************************************************
    // дополняем статистику по всем комплексным заказам в CRM
    // ******************************************************

    $orders = data_select($cb_tables['tableOrders'],
        "status=0
                        and f4451=7
                        and f6551 in ('10. Исполнение', '20. Предоплата')
                        and f10240 in ('05. Оценка стоимости','10. Разработка','20. Проверка',
                                    '30. Публикация','35. Прием заявок','40. Протокол 1','50. Протокол 2','55. Заключение контракта')
                    ");
    while ($order = sql_fetch_assoc($orders)) {
        $orders_ids = array_column($projects, 'order_id');
        $key = in_array((int)($order['id']), $orders_ids);
        if (!$key) {
            $client = data_select_array($cb_tables['tableClients'], "status=0 and id=" . $order['f4441']);

            $next_project = array(
                'order_id' => (int)($order['id']),
                'order_num' => (int)($order['f7071']),
                'order_status' => $order['f6551'],
                'order_status_prod' => $order['f10240'],
                'project_title' => $order['f4431'],
                'customer_name' => $client['f435'],
            );
            $projects[] = $next_project;
        }
    }

    if ($debug) echo "after get projects from crm<br>";

    // пересортировываем по стаусам в CRM
    usort($projects, function ($a, $b) {
        return $b['order_status_prod'] < $a['order_status_prod'];
    });

    // ******************************************************
    // получаем статистику по всем проектам
    // ******************************************************
    $projects_total = array();
    getProjectsTotals($projects_total, $sqlConnectId);

    // ******************************************************
    // отображаем материалы из выбранного проекта
    // ******************************************************

    if ($show_project <> "" || count($projects2show) > 0) {
        $params = array();
        $params['sorting'] = $sorting;
        $params['grouping'] = $grouping;
        $params['filters'] = array();
        if (trim($est_num) <> "") $params['filters']['est_num'] = (int)($est_num);
        if (trim($req_num) <> "") $params['filters']['req_num'] = (int)($req_num);
        if (trim($selected_group) <> "") $params['filters']['selected_group'] = trim($selected_group);
        if (trim($est_title) <> "") $params['filters']['est_title'] = trim($est_title);
        if (trim($req_title) <> "") $params['filters']['app_title'] = trim($req_title);
        if (trim($app_title) <> "") $params['filters']['req_title'] = trim($app_title);
        if (trim($req_modifyuser) <> "") $params['filters']['req_modifyuser'] = trim($req_modifyuser);
        if (trim($app_modifyuser) <> "") $params['filters']['app_modifyuser'] = trim($app_modifyuser);

        if (trim($req_check_status) <> "") $params['check_filters']['req_check_status'] = $req_check_status;
        if (trim($app_check_status) <> "") $params['check_filters']['app_check_status'] = $app_check_status;

        if (trim($list_format) <> "") $params['show']['list_format'] = trim($list_format);
        if (trim($position_format) <> "") $params['show']['position_format'] = trim($position_format);
        if (trim($show_compare) <> "") $params['show']['show_compare'] = $show_compare;

        if (trim($min_position) <> "") $params['show']['min_position'] = $min_position;
        if (trim($max_position) <> "") $params['show']['max_position'] = $max_position;

        if (trim($req_show) <> "") $params['show']['req'] = trim($req_show);
        if (trim($app_show) <> "") $params['show']['app'] = trim($app_show);

        $materials = getEstimatesByProjects($projects_uids, $req_status, $app_status, $params, $cb_tables, $pspell, $sqlConnectId);

        if ($debug) echo "after get getEstimatesByProjects<br>";

        // обновляем галочки о проверенных материалах
        foreach ($materials['req_uids'] as $req_uid) {
            $row_req_obj_checked = data_select_array($cb_tables['tableServiceObjChecks'], "status=0 and f15241='$req_uid'");
            if ($row_req_obj_checked['id'] <> "") {
                // отмечаем чекбокс для отображения
                $req_obj_checked[$req_uid] = $req_uid;
                $max_check_status[$req_uid] = $row_req_obj_checked['f15341'];
            }
            //$max_check_status
        }
    } // end if show_project

} // end if sqlConnected

// =============================================================================================

$smarty->assign("site_root", $config["site_root"]);

$smarty->assign("projects", $projects);
$smarty->assign("projects_total", $projects_total);
$smarty->assign("count_changed", $count_changed);
$smarty->assign("materials", $materials);
$smarty->assign("groups", $groups);
$smarty->assign("checks", $checks);
$smarty->assign("page_rnd", $page_rnd);

$smarty->assign("max_check_status", $max_check_status);
$smarty->assign("num_projects", sizeof($materials['projects']));
$smarty->assign("projects_uids", $projects_uids);

// фильтры по статусам ТЗ
$smarty->assign("req_show", $req_show);
$smarty->assign("req_status_00", $req_status['00']);
$smarty->assign("req_status_01", $req_status['01']);
$smarty->assign("req_status_02", $req_status['02']);
$smarty->assign("req_status_10", $req_status['10']);
$smarty->assign("req_check_status", $req_check_status);
$smarty->assign("req_modifyuser", $req_modifyuser);

// фильтры по статусам заявки
$smarty->assign("app_show", $app_show);
$smarty->assign("app_status_00", $app_status['00']);
$smarty->assign("app_status_01", $app_status['01']);
$smarty->assign("app_status_02", $app_status['02']);
$smarty->assign("app_status_10", $app_status['10']);
$smarty->assign("app_check_status", $app_check_status);
$smarty->assign("app_modifyuser", $app_modifyuser);

$smarty->assign("first_time", $first_time);
$smarty->assign("show_project", $show_project);

// форматы списка и позиции
$smarty->assign("list_format", $list_format);
$smarty->assign("position_format", $position_format);
$smarty->assign("show_compare", $show_compare);

$smarty->assign("min_position", $min_position);
$smarty->assign("max_position", $max_position);

// сортировки и группировки
$smarty->assign("grouping", $grouping);
$smarty->assign("sorting", $sorting);

// фильтры по содержанию
$smarty->assign("est_num", $est_num);
$smarty->assign("req_num", $req_num);
$smarty->assign("est_title", $est_title);
$smarty->assign("req_title", $req_title);
$smarty->assign("app_title", $app_title);
$smarty->assign("selected_group", $selected_group);
