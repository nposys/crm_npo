<?php
/**
 * Created by PhpStorm.
 * User: miheev
 * Date: 10.07.2018
 * Time: 11:40
 */

require_once 'add/db.php';
require_once 'add/tables.php';
require_once 'add/functions/service_functions.php';
require_once 'add/vendor/autoload.php';

use classes\Base\Table;

try {
    if(isset($_POST['form_name']) & function_exists($_POST['form_name'])){
        $_POST['form_name']();
    }
}catch (Exception $ex){

}

 function actionRemovalDuplicateContractors()
 {
     try {
         //return;
         $variables =  data_select_array(1191, '`status`=0 AND f19991 = \'RemovalDuplicateContractors\'');
         set_time_limit(6000);
         $results = data_select_array(42, ALL_ROWS, '`status`=0 AND f435 <> \'\'  AND id >'.$variables['f20001'].' order by id LIMIT 3000');
         $rows=[];
         foreach ($results as $result){
             $rows[$result['id']] = $result;
         }
         while (count($rows) > 0) {
             $row = array_shift($rows);
             $row['f435'] = trim($row['f435']);
             $row['f1056'] = trim($row['f1056']);
             $row['f1057'] = trim($row['f1057']);
             if (isset($row['id']) & !empty($row['id'])) {
                 $variables['f20001'] = $row['id'];
                 $duplicateRowsQuery = ' status = 0 AND id>' . $row['id'] .
                     ' AND (f435=\'' . preg_replace("/(?<!\\\)\'/", '\\\'', $row['f435']) . '\' ';
                 if ($row['f1056'] != '') {
                     $duplicateRowsQuery = $duplicateRowsQuery . ' OR f1056=\'' . $row['f1056'] . '\'';
                 }
                 $duplicateRowsQuery = $duplicateRowsQuery . ')';
                 $duplicateRows = data_select_array(42, ALL_ROWS, $duplicateRowsQuery);

                 foreach ($duplicateRows as $duplicateRow) {

                     $duplicateRow['f435'] = trim($duplicateRow['f435']);
                     $duplicateRow['f1056'] = trim($duplicateRow['f1056']);
                     $duplicateRow['f1057'] = trim($duplicateRow['f1057']);
                     $checkName = $row['f435'] !='' & $duplicateRow['f435'] !='' &
                         $row['f435'] == $duplicateRow['f435'];

                     $checkInn= ($checkName &($row['f1056'] == '' |
                         $duplicateRow['f1056'] == '' )) |
                         ($duplicateRow['f1056'] == $row['f1056'] & $row['f1056'] != '');

                     $checkKpp= $checkInn & ($row['f1057'] == '' |
                         $duplicateRow['f1057'] == '' |
                         $duplicateRow['f1057'] == $row['f1057'] & $row['f1057'] != '') ;

                     if ($checkKpp) {
                        var_dump("REAL ".$row['id']."  Duplicate ".$duplicateRow['id']."");
                         foreach ($duplicateRow as $fieldName => $value) {
                             if ($fieldName != 'id' & $fieldName != 'user_id' & $fieldName != 'add_time' & $fieldName != 'status' & $fieldName != 'r'& $fieldName != 'u') {
                                 if (!is_null($value) & !empty($value) & $value != '0' & $value != '0000-00-00 00:00:00') {
                                     $row[$fieldName] = preg_replace('/(?<!\\\)\'/', '\\\'', $value);
                                 }
                             }
                         }
                         data_update(43, array('f839' => $row['id']), "`f839`=" . $duplicateRow['id']);//Счета(Клиент)
                         data_update(47, array('f1067' => $row['id']), "`f1067`=" . $duplicateRow['id']);//Задания(Клиент)
                         data_update(48, array('f508' => $row['id']), "`f508`=" . $duplicateRow['id']);//Расходы(Компания)
                         data_update(51, array('f545' => $row['id']), "`f545`=" . $duplicateRow['id']);//Сотрудники(Контрагент)
                         data_update(52, array('f570' => $row['id']), "`f570`=" . $duplicateRow['id']);//Платежное поручение(Получатель)
                         data_update(53, array('f589' => $row['id']), "`f589`=" . $duplicateRow['id']);//Накладная(Получатель)
                         data_update(53, array('f592' => $row['id']), "`f592`=" . $duplicateRow['id']);//Накладная(Через кого)
                         data_update(56, array('f615' => $row['id']), "`f615`=" . $duplicateRow['id']);//Доверенность(Поставщик)
                         data_update(57, array('f628' => $row['id']), "`f628`=" . $duplicateRow['id']);//ПКО(Принято от)
                         data_update(62, array('f723' => $row['id']), "`f723`=" . $duplicateRow['id']);//Работа с клиентами(Компания)
                         data_update(74, array('f7451' => $row['id']), "`f7451`=" . $duplicateRow['id']);//Позиции счета(Клиент)
                         data_update(78, array('f849' => $row['id']), "`f849`=" . $duplicateRow['id']);//Договор(Компания)
                         data_update(79, array('f855' => $row['id']), "`f855`=" . $duplicateRow['id']);//Счет-фактура(На кого)
                         data_update(81, array('f871' => $row['id']), "`f871`=" . $duplicateRow['id']);//Акт(Клиент)
                         data_update(87, array('f1118' => $row['id']), "`f1118`=" . $duplicateRow['id']);//Приход на склад(Поставщик)
                         data_update(88, array('f1129' => $row['id']), "`f1129`=" . $duplicateRow['id']);//Расход со склада(Покупатель)
                         data_update(130, array('f1750' => $row['id']), "`f1750`=" . $duplicateRow['id']);//Заявки(Клиент)
                         data_update(271, array('f4441' => $row['id']), "`f4441`=" . $duplicateRow['id']);//Заказы(Клиент)
                         data_update(311, array('f7950' => $row['id']), "`f7950`=" . $duplicateRow['id']);//Группы контрагентов(Основной контрагент)
                         data_update(371, array('f10190' => $row['id']), "`f10190`=" . $duplicateRow['id']);//Поиск(Клиент)
                         data_update(381, array('f7731' => $row['id']), "`f7731`=" . $duplicateRow['id']);//Заявка(Клиент)
                         data_update(401, array('f11781' => $row['id']), "`f11781`=" . $duplicateRow['id']);//Документация(Клиент)
                         data_update(441, array('f6071' => $row['id']), "`f6071`=" . $duplicateRow['id']);//Реестр ЭП(Клиент)
                         data_update(511, array('f7531' => $row['id']), "`f7531`=" . $duplicateRow['id']);//Поступления(Клиент)
                         data_update(531, array('f7871' => $row['id']), "`f7871`=" . $duplicateRow['id']);//Реестр логинов(Клиент)
                         data_update(560, array('f11150' => $row['id']), "`f11150`=" . $duplicateRow['id']);//Позиции счета поступления(Контрагент поступление)
                         data_update(590, array('f8620' => $row['id']), "`f8620`=" . $duplicateRow['id']);//Критерии поиска(Клиент)
                         data_update(620, array('f12531' => $row['id']), "`f12531`=" . $duplicateRow['id']);//Прочие(Клиент)
                         data_update(630, array('f10410' => $row['id']), "`f10410`=" . $duplicateRow['id']);//Данные клиента(Компания)
                         data_update(650, array('f11140' => $row['id']), "`f11140`=" . $duplicateRow['id']);//Позиции заказы счета поступления(Клиент)
                         data_update(751, array('f12621' => $row['id']), "`f12621`=" . $duplicateRow['id']);//Работа по заказам(Клиент)
                         data_update(871, array('f15081' => $row['id']), "`f15081`=" . $duplicateRow['id']);//Обучение(Клиент)
                         data_update(901, array('f15531' => $row['id']), "`f15531`=" . $duplicateRow['id']);//Вехи(Клиент)
                         data_update(980, array('f16960' => $row['id']), "`f16960`=" . $duplicateRow['id']);//Реестр Аккредитаций(Компания)
                         data_update(1000, array('f17160' => $row['id']), "`f17160`=" . $duplicateRow['id']);//Отрасли клиентов(Компания)
                         data_update(1010, array('f17290' => $row['id']), "`f17290`=" . $duplicateRow['id']);//Реестр лицензий(Клиент)
                         data_update(1071, array('f18851' => $row['id']), "`f18851`=" . $duplicateRow['id']);//Лог рассылок(42)
                         data_update(1121, array('f19071' => $row['id']), "`f19071`=" . $duplicateRow['id']);//Кредиты(Клиент)
                         data_update(1121, array('f19131' => $row['id']), "`f19131`=" . $duplicateRow['id']);//Кредиты(Банк)
                         data_update(1131, array('f19211' => $row['id']), "`f19211`=" . $duplicateRow['id']);//Потребности(Клиент)
                         data_update(42, array('status' => 1), "`id`=" . $duplicateRow['id']);
                     }
                     unset($rows[$duplicateRow['id']]);
                 }
                 $updateQuery = 'UPDATE ' . DATA_TABLE . '42  SET ';
                 $setString = '';
                 foreach ($row as $fieldName => $value) {
                     if ($fieldName != 'id' & $fieldName != 'user_id' & $fieldName != 'add_time' & $fieldName != 'status') {
                         if ($setString != '') {
                             $setString .= ', ';
                         }
                         $value = preg_replace("/(?<!\\\)\'/", '\\\'', $value);
                         $setString .= "$fieldName='$value'";
                     }
                 }
                 $updateQuery .= "$setString WHERE id = " . $row['id'] . ";";
                 sql_query($updateQuery);

                 data_update(1191,$variables,'`status`=0 AND f19991 = \'RemovalDuplicateContractors\'');
             }
         }
     } catch (Exception $ex) {
     }
 }

 function actionCorrectPhones()
 {
     $bDoCompanies = false;
     $bDoPersonal = true;

     if($bDoCompanies){
         $company = new Table("Контрагенты");
         $company->getData(["ID", "Телефон"],
             [
                 [
                     "field_name" => "Телефон",
                     "operator" => "<>",
                     "value" => "''"

                 ]

             ]);

         $count_done = 0;
         write_log("Num:".$company->num_rows);
         for($i=0;$i<$company->num_rows;$i++){
             $id = (int)$company->data[$i]['ID'];
             $phone_original = $company->data[$i]['Телефон'];
             $phone_clean = cleanupPhone($phone_original);

             if($phone_clean != $phone_original){
                 $company->data[$i]['Телефон'] = $phone_clean;
                 $company->save($id, false);
                 $count_done++;
             }
             //write_log("id:$id ".$phone_original." => ".$phone_clean);
         }

         write_log("обновлено $count_done записей");
     }


     // ===============================================================
     if($bDoPersonal){
         $company_personal = new Table("Сотрудники");
         $company_personal->getData(["ID", "Тел. рабочий", "Тел. сотовый"],
             [
                 [
                     "field_name" => "Тел. рабочий",
                     "operator" => "<>",
                     "value" => "''"
                 ],
                 [
                     "field_name" => "Тел. сотовый",
                     "operator" => "<>",
                     "value" => "''"
                 ]

             ], [], ["link_operator" => "or"]);

         $count_done_pers = 0;

         write_log("Num pers:".$company_personal->num_rows);

         for($i=0;$i<$company_personal->num_rows;$i++){
             $bDoSave = false;

             $id = (int)$company_personal->data[$i]['ID'];
             $phone_original_work = $company_personal->data[$i]['Тел. рабочий'];
             $phone_original_cell = $company_personal->data[$i]['Тел. сотовый'];

             $phone_clean_work = cleanupPhone($phone_original_work);
             $phone_clean_cell = cleanupPhone($phone_original_cell);

             if($phone_clean_work != $phone_original_work){
                 $company_personal->data[$i]['Тел. рабочий'] = $phone_clean_work;
                 $bDoSave = true;
             }
             if($phone_clean_cell != $phone_original_cell){
                 $company_personal->data[$i]['Тел. сотовый'] = $phone_clean_cell;
                 $bDoSave = true;
             }

             if($bDoSave){
                 $company_personal->save($id, false);
                 $count_done_pers++;
             }
             //write_log("id:$id work: ".$phone_original_work." => ".$phone_clean_work);
             //write_log("id:$id cell: ".$phone_original_cell." => ".$phone_clean_cell);
         }

         write_log("обновлено $count_done_pers записей сотрудников");

     }

 }


