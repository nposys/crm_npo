<?php

require_once 'add/vendor/autoload.php';

$controller  = new classes\Controllers\ExpertSearchCalculatorController();
if($controller->data['filters']){
	$calculate_result = $controller->calculateFilters();
	echo  $calculate_result;
	exit;
}else if($controller->data['save_order']){

	$save_result = $controller->createOrder();
	//echo json_encode($save_result);
	echo json_encode($controller->data);
	exit;
}
$filter_page=$controller->createFilterPage();
global $smarty;
$smarty->assign('filter_page', $filter_page);