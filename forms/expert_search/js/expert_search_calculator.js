

$("input.sum-filter").on({
    input: function() {
        var input = $(this);
        var input_val = input.val();
        input_val = input_val.replace(",","").replace(".","");
        if (input_val === "") {
            input.prop('title',0);
            return;
        }
        var original_len = input_val.length;
        var caret_pos = input.prop("selectionStart");
        input_val = input_val.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, " ");
        input.val(input_val);
        input.prop('title',input_val);
        var updated_len = input_val.length;
        caret_pos = updated_len - original_len + caret_pos;
        input[0].setSelectionRange(caret_pos, caret_pos);
        calculate($(this).closest('.filters_block'));
    }
});
$(".additional_filter_radio").on({
    change:function () {
        var value = this.value;
        $(this).closest('.additional-filter').find(".additional_filter_radio").prop("checked", false);
        $(this).prop("checked", true);
        if(value==1){
            $(this).closest('.additional-filter').find('.additional_filter_text').removeClass('hide');
        }else{
            $(this).closest('.additional-filter').find('.additional_filter_text:not(.hide)').addClass('hide');
        }
        calculate($(this).closest('.filters_block'));
    }
});
$("input.laws_filter").on({
    change:function () {
        calculate($(this).closest('.filters_block'));
    }
});

