jQuery.expr[':'].contains = function(a, i, m) {
    return jQuery(a).text().toUpperCase()
        .indexOf(m[3].toUpperCase()) >= 0;
};

$('.add-new-filter').on({
    click:function () {
        $('.filters-forms').append($('.base_filter .filters_block').clone(true));
        $('html').scrollTop($("html").get(0).scrollHeight);
    }
});
$('.copy-filter').on({
    click:function () {
        $('.filters-forms').append($(this).closest('.filters_block').clone(true));
        $('html').scrollTop($("html").get(0).scrollHeight);
        calculate_full_sum();
    }
});
$('.delete-filter').on({
    click:function () {
        var count = $('.filters-forms').find('.filters_block');
        if(count.length>1){
            $(this).closest('.filters_block').remove();
        }
        calculate_full_sum();
    }
});
$('.full-sum .list-group-item').on({
   click:function () {
       if(!$(this).hasClass('active')){
           $(this).closest('.list-group').find('.list-group-item.active').removeClass('active');
           $(this).closest('.list-group').find('.list-group-item.active .search-period').prop("checked", false);
           $(this).addClass('active');
           $(this).find('.search-period').prop("checked", true);
       }
   }
});


function calculate(form) {
    var laws = $(form).find('.laws_filter:checked');
    var regions = $(form).find('.tree-values[name="regions"]').val();
    var categories = $(form).find('.tree-values[name="categories"]').val();
    $(form).find('.filter-error:not(.hide)').addClass('hide');
    $(form).find('.filter-error').text();
    $(form).find('.result:not(.hide),.result-between:not(.hide)').addClass('hide');
    $(form).find('.result-cost').addClass('hide');
    $(form).find('.filter_sum').val(0);
    if(laws.length>0 && regions!='' && categories!='') {
        var data = [];
        var base_data = $(form).find('input').serialize();
        data.push("filters=1&csrf=" + csrf);
        data.push(base_data);
        var form_data = data.join('&');
        $.ajax({ // Формируем запрос
            type: "POST",
            dataType: 'json',
            data: form_data,
            success: function (res) { // Сообщение с сервера
                if (res.success == 1) {
                    $(form).find('.result-sum').text(res.result_cost);
                    $(form).find('.filter_sum').val(res.result_cost);
                    if (res.not_final_cost) {
                        $(form).find('.result-between').removeClass('hide');
                    } else {
                        $(form).find('.result').removeClass('hide');
                    }
                    $(form).find('.result-cost').removeClass('hide');
                } else {
                    if (res.error.categories) {
                        $(form).find('.categories-error').removeClass('hide').text(res.error.categories);
                    }
                    if (res.error.regions) {
                        $(form).find('.regions-error').removeClass('hide').text(res.error.regions);
                    }
                    if (res.error.sums) {
                        $(form).find('.sums-error').removeClass('hide').text(res.error.sums);
                    }
                    if (res.error.laws) {
                        $(form).find('.laws-error').removeClass('hide').text(res.error.laws);
                    }
                }
                calculate_full_sum();
                console.log(res);
            },
            error: function (res) {
                console.log(res);
            }
        });
    }else{
        $(".full-sum").addClass('hide')
    }
}

function calculate_full_sum() {
    var sums = $(".filters-forms .filter_sum");
    var result_sum=0.0;
    for(var i =0;i<sums.length;i++){
        result_sum+=parseFloat($(sums[i]).val().replace(/\s/g,""));
    }
    if(result_sum>0) {
        switch (sums.length) {
            case 1:
                break;
            case 2:
                result_sum = result_sum * 0.95;
                break;
            case 3:
                result_sum = result_sum * 0.90;
                break;
            default:
                result_sum = result_sum * 0.75;
                break;
        }
        $("#per-month").text(result_sum);
        $("#per-year").text(result_sum * 12 * 0.8);
        $("#six-month").text(result_sum * 6 * 0.9);
        $("#three-month").text(result_sum * 3 * 0.95);
        $(".full-sum").removeClass('hide')
    }else {
        $("#per-month").text('');
        $("#per-year").text('');
        $("#six-month").text('');
        $("#three-month").text('');
    }
}

$('.create-order').on({
    click: function () {
        var base_data = [];
        var filters = $('.result-cost:not(.hide)').closest('.filters_block');
        var period = $('.full-sum .active .search-period').val();
        var order_cost = $('.full-sum .active .order-cost').text();
        var company_id = $('#company_id').val();
        if (filters.length > 0) {
            var i;
            for (i = 0; i < filters.length; i++) {
                base_data[i] = $(filters[i]).find('input,textarea').serializeArray();
            }
        }
        $.ajax({ // Формируем запрос
            type: "POST",
            dataType: 'json',
            data: {
                filter:base_data,
                save_order:1,
                period:period,
                order_cost:order_cost,
                company_id:company_id,
                csrf:csrf
            },
            success: function (res) {
                console.log(1);
                console.log(res);
            },
            error:function (res) {
                console.log(0);
                console.log(res);
            }
        });
    }
});