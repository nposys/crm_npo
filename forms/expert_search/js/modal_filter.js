
var target_modal_button;
$(document).ready(function(){
    $('.tree-modal').on('show.bs.modal', function(e) {
        $(this).closest('#category_select').find('.Node.disable').removeClass('disable');
        $(this).find("input:checked").prop("checked", false);
        $(this).find(".Node.ExpandOpen").addClass('ExpandClosed').removeClass('ExpandOpen');
        target_modal_button = e.relatedTarget;
        var value =$(target_modal_button).closest('div').find(".tree-values").val();
        if(value!='') {
            var values = value.split(",");
            for (var i = 0; i < values.length; i++) {
                if (intval(values[i]) > 0) {
                    var checkbox = $(this).find("input[value='" + values[i] + "']");
                    $(checkbox).closest('.Node').find('>input').prop("checked", true);
                }
            }
            if (this.id == 'category_select') {
                $(this).find("input:checked:last").change()
            }
            $(this).find(".Node.ExpandClosed:has(*input:checked)").addClass('ExpandOpen').removeClass('ExpandClosed');
        }
    });
    $('.tree-modal').on('hide.bs.modal', function(e) {
        var checked=$(this).find('input:checked');
        var values=[];
        var i;
        for( i =0;i<checked.length;i++){
            if(intval(checked.get(i).value)>0) {
                values.push(checked.get(i).value);
            }
        }
        var value = values.join(",");
        $(target_modal_button).closest('div').find(".tree-values").val(value);

        var titles_array=$(this).find('.Node:not(.level1)>input:checked').closest(".Node").find(">.Content");
        var titles=[];
        for(i =0;i<titles_array.length;i++){
            if(titles_array.get(i).innerText!='') {
                titles.push($.trim(titles_array.get(i).innerText));
            }
        }
        var title_string = titles.join(", ");
        $(target_modal_button).closest('td').find(".filter-result").text(title_string);

        calculate($(target_modal_button).closest('.filters_block'));
        target_modal_button=null;
    });
});

$('.Expand:not(.disable)').on('click',function () {
    var node = this.parentNode;
    var newClass,oldClass;
    if($(node).hasClass('ExpandOpen')){
        newClass =  'ExpandClosed';
        oldClass =  'ExpandOpen';
        $(node).find('.Node.ExpandOpen').addClass(newClass);
        $(node).find('.Node.ExpandOpen').removeClass(oldClass);
    }else{
        newClass = 'ExpandOpen';
        oldClass = 'ExpandClosed' ;
    }
    $(node).addClass(newClass);
    $(node).removeClass(oldClass);
});

$('.Node .tree_checkbox[type=checkbox]').on('change',function () {
    $(this).closest('.Node').find(".Node .tree_checkbox[type=checkbox]").prop("checked", this.checked);

    // var parent_container = $(this).closest('.Container');
    // var unchecked =0;
    // while(parent_container.length>0){
    //     unchecked = $(parent_container).find(" .tree_checkbox[type=checkbox]:not(:checked)");
    //     $(parent_container).closest('.Node').find('.tree_checkbox[type=checkbox]:first').prop("checked", unchecked.length === 0);
    //     parent_container = $(parent_container).closest('.Node').closest('.Container');
    // }
});

$('#category_select .tree_checkbox').on('change',function () {
    var checked =$(this).closest('.tree-filter').find(".tree_checkbox:checked");
    if(checked.length>0){
        $(this).closest('.tree-filter').find('.Node:not(.disable)').addClass('disable');
        $(this).closest('.Node.level3').removeClass('disable').find(".Node.disable").removeClass('disable');
    }else{
        $(this).closest('.tree-filter').find('.Node.disable').removeClass('disable');
    }
});

$('.tree-filter .text-filter input[type="text"]').on('input',function () {
    var text = $(this).val();
    var tree_filter=$(this).closest('.tree-filter');
    if(text.length>=3){
        $(tree_filter).find('.ExpandOpen')
            .addClass('ExpandClosed').removeClass('ExpandOpen');
        $(tree_filter).find(".Node")
            .addClass('unmatch').removeClass('last').removeClass('match');
        $(tree_filter).find(".Description:not(.hide)").addClass('hide');
        $(tree_filter).find(".Node:has(*:contains('"+text+"'))")
            .removeClass('unmatch');
        $(tree_filter).find(".Node:has(>.Description:contains('"+text+"'),>.Content:contains('"+text+"'))")
            .addClass('match');
        $(tree_filter).find(".ExpandClosed:has(.match)")
            .addClass('ExpandOpen').removeClass('ExpandClosed');
        $(tree_filter).find(".match:not(:has(.match))")
            .find('.unmatch').removeClass('unmatch');
        $(tree_filter).find('.Node:has(.match),.match').closest('.Container').each(function (index, value) {
            var nodes = $(this).children('.Node:not(.unmatch)');
            $(nodes.last()).addClass('last');
        });

        $(tree_filter).find(".match>.Description:contains('"+text+"')").removeClass('hide');
    }else{
        $(tree_filter).find(".Node").removeClass('match').removeClass('unmatch').removeClass('last');
    }
});

$('button.btn-clear').on({
    click: function () {
        $(this).closest('.text-filter').find('input[type=text]').val('').trigger('input');
    }
});


$('.tree-modal .clear-tree').on({
    click:function () {
        if($(this).closest('.tree-modal').prop('id')=="category_select") {
            $(this).closest('.tree-modal').find('.Node.disable').removeClass('disable');
        }
        $(this).closest('.tree-modal').find("input:checked").prop("checked", false);
    }
});