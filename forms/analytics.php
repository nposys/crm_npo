<?php
/**
 * Created by PhpStorm.
 * User: miheev
 * Date: 26.04.2019
 * Time: 11:12
 */

if ($_REQUEST['date1']) {
    $date1 = date("d.m.Y", strtotime(form_eng_time($_REQUEST['date1'])));
    $date1_select = date("Y-m-d 00:00:00", strtotime(form_eng_time($_REQUEST['date1'])));
}else {
    $date1 = date("d.m.Y", strtotime(date("Y-m-d")." - 7 days"));
    $date1_select = date("Y-m-d 00:00:00", strtotime(date("Y-m-d")." - 7 days"));
}

if ($_REQUEST['date2']) {
    $date2 = date("d.m.Y", strtotime(form_eng_time($_REQUEST['date2'])));
    $date2_select = date("Y-m-d 23:59:59", strtotime(form_eng_time($_REQUEST['date2'])));
}
else {
    $date2 = date("d.m.Y");
    $date2_select = date("Y-m-d 23:59:59");
}

function formatNumericData(&$data, $level = 0)
{
    foreach ($data as $key => &$value){
        if(is_array($value) && $level<10){
            formatNumericData($value, $level+1);
        } else {
            if (is_float($value)){
                $value = number_format($value,2,',',' ');
            } else if( is_int($value)) {
                $value = number_format($value,0,',',' ');

            }
        }
    }
}
global $user;
$user_group = (int)$user['group_id'];
$order_where = " AND f4451 != 7 ";
if($user_group===1){
    $order_where="";
}

$query_first = "SELECT 
                    COUNT(id) as num,
                    f552 AS lid_stat,
                    f4921 AS client_stat,
                    f772 AS type,
                    (num_actions > 0) AS actions_exists
                FROM
                    cb_data42 AS cli
                        LEFT JOIN
                    (SELECT 
                        COUNT(id) AS num_actions, f723
                    FROM
                        cb_data62
                    WHERE
                        status = 0 AND user_id > 0
                            OR f1053 = 'Да'
                    GROUP BY f723) AS actions ON actions.f723 = cli.id
                WHERE
                    status = 0
                GROUP BY lid_stat , client_stat ,type, actions_exists";


$result_first = sql_query($query_first);
$export_first=[];
while($row = sql_fetch_assoc($result_first)){
    $export_first['total']+=(float)$row["num"];
    if((int)$row['actions_exists']==0) {
        $export_first['automatic'] += (float)$row["num"];
    }else{
        $export_first['with_actions'] += (float)$row["num"];

        if($row['type']=='Клиент') {
            if($row['client_stat']==''){
                $row['client_stat']='Неизвестен';
            }
            $export_first['client'][$row['client_stat']] += (float)$row["num"];
        }else if($row['type']=='Потенциальный') {
            if($row['lid_stat']==''){
                $row['lid_stat']='Неизвестен';
            }
            $export_first['lid'][$row['lid_stat']] += (float)$row["num"];
        }else {
            $export_first['other'] += (float)$row["num"];
        }
    }
}
arsort($export_first['lid']);
arsort($export_first['client']);
formatNumericData($export_first);


$query_second = "SELECT 
                    COUNT(cli.id) as num,
                    f21111 != '' AS metrica,
                    user_id > 0 AS create_manager,
                    f552 AS lid_stat,
                    f4921 AS client_stat,
                    f4881 AS active,
                    f4891 AS size,
                    f772 AS type,
                    sum(count) as invoice_count
                FROM
                    cb_data42 AS cli
                        LEFT JOIN
                    (SELECT 
                        f4441,count(id) as count
                    FROM
                        cb_data271 AS ord
                    WHERE
                        f10780 > 0 AND status = 0 AND f6551 <'97' AND f4451=4 $order_where
                    GROUP BY f4441) AS tOrd ON tOrd.f4441 = cli.id
                WHERE
                    add_time >= '$date1_select'
                        AND add_time <= '$date2_select'
                        AND status = 0
                        AND cli.id !=3813
                GROUP BY lid_stat , client_stat ,type, active , size , metrica , create_manager";
$result_second = sql_query($query_second);
$export_second=[];
while($row = sql_fetch_assoc($result_second)){
    $export_second['total']+=(float)$row["num"];


    if($row['type']=='Клиент') {
        if($row['client_stat']==''){
            $row['client_stat']='Неизвестен';
        }
        $export_second['client'][$row['client_stat']]+=(float)$row["num"];
    }else if($row['type']=='Потенциальный') {
        if($row['lid_stat']==''){
            $row['lid_stat']='Неизвестен';
        }
        $export_second['lid'][$row['lid_stat']]+=(float)$row["num"];
    }else {
        $export_second['other']+=(float)$row["num"];
    }
    if((int)$row["metrica"]){
        $export_second['metrica']+=(float)$row["num"];
        $export_second['invoice_count']+=(float)$row["invoice_count"];
    }else{
        $export_second['other_users']+=(float)$row["num"];

        if(!isset($row['active'])|| $row['active']==''){
            $row['active']='нет данных';
        }
        if(!isset($row['size'])|| $row['size']==''){
            $row['size']='нет данных';
        }
        $export_second['active'][$row['active']]+=(float)$row["num"];
        $export_second['size'][$row['size']]+=(float)$row["num"];
    }
}
arsort($export_second['lid']);
arsort($export_second['client']);
ksort($export_second['active']);
ksort($export_second['size']);
$query_second = "SELECT 
                        SUM(f4461) AS total, SUM(f10270) AS paid
                    FROM
                        cb_data271 AS ord
                    WHERE
                        f4461 > 0 AND status = 0 $order_where
                            AND f6551 <'97'
                            AND add_time >= '$date1_select'
                            AND add_time <= '$date2_select'
                            AND f4441!=3813";
$result_second = sql_query($query_second);
$row = sql_fetch_assoc($result_second);
$export_second['total_sum']+=(float)$row["total"];
$export_second['paid_sum']+=(float)$row["paid"];
formatNumericData($export_second);



$query_third = "SELECT 
                    COUNT(cli.id) as num,
                    f552 AS lid_stat,
                    f4921 AS client_stat,
                    f772 AS type,
                    EXISTS(SELECT 
                            *
                        FROM
                            cb_data271 AS o
                                LEFT JOIN
                            cb_data371 AS se ON o.id = f5411
                                LEFT JOIN
                            cb_data381 AS ap ON o.id = f5471
                        WHERE
                            o.status = 0 AND f4441 = cli.id $order_where
                                AND o.f6551 < '97'
                                AND ((o.add_time <= '$date2_select'
                                AND o.f13361 >= '$date1_select'
                                AND se.id IS NULL
                                AND ap.id IS NULL)
                                OR (se.status = 0
                                AND se.f6941 <= '$date2_select'
                                AND se.f6951 >= '$date1_select')
                                OR (ap.status = 0
                                AND ap.add_time <= '$date2_select'
                                AND ap.f7711 >= '$date1_select'))) as cur_task
                FROM
                    cb_data42 AS cli
                    WHERE status = 0
                    AND f772!='Партнер'
                    AND EXISTS(SELECT 
                            *
                        FROM
                            cb_data271 AS o
                                LEFT JOIN
                            cb_data371 AS se ON o.id = f5411
                                LEFT JOIN
                            cb_data381 AS ap ON o.id = f5471
                        WHERE
                            o.status = 0 AND f4441 = cli.id $order_where)
				GROUP BY lid_stat,client_stat,type,cur_task";
$result_third = sql_query($query_third);
$export_third=['total'=>0,'other_users'=>0,'tm_users'=>0];
while($row = sql_fetch_assoc($result_third)){
    $export_third['total']+=(float)$row["num"];
    if($row['cur_task']>0){
        $export_third['cur_clients']+=(float)$row["num"];
    }
    if($row['type']=='Клиент') {
        if($row['client_stat']==''){
            $row['client_stat']='Неизвестен';
        }
        $export_third['client'][$row['client_stat']]+=(float)$row["num"];
    }else if($row['type']=='Потенциальный') {
        if($row['lid_stat']==''){
            $row['lid_stat']='Неизвестен';
        }
        $export_third['lid'][$row['lid_stat']]+=(float)$row["num"];
    }else {
        $export_third['other']+=(float)$row["num"];
    }
}
arsort($export_third['lid']);
arsort($export_third['client']);
formatNumericData($export_third);


$query_fourth = "SELECT 
                    count(cli.id) as num,
                    SUM(invoice_num) as invoice_num,
                    SUM(total_sum) as total_sum,
                    SUM(paid_sum) as paid_sum,
                    total_sum>0  AS paid_subscription,
                    is_search,
                    SUM(task_yes) as task_yes,
                    SUM(task_no) as task_no,
                    f11161 as category
                FROM
                    cb_data42 AS cli
                    INNER JOIN
                    (SELECT 
						f4441,
						COUNT(o.id) AS invoice_num,
						SUM(f4461) AS total_sum,
						SUM(f10270) AS paid_sum,
						MAX(f4451 = 4) AS is_search
					FROM
						cb_data271 AS o
					WHERE
						o.status = 0 AND o.f6551 < '97'
							AND o.f6551 > '06'
							AND ((o.add_time <= '$date2_select'
							AND o.f13361 >= '$date1_select'
							AND NOT EXISTS( SELECT 
								*
							FROM
								cb_data371 AS se
							WHERE
								se.status = 0 AND o.id = f5411)
							AND NOT EXISTS( SELECT 
								*
							FROM
								cb_data381 AS ap
							WHERE
								ap.status = 0 AND o.id = f5471))
							OR EXISTS( SELECT 
								*
							FROM
								cb_data371 AS se
							WHERE
								se.status = 0 AND o.id = f5411
									AND se.f6941 <= '$date2_select'
									AND se.f6951 >= '$date1_select')
							OR EXISTS( SELECT 
								*
							FROM
								cb_data381 AS ap
							WHERE
								ap.status = 0 AND o.id = f5471
									AND ap.add_time <= '$date2_select'
									AND ap.f7711 >= '$date1_select'))
									
										GROUP BY f4441) as ord ON f4441 = cli.id
                    LEFT JOIN (SELECT 
                        f723,
                            COUNT(CASE
                                WHEN f1053 = 'Да' THEN 1
                                ELSE NULL
                            END) AS task_yes,
                            COUNT(CASE
                                WHEN f1053 = 'Да' THEN NULL
                                ELSE 1
                            END) AS task_no
                    FROM
                        cb_data62
                    WHERE
                        status = 0
                    GROUP BY f723) AS tTask ON tTask.f723 = cli.id
                WHERE
                    status = 0
                GROUP BY is_search,paid_subscription,f11161";
$result_fourth = sql_query($query_fourth);
$export_fourth=[];
while($row = sql_fetch_assoc($result_fourth)){
    if($row["paid_subscription"]==1){
        $export_fourth['invoice_num']+=(float)$row["invoice_num"];
        if($row["is_search"]==1){
            $export_fourth['paid_search']['sum']+=(float)$row["num"];
            $export_fourth['paid_search']['task_yes']+=(float)$row["task_yes"];
            $export_fourth['paid_search']['task_total']+=(float)$row["task_yes"]+=(float)$row["task_no"];
        }else{
            $export_fourth['npo']['sum']+=(float)$row["num"];
            $export_fourth['npo']['task_yes']+=(float)$row["task_yes"];
            $export_fourth['npo']['task_total']+=(float)$row["task_yes"]+=(float)$row["task_no"];
        }
    }else{
        if($row["is_search"]==1){
            $export_fourth['free_search']['sum']+=(float)$row["num"];
            $export_fourth['free_search']['task_yes']+=(float)$row["task_yes"];
            $export_fourth['free_search']['task_total']+=(float)$row["task_yes"]+=(float)$row["task_no"];
        }else{
            $export_fourth['npo']['sum']+=(float)$row["num"];
            $export_fourth['npo']['task_yes']+=(float)$row["task_yes"];
            $export_fourth['npo']['task_total']+=(float)$row["task_yes"]+=(float)$row["task_no"];
        }
    }
    $export_fourth['total_sum']+=(float)$row["total_sum"];
    $export_fourth['paid_sum']+=(float)$row["paid_sum"];
    $export_fourth['category'][$row["category"]]+=(float)$row["num"];

}
formatNumericData($export_fourth);


if($_REQUEST['xsl'] == 1){

    $xsl= "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"></head><body>\r\n";
    $xsl.= "<table class=\"table table-striped table-bordered table-hover\">
	
	<tr>
		<th colspan=\"2\">1. Общий блок - описание клиентской базы</th>
	</tr>
	<tr>
		<th width=\"300\">1.Контактов в базе</th>
		<td width=\"100\">".$export_first["total"]."</td>
	</tr>
	<tr>
		<th>Автоматических</th>
		<td>".$export_first["automatic"]."</td>
	</tr>
	<tr>
		<th>Клиенты с действиями</th>
		<td>".$export_first["with_actions"]."</td>
	</tr>
	<tr>
		<th>Клиенты</th>
		<th ></th>
	</tr>
	";
    foreach( $export_first["client"] AS $key=>$value) {
        $xsl.= "<tr>
		<td>$key</td>
		<td>$value</td>
	</tr>";
    }
    $xsl.= "<tr>
		<th>Лиды</th>
		<th ></th>
	</tr>
	";
    foreach ($export_first["lid"] AS $key=>$value) {
        $xsl.= "<tr>
		<td>$key</td>
		<td>$value</td>
	</tr>";
    }
    $xsl.= "<tr>
		<th>Другие</th>
		<th >".$export_first["other"]."</th>
	</tr>
	
	
	
	<tr>
		<th colspan=\"2\">2. Блок Тендер монитор. Регистрации ТМ и выгрузки</th>
	</tr>
	<tr>
		<th>1. Всего регистраций за период</th>
		<td>".$export_second["total"]."</td>
	</tr>
	<tr>
		<th>Клиенты</th>
		<th ></th>
	</tr>
	";
    foreach($export_second["client"] AS $key=>$value) {
        $xsl.= "<tr>
		<td>$key</td>
		<td>$value</td>
	</tr>";
    }
    $xsl.= "<tr>
		<th>Лиды</th>
		<th ></th>
	</tr>
	";
    foreach($export_second["lid"] AS $key=>$value) {
        $xsl.= "<tr>
		<td>$key</td>
		<td>$value</td>
	</tr>";
    }
    $xsl.= "<tr>
		<th>Другие</th>
		<th >".$export_second["other"]."</th>
	</tr>
	<tr>
		<th>2. Регистрации ТМ (по userID метрики Яндекса)</th>
		<td>".$export_second["metrica"]."</td>
	</tr>
	<tr>
		<th>2.1 Выписано счетов по регистрации</th>
		<td>".$export_second["invoice_count"]."</td>
	</tr>
	<tr>
		<th>2.2 Сумма счетов, выписано</th>
		<td>".$export_second["total_sum"]."</td>
	</tr>
	<tr>
		<th>2.3 Сумма счетов, оплачено</th>
		<td>".$export_second["paid_sum"]."</td>
	</tr>
	<tr>
		<th>3.Чужие от выгрузки</th>
		<td>".$export_second["other_users"]."</td>
	</tr>
	<tr>
		<th class=\"sub-title\">Категория по активности</th>
		<th class=\"sub-title left-align\">Число</th>
	</tr>
    ";
    foreach($export_second["active"] AS $key=>$value) {
        $xsl.= "<tr>
		<td>$key</td>
		<td>$value</td>
	</tr>";
    }
    $xsl.= "<tr>
		<th class=\"sub-title\">Категория по размеру</th>
		<th class=\"sub-title left-align\">Число</th>
	</tr>
    ";
    foreach($export_second["size"] AS $key=>$value) {
        $xsl.= "<tr>
		<td>$key</td>
		<td>$value</td>
	</tr>";
    }

    $xsl.= "<tr>
		<th colspan=\"2\">3.А Реально рабочая клиентская база за период - по кому велась работа</th>
	</tr>
	
	
	<tr>
		<th>1. Клиенты компании в работе</th>
		<td>".$export_third["cur_clients"]."</td>
	</tr>
	<tr>
		<th>1.1 Всего клиентов в базе</th>
		<td>".$export_third["total"]."</td>
	</tr>
	<tr>
		<th>1.1 Статусы клиентов</th>
		<th></th>
	</tr>
	<tr>
		<th>Клиенты</th>
		<th ></th>
	</tr>
	";
    foreach($export_third["client"] AS $key=>$value) {
        $xsl.= "<tr>
		<td>$key</td>
		<td>$value</td>
	</tr>";
    }
    $xsl.= "<tr>
		<th>Лиды</th>
		<th ></th>
	</tr>
	";
    foreach($export_third["lid"] AS $key=>$value) {
        $xsl.= "<tr>
		<td>$key</td>
		<td>$value</td>
	</tr>";
    }
    $xsl.= "<tr>
		<th>Другие</th>
		<th >".$export_third["other"]."</th>
	</tr>
	



	
	<tr>
		<th colspan=\"2\">3.Б.  Описываем клиентов+деньги на момент отчета</th>
	</tr>
	
	
	<tr>
		<th>1. Выписано счетов</th>
		<td>".$export_fourth["invoice_num"]."</td>
	</tr>
	<tr>
		<th>2. Сумма счетов</th>
		<td>".$export_fourth["total_sum"]."</td>
	</tr>
	<tr>
		<th>3. Оплачено счетов</th>
		<td>".$export_fourth["paid_sum"]."</td>
	</tr>
	<tr>
		<th>4.Клиенты на платной подписке Тендермонитор</th>
		<td>".$export_fourth["paid_search"]["sum"]."</td>
	</tr>
	<tr>
		<th>Действий запланировано</th>
		<td>".$export_fourth["paid_search"]["task_total"]."</td>
	</tr>
	<tr>
		<th>Действий выполнено</th>
		<td>".$export_fourth["paid_search"]["task_yes"]."</td>
	</tr>
	<tr>
		<th>4.1 Клиенты оплаченные услуги НПО Система</th>
		<td>".$export_fourth["npo"]["sum"]."</td>
	</tr>
	<tr>
		<th>Действий запланировано</th>
		<td>".$export_fourth["npo"]["task_total"]."</td>
	</tr>
	<tr>
		<th>Действий выполнено</th>
		<td>".$export_fourth["npo"]["task_yes"]."</td>
	</tr>
	<tr>
		<th>5.Клиенты бесплатной подписки/тестовый период </th>
		<td>".$export_fourth["free_search"]["sum"]."</td>
	</tr>
	<tr>
		<th>Действий запланировано</th>
		<td>".$export_fourth["free_search"]["task_total"]."</td>
	</tr>
	<tr>
		<th>Действий выполнено</th>
		<td>".$export_fourth["free_search"]["task_yes"]."</td>
	</tr>
	<tr>
		<th>6.Клиенты по Категории</th>
		<td></td>
	</tr>
	";
    foreach($export_fourth["category"] AS $key=>$value) {
        $xsl.= "<tr>
		<td>$key</td>
		<td>$value</td>
	</tr>";
    }
    $xsl.= "</table>";


    $name = 'analytics.xls';
    header("Content-type: application/vnd.ms-excel; charset=utf-8");
    header("Content-Disposition: attachment; filename=".$name);
    echo $xsl;
    exit;
}else {

    $smarty->assign("date1", $date1);
    $smarty->assign("date2", $date2);
    $smarty->assign("export_first", $export_first);
    $smarty->assign("export_second", $export_second);
    $smarty->assign("export_third", $export_third);
    $smarty->assign("export_fourth", $export_fourth);
}