<?php

include 'add/lib/diff-match-patch-master/src/DiffMatchPatch.php';
include 'add/lib/diff-match-patch-master/src/Diff.php';
include 'add/lib/diff-match-patch-master/src/DiffToolkit.php';
include 'add/lib/diff-match-patch-master/src/Match.php';
include 'add/lib/diff-match-patch-master/src/Patch.php';

use DiffMatchPatch\DiffMatchPatch;

function compareString($text1, $text2)
{
    $res = array();

    $dmp = new DiffMatchPatch();
    $diffs = $dmp->diff_main(mb_convert_encoding($text1, 'UTF-8'), mb_convert_encoding($text2, 'UTF-8'), false);

    $str1 = "";
    $str2 = "";

    foreach ($diffs as $ind => $diff) {
        $len_diff = mb_strlen($diff[1]);
        switch ($diff[0]) {
            case 1: // insert
                $str1 .= str_repeat("~", $len_diff);
                $str2 .= $diff[1];
                break;
            case 0: // equal
                $str1 .= $diff[1];
                $str2 .= $diff[1];
                break;
            case -1: // delete
                $str1 .= $diff[1];
                $str2 .= str_repeat("~", $len_diff);
                break;
        }
    }

    $res['str1'] = $str1;
    $res['str2'] = $str2;

    return $res;
}

// ------------------------------------------------------------------------
// получаем данные о проектах
// ------------------------------------------------------------------------
function getProjects(&$projects, $cb_tables, $sqlConnectId)
{
    $sqlQuery = "
                SELECT
                	CAST(sg.ProjectUID AS NVARCHAR(max)) ProjectUID
                	,sg.ProjectTitle
                	,sg.ProjectCustomer
                    ,sg.ProjectWorkType
                	,COUNT (sg.EstimatesUID) AS CountTotal

                	,COUNT (CASE WHEN ReqStatusID IS NULL THEN sg.EstimatesUID ELSE NULL END) AS ReqCountNull

                    ,COUNT (CASE WHEN ReqStatusID=0 AND ReqTimeChecked<'2016-01-01 00:00:00' THEN sg.EstimatesUID ELSE NULL END) AS ReqCount00BC
                	,COUNT (CASE WHEN ReqStatusID=0 AND ReqTimeChecked>='2016-01-01 00:00:00' THEN sg.EstimatesUID ELSE NULL END) AS ReqCount00AC

                	,COUNT (CASE WHEN ReqStatusID=1 AND ReqTimeChecked<'2016-01-01 00:00:00' THEN sg.EstimatesUID ELSE NULL END) AS ReqCount01BC
                	,COUNT (CASE WHEN ReqStatusID=1 AND ReqTimeChecked>='2016-01-01 00:00:00' THEN sg.EstimatesUID ELSE NULL END) AS ReqCount01AC

                	,COUNT (CASE WHEN ReqStatusID=2 AND ReqTimeChecked<'2016-01-01 00:00:00' THEN sg.EstimatesUID ELSE NULL END) AS ReqCount02BC
                	,COUNT (CASE WHEN ReqStatusID=2 AND ReqTimeChecked>='2016-01-01 00:00:00' THEN sg.EstimatesUID ELSE NULL END) AS ReqCount02AC

                	,COUNT (CASE WHEN ReqStatusID=3 AND ReqTimeChecked<'2016-01-01 00:00:00' THEN sg.EstimatesUID ELSE NULL END) AS ReqCount10BC
                	,COUNT (CASE WHEN ReqStatusID=3 AND ReqTimeChecked>='2016-01-01 00:00:00'
                                AND ReqUserChecked NOT IN ('Чекушкин Евгений','Шнитко Александр') THEN sg.EstimatesUID ELSE NULL END) AS ReqCount10AC
                	,COUNT (CASE WHEN ReqStatusID=3 AND ReqTimeChecked>='2016-01-01 00:00:00'
                                AND ReqUserChecked IN ('Чекушкин Евгений','Шнитко Александр') THEN sg.EstimatesUID ELSE NULL END) AS ReqCount10ACFin

                	,COUNT (CASE WHEN ReqStatusID=4 AND ReqTimeChecked<'2016-01-01 00:00:00' THEN sg.EstimatesUID ELSE NULL END) AS ReqCount99BC
                	,COUNT (CASE WHEN ReqStatusID=4 AND ReqTimeChecked>='2016-01-01 00:00:00' THEN sg.EstimatesUID ELSE NULL END) AS ReqCount99AC

                	,COUNT (CASE WHEN AppStatusID IS NULL THEN sg.EstimatesUID ELSE NULL END) AS AppCountNull

                    ,COUNT (CASE WHEN AppStatusID=0 AND AppTimeChecked<'2016-01-01 00:00:00' THEN sg.EstimatesUID ELSE NULL END) AS AppCount00BC
                	,COUNT (CASE WHEN AppStatusID=0 AND AppTimeChecked>='2016-01-01 00:00:00' THEN sg.EstimatesUID ELSE NULL END) AS AppCount00AC

                	,COUNT (CASE WHEN AppStatusID=1 AND AppTimeChecked<'2016-01-01 00:00:00' THEN sg.EstimatesUID ELSE NULL END) AS AppCount01BC
                	,COUNT (CASE WHEN AppStatusID=1 AND AppTimeChecked>='2016-01-01 00:00:00' THEN sg.EstimatesUID ELSE NULL END) AS AppCount01AC

                	,COUNT (CASE WHEN AppStatusID=2 AND AppTimeChecked<'2016-01-01 00:00:00' THEN sg.EstimatesUID ELSE NULL END) AS AppCount02BC
                	,COUNT (CASE WHEN AppStatusID=2 AND AppTimeChecked>='2016-01-01 00:00:00' THEN sg.EstimatesUID ELSE NULL END) AS AppCount02AC

                	,COUNT (CASE WHEN AppStatusID=3 AND AppTimeChecked<'2016-01-01 00:00:00' THEN sg.EstimatesUID ELSE NULL END) AS AppCount10BC
                	,COUNT (CASE WHEN AppStatusID=3 AND AppTimeChecked>='2016-01-01 00:00:00'
                                AND AppUserChecked NOT IN ('Чекушкин Евгений','Шнитко Александр')  THEN sg.EstimatesUID ELSE NULL END) AS AppCount10AC
                	,COUNT (CASE WHEN AppStatusID=3 AND AppTimeChecked>='2016-01-01 00:00:00'
                                AND AppUserChecked IN ('Чекушкин Евгений','Шнитко Александр') THEN sg.EstimatesUID ELSE NULL END) AS AppCount10ACFin

                	,COUNT (CASE WHEN AppStatusID=4 AND AppTimeChecked<'2016-01-01 00:00:00' THEN sg.EstimatesUID ELSE NULL END) AS AppCount99BC
                	,COUNT (CASE WHEN AppStatusID=4 AND AppTimeChecked>='2016-01-01 00:00:00' THEN sg.EstimatesUID ELSE NULL END) AS AppCount99AC

                FROM
                	statGetActualMaterials AS sg
                WHERE
                	1=1
					-- AND sg.ProjectHidden = 0 
                	-- AND sg.ProjectStatusId IN  (1,2,3,4)
					AND sg.ProjectTitle LIKE '%5104%'
					AND sg.ProjectWorkType = 'Документация'
                	AND sg.ProjectCustomer NOT IN ('НПО Система','')
                GROUP BY
                	sg.ProjectUID, sg.ProjectTitle, sg.ProjectCustomer, sg.ProjectWorkType
                ORDER BY
                	sg.ProjectCustomer, sg.ProjectTitle
            ";
    // TITLE [5167] Республика Крым
    //

    $resultSQLQuery = sqlsrv_query($sqlConnectId, $sqlQuery);
    if ($resultSQLQuery) {
        //$numRows = sqlsrv_num_rows($resultSQLQuery);

        $i = 0;
        while ($rowSQL = sqlsrv_fetch_array($resultSQLQuery)) {
            $project_title = trim($rowSQL['ProjectTitle']);
            $order_num = (int)(substr($project_title, 1, 4));

            if ($order_num > 0) {
                $rowOrd = data_select_array($cb_tables['tableOrders'], "status=0 and f4451=7 and f7071=" . $order_num);
                if ($rowOrd['id'] <> "") {
                    $projects[$i]['order_id'] = (int)($rowOrd['id']);
                    $projects[$i]['order_num'] = (int)($rowOrd['f7071']);
                    $projects[$i]['order_status'] = $rowOrd['f6551'];
                    $projects[$i]['order_status_prod'] = $rowOrd['f10240'];

                    $project_title = substr($project_title, 7);
                }
            }

            $projects[$i]['project_title'] = $project_title;
            $projects[$i]['project_type'] = trim($rowSQL['ProjectWorkType']);
            $projects[$i]['project_uid'] = trim($rowSQL['ProjectUID']);
            $projects[$i]['customer_name'] = trim($rowSQL['ProjectCustomer']);
            $projects[$i]['contractor_name'] = trim($rowSQL['NameContractor']);

            $projects[$i]['num_total'] = (int)($rowSQL['CountTotal']);
            if ($projects[$i]['project_type'] == 'Документация')
                $projects[$i]['percent_total'] = (int)($rowSQL['ReqCount10ACFin']) / (int)($rowSQL['CountTotal']) * 100;
            else
                $projects[$i]['percent_total'] = (int)($rowSQL['AppCount10ACFin']) / (int)($rowSQL['CountTotal']) * 100;


            // ---------------------------------------------------------------
            $projects[$i]['req']['num_null'] = (int)($rowSQL['ReqCountNull']);

            $projects[$i]['req']['num_00_bc'] = (int)($rowSQL['ReqCount00BC']);
            $projects[$i]['req']['num_00_ac'] = (int)($rowSQL['ReqCount00AC']);

            $projects[$i]['req']['num_01_bc'] = (int)($rowSQL['ReqCount01BC']);
            $projects[$i]['req']['num_01_ac'] = (int)($rowSQL['ReqCount01AC']);

            $projects[$i]['req']['num_02_bc'] = (int)($rowSQL['ReqCount02BC']);
            $projects[$i]['req']['num_02_ac'] = (int)($rowSQL['ReqCount02AC']);

            $projects[$i]['req']['num_10_bc'] = (int)($rowSQL['ReqCount10BC']);
            $projects[$i]['req']['num_10_ac'] = (int)($rowSQL['ReqCount10AC']);
            $projects[$i]['req']['num_10_ac_fin'] = (int)($rowSQL['ReqCount10ACFin']);

            $projects[$i]['req']['num_99_bc'] = (int)($rowSQL['ReqCount99BC']);
            $projects[$i]['req']['num_99_ac'] = (int)($rowSQL['ReqCount99AC']);

            // ---------------------------------------------------------------
            $projects[$i]['app']['num_null'] = (int)($rowSQL['AppCountNull']);

            $projects[$i]['app']['num_00_bc'] = (int)($rowSQL['AppCount00BC']);
            $projects[$i]['app']['num_00_ac'] = (int)($rowSQL['AppCount00AC']);

            $projects[$i]['app']['num_01_bc'] = (int)($rowSQL['AppCount01BC']);
            $projects[$i]['app']['num_01_ac'] = (int)($rowSQL['AppCount01AC']);

            $projects[$i]['app']['num_02_bc'] = (int)($rowSQL['AppCount02BC']);
            $projects[$i]['app']['num_02_ac'] = (int)($rowSQL['AppCount02AC']);

            $projects[$i]['app']['num_10_bc'] = (int)($rowSQL['AppCount10BC']);
            $projects[$i]['app']['num_10_ac'] = (int)($rowSQL['AppCount10AC']);
            $projects[$i]['app']['num_10_ac_fin'] = (int)($rowSQL['AppCount10ACFin']);

            $projects[$i]['app']['num_99_bc'] = (int)($rowSQL['AppCount99BC']);
            $projects[$i]['app']['num_99_ac'] = (int)($rowSQL['AppCount99AC']);

            //if ($projects[$i]['project_uid'] == $show_project) $num_project = $i;

            $i++;

        } // end while fetch_array

    } // end if resultSQLQuery

}

// ------------------------------------------------------------------------
// получаем статистику по всем проектам
// ------------------------------------------------------------------------
function getProjectsTotals(&$projects_total, $sqlConnectId)
{
    $sqlQuery = "
                SELECT
                	count (DISTINCT sg.ObjectUID) AS CountTotal
                FROM
                	statGetActualMaterials AS sg
                WHERE
                	sg.ProjectWorkTypeId = 2 AND sg.ProjectHidden = 0 AND sg.ProjectStatusId = 1  AND sg.ProjectCustomer <> 'НПО Система'
            ";

    $resultSQLQuery = sqlsrv_query($sqlConnectId, $sqlQuery);
    if ($resultSQLQuery) {
        $rowSQL = sqlsrv_fetch_array($resultSQLQuery);

        $projects_total['num_total'] = (int)($rowSQL['CountTotal']);

        // ---------------------------------------------------------------
        $projects_total['req']['num_null'] = (int)($rowSQL['ReqCountNull']);

        $projects_total['req']['num_00_bc'] = (int)($rowSQL['ReqCount00BC']);
        $projects_total['req']['num_00_ac'] = (int)($rowSQL['ReqCount00AC']);

        $projects_total['req']['num_01_bc'] = (int)($rowSQL['ReqCount01BC']);
        $projects_total['req']['num_01_ac'] = (int)($rowSQL['ReqCount01AC']);

        $projects_total['req']['num_02_bc'] = (int)($rowSQL['ReqCount02BC']);
        $projects_total['req']['num_02_ac'] = (int)($rowSQL['ReqCount02AC']);

        $projects_total['req']['num_10_bc'] = (int)($rowSQL['ReqCount10BC']);
        $projects_total['req']['num_10_ac'] = (int)($rowSQL['ReqCount10AC']);

        $projects_total['req']['num_99_bc'] = (int)($rowSQL['ReqCount99BC']);
        $projects_total['req']['num_99_ac'] = (int)($rowSQL['ReqCount99AC']);

        // ---------------------------------------------------------------
        $projects_total['app']['num_null'] = (int)($rowSQL['AppCountNull']);

        $projects_total['app']['num_00_bc'] = (int)($rowSQL['AppCount00BC']);
        $projects_total['app']['num_00_ac'] = (int)($rowSQL['AppCount00AC']);

        $projects_total['app']['num_01_bc'] = (int)($rowSQL['AppCount01BC']);
        $projects_total['app']['num_01_ac'] = (int)($rowSQL['AppCount01AC']);

        $projects_total['app']['num_02_bc'] = (int)($rowSQL['AppCount02BC']);
        $projects_total['app']['num_02_ac'] = (int)($rowSQL['AppCount02AC']);

        $projects_total['app']['num_10_bc'] = (int)($rowSQL['AppCount10BC']);
        $projects_total['app']['num_10_ac'] = (int)($rowSQL['AppCount10AC']);

        $projects_total['app']['num_99_bc'] = (int)($rowSQL['AppCount99BC']);
        $projects_total['app']['num_99_ac'] = (int)($rowSQL['AppCount99AC']);
    } // end if resultSQLQuery
}

// ------------------------------------------------------------------------
// получаем данные о материалах из БД Материалов
// ------------------------------------------------------------------------
function getEstimatesByProjects($projects, $req_status, $app_status, $params, $cb_tables, $pspell, $sqlConnectId)
{
    if (is_array($projects)) {
        $res_materials = array();

        // --------------------------------------------
        // готовим фильтр по проектам
        // --------------------------------------------
        $projects_filter = " and [materialdb].[dbo].[statGetActualMaterials].[ProjectUID] in (";
        $num_projects = count($projects);

        $i = 0;
        foreach ($projects as $next_porject) {
            if ($i > 0) $projects_filter .= ",";
            $projects_filter .= "'" . $next_porject . "'";
            $i++;
        }

        $projects_filter .= ")";

        // --------------------------------------------
        // готовим фильтр по статусам
        // --------------------------------------------
        $req_status_filter = " and ReqStatusID in (";
        $j = 0;
        foreach ($req_status as $status_id) {
            if ($status_id <> "") {
                if ($j > 0) $req_status_filter .= ",";
                $req_status_filter .= $status_id;
                $j++;
            }
        }
        if ($j > 0) $req_status_filter .= ")"; else $req_status_filter = "";

        $app_status_filter = " and AppStatusID in (";
        $j = 0;
        foreach ($app_status as $status_id) {
            if ($status_id <> "") {
                if ($j > 0) $app_status_filter .= ",";
                $app_status_filter .= $status_id;
                $j++;
            }
        }
        if ($j > 0) $app_status_filter .= ")"; else $app_status_filter = "";

        // --------------------------------------------
        // готовим сортировки
        // --------------------------------------------
        switch ($params['sorting']) {
            case "№ Смета":
                $sql_sorting = " ORDER BY NumSmeta ASC ";
                break;
            case "№ ТЗ":
                $sql_sorting = " ORDER BY NumRandom ASC ";
                break;
            case "Название смета":
                $sql_sorting = " ORDER BY MaterialName ASC ";
                break;
            case "Название ТЗ":
                $sql_sorting = " ORDER BY ReqObjectTitle ASC ";
                break;
            case "Название заявка":
                $sql_sorting = " ORDER BY AppObjectTitle ASC ";
                break;
            default:
                $sql_sorting = " ORDER BY NumSmeta ASC ";
                break;
        }

        // --------------------------------------------
        // готовим сортировки
        // --------------------------------------------
        $ex_filters = "";
        if ($params['filters']['est_num']) $ex_filters .= " AND NumSmeta=" . $params['filters']['est_num'];
        if ($params['filters']['req_num']) $ex_filters .= " AND NumSmeta=" . $params['filters']['req_num'];
        if ($params['filters']['est_title']) $ex_filters .= " AND MaterialName LIKE '%" . $params['filters']['est_title'] . "%'";
        if ($params['filters']['req_title']) $ex_filters .= " AND ReqObjectTitle LIKE '%" . $params['filters']['req_title'] . "%'";
        if ($params['filters']['app_title']) $ex_filters .= " AND AppObjectTitle LIKE '%" . $params['filters']['app_title'] . "%'";
        if ($params['filters']['req_modifyuser']) $ex_filters .= " AND ReqUserChecked = '" . $params['filters']['req_modifyuser'] . "'";
        if ($params['filters']['app_modifyuser']) $ex_filters .= " AND AppUserChecked = '" . $params['filters']['app_modifyuser'] . "'";

        // ******************************************************
        // получаем список позиций из БД материалов для указанных проектов
        // ******************************************************
        $sqlQuery = "
        SELECT
            CAST([EstimatesUID] as nvarchar(max)) as EstimatesUID
            ,CAST([ProjectUID] as nvarchar(max)) as ProjectUID
            ,[ProjectTitle]
            ,[ProjectHidden]
            ,[ProjectCustomer]
            ,[ProjectContractor]
            ,[ProjectStatusId]
            ,[ProjectStatus]
            ,[ProjectWorkTypeId]
            ,[ProjectWorkType]
            ,[NumSmeta]
            ,[NumRandom]
            ,[MaterialName]
            ,CAST([ObjectUID] as nvarchar(max)) as ObjectUID
            ,CAST([ObjectGroupUID] as nvarchar(max)) as ObjectGroupUID
            ,[ObjectVersion]
            ,[ObjectModiyDate]
            ,[ObjectTitle]
            ,CAST([ReqObjectUID] as nvarchar(max)) as ReqObjectUID
            ,[ReqObjectTitle]
            ,[ReqObjectDescription]
            ,[ReqObjectStandart]
            ,[ReqObjectVersion]
            ,[ReqStatusID]
            ,[ReqStatus]
            ,[ReqTimeChecked]
            ,[ReqUserChecked]
            ,CAST([AppObjectUID] as nvarchar(max)) as AppObjectUID
            ,[AppObjectTitle]
            ,[AppObjectDescription]
            ,[AppObjectStandart]
            ,[AppObjectVendor]
            ,[AppObjectVersion]
            ,[AppStatusID]
            ,[AppStatus]
            ,[AppTimeChecked]
            ,[AppUserChecked]
            ,[Conected]
            ,[SaveUser]
            ,[comment]
            ,[ReqObjectMaxAppRoved]
        FROM
            [materialdb].[dbo].[statGetActualMaterials]
        WHERE
            1=1 
            -- and ProjectHidden = 0 
            -- and ProjectStatusId in (1,2,3,4)
        $projects_filter
        $req_status_filter
        $app_status_filter
        $ex_filters
        $sql_sorting
        ";

        $resultSQLQuery = sqlsrv_query($sqlConnectId, $sqlQuery);
        if ($resultSQLQuery) {
            $numRows = sqlsrv_num_rows($resultSQLQuery);

            $i = 0;
            while ($rowSQL = sqlsrv_fetch_array($resultSQLQuery)) {
                if ($params['show']['list_format'] == 'Список новый' && $params['show']['position_format'] == 'Проверка построчно'
                    && ($i < $params['show']['min_position'] || $i >= $params['show']['max_position'])) {
                    $i++;
                    continue;
                }

                // данные объекта
                $obj_uid = trim($rowSQL['ObjectGroupUID']); // trim($rowSQL['ObjectUID']); более правильно привязываться к групповому объекту
                $obj_uid_element = trim($rowSQL['ObjectUID']);
                $obj_group_uid = trim($rowSQL['ObjectGroupUID']);

                // данные проекта
                $num_smeta = (int)($rowSQL['NumSmeta']);
                $num_req = (int)($rowSQL['NumRandom']) + 1;
                $proj_uid = trim($rowSQL['ProjectUID']);
                $proj_title = trim($rowSQL['ProjectTitle']);
                $proj_cusotmer = trim($rowSQL['ProjectCustomer']);
                $proj_work_type = trim($rowSQL['ProjectWorkType']);
                $est_uid = trim($rowSQL['EstimatesUID']);
                $est_title = trim($rowSQL['MaterialName']);

                // часть для заявки
                $app_obj_uid = trim($rowSQL['AppObjectUID']);
                $app_title = trim($rowSQL['AppObjectTitle']);
                $app_description_raw = trim($rowSQL['AppObjectDescription']);
                $app_description_raw_br = str_replace(chr(10), "<br>", trim($rowSQL['AppObjectDescription']));
                $app_description = trim($rowSQL['AppObjectDescription']);
                $app_standart = trim($rowSQL['AppObjectStandart']);
                $app_vendor = trim($rowSQL['AppObjectVendor']);
                $app_version = trim($rowSQL['AppObjectVersion']);
                $app_date = date("Y-m-d H:i:s", strtotime(trim($rowSQL['AppTimeChecked'])));
                $app_status = trim($rowSQL['AppStatus']);
                $app_status_id = trim($rowSQL['AppStatusID']);
                $app_user = trim($rowSQL['AppUserChecked']);

                // часть для ТЗ
                $req_obj_uid = trim($rowSQL['ReqObjectUID']);
                $req_title = trim($rowSQL['ReqObjectTitle']);
                $req_description_raw = trim($rowSQL['ReqObjectDescription']);
                $req_description_raw_br = str_replace(chr(10), "<br>", trim($rowSQL['ReqObjectDescription']));
                $req_description = trim($rowSQL['ReqObjectDescription']);
                $req_standart = trim($rowSQL['ReqObjectStandart']);
                $req_version = trim($rowSQL['ReqObjectVersion']);
                $req_date = date("Y-m-d H:i:s", strtotime(trim($rowSQL['ReqTimeChecked'])));
                $req_status = trim($rowSQL['ReqStatus']);
                $req_status_id = trim($rowSQL['ReqStatusID']);
                $req_user = trim($rowSQL['ReqUserChecked']);
                $req_date = date("Y-m-d H:i:s", strtotime(trim($rowSQL['ReqTimeChecked'])));


                $status_checks = array(
                    "ТЗ" => array(
                        "object_type" => "req",
                        "specific_uid" => $req_obj_uid,
                        "check_filter" => "req_check_status"
                    ),
                    "Заявка" => array(
                        "object_type" => "app",
                        "specific_uid" => $app_obj_uid,
                        "check_filter" => "app_check_status"
                    )
                );

                // -------------------------------------------------------------------------------
                // название группы из CRM и фильтрация по группам
                // -------------------------------------------------------------------------------
                $rowObjGroup = data_select_array($cb_tables['tableServiceObjGroups'], "status=0 and f16001='$obj_uid' and  f15421='$proj_uid'");
                $groupName = trim($rowObjGroup['f15391']);
                $res_materials['groups'][$groupName] = $groupName;
                $do_continue = false;
                switch ($params['filters']['selected_group']) {
                    case "[ВСЕ ГРУППЫ]":
                        break;
                    case "[БЕЗ ГРУППЫ]":
                        if ($groupName <> "") $do_continue = true;
                        break;
                    default:
                        //echo "tut: $groupName = ".$params['filters']['selected_group']."<br>";
                        if ($groupName <> $params['filters']['selected_group']) $do_continue = true;
                        break;
                }

                if ($do_continue) {
                    unset($res_materials['obj'][$obj_uid]);
                    continue (1);
                }

                if ($groupName <> "") {
                    $res_materials['obj'][$obj_uid]['common']['group_title'] = $groupName;
                    $res_materials['obj'][$obj_uid]['common']['group_id'] = $rowObjGroup['id'];
                }

                // -------------------------------------------------------------------------------
                // проверяем не было ли раньше проверки по этому объекту
                $do_continue = false;

                foreach ($status_checks as $obj_type_check => $obj_check_params) {
                    $rowObjCheck = data_select_array($cb_tables['tableServiceObjChecks'], "status=0 and f15811='" . $obj_type_check . "' and f16021='Позиция в целом' and f15241='" . $obj_check_params['specific_uid'] . "' and f16011='$obj_uid'", "ORDER BY f15301 DESC");
                    $rowObjCheckPrev = data_select_array($cb_tables['tableServiceObjChecks'], "status=0 and f15811='" . $obj_type_check . "' and f16021='Позиция в целом' and f16011='$obj_uid'", "ORDER BY f15301 DESC");

                    switch ($params['check_filters'][$obj_check_params['check_filter']]) {
                        case "Проверено":
                            if ($rowObjCheck['f15341'] <> "Проверено") $do_continue = true;
                            break;
                        case "Принято":
                            if ($rowObjCheck['f15341'] <> "Принято") $do_continue = true;
                            break;
                        case "Исправлено":
                            if ($rowObjCheckPrev['id'] == "" || $rowObjCheck['id'] <> "") $do_continue = true;
                            break;
                        case "Не проверенные":
                            if ($rowObjCheck['id'] <> "" || $rowObjCheckPrev['id'] <> '') $do_continue = true;
                            break;
                        case "Кроме принятых":
                            if ($rowObjCheck['f15341'] == "Принято") $do_continue = true;
                            break;
                    }

                    if ($rowObjCheckPrev['id'] <> "") {
                        // какую последнюю версию проверяли
                        $res_materials['obj'][$obj_uid][$obj_check_params['object_type']]['last_checked_version'] = $rowObjCheckPrev['f15291'];
                        $res_materials['obj'][$obj_uid][$obj_check_params['object_type']]['last_checked_date'] = $rowObjCheckPrev['f15301'];
                        $res_materials['obj'][$obj_uid][$obj_check_params['object_type']]['last_checked_status'] = $rowObjCheckPrev['f15341'];
                    }

                }

                if ($do_continue) {
                    unset($res_materials['obj'][$obj_uid]);
                    continue (1);
                }

                $res_materials['obj'][$obj_uid]['common']['obj_uid'] = $obj_uid_element;
                $res_materials['obj'][$obj_uid]['common']['obj_uid_count'] += 1;

                // --------------------------------------------------------------------------------
                // добавляем объект в список
                if ($req_obj_uid == "") $req_obj_uid = $i;
                $standart = $req_standart;

                if ($params['show']['req']) {
                    $req_decription_array = array();
                    $req_description = exFormatString($req_description, $req_decription_array);
                    $req_description_orpho = highlight_errors($req_description_raw, $pspell);
                    if ($params['show']['position_format'] == "Орфография") $req_title_orpho = highlight_errors($req_title, $pspell);
                }

                if ($params['show']['app']) {
                    $app_decription_array = array();
                    $app_description = exFormatString($app_description, $app_decription_array);
                    $app_description_orpho = highlight_errors($app_description_raw, $pspell);
                    if ($params['show']['position_format'] == "Орфография") $app_title_orpho = highlight_errors($app_title, $pspell);


                    if ($params['show']['req'] && $params['show']['position_format'] == "Сравнение") {
                        $cmp = compareString($req_description_raw, $app_description_raw);
                        $res_materials['obj'][$obj_uid]['common']['compare'] = $cmp;
                    }

                    if ($params['show']['req'] && $params['show']['position_format'] == "Проверка построчно" && $params['show']['show_compare'] == 1) {
                        for ($k = 0; $k < count($req_decription_array); $k++) {
                            if ($req_decription_array[$k]['original'] <> "" && $app_decription_array[$k]['original'] <> "") {
                                $cmp = compareString($req_decription_array[$k]['original'], $app_decription_array[$k]['original']);
                                $app_decription_array[$k]['compare'] = $cmp;
                            }
                        }
                    }
                }

                $res_materials['projects'][$proj_uid]['uid'] = $proj_uid;
                $res_materials['projects'][$proj_uid]['title'] = $proj_title;
                $res_materials['projects'][$proj_uid]['project_type'] = $proj_work_type;
                $res_materials['projects'][$proj_uid]['customer'] = $proj_cusotmer;

                $res_materials['standarts'][$req_standart] = $req_standart;

                $res_materials['obj'][$obj_uid]['projects'][$proj_uid]['uid'] = $proj_uid;
                $res_materials['obj'][$obj_uid]['projects'][$proj_uid]['title'] = $proj_title;
                $res_materials['obj'][$obj_uid]['projects'][$proj_uid]['customer'] = $proj_cusotmer;
                $res_materials['obj'][$obj_uid]['projects'][$proj_uid]['est_uid'] = $est_uid;
                $res_materials['obj'][$obj_uid]['projects'][$proj_uid]['est_title'] = $est_title;
                $res_materials['obj'][$obj_uid]['projects'][$proj_uid]['est_num'] = $num_smeta;
                $res_materials['obj'][$obj_uid]['projects'][$proj_uid]['rnd_num'] = $num_req;

                $res_materials['obj'][$obj_uid]['req']['uid'] = $req_obj_uid;
                $res_materials['obj'][$obj_uid]['req']['title'] = $req_title;
                $res_materials['obj'][$obj_uid]['req']['title_orpho'] = $req_title_orpho;
                $res_materials['obj'][$obj_uid]['req']['description'] = $req_description;
                $res_materials['obj'][$obj_uid]['req']['description_orpho'] = $req_description_orpho;
                $res_materials['obj'][$obj_uid]['req']['description_orig'] = $req_description_raw;
                $res_materials['obj'][$obj_uid]['req']['description_orig_br'] = $req_description_raw_br;
                $res_materials['obj'][$obj_uid]['req']['description_array'] = $req_decription_array;
                $res_materials['obj'][$obj_uid]['req']['standart'] = $req_standart;
                $res_materials['obj'][$obj_uid]['req']['version'] = $req_version;
                $res_materials['obj'][$obj_uid]['req']['user'] = $req_user;
                $res_materials['obj'][$obj_uid]['req']['status'] = $req_status;
                $res_materials['obj'][$obj_uid]['req']['status_id'] = $req_status_id;
                $res_materials['obj'][$obj_uid]['req']['modify_date'] = $req_date;

                $res_materials['obj'][$obj_uid]['app']['uid'] = $app_obj_uid;
                $res_materials['obj'][$obj_uid]['app']['title'] = $app_title;
                $res_materials['obj'][$obj_uid]['app']['title_orpho'] = $app_title_orpho;
                $res_materials['obj'][$obj_uid]['app']['description'] = $app_description;
                $res_materials['obj'][$obj_uid]['app']['description_orpho'] = $app_description_orpho;
                $res_materials['obj'][$obj_uid]['app']['description_orig'] = $app_description_raw;
                $res_materials['obj'][$obj_uid]['app']['description_orig_br'] = $app_description_raw_br;
                $res_materials['obj'][$obj_uid]['app']['description_array'] = $app_decription_array;
                $res_materials['obj'][$obj_uid]['app']['standart'] = $app_standart;
                $res_materials['obj'][$obj_uid]['app']['vendor'] = $app_vendor;
                $res_materials['obj'][$obj_uid]['app']['version'] = $app_version;
                $res_materials['obj'][$obj_uid]['app']['user'] = $app_user;
                $res_materials['obj'][$obj_uid]['app']['status'] = $app_status;
                $res_materials['obj'][$obj_uid]['app']['status_id'] = $app_status_id;
                $res_materials['obj'][$obj_uid]['app']['modify_date'] = $app_date;
                $res_materials['obj'][$obj_uid]['app']['standarts_match'] = ($req_standart == $app_standart);

                $obj_types = array(
                    "req" => array("type" => "ТЗ", "specific_uid" => $req_obj_uid),
                    "app" => array("type" => "Заявка", "specific_uid" => $app_obj_uid)
                );

                foreach ($obj_types as $obj_type => $obj_type_params) {
                    if ($params['show'][$obj_type] == 1) {
                        // -------------------------------------------------------------------------------
                        // получаем статусы проверки
                        // -------------------------------------------------------------------------------

                        if ($rowObjCheck = data_select_array($cb_tables['tableServiceObjChecks'], "status=0 and f16011='" . $obj_uid . "' and f15241='" . $obj_type_params['specific_uid'] . "' and f15811='" . $obj_type_params['type'] . "' and f16021='Позиция в целом'")) {
                            $checks = explode(";", $rowObjCheck['f15821']);
                            foreach ($checks as $check)
                                if ($check <> "") $res_materials['obj'][$obj_uid][$obj_type]['checks']['title'][$check] = 1;

                            if ($rowObjCheck['f15261'] <> "") {
                                $rowPersonal = data_select_array($cb_tables['tablePersonal'], "status=0 and f1400=" . $rowObjCheck['f15261']);
                                $res_materials['obj'][$obj_uid][$obj_type]['check_common_user'] = $rowPersonal['f483'];
                            }

                            $res_materials['obj'][$obj_uid][$obj_type]['check_common_status'] = $rowObjCheck['f15341'];
                        }

                        if ($params['show']['position_format'] == "Проверка построчно") {
                            // чекбоксы по статусам
                            $num_lines = 0;
                            foreach ($res_materials['obj'][$obj_uid][$obj_type]['description_array'] as &$next_line) {
                                $line_hash = $next_line['hash'];

                                // ищем сперва проверки конкретной версии объекта
                                $rowObjCheck = data_select_array($cb_tables['tableServiceObjChecks'],
                                    " status=0 and f16011='$obj_uid' " .
                                    " and f15241='" . $obj_type_params['specific_uid'] . "'" .
                                    " and f16031='$line_hash' " .
                                    " and f15811='" . $obj_type_params['type'] . "'" .
                                    " and f16021='Строка в описании'");

                                // если не нашли, то ищем по группе версий объекта
                                if ($rowObjCheck['id'] == "")
                                    $rowObjCheck = data_select_array($cb_tables['tableServiceObjChecks'],
                                        " status=0 and f16011='$obj_uid' " .
                                        " and f16031='$line_hash' " .
                                        " and f15811='" . $obj_type_params['type'] . "'" .
                                        " and f16021='Строка в описании' ORDER BY f15291 DESC"); // сортировка по версии - вернет одну запись для последней версии

                                if ($rowObjCheck['id'] > 0) {
                                    $check_groups = explode("|", $rowObjCheck['f15821']);

                                    foreach ($check_groups as $check_group) {
                                        if ($check_group <> "") {
                                            $check_subgroup = explode(":", $check_group);
                                            $group_name = $check_subgroup[0];
                                            $checks = explode(";", $check_subgroup[1]);

                                            foreach ($checks as $check) {
                                                if ($check <> "") {
                                                    $next_line['checks'][$group_name][$check] = 1;
                                                }
                                            }
                                        }
                                    }
                                }

                                $num_lines++;
                            } // end foreach чекбоксы по статусам
                        }

                        // -------------------------------------------------------------------------------
                        // сведения об замечаниях по проверкам из CRM
                        // -------------------------------------------------------------------------------
                        $resObjBug = data_select($cb_tables['tableServiceObjBugs'],
                            "status=0 and f15891='" . $obj_uid . "' and f15951='" . $obj_type_params['type'] . "' and f15991='Позиция в целом' and f15921='Новое'");
                        while ($rowObjBug = sql_fetch_assoc($resObjBug)) {
                            $arr_bug = array();
                            $arr_bug['id'] = $rowObjBug['id'];
                            $arr_bug['num'] = $rowObjBug['f15961'];
                            $arr_bug['date'] = $rowObjBug['f15971'];
                            $arr_bug['text'] = $rowObjBug['f15911'];
                            $arr_bug['status_fix'] = $rowObjBug['f16101'];
                            $arr_bug['reporter'] = $rowObjBug['f15931'];
                            $arr_bug['fixer'] = $rowObjBug['f15941'];

                            $res_materials['obj'][$obj_uid][$obj_type]['bugs'][] = $arr_bug;
                        }

                        if ($params['show']['position_format'] == "Проверка построчно") {
                            // ошибки по строкам
                            $resObjBug = data_select($cb_tables['tableServiceObjBugs'], "status=0 and f15891='" . $obj_uid . "' and f15951='" . $obj_type_params['type'] . "' and f15991='Строка в описании' and f15921='Новое'");
                            while ($rowObjBug = sql_fetch_assoc($resObjBug)) {
                                $arr_bug = array();
                                $arr_bug['id'] = $rowObjBug['id'];
                                $arr_bug['num'] = $rowObjBug['f15961'];
                                $arr_bug['date'] = $rowObjBug['f15971'];
                                $arr_bug['text'] = $rowObjBug['f15911'];
                                $arr_bug['status_check'] = $rowObjBug['f15921'];
                                $arr_bug['status_fix'] = $rowObjBug['f16101'];
                                $arr_bug['reporter'] = $rowObjBug['f15931'];
                                $arr_bug['fixer'] = $rowObjBug['f15941'];

                                $line_hash = $rowObjBug['f15981'];

                                $match_found = false;
                                foreach ($res_materials['obj'][$obj_uid][$obj_type]['description_array'] as &$next_line) {
                                    if ($next_line['hash'] == $line_hash) {
                                        $next_line['bugs'][] = $arr_bug;
                                        $match_found = true;
                                        break;
                                    }
                                }
                                if (!$match_found) $res_materials['obj'][$obj_uid][$obj_type]['bugs'][] = $arr_bug;
                            }
                        }
                    }

                } // end foreach obj_types

                // -------------------------------------------------------------------------------
                // ссылки из базы материалов для данных объектов
                // -------------------------------------------------------------------------------
                $sqlQueryLinks = "
                SELECT
                    [Link]
                FROM
                    [materialdb].[dbo].[Links]
                WHERE
                    ObjectUID IN
                        (SELECT UID FROM [materialdb].[dbo].[Objects] WHERE objectUID ='$obj_uid')
                ";
                $resultSQLQueryLinks = sqlsrv_query($sqlConnectId, $sqlQueryLinks);
                while ($rowSQLLink = sqlsrv_fetch_array($resultSQLQueryLinks)) {
                    $link = trim($rowSQLLink['Link']);
                    $hash = hash("md5", $link);
                    $res_materials['obj'][$obj_uid]['links'][$hash] = $link;
                }

                $res_materials['est_uids'][$est_uid] = $est_uid;
                $res_materials['obj_uids'][$obj_uid] = $obj_uid;
                $res_materials['req_uids'][$req_obj_uid] = $req_obj_uid;
                $res_materials['app_uids'][$app_obj_uid] = $app_obj_uid;
                $res_materials['req_users'][$req_user] = $req_user;
                $res_materials['app_users'][$app_user] = $app_user;

                if ($params['show']['list_format'] == "ТЗ" || $params['show']['list_format'] == "ТЗ без ГОСТ") {
                    $req_description_raw = preg_replace('/\r?\n|\r/', ' ', $req_description_raw); // удаляем переводы строк
                    $req_description_raw = preg_replace('/\s\s+/', ' ', $req_description_raw); // очищаем лишние пробелы
                }

                $res_materials['raw'][$i]['req_uid'] = $req_obj_uid;
                $res_materials['raw'][$i]['obj_uid'] = $obj_uid;
                $res_materials['raw'][$i]['est_uid'] = $est_uid;
                $res_materials['raw'][$i]['proj_uid'] = $proj_uid;
                $res_materials['raw'][$i]['proj_title'] = $proj_title;
                $res_materials['raw'][$i]['req_title'] = $req_title;
                $res_materials['raw'][$i]['req_description'] = $req_description_raw;
                $res_materials['raw'][$i]['req_standart'] = $req_standart;
                $res_materials['raw'][$i]['req_version'] = $req_version;
                $res_materials['raw'][$i]['req_date'] = $req_date;
                $res_materials['raw'][$i]['req_status'] = $req_status;
                $res_materials['raw'][$i]['req_user'] = $req_user;
                $res_materials['raw'][$i]['est_num'] = (int)($rowSQL['NumSmeta']);
                $res_materials['raw'][$i]['rnd_num'] = (int)($rowSQL['NumRandom']) + 1;
                $res_materials['raw'][$i]['est_title'] = trim($rowSQL['MaterialName']);
                $res_materials['raw'][$i]['obj_title'] = trim($rowSQL['ObjectTitle']);
                $res_materials['raw'][$i]['app_uid'] = $app_obj_uid;
                $res_materials['raw'][$i]['app_title'] = $app_title;
                $res_materials['raw'][$i]['app_description'] = $app_description_raw;
                $res_materials['raw'][$i]['app_standart'] = $app_standart;
                $res_materials['raw'][$i]['app_vendor'] = $app_vendor;
                $res_materials['raw'][$i]['app_version'] = $app_version;
                $res_materials['raw'][$i]['app_date'] = $app_date;
                $res_materials['raw'][$i]['app_status'] = $app_status;
                $res_materials['raw'][$i]['app_user'] = $app_user;

                $i++;
            } // end while next row

        } // end if $resultSQLQuery

        $res_materials['data']['num_total'] = $i;
        $res_materials['data']['num_selected'] = $numRows;
        $res_materials['data']['num_total_actual'] = count($res_materials['obj']);

        // пересортировываем по номеру тз
        usort($res_materials['raw'], function ($a, $b) {
            return $b['rnd_num'] < $a['rnd_num'];
        });

        usort($res_materials['groups'], function ($a, $b) {
            return $b < $a;
        });

        usort($res_materials['standarts'], function ($a, $b) {
            return $b < $a;
        });

        return $res_materials;
    } // end if is_array

    return false;

} // end function

// ------------------------------------------------------------------------
// форматирует строку
// ------------------------------------------------------------------------
function exFormatString($string, &$arr_res)
{
    $operators_patterns = array(
        "/(<|>|≤|≥)/",
        "/((\s+не )?мен(ее|ьше))\s+/",
        "/((\s+не )?бол(ее|ьше))\s+/",
        "/((\s+не )?(с)?выше)\s+/",
        "/((\s+не )?ниже)\s+/",
        "/(((\s+не )|(Не ))?(Д|д)олж(ен|на|но|ны)( быть)*)/",
        "/(((\s+не )|(Не ))?(М|м)о((жет)|(гут))( быть)*)/",
        "/(\s+или\s+)/",
        "/(\s+и\s+)/",
        "/(\s+(от|до)\s+)/",
    );

    $measure['full']['pref'] = "(экса|пета|тера|гига|мега|кило|гекто|дека|деци|санти|милли|микро|нано|пико|фемто|атто)?";
    $measure['full']['values'] = array(
        "метр",
        "грамм", // вообще в СИ - килограмм
        "секунда",
        "ампер",
        "кельвин",
        "моль",
        "кандела",

        "радиан",
        "стерадиан",
        "градус Цельсия",
        "герц",
        "ньютон",
        "джоуль",
        "ватт",
        "паскаль",
        "люмен",
        "люкс",
        "кулон",
        "вольт",
        "ом",
        "фарад",
        "вебер",
        "тесла",
        "генри",
        "сименс",
        "беккерель",
        "грей",
        "зиверт",
        "катал"
    );

    $measure['short']['pref'] = "(Э|П|Т|Г|М|к|г|да|д|с|м|мк|н|п|ф|а)?";
    $measure['short']['values'] = array(
        "м",
        "г", // вообще в СИ кг
        "с",
        "А",
        "К",
        "моль",
        "кд",

        "рад",
        "ср",
        "°C",
        "Гц",
        "Н",
        "Дж",
        "Вт",
        "Па",
        "лм",
        "лкс",
        "Кл",
        "В",
        "Ом",
        "Ф",
        "Вб",
        "Тл",
        "Гн",
        "См",
        "Бк",
        "Гр",
        "Зв",
        "кат"
    );


    $suf = "(\s|\.|,|:|;|$)+";
    // -----------------------------------------

    $measurments_patterns = array(

        "/(градус(ы|а|ов)?\s+(Ц|ц)ельсия)/",
        "/(°С)/",
        "/(°)/",
        "/(\"(\s|\.|,|:|;|$)+)/",

        "/\s+(час(а|ов)?(\s|\.|>)+)/",

        "/\s+((гига|милли|нано|пика|кило|мега|микро)?литр(а|ов)?)/",

        "/((((М|м)ега)|((К|к)ило))?((П|п)аскал(ь|ей)))/",
        "/(((М|м)|(К|к))(П|п)а)/",

        "/\s+((((квадратны)(й|х))|((кубически)(й|х)))?\s*(гига|милли|нано|пика|кило|мега|санти|микро)?(метр)(е|а|ов|ы)?\s*(((квадратн)(ом|ый|ых))|((кубическ)(им|ий|их)))?)/",

        "/\s+((милли|гига|нано|пика|кило|мега|микро)?(грамм)(ов|а)*)/",

        "/((к|м)*г(с)?\s*\/\s*(д|с|к|м)*м(2|²|3|³))/",    // кгс/см2
        "/\s+((д|с|к|м)*м(2|²|3|³))/",    // см2
        "/((к|м)*г\s*\/\s*л)/",         // кг/л

        "/\s+((милли|гига|нано|пика|кило|мега|микро)?(А|а)мпер(а)?(\s|\.|,|:|;|$)+)/",
        "/\s+((Г|М|К|к|м)?А(\s|\.|,|:|;|$)+)/",

        "/\s+((милли|гига|нано|пика|кило|мега|микро)?(В|в)ольт(\s|\.|,|:|;|$)+)/",
        "/\s+((Г|М|К|к|м)?В(\s|\.|,|:|;|$)+)/",

        "/\s+((милли|гига|нано|пика|кило|мега|микро)?(О|о)м(\s|\.|,|:|;|$)+)/",

        "/\s+((милли|гига|нано|пика|кило|мега|микро)?(В|в)атт(\s|\.|,|:|;|$)+)/",
        "/\s+((Г|М|К|к|м)?(В|в)т(\s|\.|,|:|;|$)+)/",

        "/\s+((милли|гига|нано|пика|кило|мега|микро)?(Г|г)ерц(\s|\.|,|:|;|$)+)/",
        "/\s+((Г|М|К|к|м)?Гц(\s|\.|,|:|;|$)+)/",

        "/\s+((милли|гига|нано|пика|кило|мега|микро)?(П|п)аскаль(\s|\.|,|:|;|$)+)/",
        "/\s+((Г|М|К|к|м)?Па(\s|\.|,|:|;|$)+)/",

        "/\s+((милли|гига|нано|пика|кило|мега|микро)?литр(а|ов)?(\s|\.|,|:|;|$)+)/",
        "/\s+((Г|М|К|к|м)?л(\s|\.|,|:|;|$)+)/",

        "/((Б|б)еккерелей)/",
        "/((Б|б)к\s*(\/)?\s*((к|м)*г)?(\s|\.|,|:|;|$)+)/",

        "/(%\s?(по)?\s?((масс(е|ы))|(объем(а|у)))?)/",


        "/\s+((к|м|c)?(к)?м(2|²|3|³)?(\s|\.|,|:|;|$)+)/", // мм см км

        "/\s+(шт(ук(и)?)?(\s|\.|,|:|;|$)+)/", // шт
        "/\s+(((к|м)?г)(\s|\.|,|:|;|$)+)/", // кг г

        "/\s+((тонн)(ы|а)*)/"
    );

    $res = $string;
    $res = preg_replace('/\s\s+/', ' ', $res); // очищаем лишние пробелы
    $res = preg_replace("/процент(а|ов|ах)/", "%", $res);

    $res_tmp = $res;
    $res_tmp = preg_replace('/((\/|\s)+)(кв)\.((с|м)?м)/', ' $1$2$3[dot]$4', $res_tmp);
    $res_tmp = preg_replace('/\s+(др)\./', ' $1[dot]', $res_tmp);
    $res_tmp = preg_replace('/\s+(1)\.(25D)/', ' $1[dot]$2', $res_tmp);
    $res_tmp = preg_replace('/\s+(0)\.(5\*)/', ' $1[dot]$2', $res_tmp);
    $res_tmp = preg_replace('/\s+\.\s+(Содержание)/', ' [dot]$1', $res_tmp);
    $res_tmp = preg_replace('/\s+(рт)\.\s*(ст)\s*\./', ' $1[dot]$2[dot]', $res_tmp);
    $res_tmp = preg_replace('/([0-9])\.([0-9])/', '$1[dot]$2', $res_tmp);

    $arr_res_tmp = explode(".", $res_tmp);

    $res = preg_replace($operators_patterns, " <div class='div_bold div_inline div_green'>$1</div> ", $res);
    $res = preg_replace($measurments_patterns, " <div class='div_bold div_blue div_inline'>$1</div> ", $res);
    $res = preg_replace("/(?<!ГОСТ)(\s+[0-9,.]+)(-)([0-9,.]+)/", " $1<font style='color:green;font-weight: bold;font-size:120%;'> $2 </font>$3 ", $res);
    $res = preg_replace("/(ГОСТ)\s+([1-9][0-9]+\-[0-9]+)(\.)*/", "<a href='http://docs.cntd.ru/document/gost-$2' target='_blank'>$1 $2</a>$3", $res);
    $res = preg_replace("/(ГОСТ\sР)\s+([1-9][0-9]+\-[0-9]+)(\.)*/", "<a href='http://docs.cntd.ru/document/gost-r-$2' target='_blank'>$1 $2</a>$3", $res);
    $res = preg_replace('/\.\s+/', '.<br><br>', $res);

    foreach ($arr_res_tmp as $arr_item) {
        if (trim($arr_item) <> "") {
            // по запросу отдела производства добавлен парсинг ";"
            $arr_item_sub_tmp = explode(";", $arr_item);
            foreach ($arr_item_sub_tmp as $arr_item_sub) {
                if (trim($arr_item_sub) <> "") {
                    $arr_item_orig = $arr_item_sub;

                    $arr_item_sub = preg_replace($operators_patterns, " <div class='div_bold div_green div_inline'>$1</div> ", $arr_item_sub);
                    $arr_item_sub = preg_replace($measurments_patterns, " <div class='div_bold div_blue div_inline'>$1</div> ", $arr_item_sub);
                    $arr_item_sub = preg_replace("/(\[dot\])/", "<div class='div_bold div_yellow div_inline'>$1</div> ", $arr_item_sub);
                    $arr_item_sub = preg_replace("/(?<!ГОСТ)(\s+[0-9,.]+)(-)([0-9,.]+)/", " $1<font style='color:green;font-weight: bold;font-size:120%;'> $2 </font>$3 ", $arr_item_sub);
                    $arr_item_sub = preg_replace("/(ГОСТ)\s+([1-9][0-9]+\-[0-9]+)(\.)*/", "<a href='http://docs.cntd.ru/document/gost-$2' target='_blank'>$1 $2</a>$3", $arr_item_sub);
                    $arr_item_sub = preg_replace("/(ГОСТ\sР)\s+([1-9][0-9]+\-[0-9]+)(\.)*/", "<a href='http://docs.cntd.ru/document/gost-r-$2' target='_blank'>$1 $2</a>$3", $arr_item_sub);

                    $next_line = array();
                    $next_line['original'] = $arr_item_orig;
                    $next_line['text'] = $arr_item_sub;
                    $next_line['hash'] = md5($arr_item_sub);

                    $arr_res[] = $next_line;
                }
            }
        }
    }
    return $res;
}

// ------------------------------------------------------------------------
// выделяет ошибки в строке
// ------------------------------------------------------------------------
function highlight_errors($str_to_check, &$pspell)
{
    //$css_style = 'color:#cc3300;font-weight:bold;font-size:110%;text-decoration:underline;';
    $preg_mask = array('/([:,;\"\'\.\(\)\s])+/', '/[\s+(0-9)*\s+]/');

    $res = "";

    $str_to_check = preg_replace('/\n/', ' ', $str_to_check); // очищаем переводы строк
    $str_to_check = preg_replace('/\s\s+/', ' ', $str_to_check); // очищаем лишние пробелы
    $words = explode(" ", $str_to_check);

    foreach ($words as $next_word) {
        $next_word_check = trim(preg_replace($preg_mask, " ", trim($next_word)));
        //echo "next_check: $next_word_check<br>";
        if (!pspell_check($pspell, $next_word_check)) $next_word = "<div class='div_bold div_red div_inline div_bigfont'>$next_word</div>";

        $res .= $next_word . " ";
    }

    return trim($res);
}

// ------------------------------------------------------------------------
// возвращает подробные данные о позиции из БД Материалов
// ------------------------------------------------------------------------
function getExtendedPositionData($obj_uid, $proj_uid, $specific_uid, $check_type, $sqlConnectId)
{
    if (!$sqlConnectId) return;

    if ($check_type == "req") $specific_type = "Req";
    else if ($check_type == "app") $specific_type = "App";
    else return;

    $object_filter = " [ObjectGroupUID]='$obj_uid' ";
    $project_filter = " [ProjectUID]='$proj_uid' ";
    $specific_filter = " [" . $specific_type . "ObjectUID] = '$specific_uid' ";

    $sqlQuery = "
    SELECT TOP 1
        CAST([EstimatesUID] as nvarchar(max)) as EstimatesUID
        ,CAST([ProjectUID] as nvarchar(max)) as ProjectUID
        ,[ProjectTitle], [ProjectHidden], [ProjectCustomer], [ProjectContractor], [ProjectStatusId], [ProjectStatus], [ProjectWorkTypeId], [ProjectWorkType]
        ,[NumSmeta]
        ,[NumRandom]
        ,[MaterialName]
        ,CAST([ObjectUID] as nvarchar(max)) as ObjectUID
        ,CAST([ObjectGroupUID] as nvarchar(max)) as ObjectGroupUID
        ,[ObjectVersion]
        ,[ObjectModiyDate]
        ,[ObjectTitle]
        ,CAST([ReqObjectUID] as nvarchar(max)) as ReqObjectUID
        ,[ReqObjectTitle]
        ,[ReqObjectDescription]
        ,[ReqObjectStandart]
        ,[ReqObjectVersion]
        ,[ReqStatusID]
        ,[ReqStatus]
        ,[ReqTimeChecked]
        ,[ReqUserChecked]
        ,CAST([AppObjectUID] as nvarchar(max)) as AppObjectUID
        ,[AppObjectTitle]
        ,[AppObjectDescription]
        ,[AppObjectStandart]
        ,[AppObjectVendor]
        ,[AppObjectVersion]
        ,[AppStatusID]
        ,[AppStatus]
        ,[AppTimeChecked]
        ,[AppUserChecked]
        ,[Conected]
        ,[SaveUser]
        ,[comment]
        ,[ReqObjectMaxAppRoved]
    FROM
        [materialdb].[dbo].[statGetActualMaterials]
    WHERE
        $object_filter
        and $project_filter
        and $specific_filter
    ";

    $resultSQLQuery = sqlsrv_query($sqlConnectId, $sqlQuery);
    $exData = array();
    if ($rowSQL = sqlsrv_fetch_array($resultSQLQuery)) {
        $exData['specific_type'] = $check_type;
        $exData['specific_uid'] = $rowSQL[$specific_type . 'ObjectUID'];
        $exData['title'] = $rowSQL[$specific_type . 'ObjectTitle'];
        $exData['description'] = $rowSQL[$specific_type . 'ObjectDescription'];
        $exData['standart'] = $rowSQL[$specific_type . 'ObjectStandart'];
        $exData['version'] = $rowSQL[$specific_type . 'ObjectVersion'];
        $exData['modify_date'] = date("Y-m-d H:i:s", strtotime(trim($rowSQL[$specific_type . 'TimeChecked'])));
        $exData['user'] = $rowSQL[$specific_type . 'UserChecked'];

        $exData['project_title'] = $rowSQL['ProjectTitle'];
        $exData['project_customer'] = $rowSQL['ProjectCustomer'];

        $exData['est_title'] = $rowSQL['MaterialName'];
        $exData['est_uid'] = $rowSQL['EstimatesUID'];
        $exData['num_smeta'] = $rowSQL['NumSmeta'];
        $exData['num_random'] = $rowSQL['NumRandom'];
    }

    return $exData;
}