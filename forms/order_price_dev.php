<?php

require_once 'add/db.php';
require_once 'add/tables.php';
require_once 'add/functions/service_functions.php';

$order_data = array();
if ($_REQUEST['order_num'])
{
    $order_num = (int)($_REQUEST['order_num']);
    if($row_in_order = data_select_array($cb_tables['tableOrders'] , "status=0 and f7071=$order_num"))
    {
        $row_in_client = data_select_array($cb_tables['tableClients'] , "status=0 and id=".$row_in_order['f4441']);

        $order_data['num'] = $order_num;
        $order_data['date'] = date("Y-m-d", strtotime($row_in_order['f6591']));
        $order_data['sum'] = $row_in_order['f4461'];
        $order_data['service_id'] = $row_in_order['f4451'];
        $order_data['price_id'] = $row_in_order['f13071'];
        $order_data['description'] = $row_in_order['f4431'];
        $order_data['client'] = $row_in_client['f435'];

        if((float)($order_data['sum'])== 0 && (float)($_REQUEST['h_sum_total'])>0.0)
        {
            $new_sum = (float)($_REQUEST['h_sum_total']);
            data_update($cb_tables['tableOrders'],array("f4461"=>$new_sum),"id=".$row_in_order['id']);
        }
    }
}

$typical_apps = array();

$res_types = data_select($cb_tables['tablePrice'],"status=0 and f8500=5");
while($row_type = sql_fetch_assoc($res_types))
{
    $price_id = $row_type['id'];
    $price_name = $row_type['f8510'];

    $typical_apps[$price_id] =
        array(
            "name" => $price_name,
            "id" => transliterate(trim($price_name)),
        );
}

$main_price=[];
$mat_price=[];
$complementary_services=[];
$price_page = 0;
$price_doc_char=0;
$fast_app_price=[];
$appType=[];

// Таблица Прайс-лист / Оценка заявок
$price_evaluation_applications = data_select_array(1161, 'ALL_ROWS','status=0 ORDER BY f19741 ASC, f20531 ASC, id');
foreach($price_evaluation_applications as $row) {
    $price = [];
    $price["Title"] = $row['f19731'];
    $price["paramTitle"] = transliterate($row['f19731']);
    $price["FullPrice"] = $row['f19761'];
    $price["PricePerUnit"] = $row['f19771'];
    $price["PricePercent"] = $row['f19781'];
    $price["NeedCalc"] = $row['f19751'] == "Нет" ? false : true;
    $price["Help"] = $row['f20021'];

    switch ($row['f19741']) {
        case "1.Основные параметры":
            if ($price["Title"] == "Срочная заявка") {
                $fast_app_price = $price;
            } else if ($price["Title"] == "Тип закупки") {
                $appType = $price;
            } else {
                $main_price[] = $price;
            }
            break;
        case "2.Первая часть заявки":
            $mat_price[] = $price;
            break;
        case "3.Вторая часть заявки":
            if ($price["Title"] == "Кол-во документов") {
                $price_doc_char = $price["PricePerUnit"];
            } else if ($price["Title"] == "Кол-во страниц в каждом") {
                $price_page = $price["PricePerUnit"];
            }
            break;
        case "4.Дополнительные услуги":
            $complementary_services[] = $price;
            break;
    }
}


$base_doc_price=[];
$doc_price=[];

// Таблица Прайс-лист / Оценка заявок (Документы)
$have_pages_price = false;
$price_documents = data_select_array(1171, 'ALL_ROWS','status=0 ORDER BY f24041');
foreach($price_documents as $row) {
    $price = [];
    $doc_group = $row['f24051'];
    $price["Title"] = $row['f19831'];
    $price["paramTitle"] = transliterate($row['f19831']);
    $price["FullPrice"] = $row['f19871'];
    $price["PricePerDoc"] = $row['f24061'];
    $price["PricePerPage"] = $row['f24071'];
    $price["DocCount"] = $row['f19851'];
    $price["PageCount"] = $row['f19861'];
    $price["NeedCalc"] = $row['f19841'] == "Да" ? false : true;
    $price["Help"] = $row['f20031'];

    if(!$have_pages_price && $row['f19841'] == "Нет" && (int)$price["PageCount"]>0 && (float)$price["PricePerPage"] > 0.0) {
        $have_pages_price = true;
    }

    // добавляем для каких типов закупок по-умолчанию отмеччать опцию
    if($row['f20011']) {
        $price["AppType"] ="";
        $array = explode(";" ,$row['f20011']);
        foreach ($array as $element){
            $price["AppType"] .= transliterate(trim($element)).' ';
        }
    }

    if($price["NeedCalc"]){
        $doc_price[$doc_group][] = $price;
    }else{
        $base_doc_price[]=$price;
    }
}


$smarty->assign("main_price", $main_price);
$smarty->assign("mat_price", $mat_price);
$smarty->assign("base_doc_price", $base_doc_price);
$smarty->assign("doc_price", $doc_price);
$smarty->assign("price_doc_char", $price_doc_char);
$smarty->assign("price_page", $price_page);
$smarty->assign("have_pages_price", $have_pages_price);
$smarty->assign("fast_app_price", $fast_app_price);
$smarty->assign("appType", $appType);
$smarty->assign("complementary_services", $complementary_services);

function transliterate($st) {
    $charsRus = preg_split('//u', "абвгдежзийклмнопрстуфыэАБВГДЕЖЗИЙКЛМНОПРСТУФЫЭ",
        NULL, PREG_SPLIT_NO_EMPTY);
    $charsEng = preg_split('//u', "abvgdegziyklmnoprstufieabvgdegziyklmnoprstufie",
        NULL, PREG_SPLIT_NO_EMPTY);
    $resultArray=[];
    for($i = 0;$i<count($charsRus);$i++)
    {
        if(count($charsEng)>$i) {
            $resultArray[$charsRus[$i]]=$charsEng[$i];
        }
    }
    $st = strtr($st,
        $resultArray
    );
    $st = strtr($st, array(
        'ё'=>"yo",    'х'=>"h",  'ц'=>"ts",  'ч'=>"ch", 'ш'=>"sh",
        'щ'=>"shch",  'ъ'=>'',   'ь'=>'',    'ю'=>"yu", 'я'=>"ya",
        'Ё'=>"yo",    'Х'=>"h",  'Ц'=>"ts",  'Ч'=>"ch", 'Ш'=>"sh",
        'Щ'=>"shch",  'Ъ'=>'',   'Ь'=>'',    'Ю'=>"yu", 'Я'=>"ya",
        ' '=>"_",     '/'=>"_",  '('=>"",    ')'=>"",   '+'=>"",'.'=>"",
    ));
    return $st;
}

$smarty->assign("doc_type", $doc_type);
$smarty->assign("typical_apps", $typical_apps);
$smarty->assign("order_data", $order_data);

