<?php

// заполнение кодов приглашения для тестировщиков
function FillInviteCodes()
{
    require_once 'add/functions/service_functions.php';
    $filePath = "/var/www/cb/add/tmp/access.txt";

    $handle = fopen($filePath, "r");
    if ($handle) {
        while (($line = fgets($handle)) !== false) {
            $line = trim($line);

            // process the line read.
            if (trim($line) <> "") {
                // проверяем не использовался ли ранее данных код приглашения
                $vacancyId = 29; // тестировщик 2017-01
                $rowCandidateWithInvite = data_select_array(721, "status=0 and f12081=$vacancyId and f17810=$line");
                if ($rowCandidateWithInvite['id'] == "") {
                    $rowCandidate = data_select_array(721, "status=0 and f12081=$vacancyId and f12111 in ('12. Среднее соответствие','11. Интересный') and (f17810='' or f17810 is null)");
                    $candiateId = $rowCandidate['id'];
                    if ($candiateId > 0) {
                        data_update(721, array('f17810' => $line), "id=$candiateId");
                    }
                }
            }
        }

        fclose($handle);
    } else {
        // error opening the file.
    }
}

require_once 'add/functions/common_job_functions.php';
$result = CheckJobMail();

// если в режиме кнопки "Проверки почты", то можно использовать редирект
// header('Location: http://192.168.0.138/cb/fields.php?table=950');

$smarty->assign("new_messages", $result['new_messages']);
$smarty->assign("updated_messages", $result['updated_messages']);
