<?php

// редирект на нужную карточку
$line_id = $_REQUEST['_line_id'];
if ($line_id) {
    switch ($_REQUEST['_type']) {
        case "ИНН":
            header("Location: " . $config["site_root"] . "/report.php?id=191&inn=" . $line_id . "&back_url=" . $base64_current_url);
            break;
        case "заказ":
            header("Location: " . $config["site_root"] . "/view_line2.php?table=271&filter=1891&line=" . $line_id . "&back_url=" . $base64_current_url);
            break;
    }
}

// отчет для просмотра активности того или иного поставщика
require_once 'add/db.php';
require_once 'add/tables.php';
require_once 'add/functions/service_functions.php';

// лимиты для аналитики
$arr_regions_num = array(1, 3, 5, 10);
if ($user['group_id'] == 1) $arr_regions_num = array(999, 1, 3, 5, 10);
$num_regions = $_REQUEST['regions_num'];
if ($num_regions == "") $num_regions = 1;

$arr_competitors_num = array(0, 1, 3);
if ($user['group_id'] == 1) $arr_competitors_num = array(0, 1, 3, 5, 10, 15, 20, 25);
$num_competitors = $_REQUEST['competitors_num'];
if ($num_competitors == "") $num_competitors = 0;

$arr_customers_num = array(0, 1, 3);
if ($user['group_id'] == 1) $arr_customers_num = array(0, 1, 3, 5, 10, 15, 20, 25);
$num_customers = $_REQUEST['customers_num'];
if ($num_customers == "") $num_customers = 0;

$debug = false;

// =============================================================================
// обрабатываем фильтры на входе
if ($_REQUEST['date1']) $date1 = date("d.m.Y", strtotime(form_eng_time($_REQUEST['date1'])));
else $date1 = "01.01.2017";

if ($_REQUEST['date2']) $date2 = date("d.m.Y", strtotime(form_eng_time($_REQUEST['date2'])));
else $date2 = date("d.m.Y");

$date1_fet = form_eng_time($date1 . ' 00:00:00');
$date2_fet = form_eng_time($date2 . ' 23:59:59');

$max_on_page = 3000;
$page = 1;
$num_start = 0;
$num_finish = $max_on_page;
$num_total = 0;
// ----------------------------------------------------------------------
// объявляем переменные
$protocol_types = array(
    "44-ФЗ" =>
        array(
            "Электронный аукцион" =>
                array(
                    "table_name" => "ProtocolsEF",
                    "field_id" => "prot.id",
                    "field_id_win" => "CASE WHEN prot.AppRating=1 and t.Status=3 THEN prot.id ELSE NULL END",
                    "field_total_cost" => "l.Cost",
                    "field_total_win" => "CASE WHEN prot.AppRating=1 and t.Status=3 THEN CASE WHEN prot.LastPrice is null THEN l.Cost ELSE prot.LastPrice END ELSE NULL END",
                    "field_price" => "(CASE WHEN prot.AppRating=1 and prot.LastPrice IS NULL THEN l.cost ELSE prot.LastPrice END)",
                    "field_win_price" => "(CASE WHEN prot_win.AppRating=1 and prot_win.LastPrice IS NULL THEN l.cost ELSE prot_win.LastPrice END)",
                    "field_date" => "prot.LastDate",
                    "field_preadmitted" => "prot.FirstPartAdmitted",
                    "field_admitted" => "prot.SecondPartAdmitted",
                    "field_place" => "prot.AppRating",
                    "field_protocol_inn" => "prot.INN",
                    "field_protocol_kpp" => "prot.KPP",
                    "field_protocol_contactperson" => "prot.ContactPerson",
                    "field_protocol_phone" => "prot.Phone",
                    "field_protocol_email" => "prot.Email",
                    "field_protocol_apprejectreason" => "prot.AppRejectReason",
                    "field_protocol_apprejectexplanation" => "prot.AppRejectExplanation",
                    "field_prot_win_supplier_id" => "prot_win.SuppliersID",
                    "join_lots_and_tenders" => "LEFT JOIN lots as l on prot.LotUID = l.LotUID LEFT JOIN tenders as t on t.UID = l.TenderUID RIGHT JOIN tendersUnique as tu on tu.GovRuID = t.GovRuID ",
                    "where_supplier_id" => "prot.SuppliersID",
                    "prot_win_join_condition" => "prot_win.LotUID = prot.LotUID and prot_win.AppRating = 1"

                ),
            "Открытый конкурс" =>
                array(
                    "table_name" => "ProtocolsOK",
                    "field_id" => "prot.id",
                    "field_id_win" => "CASE WHEN prot.AppRating=1 and t.Status=3 THEN prot.id ELSE NULL END",
                    "field_total_cost" => "l.Cost",
                    "field_total_win" => "CASE WHEN prot.AppRating=1 and t.Status=3 THEN prot.Price ELSE NULL END",
                    "field_price" => "prot.Price",
                    "field_win_price" => "prot_win.Price",
                    "field_date" => "prot.AppDate",
                    "field_preadmitted" => "NULL",
                    "field_admitted" => "prot.Admitted",
                    "field_place" => "prot.AppRating",
                    "field_protocol_inn" => "prot.INN",
                    "field_protocol_kpp" => "prot.KPP",
                    "field_protocol_contactperson" => "prot.ContactPerson",
                    "field_protocol_phone" => "prot.Phone",
                    "field_protocol_email" => "prot.Email",
                    "field_protocol_apprejectreason" => "prot.AppRejectReason",
                    "field_protocol_apprejectexplanation" => "prot.AppRejectExplanation",
                    "field_prot_win_supplier_id" => "prot_win.SuppliersID",
                    "join_lots_and_tenders" => "LEFT JOIN lots as l on prot.LotUID = l.LotUID LEFT JOIN tenders as t on t.UID = l.TenderUID RIGHT JOIN tendersUnique as tu on tu.GovRuID = t.GovRuID",
                    "where_supplier_id" => "prot.SuppliersID",
                    "prot_win_join_condition" => "prot_win.LotUID = prot.LotUID and prot_win.AppRating = 1"

                ),
            "Запрос котировок" =>
                array(
                    "table_name" => "ProtocolsZK",
                    "field_id" => "prot.id",
                    "field_id_win" => "CASE WHEN prot.AppRating=1 and t.Status=3 THEN prot.id ELSE NULL END",
                    "field_total_cost" => "l.Cost",
                    "field_total_win" => "CASE WHEN prot.AppRating=1 and t.Status=3 THEN prot.Cost ELSE NULL END",
                    "field_price" => "prot.Cost",
                    "field_win_price" => "prot_win.Cost",
                    "field_date" => "prot.AppDate",
                    "field_preadmitted" => "NULL",
                    "field_admitted" => "prot.Admitted",
                    "field_place" => "prot.AppRating",
                    "field_protocol_inn" => "prot.INN",
                    "field_protocol_kpp" => "prot.KPP",
                    "field_protocol_contactperson" => "prot.ContactPerson",
                    "field_protocol_phone" => "prot.Phone",
                    "field_protocol_email" => "prot.Email",
                    "field_protocol_apprejectreason" => "prot.AppRejectReason",
                    "field_protocol_apprejectexplanation" => "prot.AppRejectExplanation",
                    "field_prot_win_supplier_id" => "prot_win.SuppliersID",
                    "join_lots_and_tenders" => "LEFT JOIN lots as l on prot.LotUID = l.LotUID LEFT JOIN tenders as t on t.UID = l.TenderUID RIGHT JOIN tendersUnique as tu on tu.GovRuID = t.GovRuID",
                    "where_supplier_id" => "prot.SuppliersID",
                    "prot_win_join_condition" => "prot_win.LotUID = prot.LotUID and prot_win.AppRating = 1"
                ),
        ),
    "223-ФЗ" =>
        array(
            "Прочие" =>
                array(
                    "table_name" => "prot_Results",
                    "field_id" => "prot.ID",
                    "field_id_win" => "CASE WHEN prot.AppPlace=1 and t.Status=3 THEN prot.ID ELSE NULL END",
                    "field_total_cost" => "l.Cost",
                    "field_total_win" => "CASE WHEN prot.AppPlace=1 and t.Status=3 THEN prot.Price ELSE NULL END",
                    "field_price" => "prot.Price",
                    "field_win_price" => "prot_win.Price",
                    "field_date" => "prot.AppDate",
                    "field_preadmitted" => "NULL",
                    "field_admitted" => "(CASE WHEN prot.Admitted Is NULL THEN -1 ELSE prot.Admitted END)",
                    "field_place" => "prot.AppPlace",
                    "field_protocol_inn" => "prot.SupplierINN",
                    "field_protocol_kpp" => "prot.SupplierKPP",
                    "field_protocol_contactperson" => "NULL",
                    "field_protocol_phone" => "NULL",
                    "field_protocol_email" => "NULL",
                    "field_protocol_apprejectreason" => "NULL",
                    "field_protocol_apprejectexplanation" => "NULL",
                    "field_prot_win_supplier_id" => "prot_win.SupplierID",
                    "join_lots_and_tenders" => "LEFT JOIN tenders as t on t.GovRuID = prot.GovRuId RIGHT JOIN tendersUnique as tu on tu.GovRuID = t.GovRuID LEFT JOIN lots as l on l.TenderUID=t.UID and prot.LotNumber = l.LotNumber",
                    "where_supplier_id" => "prot.SupplierID",
                    "prot_win_join_condition" => "prot_win.GovRuId = prot.GovRuId and prot_win.LotNumber = prot.LotNumber and prot_win.AppPlace = 1"
                )
        ),
    "служебное" =>
        array(
            "Поставщики" =>
                array(
                    "table_name" => "Suppliers",
                    "field_protocol_inn" => "prot.INN",
                    "field_protocol_kpp" => "prot.KPP",
                    "field_protocol_contactperson" => "prot.ContactPerson",
                    "field_protocol_phone" => "prot.Phone",
                    "field_protocol_email" => "prot.Email"
                ),
            "Контракты" =>
                array(
                    "table_name" => "Contracts",
                    "field_protocol_inn" => "prot.INN",
                    "field_protocol_kpp" => "prot.KPP",
                    "field_protocol_contactperson" => "prot.ContactPerson",
                    "field_protocol_phone" => "prot.Phone",
                    "field_protocol_email" => "prot.SupplierEmail"
                )
        )
);

$fields_contacts = array("phones" => "field_protocol_phone", "emails" => "field_protocol_email", "contacts" => "field_protocol_contactperson");

// ----------------------------------------------------------------------
// обрабока переменных с формы

$dt_period = ""; // даты участий
if ($_REQUEST['inn']) $inn = trim($_REQUEST['inn']); else $inn = "";//"5405442239"; // фильтр по ИНН
$kpp = ""; // фильтр по КПП
$title = ""; // фильтр по названию
$contactperson = ""; // фильтр по контактному лицу
$email = ""; // фильтр по емайл
$phone = ""; // фильтр по телефону
$address = ""; // фильтр по адресу

// ----------------------------------------------------------------------
// получение и подготовка данных

$arr_result = array();
$arr_result["tenders"] = array();
$arr_result["totals"] = array();

// подключение к SQL
if ($inn <> "") {
    ini_set('max_execution_time', 900);
    $serverName = $databases["MSSQL"]["server"];
    $connectionOptions = array(
        "Database" => $databases["MSSQL"]["db"],
        "Uid" => $databases["MSSQL"]["user"],
        "PWD" => $databases["MSSQL"]["pass"],
        "CharacterSet" => "UTF-8",
        'ReturnDatesAsStrings' => true,
    );

    $sqlConnectId = sqlsrv_connect($serverName, $connectionOptions);

    if ($sqlConnectId) {
        //if($debug) echo "sql connected<br>\r\n";
        // $db = sqlsrv_select_db($databases["MSSQL"]["db"], $sqlConnectId);
        if (true) {

            // получаем контактную информацию из всех источников
            foreach ($fields_contacts as $name_array => $field) {
                $sqlQueryBase = "SELECT @field@ AS @field_output@ FROM @table_name@ WHERE @field_protocol_inn@ LIKE '@inn@%' GROUP BY @field@ HAVING @field@<>''";
                $sqlQuery = "";

                $i = 0;
                foreach ($protocol_types as $tender_law => $protocol) {
                    foreach ($protocol as $tender_type => $protocol_data) {

                        $table_name = $protocol_data["table_name"];
                        $field_name = str_replace("prot.", "", $protocol_data[$field]);
                        $field_protocol_inn = str_replace("prot.", "", $protocol_data["field_protocol_inn"]);

                        if ($field_name <> "NULL") {
                            $nextQuery = str_replace("@inn@", $inn,
                                str_replace("@field@", $field_name,
                                    str_replace("@field_protocol_inn@", $field_protocol_inn,
                                        str_replace("@table_name@", $table_name,
                                            str_replace("@field_output@", $field,
                                                $sqlQueryBase
                                            )
                                        )
                                    )
                                )
                            );
                            //if($debug) echo "!!!!!! next contact query: $nextQuery<br>\r\n";
                            if ($i > 0) $sqlQuery .= " UNION " . $nextQuery; else $sqlQuery .= $nextQuery;
                            $i++;
                        }
                    }
                }

                $resultSQLQuery = sqlsrv_query($sqlConnectId, $sqlQuery);
                //if($debug) echo "next contact query: $sqlQuery<br>\r\n";
                while ($rowSQL = sqlsrv_fetch_array($resultSQLQuery))
                    $arr_result["suppliers"][$inn][$name_array][] = trim($rowSQL[$field]);
                sqlsrv_free_stmt($resultSQLQuery);

            } // end foreach field_contacts

            // вычисляем число участий поставщика по всем протоколам, по всем регионам
            $supplier_brief_data = get_supplier_brief_data($inn, $protocol_types, $sqlConnectId, $date1_fet, $date2_fet, "", $debug);

            $num_total = $supplier_brief_data["num_total"];
            if ($max_on_page > $num_total) $num_finish = $num_total;

            // готовим фильтры по датам
            $where_date_from = "";
            if (trim($date1_fet) <> "") $where_date_from = " and t.PublicDate>='$date1_fet' ";
            $where_date_to = "";
            if (trim($date2_fet) <> "") $where_date_to = " and t.PublicDate<='$date2_fet' ";

            $sqlQuery = "
                SELECT
                	  '@tender_law@' as TenderLaw
                	  ,'@tender_type@' as TenderType
                	  ,t.GovRuID
                	  ,l.LotNumber
                	  ,l.Title
                	  ,l.Cost
                	  ,t.PublicDate
                	  ,t.OpenDate
                	  ,t.ExamDate
                	  ,t.StartDate
                	  ,CAST(t.Link as nvarchar(max)) as Link
                	  ,r.Title as RegionTitle
                	  ,r.ID as RegionId
                	  ,o.Title as OkrugTitle
                	  ,ts.ID as TenderStatusId
                	  ,ts.StatusName as TenderStatus

                	  ,gc.FullName as CustomerTitle
	                  ,gc.INN as CustomerINN
	                  ,gc.KPP as CustomerKPP

	                  ,sup_win.INN as WinnerINN
	                  ,sup_win.KPP as WinnerKPP
	                  ,sup_win.Title as WinnerTitle
	                  ,sup_win.Form as WinnerOrgForm
	                  ,sup_win.ContactPerson as WinnerContactPerson
                      ,@field_win_price@ as WinnerPrice
                      ,(SELECT count(*) FROM @table_name@ as p_sub WHERE p_sub.GovRuId = t.GovRuId) AS NumApps

                	  ,sup.INN as SupplierINN
                	  ,sup.KPP as SupplierKPP
                	  ,sup.Title as SupplierTitle
                	  ,sup.Form as SupplierOrgForm
                	  ,sup.Phone as SupplierPhone
                	  ,sup.Fax as SupplierFax
                	  ,sup.Email as SupplierEmail
                	  ,sup.FactualAddress as SupplierFactAddr
                	  ,sup.PostAddress as SupplierPostAddr
                	  ,sup.ContactPerson as SupplierContactPerson

                      ,@field_protocol_inn@ as ProtocolINN
                      ,@field_protocol_kpp@ as ProtocolKPP
                      ,@field_protocol_contactperson@ as ProtocolContactPerson
                      ,@field_protocol_email@ as ProtocolEmail
                      ,@field_protocol_phone@ as ProtocolPhone

                      ,@field_price@ as Price
                      ,@field_date@ as DateFromProtocol
                      ,@field_preadmitted@ as PreAdmitted
                      ,@field_admitted@ as Admitted

                      ,@field_protocol_apprejectreason@ as AppRejectReason
                      ,@field_protocol_apprejectexplanation@ as AppRejectExplanation
                      ,@field_place@ as AppRating
                      ,prot.Comment
                  FROM
                	@table_name@ as prot
                		LEFT JOIN Suppliers as sup on sup.ID = @where_supplier_id@
                		@join_lots_and_tenders@
                		LEFT JOIN Cities as c ON c.ID = t.City LEFT JOIN Regions as r ON r.ID = c.Region LEFT JOIN Okrugs as o ON o.ID = r.Okrug
                		LEFT JOIN TenderStatuses as ts ON ts.ID = t.Status
                		LEFT JOIN GovCustomers as gc on gc.ID = l.CustomerID
                          LEFT JOIN @table_name@ as prot_win on @prot_win_join_condition@
		                  LEFT JOIN Suppliers as sup_win on sup_win.ID = @field_prot_win_supplier_id@
                WHERE
                	@field_protocol_inn@ LIKE '$inn%'
                    $where_date_from
                    $where_date_to
                ORDER BY
                	t.StartDate DESC
                OFFSET " . $num_start . " ROWS
                FETCH NEXT " . ($num_finish - $num_start) . " ROWS ONLY
                ";

            $sumWins = 0.0;
            $sumApps = 0.0;

            // если есть участия у компнаии, то получаем подробности
            if ($num_total !== 0)
                foreach ($protocol_types as $tender_law => $protocol) {
                    //if($debug) write_log("next law:$tender_law");

                    if ($tender_law <> "служебное") {
                        foreach ($protocol as $tender_type => $protocol_data) {
                            // if($debug) write_log("___next type:$tender_type");

                            $protocol_query = $sqlQuery;

                            $protocol_query = str_replace("@tender_law@", $tender_law, $protocol_query);
                            $protocol_query = str_replace("@tender_type@", $tender_type, $protocol_query);

                            foreach ($protocol_data as $field_name => $field_value) {
                                $protocol_query = str_replace("@" . $field_name . "@", $field_value, $protocol_query);
                            }

                            $resultSQLQuery2 = sqlsrv_query($sqlConnectId, $protocol_query, null, ['Scrollable'=>SQLSRV_CURSOR_STATIC]); // , ['Scrollable'=>SQLSRV_CURSOR_STATIC]

                            if (!$resultSQLQuery2) {
                                die(print_r(sqlsrv_errors(), true));
                            }


                            if ($resultSQLQuery2) {
                                $numRows = sqlsrv_num_rows($resultSQLQuery2);
                                if ($debug) write_log("num_rows:$numRows");

                                while ($rowSQL = sqlsrv_fetch_array($resultSQLQuery2, SQLSRV_FETCH_ASSOC)) {

                                    $govRuId = $rowSQL['GovRuID'];
                                    $tender_status = $rowSQL['TenderStatus'];
                                    $tender_status_id = $rowSQL['TenderStatusId'];
                                    $inn = trim($rowSQL['SupplierINN']);
                                    $kpp = trim($rowSQL['SupplierKPP']);
                                    $phone = trim($rowSQL['SupplierPhone']);
                                    $email = trim($rowSQL['SupplierEmail']);
                                    $contact = trim($rowSQL['SupplierContactPerson']);

                                    $region = trim($rowSQL['RegionTitle']);
                                    $region_id = trim($rowSQL['RegionId']);
                                    $okrug = trim($rowSQL['OkrugTitle']);

                                    $prot_phone = trim($rowSQL['ProtocolPhone']);
                                    $prot_email = trim($rowSQL['ProtocolEmail']);
                                    $prot_contact = trim($rowSQL['ProtocolContactPerson']);

                                    $winner_inn = trim($rowSQL['WinnerINN']);
                                    $winner_kpp = trim($rowSQL['WinnerKPP']);
                                    $winner_title = trim($rowSQL['WinnerTitle']);
                                    $winner_form = trim($rowSQL['WinnerOrgForm']);
                                    $winner_contact = trim($rowSQL['WinnerContactPerson']);
                                    if ($winner_inn <> "" && $winner_title == "" && $winner_contact <> "") $winner_title = $winner_contact;

                                    $customer_inn = trim($rowSQL['CustomerINN']);
                                    $customer_kpp = trim($rowSQL['CustomerKPP']);
                                    $customer_title = trim($rowSQL['CustomerTitle']);

                                    // получаем данные о заказах
                                    $order_app_id = "";
                                    $order_app = "";
                                    $rowOrdApp = data_select_array($cb_tables['tableOrdersApp'], "status=0 and f7441='$govRuId'");
                                    if ($rowOrdApp['id'] <> "") {
                                        $rowOrd = data_select_array($cb_tables['tableOrders'], "status=0 and id=" . $rowOrdApp['f5471']);
                                        $rowClient = data_select_array($cb_tables['tableClients'], "status=0 and id=" . $rowOrd['f4441']);
                                        if (trim($rowClient['f1056']) == $inn) {
                                            $order_app_id = $rowOrd['id'];
                                            $order_app = $rowOrd['f7071'];
                                        }
                                    }

                                    $arr_result["tenders"][$govRuId]["order_app_id"] = (int)$order_app_id;
                                    $arr_result["tenders"][$govRuId]["order_app"] = $order_app;
                                    $arr_result["tenders"][$govRuId]["start_date"] = substr(str_replace('.0000000', '', $rowSQL['StartDate']), 0, 10);
                                    $arr_result["tenders"][$govRuId]["public_date"] = substr(str_replace('.0000000', '', $rowSQL['PublicDate']), 0, 10);
                                    $arr_result["tenders"][$govRuId]["govruid"] = $govRuId;
                                    $arr_result["tenders"][$govRuId]["tender_status"] = $tender_status;
                                    $arr_result["tenders"][$govRuId]["tender_status_id"] = (int)$tender_status_id;
                                    $arr_result["tenders"][$govRuId]["tender_law"] = $rowSQL['TenderLaw'];
                                    $arr_result["tenders"][$govRuId]["tender_type"] = $rowSQL['TenderType'];
                                    $arr_result["tenders"][$govRuId]["title"] = $rowSQL['Title'];
                                    $arr_result["tenders"][$govRuId]["link"] = $rowSQL['Link'];
                                    $arr_result["tenders"][$govRuId]["cost"] = (float)$rowSQL['Cost'];
                                    $arr_result["tenders"][$govRuId]["price"] = (float)$rowSQL['Price'];
                                    $arr_result["tenders"][$govRuId]["discount"] =
                                        (float)$rowSQL['Cost'] >0 ? round(100.0 - (float)$rowSQL['Price']/(float)$rowSQL['Cost']*100.0,2) : '-';

                                    $arr_result["tenders"][$govRuId]["place"] = (int)$rowSQL['AppRating'];
                                    $arr_result["tenders"][$govRuId]["pre_admitted"] = (int)$rowSQL['PreAdmitted'];
                                    $arr_result["tenders"][$govRuId]["admitted"] = (int)$rowSQL['Admitted'];
                                    $arr_result["tenders"][$govRuId]["reject_reason"] = $rowSQL['AppRejectReason'];
                                    $arr_result["tenders"][$govRuId]["reject_explanation"] = $rowSQL['AppRejectExplanation'];
                                    $arr_result["tenders"][$govRuId]["num_apps"] = (int)$rowSQL['NumApps'];

                                    $arr_result["tenders"][$govRuId]["region"] = $region;
                                    $arr_result["tenders"][$govRuId]["okrug"] = $okrug;

                                    // статистика участий по регионам
                                    $arr_result["suppliers"][$inn]["regions"][$region]["title"] = $region;
                                    $arr_result["suppliers"][$inn]["regions"][$region]["id"] = (int)$region_id;
                                    $arr_result["suppliers"][$inn]["regions"][$region]["num_total"] += 1;
                                    $arr_result["suppliers"][$inn]["regions"][$region]["sum_total"] += $rowSQL['Cost'];

                                    // данные о победителе тендера
                                    $arr_result["tenders"][$govRuId]["winner_title"] = $winner_title;
                                    $arr_result["tenders"][$govRuId]["winner_form"] = $winner_form;
                                    $arr_result["tenders"][$govRuId]["winner_inn"] = $winner_inn;
                                    $arr_result["tenders"][$govRuId]["winner_kpp"] = $winner_kpp;
                                    $arr_result["tenders"][$govRuId]["winner_price"] = $rowSQL['WinnerPrice'];
                                    $arr_result["tenders"][$govRuId]["winner_discount"] =
                                        (float)$rowSQL['Cost'] >0 ? round(100.0 - (float)$rowSQL['WinnerPrice']/(float)$rowSQL['Cost']*100.0,2) : '-';

                                    if ($winner_inn <> $inn && $winner_inn <> "") {
                                        // статистика по конкурентам в разрезе по регионам
                                        if ($arr_result["suppliers"][$inn]["regions"][$region]["competitors"][$winner_inn]["inn"] == "") {
                                            $arr_result["suppliers"][$inn]["regions"][$region]["competitors"][$winner_inn]["inn"] = $winner_inn;
                                            $arr_result["suppliers"][$inn]["regions"][$region]["competitors"][$winner_inn]["kpp"] = $winner_kpp;
                                            $arr_result["suppliers"][$inn]["regions"][$region]["competitors"][$winner_inn]["title"] = $winner_title;
                                            $arr_result["suppliers"][$inn]["regions"][$region]["competitors"][$winner_inn]["form"] = winner_form;
                                        }
                                        $arr_result["suppliers"][$inn]["regions"][$region]["competitors"][$winner_inn]["count"] += 1;

                                        // статистика по конкурентам в целом
                                        if ($arr_result["suppliers"][$inn]["competitors"][$winner_inn]["inn"] == "") {
                                            $arr_result["suppliers"][$inn]["competitors"][$winner_inn]["inn"] = $winner_inn;
                                            $arr_result["suppliers"][$inn]["competitors"][$winner_inn]["kpp"] = $winner_kpp;
                                            $arr_result["suppliers"][$inn]["competitors"][$winner_inn]["title"] = $winner_title;
                                            $arr_result["suppliers"][$inn]["competitors"][$winner_inn]["form"] = winner_form;
                                        }
                                        $arr_result["suppliers"][$inn]["competitors"][$winner_inn]["count"] += 1;
                                    }

                                    $arr_result["tenders"][$govRuId]["supplier_title"] = trim($rowSQL['SupplierTitle']);
                                    $arr_result["tenders"][$govRuId]["supplier_form"] = trim($rowSQL['SupplierOrgForm']);
                                    $arr_result["tenders"][$govRuId]["supplier_inn"] = trim($rowSQL['ProtocolINN']);
                                    $arr_result["tenders"][$govRuId]["supplier_kpp"] = trim($rowSQL['ProtocolKPP']);

                                    $arr_result["tenders"][$govRuId]["supplier_contact"] = trim($rowSQL['ProtocolContactPerson']);
                                    $arr_result["tenders"][$govRuId]["supplier_phone"] = trim($rowSQL['ProtocolPhone']);
                                    $arr_result["tenders"][$govRuId]["supplier_email"] = trim($rowSQL['ProtocolEmail']);

                                    // данные по заказчикам
                                    $arr_result["tenders"][$govRuId]["customer_title"] = $customer_title;
                                    $arr_result["tenders"][$govRuId]["customer_inn"] = $customer_inn;
                                    $arr_result["tenders"][$govRuId]["customer_kpp"] = $customer_title;

                                    if ($arr_result["suppliers"][$inn]["regions"][$region]["customers"][$winner_inn]["inn"] == "") {
                                        $arr_result["suppliers"][$inn]["regions"][$region]["customers"][$customer_inn]["inn"] = $customer_inn;
                                        $arr_result["suppliers"][$inn]["regions"][$region]["customers"][$customer_inn]["kpp"] = $customer_title;
                                        $arr_result["suppliers"][$inn]["regions"][$region]["customers"][$customer_inn]["title"] = $customer_title;
                                    }

                                    $arr_result["suppliers"][$inn]["regions"][$region]["customers"][$customer_inn]["num_total"] += 1;
                                    $arr_result["suppliers"][$inn]["regions"][$region]["customers"][$customer_inn]["sum_total"] += $rowSQL['Cost'];


                                    if ($arr_result["suppliers"][$inn]["title"] == "") $arr_result["suppliers"][$inn]["title"] = trim($rowSQL['SupplierTitle']);
                                    if ($arr_result["suppliers"][$inn]["form"] == "") $arr_result["suppliers"][$inn]["form"] = trim($rowSQL['SupplierOrgForm']);
                                    if ($arr_result["suppliers"][$inn]["inn"] == "") $arr_result["suppliers"][$inn]["inn"] = $inn;
                                    if ($arr_result["suppliers"][$inn]["kpp"] == "") $arr_result["suppliers"][$inn]["kpp"] = $kpp;
                                    if ($arr_result["suppliers"][$inn]["orgform"] == "") $arr_result["suppliers"][$inn]["orgform"] = trim($rowSQL['SupplierOrgForm']);

                                    if ($phone <> "" && !in_array($phone, $arr_result["suppliers"][$inn]["phones"])) $arr_result["suppliers"][$inn]["phones"][] = $phone;
                                    if ($prot_phone <> "" && !in_array($prot_phone, $arr_result["suppliers"][$inn]["phones"])) $arr_result["suppliers"][$inn]["phones"][] = $prot_phone;

                                    if ($email <> "" && !in_array($email, $arr_result["suppliers"][$inn]["emails"])) $arr_result["suppliers"][$inn]["emails"][] = $email;
                                    if ($prot_email <> "" && !in_array($prot_email, $arr_result["suppliers"][$inn]["emails"])) $arr_result["suppliers"][$inn]["emails"][] = $prot_email;

                                    if ($contact <> "" && !in_array($contact, $arr_result["suppliers"][$inn]["contacts"])) $arr_result["suppliers"][$inn]["contacts"][] = $contact;
                                    if ($prot_contact <> "" && !in_array($prot_contact, $arr_result["suppliers"][$inn]["contacts"])) $arr_result["suppliers"][$inn]["contacts"][] = $prot_contact;

                                    $arr_result["suppliers"][$inn]["sum_apps"] += $rowSQL['Cost'];
                                    $arr_result["suppliers"][$inn]["num_apps"] += 1;


                                    if ($rowSQL['Admitted'] == 1 && $rowSQL['AppRating'] == 1 && $tender_status_id == 3) {
                                        $arr_result["suppliers"][$inn]["sum_wins"] += $rowSQL['Price'];
                                        $arr_result["suppliers"][$inn]["num_wins"] += 1;

                                        $arr_result["suppliers"][$inn]["regions"][$region]["num_win"] += 1;
                                        $arr_result["suppliers"][$inn]["regions"][$region]["sum_win"] += $rowSQL['Price'];

                                        $arr_result["suppliers"][$inn]["regions"][$region]["customers"][$customer_inn]["sum_win"] += $rowSQL['Price'];
                                        $arr_result["suppliers"][$inn]["regions"][$region]["customers"][$customer_inn]["num_win"] += 1;
                                    }

                                    if ($rowSQL['Admitted'] == 0) {
                                        $arr_result["suppliers"][$inn]["sum_rejected"] += $rowSQL['Cost'];
                                        $arr_result["suppliers"][$inn]["num_rejected"] += 1;
                                    }

                                } // end while query
                            } // end if result sqlQuery
                        } // end foreach tender_type
                    }
                } // end foreach tender_law

            usort($arr_result["tenders"], function ($a, $b) {
                return $b['public_date'] >= $a['public_date'];
            });

            usort($arr_result["suppliers"][$inn]["regions"], function ($a, $b) {
                return $b['num_total'] >= $a['num_total'];
            });

            $fact_num_regions = count($arr_result["suppliers"][$inn]["regions"]);
            if ($fact_num_regions < $num_regions) {
                $num_regions = $fact_num_regions;
            }

            // загружаем данные об участиях конкурентов группами по регионам
            for ($r = 0; $r < $num_regions; $r++) {
                $region = $arr_result["suppliers"][$inn]["regions"][$r]['id'];
                $arr_result["suppliers"][$inn]["regions"][$r]["market_results"]["num_win_total"] = 0;
                $arr_result["suppliers"][$inn]["regions"][$r]["market_results"]["sum_win_total"] = 0;

                // пересортировываем заказчиков по количеству участий
                usort($arr_result["suppliers"][$inn]["regions"][$r]["customers"], function ($a, $b) {
                    return $b['sum_win'] >= $a['sum_win'];
                });

                // пересортировываем конкурентов по количеству пересечений в тендерах
                usort($arr_result["suppliers"][$inn]["regions"][$r]["competitors"], function ($a, $b) {
                    return $b['count'] >= $a['count'];
                });

                 for ($i = 0; $i < $num_competitors; $i++) {
                    if ($arr_result["suppliers"][$inn]["regions"][$r]["competitors"][$i]["inn"] == "") break;
                    $winner_inn = $arr_result["suppliers"][$inn]["regions"][$r]["competitors"][$i]["inn"];

                    $comptetitor_data = get_supplier_brief_data($winner_inn, $protocol_types, $sqlConnectId, $date1_fet, $date2_fet, $region, false);
                    $arr_result["suppliers"][$inn]["regions"][$r]["competitors"][$i]["stat"]["num_total"] = $comptetitor_data["num_total"];
                    $arr_result["suppliers"][$inn]["regions"][$r]["competitors"][$i]["stat"]["num_win"] = $comptetitor_data["num_win"];
                    $arr_result["suppliers"][$inn]["regions"][$r]["competitors"][$i]["stat"]["sum_total"] = $comptetitor_data["sum_total"];
                    $arr_result["suppliers"][$inn]["regions"][$r]["competitors"][$i]["stat"]["sum_win"] = $comptetitor_data["sum_win"];

                    $arr_result["suppliers"][$inn]["regions"][$r]["market_results"]["num_win_total"] += $comptetitor_data["num_win"];
                    $arr_result["suppliers"][$inn]["regions"][$r]["market_results"]["sum_win_total"] += $comptetitor_data["sum_win"];
                }

                // добавляем данные в список и текущего участника
                $arr_competitor = array();
                $arr_competitor["inn"] = $inn;
                $arr_competitor["kpp"] = $kpp;
                $arr_competitor["title"] = $arr_result["suppliers"][$inn]["title"];
                $arr_competitor["form"] = $arr_result["suppliers"][$inn]["form"];

                $supplier_brief_data = get_supplier_brief_data($inn, $protocol_types, $sqlConnectId, $date1_fet, $date2_fet, $region, false);
                $arr_competitor["stat"]["num_total"] = $supplier_brief_data["num_total"];
                $arr_competitor["stat"]["num_win"] = $supplier_brief_data["num_win"];
                $arr_competitor["stat"]["sum_total"] = $supplier_brief_data["sum_total"];
                $arr_competitor["stat"]["sum_win"] = $supplier_brief_data["sum_win"];

                $arr_result["suppliers"][$inn]["regions"][$r]["market_results"]["num_win_total"] += $supplier_brief_data["num_win"];
                $arr_result["suppliers"][$inn]["regions"][$r]["market_results"]["sum_win_total"] += $supplier_brief_data["sum_win"];

                array_unshift($arr_result["suppliers"][$inn]["regions"][$r]["competitors"], $arr_competitor);
                // вычислем доли рынка
                for ($i = 0; $i < $num_competitors + 1; $i++) {
                    $arr_result["suppliers"][$inn]["regions"][$r]["competitors"][$i]["stat"]["market_share"] =
                        $arr_result["suppliers"][$inn]["regions"][$r]["competitors"][$i]["stat"]["sum_win"] / $arr_result["suppliers"][$inn]["regions"][$r]["market_results"]["sum_win_total"] * 100;
                }
            } // end for regions
        } // end if db selected
        sqlsrv_close($sqlConnectId);
    } // end if sqlConnectId
}

// ----------------------------------------------------------------------
// передача информации в форму
$smarty->assign("arr_result", $arr_result);
$smarty->assign("dt_period", $dt_period);

$smarty->assign("inn", $inn);
$smarty->assign("kpp", $kpp);
$smarty->assign("title", $kpp);
$smarty->assign("contactperson", $contactperson);
$smarty->assign("email", $email);
$smarty->assign("phone", $phone);
$smarty->assign("address", $address);

$smarty->assign("date1", $date1);
$smarty->assign("date2", $date2);

$smarty->assign("page", $page);

$smarty->assign("regions_nums", $arr_regions_num);
$smarty->assign("num_regions", $num_regions);

$smarty->assign("competitiors_nums", $arr_competitors_num);
$smarty->assign("num_competitors", $num_competitors);

$smarty->assign("customers_nums", $arr_customers_num);
$smarty->assign("num_customers", $num_customers);

$smarty->assign("num_regions", $num_regions);

$smarty->assign("num_start", $num_start);
$smarty->assign("num_finish", $num_finish);
$smarty->assign("num_total", $num_total);

// =========================================================
function array_sort_by_column(&$arr, $col, $dir = SORT_ASC)
{
    $sort_col = array();
    foreach ($arr as $key => $row) {
        $sort_col[$key] = $row[$col];
    }

    array_multisort($sort_col, $dir, $arr);
}

// =========================================================
function resort_array($array_sort)
{
    $arr_len = count($array_sort);
    foreach ($array_sort as $govruid => $arr_data) {
        $next_date = $arr_data["start_date"];
    }
}

// =========================================================
// получаем основные данные по участию поставщика
// =========================================================
function get_supplier_brief_data($inn, $protocol_types, $sqlConnectId, $date_from = "", $date_to = "", $region = "", $debug = false)
{
    if (trim($inn) == "") return;

    $res_data = array(
        "num_total" => 0,
        "num_win" => 0,
        "sum_total" => 0,
        "sum_win" => 0
    );

    $where_date_from = "";
    if (trim($date_from) <> "") $where_date_from = " and t.PublicDate>='$date_from' ";

    $where_date_to = "";
    if (trim($date_from) <> "") $where_date_to = " and t.PublicDate<='$date_to' ";

    // вычисляем по всем видам законов
    foreach ($protocol_types as $tender_law => $protocol) {
        if ($tender_law <> "служебное") {
            // вычислчем по всем типам тендеров в данном законе
            foreach ($protocol as $tender_type => $protocol_data) {
                // get total number of records
                $protocol_query = "
                        SELECT
                            count(@field_id@) as CountTotal
                            ,count(@field_id_win@) as CountWin
                            ,sum(@field_total_cost@) as SumTotal
                            ,sum(@field_total_win@) as SumWin
                    FROM
                            " . $protocol_data['table_name'] . " as prot
                		      @join_lots_and_tenders@
                		      LEFT JOIN Cities as c ON c.ID = t.City LEFT JOIN Regions as r ON r.ID = c.Region LEFT JOIN Okrugs as o ON o.ID = r.Okrug
                        WHERE
                            @field_protocol_inn@ LIKE '$inn%'
                    " . $where_date_from . " " . $where_date_to;

                if ($region <> "") $protocol_query .= " and r.ID=$region";

                foreach ($protocol_data as $field_name => $field_value)
                    $protocol_query = str_replace("@" . $field_name . "@", $field_value, $protocol_query);

                //if($debug) write_log("+++:$protocol_query");

                $resultSQLQuery = sqlsrv_query($sqlConnectId, $protocol_query);
                $rowSQL = sqlsrv_fetch_array($resultSQLQuery);

                $res_data["num_total"] += $rowSQL['CountTotal'];
                $res_data["num_win"] += $rowSQL['CountWin'];
                $res_data["sum_total"] += $rowSQL['SumTotal'];
                $res_data["sum_win"] += $rowSQL['SumWin'];

                sqlsrv_free_stmt($resultSQLQuery);

            }
        }
    }

    return $res_data;
}

?>