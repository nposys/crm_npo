<?php
// -------------------------------------------------------------
// Здесь подготовливаем данные для вывода в отчете

// обрабатываем фильтры на входе
if ($_REQUEST['date1']) $date1 = date("d.m.Y",strtotime(form_eng_time($_REQUEST['date1'])));
else $date1 = date("d.m.Y", strtotime("first day of this month"));

if ($_REQUEST['date2']) $date2 = date("d.m.Y",strtotime(form_eng_time($_REQUEST['date2'])));
else $date2 = date("d.m.Y");

$date1_fet = form_eng_time($date1.' 00:00:00');
$date2_fet = form_eng_time($date2.' 23:59:59');

if ($_REQUEST['_type'] || $_REQUEST['_manager'] || $_REQUEST['_dates'])
{
    reset_filters(62);
    if ($_REQUEST['_dates']) set_filter(724, "period", $date1." 00:00:00", $date2." 23:59:59");
    if ($_REQUEST['_manager']) set_filter(727, "=", intval($_REQUEST['_manager']));
    if ($_REQUEST['_type']) set_filter(773, "=", form_input($_REQUEST['_type']));
    set_filter(1053, "=", "Да");
    header("Location: ".$config["site_root"]."/fields.php?table=62");
}

if ($_REQUEST['manager']) $manager = intval($_REQUEST['manager']); elseif ($user['group_id']!=1) $manager = $user['id'];

if ($_REQUEST['stext']) $stext = trim($_REQUEST['stext']);

// -------------------------------------------------------------
// инициализируем переменные
$tablePersonal = 46;
$tableEvents = 62;

$i = 0;
$j = 0;
$strSeriesName1 = "";
$strSeriesName2 = "";


if($stext<>"")
{
    // connect to ms sql
    $serverName = '192.168.0.10';
    $dbName = 'tenderdb';
    $aTenders = array();
    $numRows = 0;

    ini_set('mssql.charset','UTF8');
    $sqlLink = mssql_connect ($serverName , 'tenders', 'gdlNtylthc 98');

    if ($sqlLink)
    {
        $db = mssql_select_db("tenderdb", $sqlLink);
        if ($db)
        {
            // выбираем тендеры в статусе на подготовке заявки
            $query = "select top 20 t.* from tenders as t where t.PublicDate>='".$date1_fet."' and t.PublicDate<='".$date2_fet."' and FREETEXT(t.title, '".$stext."') ";

            //execute the SQL query and return records
            $resultQuery = mssql_query($query);

            if($resultQuery)
            {
                $numRows = mssql_num_rows($resultQuery);
                // перебираем заказы из подготовки
                while($row = mssql_fetch_array($resultQuery, MSSQL_BOTH))
                {
                    $aTenders[$i]["GovRuID"] = $row['GovRuID'];
                    $aTenders[$i]["Title"] = $row['Title'];
                    $aTenders[$i]["Customer"] = $row['Customer'];
                    $aTenders[$i]["Cost"] = number_format($row['Cost'], 2, ',', ' ');
                    $aTenders[$i]["Link"] = $row['Link'];

                    $i++;
                }

            } // end if result query

        } // end if select db
        //close the connection
        mssql_close($sqlLink);
    } // end if ms sql link

}

if($manager) $userCond = " and pers.f1400=".$manager;

// формируем селектбокс по выбору менеджеров
if ($user['group_id']==1)
    $sel_manager = "<option value=''>Все</option>\r\n";

$result = sql_query("SELECT DISTINCT `user`.`id`, `user`.`fio` FROM `".USERS_TABLE."` AS `user`, `".GROUPS_TABLE."` AS `group` WHERE `user`.`arc`=0 AND `user`.`group_id`!='777'");
while ($row = sql_fetch_assoc($result))
    $sel_manager.= "<option value='".$row['id']."'".(($row['id']==$manager)?" selected":"").">".$row['fio']."</option>\r\n";

// Переносим переменные в отображение
$smarty->assign("stext", $stext);
$smarty->assign("date1", $date1);
$smarty->assign("date2", $date2);
$smarty->assign("data", $data_m);
$smarty->assign("manager", $manager);
$smarty->assign("sel_manager", $sel_manager);
$smarty->assign("user_group", $user['group_id']);
$smarty->assign("aTenders", $aTenders);
$smarty->assign("numTenders", $numRows );
$smarty->assign("sql", $query);