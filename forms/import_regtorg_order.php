<?php
/**
 * Created by PhpStorm.
 * User: miheev
 * Date: 18.01.2019
 * Time: 15:30
 */

require_once 'add/db.php';
require_once 'add/tables.php';
require_once 'add/functions/service_functions.php';
require_once 'add/functions/common_order_functions.php';


$arrManagers = ['341']; // 341 - Круппа; 421 - Рожкова; 630 - Лаврентьева 741 - Головченко 821 - Шмигирилова

$LotUID = $_POST["LotUID"];
$CreateOrder = (int)$_POST["CreateOrder"];
$IsGov = (int)$_POST["IsGov"];
$Name = $_POST["Name"];
$Folder = $_POST["Folder"];
$Manager = $_POST["Manager"];
$OrderDate = $_POST["OrderDate"];
if($LotUID) {
    if($CreateOrder===1)
    {
        $rowOrderApp = data_select_array($tableOrders, "status=0 and f4441=3813 and f20761 = '".trim($LotUID)."'");
        if ($rowOrderApp['id'] == "") {

            if ((int)$IsGov == 1) {
                $orderSum = 600; // 44-ФЗ по 400 рублей (с 01.05.2018 по 600)
                $pricePosId = 51; // "Заявка [РТ] обычная"
            } else {
                $orderSum = 3000; // 223-ФЗ и прочие по 2000 рублей (с 01.05.2018 по 3000)
                $pricePosId = 57; // "Заявка [РТ] коммерческая"
            }

            // 2.1. Если нет такого заказа, то создаем его в КБ
            $arrInsert = array(
                'status' => '0',
                'f4451' => 5,                     // тип заказа = "Заявка"
                'f13071' => $pricePosId,                     // позиция прайса = "Заявка [РТ]"
                'f6551' => '10. Исполнение',      // статус заказа
                'f4471' => 'Да',                  // согласовано с клиентом
                'f4431' => $Name,         // название тендера
                'f6621' => $Folder,
                'f7301' => $Manager,   // примечание
                'f6591' => $OrderDate,     // дата заказа
                //'f6601'=>$row['PlanDate'],
                'f9010' => "-" . $arrManagers[$indManager] . "-",
                'f20761' => trim($LotUID),  //
                'f4461' => $orderSum,            // стоимость подготовки заявки регионторг
                'f4441' => 3813                   // клиент = регионторг
            );

            $orderId = data_insert($tableOrders, EVENTS_ENABLE, $arrInsert);
            //$indManager = !$indManager; // меняем менеджера производство на следующего

            // вставляем дополнительную информацию
            $arrInsert = array(
                "status" => 0,
                "f10620" => $orderId,
                "f10670" => 1,
                "f10720" => $row['ID'],// MS SQL ID
                "f10730" => $row['PlanDate']
            );
            data_insert(640, $arrInsert);

            $arrOrders[$i]['orderID'] = $orderId;
            $arrOrders[$i]['GovRuID'] = trim($row['GovRuID']);
            $arrOrders[$i]['LotNumber'] = trim($row['LotNumber']);

            // создаем задачу на менеджера
            $dateStart = date("Y-m-d H:i:s");
            $dateFinish = date("Y-m-d H:i:s", strtotime($dateStart . " + 10 min"));
            $arrJob = array(
                "status" => 0,
                "f12601" => $orderId, // заказ
                "f12621" => 3813,                   // клиент = регионторг
                "f12611" => 5,                     // тип заказа = "Заявка"
                "f12951" => $row['OrderTitle'],         // название тендера
                "f12661" => $arrManagers[$indManager],    // менеджер
                "f13591" => 10,                 // трудоемкость
                "f12981" => $dateStart,  // дата начала план
                "f12991" => $dateFinish,  // дата начала план
                "f14741" => "Обычная",    // важность
                "f12861" => 4, // тип работы - оформление заявки
                "f12671" => "Начать работу по заказу", // описание
                "f12631" => "10. Запланировано" //статус
            );
            $jobId = data_insert($tableOrderJobs, EVENTS_ENABLE, $arrJob);

            $i++;

        } else {
            // если заказ уже есть - обновляем данные
            write_log('already have order ' . $rowOrderApp['id']);
            $rowOrderAppProd = data_select_array($tableOrdersApp, "status=0 and f5471=" . $rowOrderApp['id']);
            if ($rowOrderAppProd['id'] == "") {
                $orderAppId = data_insert($tableOrdersApp,
                    EVENTS_ENABLE,
                    array(
                        'status' => '0',
                        'f7441' => trim($row['GovRuID']),
                        'f8680' => trim($row['LotNumber']),
                        'f5491' => 'Нет', // срочная
                        'f8110' => 'Нет', // подбор материалов
                        'f8120' => 'Да',  // подача
                        'f8130' => 'Да',  // торги
                        'f6561' => '20. Заполнение',   // статус производства
                        'f5471' => $rowOrderApp['id']      // связь с "Заказы"
                    )
                );
                $appProdAdd++;
            }

            $rowOrderAppJob = data_select_array($tableOrderJobs, "status=0 and f12601=" . $rowOrderApp['id']);
            if ($rowOrderAppJob['id'] == "") {
                // создаем задачу на менеджера
                $dateStart = date("Y-m-d H:i:s");
                $dateFinish = date("Y-m-d H:i:s", strtotime($dateStart . " + 10 min"));
                $arrJob = array(
                    "status" => 0,
                    "f12601" => $rowOrderApp['id'], // заказ
                    "f12621" => 3813,                   // клиент = регионторг
                    "f12611" => 5,                     // тип заказа = "Заявка"
                    "f12951" => $row['OrderTitle'],         // название тендера
                    "f12661" => $arrManagers[$indManager],    // менеджер
                    "f13591" => 10,                 // трудоемкость
                    "f12981" => $dateStart,  // дата начала план
                    "f12991" => $dateFinish,  // дата начала план
                    "f14741" => "Обычная",    // важность
                    "f12861" => 4, // тип работы - оформление заявки
                    "f12671" => "Начать работу по заказу", // описание
                    "f12631" => "10. Запланировано" //статус
                );
                $jobId = data_insert($tableOrderJobs, $arrJob);
                $appJobAdd++;
            }

        }
    }

}