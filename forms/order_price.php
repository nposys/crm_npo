<?php 
require_once("add/tables.php");

$order_data = array();
if ($_REQUEST['order_num'])
{
    $order_num = (int)($_REQUEST['order_num']);
    if($row_in_order = data_select_array($cb_tables['tableOrders'] , "status=0 and f7071=$order_num"))
    {
        $row_in_client = data_select_array($cb_tables['tableClients'] , "status=0 and id=".$row_in_order['f4441']);
        
        $order_data['num'] = $order_num;
        $order_data['date'] = date("Y-m-d", strtotime($row_in_order['f6591']));
        $order_data['sum'] = $row_in_order['f4461'];
        $order_data['service_id'] = $row_in_order['f4451'];
        $order_data['price_id'] = $row_in_order['f13071'];
        $order_data['description'] = $row_in_order['f4431'];
        $order_data['client'] = $row_in_client['f435'];
        
        if((float)($order_data['sum'])== 0 && (float)($_REQUEST['h_sum_total'])>0.0)
        {
            $new_sum = (float)($_REQUEST['h_sum_total']);
            data_update($cb_tables['tableOrders'],array("f4461"=>$new_sum),"id=".$row_in_order['id']);
        }        
    }
}
// ==================================================================

$doc_type = array();

$doc_type['doc_opis'] = array(
    'base' => 1, 
    'name' => "Опись", 
    'number' => 1,
    'pages' => 1
    );

$doc_type['doc_anketa'] = array(
    'base' => 1, 
    'name' => "Анкета",
    'number' => 1,
    'pages' => 1
);

$doc_type['doc_forms'] = array(
    'base' => 1,
    'name' => "Формы заявки",
    'number' => 1,
    'pages' => 1
);

$doc_type['doc_egrul'] = array(
    'base' => 1, 
    'name' => "Выписка ЕГРЮЛ/ЕГРИП",
    'number' => 1,
    'pages' => 3
);

$doc_type['doc_ustav'] = array(
    'base' => 1,
    'name' => "Устав",
    'number' => 1,
    'pages' => 10
);

$doc_type['doc_svid'] = array(
    'base' => 1,
    'name' => "Свидетельства",
    'number' => 2,
    'pages' => 1
);

$doc_type['doc_app_gurantee'] = array(
    'base' => 1,
    'name' => "Платежка или БГ",
    'number' => 1,
    'pages' => 1
);


$doc_type['doc_sro'] = array(
    'base' => 0, 
    'name' => "Свидетельство СРО",
    'number' => 1,
    'pages' => 5    
);

$doc_type['doc_license'] = array(
    'base' => 0,
    'name' => "Лицензия",
    'number' => 1,
    'pages' => 2
);

$doc_type['doc_finance'] = array(
    'base' => 0,
    'name' => "Финансовая отчетность",
    'number' => 1,
    'pages' => 5
);

$doc_type['doc_contracts'] = array(
    'base' => 0,
    'name' => "Договоры (опыт)",
    'number' => 3,
    'pages' => 5
);

$doc_type['doc_acts'] = array(
    'base' => 0,
    'name' => "Акты (опыт)",
    'number' => 3,
    'pages' => 2
);

$doc_type['doc_otzyv'] = array(
    'base' => 0,
    'name' => "Отзывы",
    'number' => 3,
    'pages' => 1
);

$doc_type['doc_personal_diploma'] = array(
    'base' => 0,
    'name' => "Диплом или свидетельство",
    'number' => 1,
    'pages' => 1
);

$doc_type['doc_personal_trudovaya'] = array(
    'base' => 0,
    'name' => "Трудовая книжка",
    'number' => 1,
    'pages' => 4
);

$doc_type['doc_technics'] = array(
    'base' => 0,
    'name' => "Карточки основных средств",
    'number' => 1,
    'pages' => 1
);

$doc_type['doc_other'] = array(
    'base' => 0,
    'name' => "Прочий документ",
    'number' => 1,
    'pages' => 1
);

// ==================================================================

$typical_apps = array();

$res_types = data_select($cb_tables['tablePrice'],"status=0 and f8500=5");
while($row_type = sql_fetch_assoc($res_types))
{
    $price_id = $row_type['id'];
    $price_name = $row_type['f8510'];
    $price_base = $row_type['f8520'];

    $typical_apps[$price_id] =
    array(
        "name" => $price_name,
        "base_price" => $price_base
    );
    
    if($order_data['price_id'] ==$price_id) $order_data['base_price'] = $price_base;
}

/*

$typical_apps['44_ea_ul'] =
array(
    "name" => "44-ФЗ Электронный аукцион",
    "base_price" => "2500",
    "contents"=> array(
        "doc_anketa"=>1
    )
);

$typical_apps['44_ok_ul'] = 
    array(
        "name" => "44-ФЗ Открытый конкурс",
        "base_price" => "5000",
        "contents"=> array(
                "doc_opis"=>1,
                "doc_anketa"=>1,
                "doc_egrul"=>1, 
                "doc_ustav"=>1, 
                "doc_app_gurantee"=>1, 
                "doc_forms"=>3
            )
    );
    
  */  
// ------------------------------------------------------------------

$smarty->assign("doc_type", $doc_type);
$smarty->assign("typical_apps", $typical_apps);
$smarty->assign("order_data", $order_data);
