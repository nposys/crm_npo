<?php

require_once 'add/db.php';
require_once 'add/tables.php';

$pspell = pspell_new('ru', '', '', 'utf-8', PSPELL_BAD_SPELLERS);

// setup SQL connection
ini_set('max_execution_time', 900);
ini_set('mssql.charset', 'UTF-8');
ini_set('mssql.connect_timeout', '120');
ini_set('mssql.timeout', '600');

// connect to ms sql
$serverName = $databases["MSSQL"]["server"];
$connectionOptions = array(
    "Database" => "materialdb",
    "Uid" => $databases["MSSQL"]["user"],
    "PWD" => $databases["MSSQL"]["pass"],
    'ReturnDatesAsStrings' => true
);
$sqlConnectId = sqlsrv_connect($serverName, $connectionOptions);

$groups = array();
$projects = array();

// получаем из базы данных объекты для сравнения
$sqlQuery = "SELECT f15391 AS grp_name FROM " . DATA_TABLE . $cb_tables['tableServiceObjGroups'] . " WHERE status=0 GROUP BY f15391";
$res = sql_query($sqlQuery);
while ($row = sql_fetch_array($res)) $groups[] = $row['grp_name'];

$sqlQuery = "SELECT f15421 AS proj_uid FROM " . DATA_TABLE . $cb_tables['tableServiceObjGroups'] . " WHERE status=0 GROUP BY f15421";
$res = sql_query($sqlQuery);
while ($row = sql_fetch_array($res)) {
    $sqlQuery = "SELECT p.[Title], p.[NameCustomer] FROM [materialdb].[dbo].[Projects] AS p WHERE p.[UID] = '" . $row['proj_uid'] . "'";
    $resultSQLQuery = sqlsrv_query($sqlConnectId, $sqlQuery);
    if ($resultSQLQuery) {
        if ($rowSQL = sqlsrv_fetch_array($resultSQLQuery)) {
            $next_project = array("uid" => $row['proj_uid'], "title" => $rowSQL['Title'] . " (" . $rowSQL['NameCustomer'] . ")");
            $projects[] = $next_project;
        }
    }
}

$obj_uid = $_REQUEST['obj_uid'];
$proj_uid = $_REQUEST['proj_uid'];
$group_id = $_REQUEST['group_id'];
$selected_mode = $_REQUEST['selected_mode'];
if ($selected_mode == '') $selected_mode = 'ТЗ';
$selected_group = $_REQUEST['selected_group'];
$selected_project = $_REQUEST['selected_project'];

if ($selected_project == "" && $proj_uid <> "") $selected_project = $proj_uid;

$mode = $_REQUEST['mode'];
if ($mode == 'req2app') $selected_group = 'Сравнение ТЗ и заявки';
if ($mode == 'cmp_ver') $selected_group = 'Сравнение версий';

if ($mode == 'compare_group' && $group_id <> "") {
    $rowObjGroup = data_select_array($cb_tables['tableServiceObjGroups'], "status=0 and id=$group_id");
    $selected_group = trim($rowObjGroup['f15391']);
}

if ($obj_uid <> "" && $selected_group == "") $selected_group = "Сравнение версий";
$selected_version_1 = $_REQUEST['selected_version_1'];
$selected_version_2 = $_REQUEST['selected_version_2'];

$req_versions = array();
$strings = array();

switch ($selected_group) {
    case "Ручное сравнение":
        $str1 = trim($_REQUEST['str1']);
        $str2 = trim($_REQUEST['str2']);
        $str3 = trim($_REQUEST['str3']);
        $str4 = trim($_REQUEST['str4']);
        $str5 = trim($_REQUEST['str5']);

        if ($str1 <> "") $strings[0] = $str1;
        if ($str2 <> "") $strings[1] = $str2;
        if ($str3 <> "") $strings[2] = $str3;
        if ($str4 <> "") $strings[3] = $str4;
        if ($str5 <> "") $strings[4] = $str5;

        break;

    case "Сравнение ТЗ и заявки":
        if ($db) {

            $sqlQuery = "
                        SELECT TOP 1
                            [ReqObjectTitle]
                            ,[ReqObjectDescription]
                            ,[ReqObjectStandart]
                            ,[ReqTimeChecked]
                            ,[ReqObjectVersion]
                            
                            ,[AppObjectTitle]
                            ,[AppObjectDescription]
                            ,[AppObjectStandart]
                            ,[AppTimeChecked]
                            ,[AppObjectVersion]
                            
                            ,[MaterialName]
                            ,[ObjectTitle]
                            ,[NumSmeta]
                            ,[EstimatesUID]
                        FROM
                            [materialdb].[dbo].[statGetActualMaterials]
                        WHERE
                            ObjectGroupUID='$obj_uid' and [ProjectUID]='$proj_uid'
                        ORDER BY ReqTimeChecked DESC
                        ";

            $resultSQLQuery = sqlsrv_query($sqlConnectId, $sqlQuery);
            if ($resultSQLQuery) {
                if ($rowSQL = sqlsrv_fetch_array($resultSQLQuery)) {

                    $req_string .= "Версия ТЗ и дата: [" . $rowSQL["ReqObjectVersion"] . "] " . $rowSQL["ReqTimeChecked"] . " . ";
                    $req_string .= "Название из сметы: " . $rowSQL["MaterialName"] . ". ";
                    $req_string .= "Номер сметы: " . $rowSQL["NumSmeta"] . ".";
                    $req_string .= "Название в проекте: " . $rowSQL["ReqObjectTitle"] . ". ";
                    if (trim($rowSQL["ReqObjectStandart"]) <> "") $req_string .= "Требования стандарта: " . $rowSQL["ReqObjectStandart"] . ". ";
                    $req_string .= $rowSQL["ReqObjectDescription"] . ". ";

                    $strings[] = $req_string;

                    $app_string .= "Версия ТЗ и дата: [" . $rowSQL["AppObjectVersion"] . "] " . $rowSQL["AppTimeChecked"] . " . ";
                    $app_string .= "Название из сметы: " . $rowSQL["MaterialName"] . ". ";
                    $app_string .= "Номер сметы: " . $rowSQL["NumSmeta"] . ".";
                    $app_string .= "Название в проекте: " . $rowSQL["AppObjectTitle"] . ". ";
                    if (trim($rowSQL["ReqObjectStandart"]) <> "") $app_string .= "Требования стандарта: " . $rowSQL["AppObjectStandart"] . ". ";
                    $app_string .= $rowSQL["AppObjectDescription"] . ". ";

                    $strings[] = $app_string;
                }
            }
        }
        break;

    case "Сравнение версий":

        if ($selected_mode == "ТЗ") $pref = "Req";
        else if ($selected_mode == "Заявка") $pref = "App";
        $arr_versions = array($selected_version_1, $selected_version_2);

        if ($obj_uid <> "") {
            if ($db) {
                foreach ($arr_versions as $next_version) {
                    $sqlQuery = "
                        SELECT 
                            TOP 1 so.[UID],so.[ObjectUID],so.[Title],so.[Description],so.[Standart],so.[ModifyDate], so.[Version], e.[MaterialName], e.[Number]
                        FROM 
                            [materialdb].[dbo].[" . $pref . "Objects] as so
                                LEFT JOIN [materialdb].[dbo].[Estimates] as e ON e.[" . $pref . "ObjectUID] = so.[UID]
                        WHERE 
                            so.[ObjectUID] in
                                (SELECT subobj.[UID] FROM [materialdb].[dbo].[Objects] as subobj WHERE subobj.[ObjectUID] = '$obj_uid') 
                            and so.[Version] = $next_version
                     ";

                    $next_string = "";
                    $resultSQLQuery = sqlsrv_query($sqlConnectId, $sqlQuery);
                    if ($resultSQLQuery) {
                        $rowSQL = sqlsrv_fetch_array($resultSQLQuery);
                        $next_string .= "Версия для сравнения: [" . $rowSQL["Version"] . "] " . $rowSQL["ModifyDate"] . ".";
                        $next_string .= "Название в проекте: " . $rowSQL["Title"] . ".";
                        if (trim($rowSQL["Standart"]) <> "") $next_string .= "Требования стандарта: " . $rowSQL["Standart"] . ".";
                        $next_string .= $rowSQL["Description"] . ".";
                    }

                    if (trim($next_string) <> "") $strings[] = $next_string;
                }
            }
        }
         break;

    default:

        $arr_fields = array(
            "ТЗ" => array(
                "fields" => array(
                    "title" => "ReqObjectTitle",
                    "description" => "ReqObjectDescription",
                    "standart" => "ReqObjectStandart",
                    "version" => "ReqObjectVersion",
                    "time" => "ReqTimeChecked"
                )
            ),
            "Заявка" => array(
                "fields" => array(
                    "title" => "AppObjectTitle",
                    "description" => "AppObjectDescription",
                    "standart" => "AppObjectStandart",
                    "version" => "AppObjectVersion",
                    "time" => "AppTimeChecked"
                )
            )
        );

        $res = data_select($cb_tables['tableServiceObjGroups'], "status=0 and f15391='$selected_group'");
        $obj_uids = array();
        while ($row = sql_fetch_array($res)) {
            $next_obj_uid = trim($row['f16001']);
            $next_string = "";

            if ($next_obj_uid <> "" && !in_array($next_obj_uid, $obj_uids)) {
                $obj_uids[] = $next_obj_uid;
                if ($db) {
                    $project_filter = "";
                    if ($selected_project <> "") $project_filter = " AND [ProjectUID] = '" . $selected_project . "' ";


                    $sqlQuery = "
                        SELECT TOP 1
                            " . $arr_fields[$selected_mode]["fields"]["title"] . "
                            ," . $arr_fields[$selected_mode]["fields"]["description"] . "
                            ," . $arr_fields[$selected_mode]["fields"]["standart"] . "
                            ," . $arr_fields[$selected_mode]["fields"]["version"] . "
                            ," . $arr_fields[$selected_mode]["fields"]["time"] . "
                            ,[MaterialName]
                            ,[ObjectTitle]
                            ,[NumSmeta]
                            ,[EstimatesUID]
                        FROM
                            [materialdb].[dbo].[statGetActualMaterials]
                        WHERE
                            ProjectWorkTypeId = 2 
                            and ProjectHidden = 0 
                            and ProjectStatusId in (1,2,3,4) 
                            and ObjectGroupUID='$next_obj_uid' 
                            $project_filter
                        ORDER BY 
                            ReqTimeChecked DESC
                        ";

                    $resultSQLQuery = sqlsrv_query($sqlConnectId, $sqlQuery);
                    if ($resultSQLQuery) {
                        if ($rowSQL = sqlsrv_fetch_array($resultSQLQuery)) {

                            $next_string .= "Версия и дата: [" . $rowSQL[$arr_fields[$selected_mode]["fields"]["version"]] . "] " . $rowSQL[$arr_fields[$selected_mode]["fields"]["time"]] . " . ";
                            $next_string .= "Название из сметы: " . $rowSQL["MaterialName"] . ". ";
                            $next_string .= "Номер сметы: " . $rowSQL["NumSmeta"] . ".";
                            $next_string .= "Название из базы: " . $rowSQL[$arr_fields[$selected_mode]["fields"]["title"]] . ". ";
                            if (trim($rowSQL[$arr_fields[$selected_mode]["fields"]["standart"]]) <> "")
                                $next_string .= "Требования стандарта: " . $rowSQL[$arr_fields[$selected_mode]["fields"]["standart"]] . ". ";
                            $next_string .= $rowSQL[$arr_fields[$selected_mode]["fields"]["description"]] . ". ";

                            if (trim($next_string) <> "") $strings[] = $next_string;
                        }
                    }
                }
            }
        }
        break;
}


$num_objects = count($strings); // количество объектов для сравнения

$data = array();
$data_res = array();

// подготавливаем данные для сравнения
$main_index = 0;
for ($i = 0; $i < $num_objects; $i++) {
    $strings[$i] = preg_replace('/\s\s+/', ' ', $strings[$i]); // очищаем лишние пробелы
    $strings[$i] = preg_replace('/(\:)/', ' $1 ', $strings[$i]); // очищаем лишние пробелы
    $data[$i]['data'] = getLineStat($strings[$i]);

    $data[$i]['proximity'] = getProximity($data[$main_index]['data'], $data[$i]['data']);
}

// =======================================================
// готовим вывод результатов

$excluded_index = array();
$used_index = array();

$css_error_style = 'color:#cc3300;font-weight:bold;font-size:110%;text-decoration:underline;';
$preg_mask = array('/[:,\.\(\)]/', '/[\s+(0-9)+\s+]/');

$exit = false;
$main_line = 0;
while (!$exit && $main_line < 1000) {
    $line_pref = "";
    $line_suf = "";
    $line_mid = array();

    for ($j = 1; $j < $num_objects; $j++) {
        // выбираем номер линии из подчиненного массива которую нам подсказывает статистика
        $num_sub = $data[$j]['proximity'][$main_line][0]['line_num'];
        //$num_main_best_match = getBestMatch($data[0], $data[$j], $main_line);
        $num_main_best_match = getBestMatchByShingles($data[0], $data[$j], $main_line);
        // echo "-- main_line:$main_line suggested_line for $j:$num_sub best_match for $j:$num_main_best_match<br>";

        if ($num_main_best_match == $num_sub) {
            if (!in_array($num_sub, $excluded_index[$j], true)) {
                // если нужная строка самая лучшая и мы ее раньше не использовали
                // то ищем сколько слов в начале строки совпало
                $data[0][$main_line]['similarity'][$j] = $data[$j]['proximity'][$main_line][0]['proximity'];

                // исключаем строку из дальнейших сравнений
                $excluded_index[$j][] = $num_sub;
            } else {
                //echo "for $main_line line $j.$num_sub already excluded<br>";
                $data[0][$main_line]['similarity'][$j] = 0;
            }
        } else {
            $data[0][$main_line]['similarity'][$j] = 0;
        }
    }

    $not_empty_string = false;

    // определяем минмиально надежное совпадение
    $min_similarity = min($data[0][$main_line]['similarity']);
    if ($min_similarity == "") $min_similarity = 0;

    //print_r($data[0]);
    //if($main_line==9) {     print_r($data[0][$main_line]['similarity']); echo "<br>";   }

    for ($k = 0; $k < $min_similarity; $k++) {
        $next_word = $data[0]['data'][$main_line]["words"][$k];
        $next_word_check = preg_replace($preg_mask, "", trim($next_word));
        if (!pspell_check($pspell, $next_word_check)) $next_word = "<font style='$css_error_style'>$next_word</font>";

        $line_pref .= $next_word . " ";
    }
    //echo "[$main_line] $line_pref<br>";
    if (trim($line_pref) <> "") $not_empty_string = true;

    // определяем максимальную длину строк в словах из всех варантов
    $max_words = 0;
    for ($m = 0; $m < $num_objects; $m++) {
        $num_match_line = $data[$m]['proximity'][$main_line][0]['line_num'];
        $next_max_words = count($data[$m]['data'][$num_match_line]['words']);

        if ($next_max_words > $max_words) $max_words = $next_max_words;
    }

    // заполняем различающиеся части строк
    for ($m = 0; $m < $num_objects; $m++) {
        $num_match_line = $data[$m]['proximity'][$main_line][0]['line_num'];

        for ($k = $min_similarity; $k < $max_words; $k++) {
            $next_word = $data[$m]['data'][$num_match_line]["words"][$k];
            $next_word_check = preg_replace($preg_mask, "", trim($next_word));
            if (!pspell_check($pspell, $next_word_check)) $next_word = "<font style='$css_error_style'>$next_word</font>";

            if ($m == 0) {
                $line_mid[$m] .= $next_word . " ";
            } else if (!in_array($num_match_line, $used_index[$m], true)) {
                $line_mid[$m] .= $next_word . " ";
            }

            if (trim($line_mid[$m]) <> "") $not_empty_string = true;
        }

        $used_index[$m][] = $num_match_line; // запоминаем фактически использованный индекс строк не являющихся эталоном
    }

    if ($min_similarity == $max_words) {
        $full_match = 1;
    } else {
        $full_match = 0;
    }

    if ($not_empty_string) {
        $data_res[$main_line]['full_match'] = $full_match;
        $data_res[$main_line]['line_pref'] = $line_pref;
        $data_res[$main_line]['line_suf'] = $line_suf;
        for ($l = 0; $l < $num_objects; $l++) {
            if (trim($line_mid[$l]) == "" && $l > 0 && $full_match == 0) $line_mid[$l] = "-";
            $data_res[$main_line]['lines_mid'][$l] = $line_mid[$l];
        }
    }

    $main_line++;

    if ($main_line == count($data[0]['data'])) $exit = true;
}


// добавляем линии не найденные в первых массивах
for ($i = 1; $i < $num_objects; $i++) {
    //print_r($used_index[$i]);
    $num_lines = count($data[$i]['data']);

    for ($j = 0; $j < $num_lines; $j++) {
        if (!in_array($j, $used_index[$i], true) && trim($data[$i]['data'][$j]['line']) <> "") {
            $data_res[$main_line]['full_match'] = -1;
            $data_res[$main_line]['line_pref'] = "";
            $data_res[$main_line]['line_suf'] = "";
            $data_res[$main_line]['lines_mid'][$i] = $data[$i]['data'][$j]['line'];

            $main_line++;
        }
    }
}

// ------------------------------------------------------------------------
// функция для преобразования строки в массивы и подсчета статистиик
// ------------------------------------------------------------------------
function getLineStat($str)
{
    $res = array();

    $arr = str_getcsv($str, "."); // разбиваем построчно предложения-параметры.

    // для каждой строки вычисляем параметры
    $i = 0;
    foreach ($arr as $line) {
        $line = trim($line);
        $chars = str_split($line, 1); // получаем массив символов из строки
        $char_sum = 0;
        $char_sum_diff = 0;

        // подсчитываем функции для строки
        $chars_codes = array();
        $j = 0;
        foreach ($chars as $char) {
            $char_code = ord($char);
            $char_sum += $char_code;

            $chars_codes[$j] = $char_code;
            if ($j > 0) $char_sum_diff += abs($chars_codes[$j] - $chars_codes[$j - 1]);

            $j++;
        }

        $words = explode(" ", $line);

        $res[$i]["line"] = $line;
        $res[$i]["words"] = $words;

        $res[$i]["stat"]["words_count"] = count($words);
        $res[$i]["stat"]["chars_count"] = count($chars);

        $res[$i]["stat"]["chars_sum"] = $char_sum;
        $res[$i]["stat"]["chars_diff_sum"] = $char_sum_diff;

        $i++;
    }

    return $res;
}

// ------------------------------------------------------------------------------------------------
// функция вычисляет близость строк в двух массивах данных
// ------------------------------------------------------------------------------------------------
function getProximity(&$data1, &$data2)
{
    $proximity = array();

    $len1 = count($data1);
    $len2 = count($data2);

    // вычисляем близость строк двух массивов
    for ($i = 0; $i < $len1; $i++) {
        // перебираем все строки второго массива
        $next_match = 0;
        for ($j = 0; $j < $len2; $j++) {
            $ind_prox = 0;
            $shingles = check_it($data1[$i]["line"], $data2[$j]["line"]);

            // сравниваем по словам сначала
            for ($k = 0; $k < $data1[$i]['stat']['words_count']; $k++) {
                if (trim($data1[$i]["words"][$k]) == trim($data2[$j]["words"][$k])) {
                    $ind_prox = $k + 1;
                } else {
                    break;
                }
            }

            if ($ind_prox > 0 || $shingles[1] > 30) {
                $proximity[$i][$next_match]['line_num'] = $j;
                $proximity[$i][$next_match]['proximity'] = $ind_prox;
                $proximity[$i][$next_match]['shingles'] = $shingles[1];
                $next_match++;
            }
        }
    }

    // пересортировываем сроик
    for ($i = 0; $i < $len1; $i++) {
        usort($proximity[$i], function ($a, $b) {
            return $b['proximity'] >= $a['proximity'];
        });
    }

    return $proximity;
}

// ------------------------------------------------------------------------------------------------
// функция которая возвращает наиболее близкую строку из массива 1 для заданной строки массива 2
// ------------------------------------------------------------------------------------------------
function getBestMatch($data1, $data2, $num_main)
{
    $len1 = count($data1['data']);

    $checked_line_num = $data2['proximity'][$num_main][0]['line_num'];
    $checked_proximity = $data2['proximity'][$num_main][0]['proximity'];

    $best_match = $checked_line_num;

    $prox_max = 0;
    for ($n = 0; $n < $len1; $n++) {
        if ($data2['proximity'][$n][0]['line_num'] == $checked_line_num &&
            $data2['proximity'][$n][0]['proximity'] > $checked_proximity) {
            $prox_max = $data2['proximity'][$n][0]['proximity'];
            $best_match = $n;
        }
    }

    return $best_match;
}

function getBestMatchByShingles($data1, $data2, $num_main)
{
    $len1 = count($data1['data']);

    $checked_line_num = $data2['proximity'][$num_main][0]['line_num'];
    $checked_proximity = $data2['proximity'][$num_main][0]['shingles'];

    $best_match = $checked_line_num;

    $prox_max = 0;
    for ($n = 0; $n < $len1; $n++) {
        if ($data2['proximity'][$n][0]['line_num'] == $checked_line_num &&
            $data2['proximity'][$n][0]['shingles'] > $checked_proximity) {
            $prox_max = $data2['proximity'][$n][0]['proximity'];
            $best_match = $n;
        }
    }

    return $best_match;
}

// ------------------------------------------------------------------------------------------------
// функции сравнения по шинглам
// ------------------------------------------------------------------------------------------------
function get_shingle($text, $n = 3)
{
    $shingles = array();
    $text = clean_text($text);
    $elements = explode(" ", $text);
    for ($i = 0; $i < (count($elements) - $n + 1); $i++) {
        $shingle = '';
        for ($j = 0; $j < $n; $j++) {
            $shingle .= mb_strtolower(trim($elements[$i + $j]), 'UTF-8') . " ";
        }
        if (strlen(trim($shingle)))
            $shingles[$i] = trim($shingle, ' -');
    }
    return $shingles;
}

function clean_text($text)
{
    $new_text = eregi_replace("[\,|\.|\'|\"|\\|\/]", "", $text);
    $new_text = eregi_replace("[\n|\t]", " ", $new_text);
    $new_text = preg_replace('/(\s\s+)/', ' ', trim($new_text));
    return $new_text;
}

function check_it($first, $second)
{
    $arr_res = array();

    if (!$first || !$second) {
        //echo "Отсутствуют оба или один из текстов!";
        return 1;
    }

    if (strlen($first) > 200000 || strlen($second) > 200000) {
        //echo "Длина обоих или одного из текстов превысила допустимую!";
        return 2;
    }


    for ($i = 1; $i < 5; $i++) {
        $first_shingles = array_unique(get_shingle($first, $i));
        $second_shingles = array_unique(get_shingle($second, $i));

        if (count($first_shingles) < $i - 1 || count($second_shingles) < $i - 1) {
            //echo "Количество слов в тексте меньше чем длинна шинглы<br />";
            continue;
        }

        $intersect = array_intersect($first_shingles, $second_shingles);

        $merge = array_unique(array_merge($first_shingles, $second_shingles));

        $diff = (count($intersect) / count($merge)) / 0.01;

        //echo "Количество слов в шингле - $i. Процент схожести - ".round($diff, 2)."%<br />";
        $arr_res[$i] = $diff;
    }

    return $arr_res;
}

// ------------------------------------------------------------------------------------------------
// Переносим переменные в отображение
// ------------------------------------------------------------------------------------------------

$smarty->assign("data", $data);
$smarty->assign("strings", $strings);
$smarty->assign("groups", $groups);
$smarty->assign("projects", $projects);

$smarty->assign("req_versions", $req_versions);
$smarty->assign("selected_version_1", $selected_version_1);
$smarty->assign("selected_version_2", $selected_version_2);

$smarty->assign("obj_uid", $obj_uid);
$smarty->assign("proj_uid", $proj_uid);

$smarty->assign("num_objects", $num_objects);
$smarty->assign("data_res", $data_res);
$smarty->assign("selected_mode", $selected_mode);
$smarty->assign("selected_group", $selected_group);
$smarty->assign("selected_project", $selected_project);
