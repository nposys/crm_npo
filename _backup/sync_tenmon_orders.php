<?php 

require_once 'add/db.php';
require_once 'add/tables.php';
require_once 'add/functions/service_functions.php';
require_once 'add/functions/tenmon_functions.php';
require_once 'add/functions/common_order_functions.php';

// ---------------------------------------------------------------------
// подключаемся к базе ms sql
// ---------------------------------------------------------------------
$bSQLok = false;
$sqlLink = mssql_connect ($databases['MSSQL']['server'] , $databases['MSSQL']['user'], $databases['MSSQL']['pass']);
if ($sqlLink)
{
    $db = mssql_select_db($databases['MSSQL']['db'], $sqlLink);
    if ($db) $bSQLok = true;
    else mssql_close($sqlLink);
}

// ---------------------------------------------------------------------
// подключаемся к базе тенмон
// ---------------------------------------------------------------------
$bTMok = false;
$mysqli = mysqli_init();
if (!$mysqli->real_connect($databases['TM']['server'],  $databases['TM']['user'], $databases['TM']['pass'], $databases['TM']['database'])) 
{
    die('Ошибка подключения (' . mysqli_connect_errno() . ') '
        . mysqli_connect_error());
}
else 
    $bTMok = true;
$mysqli->query("SET NAMES 'utf8'");

$count_clients_missed = 0;
$count_clients_ok = 0;
$count_clients_disabled = 0;
$count_clients_enabled = 0;

// ---------------------------------------------------------------------
// 1. выбираем все заказы с search_enable из Tenmon
// ---------------------------------------------------------------------
$query_tm =
"
SELECT
    u.ID
    ,u.ShortName
    ,u.FullName
    ,u.INN
    ,u.KPP
    ,u.OGRN
    ,u.LegalAddress
    ,u.SearchEnable
    ,u.ManagerID
    ,u.login
    ,u.is_Advert
    ,u.Tariff
FROM 
       tender_monitor.Users as u
WHERE 
      u.SearchEnable = 1
ORDER BY
       u.ID ASC
";
if ($result_tm = $mysqli->query($query_tm)) {
    write_log("Select вернул ".$result_tm->num_rows." строк.");

    while ($row_tm = $result_tm->fetch_array(MYSQLI_ASSOC))
    {
        $tenmon_uid = intval($row_tm['ID']);
        $tenmom_client_name = trim($row_tm['ShortName']);
        $tenmon_client_inn = trim($row_tm['INN']);
        $tenmon_tariff = intval($row_tm['Tariff']);
        
        // ---------------------------------------------------------------------
        // 2. проверяем существует ли такой клиент в CRM
        // ---------------------------------------------------------------------
        $row_client = data_select_array($cb_tables['tableClients'], "status=0 and f16230=$tenmon_uid");
        
        if($row_client['id']>0)
        {
            $client_id = $row_client['id'];
            // ---------------------------------------------------------------------
            // 3. проверяем в CRM есть ли действующий заказ на клиентов в данной группе компаний
            // ---------------------------------------------------------------------
            
            $row_order_search = GetSearchOrderForCompanyGroup($client_id, $cb_tables);            
            if($row_order_search['id']>0)
            {
                $order_num = $row_order_search['f7071'];
                $crm_tariff = GetTenmonTariffByPriceID($row_order_search['f13071']);
               
                // если тариф в CRM не совпадате с тем что в TM то включаем правильный тариф и поиск
                if($crm_tariff<>$tenmon_tariff)
                {
                    // write_log("{$tenmon_uid} client $tenmom_client_name [$tenmon_client_inn] ($str_company_group) have active search in CRM num:$order_num (tenmon_tariff:$tenmon_tariff) (crm_tariff:$crm_tariff)");
                    SetTenmonTariff($tenmon_uid, $crm_tariff, true, true, $sqlLink);
                }

                $count_clients_ok++;
            }
            else
            {
                // ---------------------------------------------------------------------
                // 4. отключаем не действующие заказы
                // ---------------------------------------------------------------------
                write_log("---- {$tenmon_uid} disable client $tenmom_client_name [$tenmon_client_inn] don't have active search in CRM (tariff:$tenmon_tariff)");
                
                SetTenmonTariff($tenmon_uid, 1, false, true, $sqlLink);
                $count_clients_disabled++;
            }            
            
        } 
        else 
        {
            //write_log(" ???? {$tenmon_uid} client $tenmom_client_name [$tenmon_client_inn] don't exist in CRM (tariff:$tenmon_tariff)");
            $count_clients_missed++;
        } // end if row_client exists
        
        
    } // end while tm_row
    
    /* очищаем результирующий набор */
    $result_tm->close();
    
} // end if result sql tm

// =================================================================================
// проверяем заказы на поиск не привязанные к ТМ
// =================================================================================


$res_orders = data_select($cb_tables['tableOrders'], "status=0 and f4451=4 and f6551='10. Исполнение'");
while($row_order = sql_fetch_assoc($res_orders))
{
    $order_id = $row_order['id'];
    $order_num = $row_order['f7071'];    
    $price_id = $row_order['f13071'];
    $crm_tariff = GetTenmonTariffByPriceID($price_id);
    
    // проверяем что заплнено поле id клиента (защита от ошибки)
    if($row_order['f4441']>0)
    {
        $bHaveTmClient = false;
    
        $row_client = data_select_array($cb_tables['tableClients'],"id=".$row_order['f4441']);
        
        $company_group_id = $row_client['f6301'];
        $client_name = $row_client['f435'];
        $client_inn = $row_client['f1056'];
        $tenmon_uid = $row_client['f16230'];
        
        $tm_data = GetTenmonClientData($tenmon_uid, $mysqli);
        $tm_tariff = $tm_data['Tariff'];
        $tm_search_enable = $tm_data['SearchEnable'];
        
        if($tenmon_uid>0)
        {
            // исправляем тариф для компаний у которых заказ на поиск создан на тоже лицо что и регистрация в tm
            if($crm_tariff<>$tm_tariff)
            {
                if($crm_tariff==2 || $crm_tariff=3) $bSearchEnable = true;
                else $bSearchEnable = false;
                $res = SetTenmonTariff($tenmon_uid, $crm_tariff, $bSearchEnable, true, $sqlLink);
                write_log("enable tariff for $client_name tm:$tenmon_uid tariff:$crm_tariff ($bSearchEnable)");
            }
            
            $bHaveTmClient = true;
        }
        
        
        // если это группа компаний, то проверяем нет ли других компаний из этой группы в тенмоне
        if($company_group_id>0) 
        {
            // todo: добавить получение максиманльного тарифа для группы компаний
            
            $res_group = data_select($cb_tables['tableClients'],"f6301=$company_group_id");
            while($row_next_company = sql_fetch_assoc($res_group))
            {
                $next_company_tenmon_uid = intval($row_next_company['f16230']);
                $next_company_name = $row_next_company['f435'];
                $next_company_inn = $row_next_company['f1056'];
                
                // проверяем что поле tm_uid заполнено у очередной компании из группы компаний
                if($next_company_tenmon_uid>0)
                {
                    $tm_next_comapny_data = GetTenmonClientData($next_company_tenmon_uid, $mysqli);
                    $tm_next_comapny_tariff = $tm_next_comapny_data['Tariff'];
                    $tm_next_comapny_search_enable = $tm_next_comapny_data['SearchEnable'];
                    
                    if($crm_tariff>$tm_next_comapny_tariff)
                    {
                        if($crm_tariff==2 || $crm_tariff=3) $bSearchEnable = true;
                        else $bSearchEnable = false;
                        $res = SetTenmonTariff($next_company_tenmon_uid, $crm_tariff, $bSearchEnable, true, $sqlLink);
                        // write_log("enable tariff for company in group: tm_uid:$next_company_tenmon_uid $next_company_name $next_company_inn crm_tariff:$crm_tariff tm_tariff:$tm_next_comapny_tariff");
                    }
                    
                    /*
                    // включаем поиск и устанавливаем тариф для данного клиента
                    if($crm_tariff==2 || $crm_tariff=3) $bSearchEnable = true;
                    else $bSearchEnable = false;                    
                    // SetTenmonTariff($tenmon_uid, $crm_tariff, $bSearchEnable, true, $sqlLink);                    
                    */
                    
                    $bHaveTmClient = true;
                }
            }
        } // end if company_group
        
        if(!$bHaveTmClient)
        {
            write_log("----Don't have client or group for $client_name [$client_inn] in TM but have active search order in CRM order_id:$order_num");
        }
       //else  write_log("Have client $client_name in TM $tenmon_uid for order_id:$order_id");
    }
}

write_log("no clients in CRM: $count_clients_missed");
write_log("tariffs remained: $count_clients_ok");
write_log("tariffs disabled: $count_clients_disabled");

if($bSQLok) mssql_close($sqlLink);
if($bTMok) $mysqli->close();

?>