<?php 
// отчет для просмотра активности того или иного поставщика
require_once 'add/db.php';
require_once 'add/tables.php';

$debug = false;

// =============================================================================
// обрабатываем фильтры на входе
if ($_REQUEST['date1']) $date1 = date("d.m.Y",strtotime(form_eng_time($_REQUEST['date1'])));
else $date1 = "01.01.2010";

if ($_REQUEST['date2']) $date2 = date("d.m.Y",strtotime(form_eng_time($_REQUEST['date2'])));
else $date2 = date("d.m.Y");

$date1_fet = form_eng_time($date1.' 00:00:00');
$date2_fet = form_eng_time($date2.' 23:59:59');

$max_on_page = 500;
$page = 1;
$num_start = 0;
$num_finish = $max_on_page;
$num_total = 0;
// ----------------------------------------------------------------------
// объявляем переменные
$protocol_types = array(
    "44-ФЗ" => 
     array(
        "Электронный аукцион" =>
            array(
               "table_name" => "ProtocolsEF",
               "field_price" => "(CASE WHEN prot.AppRating=1 and prot.LastPrice IS NULL THEN l.cost ELSE prot.LastPrice END)",
               "field_win_price" => "(CASE WHEN prot_win.AppRating=1 and prot_win.LastPrice IS NULL THEN l.cost ELSE prot_win.LastPrice END)",
                "field_date" => "prot.LastDate",
               "field_preadmitted" => "prot.FirstPartAdmitted",
               "field_admitted" => "prot.SecondPartAdmitted",
               "field_place" => "prot.AppRating",
               "field_protocol_inn" => "prot.INN",
               "field_protocol_kpp" => "prot.KPP",
               "field_protocol_contactperson" => "prot.ContactPerson",
               "field_protocol_phone" => "prot.Phone",
               "field_protocol_email" => "prot.Email",
               "field_protocol_apprejectreason" => "prot.AppRejectReason",
               "field_protocol_apprejectexplanation" => "prot.AppRejectExplanation",
               "field_prot_win_supplier_id" => "prot_win.SuppliersID",
                "where_join_lots_and_tenders" => "LEFT JOIN lots as l on prot.LotUID = l.LotUID LEFT JOIN tenders as t on t.UID = l.TenderUID",
               "where_supplier_id" => "prot.SuppliersID",
               "prot_win_join_condition" => "prot_win.LotUID = prot.LotUID and prot_win.AppRating = 1"
                
            ),
        "Открытый конкурс" =>
            array(
               "table_name" => "ProtocolsOK",
               "field_price" => "prot.Price",
               "field_win_price" => "prot_win.Price",
                "field_date" => "prot.AppDate",
               "field_preadmitted" => "NULL",
               "field_admitted" => "prot.Admitted",
               "field_place" => "prot.AppRating",
               "field_protocol_inn" => "prot.INN",
               "field_protocol_kpp" => "prot.KPP",
               "field_protocol_contactperson" => "prot.ContactPerson",
               "field_protocol_phone" => "prot.Phone",
               "field_protocol_email" => "prot.Email",
               "field_protocol_apprejectreason" => "prot.AppRejectReason",
               "field_protocol_apprejectexplanation" => "prot.AppRejectExplanation",
               "field_prot_win_supplier_id" => "prot_win.SuppliersID",
                "where_join_lots_and_tenders" => "LEFT JOIN lots as l on prot.LotUID = l.LotUID LEFT JOIN tenders as t on t.UID = l.TenderUID",
               "where_supplier_id" => "prot.SuppliersID",
                "prot_win_join_condition" => "prot_win.LotUID = prot.LotUID and prot_win.AppRating = 1"
                
            ),
        "Запрос котировок" =>
            array(
               "table_name" => "ProtocolsZK",
               "field_price" => "prot.Cost",
               "field_win_price" => "prot_win.Cost",
                "field_date" => "prot.AppDate",
               "field_preadmitted" => "NULL",
               "field_admitted" => "prot.Admitted",
               "field_place" => "prot.AppRating",
               "field_protocol_inn" => "prot.INN",
               "field_protocol_kpp" => "prot.KPP",
               "field_protocol_contactperson" => "prot.ContactPerson",
               "field_protocol_phone" => "prot.Phone",
               "field_protocol_email" => "prot.Email",
               "field_protocol_apprejectreason" => "prot.AppRejectReason",
               "field_protocol_apprejectexplanation" => "prot.AppRejectExplanation",
               "field_prot_win_supplier_id" => "prot_win.SuppliersID",
                "where_join_lots_and_tenders" => "LEFT JOIN lots as l on prot.LotUID = l.LotUID LEFT JOIN tenders as t on t.UID = l.TenderUID",
               "where_supplier_id" => "prot.SuppliersID",
                "prot_win_join_condition" => "prot_win.LotUID = prot.LotUID and prot_win.AppRating = 1"
            ),
     ),
    "223-ФЗ" =>
        array(
            "Прочие" =>
            array(
                "table_name" => "prot_Results",
                "field_price" => "prot.Price",
                "field_win_price" => "prot_win.Price",
                "field_date" => "prot.AppDate",
                "field_preadmitted" => "NULL",
                "field_admitted" => "(CASE WHEN prot.Admitted Is NULL THEN -1 ELSE prot.Admitted END)",
                "field_place" => "prot.AppPlace",
                "field_protocol_inn" => "prot.SupplierINN",
                "field_protocol_kpp" => "prot.SupplierKPP",
                "field_protocol_contactperson" => "NULL",
                "field_protocol_phone" => "NULL",
                "field_protocol_email" => "NULL",
               "field_protocol_apprejectreason" => "NULL",
               "field_protocol_apprejectexplanation" => "NULL",
                "field_prot_win_supplier_id" => "prot_win.SupplierID",
                "where_join_lots_and_tenders" => "LEFT JOIN tenders as t on t.GovRuID = prot.GovRuId and t.UID in (select UID from UniqueTenders as ut where ut.GovRuID=prot.GovRuId) LEFT JOIN lots as l on l.TenderUID=t.UID and prot.LotNumber = l.LotNumber",
                "where_supplier_id" => "prot.SupplierID",
                "prot_win_join_condition" => "prot_win.GovRuId = prot.GovRuId and prot_win.LotNumber = prot.LotNumber and prot_win.AppPlace = 1"
                )            
        ),
    "служебное" =>
        array(
           "Поставщики" =>
            array(
                "table_name" => "Suppliers",
                "field_protocol_inn" => "prot.INN",
                "field_protocol_kpp" => "prot.KPP",
                "field_protocol_contactperson" => "prot.ContactPerson",
                "field_protocol_phone" => "prot.Phone",
                "field_protocol_email" => "prot.Email"                
            ),
            "Контракты" => 
            array(
                "table_name" => "Contracts",
                "field_protocol_inn" => "prot.INN",
                "field_protocol_kpp" => "prot.KPP",
                "field_protocol_contactperson" => "prot.ContactPerson",
                "field_protocol_phone" => "prot.Phone",
                "field_protocol_email" => "prot.SupplierEmail"                
            )
        )
);

$fields_contacts = array("phones"=>"field_protocol_phone", "emails"=>"field_protocol_email", "contacts"=>"field_protocol_contactperson");

// ----------------------------------------------------------------------
// обрабока переменных с формы

$dt_period = ""; // даты участий
if ($_REQUEST['inn']) $inn = trim($_REQUEST['inn']); else $inn="";//"5405442239"; // фильтр по ИНН
$kpp = ""; // фильтр по КПП
$title = ""; // фильтр по названию
$contactperson = ""; // фильтр по контактному лицу
$email = ""; // фильтр по емайл
$phone = ""; // фильтр по телефону
$address = ""; // фильтр по адресу

// ----------------------------------------------------------------------
// получение и подготовка данных

$arr_result = array();
$arr_result["tenders"] = array();
$arr_result["totals"] = array();

// подключение к SQL
if($inn<>"")
{
    ini_set('max_execution_time', 900);
    ini_set('mssql.charset','UTF8');
    ini_set('mssql.connect_timeout','120');
    ini_set('mssql.timeout','600');
    $sqlConnectId = mssql_connect ($databases["MSSQL"]["server"] , $databases["MSSQL"]["user"] , $databases["MSSQL"]["pass"] );
    
    if($sqlConnectId)
    {
        if($debug) echo "sql connected<br>\r\n";
        $db = mssql_select_db($databases["MSSQL"]["db"], $sqlConnectId);
        if ($db)
        {
            // получаем контактную информацию из всех источников
            foreach($fields_contacts as $name_array=>$field)
            {
                $sqlQueryBase = "SELECT @field@ as @field_output@ FROM @table_name@ WHERE @field_protocol_inn@ LIKE '@inn@%' GROUP BY @field@ HAVING @field@<>''";
                $sqlQuery = "";
                
                $i = 0;
                foreach ($protocol_types as $tender_law => $protocol)
                {
                    foreach ($protocol as $tender_type => $protocol_data)
                    {
                        $table_name = $protocol_data["table_name"];
                        $field_name = str_replace("prot.", "", $protocol_data[$field]);
                        $field_protocol_inn = str_replace("prot.", "", $protocol_data["field_protocol_inn"]);
                        
                        if($field_name <> "NULL")
                        {
                            $nextQuery = str_replace("@inn@",$inn,
                                            str_replace("@field@", $field_name, 
                                                str_replace("@field_protocol_inn@", $field_protocol_inn, 
                                                    str_replace("@table_name@", $table_name, 
                                                        str_replace("@field_output@", $field, 
                                                            $sqlQueryBase
                                                        )
                                                    )
                                                )
                                            )
                                          );
                            //if($debug) echo "!!!!!! next contact query: $nextQuery<br>\r\n";
                            if($i>0) $sqlQuery .= " UNION ".$nextQuery; else $sqlQuery .= $nextQuery;
                            $i++;
                        }
                    }
                }

                $resultSQLQuery = mssql_query($sqlQuery, $sqlConnectId);
                //if($debug) echo "next contact query: $sqlQuery<br>\r\n";
                while($rowSQL = mssql_fetch_array($resultSQLQuery, MSSQL_BOTH))
                    $arr_result["suppliers"][$inn][$name_array][] = trim($rowSQL[$field]);                
            } // end foreach field_contacts
            
            //if($debug) print_r($arr_result);
            
            // вычисляем число участий поставщика по всем протоколам
            foreach ($protocol_types as $tender_law => $protocol)
            {
                if($tender_law <> "служебное")
                {
                    foreach ($protocol as $tender_type => $protocol_data)
                    {
                        // get total number of records
                        $sqlQueryBase = "SELECT count(*) as CountNum FROM ".$protocol_data['table_name']." as prot WHERE @field_protocol_inn@ LIKE '$inn%'";
                        $sqlQuery = str_replace("@field_protocol_inn@",$protocol_data["field_protocol_inn"], $sqlQueryBase);
                        //if($debug) echo "next count query:<br>\r\n $sqlQuery<br>\r\n";
                        $resultSQLQuery = mssql_query($sqlQuery, $sqlConnectId);
                        $rowSQL = mssql_fetch_array($resultSQLQuery, MSSQL_BOTH);
                        $num_total += $rowSQL['CountNum'];                    
                    }
                }
            }

            if($max_on_page>$num_total) $num_finish = $num_total;
                
            $sqlQuery = "
                SELECT
                	  '@tender_law@' as TenderLaw
                	  ,'@tender_type@' as TenderType
                	  ,t.GovRuID 
                	  ,l.Title
                	  ,l.Cost
                	  ,t.PublicDate
                	  ,t.OpenDate
                	  ,t.ExamDate
                	  ,t.StartDate
                	  ,t.Link
                	  ,r.Title as RegionTitle
                	  ,o.Title as OkrugTitle
                	  
                	  ,gc.FullName as CustomerTitle
	                  ,gc.INN as CustomerINN
	                  ,gc.KPP as CustomerKPP
	                  
	                  ,sup_win.INN as WinnerINN
	                  ,sup_win.KPP as WinnerKPP
	                  ,sup_win.Title as WinnerTitle
	                  ,sup_win.Form as WinnerOrgForm 
	                  ,sup_win.ContactPerson as WinnerContactPerson
                      ,@field_win_price@ as WinnerPrice
	                  
                	  ,sup.INN as SupplierINN
                	  ,sup.KPP as SupplierKPP
                	  ,sup.Title as SupplierTitle
                	  ,sup.Form as SupplierOrgForm
                	  ,sup.Phone as SupplierPhone
                	  ,sup.Fax as SupplierFax
                	  ,sup.Email as SupplierEmail
                	  ,sup.FactualAddress as SupplierFactAddr
                	  ,sup.PostAddress as SupplierPostAddr
                	  ,sup.ContactPerson as SupplierContactPerson
                
                      ,@field_protocol_inn@ as ProtocolINN
                      ,@field_protocol_kpp@ as ProtocolKPP
                      ,@field_protocol_contactperson@ as ProtocolContactPerson
                      ,@field_protocol_email@ as ProtocolEmail
                      ,@field_protocol_phone@ as ProtocolPhone
                
                      ,@field_price@ as Price
                      ,@field_date@ as DateFromProtocol
                      ,@field_preadmitted@ as PreAdmitted
                      ,@field_admitted@ as Admitted

                      ,@field_protocol_apprejectreason@ as AppRejectReason
                      ,@field_protocol_apprejectexplanation@ as AppRejectExplanation
                      ,@field_place@ as AppRating
                      ,prot.Comment
                  FROM 
                	@table_name@ as prot
                		LEFT JOIN Suppliers as sup on sup.ID = @where_supplier_id@ 
                		@where_join_lots_and_tenders@
                		LEFT JOIN Cities as c ON c.ID = t.City
                		LEFT JOIN Regions as r ON r.ID = c.Region
                		LEFT JOIN Okrugs as o ON o.ID = r.Okrug
                		LEFT JOIN GovCustomers as gc on gc.ID = l.CustomerID
                          LEFT JOIN @table_name@ as prot_win on @prot_win_join_condition@ 
		                  LEFT JOIN Suppliers as sup_win on sup_win.ID = @field_prot_win_supplier_id@ 
                WHERE 
                	@field_protocol_inn@ LIKE '$inn%'
                ORDER BY 
                	t.StartDate DESC
                OFFSET ".$num_start." ROWS
                FETCH NEXT ".($num_finish-$num_start)." ROWS ONLY
                ";

            $sumWins = 0.0;
            $sumApps = 0.0;
            
            foreach ($protocol_types as $tender_law => $protocol)
            {
                if($tender_law <> "служебное")
                {
                    foreach ($protocol as $tender_type => $protocol_data)
                    {
                        $protocol_query = $sqlQuery;
                        
                        $protocol_query = str_replace("@tender_law@", $tender_law, $protocol_query);
                        $protocol_query = str_replace("@tender_type@", $tender_type, $protocol_query);                    
                        
                        foreach($protocol_data as $field_name => $field_value )
                            $protocol_query = str_replace("@".$field_name."@", $field_value, $protocol_query);
    
                            //if($debug) echo "next query:<br>\r\n $protocol_query<br>\r\n";                        
                            $resultSQLQuery = mssql_query($protocol_query, $sqlConnectId);
                            if($resultSQLQuery)
                            {
                                $numRows = mssql_num_rows($resultSQLQuery);
                                while($rowSQL = mssql_fetch_array($resultSQLQuery, MSSQL_BOTH))
                                {
                                    $govRuId =  $rowSQL['GovRuID'];
                                    $inn = trim($rowSQL['SupplierINN']);
                                    $kpp = trim($rowSQL['SupplierKPP']);
                                    $phone = trim($rowSQL['SupplierPhone']);
                                    $email = trim($rowSQL['SupplierEmail']);
                                    $contact = trim($rowSQL['SupplierContactPerson']);
                                    
                                    $region = trim($rowSQL['RegionTitle']);
                                    $okrug = trim($rowSQL['OkrugTitle']);

                                    $prot_phone = trim($rowSQL['ProtocolPhone']);
                                    $prot_email = trim($rowSQL['ProtocolEmail']);
                                    $prot_contact = trim($rowSQL['ProtocolContactPerson']);
                                    
                                    $winner_inn = trim($rowSQL['WinnerINN']);
                                    $winner_kpp = trim($rowSQL['WinnerKPP']);
                                    $winner_title = trim($rowSQL['WinnerTitle']);
                                    $winner_form = trim($rowSQL['WinnerOrgForm']);                                    
                                    $winner_contact = trim($rowSQL['WinnerContactPerson']);
                                    if($winner_inn<>"" && $winner_title=="" && $winner_contact<>"") $winner_title = $winner_contact;

                                    // получаем данные о заказах
                                    $order_app = "";
                                    $rowOrdApp = data_select_array($cb_tables['tableOrdersApp'],"status=0 and f7441='$govRuId'");
                                    if($rowOrdApp['id']<>"")
                                    {
                                        $rowOrd = data_select_array($cb_tables['tableOrders'],"status=0 and id=".$rowOrdApp['f5471']);
                                        $rowClient = data_select_array($cb_tables['tableClients'],"status=0 and id=".$rowOrd['f4441']);
                                        if(trim($rowClient['f1056'])==$inn)
                                            $order_app = $rowOrd['f7071'];
                                    }
                                    
                                    $arr_result["tenders"][$govRuId]["order_app"] = $order_app;
                                    $arr_result["tenders"][$govRuId]["start_date"] = substr(str_replace('.0000000','',$rowSQL['StartDate']),0,10);
                                    $arr_result["tenders"][$govRuId]["govruid"] = $govRuId;
                                    $arr_result["tenders"][$govRuId]["tender_law"] = $rowSQL['TenderLaw'];
                                    $arr_result["tenders"][$govRuId]["tender_type"] = $rowSQL['TenderType'];
                                    $arr_result["tenders"][$govRuId]["title"] = $rowSQL['Title'];
                                    $arr_result["tenders"][$govRuId]["link"] = $rowSQL['Link'];
                                    $arr_result["tenders"][$govRuId]["cost"] = $rowSQL['Cost'];
                                    $arr_result["tenders"][$govRuId]["price"] = $rowSQL['Price'];
                                    $arr_result["tenders"][$govRuId]["place"] = $rowSQL['AppRating'];
                                    $arr_result["tenders"][$govRuId]["pre_admitted"] = $rowSQL['PreAdmitted'];
                                    $arr_result["tenders"][$govRuId]["admitted"] = $rowSQL['Admitted'];
                                    $arr_result["tenders"][$govRuId]["reject_reason"] = $rowSQL['AppRejectReason'];
                                    $arr_result["tenders"][$govRuId]["reject_explanation"] = $rowSQL['AppRejectExplanation'];

                                    $arr_result["tenders"][$govRuId]["region"] = $region;
                                    $arr_result["tenders"][$govRuId]["okrug"] = $okrug;
                                    
                                    // статистика участий по регионам
                                    $arr_result["suppliers"][$inn]["regions"][$region]["title"] = $region;
                                    $arr_result["suppliers"][$inn]["regions"][$region]["count"] += 1;
                                    
                                    $arr_result["tenders"][$govRuId]["winner_title"] = $winner_title;
                                    $arr_result["tenders"][$govRuId]["winner_form"] = $winner_form;
                                    $arr_result["tenders"][$govRuId]["winner_inn"] = $winner_inn;
                                    $arr_result["tenders"][$govRuId]["winner_kpp"] = $winner_kpp;
                                    $arr_result["tenders"][$govRuId]["winner_price"] = $rowSQL['WinnerPrice'];

                                    // статистика по конкурентам
                                    if($winner_inn<>$inn && $winner_inn<>"")                                        
                                    {
                                        if($arr_result["suppliers"][$inn]["competitors"][$winner_inn]["inn"] == "")
                                        {
                                            $arr_result["suppliers"][$inn]["competitors"][$winner_inn]["inn"] = $winner_inn;
                                            $arr_result["suppliers"][$inn]["competitors"][$winner_inn]["kpp"] = $winner_kpp;
                                            $arr_result["suppliers"][$inn]["competitors"][$winner_inn]["title"] = $winner_title;
                                            $arr_result["suppliers"][$inn]["competitors"][$winner_inn]["form"] = winner_form;
                                        }
                                        $arr_result["suppliers"][$inn]["competitors"][$winner_inn]["count"] += 1;                                  
                                    }                                    
                                    
                                    $arr_result["tenders"][$govRuId]["supplier_title"] = trim($rowSQL['SupplierTitle']);
                                    $arr_result["tenders"][$govRuId]["supplier_form"] = trim($rowSQL['SupplierOrgForm']);
                                    $arr_result["tenders"][$govRuId]["supplier_inn"] = trim($rowSQL['ProtocolINN']);
                                    $arr_result["tenders"][$govRuId]["supplier_kpp"] = trim($rowSQL['ProtocolKPP']);
                                    
                                    $arr_result["tenders"][$govRuId]["supplier_contact"] = trim($rowSQL['ProtocolContactPerson']);
                                    $arr_result["tenders"][$govRuId]["supplier_phone"] = trim($rowSQL['ProtocolPhone']);
                                    $arr_result["tenders"][$govRuId]["supplier_email"] = trim($rowSQL['ProtocolEmail']);
                                    
                                    $arr_result["tenders"][$govRuId]["customer_title"] = trim($rowSQL['CustomerTitle']);
                                    $arr_result["tenders"][$govRuId]["customer_inn"] = trim($rowSQL['CustomerINN']);
                                    $arr_result["tenders"][$govRuId]["customer_kpp"] = trim($rowSQL['CustomerKPP']);
                            
                                    if($arr_result["suppliers"][$inn]["title"]=="") $arr_result["suppliers"][$inn]["title"] = trim($rowSQL['SupplierTitle']);
                                    if($arr_result["suppliers"][$inn]["form"]=="") $arr_result["suppliers"][$inn]["form"] = trim($rowSQL['SupplierOrgForm']);
                                    if($arr_result["suppliers"][$inn]["inn"]=="") $arr_result["suppliers"][$inn]["inn"] = $inn;
                                    if($arr_result["suppliers"][$inn]["kpp"]=="") $arr_result["suppliers"][$inn]["kpp"] = $kpp;
                                    if($arr_result["suppliers"][$inn]["orgform"]=="") $arr_result["suppliers"][$inn]["orgform"] = trim($rowSQL['SupplierOrgForm']);
                            
                                    if($phone<>"" && !in_array($phone, $arr_result["suppliers"][$inn]["phones"])) $arr_result["suppliers"][$inn]["phones"][] = $phone;
                                    if($prot_phone<>"" && !in_array($prot_phone, $arr_result["suppliers"][$inn]["phones"]))  $arr_result["suppliers"][$inn]["phones"][] = $prot_phone;
                            
                                    if($email<>"" && !in_array($email, $arr_result["suppliers"][$inn]["emails"])) $arr_result["suppliers"][$inn]["emails"][] = $email;
                                    if($prot_email<>"" && !in_array($prot_email, $arr_result["suppliers"][$inn]["emails"]))  $arr_result["suppliers"][$inn]["emails"][] = $prot_email;
                            
                                    if($contact<>"" && !in_array($contact, $arr_result["suppliers"][$inn]["contacts"])) $arr_result["suppliers"][$inn]["contacts"][] = $contact;
                                    if($prot_contact<>"" && !in_array($prot_contact, $arr_result["suppliers"][$inn]["contacts"]))  $arr_result["suppliers"][$inn]["contacts"][] = $prot_contact;

                                    $arr_result["suppliers"][$inn]["sum_apps"] += $rowSQL['Cost'];
                                    $arr_result["suppliers"][$inn]["num_apps"] += 1;
                                    
                                    
                                    if($rowSQL['Admitted']==1 && $rowSQL['AppRating']==1)
                                    {   
                                        $arr_result["suppliers"][$inn]["sum_wins"] += $rowSQL['Price'];                                 
                                        $arr_result["suppliers"][$inn]["num_wins"] += 1;                                 
                                        $arr_result["suppliers"][$inn]["regions"][$region]["win"] += 1;
                                    }
    
                                    if($rowSQL['Admitted']==0 )
                                    {
                                        $arr_result["suppliers"][$inn]["sum_rejected"] += $rowSQL['Cost'];
                                        $arr_result["suppliers"][$inn]["num_rejected"] += 1;
                                    }
                                    
                                } // end while query 
                                
                            } // end if result sqlQuery
                            
                            //$arr_res = array_sort_by_column($arr_result["tenders"], 'start_date');
                            //print_r($arr_result["suppliers"]);
                    } // end foreach tender_type
                }
            } // end foreach tender_law
            
            usort($arr_result["tenders"], function($a, $b) {
                return $b['start_date'] >= $a['start_date'];
            });
                 
            usort($arr_result["suppliers"][$inn]["regions"], function($a, $b) {
                return $b['count'] >= $a['count'];
            });
            

            usort($arr_result["suppliers"][$inn]["competitors"], function($a, $b) {
                return $b['count'] >= $a['count'];
            });            
                
                
        } // end if db selected
        mssql_close($sqlConnectId);
    } // end if sqlConnectId
}

//print_r($arr_result);

// ----------------------------------------------------------------------
// передача информации в форму
$smarty->assign("arr_result", $arr_result);
$smarty->assign("dt_period", $dt_period);

$smarty->assign("inn", $inn);
$smarty->assign("kpp", $kpp);
$smarty->assign("title", $kpp);
$smarty->assign("contactperson", $contactperson);
$smarty->assign("email", $email);
$smarty->assign("phone", $phone);
$smarty->assign("address", $address);

$smarty->assign("date1", $date1);
$smarty->assign("date2", $date2);

$smarty->assign("page", $page);
$smarty->assign("num_start", $num_start);
$smarty->assign("num_finish", $num_finish);
$smarty->assign("num_total", $num_total);

function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
    $sort_col = array();
    foreach ($arr as $key=> $row) {
        $sort_col[$key] = $row[$col];
    }

    array_multisort($sort_col, $dir, $arr);
}

function resort_array($array_sort)
{
   $arr_len = sizeof($array_sort);
   foreach ($array_sort as $govruid=>$arr_data)
   {
       $next_date = $arr_data["start_date"];
   }
}
?>