<?php
// ------------------------------------------------------------------------
// возвращает точные данные о перменных с формы
// ------------------------------------------------------------------------
function getRealPOST() {
    $pairs = explode("&", file_get_contents("php://input"));
    $vars = array();
    foreach ($pairs as $pair) {
        $nv = explode("=", $pair);
        $name = urldecode($nv[0]);
        $value = urldecode($nv[1]);
        $vars[$name] = $value;
    }
    return $vars;
}

function write($str,$mode=1)
{
    switch($mode)
    {
        case 1:
            echo $str."<br>\r\n";
            break;
        case 2:
            echo $str."\r\n";
            break;
        default:
            echo $str;
            break;
    }

}

function write_log($text, $bAddTime = true, $bWritFile = false, $log_file = '/var/www/cb/add/logs/common.log')
{
    if($bAddTime) $txtToWrite = "[".date('Y-m-d H:i:s')."] ".$text;
    else $txtToWrite = $text;

    if($bWritFile)
    {
        if (!file_exists('/var/www/cb/add/logs')) {
            mkdir('/var/www/cb/add/logs', 0777, true);
        }

        file_put_contents($log_file, $txtToWrite."\n", FILE_APPEND | LOCK_EX);
    }
    else
    {
        echo $txtToWrite."<br>\r\n";
    }

}

if(!function_exists("array_column"))
{
    function array_column($array,$column_name)
    {
        return array_map(function($element) use($column_name){return $element[$column_name];}, $array);
    }
}


/*
 * Функция подбора ближайшего будущего рабочего времени относительно, заданного времени
 * mode = 0 - самое близкое рабочее время
 * mode = 1 - следующий ближайший рабочий час
 * mode = 2 - следующий ближайший рабочий день
 */
function GetNearestWorkingTime($datetime = "", $mode = 0)
{
    if($datetime=="") $datetime = date("Y-m-d H:i:s");
    $result_date = "";
    $move_date = false;

    $week_day = date("N", strtotime($datetime));
    $is_weekend = false;
    if($week_day==6 || $week_day==7) $is_weekend = true;

    switch ($mode)
    {
        default:
        case 0:
            $temp_time = date("H:i:s");
            break;
        case 1:
            $temp_time = date("H", strtotime($datetime." +1 hour")).":00:00";
            break;
        case 2:
            $temp_time = "09:00:00";
            $move_date = true;
            break;
        case 3:
            $temp_time = "14:00:00";
            break;
    }

    // если не уложились в рабочее время, то двигаем
    if($temp_time<"09:00:00")
        $temp_time = "09:00:00";
        else
            if($temp_time>="18:00:00")
            {
                $temp_time = "09:00:00";
                $move_date = true;
            }

        // если не уложились в рабочие дние, то двигаем
        if($is_weekend || ($week_day==5 && $move_date))
            $temp_date = date("Y-m-d", strtotime($datetime." next monday"));
            else
                if(!$move_date)
                    $temp_date = date("Y-m-d");
                    else
                        $temp_date = date("Y-m-d", strtotime($datetime." +1 day"));

                        $result_date = $temp_date." ".$temp_time;
                        return $result_date;

}

/*
 * Получение разряда числа
 */
function get_digit($number, $digit) {
    $up = pow(10, $digit);
    $down = pow(10, $digit - 1);

    return ($number >= $down)
             ? floor( ($number % $up) / $down )
             : 0 ;
}

/*
 * Возвращает число прописью в именительном падеже
 * $gender: female|male|middle
 */
function number_written_alt($number, $words, $gender = 'female') {



    if (!is_array($words))
        $words = explode(',', $words);

    $str = '';

    $names = array(
            1 => 'тысяча,тысячи,тысяч',
            'миллион,миллиона,миллионов',
            'миллиард,миллиарда,миллиардов'
            // сюда добавить по желанию
        );

    $F = __FUNCTION__;

    foreach (array_reverse($names, TRUE) as $i => $w) {

        $pow = pow(1000, $i);

        if ($number >= $pow) {
            $str .= $F(
                    floor($number/$pow),
                    $w,
                    ( ($i == 1) ? 'female' : 'male' )
                ) . ' ';

            $number = $number % $pow;
        }
    }


    # Сотни

        if ($number >= 100) {
            $hundreds = array(
                1 => 'сто',
                'двести',
                'триста',
                'четыреста',
                'пятьсот',
                'шестьсот',
                'семьсот',
                'восемьсот',
                'девятьсот'
            );
            $h = get_digit($number, 3);
            if (isset($hundreds[$h]))
                $str .= "$hundreds[$h] ";
        }


    # Десятки

        $d = get_digit($number, 2);

        if ($d >= 2 OR $d == 0) {
            $decs = array(
                2 => 'двадцать',
                'тридцать',
                'сорок',
                'пятьдесят',
                'шестьдесят',
                'семьдесят',
                'восемьдесят',
                'девяносто'
            );
            if (isset($decs[$d]))
                $str .= "$decs[$d] ";

            # Единицы

            $u = get_digit($number, 1);

            if ($u > 2) {
                $units = array(
                        3 => 'три',
                        'четыре',
                        'пять',
                        'шесть',
                        'семь',
                        'восемь',
                        'девять'
                    );
                $str .= "$units[$u] "
                        . (
                              ($u > 4)
                              ? $words[2]
                              : $words[1]
                          ) ;
            }

            elseif ($u == 2) {
                $tmp = array(
                        'female' => 'две',
                        'male' => 'два',
                        'middle' => 'два'
                    );
                $str .= "$tmp[$gender] $words[1]";
            }
            elseif ($u == 1) {
                $tmp = array(
                        'female' => 'одна',
                        'male' => 'один',
                        'middle' => 'одно'
                    );
                $str .= "$tmp[$gender] $words[0]";
            }
            else
                $str .= $words[2]; // ноль

        }
        else {

            $sub_d = $number % 100;

            $tmp = array(
                    10 => 'десять',
                    'одиннадцать',
                    'двенадцать',
                    'тринадцать',
                    'четырнадцать',
                    'пятнадцать',
                    'шестнадцать',
                    'семнадцать',
                    'восемьнадцать',
                    'девятнадцать'
                );
            $str .= "$tmp[$sub_d] $words[2]";
            unset($tmp);
        }



    return $str;
}

function mb_trim($string, $charlist='\\\\s', $ltrim=true, $rtrim=true)
{
    $both_ends = $ltrim && $rtrim;

    $char_class_inner = preg_replace(
        array( '/[\^\-\]\\\]/S', '/\\\{4}/S' ),
        array( '\\\\\\0', '\\' ),
        $charlist
        );

    $work_horse = '[' . $char_class_inner . ']+';
    $ltrim && $left_pattern = '^' . $work_horse;
    $rtrim && $right_pattern = $work_horse . '$';

    if($both_ends)
    {
        $pattern_middle = $left_pattern . '|' . $right_pattern;
    }
    elseif($ltrim)
    {
        $pattern_middle = $left_pattern;
    }
    else
    {
        $pattern_middle = $right_pattern;
    }

    return preg_replace("/$pattern_middle/usSD", '', $string);
}

/*
 * Функция для обновления данных статического поля типа "Список" данные из заданной таблицы.
 * Полезно когда необходимо реализовать изменение динамических статусов в табличной форме без входа в карточку записи.
 */
function UpdateStaticSelectByDynamicTableRows(
    $tableWithStaticSelectId
    ,$fieldStaticSelectId // без префикса f
    ,$tableWithDynamicDataId
    ,$fieldWithDynamicDataId // без префикса f
    ,$sqlFilter
    )
{
    $dynamicData = array();
    $query = sql_select_field($tableWithDynamicDataId, "f$fieldWithDynamicDataId", $sqlFilter);
    while ($nextDynamicDataRow = sql_fetch_row($query))
    {
        $dynamicData[] = $nextDynamicDataRow[0];
    }

    $var = array('type_value'=>implode("\r\n",$dynamicData));
    $w = var_dump($var);

    // need test!
    // sql_update(FIELDS_TABLE, array('type_value'=>implode("\r\n",$dynamicData)),"id=$fieldStaticSelectId");
}

function UpdateStaticSelectByData(
    $fieldStaticSelectId // без префикса f
    ,$data
    )
{
    if($fieldStaticSelectId>0)
    sql_update(FIELDS_TABLE, array('type_value'=>implode("\r\n",$data)),"id=$fieldStaticSelectId");
}

function AddWorkingDays($date, $count)
{
    if($count<=0) return $date;

    $i = 0;
    $newdate = $date;

    while($i<$count)
    {
        $newdate = date("Y-m-d H:i:s", strtotime($newdate." +1 day"));
        if(!isWeekend($newdate)) $i++;
    }

    return $newdate;
}

function isWeekend($date)
{
    return (date('N', strtotime($date)) >= 6);
}

function num2str_fraction($num, $currency, $cents, $caps)
{

    $arr_words1 = ["целая","целых"];
    $arr_words2 = [ 0=>["десятая","сотая","тысячная","десятитысячная"]
                    ,1=>["десятых","сотых","тысячных","десятитысячных"]];


    $str_number = strval($num);
    if(strpos($str_number, ".")>0)
    {
        $str_array = explode(".",$str_number);

        $num1 = $str_array[0];
        $num2 = $str_array[1];

        $num1_len = strlen($num1) - 1;
        $num1_final = substr($num1, -1, 1);
        $num1_final2 = substr($num1, -2, 1);
        if($num1_final == "1" && ($num1_final2 == "0" || $num1_len==0))
        {
            $add_str_num1 = $arr_words1[0];
            $str_num1 = num2str($num1, $currency, $cents, $caps, 1);
        }
        else
        {
            $add_str_num1 = $arr_words1[1];
            $str_num1 = num2str($num1, $currency, $cents, $caps, 0);
        }

        $num2_len = strlen($num2) - 1;
        $num2_final = substr($num2, -1, 1);
        $num2_final2 = substr($num2, -2, 1);
        if($num2_final == "1" && ($num2_final2 == "0" || $num2_len==0))
        {
            $add_str_num2 = $arr_words2[0][$num2_len];
            $str_num2 = num2str($num2, $currency, $cents, $caps, 1);
        }
        else
        {
            $add_str_num2 = $arr_words2[1][$num2_len];
            $str_num2 = num2str($num2, $currency, $cents, $caps);
        }



        $result = $str_num1." ".$add_str_num1." ".$str_num2." ".$add_str_num2;

    }
    else
    {
        $result = num2str($num, $currency, $cents, $caps);
    }

    return $result;
}

function cleanupPhone($dirty_phone)
{
    $clean_phone = preg_replace( '/[\-\(\)]/', '', $dirty_phone );
    $clean_phone = preg_replace( '/([0-9]) ([0-9])/', '\1\2', $clean_phone );
    $clean_phone = trim($clean_phone);
    return $clean_phone;
}

