<?php

function CreateCommonOrder(&$line, $config)
{
    if ($line['f11080'] == 1) // Общий счет
    {
        ini_set('max_execution_time', 300);

        global $base64_current_url;

        $tableInvoices = 43;
        $tableInvoicesPositions = 74;
        $tableOrders = 271;

        // создаем массив для создания счета
        $invoice = array();
        $invoice['f839'] = $line['f4441']; // ссылка на клиента
        $invoice['f2071'] = $line['f4461']; // сумма
        $invoice['f10810'] = date("Y-m-d",strtotime("+1 week"));  // прогноз оплаты

        // создаем запись счета
        $newInvoice = data_insert($tableInvoices, EVENTS_ENABLE, $invoice);

        // создаем запись в таблице Позиции счета по заказам которые имеют признак общий счет
        $resOrders = data_select(271, 'f11080=1 and status=0');
        $sum = 0.0;
        while($rowOrder = sql_fetch_assoc($resOrders))
        {
            if($rowOrder['id']<>"")
            {
                $invoicePosition = array();
                $invoicePosition['f807'] = $newInvoice;   // ссылка на счет
                $invoicePosition['f7061'] = $rowOrder['id'];    // ссылка на заказ
                $invoicePosition['f8320'] = $rowOrder['f4461']; // сумма
                $invoicePosition['f810'] = $rowOrder['f4461']; // что выставлено
                $invoicePosition['f2331']['ID'] = $rowOrder['f4451'];  // услуга
                $invoicePosition['f1440'] = ""; // описание

                data_insert($tableInvoicesPositions , EVENTS_ENABLE, $invoicePosition );

                // сбрасываем признак Общий счет и проставляем данные о выставленной сумме
                data_update($tableOrders, EVENTS_ENABLE, array('f11080'=>'','f10780'=>$rowOrder['f4461']), "id=".$rowOrder['id']);

                $sum += (float)($rowOrder['f4461']);
            }
        }

        // обновляем сумму в созданном счете
        if($newInvoice >0) data_update($tableInvoices, EVENTS_ENABLE, array('f2071'=>$sum), "id=".$newInvoice);

        $line['f10780'] = $line['f4461']; // проставляем сумму
        $line['f11080'] = ""; // сбрасываем признак общего счета

        echo "<script>location.href='".$config["site_root"]."/view_line.php?table=43&line=".$newInvoice."&back_url=".$base64_current_url."'</script>";
    }

} //end function

?>