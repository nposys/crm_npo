<?php
require_once 'add/db.php';

/**
 * Переводим в MS SQL статус заказа в "17. Заявка на доставке" все заказы, которые перевели
 * в статус производства "50. На подаче" в CRM.
 *
 * @param $idSql - ClientLotId in MS SQL
 */
function UpdateRtAppStatus($idSql)
{
    if(trim($idSql)!=""){
        global $databases;

        ini_set('mssql.charset','UTF8');
        $serverName = $databases["MSSQL"]["server"];
        $connectionOptions = [
            "Database" => $databases["MSSQL"]["db"],
            "Uid" => $databases["MSSQL"]["user"],
            "PWD" => $databases["MSSQL"]["pass"],
            'ReturnDatesAsStrings'=>true,
        ];

        $sqlLink = sqlsrv_connect($serverName, $connectionOptions);
        if ($sqlLink){
            // выбираем статус этого тендера
            $query = "SELECT [Status] FROM [ClientsLots] WHERE [ID] = $idSql";
            $resultQuery = sqlsrv_query($sqlLink, $query);
            if($resultQuery){
                $row = sqlsrv_fetch_array($resultQuery);
                if((int)($row["Status"]) == 9){
                    // ID  StatusName 19  17. Заявка на доставке
                    $queryUpdate = "UPDATE [ClientsLots] SET [Status] = 19 WHERE [ID] = ".$idSql;
                    sqlsrv_query($sqlLink, $queryUpdate);
                }
            } // end if result query

            //close the connection
            sqlsrv_close($sqlLink);
        } // end if SQL LINK

        return 1;
    } else {
        return -1;
    }
}

/**
 *  Переводим в CRM RT статус заказа в "25. Торги" все заказы, которые перевели
 * в статус производства "50. На подаче" в CRM.
 * @param $idCRM - ID записи в таблице Тендеры в CRM RT
 */
function UpdateRtAppStatusCRM($idCRM)
{
    if((int)$idCRM>0){
        global $databases;

        /* подключаемся к базе CRM RT */
        $mysqli = mysqli_init();
        if (!$mysqli->real_connect(
            $databases['CRM_RT']['server'],
            $databases['CRM_RT']['user'],
            $databases['CRM_RT']['pass'],
            $databases['CRM_RT']['db'])) {
            die('Ошибка подключения (' . mysqli_connect_errno() . ') '
                . mysqli_connect_error());
        }

        $mysqli->query("SET NAMES 'utf8'");

        $queryUpdate = "UPDATE cb_data40 SET f950 = '25. Торги' WHERE id = ".$idCRM;
        $mysqli->query($queryUpdate);

        /* закрываем соединение */
        $mysqli->close();

        return 1;
    } else {
        return -1;
    }

}
