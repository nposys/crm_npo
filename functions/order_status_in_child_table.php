<?php

require_once ('add/tables.php');
require_once ('add/functions/service_functions.php');
function SetChildTableOrderStatus($line)
{
    global $cb_tables;
    $orderId = $line['id'];
    $serviceType = $line['f4451']['id'];
    $orderStatus = $line['f6551'];

    if($orderId>0)
    {
        switch($serviceType)
        {
            case 4:
                // можно прописать массив с таблицами и полями и автоматически подставлять в data_update
                $productionTable = $cb_tables['tableOrdersSearch'];
                $orderStatusField = 'f9260';
                $orderIdField = 'f5411';

                data_update($productionTable, EVENTS_ENABLE, array("`$orderStatusField`"=>$orderStatus), "`$orderIdField`=$orderId");
                break;
            default:
                break;
        }
    }

} // end function SetChildTableOrderStatus($line)


?>