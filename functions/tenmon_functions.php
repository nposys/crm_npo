<?php

/* ==============================================================
 Получение данных о счетах
 ============================================================== */
function GetTenmonInvoices ($dateFrom)
{
    $out = [];

    $request = [
        'service' => 'billing',
        'type' => 'json',
        'method' => 'getPaymentInvoice',
        'params' => json_encode([
            'dateFrom' => (int)strtotime($dateFrom),
        ])
    ];

    if ($curl = curl_init()) {
        curl_setopt($curl, CURLOPT_URL, 'https://tenmon.ru/api/v1/');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
        $out = json_decode(curl_exec($curl), TRUE);
        curl_close($curl);
    }

    return $out;
}



/* ==============================================================
 Простановка данных об оплате счетов
 ============================================================== */
// UNIX TIMESTAMP
function ChangePaymentInvoice ($invoices)
{
    $out = [];

    if (is_array($invoices) > 0) {
        $request = [
            'service' => 'billing',
            'type' => 'json',
            'method' => 'changePaymentInvoice',
            'params' => json_encode([
                'invoices' => $invoices
            ])
        ];

        if ($curl = curl_init()) {
            curl_setopt($curl, CURLOPT_URL, 'https://tenmon.ru/api/v1/');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
            $out = json_decode(curl_exec($curl), TRUE);
            curl_close($curl);
        }

    } else {
        write_log("ChangePaymentInvoice error in input params");
    }

    return $out;
}


/* ==============================================================
 Простановка данных о платных поисках
 ============================================================== */
// UNIX TIMESTAMP
function SetPaymentInfo($tenmonUid, $dateStart, $dateEnd, $summary)
{
    $out = [];

    $tenmonUid = (int)$tenmonUid;
    $dateTariffStart = strtotime($dateStart);
    $dateTariffEnd = strtotime($dateEnd);
    $sum = (float)$summary;

    if ($tenmonUid > 0) {
        $request = [
            'service' => 'billing',
            'type' => 'json',
            'method' => 'setPaymentInfo',
            'params' => json_encode([
                'params' => [
                    'userId' => $tenmonUid,
                    'dateStart' => $dateTariffStart,
                    'dateEnd' => $dateTariffEnd,
                    'summary' => $sum,
                ]
            ])
        ];

        if ($curl = curl_init()) {
            curl_setopt($curl, CURLOPT_URL, 'https://tenmon.ru/api/v1/');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
            $out = json_decode(curl_exec($curl), TRUE);
            curl_close($curl);
        }

    } else {
        write_log("SetPaymentInfo error in input params");
    }

    return $out;
}

/* ==============================================================
 Простановка данных о менеджера клиента
 ============================================================== */
function SetTenmonManagers($tenmonUid, $managerSaleTmId, $managerClientTmId)
{
    $out = [];
    $tenmonUid = (int)$tenmonUid;
    $managerSaleTmId = (int)$managerSaleTmId;
    $managerClientTmId = (int)$managerClientTmId;

    if ($tenmonUid > 0) {
        $request = [
            'service' => 'account',
            'type' => 'json',
            'method' => 'setManagerTM',
            'params' => json_encode([
                'userId' => $tenmonUid,
                'saleManagerId' => (int)$managerSaleTmId,
                'accountManagerId' => (int)$managerClientTmId,
            ])
        ];

        if ($curl = curl_init()) {
            curl_setopt($curl, CURLOPT_URL, 'https://tenmon.ru/api/v1/');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
            $out = json_decode(curl_exec($curl), TRUE);
            curl_close($curl);
        }

    } else {
        write_log("SetClientManages error in input params tenmonUid:[$tenmonUid] managerSaleTmId:[$managerSaleTmId] managerClientTmId:[$managerClientTmId]");
    }

    return $out;
}

/* ==============================================================
 Простановка признака текущего клиента
 ============================================================== */
function SetIsCurrentClient($tenmonUid, $bIsCurrentClient = true, $dateEnd = '')
{
    $out = [];
    $tenmonUid = (int)$tenmonUid;

    if ($tenmonUid > 0) {

        if ($dateEnd === ''){
            $params = [
                'userId' => $tenmonUid,
                'isCurrentClient' => $bIsCurrentClient,
            ];
        } else {
            $params = [
                'userId' => $tenmonUid,
                'isCurrentClient' => $bIsCurrentClient,
                'dateEnd' => strtotime($dateEnd)
            ];
        }

        $request = [
            'service' => 'billing',
            'type' => 'json',
            'method' => 'setIsCurrentClient',
            'params' => json_encode([
                'params' => $params
            ])
        ];

        if ($curl = curl_init()) {
            curl_setopt($curl, CURLOPT_URL, 'https://tenmon.ru/api/v1/');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
            $out = json_decode(curl_exec($curl), TRUE);
            curl_close($curl);
        }

    } else {
        write_log("SetIsCurrentClient error in input params");
    }

    return $out;
}

/* ==============================================================
Объединение компаний в группы компаний в ТМ
 ============================================================== */
function JoinGroupOfCompanies($tenmonUids)
{
    $out = [];

    if (is_array($tenmonUids) && count($tenmonUids) > 0) {
        $request = [
            'service' => 'account',
            'type' => 'json',
            'method' => 'linkAccounts',
            'params' => json_encode([
                'userIds' => $tenmonUids
            ])
        ];

        if ($curl = curl_init()) {
            curl_setopt($curl, CURLOPT_URL, 'https://tenmon.ru/api/v1/');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
            $out = json_decode(curl_exec($curl), TRUE);
            curl_close($curl);
        }

    } else {
        write_log("JoinGroupOfCompanies error in input params");
    }

    return $out;
}

/* ==============================================================
 Получение сведений об организации,
 в том числе об аккредитации
 ============================================================== */
function GetCompanyInfo($tenmon_uid)
{
    $out = [];

    if ($tenmon_uid > 0) {
        $request = [
            'service' => 'info',
            'type' => 'json',
            'method' => 'getAccreditation',
            'params' => json_encode([
                'userId' => $tenmon_uid
                // ,'auctionSiteId' => 1
            ])
        ];

        if ($curl = curl_init()) {
            curl_setopt($curl, CURLOPT_URL, 'https://tenmon.ru/api/v1/');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
            $out = json_decode(curl_exec($curl), TRUE);
            curl_close($curl);
        }
    } else {
        write_log("GetCompanyInfo error in input params tenmon_uid:$tenmon_uid");
    }

    return $out;
}

/* ==============================================================
 обновление тарифа тенмон

    $tm_user_id - id пользователя
    $tm_tariff_id - тариф тенмон

    $products['search'] = 0;
    $products['autoSearch'] = 0;
    $products['expertSearch'] = 0;
    $products['userCount'] = 0;
    $products['notification'] = 0;
    $products['integration'] = 0;

    $end_date - дата истечения тарифа

 ============================================================== */

function SetTenmonServices($tm_user_id, $tm_tariff_id, $products, $end_date)
{
    $result = [
        'status' => 0,
        'message' => 'Success',
    ];

    $tenmonClientId = (int)($tm_user_id);
    $tenmonTariffId = (int)($tm_tariff_id);
    $tenmonDate = strtotime($end_date);

    // проверяем что передан id клиента
    if ($tenmonClientId > 0 && $tenmonTariffId > 0 && is_array($products) && $tenmonDate > 0) {
        $request = [
            'service' => 'billing',
            'type' => 'json',
            'method' => 'setTariff',
            'params' => json_encode([
                'userId' => $tenmonClientId,
                'tariffId' => $tenmonTariffId,
                'products' => $products,
                'endDate' => $tenmonDate
            ])
        ];

        if ($curl = curl_init()) {
            curl_setopt($curl, CURLOPT_URL, 'https://tenmon.ru/api/v1/');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
            $result = curl_exec($curl);

            if(!is_string($result)){
                $result = [
                    'status' => -3,
                    'message' => "Error in curl_exec",
                ];
            }

            curl_close($curl);
        } else {
            // не удлалось передать curl
            $result = [
                'status' => -2,
                'message' => 'Error init curl',
            ];
        }
    } else {
        $result = [
            'status' => -1,
            'message' => "Error in input params tenmonClientId:$tenmonClientId tenmonTariffId:$tenmonTariffId tenmonDate:$tenmonDate ($end_date) is_array:" . is_array($products),
        ];
    }

    return $result;

}

function GetTenmonTariffByName($name)
{
    $tariff = [
        "1.Базовый" => 1,
        "2.Стандарт" => 2,
        "3.Эксперт" => 3,
        "4.Конструктор" => 4,
        "5.Базовый" => 5,
        "6.Специалист" => 6,
        "7.Профессионал" => 7,
        "8.Эксперт" => 8,
        "9.Эконом" => 9,
        "10.Я - Специалист" => 10,
        "11.Ваш Эксперт" => 11,
        "12.Максимум" => 12];
    if ($tariff[$name] > 0) return $tariff[$name];
    else return 0;
}

function GetNameByTenmonTariff($tm_tariff_id)
{
    $tariff = ["1.Базовый" => 1, "2.Стандарт" => 2, "3.Эксперт" => 3, "4.Конструктор" => 4, "5.Базовый" => 5, "6.Специалист" => 6, "7.Профессионал" => 7, "8.Эксперт" => 8, "9.Эконом" => 9, "10.Я - Специалист" => 10, "11.Ваш Эксперт" => 11, "12.Максимум" => 12];
    foreach ($tariff as $name => $val) if ($val == intval($tm_tariff_id)) return $name;
    return "";
}

function ConvertYesNoToBool($name)
{
    if ($name == "Да") return true;
    else return false;
}

function ConvertBoolToYesNo($bool)
{
    if ($bool) return "Да";
    else return "Нет";
}

// ==============================================================
// обновление тарифа тенмон
// для раннней версии работы. после 2016-11-04 использовать не следует
// ==============================================================

function SetTenmonTariff($tm_id, $tariff_id = 1, $enable = false, $bUpdateMSSQL = false, $sqlsrv_link = "")
{
    $result = 0;

    $tenmonClientId = (int)($tm_id);
    $tariffId = (int)($tariff_id);

    if ($tenmonClientId > 0 && in_array($tariffId, array(0, 1, 2, 3))) {

        if ($enable) {
            $search_enable = 1;
            $method = 'templateSearchEnable';
        } else {
            $search_enable = 0;
            $method = 'templateSearchDisable';
        }

        $request = array(
            'service' => 'billing',
            'type' => 'json',
            'method' => $method,
            'params' => json_encode(array(
                'id' => $tenmonClientId,
                'tariff' => $tariffId
            ))
        );

        if ($curl = curl_init()) {
            curl_setopt($curl, CURLOPT_URL, 'https://tenmon.ru/api/v1/');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $request); // @array $request
            $out = curl_exec($curl);
            curl_close($curl);

            if ($bUpdateMSSQL && $sqlsrv_link <> "") {
                $queryUpdate = "UPDATE [tm_ClientsSync] SET [SearchEnable]=$search_enable WHERE [tmUserID]=$tenmonClientId";
                $resultUpdateQuery = sqlsrv_query($queryUpdate, $sqlsrv_link);
            }
        } else {
            // не удлалось передать curl
            $result = -2;
        }

    } else {
        // ошибка параметров
        $result = -1;
    }

    return $result;
}

// ============================================================
// Создаем заказ на пробный поиск тендеров
// ============================================================

function CreateSearchOrder($data, $cb_tables, $options = [])
{
    // проверяем входные параметры
    if (!$data['client_id']
        || !$data['date_current']
        || !$data['date_start']
        || !$data['date_end']
        || !$cb_tables['tableOrdersSearch']
        || !$cb_tables['tableOrders']) {
        return -1;
    }

    // проверяем не существует ли уже заказа на поиск для данного клиента из группы компаний
    $check_order_options = [];
    if ($options['paid_search'] !== true) {
        $check_order_options = ['only_paid' => false];
        $res_order_search = GetSearchOrderForCompanyGroup($data['client_id'], $cb_tables, $check_order_options);
        if ($res_order_search['id']) {
            write_log('already have order id:'.$res_order_search['id']);
            return -2;
        }
    }

    if ($options['paid_search'] === true) {
        $status_order = '20. Предоплата';
        $status_production = '30. Платный поиск';
        $order_description = 'Поиск по счету из Тенмон';
        $order_sum = $data['sum'];
        !isset($data['tariff']) ? $data['tariff'] = 4 : '';

    } else {
        $status_order = '10. Исполнение';
        $status_production = '20. Пробный поиск';
        $order_description = 'Пробный поиск (шаблоны) из Тенмон';
        $order_sum = 0.0;

        $data['tm_options']['SearchEnable'] = 1;
        $data['tm_options']['AutoSearchEnable'] = 1;
        $data['tm_options']['ExpertSearchEnable'] = 0;
        $data['tm_options']['AnalyticEnable'] = 1;
        $data['tm_options']['SMSEnable'] = 1;
        $data['tm_options']['IntegrationEnable'] = 1;
        $data['tm_options']['HoldingEnable'] = 0;
        $data['tm_options']['InvitesCount'] = 3;

        $data['tm_options']['exportLotInfo'] = 1;
        $data['tm_options']['exportAnalyticInfo'] = 1;
        $data['tm_options']['csvLimit'] = 1000;
        $data['tm_options']['analyticIndustryEnable'] = 1;
        $data['tm_options']['forecastEnable'] = 1;
        $data['tm_options']['lotStatusNotificationEnable'] = 1;
        $data['tm_options']['riskAnalyseEnable'] = 1;
    }

    if (isset($options['description']) && $options['description'] !== '') {
        $order_description = $options['description'];
    }

    // получаем позицию прайса по тарифу Тенмона
    if ($options['new_reg'] === true) {
        $price_position = 56; // Пробный поиск (шаблоны)
    } else {
        $price_position = GetPriceIdByTenmonTariff($data['tariff'], !$options['paid_search']);
    }

    // создаем заказ на поиск по шаблонам
    $order = array();
    $order['status'] = '0';
    $order['f4451'] = 4;                  // тип заказа  =  "Поиск"
    $order['f13071'] = $price_position;      // позиция прайса
    $order['f6551'] = $status_order;      // статус заказа
    $order['f10240'] = $status_production;  // статус производства
    $order['f4471'] = 'Да';                  // согласовано с клиентом
    $order['f4431'] = $order_description;         // название заказа
    $order['f6621'] = ''; // PathFolder

    if ($data['manager_sales_user']) $order['f4411'] = $data['manager_sales_user']; // manager sales
    if ($data['manager_client_user']) $order['f6581'] = $data['manager_client_user']; // manager client
    if ($data['manager_production_user']) $order['f6581'] = $data['manager_production_user']; // manager production
    if ($data['manager_sales']) $order['f9460'] = $data['manager_sales']; // manager sales
    if ($data['manager_client']) $order['f9470'] = $data['manager_client']; // manager client
    if ($data['manager_production']) $order['f4421'] = $data['manager_production']; // manager production

    $order['f16410'] = 1; // признак создания из Тенмона

    $order['f7301'] = "";   // примечание
    $order['f6591'] = $data['date_current'];     // дата заказа
    $order['f12701'] = $data['date_end'];       // deadline

    $order['f4461'] = $order_sum;            // стоимость
    $order['f4441'] = $data['client_id'];     // клиент

    $new_order_id = data_insert($cb_tables['tableOrders'], EVENTS_ENABLE, $order);

    // создаем запись в подтаблице про поиск
    if ($new_order_id > 0) {
        $order_search = array();
        $order_search['f5411'] = $new_order_id; // order
        $order_search['f10190'] = $data['client_id']; // client
        $order_search['f10230'] = $order_sum; // сумма

        $order_search['f6941'] = $data['date_start']; // дата начала
        $order_search['f6951'] = $data['date_end']; // дата окончания
        $order_search['f5421'] = $order_description; // примечание

        $order_search['f9260'] = $status_order; // статус заказа
        $order_search['f6931'] = $status_production; // статус производства

        $order_search['f16870'] = GetTariffNameById((int)$data['tariff']); // tenmon tariff
        $order_search['f16880'] = ConvertBoolToYesNo((int)$data['tm_options']['SearchEnable']); // ручной поиск
        $order_search['f16890'] = ConvertBoolToYesNo((int)$data['tm_options']['AutoSearchEnable']); // автоматический шаблонный поиск
        $order_search['f16900'] = ConvertBoolToYesNo((int)$data['tm_options']['ExpertSearchEnable']); // экспертный поиск
        $order_search['f16840'] = (int)$data['tm_options']['InvitesCount']; // кол-во пользователей
        $order_search['f16850'] = ConvertBoolToYesNo((int)$data['tm_options']['IntegrationEnable']); // интеграция с ЭТП
        $order_search['f16860'] = ConvertBoolToYesNo((int)$data['tm_options']['SMSEnable']); // уведомления
        $order_search['f18691'] = ConvertBoolToYesNo((int)$data['tm_options']['AnalyticEnable']); // аналитика
        $order_search['f17190'] = ConvertBoolToYesNo((int)$data['tm_options']['HoldingEnable']); // группы компаний

        $order_search['f19621'] = ConvertBoolToYesNo((int)$data['tm_options']['exportLotInfo']); // Выгрузка закупок в Excel
        $order_search['f19631'] = ConvertBoolToYesNo((int)$data['tm_options']['exportAnalyticInfo']); // Выгрузка результатов аналитики в Excel
        $order_search['f19641'] = (int)$data['tm_options']['csvLimit']; // Лимит строк для выгрузки результатов аналитики в Excel
        $order_search['f19651'] = ConvertBoolToYesNo((int)$data['tm_options']['analyticIndustryEnable']); //Аналитика по произвольным запросам
        $order_search['f19661'] = ConvertBoolToYesNo((int)$data['tm_options']['forecastEnable']); //  Экспертный прогноз снижения на торгах
        $order_search['f19671'] = ConvertBoolToYesNo((int)$data['tm_options']['lotStatusNotificationEnable']); // Автоматические уведомления о сменах статуса выбранных тендеров
        $order_search['f19681'] = ConvertBoolToYesNo((int)$data['tm_options']['riskAnalyseEnable']); // Экспертный анализ рисков по заказчикам

        $oldRow =data_select_array( $cb_tables['tableOrdersSearch'], 'status = 0 AND `f5411`=',$new_order_id);
        if(isset($oldRow['id']) & $oldRow['id']>0 ){
            write_log(" ++++ update orderSearch  $new_order_id  id ".$oldRow['id']);
            data_update($cb_tables['tableOrdersSearch'], EVENTS_ENABLE, $order_search, "id = ",$oldRow['id']);
        }else{
            data_insert($cb_tables['tableOrdersSearch'], $order_search);
            write_log(" ++++ insert orderSearch  $new_order_id ");
        }

    }

    return $new_order_id;
}

// ============================================================
// Создаем событие на менеджера
// ============================================================
function CreateManagerEvent($data, $cb_tables)
{
    $event_title = "Прозвонить клиента после регистрации в ТМ";
    $event_time = GetNearestWorkingTime("", 0);

    $event = [];
    $event['status'] = 0;
    $event['f724'] = $event_time;
    $event['f723'] = $data['client_id'];
    $event['f727'] = $data['manager_client_user'];
    $event['f773'] = 'Звонок';
    $event['f725'] = $event_title;
    $event['f1053'] = 'Нет'; // выполнено

    data_insert($cb_tables['tableEvents'], EVENTS_ENABLE, $event);
}

// ============================================================
// Создаем заказ на экспертный поиск тендеров
// ============================================================

function CreateExpertSearchOrder($tm_order_data)
{
	require_once 'add/tables.php';
	global $cb_tables;
	// проверяем входные параметры
	if ( !$tm_order_data['OrderData'] && $tm_order_data['OrderData']!='') {
		return false;
	}

	$order_data = json_decode($tm_order_data['OrderData']);
	$crm_order_id=(int)$order_data->crmorder;
	$tm_order_id = $tm_order_data['OrderId'];
	write_log("Обработка заказа TM ID:$tm_order_id" );

	$default_managers = data_select_array(1271, "status=0 and f21231='sync_tenmon_clients'");
	$default_manager_sales = data_select_array(46, "status=0 and id=".$default_managers['f21211']);
	$default_manager_clients = data_select_array(46, "status=0 and id=".$default_managers['f21221']);
	$default_manager_production = data_select_array(46, "status=0 and id=".$default_managers['f21241']);
	$manager_sales = $default_manager_sales['id'];
	$manager_sales_user = $default_manager_sales['f1400'];
	$manager_client = $default_manager_clients['id'];
	$manager_client_user = $default_manager_clients['f1400'];
	$manager_production = $default_manager_production['id'];
	$manager_production_user = $default_manager_production['f1400'];

	$client_id = 0;
	if($crm_order_id==0) {
		$tm_user_id = (int)$tm_order_data['tenmonId'];
		$contact_email = $tm_order_data['ContactEmail'];

		if ($tm_user_id > 0) {
			$rowClient = data_select_array($cb_tables['tableClients'], "status=0 and f16230='$tm_user_id'");
			$client_id = $rowClient['id'];
		} else {
			$rowClient = data_select_array($cb_tables['tableClients'], "status=0 and f435='$contact_email'");

			if (!$rowClient['id']) {
				// try to create client
				$contact_phone = $tm_order_data['ContactPhone'];
				$contact_name = $tm_order_data['ContactPerson'];
				$client = [];
				$client['f3721'] =$contact_name; //  ФИО
				$client['f435'] =$contact_name; //  Название
				$client['f441'] =$contact_phone; //  Телефон
				$client['f442'] =$contact_email; //  E-mail
				$client['f17020'] =$contact_email; //  E-mail для рассылок
				$client['f772'] ='Потенциальный'; //Тип
				$client['f552'] = 'Интерес'; //Статус лида
				$client['f4921'] = 'В работе'; //Статус клиента
				$client['f438'] = $manager_sales; // Менеджер продаж
				$client['f7811'] = $manager_sales_user; // Менеджер продаж
				$client['f8720'] = $manager_client; // Менеджер клиента
				$client['f9000'] = $manager_client_user; // Менеджер клиента
				$client['f18761'] = 4; //Источник 4 = Тенмон

				$new_client_id = data_insert($cb_tables['tableClients'], $client);
				if ($new_client_id) {
					$rowClient = data_select_array($cb_tables['tableClients'], "id=$new_client_id");
				}
			}
			$client_id = $rowClient['id'];
		}
	}

	$status_order = '20. Предоплата';
	$status_production = '10. Оценка стоимости';
	$order_description = 'Экспертный поиск по заказу';
	$date_start = date('Y-m-d 00:00:00',strtotime($tm_order_data['OrderCreateDate'].' + 1 day'));
	$date_end = date('Y-m-d 00:00:00',strtotime($date_start.' + '.$order_data->period.' month'));

	// создаем заказ на экспертный поиск
	$order = array();
	$order['status'] = '0';
	$order['f6591'] = $tm_order_data['OrderCreateDate']; //Дата
	$order['f4441'] = $client_id; //Клиент
	$order['f4451'] = 4; //Услуга
	$order['f13071'] = 1; //Позиция прайс-листа
	$order['f4431'] = $order_description; //Описание
	$order['f6551'] = $status_order; //Статус заказа
	$order['f10240'] = $status_production; //Статус производства
	$order['f12701'] = $date_end; //Дедлайн
	$order['f4411'] = $manager_sales_user;//Менеджер продаж
	$order['f9460'] = $manager_sales; //Менеджер продаж НПО
	$order['f6581'] = $manager_client_user; //Менеджер клиента
	$order['f9470'] = $manager_client; //Менеджер клиента НПО
	$order['f9010'] = $manager_production_user;  //Менеджеры производство
	$order['f4421'] = $manager_production;//Менеджеры производство НПО
	$order['f4461'] = $order_data->order_cost; //Сумма
	$order['f16220'] = (int)$tm_order_id; //idOrderTenMon

	if((int)$crm_order_id>0){
		$orderRow =data_select_array( $cb_tables['tableOrders'], "id = $crm_order_id");
	}else {
		$orderRow = data_select_array($cb_tables['tableOrders'], "status = 0 AND f16220 = $tm_order_id ");
		if((int)$orderRow['id']==0) {
			$orderRow = data_select_array($cb_tables['tableOrders'], "status = 0 AND f4441= " . $client_id . " AND
			 `f4451`= 4 AND f13071 = 1 AND f6551 = '$status_order' AND f10240 = '$status_production'");
		}
	}
	$new_order_id = (int)$orderRow['id'];
	if((int)$crm_order_id==0 && $status_order!=$orderRow['f6551']){
		write_log("Статус заказа ".$orderRow['f6551'].". Заказ не может быть измененн." );
		return true;
	}
	if($new_order_id>0 ){
		write_log("Существующий заказ ID:$new_order_id. " );
		$order_date = $orderRow['f6591'];
		$order_tm_id = $orderRow['f16220'];
		$order_cost = $orderRow['f4461'];
		if( $tm_order_data['OrderCreateDate']!=$order_date || (int)$order_tm_id==0) {
			if ((int)$crm_order_id > 0) {
				if($order_cost==0){
					$order['f4441'] =$orderRow['f4441'];
					data_update($cb_tables['tableOrders'], EVENTS_ENABLE, $order, "id = ", $new_order_id);
				}else{
					data_update($cb_tables['tableOrders'], EVENTS_ENABLE, ['f16220' => $order['f16220']], "id = ", $new_order_id);
				}
			} else if ((int)$crm_order_id == 0) {
				data_update($cb_tables['tableOrders'], EVENTS_ENABLE, $order, "id = ", $new_order_id);
			}
		}
	}else{
		$new_order_id = data_insert($cb_tables['tableOrders'], EVENTS_ENABLE, $order);
		write_log("Создан новый заказ ID:$new_order_id. " );
	}

	// создаем запись в подтаблице про поиск
	if ($new_order_id > 0) {
			$order_search = array();
			$order_search['f5411'] = $new_order_id; // order
			if ($client_id > 0) {
				$order_search['f10190'] = $client_id; // client
			}
			$order_search['f10230'] = $order_data->order_cost; // сумма

			$order_search['f6941'] = $date_start; // дата начала
			$order_search['f6951'] = $date_end; // дата окончания
			$order_search['f5421'] = $order_description; // примечание
			$order_search['f9260'] = $status_order; // статус заказа
			$order_search['f6931'] = $status_production; // статус производства
			$order_search['f16870'] = GetTariffNameById(4); // tenmon tariff
			$order_search['f16880'] = ConvertBoolToYesNo(0); // ручной поиск
			$order_search['f16890'] = ConvertBoolToYesNo(0); // автоматический шаблонный поиск
			$order_search['f16900'] = ConvertBoolToYesNo(1); // экспертный поиск
			$order_search['f16840'] = 3; // кол-во пользователей
			$order_search['f16850'] = ConvertBoolToYesNo(0); // интеграция с ЭТП
			$order_search['f16860'] = ConvertBoolToYesNo(0); // уведомления
			$order_search['f18691'] = ConvertBoolToYesNo(0); // аналитика
			$order_search['f17190'] = ConvertBoolToYesNo(0); // группы компаний
			$order_search['f19621'] = ConvertBoolToYesNo(1); // Выгрузка закупок в Excel
			$order_search['f19631'] = ConvertBoolToYesNo(0); // Выгрузка результатов аналитики в Excel
			$order_search['f19641'] = 1000; // Лимит строк для выгрузки результатов аналитики в Excel
			$order_search['f19651'] = ConvertBoolToYesNo(0); //Аналитика по произвольным запросам
			$order_search['f19661'] = ConvertBoolToYesNo(0); //  Экспертный прогноз снижения на торгах
			$order_search['f19671'] = ConvertBoolToYesNo(0); // Автоматические уведомления о сменах статуса выбранных тендеров
			$order_search['f19681'] = ConvertBoolToYesNo(0); // Экспертный анализ рисков по заказчикам

			$oldRow = data_select_array($cb_tables['tableOrdersSearch'], 'status = 0 AND `f5411`=', $new_order_id);
			if (isset($oldRow['id']) & $oldRow['id'] > 0) {
				if((int)$crm_order_id==0 || ((int)$crm_order_id>0 && $oldRow['f6951']=='0000-00-00 00:00:00' )){
					data_update($cb_tables['tableOrdersSearch'], EVENTS_ENABLE, $order_search, "id = ", $oldRow['id']);
				}
			} else {
				data_insert($cb_tables['tableOrdersSearch'], EVENTS_ENABLE, $order_search);
			}
		}
		$allRow = data_select_array($cb_tables['tableOrdersExpertSearch'], 'ALL_ROWS', 'status = 0 AND `f22131`=', $new_order_id);
		if((int)$crm_order_id==0 || count($allRow)!=count($order_data->filters)) {
			data_update($cb_tables['tableOrdersExpertSearch'], EVENTS_ENABLE, ["status" => 2], "f22131 = ", $new_order_id);
			foreach ($order_data->filters as $filter){
				$order_expert_search['f22041'] ='';
				if(strpos($filter->laws,'1')!==false){
					$order_expert_search['f22041'] .="44\r\n";
				}
				if(strpos($filter->laws,'2')!==false){
					$order_expert_search['f22041'] .="223\r\n";
				}
				if(strpos($filter->laws,'3')!==false){
					$order_expert_search['f22041'] .="Прочие\r\n";
				}
				if(strpos($filter->laws,'4')!==false){
					$order_expert_search['f22041'] .="ЗМО\r\n";

				}
				$order_expert_search['f22051'] = $filter->sum_from;//Сумма начала  поиска
				$order_expert_search['f22061'] = $filter->sum_to;//Сумма конца поиска
				$order_expert_search['f22071'] = implode(";\r\n ",$filter->categories_labels);//Категории поиска
				$order_expert_search['f22081'] = implode(";\r\n ",$filter->regions_labels);//Регионы поиска
				$order_expert_search['f22091'] ='Нет';//Есть дополнительные критерии поиска
				if((int)$filter->additional_filter==1){
					$order_expert_search['f22091'] ='Да';
				}else if((int)$filter->additional_filter==2){
					$order_expert_search['f22091'] ='Не знаю';
				}
				$order_expert_search['f22101'] = $filter->additional_filter_text;//Дополнительные критерии
				$order_expert_search['f22111'] = $filter->filter_name;//Название фильтра
				$order_expert_search['f22121'] = $filter->filter_sum;//Стоимость фильтра
				$order_expert_search['f22131'] = $new_order_id;//Заказ
				data_insert($cb_tables['tableOrdersExpertSearch'], EVENTS_ENABLE,$order_expert_search);
			}

	}
	write_log("Обработан заказ ID:$new_order_id. " );
	return true;
}



// ===============================================================
// получаем первый tenmon_uid из группы компаний
// ===============================================================

function GetTenmonUIDFromGroupOfComapnies($client_id, $cb_tables)
{
    if ($client_id > 0) {
        $row_client = data_select_array($cb_tables['tableClients'], "id=$client_id");
        $tenmon_uid = $row_client['f16230'];

        if ($tenmon_uid > 0) {
            // если id есть в карточке клиента, то сразу возвращаем
            return $tenmon_uid;
        } else {
            // иначе пробуем найти у кого-либо в группе компаний (вернем только первый id), если у группы компаний несколько регистраций, может работать не верно

            $company_group_id = $row_client['f6301'];
            if ($company_group_id > 0) {
                // получаем все client_id для группы компаний
                $res_companies = data_select($cb_tables['tableClients'], "status=0 and f6301=$company_group_id");
                while ($row_company_from_group = sql_fetch_assoc($res_companies)) {
                    $tenmon_uid = $row_company_from_group['f16230'];

                    // если id есть в карточке одной из компаний в группе, то возвращаем его
                    if ($tenmon_uid > 0) return $tenmon_uid;
                }
            }
        }
    } else {
        return -1;
    }
}

// ===============================================================
// получаем все tenmon_uid из группы компаний
// ===============================================================

function GetTenmonUIDsFromGroupOfComapnies($client_id, $cb_tables, $get_all_comapanies = false)
{
    $result = array();
    $i = 0;

    if ($client_id > 0) {
        $row_client = data_select_array($cb_tables['tableClients'], "id=$client_id");
        $company_group_id = $row_client['f6301'];

        if ($company_group_id > 0) {
            // получаем все client_id для группы компаний
            $res_companies = data_select($cb_tables['tableClients'], "status=0 and f6301=$company_group_id");
            while ($row_company_from_group = sql_fetch_assoc($res_companies)) {
                $tenmon_uid = $row_company_from_group['f16230'];

                // если id есть в карточке одной из компаний в группе, то возвращаем его
                if ($tenmon_uid > 0 || $get_all_comapanies === true) {
                    $result[$i]['client_id'] = $row_company_from_group['id'];
                    $result[$i]['client_name'] = $row_company_from_group['f435'];
                    $result[$i]['client_inn'] = $row_company_from_group['f1056'];
                    $result[$i]['tenmon_uid'] = $tenmon_uid;
                    $i++;
                }
            }
        }
    }
    return $result;
}

// ===============================================================
// получаем старшую действующую запись заказа для группы компаний
// ===============================================================

function GetSearchOrderForCompanyGroup($client_id, $cb_tables, $options = [])
{
    if (!$client_id || !$cb_tables['tableClients'] || !$cb_tables['tableOrders']) {
        return -1;
    }

    $row_client = data_select_array($cb_tables['tableClients'], "id=$client_id");

    // получаем группу компаний для данного клиента
    $str_company_group = "";
    $company_group_id = $row_client['f6301'];
    if ($company_group_id > 0) {
        // получаем все client_id для группы компаний
        $res_companies = data_select($cb_tables['tableClients'], "status=0 and f6301=$company_group_id");
        $num = 1;
        while ($row_company_from_group = sql_fetch_assoc($res_companies)) {
            if ($num > 1) {
                $str_company_group .= "," . $row_company_from_group['id'];
            } else {
                $str_company_group .= $row_company_from_group['id'];
            }

            $num++;
        }
    } else {
        $str_company_group = "$client_id";
    }

    $add_filter = '';
    if ($options['only_paid'] === true) {
        $add_filter = ' and f13071 not in (56, 63, 65) ';
    }

    // получаем старшую запись заказа на поиск
    return data_select_array($cb_tables['tableOrders'], "status=0 and f4451=4 and f4441 in ($str_company_group) and f6551='10. Исполнение' $add_filter ORDER BY f13071 ASC");
}

// ===============================================================
// получаем тариф для позиции прайс-листа
// ===============================================================
function GetTenmonTariffByPriceID($price_id)
{
    switch ($price_id) {
        case 1: // "Платный поиск (экспертный)"
        case 2: // "Пробный поиск (экспертный)"
            $tenmon_tariff = 11;
            break;
        case 55: // "Платный поиск (шаблоны)"
        case 56: // "Пробный поиск (шаблоны)"
            $tenmon_tariff = 10;
            break;
        case 62: // "Платный поиск (ручной)"
        case 63: // "Пробный поиск (ручной)"
            $tenmon_tariff = 9;
            break;
        case 64: // "Доступ тенмон (платный)"
        case 65: // "Доступ тенмон (пробный)"
        default:
            $tenmon_tariff = 12;
            break;
    }

    return $tenmon_tariff;
}

// ===============================================================
// получаем тариф для позиции прайс-листа
// ===============================================================
function GetPriceIdByTenmonTariff($tenmon_tariff, $is_trial = true)
{
    switch ($tenmon_tariff) {
        case 4: // конструктор
        case 7: // профессионал
        case 12: // максимум
            $is_trial ? $price_position = 65 : $price_position = 64 ;
            break;
        case 3: // эксперт
        case 8: // эксперт
        case 11: // ваш - эксперт
            $is_trial ? $price_position = 2 : $price_position = 1 ;
            break;
        case 2: // стандарт
        case 6: // стандарт
        case 10: // я - специалист
            $is_trial ? $price_position = 56 : $price_position = 55 ;
            break;
        case 1: // базовый
        case 5: // базовый
        case 9: // эконом
        $is_trial ? $price_position = 63 : $price_position = 62 ;
            break;
        default:
            $price_position = 0;
            break;
    }
    return $price_position;
}

// ===============================================================
// получаем тариф для позиции прайс-листа
// ===============================================================
function GetTariffNameById ($tariff_id)
{
    switch ($tariff_id) {
        case 12:
            return '12.Максимум';
        case 11:
            return '11.Ваш Эксперт';
        case 10:
            return '10.Я - Специалист';
        case 9:
            return '9.Эконом';
        case 8:
            return '8.Эксперт';
        case 7:
            return '7.Профессионал';
        case 6:
            return '6.Специалист';
        case 5:
            return '5.Базовый';
        case 4: // конструктор
            return '4.Конструктор';
        case 3: // эксперт
            return '3.Эксперт';
        case 2: // стандарт
            return '2.Стандарт';
        case 1: // базовый
            return '1.Базовый';
    }

    return '';
}
// ===============================================================
// получаем текущие данные о клиенте из теномна
// ===============================================================
function GetTenmonClientData($tm_id, $mysqli)
{
    $data = array();

    $tenmon_id = (int)($tm_id);

    if ($tenmon_id > 0 && $mysqli) {
        $query_tm = "SELECT u.ID,u.ShortName,u.FullName,u.INN,u.KPP,u.OGRN,u.LegalAddress,u.SearchEnable,u.ManagerID,u.login,u.is_Advert,u.Tariff
        FROM tender_monitor.Users as u WHERE u.ID = $tenmon_id LIMIT 1";

        if ($result_tm = $mysqli->query($query_tm)) {
            $data = $result_tm->fetch_array(MYSQLI_ASSOC);
        }
    }

    return $data;
}

function getInvoiceNumber($Id)
{
    return 'TM' . str_pad($Id, 7, '0', STR_PAD_LEFT);
}
