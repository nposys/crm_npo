<?php 

require_once ('add/tables.php');
require_once ('add/functions/common_order_functions.php');

if (!$event['is_new_line']) {
    $data = [];
    $data['order_id'] = $line['id'];
    $data['order_num'] = $line['f7071']; 
    $data['service_id'] = $line['f4451']['id'];
    $data['price_id'] = $line['f13071']; 
    
    $data['order_sum'] = $line['f4461'];
    $data['order_priority'] = $line['f12871'];
    $data['order_status'] = $line['f6551'];
    $data['production_status'] = $line['f10240'];
    
     $job_id = auto_create_job($data, $cb_tables);
}
