<?php

require_once 'add/tables.php';
require_once 'add/db.php';

// функция обновляет данный тендера по id либо по GovRuId + LotNumber
function UpdateTenderData($data, $cb_tables, $databases)
{
    global $cb_tables;
    global $databases;

    if($data['id']>0 && is_array($cb_tables) && is_array($databases))
    {
        $row_tender = data_select_array($cb_tables['tableTenders'],"id=".$data['id']);
    }
    else
    {
        if($data['govruid']<>"" && $data['lot_number']<>"")
        {
            $row_tender = data_select_array($cb_tables['tableTenders'],"f5041='".$data['govruid']."' and f6391=".$data['lot_number']);
        }
        else
        {
            return -1;
        }
    }

    if(!$row_tender['id']) return -1;

    $data_update = array();
    $tender_id = $row_tender['id'];
    $GovRuID = $row_tender['f5041'];
    $LotNumber = $row_tender['f6391'];

    // connect to MS SQL
    ini_set('mssql.charset','UTF8');
    $serverName = $databases["MSSQL"]["server"];
    $connectionOptions = array(
        "Database" => $databases["MSSQL"]["db"],
        "Uid" => $databases["MSSQL"]["user"],
        "PWD" => $databases["MSSQL"]["pass"],
        'ReturnDatesAsStrings'=>true,

    );

    $sqlLink = sqlsrv_connect($serverName, $connectionOptions);
    //sqlsrv_connect ($databases['MSSQL']['server'] , $databases['MSSQL']['user'], $databases['MSSQL']['pass']);
    if($sqlLink)
    {
        //$db = sqlsrv_select_db($databases['MSSQL']['db'], $sqlLink);
        if (true)
        {
            //declare the SQL statement that will query the database
            $query = "
                      SELECT TOP 1
                        t.uid,t.TenderLaw, t.PublicDate,t.OpenDate,t.StartDate,t.ExamDate, t.link, t.Title as Title, t.SiteCode, ts.StatusName,
                        gc.ShortName as CustomerName, gc.LegalAddress as CustomerLegalAddress,
                        gc.Phone as CustomerPhone, gc.Email as CustomerEmail, gc.ContactPerson as CustomerContactPerson,
                        tt.TenderType as TenderType,
                        rg.Title as Region,
                        rg.ID as RegionID,
                        l.LotNumber, l.Cost, l.RequestCost, l.ContractCost, t.TimeZoneUTC
                      FROM
                        tendersUnique as ut, tenders as t
                          join lots as l on l.TenderUID = t.UID
                          left join GovCustomers as gc on l.CustomerID = gc.ID
                          left join Cities as ct on ct.ID = t.City
                          left join Regions as rg on rg.ID = ct.Region
                          left join Okrugs as ok on ok.ID = rg.Okrug
                          left join TenderTypes as tt on tt.id = t.TenderType
                          left join TenderStatuses as ts on ts.id = t.Status
                      WHERE
                          ut.GovRuID Like '".$GovRuID."%' and l.LotNumber=$LotNumber and t.UID=ut.UID
                   ";

            // execute the SQL query and return records
            $res = sqlsrv_query($sqlLink, $query);
            $numRows = sqlsrv_num_rows($res);
            $sql_row = sqlsrv_fetch_array($res);

            //display the results
            if($sql_row['uid']<>"")
            {
                switch($sql_row['TenderLaw'])
                {
                    case 1:
                        $data_update['f5031'] = "44-ФЗ";
                        break;
                    case 2:
                        $data_update['f5031'] = "223-ФЗ";
                        break;
                    case 3:
                        $data_update['f5031'] = "Прочие";
                        break;
                    default:
                        $data_update['f5031'] = "";
                        break;
                }

                // -------------------------------------------------------
                // get id for Вид тенедера
                $rowTenderType = data_select_array($cb_tables['tableTenderTypes'], 'status=0 and f6681="'.$sql_row["TenderType"].'"');
                $data_update['f5111'] = $rowTenderType['id'];

                // -------------------------------------------------------
                // get id for Площадка
                if (trim($sql_row["SiteCode"]) <> "")
                {
                    $rowTenderSite = data_select_array($cb_tables['tableTenderSites'], 'status=0 and f8100="'.$sql_row["SiteCode"].'"');
                    $data_update['f6341'] = $rowTenderSite['id'];
                }
                else
                {
                    $data_update['f6341'] = "";
                }

                // -------------------------------------------------------
                // готовим ссылку на регион
                $regionID = "";
                if(intval($sql_row['RegionID'])>0)
                {
                    $rowRegion = data_select_array($cb_tables['tableRegions'], "status=0 and f14441=".$sql_row['RegionID']);
                    $regionID = $rowRegion['id'];
                }

                // -------------------------------------------------------
                $data_update['f5051'] = $sql_row["Title"];
                $data_update['f6381'] = $sql_row["StatusName"];
                $data_update['f5121'] = $sql_row["link"];

                $data_update['f5061'] = $sql_row["CustomerName"];
                $data_update['f6411'] = $sql_row["CustomerLegalAddress"] . " " . $sql_row["CustomerPhone"] . " " . $sql_row["CustomerEmail"] . " " . $sql_row["CustomerContactPerson"];
                $data_update['f6401'] = $regionID; // $sql_row["Region"];

                $data_update['f5071'] = $sql_row["Cost"];
                $data_update['f5131'] = $sql_row["RequestCost"];
                $data_update['f5141'] = $sql_row["ContractCost"];

                $data_update['f5081'] = $sql_row["PublicDate"];    //'Дата публикации'
                $data_update['f5091'] = $sql_row["OpenDate"];  // 'Дата и время подачи'
                $data_update['f6371'] = $sql_row["ExamDate"];    // 'Дата рассмотрения'
                $data_update['f5101'] = $sql_row["StartDate"]; // 'Дата и время торгов'

                if($sql_row["TimeZoneUTC"]<>"")
                {
                    $data_update['f13991'] = intval($sql_row["TimeZoneUTC"]);

                    // корректируем  даты и время
                    $utcDiff = 7 - intval($sql_row['TimeZoneUTC']);

                    if($utcDiff<>0)
                    {
                        if($utcDiff>0) $time_diff = " + ".$utcDiff; else $time_diff = $utcDiff;

                        $datePublicNsk = date('Y-m-d H:i:s', strtotime($sql_row['PublicDate'].$time_diff." hour"));
                        $dateOpenNsk = date('Y-m-d H:i:s', strtotime($sql_row['OpenDate'].$time_diff." hour"));
                        $dateExamNsk = date('Y-m-d H:i:s', strtotime($sql_row['ExamDate'].$time_diff." hour"));
                        $dateStartNsk = date('Y-m-d H:i:s', strtotime($sql_row['StartDate'].$time_diff." hour"));
                    }
                    else
                    {
                        $datePublicNsk = $sql_row['PublicDate'];
                        $dateOpenNsk = $sql_row['OpenDate'];
                        $dateExamNsk = $sql_row['ExamDate'];
                        $dateStartNsk = $sql_row['StartDate'];
                    }

                    $data_update['f14011'] = $datePublicNsk; // 'Дата публикации (НСК)'
                    $data_update['f13971'] = $dateOpenNsk; // 'Дата и время подачи (НСК)'
                    $data_update['f14021'] = $dateExamNsk; // 'Дата рассмотрения (НСК)'
                    $data_update['f13981'] = $dateStartNsk; // 'Дата и время торгов (НСК)'
                }

                data_update($cb_tables['tableTenders'],$data_update,"id=$tender_id");
            } // end if get record from ms sql
        } // end if db

        //close the connection
        sqlsrv_close($sqlLink);
    }
} // end function

?>