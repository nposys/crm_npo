<?php
require_once 'add/tables.php';

/*
 * Автор: Шнитко А.Г.
 * Дата создания: 2016-11-16
 * Назначение:
 *  вычисление статуса аккредитации
 *  работает для 44-ФЗ (предполагает 3 летнюю аккредитацию)
 *
 * Входные параметры:
 *  $dateFinish - дата завершения аккредитации
 *
 * Выходные параметры:
 *  в случае успеах: статус аккредитации
 */

function GetAccreditationStatus($dateFinish)
{
    $status = "";

    $dateNow = date("Y-m-d");
    $dateLimited = date("Y-m-d", strtotime($dateFinish." -3 month"));
    $dateProlongation = date("Y-m-d", strtotime($dateFinish." -6 month"));

    if($dateNow>$dateFinish)
        $status = "Не действующая";
    else
        if($dateNow>=$dateLimited)
            $status = "Ограниченная";
        else
            if ($dateNow>=$dateProlongation)
                $status = "Возможно продление";
            else
                $status = "Действующая";

    return $status;
}

/*
 * Автор: Шнитко А.Г.
 * Дата создания: 2016-11-16
 * Назначение:
 *  вычисление статуса эцп
 *
 * Входные параметры:
 *  $dateFinish - дата завершения эцп
 *
 * Выходные параметры:
 *  в случае успеах: статус эцп
 */

function GetSignatureStatus($dateFinish)
{
    $status = "";

    $dateNow = date("Y-m-d");

    if($dateNow>$dateFinish)
        $status = "истекла";
    else
        $status = "действует";

    return $status;
}

/*
 * Автор: Шнитко А.Г.
 * Дата создания: 2016-11-17
 * Назначение:
 *  получение id пользователя соответствующей должности
 *
 * Статус: в разработке
 *
 * Входные параметры:
 *  $positionType - тип должности
 *      1 = Начальник отдела продаж
 *      2 = Менеджер-аналитик
 *      3 = Исполнительный директор
 * Выходные параметры:
 *  в случае успеах: id пользователя
 *  в случае ошибки: -1
 */
function GetIdForManagerByPosition($positionType = 1)
{
    global $cb_tables;

    $userId = -1;
    $positionId = 0;
    switch($positionType)
    {
        default:
            break;
        case 1:
            $positionId = 21;
            break;
        case 2:
            $positionId = 15;
            break;
        case 3:
            $positionId = 5;
            break;
    }

    $rowManager = data_select_array($cb_tables['tablePersonal'], "status=0 and f484=$positionId");
    if($rowManager['id']>0){
        $userId = $rowManager['f1400'];
    }

    return $userId;
}

/*
 * Получаем id менеджера на которого требуется создать событие
 * (например, приближается срок переаккредитации, продления ЭЦП, поиска)
 *
 * Входные параметры:
 *  $managerId - id пользователя (из таблицы cb_users)
 *
 * Выхожные параметры:
 *  в случае успеха: id пользователя
 *  в случае ошибки: -1
 */

function GetActiveManagerForEvent($managerId, $taskText)
{
    $resultId = -1;
    // выбераем нужного сотрудника из таблицы связи
    // если пустое значение, то присваеваем id исполнительного директора
    if ($managerId == 0 || $managerId == "") {
        $resultId = GetManagerForAutoTask($taskText);
        $rowManagerUser = sql_select_array(USERS_TABLE, "id=$resultId");
        if ($rowManagerUser["arc"] == 1) {
            $resultId = GetIdForManagerByPosition(1);
        }
    } else {
        $resultId = $managerId;
        $sqlQuery = "SELECT 
                        u.id
                    FROM
                        cb_users AS u
                            INNER JOIN
                        cb_data781 AS t ON u.id = t.f13431
                    WHERE
                        u.id = $managerId
                            AND (f13461 <= '".date("Y-m-d")."'
                            AND f13471 >= '".date("Y-m-d")."'
                            OR u.arc = 1)";
        $result = sql_query($sqlQuery);
        // если пользователь в архиве - возвращаем данные начальника
        if (sql_fetch_assoc($result)) {
            $resultId = GetManagerForAutoTask($taskText);
            $rowManagerUser = sql_select_array(USERS_TABLE, "id=$resultId");
            if ($rowManagerUser["arc"] == 1) {
                $resultId = GetIdForManagerByPosition(1);
            }
        }
    }

    return $resultId;
}

/*
 * Проверка наличия события
 *
 *  Входные параметры:
 *      $companyId - id компании
 *      $baseTitle - базовая формулировка события
 *      $objectName - служебное название объекта в событии
 *      $objectId - id объекта в событии
 *      $eventType - тип события
 *
 *  Выходные параметры:
 *      true - если событие для данного клиента и объекта уже создано
 *      false - иначе
 */
function CheckEventExistForCondition($companyId, $baseTitle, $objectName, $objectId, $eventType = 'Звонок')
{
    global $cb_tables;
    $result = false;

    if($companyId>0)
    {
        $eventTitleTest = "%$baseTitle%[$objectName:$objectId]%";

        $rowEventTest = data_select_array($cb_tables['tableEvents'],
            "status=0 and f723=$companyId and f773='$eventType' and f725 LIKE '$eventTitleTest'");

        if($rowEventTest['id']>0) $result = true;
    }

    return $result;
}

function GetManagerForAutoTask($taskTitle){

    global $cb_tables;
    $rowManager = data_select_array(1181, "status=0 and f19921='$taskTitle'");
    if($rowManager['id']>0)
    {
        $userId = $rowManager['f19931'];
    }else {

        $userId = GetIdForManagerByPosition(1);
    }
    return $userId;
}

?>