<?php

require ('add/tables.php');
require_once ('add/functions/service_functions.php');
require_once ('add/functions/common_order_functions.php');
require_once ('add/functions/common_clients_functions.php');

// получаем id клиента
if(is_array($line['f4441'])){
    $client_id = $line['f4441']['id'];
}else{
    $client_id = $line['f4441'];
}

// получаем id услуги
if(is_array($line['f4451'])){
    $service_id = $line['f4451']['id'];
}else{
    $service_id = $line['f4451'];
}

// заполняем массив для создания заказа
$data = [];
$data['user_id'] = $line['user_id'];
$data['order_id'] = $line['id'];
$data['order_path'] = $line['f6621'];
$data['order_date'] = $line['f6591'];
$data['order_num'] = $line['f7071'];
$data['order_description'] = $line['f4431'];
$data['service_num'] = $service_id;
$data['client_id'] = $client_id;

// получаем путь до папки клиента
if(!is_array($client_id) && $client_id>0 && $cb_tables['tableClients']>0) {
    $row_client = data_select_array($cb_tables['tableClients'],"id=$client_id");
    $data['client_path_folder'] = $row_client['f10080'];

    $result = CreateOrderPath($data, $cb_tables, true);
}
