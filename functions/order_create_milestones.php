<?php 

require 'add/tables.php';
require_once 'add/functions/service_functions.php';
require_once 'add/functions/common_order_functions.php';

CreateOrderMilestones($line, $cb_tables);

/*
$debug = true;


$order_id = $line['id'];
$service_type_id = $line['f4451'];
$price_type_id = $line['f13071'];

$client_id = $line['f4441'];
$order_name = $line['f4431'];
$manager_client_id = $line[''];
$status_order = $line['f6551'];
$status_production = $line['f10240'];

switch($service_type_id)
{
    case 5: // подготовка заявки
        $milestones = array();      
        
        if($row_app = data_select_array($cb_tables['tableOrdersApp'],"status=0 and f5471=".$order_id))
        {
            $tender_id = $row_app['f5481'];
            
            if($tender_id<>'')
            {
                if($row_tender = data_select_array($cb_tables['tableTenders'],"status=0 and id=".$tender_id))
                {
                    $govruid = $row_tender['f5041'];
                    $lotnum = $row_tender['f6391'];
                    
                    $date_podacha = $row_tender['f13971'];                    
                    $date_rassmotrenie = $row_tender['f14021'];
                    $date_torgi = $row_tender['f13981'];

                    if($date_podacha<>'0000-00-00 00:00:00')
                        $milestones[] = array('type'=>'Подача заявки', 'date' =>$date_podacha);
                    
                    if($date_torgi<>'0000-00-00 00:00:00' && $date_torgi<>$date_podacha)
                    {
                        $milestones[] = array('type'=>'Торги', 'date' =>$date_torgi);
                    }
                        
                    if($date_torgi<>'0000-00-00 00:00:00')
                    {
                        $date_contract = date('Y-m-d H:i:s', strtotime($date_torgi." +5 day"));
                        $milestones[] = array('type'=>'Подписание контракта', 'date' =>$date_contract);
                    }
                    
                    
                    foreach($milestones as $milestone)
                    {
                        if($row_milestone = data_select_array($cb_tables['tableOrdersMilestones'],
                                   "status=0 and f15471=".$order_id." and f15481='".$milestone['type']."'"))
                        {
                            $arr_milestone = array(
                                "f15491" => $milestone['date']
                            );
                            data_update($cb_tables['tableOrdersMilestones'],$arr_milestone,"id=".$row_milestone['id']);                            
                        }
                        else
                        {
                            $arr_milestone = array(
                             "status" => 0,
                             "f15471" => $order_id,
                             "f15481" => $milestone['type'],
                             "f15491" => $milestone['date'],
                                    "f15761" => 'Нет',
                                'f15531' => $client_id,
                                'f15541' => $order_name,
                                'f15551' => $service_type_id,
                                'f15561' => $price_type_id,
                                'f15581' => $manager_client_id,
                                'f15591' => $status_order,
                                'f15601' => $status_production                                
                            );
                            data_insert($cb_tables['tableOrdersMilestones'],$arr_milestone);
                        }
                    }
                }
                                
            }
        }
        
        break;
    case 7: // подготовка документации
        if($row_app = data_select_array($cb_tables['tableOrdersCmplx'],"status=0 and f5621=".$order_id))
        {
            $tender_id = $row_app['f5821'];
            
            if($tender_id<>'')
            {
                if($row_tender = data_select_array($cb_tables['tableTenders'],"status=0 and id=".$tender_id))
                {
                    $govruid = $row_tender['f5041'];
                    $lotnum = $row_tender['f6391'];
            
                    $date_podacha = $row_tender['f13971'];
                    $date_rassmotrenie = $row_tender['f14021'];
                    $date_torgi = $row_tender['f13981'];            
                    
                    if($date_podacha<>'0000-00-00 00:00:00')
                        $milestones[] = array('type'=>'Подача заявки', 'date' =>$date_podacha);

                    if($date_rassmotrenie<>'0000-00-00 00:00:00')
                        $milestones[] = array('type'=>'Протокол 1Ч', 'date' =>$date_rassmotrenie);

                        
                    if($date_torgi<>'0000-00-00 00:00:00')
                    {
                        $date_contract = date('Y-m-d H:i:s', strtotime($date_torgi." +5 day"));
            
                        $milestones[] = array('type'=>'Торги', 'date' =>$date_torgi);
                        $milestones[] = array('type'=>'Подписание контракта', 'date' =>$date_contract);
                    }
            
            
                        foreach($milestones as $milestone)
                        {
                            if($row_milestone = data_select_array($cb_tables['tableOrdersMilestones'],
                                "status=0 and f15471=".$order_id." and f15481='".$milestone['type']."'"))
                            {
                                $arr_milestone = array(
                                    "f15491" => $milestone['date']
                                );
                                data_update($cb_tables['tableOrdersMilestones'],$arr_milestone,"id=".$row_milestone['id']);
                                //echo "update ".$milestone['type']." date:".$milestone['date']."<br>\r\n";
                            }
                            else
                            {
                                $arr_milestone = array(
                                    "status" => 0,
                                    "f15471" => $order_id,
                                    "f15481" => $milestone['type'],
                                    "f15491" => $milestone['date'],
                                    "f15761" => 'Нет',
                                'f15531' => $client_id,
                                'f15541' => $order_name,
                                'f15551' => $service_type_id,
                                'f15561' => $price_type_id,
                                'f15581' => $manager_client_id,
                                'f15591' => $status_order,
                                'f15601' => $status_production                                
                                );
                                data_insert($cb_tables['tableOrdersMilestones'],$arr_milestone);
                            }
                        }
                }
            
            }            
        }
        break;
}

*/
?>