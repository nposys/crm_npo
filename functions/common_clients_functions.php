<?php
require_once ('add/functions/service_functions.php');

// -----------------------------------------------------------
// создание карточки по компании
//  $sqlConnectId - либо ссылка на готовое подключение к БД, либо массив с параметрами подключенияк БД
// -----------------------------------------------------------
function CreateClientPath($data, $cb_tables, $bReturnWinPath = true)
{
    mb_internal_encoding("UTF-8");
    mb_regex_encoding("UTF-8");

    $client_id = (int)($data['client_id']);
    $client_path = trim($data['client_path']);
    $path_folder = trim($data['path_folder']); // старый путь из базы MS SQL
    $strName = $data['name'];

    if($client_id == 0 || $strName == "") return -1;

    // если пустое поле, то формируем папку
    if ( $client_path == "" || $path_folder == "")
    {
        // удаляем спецсимволы из названия папки
        $pattern = "[".preg_quote ('\/»«"!*:|></?+.,\'')."]";
        $strName = mb_ereg_replace($pattern, "", $strName);

        if($path_folder=="") $path_folder = $strName;

        $strPathPrefix = "\\\\192.168.0.9\\Common\\Data\\Клиенты\\$path_folder";
        $strPathPrefixUnix = "/mnt/common/Data/Клиенты/$path_folder/";

        data_update(42,array("f10080"=>$path_folder, "f11731"=>$strPathPrefix),"id=$client_id"); //$line['PathFolder'] = $strName;  $line['Папка клиента'] = $strPathPrefix;

        // создаем папки если она не существует
        $arrFolders = array
        (
            '10. Договоры',
            '20. Документы/00. Реквизиты',
            '20. Документы/05. Уведомление из статистики (коды)',
            '20. Документы/10. Свидетельства ФНС (листы записи)',
            '20. Документы/20. ЕГРЮЛ (ИП)',
            '20. Документы/30. Учредительные документы',
            '20. Документы/31. Сведения об учредителях и руководителе',
            '20. Документы/35. Решения (протоколы), приказы',
            '20. Документы/40. Бухгалтерская отчётность',
            '20. Документы/45. Налоговая отчётность',
            '20. Документы/46. Справки ФНС',
            '20. Документы/50. Лицензии и допуски',
            '20. Документы/60. Документы недвижимость',
            '20. Документы/70. Опыт',
            '20. Документы/80. Производственные мощности',
            '20. Документы/85. Кадровые ресурсы',
            '20. Документы/90. Прочие документы',
            '20. Документы/91. Счета',
            '20. Документы/99. Архив',
            '30. Заказы',
            '40. Отчеты'
        );

        // если папка не существует, то создаем папку и всю структуру
        if (!file_exists($strPathPrefixUnix)) foreach($arrFolders as $strPath ) mkdir($strPathPrefixUnix.$strPath, 0777, true);

        if($bReturnWinPath)
            return $strPathPrefix;
        else
            return $path_folder;
    }
}


// -----------------------------------------------------------
// создание карточки по компании
//  $sqlConnectId - либо ссылка на готовое подключение к БД, либо массив с параметрами подключенияк БД
// Возвращаемые значения: 1 - создан клиент, 2 - обновлен клиент, -1 - ошибка
// -----------------------------------------------------------
function CreateClient($inn, $kpp, $sqlConnectId, $cb_tables, $bDoUpdate=false, $dt_start='2015-04-01 00:00:00', $debug=false)
{
    global $databases;
    global $user;
    $result = -1;

    $protocol_types = array(
        "44-ФЗ" =>
        array(
            "Электронный аукцион" =>
            array(
                "table_name" => "ProtocolsEF",
                "field_protocol_inn" => "prot.INN",
                "field_protocol_kpp" => "prot.KPP",
                "field_protocol_contactperson" => "prot.ContactPerson",
                "field_protocol_phone" => "prot.Phone",
                "field_protocol_email" => "prot.Email"
            ),
            "Открытый конкурс" =>
            array(
                "table_name" => "ProtocolsOK",
                "field_protocol_inn" => "prot.INN",
                "field_protocol_kpp" => "prot.KPP",
                "field_protocol_contactperson" => "prot.ContactPerson",
                "field_protocol_phone" => "prot.Phone",
                "field_protocol_email" => "prot.Email"
            ),
            "Запрос котировок" =>
            array(
                "table_name" => "ProtocolsZK",
                "field_protocol_inn" => "prot.INN",
                "field_protocol_kpp" => "prot.KPP",
                "field_protocol_contactperson" => "prot.ContactPerson",
                "field_protocol_phone" => "prot.Phone",
                "field_protocol_email" => "prot.Email"
            ),
        ),
        "223-ФЗ" =>
        array(
            "Прочие" =>
            array(
                "table_name" => "prot_Results",
                "field_protocol_inn" => "prot.SupplierINN",
                "field_protocol_kpp" => "prot.SupplierKPP",
                "field_protocol_contactperson" => "NULL",
                "field_protocol_phone" => "NULL",
                "field_protocol_email" => "NULL"
            )
        ),
        "служебное" =>
        array(
            "Поставщики" =>
            array(
                "table_name" => "Suppliers",
                "field_protocol_inn" => "prot.INN",
                "field_protocol_kpp" => "prot.KPP",
                "field_protocol_contactperson" => "prot.ContactPerson",
                "field_protocol_phone" => "prot.Phone",
                "field_protocol_email" => "prot.Email"
            ),
            "Контракты" =>
            array(
                "table_name" => "Contracts",
                "field_protocol_inn" => "prot.INN",
                "field_protocol_kpp" => "prot.KPP",
                "field_protocol_contactperson" => "prot.ContactPerson",
                "field_protocol_phone" => "prot.Phone",
                "field_protocol_email" => "prot.SupplierEmail"
            )
        )
    );
    $fields_contacts = array("phones"=>"field_protocol_phone", "emails"=>"field_protocol_email", "contacts"=>"field_protocol_contactperson");

    $inn = trim($inn);
    $kpp = trim($kpp);

    if($inn=="" || !$sqlConnectId || !$cb_tables) {
        if($debug) {
            echo "error in input $inn $sqlConnectId $cb_tables<br>\r\n";
        }
        return -1;
    }

    // connect to mssql
    $bDoConnect = false;
    if(is_array($sqlConnectId)) {
        ini_set('mssql.charset','UTF8');
        $serverName = $databases["MSSQL"]["server"];
        $connectionOptions = array(
            "Database" => $databases["MSSQL"]["db"],
            "Uid" => $databases["MSSQL"]["user"],
            "PWD" => $databases["MSSQL"]["pass"],
            'ReturnDatesAsStrings'=>true,
        );

        $sqlConnectId = sqlsrv_connect($serverName, $connectionOptions);
        if ($sqlConnectId) {
            $bDoConnect = true;
        }
    } // end if connect to ms sql

    $row = data_select_array($cb_tables['tableClients'], "status=0 and f1056='".$inn."'");
    $clientId = $row['id'];

    // ----------------------------------------------------------
    // если потребуются данные для карточки, то делаем запрос в SQL
    // ----------------------------------------------------------
    if($clientId=='' || $bDoUpdate) {
        if($kpp != "") {
            $strKPPfilter = " AND KPP='".$kpp."'";
        } else {
            $strKPPfilter = "";
        }

        //  выбираем данные о компании из MS SQL
        $sqlQuery = "
                SELECT TOP 1
                    [ID],[Type],[INN],[KPP],[Form],[Title],[FactualAddress],[PostAddress]
                    ,[ContactPerson],[Email],[Phone],[Fax],[AdditionalInfo],[Region]
                FROM Suppliers WHERE [Title] is not NULL and INN='$inn'".$strKPPfilter;
        $resultSQL = sqlsrv_query($sqlConnectId, $sqlQuery);
        $rowSQL = sqlsrv_fetch_array($resultSQL);

        // заполняем переменные с данными про клиента из таблицы поставщиков
        if($rowSQL['ID']<>"") {
            if($kpp=="") {
                $kpp = trim($rowSQL['KPP']);
            }
            $contact_person = trim($rowSQL['ContactPerson']);
            $add_info =  trim($rowSQL['AdditionalInfo']);

            $comment = "";
            if($contact_person<>"") {
                $comment .= "Контактные лица:".$contact_person."\r\n";
            }
            if($add_info<>"") {
                $comment .= "Контактные лица:".$contact_person."\r\n";
            }

            $ifnsCode = substr($inn, 0, 4);
            $rowIFNS = data_select_array($cb_tables['tableIFNS'], "status=0 and f8390='".$ifnsCode."'");
            $ifnsId = $rowIFNS['id'];

            $fact_address = trim($rowSQL['FactualAddress']);
            $post_address = trim($rowSQL['PostAddress']);
            if($post_address=="") {
                $post_address = $fact_address;
            }

            $email = trim($rowSQL['Email']);
            $phone = trim($rowSQL['Phone']);

            $title = $rowSQL['Title'];
            if(trim($rowSQL['Form'])<>"") {
                $title .= ", ".trim($rowSQL['Form']);
            }
            if(trim($title)=="" && strlen($inn)==12) {
                $title = $contact_person;
            }
        }

        // берем дополнительные контактные данные из реестра контрактов и протоколов
        // получаем контактную информацию из всех источников
        $contact_data = array();
        foreach($fields_contacts as $name_array=>$field)
        {
            $sqlQueryBase =
                "SELECT @field@ as @field_output@ 
                  FROM @table_name@ 
                  WHERE @field_protocol_inn@ 
                  LIKE '@inn@%' GROUP BY @field@ HAVING @field@<>''";
            $sqlQuery = "";

            $i = 0;
            foreach ($protocol_types as $tender_law => $protocol) {
                foreach ($protocol as $tender_type => $protocol_data) {
                    $table_name = $protocol_data["table_name"];
                    $field_name = str_replace("prot.", "", $protocol_data[$field]);
                    $field_protocol_inn = str_replace("prot.", "", $protocol_data["field_protocol_inn"]);

                    if($field_name <> "NULL") {
                        $nextQuery = str_replace("@inn@",$inn,
                            str_replace("@field@", $field_name,
                                str_replace("@field_protocol_inn@", $field_protocol_inn,
                                    str_replace("@table_name@", $table_name,
                                        str_replace("@field_output@", $field,
                                            $sqlQueryBase
                                            )
                                        )
                                    )
                                )
                            );
                        if($i>0) {
                            $sqlQuery .= " UNION ".$nextQuery;
                        } else {
                            $sqlQuery .= $nextQuery;
                        }
                        $i++;
                    }
                }
            }

            $resultSQLQuery = sqlsrv_query($sqlConnectId, $sqlQuery);
            while($rowSQL = sqlsrv_fetch_array($resultSQLQuery)) {
                $contact_data[$name_array][] = trim($rowSQL[$field]);
            }
        } // end foreach field_contacts

        //print_r($contact_data);
        $add_phones = trim(implode(", ",$contact_data["phones"]));
        $add_emails = trim(implode(", ",$contact_data["emails"]));
        $add_contacts = trim(implode(", ",$contact_data["contacts"]));

        $additional_contacts = "";
        if($add_phones<>"") {
            $additional_contacts .= "Телефоны: ".implode(", ",$contact_data["phones"])."\r\n\r\n";
        }
        if($add_emails<>"") {
            $additional_contacts .= "Emails: ".implode(", ",$contact_data["emails"])."\r\n\r\n";
        }
        if($add_contacts<>"") {
            $additional_contacts .= "Контактные лица: ".implode(", ",$contact_data["contacts"]);
        }
        if($additional_contacts=="") {
            $additional_contacts = "Контактная информация не найдена";
        }

        if($phone=="" && $add_phones<>"") {
            $phone = $contact_data["phones"][0];
        }
        if($email=="" && $add_emails<>"") {
            $email = $contact_data["emails"][0];
        }

    } // end if get SQL data

    // получаем id региона
    $region = substr($inn, 0, 2);
    $regions_res = data_select_array(841, "status=0 and f14381=$region");
    $idRegion = (int)$regions_res['id'];

    // создание или обновление карточки клиента
    if($clientId=='') {

        //  если нет такой компании, то создаем карточку
        // добавляем строчку в таблицу клиентов
        $arr_client = array();
        $arr_client['status'] = 0; // инн
        $arr_client['f1056'] = $inn; // инн
        $arr_client['f1057'] = $kpp; // кпп
        $arr_client['f439'] = $title; // юр.название
        $arr_client['f440'] = $fact_address; // юр.адрес
        $arr_client['f435'] = $title; // название
        $arr_client['f1047'] = $post_address; // факт.адрес
        $arr_client['f442'] = $email; // емайл
        $arr_client['f441'] = $phone; // тел
        $arr_client['f14821'] = $additional_contacts; // дополнительные контакты
        $arr_client['f445'] = $comment; // примечания !
        $arr_client['f20481'] = date('Y-m-d H:i:s'); // Дата обновления статистики
        $arr_client['f18761'] = 6; // истоичник = Загружен из протоколов

        //$arr_client['f8990'] = $ifnsId; // район ИФНС связь
        //$arr_client['f4911'] = $ifnsCode; // налоговая

        $arr_client['f552'] = 'Холодный'; // статус лида  Холодный
        $arr_client['f4921'] = ''; // статус постоянного клиента
        $arr_client['f3711'] = 'Юр.лицо'; // вид: Юр.лицо Физ.лицо
        $arr_client['f772'] = 'Потенциальный'; // тип: Потенциальный Клиент Партнер
        $arr_client['f11161'] = 'Поставщик'; // категория: Поставщик Заказчик
        $user_id = (int)$user['id'];
        $user_group = (int)$user['group_id'];
        if(in_array($user_group,[811,2])){
            if(in_array($user_id,[471,640,751])){
                $arr_client['f8720'] = $user_id; // Менеджер клиента
            }else  if(in_array($user_id,[921,911])){
                $arr_client['f438'] = $user_id; // Менеджер продаж
            }
        }

        $arr_client['f19561'] = $idRegion; // регион

        $clientId = data_insert($cb_tables['tableClients'], $arr_client);
        if($clientId>0) {
            // добавляем строчку в таблицу статистики по клиенту
            $arr_client_data = array();
            $arr_client_data['status'] = 0;
            $arr_client_data['f10410'] = $clientId;
            $arr_client_data['f12921'] = 'Потенциальный'; // тип
            $arr_client_data['f12941'] = ''; // Статус клиента
            $arr_client_data['f14491'] = 'Холодный'; // Статус лида
            data_insert($cb_tables['tableClientsData'], $arr_client_data);
        }

        // обновляем данные об участиях в тендерах
        UpdateParticipations($clientId, $sqlConnectId, $cb_tables, $dt_start, $debug);

        if($debug) {
            echo "inserted client $inn $title<br>\r\n";
        }
        $result = 1;
    } else {
        if($debug) {
            write_log("already have client $inn $title (id:$clientId)");
        }

        if($bDoUpdate) {
            // если клиент есть, то обновляем данные по возможности
            $arr_client = [];

            if(trim($row['f1057'])=="") {
                $arr_client['f1057'] = $kpp;
            } // кпп
            if(trim($row['f439'])=="" || trim($row['f439'])==1) {
                $arr_client['f439'] = $title;
            } // юр.название
            if(trim($row['f440'])=="") {
                $arr_client['f440'] = $fact_address;
            } // юр.адрес
            if(trim($row['f435'])=="") {
                $arr_client['f435'] = $title;
            } // название
            if(trim($row['f1047'])=="") {
                $arr_client['f1047'] = $post_address;
            } // факт.адрес
            if(trim($row['f442'])=="") {
                $arr_client['f442'] = $email;
            } // емайл
            if(trim($row['f441'])=="") {
                $arr_client['f441'] = $phone;
            } // тел
            if(trim($row['f445'])=="") {
                $arr_client['f445'] = $comment;
            }
            //if(trim($row['f8990'])=="") $arr_client['f8990'] = $ifnsId; // район ИФНС связь
            //if(trim($row['f4911'])=="") $arr_client['f4911'] = $ifnsCode; // налоговая
            if(trim($row['f14821'])=="") {
                $arr_client['f14821'] = $additional_contacts; // дополнительные контакты
            }
            if(!$row['f19561']) {
                $arr_client['f19561'] = $idRegion;
            } // регион

            if(count($arr_client)>0) {
                $arr_client['f20481'] = date('Y-m-d H:i:s'); // Дата обновления статистики
                data_update($cb_tables['tableClients'], $arr_client, "status=0 and id=".$clientId);
            }

            // проверяем наличие записи в подтаблице с доп.данными
            $rowClientData = data_select_array($cb_tables['tableClientsData'],"status=0 and f10410=".$clientId);
            if($rowClientData['id']=="") {
                // добавляем строчку в таблицу статистики по клиенту
                $arr_client_data = array();
                $arr_client_data['status'] = 0;
                $arr_client_data['f10410'] = $clientId;
                $arr_client_data['f12921'] = 'Потенциальный'; // тип
                $arr_client_data['f12941'] = ''; // Статус клиента
                $arr_client_data['f14491'] = 'Холодный'; // Статус лида
                data_insert($cb_tables['tableClientsData'], $arr_client_data);
            }

            // обновляем данные об участиях в тендерах
            $bAlwaysUpdate = true;
            $dt_not_update = date('Y-m-d', strtotime(date('Y-m-d H:i:s')." -1 hours"));
            if($bAlwaysUpdate || $rowClientData['f14781']<=$dt_not_update) {
                if($debug) {
                    write_log("before UpdateParticipations");
                }
                UpdateParticipations($clientId, $sqlConnectId, $cb_tables, $dt_start, $debug);
                if($debug) {
                    write_log ("update participations done for $inn ".$rowClientData['f14781']." < ".$dt_not_update);
                }
            } else {
                if($debug) {
                    write_log("update participations not required for $inn ".$rowClientData['f14781']." >= ".$dt_not_update);
                }
            }

            if($debug) {
                write_log("updated client data $inn");
            }
            $result = 2;
        }
    }
    if($bDoConnect) {
        sqlsrv_close($sqlConnectId);
    }
    return $result;
}

// -----------------------------------------------------------
// обновление статистики по компании
//  $dt_start - дата с которой вычислять статистику
// -----------------------------------------------------------
function UpdateParticipations($clientId, $sqlConnectId, $cb_tables, $dt_start='2017-01-01 00:00:00', $debug=false)
{
    if($clientId=="") {
        return false;
    } // если id клиента не передан, то выходим
    $result = false;

    $debug_str = "";
    if($debug){
        write_log("start update participations $clientId");
    }

    // проверяем наличие записи в подтаблице с доп.данными
    $rowClientData = data_select_array($cb_tables['tableClientsData'],"status=0 and f10410=".$clientId);
    if($rowClientData['id']=="") {
        // добавляем строчку в таблицу статистики по клиенту
        $arr_client_data = array();
        $arr_client_data['status'] = 0;
        $arr_client_data['f10410'] = $clientId;
        $arr_client_data['f12921'] = 'Потенциальный'; // тип
        $arr_client_data['f12941'] = ''; // Статус клиента
        $arr_client_data['f14491'] = 'Холодный'; // Статус лида
        data_insert($cb_tables['tableClientsData'], $arr_client_data);
    }

    // connect to mssql
    $bDoConnect = false;
    if(is_array($sqlConnectId)) {
        $databases = $sqlConnectId;
        ini_set('mssql.charset','UTF8');
        $serverName = $databases["MSSQL"]["server"];
        $connectionOptions = array(
            "Database" => $databases["MSSQL"]["db"],
            "Uid" => $databases["MSSQL"]["user"],
            "PWD" => $databases["MSSQL"]["pass"],
            //'ReturnDatesAsStrings'=>true,
        );

        $sqlConnectId = sqlsrv_connect($serverName, $connectionOptions);
        if ($sqlConnectId) {
            $bDoConnect = true;
        }
    }

    $arr_activity = array(
        "00. нет участий" => 0,
        "10. мало участий" => 1,
        "20. средне участий" => 6,
        "30. много участий" => 999999
    );

    $arr_size = array(
        "00. не определено" => 0.0,
        "10. низкий средний чек" => 500000.0,
        "20. нормальный средний чек" => 3000000.0,
        "30. высокий средний чек" => 9999999999999.0
    );

    $date_filter = " t.CreateDate >=CAST('".$dt_start."' as datetime2) AND ";

    // константы для мониторинга участий без нас
    $days_delay_apps = 30;
    $days_delay_complex = 60;

    $row_client_data = data_select_array($cb_tables['tableClientsData'],"status=0 and f10410=".$clientId);
    if($row_client_data['id']=="") {
        return false;
    }  // если нет такой карточки клиента, выходим (но можно попробовать создать)

    $row_client = data_select_array($cb_tables['tableClients'],"status=0 and id=".$clientId);
    if($row_client['id']=="") {
        return false;
    }  // если нет такой карточки клиента, выходим (хотя можно попробовать создать)
    $inn = trim($row_client['f1056']);

    if($debug){
        write_log("client:$clientId inn:$inn");
        write_log ("UpdateParticipations title:".$row_client['f435']." inn:$inn row_client_data_id:".$row_client_data['id']." row_client_id:".$row_client['id']);
    }

    $queries = array (
        "total" => array(
            "223" =>

            "SELECT DISTINCT
          COUNT(prot.ID) as NumProt,SUM (prot.Price) as SumLots,SUM (prot.Price) as SumProt,MIN(t.CreateDate) as MinDate,MAX(t.CreateDate) as MaxDate
        FROM
          prot_Results as prot 
            LEFT JOIN Suppliers as sup ON sup.ID = prot.SupplierID 
            LEFT JOIN Tenders as t ON t.GovRuID = prot.GovRuId and t.UID in (select UID from UniqueTenders as ut where ut.GovRuID=prot.GovRuId) 
            LEFT JOIN Lots as l ON l.LotUID = t.UID AND prot.LotNumber =l.LotNumber
        WHERE
          sup.INN = @INN;
      ",

            "TotlaZK44" =>

            "SELECT DISTINCT
        COUNT(prot.ID) as NumProt
        ,SUM (l.Cost) as SumLots
          ,SUM (prot.Cost) As SumProt
        ,MIN(t.ExamDate) as MinDate
        ,MAX(t.ExamDate) as MaxDate
      FROM
        ProtocolsZK as prot
          LEFT JOIN Suppliers as sup ON sup.ID = prot.SuppliersID
          LEFT JOIN Lots as l ON l.LotUID = prot.LotUID
          LEFT JOIN Tenders as t ON l.TenderUID = t.UID and t.UID in (select UID from UniqueTenders as ut where ut.GovRuID=prot.GovRuId)
      WHERE
        sup.INN = @INN;
      ",

            "TotalEF44"=>

            "SELECT DISTINCT
      COUNT(prot.ID) as NumProt
      ,SUM (l.Cost) as SumLots
        ,SUM (CASE WHEN prot.LastPrice IS NULL THEN l.cost ELSE prot.LastPrice END) As SumProt
      ,MIN(t.ExamDate) as MinDate
      ,MAX(t.ExamDate) as MaxDate
    FROM
      ProtocolsEF as prot
        LEFT JOIN Suppliers as sup ON sup.ID = prot.SuppliersID
        LEFT JOIN Lots as l ON l.LotUID = prot.LotUID
        LEFT JOIN Tenders as t ON l.TenderUID = t.UID and t.UID in (select UID from UniqueTenders as ut where ut.GovRuID=prot.GovRuId)
    WHERE
      sup.INN = @INN;
    ",

            "TotalOK44"=>

            "SELECT DISTINCT
      COUNT(prot.ID) as NumProt
      ,SUM (l.Cost) as SumLots
        ,SUM (prot.Price) As SumProt
      ,MIN(t.ExamDate) as MinDate
      ,MAX(t.ExamDate) as MaxDate
    FROM
      ProtocolsOK as prot
        LEFT JOIN Suppliers as sup ON sup.ID = prot.SuppliersID
        LEFT JOIN Lots as l ON l.LotUID = prot.LotUID
        LEFT JOIN Tenders as t ON l.TenderUID = t.UID and t.UID in (select UID from UniqueTenders as ut where ut.GovRuID=prot.GovRuId)
    WHERE
      sup.INN = @INN;
    "),

        "win" => array(
            "Win223" =>
            "SELECT DISTINCT
        COUNT(prot.ID) as NumProt,SUM (l.Cost) as SumLots,SUM (prot.Price) as SumProt,MIN(t.CreateDate) as MinDate,MAX(t.CreateDate) as MaxDate
      FROM
        prot_Results as prot 
          LEFT JOIN Suppliers as sup ON sup.ID = prot.SupplierID 
          LEFT JOIN Tenders as t ON t.GovRuID = prot.GovRuId and t.UID in (select UID from UniqueTenders as ut where ut.GovRuID=prot.GovRuId) 
          LEFT JOIN Lots as l ON l.LotUID = t.UID AND prot.LotNumber =l.LotNumber
      WHERE
        sup.INN = @INN AND prot.AppPlace = 1 AND prot.Admitted = 1
      ",

            "WinZK44"=>
            "SELECT DISTINCT
      COUNT(prot.ID) as NumProt
      ,SUM (l.Cost) as SumLots
        ,SUM (prot.Cost) As SumProt
      ,MIN(t.ExamDate) as MinDate
      ,MAX(t.ExamDate) as MaxDate
    FROM
      ProtocolsZK as prot
        LEFT JOIN Suppliers as sup ON sup.ID = prot.SuppliersID
        LEFT JOIN Lots as l ON l.LotUID = prot.LotUID
        LEFT JOIN Tenders as t ON l.TenderUID = t.UID and t.UID in (select UID from UniqueTenders as ut where ut.GovRuID=prot.GovRuId)
    WHERE
      sup.INN = @INN
      AND prot.Admitted = 1 AND prot.AppRating = 1
    ",

            "WinEF44"=>
            "SELECT DISTINCT
      COUNT(prot.ID) as NumProt
      ,SUM (l.Cost) as SumLots
        ,SUM (CASE WHEN prot.LastPrice IS NULL THEN l.cost ELSE prot.LastPrice END) As SumProt
      ,MIN(t.ExamDate) as MinDate
      ,MAX(t.ExamDate) as MaxDate
    FROM
      ProtocolsEF as prot
        LEFT JOIN Suppliers as sup ON sup.ID = prot.SuppliersID
        LEFT JOIN Lots as l ON l.LotUID = prot.LotUID
        LEFT JOIN Tenders as t ON l.TenderUID = t.UID and t.UID in (select UID from UniqueTenders as ut where ut.GovRuID=prot.GovRuId)
    WHERE
      sup.INN = @INN
      AND prot.SecondPartAdmitted = 1 AND prot.AppRating = 1
    ",
            "WinOK44"=>
            "
    SELECT DISTINCT
      COUNT(prot.ID) as NumProt
      ,SUM (l.Cost) as SumLots
        ,SUM (prot.Price) As SumProt
      ,MIN(t.ExamDate) as MinDate
      ,MAX(t.ExamDate) as MaxDate
    FROM
      ProtocolsOK as prot
        LEFT JOIN Suppliers as sup ON sup.ID = prot.SuppliersID
        LEFT JOIN Lots as l ON l.LotUID = prot.LotUID
        LEFT JOIN Tenders as t ON l.TenderUID = t.UID and t.UID in (select UID from UniqueTenders as ut where ut.GovRuID=prot.GovRuId)
    WHERE
      sup.INN = @INN
      AND prot.Admitted = 1 AND prot.AppRating = 1
    "
        )
    );

    if($inn<>"") {

        if($debug){
            write_log("before queries sqlConnectId=$sqlConnectId");
        }

        if ($sqlConnectId) {

            //execute the SQL query and return records
            foreach($queries as $type => $qarray) {
                $sup44 = false;
                $sup223 = false;

                $sum_lot = 0.0;
                $sum_prot = 0.0;
                $count_prot = 0;
                $date_min = "2100-00-00 00:00:00";
                $date_max = "2000-00-00 00:00:00";

                $bHaveErrors = false;

                foreach($qarray as $subtype => $query) {
                    $query = str_replace("@INN","'".$inn."'",$query);
                    $query = str_replace("WHERE","WHERE".$date_filter,$query);
                    $resultQuery = sqlsrv_query($sqlConnectId, $query);

                    if($debug){
                        write_log($type." : ".$query);
                    }

                    if($resultQuery) {
                        //$numRows = sqlsrv_num_rows($resultQuery);
                        $row = sqlsrv_fetch_array($resultQuery);

                        $sum_lot += $row['SumLots'];
                        $sum_prot += $row['SumProt'];
                        $count_prot += $row['NumProt'];


                        if($debug){
                            write_log("min_date:".$row['MinDate']);
                            write_log("max_date:".$row['MaxDate']);
                        }

                        $cur_min_date = "";
                        if(is_object($row['MinDate'])){
                            $cur_min_date = $row['MinDate']->format("Y-m-d h:i:s")."\r\n";
                        }
                        if($cur_min_date!=="" && $cur_min_date<$date_min) {
                            $date_min = $cur_min_date;
                        }

                        $cur_max_date = "";
                        if(is_object($row['MaxDate'])){
                            $cur_max_date = $row['MaxDate']->format("Y-m-d h:i:s")."\r\n";
                        }
                        if($cur_max_date<>"" && $cur_max_date>$date_max) {
                            $date_max = $cur_max_date;
                        }

                        if($subtype == "223" && $row['NumProt']>0) {
                            $sup223 = true;
                        }
                        if($subtype <> "223" && $row['NumProt']>0) {
                            $sup44 = true;
                        }
                    }else{
                        write_log("error in query inn:$inn type:$type subtype:$subtype");
                        write_log(print_r( sqlsrv_errors(), true));
                        $bHaveErrors = true;
                    }

                } // end foreach query

                $dt_now = date("Y-m-d H:i:s");

                if($date_max >= $date_min) {
                    $seconds_diff = strtotime($dt_now) - strtotime($dt_start);
                    $days_diff = floor($seconds_diff/(86400.0));
                    $num_month = $days_diff / 30.0;
                    $avg_count_per_month = round($count_prot / $num_month,2);
                } else {
                    $days_diff = 0.0;
                    $avg_count_per_month = 0.0;
                }

                $avg_sum_lots = 0.0;
                $avg_sum_prot = 0.0;
                if($count_prot>0.0) {
                    $avg_sum_lots = round($sum_lot / $count_prot, 2);
                    $avg_sum_prot = round($sum_prot / $count_prot, 2);
                }
                $avg_discount = 0.0;
                if ($sum_lot>0.0){
                    $avg_discount = round(($sum_lot-$sum_prot)/$sum_lot,2);
                }

                $arr_result[$type]["sum_lots"] = $sum_lot;
                $arr_result[$type]["sum_prot"] = $sum_prot;
                $arr_result[$type]["num_prot"] = $count_prot;
                $arr_result[$type]["min_date"] = $date_min;
                $arr_result[$type]["max_date"] = $date_max;
                $arr_result[$type]["days_diff"] = $days_diff;
                $arr_result[$type]["avg_count_per_month"] = $avg_count_per_month;
                $arr_result[$type]["avg_sum_lots"] = $avg_sum_lots;
                $arr_result[$type]["avg_sum_prot"] = $avg_sum_prot;
                $arr_result[$type]["avg_discount"] = $avg_discount;
                $arr_result[$type]["sup44"] = $sup44;
                $arr_result[$type]["sup223"] = $sup223;

                if($debug){
                    write_log("$type: $count_prot");
                }

            } // end foreach querytype

            if(!$bHaveErrors){
                $arr_update_stat = [];

                // to do:
                // $arr_update_stat['f10530'] = $arr_result['total']['num_prot']; // MainOKDP
                // $arr_update_stat['f10540'] = $arr_result['total']['num_prot']; // SubOKDP
                // добавить поля в таблицу:
                //  максимальное участие
                //  минимальное участие
                //  максимальная победа
                //  минимальная победа

                $date_update = date('Y-m-d H:i:s');
                $arr_update_stat['f14771'] = $dt_start; // Дата начала статистики
                $arr_update_stat['f14781'] = $date_update;  // Дата обновления данных статистики

                $arr_update_stat['f10440'] = $arr_result['total']['sum_lots']; // Начальная сумма участий
                $arr_update_stat['f10450'] = $arr_result['total']['sum_prot']; // Сумма участий
                $arr_update_stat['f10460'] = $arr_result['total']['num_prot']; // Кол-во участий

                $arr_update_stat['f10470'] = $arr_result['win']['sum_lots']; // Начальная сумма побед
                $arr_update_stat['f10480'] = $arr_result['win']['sum_prot']; // Сумма побед
                $arr_update_stat['f10490'] = $arr_result['win']['num_prot']; // Кол-во побед

                $arr_update_stat['f20471'] = 0.0;
                if((int)$arr_result['total']['num_prot']>0){
                    $arr_update_stat['f20471'] = (float) $arr_result['win']['num_prot'] / (float) $arr_result['total']['num_prot'] * 100.0; // Процент побед
                }

                $arr_update_stat['f10500'] = $arr_result['total']['avg_discount']; // Среднее снижение
                $arr_update_stat['f10510'] = $arr_result['win']['avg_sum_prot']; // Средняя сумма побед
                $arr_update_stat['f10520'] = $arr_result['total']['avg_sum_lots']; // Средняя сумма участий

                $arr_update_stat['f14561']  = '';
                $arr_update_stat['f14571'] = '';
                $arr_update_stat['f14581'] = 0.0;
                $arr_update_stat['f14601'] = 0.0;
                if((int)$arr_result['total']['num_prot']>0) {
                    if($debug){
                        write_log( $arr_result['total']['min_date']);
                        write_log( $arr_result['total']['max_date']);
                    }
                    $arr_update_stat['f14561'] = date("Y-m-d H:i:s", strtotime($arr_result['total']['min_date'])); // Дата первого участия
                    $arr_update_stat['f14571'] = date("Y-m-d H:i:s", strtotime($arr_result['total']['max_date'])); // Дата последнего участия
                    $arr_update_stat['f14581'] = $arr_result['total']['avg_count_per_month']; // Кол-во участий в месяц
                    $arr_update_stat['f14601'] = $arr_result['win']['avg_count_per_month']; // Кол-во побед в месяц
                }

                // $arr_update_stat['f14591'] .= $debug_str; // debug

                if($arr_result['total']['sup44']) {
                    $arr_update_stat['f14071'] = "Да";
                } else {
                    $arr_update_stat['f14071'] = "Нет";
                } // Участник44

                if($arr_result['total']['sup223']) {
                    $arr_update_stat['f14081'] = "Да";
                } else {
                    $arr_update_stat['f14081'] = "Нет";
                } //Участник223

                foreach($arr_activity as $category=>$limit) {
                    if ($limit >= $arr_result['total']['avg_count_per_month']) {
                        $arr_update_stat['f14501'] = $category; // Категория по активности
                        break;
                    }
                }

                //$arr_update_stat['f14511'] =
                foreach($arr_size as $category=>$limit) {
                    if ($limit >= (float)($arr_result['win']['avg_sum_prot'])) {
                        $arr_update_stat['f14511'] = $category; // Категория по размеру
                        break;
                    }
                }

                // проверяем не участвует ли клиент без нас
                $dt_client_type = $row_client_data['f12921']; // тип: Потенциальный, Клиент, Партнер;
                if($dt_client_type == "Клиент"){
                    if(UpdateWorkWithNPO($clientId, $cb_tables, $dt_start, $debug)) {
                        $row_client_data = data_select_array($cb_tables['tableClientsData'],"status=0 and f10410=".$clientId);
                    }
                }

                $arr_update_stat['f14661'] = "Да"; // Участвует без нас

                // получить данные из статистики
                $dt_last_complex = $row_client_data['f14681']; // Дата последнего заказа ТЗ
                $dt_last_app = $row_client_data['f14671']; // Дата последнего заказа заявки

                if($dt_client_type == "Клиент" && ($dt_last_complex<>"0000-00-00 00:00:00" || $dt_last_app<>"0000-00-00 00:00:00")) {
                    $bWithUs = false;
                    if ($dt_last_complex <> "0000-00-00 00:00:00") {
                        $dt_calc = $dt_last_complex;
                        $dt_limit = $days_delay_complex;
                        $seconds_diff = strtotime($arr_result['total']['max_date']) - strtotime($dt_calc);
                        $days_diff = floor($seconds_diff / 86400.0);
                        if ($days_diff < $dt_limit) {
                            $bWithUs = true;
                        }
                    }

                    if ($dt_last_app != "0000-00-00 00:00:00") {
                        $dt_calc = $dt_last_app;
                        $dt_limit = $days_delay_apps;
                        $seconds_diff = strtotime($arr_result['total']['max_date']) - strtotime($dt_calc);
                        $days_diff = floor($seconds_diff / 86400.0);
                        if ($days_diff < $dt_limit) {
                            $bWithUs = true;
                        }
                    }

                    if ($bWithUs) {
                        $arr_update_stat['f14661'] = "Нет";
                    } //Участвует без нас
                }

                data_update($cb_tables['tableClientsData'], $arr_update_stat, "status=0 and f10410=".$clientId);

                $arr_update_client = array();
                $arr_update_client['f4881'] =  $arr_update_stat['f14501']; // Категория по активности
                $arr_update_client['f4891'] =  $arr_update_stat['f14511']; // Категория по размеру
                $arr_update_client['f20481'] =  $date_update; // Дата обновления статистики
                data_update($cb_tables['tableClients'], $arr_update_client, "status=0 and id=".$clientId);

                $result = true;
            }

        } // end if $sqlConnectId

    } // end if trim inn

    if($bDoConnect) {
        sqlsrv_close($sqlConnectId);
    }

    return $result;
} // end function UpdateParticipations

// -----------------------------------------------------------
// обновление данные по работе с клиентом
//  $dt_start - дата с которой загружена статистика участий в базу, чтобы сравнивать число ТЗ и заявок
// -----------------------------------------------------------
function UpdateWorkWithNPO($clientId, $cb_tables, $dt_start="2015-04-01 00:00:00", $debug=false)
{
    if($debug) echo "UpdateWorkWithNPO() start<br>\r\n";

    if($clientId=="") return false; // если id клиента не передан, то выходим

    $tableOrders = $cb_tables['tableOrders'];
    $tableOrdersSearch = $cb_tables['tableOrdersSearch'];
    $tableInvoices = $cb_tables['tableInvoices'];
    $tableIncomes = $cb_tables['tableIncomes'];
    $tableActs = $cb_tables['tableActs'];
    $tableIncomeInvoiceOrders = $cb_tables['tableIncomeInvoiceOrders'];

    if($clientId>0)
    {
        $arr_client_data = array();
        // считаем сумму заказов
        $result = data_select_field($tableOrders , "SUM(`f4461`) AS sum", "f4441=".$clientId." and status=0");
        $row = sql_fetch_assoc($result);
        $arr_client_data['f12881'] = $row['sum']; // Выполнено заказов

        if($arr_client_data['f12881']>0) // Выполнено заказов
        {
            // считаем сумму поступлений в привызяке к заказам
            $result = data_select_field($tableIncomeInvoiceOrders  , "SUM(`f10920`) AS sum", "f11140=".$clientId." and status=0");
            $row = sql_fetch_assoc($result);
            $arr_client_data['f13011'] = $row['sum']; // Привязанно поступлений

            $arr_client_data['f12931'] = $arr_client_data['f12881'] - $arr_client_data['f13011']; // Сальдо (Заказы/Деньги)

            // -------------------------------------------------
            // считаем сумму безналичных поступлений от фактической компании
            $result = data_select_field($tableIncomes , "SUM(`f7611`) AS sum", "f7531=".$clientId." and f7621='Безналичные' and status=0");
            $row = sql_fetch_assoc($result);
            $arr_client_data['f12891'] = $row['sum']; // Получено безнал

            // считаем сумму актов на фактическую компанию
            $result = data_select_field($tableActs , "SUM(`f874`) AS sum", "f871=".$clientId." and status=0");
            $row = sql_fetch_assoc($result);
            $arr_client_data['f12901'] = $row['sum']; // Закрыто актов

            $arr_client_data['f12911'] = $arr_client_data['f12901'] - $arr_client_data['f12891']; // Сальдо (Деньги/Акты)

            // -------------------------------------------------
            // вычисляем дату первого заказа
            $result = data_select_field($tableOrders, "MIN(`f6591`) AS dt", "f4441=".$clientId." and status=0");
            $row = sql_fetch_assoc($result);
            $arr_client_data['f11110'] = $row['dt']; // Дата первого заказа

            // вычисляем дату последего заказа
            $result = data_select_field($tableOrders, "MAX(`f6591`) AS dt", "f4441=".$clientId." and status=0");
            $row = sql_fetch_assoc($result);
            $arr_client_data['f11120'] = $row['dt']; // Дата последнего заказа

            // вычисляем дату последего заказа заявки
            $result = data_select_field($tableOrders, "MAX(`f6591`) AS dt", "f4441=".$clientId." and status=0 and f4451=5");
            $row = sql_fetch_assoc($result);
            $arr_client_data['f14671'] = $row['dt']; // Дата последнего заказа заявки

            // вычисляем дату последего заказа ТЗ
            $result = data_select_field($tableOrders, "MAX(`f6591`) AS dt", "f4441=".$clientId." and status=0 and f4451=7");
            $row = sql_fetch_assoc($result);
            $arr_client_data['f14681'] = $row['dt']; // Дата последнего заказа ТЗ

            // вычисляем дату последего поиска
            $result = data_select_field($tableOrdersSearch,
                "MAX(`f6951`) AS dt", "f10190=".$clientId." and status=0");
            $row = sql_fetch_assoc($result);
            $arr_client_data['f14691'] = $row['dt']; // Дата завершения последнего поиска

            // вычисляем дату первого платежа
            $res = data_select($tableIncomeInvoiceOrders,
                'f11140='.$clientId.' and status=0 ORDER BY f11271 ASC LIMIT 1');
            $row = sql_fetch_assoc($res);
            if($row['id']>0) $arr_client_data['f13811'] = $row['f11271']; // Дата первого платежа

            // вычисляем кол-во заказанных заявок и ТЗ (с даты сравнения статистики)
            $result = data_select_field($tableOrders, "COUNT(id) AS count_id",
                "f4441=".$clientId." and status=0 and f4451 in (5,7) and f6591>='".$dt_start."' and f6551='30. Завершено'");
            $row = sql_fetch_assoc($result);
            $arr_client_data['f14651'] = $row['count_id']; // Заказано заявок

            // вычисляем сумму заказанных заявок (с даты сравнения статистики)
            $result = data_select_field($tableOrders, "SUM(f4461) AS sum_app",
                "f4441=".$clientId." and status=0 and f4451=5 and f6591>='".$dt_start."' and f6551='30. Завершено'");
            $row = sql_fetch_assoc($result);
            $arr_client_data['f14701'] = $row['sum_app']; // Заказано заявок (руб)

            // вычисляем сумму заказанных поисков (с даты сравнения статистики)
            $result = data_select_field($tableOrders, "SUM(f4461) AS sum_search",
                "f4441=".$clientId." and status=0 and f4451=4 and f6591>='".$dt_start."'");
            $row = sql_fetch_assoc($result);
            $arr_client_data['f14611'] = $row['sum_search']; // Заказано поиска (руб)

            // вычисляем сумму заказанных комплексных заказов
            $result = data_select_field($tableOrders, "SUM(f4461) AS sum_complex",
                "f4441=".$clientId." and status=0 and f4451=7 and f6591>='".$dt_start."' and f6551='30. Завершено'");
            $row = sql_fetch_assoc($result);
            $arr_client_data['f20491'] = $row['sum_complex']; // Заказано комплексных(руб)

            // вычисляем сумму заказанных прочих заказов
            $result = data_select_field($tableOrders, "SUM(f4461) AS sum_other",
                "f4441=".$clientId." and status=0 and f4451 not in (4,5,7) and f6591>='".$dt_start."' and f6551='30. Завершено'");
            $row = sql_fetch_assoc($result);
            $arr_client_data['f20501'] = $row['sum_other']; // Заказано комплексных(руб)

            if($debug) echo "              UpdateWorkWithNPO arr_client_data:<br>\r\n";
            if($debug) print_r($arr_client_data);

            data_update($cb_tables['tableClientsData'], $arr_client_data, "status=0 and f10410=".$clientId);

            return true;
        }
    }
    return false;
}

// проставить район ИФНС по ИНН
function UpdateClientIFNS($clientId, $cb_tables, $debug=false)
{
    // f14901 - ИФНС факт
    // f8990 - ИФНС регистрация
    // f1056 - ИНН
    // f1057 - КПП
    // f4911 - налоговая (текстовый список)
}

// назначить менеджера продаж по ИФНС фактического положения
function UpdateClientManagerByIFNS($clientId, $cb_tables, $debug=false)
{

    // f438 - менеджер продаж (пользователь)
    // f7811 - менеджер продаж НПО (справочник сотрудников)
}

?>