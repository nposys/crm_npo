<?php

require_once "add/functions/service_functions.php";

// ==================================================================
// автосоздание работы по заказу
//  требуется чтобы запись заказа уже была сохранена при вызове, иначе задача автоудаляется
//  настраивается в свойствах таблицы Заказы
// ==================================================================
function auto_create_job($data, $cb_tables)
{
    $orderId = $data['order_id'];
    $orderNum = $data['order_num'];
    $serviceId = $data['service_id'];
    $priceId = $data['price_id'];

    $orderSum = $data['order_sum'];
    $orderPriority = $data['order_priority'];
    $orderStatus = $data['order_status'];
    $productionStatus = $data['production_status'];

    // если есть заказ и никаикх работ не создано - поле f7071 есть только в таблице заказов
    if ($orderId <> "" && $orderNum <> "" && $serviceId <> "" && $priceId <> "" && $cb_tables['tableOrderJobs'] <> "") {
        if ($serviceId == 7) return;

        $row_job = data_select_array($cb_tables['tableOrderJobs'], "status=0 and f12601=$orderId");

        if ($row_job['id'] == "") {
            $row_job_type = data_select_array($cb_tables['tableJobTypes'], "status=0 and f12761=$serviceId and f13021='Да'");

            // добавить проверку, что менеджер не уволен
            // добавить проверку по табелю что менеджер работает , если в этот день не работает, то выбираем следующего
            $managerFound = false;
            $userId = "";
            $res_m2j = data_select($cb_tables['tableJobMatrix'], "status=0 and f16290=$priceId order by f16310 ASC");
            while ($row_manager2job = sql_fetch_assoc($res_m2j)) {
                $managerId = $row_manager2job['f16300'];
                if ($managerId) {
                    $res_manager = data_select_array($cb_tables['tablePersonal'], "status=0 and id=$managerId");
                    if ($res_manager['f7941'] == "Да") {
                        $managerFound = true;
                        $userId = $res_manager['f1400'];
                        break;
                    }
                }
            }

            // выбираем автоматически менеджера с первым приоритетом
            if ($managerFound && $serviceId && $priceId && $row_job_type['id'] <> "" && $row_manager2job['id'] <> "") {
                //$managerId = $row_manager2job['f16300'];
                $jobTypeId = $row_job_type['id'];
                $jobDuration = $row_job_type['f15511'];
                $jobStatus = '10. Запланировано';
                $jobDescription = 'Начать работу над заказом (автодобавление)';

                $managerPositionId = $res_manager['f484'];
                $managerDepartementId = $res_manager['f12021'];

                $job = array();
                $job['status'] = 0;
                $job['f12601'] = $orderId;
                $job['f12661'] = $userId;
                $job['f12861'] = $jobTypeId;
                $job['f12671'] = $jobDescription;
                $job['f12631'] = $jobStatus;

                $job['f14741'] = "Обычная"; // важность

                $job['f14481'] = $priceId;
                $job['f14711'] = $orderSum;
                $job['f12961'] = $orderPriority;
                $job['f15801'] = $orderStatus;
                $job['f14721'] = $productionStatus;

                $job['f15741'] = $managerPositionId;
                $job['f15771'] = $managerDepartementId;

                $job['f13591'] = $jobDuration;
                $job['f12981'] = date('Y-m-d H:i:s'); // date start plan
                $job['f12991'] = date('Y-m-d H:i:s', strtotime($job['f12981'] . " + $jobDuration minute ")); // date start end

                $new_job_id = data_insert($cb_tables['tableOrderJobs'], $job);

                //die($new_job_id);

                return $new_job_id;
            }
        }
    }
} // end function auto_create_job

// ==================================================================
// формирование папки заказа
// ==================================================================

function CreateOrderPath($data, $cb_tables, $bUpdateTable = true)
{
    $debug = false;

    $user_id = $data['user_id'];
    $order_id = $data['order_id'];
    $client_id = $data['client_id'];
    $order_path = mb_trim($data['order_path']);
    $client_path_folder = mb_trim($data['client_path_folder']);
    $order_date = $data['order_date'];
    $order_num = $data['order_num'];
    $order_desciption = mb_trim($data['order_description']);
    $service_num = intval($data['service_num']);

    if ($debug) write_log("CreateOrderPath begin: order_num:$order_num order_desciption:$order_desciption order_date:$order_date client_id:$client_id order_id:$order_id order_path:[$order_path] user_id:$user_id", true, true);

    if ($order_date == "" || $order_num == "" || $order_desciption == "" || $client_id == "" || $order_id == "") {
        if ($debug) write_log("CreateOrderPath not enough data. Exit.", true, true);
        return;
    }

    // если в это поле вписали пробел, значит запрашивается формирование папки заказа
    // если же в поле вписали вручную какой-то текст, то ничего не формируем
    if ($order_path == "") {
        if ($debug) write_log("CreateOrderPath order path is empty, try to create", true, true);

        $strName = $order_desciption;

        // удаляем спецсимволы из названия папки
        $pattern = "[" . preg_quote('\/»«"!*:|></?+.,\'') . "]";
        $strName = mb_ereg_replace($pattern, "", $strName);
        $strName = mb_trim(mb_substr($strName, 0, 15));

        $bEnoughData = true; // переменная для проверки все ли данные в пути сфомированы (например, если нет GovRuID, то путь не формируем для заявки)
        $bAlreadyCreated = false;

        // если у клиента не прописана папка, то запускаем ее формирование сначала
        if ($client_path_folder == "") {
            // $line['Клиент']['PathFolder'] .= " ";
            $row_client = data_select_array($cb_tables['tableClients'], "id=$client_id");

            $data_client = array();
            $data_client['client_id'] = $row_client['id'];
            $data_client['client_path'] = $row_client['f11731']; // Папка клиента
            $data_client['path_folder'] = $row_client['f10080']; // PathFolder
            $data_client['name'] = $row_client['f435']; // Название

            $client_path_folder = CreateClientPath($data_client, $cb_tables, false);
        }

        // если папка сформировалась
        if ($client_path_folder <> "") {
            $strClientPath = $client_path_folder;
            $strPathPrefix = "\\\\192.168.0.9\\Common\\Data\\Клиенты\\" . $strClientPath . "\\30. Заказы\\";
            $strPathPrefixUnix = "/mnt/common/Data/Клиенты/" . $strClientPath . "/30. Заказы/";

            $arrPrefixes = array
            (
                1 => '50. ЭЦП', // ЭЦП
                2 => '55. Настройка рабочего места',// Настройка рабочего места
                3 => '40. Аккредитация',// Аккредитация
                4 => '05. Поиск',// Поиск
                5 => '10. Заявки',// Подготовка заявки
                6 => '20. Жалобы',// Жалоба
                7 => '60. Подготовка документации',// Подготовка документации
                8 => '30. Обеспечение контракта',// Обеспечение контракта
                9 => '70. Прочие услуги',// Прочие услуги
                11 => '81. Подготовка документа заказчика',
                12 => '82. Сопровождение закупки заказчика',
                17 => '75. Регистрация в ЕИС'
            );

            $arrFoldersApp = array
            (
                '10 Документация',
                '20 Расчет',
                '30 Заявка',
                '40 Исполнение',
                '50 Запросы'
            );
            $arrFoldersComplaints = array
            (
                '10 Документация',
                '20 Жалоба',
                '30 Решение'
            );
            $arrFoldersComplex = array
            (
                '01 Договоры',
                '05 От клиента',
                '10 В работе',
                '20 На отправку'
            );

            $arrFoldersBG = array
            (
                '10 От клиента',
                '20 От банка'
            );

            $year = substr($order_date, 0, 4);
            $month = substr($order_date, 5, 2);
            $dateOrder = substr($order_date, 0, 10);

            // создаем разные папки для разных типов услуг
            switch ($service_num) {
                case 5: // Подготовка заявки
                    $rowOrderApp = data_select_array($cb_tables['tableOrdersApp'], "status=0 and f5471=$order_id");
                    $strPrefix = $order_num;
                    $strSufix = mb_trim($rowOrderApp['f7441']); // получаем GovRuID

                    if ($strSufix == "") $strSufix = mb_trim($strName);

                    if (mb_trim($strSufix) <> "") {
                        $strPathServ = $arrPrefixes[$service_num] . "/" . $year . "/" . $month . "/" . $strPrefix . " " . $dateOrder . " " . $strSufix . "/";

                        // если папка заказа не существует, то создаем структуру (и саму папку в том числе)
                        if (!file_exists($strPathPrefixUnix . $strPathServ)) {
                            foreach ($arrFoldersApp as $strPath)
                                if (mkdir($strPathPrefixUnix . $strPathServ . $strPath, 0777, true)) if (!$bAlreadyCreated) $bAlreadyCreated = true;
                        }
                    } else {
                        $bEnoughData = false;
                    }

                    break;
                case 6: // Подготовка жалобы
                    $rowOrderApp = data_select_array($cb_tables['tableOrdersComplaints'], "status=0 and f5541=$order_id");
                    $strPrefix = $order_num;
                    $strSufix = mb_trim($rowOrderApp['f8140']);  // получаем GovRuID
                    if ($strSufix == "") $strSufix = mb_trim($strName);

                    if (mb_trim($strSufix) <> "") {
                        $strPathServ = $arrPrefixes[$service_num] . "/" . $year . "/" . $month . "/" . $strPrefix . " " . $dateOrder . " " . $strSufix . "/";

                        // если папка заказа не существует, то создаем структуру (и саму папку в том числе)
                        if (!file_exists($strPathPrefixUnix . $strPathServ)) {
                            foreach ($arrFoldersComplaints as $strPath)
                                if (mkdir($strPathPrefixUnix . $strPathServ . $strPath, 0777, true)) if (!$bAlreadyCreated) $bAlreadyCreated = true;
                        }
                    } else {
                        $bEnoughData = false;
                    }
                    break;
                case 7: // Подготовка документации
                    $strPrefix = $order_num;
                    $strSufix = mb_trim($strName);

                    $strPathServ = $arrPrefixes[$service_num] . "/" . $year . "/" . $month . "/" . $strPrefix . " " . $dateOrder . " " . $strSufix . "/";

                    // если папка заказа не существует, то создаем структуру (и саму папку в том числе)
                    if (!file_exists($strPathPrefixUnix . $strPathServ)) {
                        foreach ($arrFoldersComplex as $strPath)
                            if (mkdir($strPathPrefixUnix . $strPathServ . $strPath, 0777, true)) if (!$bAlreadyCreated) $bAlreadyCreated = true;
                    }

                    break;
                case 8: // Банковская гарантия
                    $rowOrderBg = data_select_array($cb_tables['tableOrdersBg'], "status=0 and f5751=$order_id");
                    $strPrefix = $order_num;
                    $strSufix = mb_trim($rowOrderBg['f8150']); // получаем GovRuID
                    if ($strSufix == "") $strSufix = mb_trim($strName);

                    if (mb_trim($strSufix) <> "") {
                        $strPathServ = $arrPrefixes[$service_num] . "/" . $year . "/" . $month . "/" . $strPrefix . " " . $dateOrder . " " . $strSufix . "/";
                        // если папка заказа не существует, то создаем структуру (и саму папку в том числе)
                        if (!file_exists($strPathPrefixUnix . $strPathServ)) {
                            foreach ($arrFoldersBG as $strPath)
                                if (mkdir($strPathPrefixUnix . $strPathServ . $strPath, 0777, true)) if (!$bAlreadyCreated) $bAlreadyCreated = true;
                        }
                    } else {
                        $bEnoughData = false;
                    }
                    break;
                default:
                    $strPrefix = mb_trim($order_num);
                    $strSufix = mb_trim($strName);
                    $bEnoughData = true;
                    // структура подпапок не требуется
                    break;
            }

            if ($bEnoughData) {
                if ($debug) write_log("CreateOrderPath enough data to create folder. Try it.", true, true);

                $order_folder = mb_trim($strPrefix . " " . $dateOrder . " " . $strSufix);
                if (isset($arrPrefixes[$service_num]) || $arrPrefixes[$service_num] != '') {
                    $strPathServ = $arrPrefixes[$service_num] . "/" . $year . "/" . $month . "/" . $order_folder . "/";
                } else {
                    $strPathServ = $year . "/" . $month . "/" . $order_folder . "/";
                }

                if (!$bAlreadyCreated) {
                    if ($debug) write_log("CreateOrderPath folder is not created earlier: $strPathPrefixUnix and $strPathServ", true, true);

                    if (file_exists($strPathPrefixUnix . $strPathServ))
                        $bAlreadyCreated = true;
                    else
                        if (mkdir($strPathPrefixUnix . $strPathServ, 0777, true))
                            $bAlreadyCreated = true;
                }

                if ($bAlreadyCreated) {
                    if ($debug) write_log("CreateOrderPath create folder success.", true, true);

                    if ($bUpdateTable) {
                        $final_order_path = $strPathPrefix . str_replace('/', '\\', $strPathServ);
                        data_update($cb_tables['tableOrders'], array("f6621" => $final_order_path), "id=$order_id");
                    }
                    return $strPathPrefix . str_replace('/', '\\', $strPathServ);
                } else {
                    if ($debug) write_log("CreateOrderPath folder not already created. Exit.", true, true);
                    return;
                }

            } else {
                // "not enough data";
                if ($debug) write_log("CreateOrderPath not enough data to create folder. Exit.", true, true);
                return;
            }

        } // end if Client PathFolder is not null
    } // end if trim пустая папка заказа
    else {
        if ($debug) write_log("CreateOrderPath order path is not empty. Exit without work.", true, true);
    }
}

// ================================================================
// формирование вех по заказу
// ==================================================================

function CreateOrderMilestones($data, $cb_tables)
{
    $order_id = $data['id'];
    $service_type_id = $data['f4451'];
    $price_type_id = $data['f13071'];
    $client_id = $data['f4441'];
    $order_name = $data['f4431'];
    $manager_client_id = $data[''];
    $status_order = $data['f6551'];
    $status_production = $data['f10240'];

    switch ($service_type_id) {
        case 5: // подготовка заявки
            $milestones = array();

            if ($row_app = data_select_array($cb_tables['tableOrdersApp'], "status=0 and f5471=" . $order_id)) {
                $tender_id = $row_app['f5481'];

                if ($tender_id <> '') {
                    if ($row_tender = data_select_array($cb_tables['tableTenders'], "status=0 and id=" . $tender_id)) {
                        $govruid = $row_tender['f5041'];
                        $lotnum = $row_tender['f6391'];

                        $date_podacha = $row_tender['f13971'];
                        $date_rassmotrenie = $row_tender['f14021'];
                        $date_torgi = $row_tender['f13981'];

                        if ($date_podacha <> '0000-00-00 00:00:00')
                            $milestones[] = array('type' => 'Подача заявки', 'date' => $date_podacha);

                        if ($date_torgi <> '0000-00-00 00:00:00' && $date_torgi <> $date_podacha) {
                            $milestones[] = array('type' => 'Торги', 'date' => $date_torgi);
                        }

                        if ($date_torgi <> '0000-00-00 00:00:00') {
                            $date_contract = date('Y-m-d H:i:s', strtotime($date_torgi . " +5 day"));
                            $milestones[] = array('type' => 'Подписание контракта', 'date' => $date_contract);
                        }


                        foreach ($milestones as $milestone) {
                            if ($row_milestone = data_select_array($cb_tables['tableOrdersMilestones'],
                                "status=0 and f15471=" . $order_id . " and f15481='" . $milestone['type'] . "'")) {
                                $arr_milestone = array(
                                    "f15491" => $milestone['date']
                                );
                                data_update($cb_tables['tableOrdersMilestones'], $arr_milestone, "id=" . $row_milestone['id']);
                            } else {
                                $arr_milestone = array(
                                    "status" => 0,
                                    "f15471" => $order_id,
                                    "f15481" => $milestone['type'],
                                    "f15491" => $milestone['date'],
                                    "f15761" => 'Нет',
                                    'f15531' => $client_id,
                                    'f15541' => $order_name,
                                    'f15551' => $service_type_id,
                                    'f15561' => $price_type_id,
                                    'f15581' => $manager_client_id,
                                    'f15591' => $status_order,
                                    'f15601' => $status_production
                                );
                                data_insert($cb_tables['tableOrdersMilestones'], $arr_milestone);
                            }
                        }
                    }

                }
            }

            break;
        case 7: // подготовка документации
            if ($row_app = data_select_array($cb_tables['tableOrdersCmplx'], "status=0 and f5621=" . $order_id)) {
                $tender_id = $row_app['f5821'];

                if ($tender_id <> '') {
                    if ($row_tender = data_select_array($cb_tables['tableTenders'], "status=0 and id=" . $tender_id)) {
                        $govruid = $row_tender['f5041'];
                        $lotnum = $row_tender['f6391'];

                        $date_podacha = $row_tender['f13971'];
                        $date_rassmotrenie = $row_tender['f14021'];
                        $date_torgi = $row_tender['f13981'];

                        if ($date_podacha <> '0000-00-00 00:00:00')
                            $milestones[] = array('type' => 'Подача заявки', 'date' => $date_podacha);

                        if ($date_rassmotrenie <> '0000-00-00 00:00:00')
                            $milestones[] = array('type' => 'Протокол 1Ч', 'date' => $date_rassmotrenie);


                        if ($date_torgi <> '0000-00-00 00:00:00') {
                            $date_contract = date('Y-m-d H:i:s', strtotime($date_torgi . " +5 day"));

                            $milestones[] = array('type' => 'Торги', 'date' => $date_torgi);
                            $milestones[] = array('type' => 'Подписание контракта', 'date' => $date_contract);
                        }


                        foreach ($milestones as $milestone) {
                            if ($row_milestone = data_select_array($cb_tables['tableOrdersMilestones'],
                                "status=0 and f15471=" . $order_id . " and f15481='" . $milestone['type'] . "'")) {
                                $arr_milestone = array(
                                    "f15491" => $milestone['date']
                                );
                                data_update($cb_tables['tableOrdersMilestones'], $arr_milestone, "id=" . $row_milestone['id']);
                            } else {
                                $arr_milestone = array(
                                    "status" => 0,
                                    "f15471" => $order_id,
                                    "f15481" => $milestone['type'],
                                    "f15491" => $milestone['date'],
                                    "f15761" => 'Нет',
                                    'f15531' => $client_id,
                                    'f15541' => $order_name,
                                    'f15551' => $service_type_id,
                                    'f15561' => $price_type_id,
                                    'f15581' => $manager_client_id,
                                    'f15591' => $status_order,
                                    'f15601' => $status_production
                                );
                                data_insert($cb_tables['tableOrdersMilestones'], $arr_milestone);
                            }
                        }
                    }

                }
            }
            break;
    }
}

// ================================================================
// обновление вех по заказу
// ==================================================================

function UpdateOrderMilestones($line, $cb_tables)
{
    $order_id = $line['id'];
    $client_id = $line['f4441'];
    $order_name = $line['f4431'];
    $service_type_id = $line['f4451'];
    $price_type_id = $line['f13071'];
    $manager_client_id = $line[''];
    $status_order = $line['f6551'];
    $status_production = $line['f10240'];

    $arr_milestone = array(
        'f15531' => $client_id,
        'f15541' => $order_name,
        'f15551' => $service_type_id,
        'f15561' => $price_type_id,
        'f15581' => $manager_client_id,
        'f15591' => $status_order,
        'f15601' => $status_production
    );

    if ($order_id <> "" && $cb_tables['tableOrdersMilestones'] <> "") {
        data_update($cb_tables['tableOrdersMilestones'], $arr_milestone, 'f15471=' . $order_id);
    }
}


?>
