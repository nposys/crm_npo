<?php
/*
 * функции работы с вакансиями и почтой job
 */

require_once 'add/functions/common_job_functions.php';
require_once 'add/functions/service_functions.php';
require_once dirname(__DIR__) . '/vendor/autoload.php';

// https://github.com/ddeboer/imap#messages
// "ddeboer/imap": "1.5.5", last version to support php 7.0
use Ddeboer\Imap\Server;
use Ddeboer\Imap\Search\Flas\Unseen;
use Ddeboer\Imap\Message;
use Ddeboer\Imap\Message\EmailAddress;

/*
 * получаем письма от кандидата
 */
function CheckJobMail()
{
    // ===========================
    $result = [];

    write_log("CheckJobMail begin", true);

    // заполняем массив вероятных названий вакансий
    $vacanciesData = GetAllOpenVacanies();

    $server = new Server(
        "nposys.ru", // server
        143,     // port
        'novalidate-cert' // flags
    );

    $connection = $server->authenticate("job@nposys.ru", "3K0d9E4c");
    $mailbox = $connection->getMailbox('INBOX');

    write_log("JobMail: Connect to IMAP successful", true);

    //получаем сообщения

    $messages = $mailbox->getMessages();

    write_log("JobMail: Get messages successful", true);

    $numNewMessages = 0;
    $numUpdateMessages = 0;
    $numDeletedMessages = 0;

    try{
        foreach ($messages as $message) {
            // read message data
            try {

                // пропускаем все собщения старше 30 дней
                $messageDateRaw = $message->getDate();
                $messageDate = (is_object($messageDateRaw) ? $messageDateRaw->format('Y-m-d H:i:s') : "");
                $dateNow = new DateTime("now");
                $interval = date_diff($dateNow, $messageDateRaw);
                if($interval->days > 30) {
                    // удаляем сообщения старше полугода
                    if($interval->days > 183) {
                        //$message->delete();
                        $numDeletedMessages++;
                    }
                    continue;
                }

                // проблемы с обработкой почты сохранились, поэтому пропускаем письма от служебных сервисов
                // $messageAddress = new Ddeboer\Imap\Message\EmailAddress();
                $messageAddress = $message->getFrom();
                $messageFrom = trim($messageAddress->getAddress());

                if ($messageFrom == "Mailer-Daemon@nposys.ru"){
                    continue;
                }

                $messageId = $message->getId();
                $messageNumber = $message->getNumber();

                // проверяем нет ли уже этого письма в CRM
                $rowMessageInCrm = data_select_array(950, "status=0 and f16640='$messageId'");

                // если нет, то загружаем письмо в CRM
                if (!$rowMessageInCrm['id']) {

                    $rowMail = [];

                    $matchesName = [];
                    $matchesEmail = [];
                    $matchesPhone = [];

                    $messageSubject = $message->getSubject();
                    $messageText = $message->getBodyHtml();
                    if (trim($messageText) == ""){
                        $messageText = $message->getBodyText();
                    }

                    $messageSize = $message->getSize();
                    $messageIsSeen = $message->isSeen();

                    // парсинг данных с сайта Zarplata.ru
                    if ($messageFrom == "noreply@mail.zarplata.ru") {
                        // библиотека ddeboer не умеет правильно парсить boundary  в тексте сообщения, поэтом декодируем вручную

                        // $matches = array();
                        // preg_match("/\-\-_=_body_.*: base64\s(.*)\-\-_=_body_.*/siU", $messageText, $matches);
                        // $messageText = base64_decode($matches[1]);

                        preg_match("/Имя:\s[^>]*(.*)\s[^>]*<br\/>/siU", $messageText, $matchesName);
                        preg_match("/E\-mail:\s[^>]*(.*)\s[^>]*<br\/>/siU", $messageText, $matchesEmail);
                        preg_match("/Телефон:\s[^>]*(.*)<br\/>/siU", $messageText, $matchesPhone);
                        //preg_match("/Поступил новый отклик на вашу вакансию:\s[^>]*(.*)<br\/>/siU", $messageText, $matchesVacancy);
                    }

                    // парсинг данных с сайта Rabota.ru
                    if ($messageFrom == "noreply@mailer.rabota.ru") {
                        preg_match("/<a\s[^>]*href=\"http[s]?:\/\/[nsk|www]+.rabota\.ru\/resume[^>]*\"[^>]*>(.*)<\/a>/siU", $messageText, $matchesName);
                        preg_match("/<a\s[^>]*href=\"mailto:[^>]*\"[^>]*>(.*)<\/a>/siU", $messageText, $matchesEmail);
                        preg_match("/<font\s[^>]*style=\"font\-size:14px;\"[^>]*>Тел\.(.*),<br>e\-mail:/siU", $messageText, $matchesPhone);
                    }

                    $candidateName = trim($matchesName[1]);
                    $candidateEmail = trim($matchesEmail[1]);
                    $candidatePhone = trim($matchesPhone[1]);

                    $rowMail['f16640'] = $messageId;
                    $rowMail['f16590'] = $messageNumber;
                    $rowMail['f16650'] = $messageDate;
                    $rowMail['f16600'] = $messageFrom;
                    $rowMail['f16620'] = $messageSubject;
                    $rowMail['f17430'] = $messageText;
                    $rowMail['f16630'] = $messageSize;
                    $rowMail['f17440'] = 'Нет'; // ($messageIsSeen ? 'Да' : 'Нет');

                    // автоопределение вакансии
                    $rowMail['f17460'] = GetVacancyTitleByMessageSubject($vacanciesData, $messageSubject);
                    $rowMail['f17480'] = GetVacancyIdByMessageSubject($vacanciesData, $messageSubject);

                    $rowMail['f17600'] = $candidateName;
                    $rowMail['f17660'] = ($candidateEmail == "--" ? "" : $candidateEmail);
                    $rowMail['f17670'] = $candidatePhone;

                    // вставляем новую запись
                    $newRowId = data_insert(950, $rowMail);

                    if ($newRowId || true) {
                        // сохраняем вложение
                        $attachments = $message->getAttachments();

                        $numAttachments = 0;
                        $file_names = "";
                        $fios = "";
                        $fios_array = [];

                        foreach ($attachments as $attachment) {
                            $file_name = $attachment->getFilename();
                            $file_size = $attachment->getSize();

                            $fio = "";
                            $extensionPosition = strrpos($file_name, ".");
                            if ($extensionPosition > 0) {
                                $fio = trim(substr($file_name, 0, $extensionPosition));
                                $fios_array[] = $fio;
                            }

                            $messageAttachmentFieldId = '17450'; //id поля, где расположено название файла в таблице "Почта Job"
                            $messageAttachmentContent = $attachment->getDecodedContent();
                            save_data_file($messageAttachmentFieldId, $newRowId, $file_name, $messageAttachmentContent);
                            $numAttachments++;

                            if ($numAttachments > 1) {
                                if($fios=="" || !in_array($fio, $fios_array)){
                                    $fios .= $fio." ";
                                }
                                $file_names .= "\r\n" . $file_name;
                            } else {
                                $fios = $fio;
                                $file_names = $file_name;

                                // получаем первый email из xml вложения (.doc) для сайта zarplata.ru
                                if (trim($candidateEmail) == "--" && $messageFrom == "noreply@mail.zarplata.ru") {
                                    //$pattern="/[A-Za-z0-9_-]+@[A-Za-z0-9_-]+\.([A-Za-z0-9_-][A-Za-z0-9_]+)/";
                                    $pattern = "/(?:[A-Za-z0-9!#$%&'*+=?^_`{|}~-]+(?:\.[A-Za-z0-9!#$%&'*+=?^_`{|}~-]+)*|\"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*\")@(?:(?:[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?\.)+[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[A-Za-z0-9-]*[A-Za-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/";

                                    preg_match_all($pattern, $messageAttachmentContent, $matches);
                                    if (is_array($matches)) {
                                        if (is_array($matches[0])) {
                                            $candidateEmail = $matches[0][0];
                                            data_update(950, EVENTS_ENABLE,
                                                ['f17660' => $candidateEmail], "id=$newRowId");
                                        }
                                    }
                                }
                            }
                        }

                        if ($numAttachments > 0) {
                            data_update(950, EVENTS_ENABLE,
                                [
                                    'f17610' => $numAttachments,
                                    'f17450' => $file_names,
                                    'f17600' => trim($fios)]
                                ,
                                "id=$newRowId");
                        }

                        // автоматически обрабатываем кандидата если он с известных сайтов и все поля автоматически заполнились
                        if (
                            ($messageFrom == "noreply@mailer.rabota.ru" || $messageFrom == "noreply@mail.zarplata.ru")
                            && ($candidateName <> "" && $candidateEmail <> "" && GetVacancyTitleByMessageSubject($vacanciesData, $messageSubject) <> "")
                        ) {
                            $rowMailTemp = data_select_array(950, "id=$newRowId");
                            ProcessCandidateMail($rowMailTemp);
                        }

                        $numNewMessages++;
                    } // if $newRowId
                } else {
                    // если письмо уже есть в CRM
                    if($rowMessageInCrm['f16600'] == "" && $messageFrom <> "") {
                        data_update(950,
                            ["f16600" => $messageFrom],
                            "id=" . $rowMessageInCrm['id']);
                    }

                    // если кандидат не проставлен, то проверяем необходимость обновления полей
                    if ($rowMessageInCrm['f17780'] == 0) {
                        // $messageFrom = trim($message->getFrom());

                        if ($messageFrom <> "") {
                            // ищем кандидата с таким емайлом по полю письма
                            $rowCandidate = data_select_array(721, "status=0 and f12271='$messageFrom'");

                            // если из текста письма получили емайл, то перепроверяем по нему
                            $candidateEmail = trim($rowMessageInCrm['f17660']);
                            if ($rowCandidate['id'] == 0 && $candidateEmail <> ""){
                                $rowCandidate = data_select_array(721, "status=0 and f12271='$candidateEmail'");
                            }

                            // если нашли кандидата с таким емайл, то проставляем связь письма с кандидатом
                            if ($rowCandidate['id'] > 0) {
                                data_update(950, ["f17780" => $rowCandidate['id']], "id=" . $rowMessageInCrm['id']);
                                $numUpdateMessages++;
                            }
                        } // end if messageFrom <>""
                    } // end if messageInCrm candidate link not exist
                }

            } catch (Exception $ex){
                write_log('error with emailId:'.$message->getId().' message: '.$ex->getMessage());
            }
        } // end foreach message

    } catch (Exception $ex){
        write_log('error while reading messages: '.$ex->getMessage());
    }

    $result['new_messages'] = $numNewMessages;
    $result['updated_messages'] = $numUpdateMessages;
    $result['deleted_messages'] = $numDeletedMessages;

    write_log("done CheckJobMail() new:$numNewMessages updated:$numUpdateMessages deleted:$numDeletedMessages", true);

    return $result;

} // end function CheckJobMail()

/*
 * обрабатывае письмо от кандидата
 */
function ProcessCandidateMail($rowMail)
{
    $mailId = $rowMail['id'];

    if ($mailId > 0) {
        $maxNum = GetCandidateMaxNum();

        $messageDate = $rowMail['f16650'];
        $fio = $rowMail['f17600'];
        $messageProcessed = $rowMail['f17440'];
        $vacancyTitle = trim($rowMail['f17460']);
        $vacancyId = $rowMail['f17480'];
        $resumeFileNames = $rowMail['f17450'];
        $senderEmail = $rowMail['f16600'];
        $candidateEmail = $rowMail['f17660'];
        $candidatePhone = $rowMail['f17670'];
        $messageText = $rowMail['f17430'];
        $departmentId = 0;
        $candidateId = $rowMail['f17780'];

        if ($messageProcessed === "Нет") {
            if ($vacancyTitle === "") {
                // ищем нет ли ранее такого email
                $excluded_mail = ['noreply@hh.ru', 'nobody@host.superjob.ru', 'noreply@mail.zarplata.ru', 'noreply@novosibirsk.hh.ru'];
                if (!in_array($senderEmail, $excluded_mail)) {
                    $rowCandidate = data_select_array(721, "status=0 and f12271='$senderEmail' and f12111<>'97. Дубликат'");
                    if ($rowCandidate['id'] > 0) {
                        data_update(950, EVENTS_ENABLE, array("f17780" => $rowCandidate['id']), "id=$mailId");
                    }
                }

                // помечаем письмо как обработанное
                data_update(950, EVENTS_ENABLE, array("f17440" => "Да"), "id=$mailId");
            } else {
                // проверям существует ли уже такой кандидат, а также наличие ссылки на вакансию
                $rowExistingCandidate = data_select_array(721, "status=0 and f17490=$mailId");
                if ($rowExistingCandidate['id'] == 0 && $vacancyId > 0) {
                    $rowVacancy = data_select_array(691, "id=$vacancyId");
                    $departmentId = $rowVacancy['f11911'];

                    $vacancySourceId = GetCandidateSourceBySenderEmail($senderEmail);

                    $rowCandidate = array();
                    $rowCandidate['status'] = 0;
                    $rowCandidate['f12151'] = $maxNum + 1;
                    $rowCandidate['f12101'] = $messageDate; // дата обращения
                    $rowCandidate['f12071'] = (trim($fio) <> "" ? $fio : "НОВЫЙ КАНДИДАТ"); // ФИО
                    $rowCandidate['f12111'] = '10. Резюме получено'; // статус
                    $rowCandidate['f17490'] = $mailId; // ссылка на письмо
                    $rowCandidate['f12371'] = $vacancySourceId; // ссылка на источник кандидата
                    $rowCandidate['f12081'] = $vacancyId;
                    $rowCandidate['f12091'] = $departmentId;
                    $rowCandidate['u'] = 1;
                    $rowCandidate['r'] = 1;

                    if ($candidateEmail <> "") {
                        $rowCandidate['f12271'] = $candidateEmail;
                    }
                    // else if (!($vacancySourceId > 0)) $rowCandidate['f12271'] = $senderEmail; // если по емайл источника вакансии не определился, то вероятно это отправка с почты кандидата

                    $rowCandidate['f12261'] = $candidatePhone;

                    write_log("ProcessCandidateMail: Add new candidate with events: " . $rowCandidate['f12071'], true, true);

                    $newCandidateId = data_insert(721, EVENTS_ENABLE, $rowCandidate);
                    if ($newCandidateId > 0) {
                        $rowInsertedCandidate = data_select_array(721, "id=$newCandidateId");
                        SendWelcomeEmail($rowInsertedCandidate);

                        if ($resumeFileNames <> "") {
                            $arrayFileNames = explode(PHP_EOL, $resumeFileNames);
                            $numAttachments = 0;
                            $fileNames = "";

                            foreach ($arrayFileNames as $fileName) {
                                $fileName = str_replace("\n", "", str_replace("\r", "", trim($fileName)));

                                // копируем вложение
                                $resumeFilePath = get_file_path('17450', $mailId, $fileName);
                                $resumeFileData = file_get_contents($resumeFilePath, true); // загружаем файл в переменную

                                // сохраняем файл в хранилище для таблицы Кандидаты
                                save_data_file('12291', $newCandidateId, $fileName, $resumeFileData);

                                $numAttachments++;
                                if ($numAttachments > 1) {
                                    $fileNames .= "\r\n" . $fileName;
                                } else {
                                    $fileNames = $fileName;
                                }

                            }

                            // обновляем поле с именем файла
                            if ($numAttachments > 0)
                                data_update(721, EVENTS_ENABLE, array('f12291' => $fileNames), "id=$newCandidateId");
                        } else {
                            // прикрепляем текст письма
                            if ($senderEmail == "noreply@mailer.rabota.ru") {
                                preg_match("/<a\s[^>]*href=\"https:\/\/nsk\.rabota\.ru\/resume[^>]*\"[^>]*>(.*)<\/a>/siU", $messageText, $matchesName);
                                $candidateName = trim($matchesName[1]);
                                // сохраняем файл в хранилище
                                $resumeFileName = ($candidateName <> "" ? $candidateName . ".doc" : "Резюме.doc");

                                save_data_file('12291', $newCandidateId, $resumeFileName, $messageText);

                                // обновляем поле с именем файла
                                data_update(721, EVENTS_ENABLE, array('f12291' => $resumeFileName), "id=$newCandidateId");
                            }
                        }

                        // помечаем письмо как обработанное
                        data_update(950, EVENTS_ENABLE, array("f17440" => "Да", "f17780" => $newCandidateId), "id=$mailId");
                    }
                } // if($rowExistingCandidate['id']==0 && $vacancyId>0)

            } // if($vacancyTitle=="")
        } // if($messageProcessed=="Нет")
        else {
            // для обработанных сообщений: если вакансия заполнена, а ссылка на кандидата нет, то проставляем
            if ($vacancyTitle <> "" && $candidateId == 0 && $mailId > 0) {
                $rowCandidate = data_select_array(721, "status=0 and f17490=$mailId");
                data_update(950, EVENTS_ENABLE, array("f17780" => $rowCandidate['id']), "id=$mailId");
            }
        }
    } // if($mailId>0)
}

function GetCandidateSourceBySenderEmail($senderEmail)
{
    $rowSource = data_select_array(741, "status=0 and f17620 like '%$senderEmail%'");
    return $rowSource['id'];
}

function ReplyCandidate($email, $type = "deny")
{
    // типы ответов: типовой отказ, приглашение на собеседование, тестовое задание
    // все это можно настроить шаблонами рассылки, а не городить тут функцию !!!
}

/*
 * проставляем ссылку на вакансию на базе данных о выборе вакансии в статическом селектбоксе
 */
function SetVacancyId($rowMail)
{
    $mailId = $rowMail['id'];
    $vacancyFullTitle = trim($rowMail['f17460']);

    if ($mailId > 0 && $vacancyFullTitle <> "") {
        $vacancyData = GetAllOpenVacanies();
        $vacancyDataArray = explode("/", $vacancyFullTitle);

        $vacancyTitle = trim($vacancyDataArray[0]);
        $vacancyDepartmentCode = trim($vacancyDataArray[1]);
        $vacancySpecializationCode = trim($vacancyDataArray[2]);

        $fullTitle = $vacancyTitle . " / " . $vacancyDepartmentCode;
        if ($vacancySpecializationCode <> "") $fullTitle .= " / " . $vacancySpecializationCode;

        foreach ($vacancyData as $vacancyId => $nextVacancyData) {
            if ($nextVacancyData['common']['full_title'] == $fullTitle)
                data_update(950, array("f17480" => $vacancyId), "id=$mailId");
        }

        // $vacancyId = $vacancyData[$vacancyTitle]['common']['vacancy_id'];
    }
}

/*
 * получаем список всех открытых вакансий и соответствующих им заголовков объявлений
 */
function GetAllOpenVacanies()
{
    $resVacancies = data_select(691, "status=0 and f12421='Да'"); // открытые вакансии

    $vacanciesData = [];

    while ($rowVacancy = sql_fetch_assoc($resVacancies)) {
        $vacancyId = $rowVacancy['id'];
        $positionId = $rowVacancy['f11981'];
        $departemntId = $rowVacancy['f11911'];
        $specializationId = $rowVacancy['f17420'];

        if ($positionId > 0) {
            $rowPosition = data_select_array(261, "id=$positionId");
            $vacancyTitle = $rowPosition['f4331'];

            $rowDepartment = data_select_array(701, "id=$departemntId");
            $departmentTitle = $rowDepartment['f11891'];
            $departmentCode = $rowDepartment['f17500'];

            $rowSpecialization = data_select_array(1030, "id=$specializationId");
            $specializationTitle = $rowSpecialization['f17560'];
            $specializationCode = $rowSpecialization['f17570'];

            $vacanciesData[$vacancyId]['common']['vacancy_id'] = $vacancyId;
            $vacanciesData[$vacancyId]['common']['vacancy_title'] = $vacancyTitle;

            $vacanciesData[$vacancyId]['common']['position_id'] = $positionId;

            $vacanciesData[$vacancyId]['common']['department_id'] = $departemntId;
            $vacanciesData[$vacancyId]['common']['department_title'] = $departmentTitle;
            $vacanciesData[$vacancyId]['common']['department_code'] = $departmentCode;

            $vacanciesData[$vacancyId]['common']['specialization_id'] = $specializationId;
            $vacanciesData[$vacancyId]['common']['specialization_title'] = $specializationTitle;
            $vacanciesData[$vacancyId]['common']['specialization_code'] = $specializationCode;

            $fullTitle = $vacancyTitle . " / " . $departmentCode;
            if (trim($specializationCode) <> "") $fullTitle .= " / " . $specializationCode;
            $vacanciesData[$vacancyId]['common']['full_title'] = $fullTitle;

            $vacanciesData[$vacancyId]['titles'][0] = $vacancyTitle;

            $resVacanciesSites = data_select(940, "status=0 and f16540='Да' and f16500=$vacancyId"); // открытые публикации
            while ($rowVacancySite = sql_fetch_assoc($resVacanciesSites)) {
                $vacancySiteId = $rowVacancySite['f16510'];
                $vacancyTitleForSite = trim($rowVacancySite['f17470']);

                $vacanciesData[$vacancyId]['titles'][$vacancySiteId] = $vacancyTitleForSite;
            }

        } // end if $positionId >0
    }

    return $vacanciesData;
}

/*
 * Функция для определения Заголовка вакансии по Теме письма
 */
function GetVacancyTitleByMessageSubject($vacanciesData, $subject)
{
    foreach ($vacanciesData as $vacancyId => $vacancyData) {
        foreach ($vacancyData['titles'] as $siteId => $title) {
            if (stripos($subject, $title)) {
                $finalTitle = "";

                $vacancyTitle = $vacancyData['common']['vacancy_title'];
                $departmentCode = $vacancyData['common']['department_code'];
                $specializationCode = $vacancyData['common']['specialization_code'];

                $finalTitle = $vacancyTitle . " / " . $departmentCode;
                if ($specializationCode <> "") $finalTitle .= " / " . $specializationCode;

                return $finalTitle;
            }
        }
    }

    return;
}

/*
 * Функция для определения Заголовка вакансии по Теме письма
 */
function GetVacancyIdByMessageSubject($vacanciesData, $subject)
{
    foreach ($vacanciesData as $vacancyId => $vacancyData) {
        foreach ($vacancyData['titles'] as $siteId => $title) {
            if (stripos($subject, $title)) return $vacancyData['common']['vacancy_id'];
        }
    }

    return;
}

function GetCandidateMaxNum()
{
    $resMaxNum = data_select_field(721, "MAX(f12151) as max_num");
    $rowMaxNum = sql_fetch_assoc($resMaxNum);
    return $rowMaxNum['max_num'];
}

function SendWelcomeEmail($line)
{
    $WELCOME_TEMPLATE_ID = 340;

    // есть емайл, вакансия и ссылка на письмо
    if ($line['id'] > 0 && $line['f12271'] !== '' && $line['f12081'] > 0 && $line['f17490'] > 0) {
        if (!strpos($line['f17640'], 'Информация о получении резюме - SENDED')) {

            // cb_forms
            //  from_mail
            //  from_name

            $new_set = false;
            $old_email = '';
            $old_name = '';

            $id_our_company_vacancy = (int)$line['f24821'];
            if($id_our_company_vacancy>0){
                $our_company = data_select_array(50, "id=".$id_our_company_vacancy);
                $new_email = $our_company['f24841'];
                $new_sender = $our_company['f1420'];

                if($new_email != ''){
                    // update template data

                    $result = sql_query("select from_mail, from_name from cb_forms where id=".$WELCOME_TEMPLATE_ID);
                    if($row = sql_fetch_assoc($result)){
                        $old_email = $row['from_mail'];
                        $old_name = $row['from_name'];

                        // устанавливаем новые значения
                        sql_query(
                            "update cb_forms 
                                 set from_mail='$new_email',
                                     from_name = '$new_sender' 
                                 where 
                                       id=$WELCOME_TEMPLATE_ID");
                        $new_set = true;
                    }
                }
            }

            send_template($WELCOME_TEMPLATE_ID, "id=" . $line['id']);

            if($new_set){
                // возвращаем старые значения
                if($old_email !='' && $old_name != ''){
                    sql_query(
                        "update cb_forms 
                                 set from_mail='$old_email',
                                     from_name = '$old_name' 
                                 where 
                                       id=$WELCOME_TEMPLATE_ID");
                }
            }

        }
    }
}
