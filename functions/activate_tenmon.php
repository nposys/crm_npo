<?php
require_once 'add/tables.php';
require_once 'add/functions/service_functions.php';
require_once 'add/functions/tenmon_functions.php';

function ActivateTenmon($order_id)
{
    global $cb_tables;
    $order_id = (int)($order_id);

    if ($order_id > 0) {
        $rowOrder = data_select_array($cb_tables['tableOrders'], "status=0 and id=$order_id");
        $rowOrderSearch = data_select_array($cb_tables['tableOrdersSearch'], "status=0 and f5411=$order_id");

        $client_id = (int)$rowOrder['f4441'];
        $order_search_id = (int)$rowOrderSearch['id'];
        $price_position_id = (int)$rowOrder['f13071'];

        $paid_search_ids = [1, 55, 62, 64];

        if ($client_id > 0 && $order_search_id > 0) {
            $rowClient = data_select_array($cb_tables['tableClients'], "status=0 and id=$client_id");
            $tenmon_uid = (int)$rowClient['f16230'];

            // готовим набор услуг для включения в тенмон
            $dt_from = date("Y-m-d", strtotime($rowOrderSearch['f6941'])) . " 00:00:00"; // date start
            $dt_to = date("Y-m-d", strtotime($rowOrderSearch['f6951'])) . " 23:59:59"; // date finish
            $dt_now = date("Y-m-d H:i:s");

            $tariff_name = $rowOrderSearch['f16870'];
            $tm_tariff_id = GetTenmonTariffByName($tariff_name); // tenmon tariff
            $products = array();
            $products['search'] = ConvertYesNoToBool($rowOrderSearch['f16880']); // ручной поиск;
            $products['autoSearch'] = ConvertYesNoToBool($rowOrderSearch['f16890']); // автоматический шаблонный поиск
            $products['expertSearch'] = ConvertYesNoToBool($rowOrderSearch['f16900']); // экспертный поиск
            $products['userCount'] = (int)$rowOrderSearch['f16840']; // кол-во пользователей
            $products['integration'] = ConvertYesNoToBool($rowOrderSearch['f16850']); // интеграция с ЭТП
            $products['notification'] = ConvertYesNoToBool($rowOrderSearch['f16860']); // уведомления
            $products['analytics'] = ConvertYesNoToBool($rowOrderSearch['f18691']); // аналитика
            $products['holding'] = ConvertYesNoToBool($rowOrderSearch['f17190']); // группы компаний
            $products['exportLotInfo'] = ConvertYesNoToBool($rowOrderSearch['f19621']); // Выгрузка закупок в Excel
            $products['exportAnalyticInfo'] = ConvertYesNoToBool($rowOrderSearch['f19631']); // Выгрузка результатов аналитики в Excel
            $products['csvLimit'] = (int)$rowOrderSearch['f19641']; // Лимит строк для выгрузки результатов аналитики в Excel
            $products['analyticIndustryEnable'] = ConvertYesNoToBool($rowOrderSearch['f19651']); // Аналитика по произвольным запросам
            $products['forecastEnable'] = ConvertYesNoToBool($rowOrderSearch['f19661']); // Экспертный прогноз снижения на торгах
            $products['lotStatusNotificationEnable'] = ConvertYesNoToBool($rowOrderSearch['f19671']); // Автоматические уведомления о сменах статуса выбранных тендеров
            $products['riskAnalyseEnable'] = ConvertYesNoToBool($rowOrderSearch['f19681']); // Экспертный анализ рисков по заказчикам

            $bGroupOfComapnies = ConvertYesNoToBool($rowOrderSearch['f17190']); // объединение компаний в группы
            $userIds = [];

            if ($tenmon_uid > 0) {
                // включаем поиск только для тех у кого в заказе подошла дата начала
                if ($dt_now >= $dt_from) {
                    $result = SetTenmonServices($tenmon_uid, $tm_tariff_id, $products, $dt_to);
                    if (in_array($price_position_id, $paid_search_ids)) {
                        SetIsCurrentClient($tenmon_uid, true);
                    }
                    if (is_string($result)) {
                        $result_object = json_decode($result);
                        if($result_object->status === 1) {
                            display_notification ('Активация тарифа Тенмон выполнена', 1);

                            $managerSalesId = $rowClient['f7811'];
                            $managerSalesTmId = 0;
                            if ($managerSalesId>0) {
                                $rowManager = data_select_array($cb_tables['tablePersonal'], 'id='.$managerSalesId);
                                $managerSalesTmId = (int)$rowManager['f18631'];
                            }
                            $managerClientId = $rowClient['f9000'];
                            $managerClientTmId = 0;
                            if ($managerClientId>0) {
                                $rowManager = data_select_array($cb_tables['tablePersonal'], 'id='.$managerClientId);
                                $managerClientTmId = (int)$rowManager['f18631'];
                            }
                            if ($managerSalesTmId>0 || $managerClientTmId>0 ) {
                                SetTenmonManagers($tenmon_uid, $managerSalesTmId, $managerClientTmId);
                            }
                        } else {
                            display_notification ('Активация тарифа Тенмон не выполнена:'.$result_object->message, 2);
                        }
                    } else {
                        display_notification ("Активация тарифа Тенмон не выполнена ".
                            "uid:$tenmon_uid ".
                            "tariff:$tm_tariff_id ".
                            "date:$dt_to: ".
                            $result['message'], 2);
                    }
                } else {
                    display_notification ('Заказ не активирован! Сведение о заказе не переданы, так как текущая дата больше даты Начала поиска', 2);
                }
            }

            // активация для группы компаний
            $tenmon_uids = GetTenmonUIDsFromGroupOfComapnies($client_id, $cb_tables);
            foreach ($tenmon_uids as $next_company) {
                $next_client_id = $next_company['client_id'];
                $tenmon_uid = $next_company['tenmon_uid'];

                $userIds[] = $tenmon_uid; // добавляем для объединения в группу компаний

                $row_next_company_search = data_select_array($cb_tables['tableOrdersSearch'], "status=0 and f9260='10. Исполнение' and f10190=$next_client_id");

                if ((int)$row_next_company_search['id'] === 0) {
                    // берем параметры от того заказа который сейчас обрабатываем
                    if ($dt_now >= $dt_from) {
                        SetTenmonServices($tenmon_uid, $tm_tariff_id, $products, $dt_to);
                        if (in_array($price_position_id, $paid_search_ids)) {
                            SetIsCurrentClient($tenmon_uid, true);
                        }
                    }
                }
            }
            // объединяем компаниии в группу компаний в Тенмоен
            if ($bGroupOfComapnies && count($userIds) > 1) {
                // TODO Сделать очистку от лишних тестовых заказов
                JoinGroupOfCompanies($userIds);
            }
        } else {
            display_notification ("Для заказа $order_id не верные".
                " client:$client_id searchOrder:$order_search_id", 2);
        }
    } else {
        display_notification ("Id заказа $order_id не верное", 2);
    }
}
