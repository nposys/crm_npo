<?php
require_once 'add/tables.php';
require_once 'add/functions/service_functions.php';

function AutoChangeOrderStatus($order_id)
{
    global $cb_tables;
    write_log("AutoChangeOrderStatus begin order_id:$order_id",true,true);

    if($order_id>0 && $cb_tables['tableOrders']>0)
    {
        // проверяем что это действующий заказ
        $row_order = data_select_array($cb_tables['tableOrders'],"status=0 and id=$order_id");

        if($row_order['id']>0)
        {
            $order_sum = $row_order['f4461'];
            $order_paid = $row_order['f10270'];
            $status_order = $row_order['f6551'];

            $do_update = false;
            if($order_paid==$order_sum)
            {
                switch($status_order)
                {
                    case '20. Предоплата':
                        $do_update = true;
                        $new_status = '10. Исполнение';
                        break;
                    case '25. Ожидание оплаты':
                        $do_update = true;
                        $new_status = '30. Завершено';
                        break;
                }

                if($do_update) data_update($cb_tables['tableOrders'],EVENTS_ENABLE, array('f6551'=>$new_status),"id=$order_id");
            }
        } // end if row_order
        else
        {
            write_log("AutoChangeOrderStatus error select row_order",true,true);
        }
    } // end if order_id>0
    else
    {
        write_log("AutoChangeOrderStatus error in input params order_id:$order_id cb_tables:".$cb_tables['tableOrders'],true,true);
    }
}
?>