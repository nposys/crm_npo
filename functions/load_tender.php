<?php
require_once 'add/db.php';

function LoadTender($id, $sqlLinkIn)
{
    global $databases;

    $tableTenders = 331;
    $tableRegions = 841;

    if($id>0 and $rowTender = data_select_array($tableTenders,"id=".$id))
    {
        $GovRuID = $rowTender['f5041'];
        $LotNumber = $rowTender['f6391'];

        if(!$sqlLinkIn)
        {
            // connect to MS SQL
            ini_set('mssql.charset','UTF8');
            $serverName = $databases["MSSQL"]["server"];
            $connectionOptions = array(
                "Database" => $databases["MSSQL"]["db"],
                "Uid" => $databases["MSSQL"]["user"],
                "PWD" => $databases["MSSQL"]["pass"],
                'ReturnDatesAsStrings'=>true,

            );

            $sqlLink = sqlsrv_connect($serverName, $connectionOptions);
        }
        else
        {
            $sqlLink = $sqlLinkIn;
        }

        if($sqlLink)
        {
            // $db = sqlsrv_select_db("tenderdb", $sqlLink)
            if (true)
            {
                $query = "
                  SELECT TOP 1
                    t.uid,t.TenderLaw, t.PublicDate,t.OpenDate,t.StartDate,t.ExamDate, t.link, t.Title as Title, t.SiteCode, ts.StatusName,
                    gc.ShortName as CustomerName, gc.LegalAddress as CustomerLegalAddress,
                    gc.Phone as CustomerPhone, gc.Email as CustomerEmail, gc.ContactPerson as CustomerContactPerson,
                    tt.TenderType as TenderType,
                    rg.Title as Region,
                    rg.ID as RegionID,
                    l.LotNumber, l.Cost, l.RequestCost, l.ContractCost, t.TimeZoneUTC
                  FROM
                    UniqueTenders as ut, tenders as t
                      join lots as l on l.TenderUID = t.UID
                      left join GovCustomers as gc on l.CustomerID = gc.ID
                      left join Cities as ct on ct.ID = t.City
                      left join Regions as rg on rg.ID = ct.Region
                      left join Okrugs as ok on ok.ID = rg.Okrug
                      left join TenderTypes as tt on tt.id = t.TenderType
                      left join TenderStatuses as ts on ts.id = t.Status
                  WHERE
                      ut.GovRuID Like '".$GovRuID."%' and t.UID=ut.UID
               ";

                // execute the SQL query and return records
                $res = sqlsrv_query($sqlLink, $query);
                $numRows = sqlsrv_num_rows($res);

                //display the results
                if($sql_row = sqlsrv_fetch_array($res))
                {
                    $new_data = array();

                    switch($sql_row['TenderLaw'])
                    {
                        case 1:
                            $new_data['f5031'] = "44-ФЗ";
                            break;
                        case 2:
                            $new_data['f5031'] = "223-ФЗ";
                            break;
                        case 3:
                            $new_data['f5031'] = "Прочие";
                            break;
                        default:
                            $new_data['f5031'] = "";
                            break;
                    }

                    // -------------------------------------------------------
                    // get id for Вид тенедера
                    $rowTenderType = data_select_array(481, 'status=0 and f6681="'.$sql_row["TenderType"].'"');
                    $new_data['f5111'] = $rowTenderType['id']; // вид тендера

                    // -------------------------------------------------------
                    // get id for Площадка
                    if (trim($sql_row["SiteCode"]) <> "")
                    {
                        $rowTenderSite = data_select_array(471, 'status=0 and f8100="'.$sql_row["SiteCode"].'"');
                        $new_data['f6341'] = $rowTenderSite['id'];
                    }else{
                        $new_data['f6341'] = "";
                    }

                    // -------------------------------------------------------
                    // готовим ссылку на регион
                    $regionID = "";
                    if(intval($sql_row['RegionID'])>0)
                    {
                        $rowRegion = data_select_array($tableRegions, "status=0 and f14441=".$sql_row['RegionID']);
                        $regionID = $rowRegion['id'];
                    }

                    // -------------------------------------------------------
                    $new_data['f6391'] = $LotNumber;
                    $new_data['f5051'] = $sql_row["Title"];
                    $new_data['f6381'] = $sql_row["StatusName"];
                    $new_data['f5121'] = $sql_row["link"];

                    $new_data['f5061'] = $sql_row["CustomerName"];
                    $new_data['f6411'] = $sql_row["CustomerLegalAddress"] . " " . $sql_row["CustomerPhone"] . " " . $sql_row["CustomerEmail"] . " " . $sql_row["CustomerContactPerson"];
                    $new_data['f6401'] = $regionID; // $sql_row["Region"];

                    $new_data['f5071'] = $sql_row["Cost"];
                    $new_data['f5131'] = $sql_row["RequestCost"];
                    $new_data['f5141'] = $sql_row["ContractCost"];

                    $new_data['f5081'] = $sql_row["PublicDate"];
                    $new_data['f5091'] = $sql_row["OpenDate"];
                    $new_data['f6371'] = $sql_row["ExamDate"];
                    $new_data['f5101'] = $sql_row["StartDate"];

                    if($sql_row["TimeZoneUTC"]<>"")
                    {
                        $new_data['f13991'] = intval($sql_row["TimeZoneUTC"]);

                        // корректируем  даты и время
                        $utcDiff = 7 - intval($sql_row['TimeZoneUTC']);

                        if($utcDiff<>0)
                        {
                            if($utcDiff>0) $time_diff = " + ".$utcDiff; else $time_diff = $utcDiff;

                            $datePublicNsk = date('Y-m-d H:i:s', strtotime($sql_row['PublicDate'].$time_diff." hour"));
                            $dateOpenNsk = date('Y-m-d H:i:s', strtotime($sql_row['OpenDate'].$time_diff." hour"));
                            $dateExamNsk = date('Y-m-d H:i:s', strtotime($sql_row['ExamDate'].$time_diff." hour"));
                            $dateStartNsk = date('Y-m-d H:i:s', strtotime($sql_row['StartDate'].$time_diff." hour"));
                        }
                        else
                        {
                            $datePublicNsk = $sql_row['PublicDate'];
                            $dateOpenNsk = $sql_row['OpenDate'];
                            $dateExamNsk = $sql_row['ExamDate'];
                            $dateStartNsk = $sql_row['StartDate'];
                        }

                        $new_data['f14011'] = $datePublicNsk;
                        $new_data['f13971'] = $dateOpenNsk;
                        $new_data['f14021'] = $dateExamNsk;
                        $new_data['f13981'] = $dateStartNsk;

                    } // end if $sql_row["TimeZoneUTC"]

                    data_update($tableTenders,$new_data,"id=".$id);

                } // end if  $sql_row
            } // end if $db

            //close the connection
            if(!$sqlLinkIn)
                sqlsrv_close($sqlLink);
        } // end if sqlLink
    } // end if $rowTender

} //end function

?>